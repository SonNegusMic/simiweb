<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include("../funciones/connPDO.php");

class Negocios
{
    public function __construct($conn=""){
		$this->db=$conn;
	}
    
    private $id_usuario;
  
	public function consultaClienteSelect($cedula,$inmob,$idselect)
    {
	   $w_conexion = new MySQL(); 
	   getSelectP('clientes_inmobiliaria','cedula', 'CONCAT(UPPER(LEFT(nombre, 1)), LOWER(SUBSTRING(nombre, 2)))',"","where inmobiliaria=$inmob","$idselect","",'','class="form-control chosen-select" id="$idselect"','','','Seleccione Cliente');
    }
	public function consultaCliente($cedula,$inmob)
    {
 		if(($consulta=$this->db->prepare(" select telfijo,telcelular,email
		from clientes_inmobiliaria
        where cedula=?
        and inmobiliaria=?")))
		{  	
			$consulta->bind_param("ii",$cedula,$inmob);
			$consulta->execute();
//				echo "$qry";
			if($consulta->bind_result($telfijo,$telcelular,$email))
			{
				while($consulta->fetch())
				{
					$cadena=$telfijo."  ".$telcelular."?".$email;    
				}
			}
			echo $cadena;
		}
		else
		{
			echo  "error --".$conn->error;
		}
        
    }
	
	public function creaDemanda($inmob,$FechaD,$IdPromotor,$numced,$calle,$alacalle,$carrera,$alacarrera,$Ciudad,$barrio,$idgestion,$IdDestinacion,$IdTipoInm,$AreaIni,$AreaFin,$ValInicial,$ValFinal,$mgrupo,$descripcion,$fsis)
	{
 		$w_conexion = new MySQL(); 
		include('../mcomercialweb/emailtaeD.class.php');
		$f_sis=date('Y-m-d');
		$clave='0000';
		$idDemanda=consecutivo('IdDemanda','demandainmuebles',0);
		$sql="INSERT INTO demandainmuebles 
		(FechaD,IdPromotor,FechaLimite,CedulaIInt,Calle,
		 AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
		 IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
		 Descripcion,clave,IdCiudad,IdInmobiliaria,IdBarrio,
		 grupo_dem,IdDemanda)
		 VALUES ('$fsis','$IdPromotor','$FechaD','$numced','$calle',
			'$alacalle','$carrera','$alacarrera','$idgestion','$IdDestinacion',
			'$IdTipoInm','$ValInicial','$ValFinal','$AreaIni','$AreaFin',
			'$descripcion','$clave','$Ciudad','$inmob','$barrio',
			'$mgrupo','$idDemanda')";
			$res=$w_conexion->RecordSet($sql);
			if($res)
			{
				//echo "res $sql";
				echo $idDemanda;
				$envioemail = new EmailTae();
   			 	$envioemail->EnvioEmailDemandas($IdPromotor,$f_sis,$FechaD,$numced,$idDemanda);
		   	 	$enviado= $envioemail->ResultadoCorreoDemada();
			}
    }
	
	public function editaDemanda($inmob,$FechaD,$IdPromotor,$numced,$calle,$alacalle,$carrera,$alacarrera,$Ciudad,$barrio,$idgestion,$IdDestinacion,$IdTipoInm,$AreaIni,$AreaFin,$ValInicial,$ValFinal,$mgrupo,$descripcion,$fsis,$IdDemanda,$Estado)
	{
 		$w_conexion = new MySQL(); 
		$f_sis=date('Y-m-d');
		$h_sis=date('H:i:s');
		$usuario=$_SESSION['Id_Usuarios'];
		$ip=$_SERVER['REMOTE_ADDR'];

		
		//consulta de valores originales para guardar bitácora 2014-12-16
		$sql_orig="SELECT IdDemanda,IdPromotor,FechaLimite,Calle,
					AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
					IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
					Descripcion,Estado,IdBarrio,grupo_dem
					FROM demandainmuebles
					where IdDemanda='$IdDemanda'";
		$res_or=$w_conexion->ResultSet($sql_orig);
		while($f_or=$w_conexion->FilaSiguienteArray($res_or))
		{
			$IdPromotor_or 		= $f_or['IdPromotor']; 
			$FechaLimite_or 	= $f_or['FechaLimite']; 
			$Calle_or 			= $f_or['Calle']; 
			$AlaCalle_or 		= $f_or['AlaCalle']; 
			$Carrera_or 		= $f_or['Carrera']; 
			$AlaCarrera_or 		= $f_or['AlaCarrera']; 
			$IdGestion_or 		= $f_or['IdGestion']; 
			$IdDestinacion_or 	= $f_or['IdDestinacion']; 
			$IdTipoInm_or 		= $f_or['IdTipoInm']; 
			$ValInicial_or 		= $f_or['ValInicial']; 
			$ValFinal_or 		= $f_or['ValFinal']; 
			$AreaInicial_or 	= $f_or['AreaInicial']; 
			$AreaFinal_or 		= $f_or['AreaFinal'];
			$Descripcion_or 	= $f_or['Descripcion']; 
			$Estado_or 			= $f_or['Estado']; 
			$IdBarrio_or 		= $f_or['IdBarrio']; 
			$grupo_dem_or 		= $f_or['grupo_dem'];
		}
		
		$sql="UPDATE demandainmuebles 
				SET		
				IdPromotor			='$IdPromotor',
				FechaLimite			='$FechaD',	
				CedulaIInt			='$numced',	
				Calle				='$calle',
		 		AlaCalle			='$alacalle',
				Carrera				='$carrera',
				AlaCarrera			='$alacarrera',
				IdGestion			='$idgestion',
				IdDestinacion		='$IdDestinacion',
		 		IdTipoInm			='$IdTipoInm',
				ValInicial			='$ValInicial',
				ValFinal			='$ValFinal',
				AreaInicial			='$AreaIni',
				AreaFinal			='$AreaFin',
		 		Descripcion			='$descripcion',
				IdCiudad			='$Ciudad',
				IdBarrio			='$barrio',
		 		grupo_dem			= '$mgrupo',
				Estado				= '$Estado'	
				WHERE IdDemanda		= '$IdDemanda'";
			$res=$w_conexion->RecordSet($sql);
			if($res)
			{
				///guardar historial
				if($IdPromotor_or!=$IdPromotor)
				{
					$cambio++;
					$nom_campo="IdPromotor";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Promotor"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$IdPromotor,$IdPromotor_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($FechaLimite_or!=$FechaD)
				{
					$cambio++;
					$nom_campo="FechaLimite";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Fecha Limite"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$Fechalimite,$FechaLimite_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($Calle_or!=$calle)
				{
					$cambio++;
					$nom_campo="Calle";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Calle"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$calle,$Calle_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($AlaCalle_or!=$alacalle)
				{
					$cambio++;
					$nom_campo="AlaCalle";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="A la Calle"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$alacalle,$AlaCalle_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($Carrera_or!=$carrera)
				{
					$cambio++;
					$nom_campo="Carrera";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Carrera"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$carrera,$Carrera_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($AlaCarrera_or!=$alacarrera)
				{
					$cambio++;
					$nom_campo="AlaCarrera";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="A la Carrera"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$alacarrera,$AlaCarrera_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($IdGestion_or!=$idgestion)
				{
					$cambio++;
					$nom_campo="IdGestion";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Gestion"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$idgestion,$IdGestion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($IdDestinacion_or!=$IdDestinacion)
				{
					$cambio++;
					$nom_campo="IdDestinacion";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Destinacion"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$IdDestinacion,$IdDestinacion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($IdTipoInm_or!=$IdTipoInm)
				{
					$cambio++;
					$nom_campo="IdTipoInm";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Tipo de Inmueble"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$IdTipoInm,$IdTipoInm_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($ValInicial_or!=$ValInicial)
				{
					$cambio++;
					$nom_campo="ValInicial";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Valor Inicial"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$ValInicial,$ValInicial_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($ValFinal_or!=$ValFinal)
				{
					$cambio++;
					$nom_campo="ValFinal";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Valor Final"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$ValFinal,$ValFinal_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($AreaInicial_or!=$AreaIni)
				{
					$cambio++;
					$nom_campo="AreaInicial";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Area Inicial"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$AreaIni,$AreaInicial_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($AreaFinal_or!=$AreaFin)
				{
					$cambio++;
					$nom_campo="AreaFin";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Area Final"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$AreaFin,$AreaFinal_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($Descripcion_or!=$descripcion)
				{
					$cambio++;
					$nom_campo="Descripcion";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Descripcion"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$descripcion,$Descripcion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($Estado_or!=$Estado)
				{
					$cambio++;
					$nom_campo="estado_dem";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Estado"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$Estado,$Estado_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($IdBarrio_or!=$barrio)
				{
					if(is_numeric($barrio))
					{
						$cambio++;
						$nom_campo="barrio";
						$cons_log=consecutivo_log(1);
						$campo_usu_g="Barrio"; //nombre del campo en el formulario
						$param_g=0; //codigo de la tabla par?metros
						guarda_log_gral($cons_log,1,$nom_campo,$barrio,$IdBarrio_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
					}
				}
				if($grupo_dem_or!=$mgrupo)
				{
					$cambio++;
					$nom_campo="grupo";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Grupo"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$mgrupo,$grupo_dem_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
	
				echo $idDemanda;
	}
}
        
    
	public function datosDemanda($idDemanda,$inmob)
    {
        $w_conexion = new MySQL();
		$arreglo1 = array();
		$sqlpagos = "SELECT FechaD,IdPromotor,FechaLimite,CedulaIInt,Calle,
		 AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
		 IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
		 Descripcion,clave,IdCiudad,IdInmobiliaria,IdBarrio,
		 grupo_dem,IdDemanda,Estado
		 from demandainmuebles
		 where IdDemanda='$idDemanda'";
		//echo $sqlpagos."<br>";			
		$res=$w_conexion->ResultSet($sqlpagos);
		$cantidadt=$w_conexion->FilasAfectadas($res);
		if($cantidadt==0)
		{echo "No Hay Demandas para la busqueda seleccionada";}
		
		while ($fila7=$w_conexion->FilaSiguienteArray($res))
		{
			$i++;
				
				$arreglo[0]  =$fila7['FechaD'];
				$arreglo[1]  =$fila7['IdPromotor'];
				$arreglo[2]  =$fila7['FechaLimite'];
				$arreglo[3]  =$fila7['CedulaIInt'];
				$arreglo[4]  =ucwords(strtolower(getCampo('cliente_captacion','WHERE id_cliente_capta='.$fila7['CedulaIInt'].' and id_inmobiliaria='.$inmob,'nombre')));
				$arreglo[5]  =$fila7['Calle'];
				$arreglo[6]  =$fila7['IdBarrio'];
				$arreglo[7]  = ucwords(strtolower(getCampo('ciudad','WHERE IdCiudad='.getCampo('barrios','WHERE IdBarrios='.$fila7['IdBarrio'],'IdCiudad'),'Nombre')));
				$arreglo[9]  =$fila7['AlaCalle'];
				$arreglo[10] =$fila7['Carrera'];
				$arreglo[11] =$fila7['AlaCarrera'];
				$arreglo[12] =$fila7['IdGestion'];
				$arreglo[13] =$fila7['IdDestinacion'];
				$arreglo[14] =$fila7['IdTipoInm'];
				$arreglo[15] =$fila7['ValInicial'];
				$arreglo[16] =$fila7['ValFinal'];
				$arreglo[17] =$fila7['AreaInicial'];
				$arreglo[18] =$fila7['AreaFinal'];
				$arreglo[19] =$fila7['Descripcion'];
				$arreglo[20] =$fila7['IdCiudad'];
				$arreglo[21] =$fila7['grupo_dem'];
				$arreglo[22] =$fila7['Estado'];
			 
			 $arreglo1[] = $arreglo;		
		}
		return $arreglo1;
    }
	
	public function datosMisDemanda($tip_inm,$dest,$tip_ope,$asesor,$est_cli,$cod_inm,$ffecha_ini,$ffecha_fin,$inmob,$perfil,$usu)
    {
        $w_conexion = new MySQL();
		
		$cond="";
		$cond2 ="";
		if($tip_inm>0)
		{
			$cond .=" and IdTpInm='".$tip_inm."'";
		}
		if($dest>0)
		{
			$cond .=" and IdDestinacion='".$dest."'";
		}
		if($tip_ope>0)
		{
			if($tip_ope!=2)
			{
				$cond .=" and IdGestion='".$tip_ope."'";
			}
			else
			{
				$cond .=" and IdGestion in ($tip_ope,1,5)";
			}
		}
		if($asesor>0)
		{
			$cond .=" and usu_cm='".$asesor."'";
		}
		if($est_cli>0)
		{
			$cond .=" and est_cm='".$est_cli."'";
		}
		if($cod_inm>0)
		{
			$cond .=" and inmuebles.idInm='".$cod_inm."'";
		}
        if($ffecha_ini>0 and $ffecha_fin>0)
		{
			$cond .=" and fecha_cm  between'".$ffecha_ini."' and '".$ffecha_fin."'";
		}
		if($inmob!=1)
		{
			$cond2 .="AND IdInmobiliaria = '".$inmob."'";
		}
		if($perfil==3 or $perfil==10)
		{
			//$cond2 .=" ";
		}
		else
		{
			$cond2 .=" AND IdPromotor = '".$usu."'";
		}
		$hoy=date("Y-m-d");
		$sqlpagos = "";
		
		$data = array();
		$sqlpagos = "Select idInm,  IdGestion,  IdTpInm, ComiArren,ComiVenta,  
					Direccion,  IdBarrio ,IdInmobiliaria,condiciones_compartir_inm.*,IdPromotor,
					ValComiVenta,ValComiArr,IdDestinacion
					FROM inmuebles,condiciones_compartir_inm
					WHERE  idInm=id_inmu_cm
					and idEstadoinmueble<=2
					$cond2
					$cond";
		//echo $sqlpagos."-------------------<br>";
//		echo $perfil."ffffffffffffff";
				
		$res=$w_conexion->ResultSet($sqlpagos);
		$cantidadt=$w_conexion->FilasAfectadas($res);
		if($cantidadt==0)
		{
			//echo "No Hay Demandas para la busqueda seleccionada";
		}
		
		while ($fila77=$w_conexion->FilaSiguienteArray($res))
		{
			$i++;
			
			$lupa='<button type="button" id="botonpre2" class="btn btn-warning botonpre2"  data-toggle="modal"  data-target=".mymodales"><span class="glyphicon glyphicon-edit"></span></button>';
					
			$btnCLiente='<button type="button" id="botonpre3" class="btn btn-info  botonpre3"  data-toggle="modal"  data-target=".mymodalescli"><span class="glyphicon glyphicon"><i class="fa fa-files-o"></i></span></button>';
					
					$botonedit = '<input type="hidden" class="idseg" value="'.$idseguimiento.'"><input type="hidden" class="idimb" value="'.$idInmueble.'">'.$lupa." ".$btnCLiente;
			
			 $data[]=array(
				"idInm"          => $fila77['idInm'],
				"Nombres"        => ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$fila77['IdPromotor']."'",'Nombres'))),
				"apellidos"      => ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$fila77['IdPromotor']."'",'apellidos'))),
				"NCompleto"      => ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$fila77['IdPromotor']."'",'concat(Nombres," ",apellidos)'))),
				"condiciones_cm" => $fila77['condiciones_cm'],
				"ComiVenta"      => $fila77['ComiVenta']*100,
				"ComiArren"      => $fila77['ComiArren']*100,
				"ValComiArr"     => number_format($fila77['ValComiArr']),
				"ValComiVenta"   => number_format($fila77['ValComiVenta']),
				"NombresGestion" => ucwords(strtolower(getCampo('gestioncomer',"where IdGestion='".$fila77['IdGestion']."'",'NombresGestion'))),
				"destinacion"    => ucwords(strtolower(getCampo('destinacion',"where Iddestinacion='".$fila77['IdDestinacion']."'",'Nombre'))),
				"tipoinmuebles"  => ucwords(strtolower(getCampo('tipoinmuebles',"where idTipoInmueble='".$fila77['IdTpInm']."'",'Descripcion'))),
				"estcm"          => getCampo('parametros',"WHERE id_param=22 and conse_param='".$fila77['est_cm']."'",'desc_param'),
				"barr"           => ucwords(strtolower(getCampo('barrios',"WHERE IdBarrios='".$fila77['IdBarrio']."'",'NombreB'))),
				"botones"        => $botonedit
			 );
			 	
		}
		echo json_encode($data);
    }
    public function getUsuariosDemandas($idInmo)
     {
      $connPDO= new Conexion();
      $stmt = $connPDO->prepare("SELECT u.Nombres,
                  						u.Id_Usuarios,
                  						u.apellidos
          						FROM usuarios u,
               						 demandainmuebles d
          						WHERE u.Id_Usuarios=d.IdPromotor
          						AND u.IdInmmo=:idInmmo
          						GROUP BY d.IdPromotor
          						ORDER BY u.Nombres");
      $stmt->bindParam(":idInmmo", $idInmo["idInmmo"]);
      $stmt->execute();
      $data=array();
      while($row=$stmt->fetch())
      {
       $data[]=array(
         "id"=>$row["Id_Usuarios"],
         "nombres"=>ucwords(strtolower($row["Nombres"])),
         "apellidos"=>ucwords(strtolower($row['apellidos'])));
      }
      return $data;

     }
     public function listDemandas($data)
     {
		  
		   $connPDO= new Conexion();
		  
		   $cond="";
		   if($data['tip_inm'])
		   {
		    $cond .=" and d.IdTipoInm=:inmueble";

		   }
		   if($data['dest'])
		   {
		    $cond .=" and d.IdDestinacion=:dest";
		   }
		   if($data['tip_ope'])
		   {
			   if($data['tip_ope']!=2)
			   {
				$cond .=" and d.IdGestion=:tip_ope";
			   }
			   else
			   {
				   $cond .=" and d.IdGestion != 3";
			   }
		   }
		   if($data['asesor'])
		   {
		    $cond .=" and d.IdPromotor=:asesor";
		   }
		   if($data['est_cli'])
		   {
		    $cond .=" and d.Estado=:est_cli";
		   }
		   if($data['codDemanda'])
		   { //echo "ingresa a codDemanda ".$_GET['codDemanda'];
		    $cond .=" and d.IdDemanda=:codDemanda";
		   }
			if($data['interesado']) 
			{
				$cond .=" and d.CedulaIInt =:interesado";
   			}
   			if($data["ffecha_ini"]>0 and $data["ffecha_fin"]>0)
			{
				$cond .=" and fecha_cm  between :ffecha_ini and :ffecha_fin";
			}
			if($data['idInmmo']!=1)
			{
				$cond2 .="AND IdInmobiliaria = :idInmmo";
				
			}
			if($data['perfil']==3 or $data['perfil']==10)
			{
			
			}
			$hoy=date("Y-m-d");
 
			 $stmt = $connPDO->prepare("SELECT d.IdDemanda, 
								d.FechaD, 
								d.IdPromotor, 
								d.FechaLimite, 
			      				d.CedulaIInt,
			      				d.Calle,
			      				d.AlaCalle,
			      				d.Carrera, 
			      				d.AlaCarrera,                                    
			      				d.IdGestion, 
			      				d.IdDestinacion, 
			      				d.IdTipoInm, 
			      				d.ValInicial, 
			      				d.ValFinal, 
			      				d.AreaInicial, 
			      				d.AreaFinal, 
			      				d.Descripcion, 
			                    c.Nombre as Interesado,
			                    c.email,
			                    c.telfijo,
			                    c.telcelular,
			                    g.NombresGestion, 
			                    t.Descripcion As TipoInmueble, 
			      				c.cedula as cedulaInte,
			      				d.Estado, 
			      				ba.NombreB, 
			      				d.grupo_dem,
			      				des.Nombre as Nombredes,
			      				IF ('$hoy'  BETWEEN d.FechaD AND d.FechaLimite,1,0) 
			      				as RESULTADO
			      		FROM 	demandainmuebles d 
			      		INNER JOIN clientes_inmobiliaria c On d.CedulaIInt = c.cedula 
			      		LEFT JOIN barrios ba ON ba.IdBarrios 			   = d.IdBarrio
			      		INNER JOIN gestioncomer g On d.IdGestion           =g.IdGestion
			      		INNER JOIN tipoinmuebles t On d.IdTipoInm =t.idTipoInmueble 
			      		INNER JOIN destinacion des on des.Iddestinacion = d.IdDestinacion 
			      		WHERE c.inmobiliaria = :idInmmo
			      		$cond2
			      		$cond");
			if($data['tip_inm'])
		   {
		    $stmt->bindParam(":inmueble",$data['tip_inm']);

		   }
		   if($data['dest'])
		   {
		    $stmt->bindParam(':dest',$data["dest"]);
		   }
		   if($data['tip_ope'])
		   {
		    $stmt->bindParam(":tip_ope",$data["tip_ope"]);
		   }
		   if($data['asesor'])
		   {
		    $stmt->bindParam(":asesor",$data["asesor"]);
		   }
		   if($data['est_cli'])
		   {
		    $stmt->bindParam(':est_cli',$data["est_cli"]);
		   }
		   if($data['codDemanda'])
		   { //echo "ingresa a codDemanda ".$_GET['codDemanda'];
		    $stmt->bindParam(":codDemanda",$data["codDemanda"]);
		   }
			if($data['interesado']) 
			{
				$stmt->bindParam(":interesado",$data["interesado"]);
   			}
   			if($data["ffecha_ini"]>0 and $data["ffecha_fin"]>0)
			{
				$stmt->bindParam(':ffecha_ini',$data["ffecha_ini"]);
				$stmt->bindParam(':ffecha_fin',$data["ffecha_fin"]);
			}
			if($data['idInmmo']!=1)
			{
				$cond2 .="AND IdInmobiliaria = :idInmmo";
				
			}
			if($data['perfil']==3 or $data['perfil']==10)
			{
			
			}
			
			$stmt->bindParam(':idInmmo',$data["idInmmo"]);
			$stmt->execute();
      		$listnegocios=array();
      		while($row=$stmt->fetch())
      		{
      			

      			$negociarBtn=($row["RESULTADO"] == 0)?"":"<button class='btn btn-success btn-sm'><span class='glyphicon glyphicon-ok'></span></button>";
      			$editarBtn="<button class='btn btn-warning btn-sm' ><span class='fa fa-edit'></span></button>";
      			$cambiosBtn="<button class='btn btn-info btn-sm' data-toggle='modal' data-target='#modal-logs'><span class='glyphicon glyphicon-eye-open'></span></button>";
      			$seguimientoBtn="<button class='btn btn-danger btn-sm' data-toggle='modal' data-target='#modal-id' alt='seguimiento'><span class='fa fa-sign-in'></span></button>";
      			// $ofertabtn=($this->seguiDemandas($row["IdDemanda"])==1)?"<button class='btn btn-success btn-sm'><span class='glyphicon glyphicon-ok'></span></button>":"";
      			$dataModal=$negociarBtn.$editarBtn.$cambiosBtn.$seguimientoBtn;
      			$calle=ucwords(strtolower("De la calle ".$row["Calle"]." a la ".$row["AlaCalle"]));
      			$carrera=ucwords(strtolower("De la carrera ".$row["Carrera"]." a la ".$row["AlaCarrera"]));
      			$dataCliente=$row['telfijo']."-".$row['telcelular'];
      				
      			$listnegocios[]=array(
									"idemanda"     =>$row["IdDemanda"],
									"destinacion"  =>ucwords(strtolower($row["Nombredes"])),
									"fechaLimite"  =>$row["FechaLimite"],
									"fechaD"       =>$row["FechaD"],
									"interesado"   =>ucwords(strtolower($row["Interesado"])),
									"barrio"       =>ucwords(strtolower($row["NombreB"])),
									"gestion"      =>ucwords(strtolower($row["NombresGestion"])),
									"tipoInmueble"  =>ucwords(strtolower($row["TipoInmueble"])),
									"valorInicial" =>"$".number_format($row["ValInicial"]),
									"valorFinal"   =>"$".number_format($row["ValFinal"]),
									"Calle"	   	   =>$calle,
									"Carrera"	   =>$carrera,
									"btnModal"     =>$dataModal,
									"telcli"	   =>$dataCliente,
									"mailcli"      =>$row["email"]         
									
      							);
      		}
      		return $listnegocios;
     } //fin function listDemandas()
	 
	 
	 public function listInmDemandas($data)
     {
		 //Danny
		 
		$connPDO= new Conexion();
		$Demandas	= new Negocios();
		
		$cond="";
		if($data['tip_inm'])
		{
		$cond .=" and i.IdTpInm=:inmueble";
		
		}
		if($data['dest'])
		{
		$cond .=" and i.IdDestinacion=:dest";
		}
		if($data['tip_ope'])
	   {
		   if($data['tip_ope']!=2)
		   {
			$cond .=" and i.IdGestion=:tip_ope";
		   }
		   else
		   {
			   $cond .=" and i.IdGestion != 3";
		   }
	   }
		if($data['asesor'])
		{
		$cond .=" and i.IdPromotor=:asesor";
		}
		if($data['idInm'])
		{ //echo "ingresa a codDemanda ".$_GET['codDemanda'];
		$cond .=" and i.idInm=:idInm";
		}
		/*if($data['idInmmo']!=1)
		{
			$cond2 .="AND IdInmobiliaria = :idInmmo";
		}*/
		if($data['perfil']==3 or $data['perfil']==10)
		{
		
		}
		$hoy=date("Y-m-d");
		
		 $stmt = $connPDO->prepare("SELECT idInm,IdGestion,IdDestinacion,ValorVenta,ValorCanon,IdPromotor,IdTpInm,politica_comp
					FROM inmuebles i 
					WHERE i.IdInmobiliaria = :idInmmo
					and i.politica_comp!=''
					$cond2
					$cond");
		if($data['tip_inm'])
		{
			$stmt->bindParam(":inmueble",$data['tip_inm']);
		}
		if($data['dest'])
		{
			$stmt->bindParam(':dest',$data["dest"]);
		}
		if($data['tip_ope'])
		{
			$stmt->bindParam(":tip_ope",$data["tip_ope"]);
		}
		if($data['asesor'])
		{
			$stmt->bindParam(":asesor",$data["asesor"]);
		}
		
		if($data['idInm'])
		{ //echo "ingresa a codDemanda ".$_GET['codDemanda'];
			$stmt->bindParam(":idInm",$data["idInm"]);
		}
		
		if($data['idInmmo']!=1)
		{
			$cond2 .="AND IdInmobiliaria = :idInmmo";
			
		}
		if($data['perfil']==3 or $data['perfil']==10)
		{
		
		}
		
		$stmt->bindParam(':idInmmo',$data["idInmmo"]);
		$stmt->execute();
		$listnegocios=array();
		while($row=$stmt->fetch()) 
		{	
			///boton para descartar demanda
			$descartarBtn=($row["RESULTADO"] == 1)?"":"<button class='btn btn-danger btn-sm dct' data-toggle='modal' data-target='#modal1-id' title='Descartar Demanda'><span class='glyphicon fa fa-times-circle'></span></button>";
			
			$negociarBtn=($row["RESULTADO"] == 0)?"":"<button class='btn btn-success btn-sm' 	><span class='glyphicon fa fa-check-square-o'></span></button>";
			$editarBtn="<button class='btn btn-warning btn-sm dct1' title='Buscar Demanda' data-toggle='modal' data-target='#modal2-id'><span class='fa fa-search'></span></button>";
			$cambiosBtn="<button class='btn btn-info btn-sm' toggle='modal' data-target='#modal-id'><span class='glyphicon glyphicon-eye-open'></span></button>";
			$seguimientoBtn="<button class='btn btn-danger btn-sm'><span class='fa fa-sign-in'></span></button>";
			// $ofertabtn=($this->seguiDemandas($row["IdDemanda"])==1)?"<button class='btn btn-success btn-sm'><span class='glyphicon glyphicon-ok'></span></button>":"";
			$dataModal=$descartarBtn.$negociarBtn.$editarBtn.$cambiosBtn.$seguimientoBtn;
			$dem=$Demandas->buscarDemandas($row["idInm"]);
			//echo $dem;
				//$dem=293;
			$listnegocios[]=array(
								"idInm"     	=> $row["idInm"],
								"destinacion"  	=> ucwords(strtolower(getCampo('destinacion',"where IdDestinacion=".$row["IdDestinacion"],'Nombre'))),
								"gestion"      	=> ucwords(strtolower(getCampo('gestioncomer',"where IdGestion=".$row["IdGestion"],'NombresGestion'))),
								"tipoInmueble"  => ucwords(strtolower(getCampo('tipoinmuebles',"where idTipoInmueble=".$row["IdTpInm"],'Descripcion'))),
								"ValorVenta" 	=> "$".number_format($row["ValorVenta"]),
								"ValorCanon"   	=> "$".number_format($row["ValorCanon"]),
								"politica"	    => $row["politica_comp"],
								"promotor"	   	=> ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$row["IdPromotor"]."'",'concat(Nombres," ",apellidos)'))),
								"dem"	   		=> "$dem",
								"btnModal"     	=> $dataModal
								
							);
		}//print_r($listnegocios)."ffff";
		return $listnegocios;
     } //fin function listDemandas()
	 
     public function observaDemandas($idDemanda)
     {
     	$connPDO= new Conexion();

     	$stmt=$connPDO->prepare("SELECT cons_log
								FROM    lg_ch_tb 
								WHERE tipo_log = 1 
  								AND client_g = :idDemanda 
								ORDER BY cons_log DESC ");

     	$stmt->bindParam("idDemanda",$idDemanda);
     	$stmt->execute();
     	$seguimiento=$stmt->rowCount();
     	if($seguimiento>0)
     	{
     		return 1;
     	}
     }
     public function saveTracingDemandas($sDemandas,$idSeguimiento)
     {
     	$connPDO= new Conexion();
     	try {
     		$stmt=$connPDO->prepare("INSERT INTO SeguimientoDemandaNvo
												(idSeguimiento,
												 idDemanda,tseguimiento,
												 observacion,resseg,
												 frec,horarec,
												 agendar,agendarc,
												 mailcop) 
						     		 VALUES 	(:idSeguimiento,
												 :idDemanda,:tseguimiento,
												 :observacion,:resseg,
												 :frec,:horarec,
												 :agendar,:agendarc,
												 :mailcop)");
     		if($stmt->execute(array(
						':idSeguimiento' => $idSeguimiento,
						':idDemanda'     => $sDemandas['idDemanda'],    
						':tseguimiento'  => $sDemandas['tseguimiento'], 
						':observacion'   => $sDemandas['descripcion'],  
						':resseg'        => $sDemandas['resseg'],       
						':frec'          => $sDemandas['frec'],         
						':horarec'       => $sDemandas['hora_ini'],      
						':agendar'       => $sDemandas['agendar'],      
						':agendarc'      => $sDemandas['agendarc'],
						':mailcop'       => $sDemandas['mailcop'],
					)))
					{
						return 1;
					}   
			if($sDemandas['cerrard']==1)
			{
				$this->closeDemandas($sDemandas['idDemanda']);
			}
     	}
     	catch (PDOException $e) 
		{
    		print $e->getMessage ();
     	}

     }
     public function closeDemandas($cDemandas)
     {
     	$connPDO= new Conexion();
     	try
     	{
     		$stmt=$connPDO->prepare("UPDATE demandainmuebles 
     				 			 SET Estado 	 = 1
     				             WHERE IdDemanda = :IdDemanda");
   			$stmt->bindParam(":IdDemanda",$cDemandas);
   			

   			if($stmt->execute())
   			{
   				return 1;
   			}

     	}
     	catch (PDOException $e) {
    		print $e->getMessage ();
     	}

     	
     }
     public function seguimientoDemandas($seDemandas)
     {
     	$connPDO= new Conexion();

     	try
     	{
     		$stmt=$connPDO->prepare("SELECT  idSeguimiento,										 				 
											idDemanda,
     										 tseguimiento,
     										 observacion,
     										 resseg,frec,
     										 horarec,agendar,
     										 agendarc,mailcop
									 FROM SeguimientoDemandaNvo
									 WHERE  idDemanda=:idDemanda
									 order by idSeguimiento desc");
			$stmt->bindParam(":idDemanda",$seDemandas);
   			$stmt->execute();
   			$datas=array();
   			while ($row=$stmt->fetch()) {
   				$nomTseguimiento=getCampo('parametros',"where id_param=20 and conse_param=".$row['tseguimiento'],'desc_param');
   				$reSeguimiento=getCampo('parametros',"where id_param=10 and conse_param=".$row['resseg'],'desc_param');
   				$copyCliente=getCampo('parametros',"where id_param=3 and conse_param=".$row['agendarc'],'desc_param');
   				$fechaRec=($row['frec'] != '0000-00-00')?$row['frec']." <br>". $row['horarec']:"NO";

   				
   				$datas[]=array(
   					'tiposeguimiento'=>$nomTseguimiento,
   					'observacion'	 =>$row["observacion"],
   					'resultado'		 =>$reSeguimiento,
   					'recordatorio'   =>$fechaRec,
   					'copiarCliente'  =>$copyCliente
   				);
   			}
   			return $datas;
   			// echo $seDemandas;
     	}
     	catch (PDOException $e) {
    		print $e->getMessage ();
     	}
     }
     public function logs_CruceDeNegocios($logsDemandas)
     {
     	$connPDO= new Conexion();

     	try {
     		$stmt=$connPDO->prepare("SELECT cons_log,
 										 	tipo_log,
 										 	campo_g,
 										 	esta_ini_g,
 										 	esta_fin_g,
 										 	fecha_g,
 										 	hora_g,
 										 	usuario_g,
 										 	ip_g,
 										 	client_g,
 										 	campo_usu_g,
 										 	param_g 
									FROM    lg_ch_tb 
									WHERE tipo_log = 1 
  									AND client_g = :idDemanda 
									ORDER BY cons_log DESC ");
     		
     		$stmt->bindParam(":idDemanda",$logsDemandas["idDemanda"]);

     		$stmt->execute();
     		$data=array();
     		while ($row=$stmt->fetch()) {
     			$campo        = $row['campo_g'];
				
				
				$estado_ini   = $row['esta_ini_g'];
				$estado_fin   = $row['esta_fin_g'];
				$fecha        = $row['fecha_g'];
				$hora         = $row['hora_g'];
				$usuario      = $row['usuario_g'];
        		$ip_g         = $row['ip_g'];
        		$campo_usu_g  = $row['campo_usu_g'];
        		$param_g      = $row['param_g'];
        		$nomUsuario=ucwords(strtolower(getCampo('usuarios',
        			"where Id_Usuarios='$usuario'",
        			'concat(Nombres," ",apellidos)')));
        		$nomTabla=ucwords(strtolower(getCampo('parametros',
        			"WHERE id_param=17 and conse_param=1",
        			'desc_param')));
        		if($campo == "grupo")
        		{
        		$dataIni=getCampo('parametros',
        			"WHERE id_param=3 and conse_param='".$estado_ini."'",
        			'desc_param'); 
    			$dataFin=getCampo('parametros',
    				"WHERE id_param=3 and conse_param='".$estado_fin."'",
    				'desc_param');
        		}
        		elseif($campo =="IdPromotor")
        		{
        			$dataIni=getCampo('usuarios',
        			"where Id_Usuarios='$estado_ini'",
        			'concat(Nombres," ",apellidos)');

        			$dataFin=getCampo('usuarios',
        			"where Id_Usuarios='$estado_fin'",
        			'concat(Nombres," ",apellidos)');
        		}
        		elseif($campo == "estado_dem")
        		{
        			$dataIni = getCampo('parametros',
        				"WHERE id_param=16 and conse_param='".$estado_ini."'",
        				'desc_param'); 
    				$dataFin = getCampo('parametros',
    					"WHERE id_param=16 and conse_param='".$estado_fin."'",
    					'desc_param'); 
        		}
        		elseif($campo == "IdTipoInm")
        		{
        			$dataIni=getCampo('tipoinmuebles',
        				"WHERE idTipoInmueble='".$estado_ini."'"
        				,'CONCAT(UPPER(LEFT(Descripcion, 1)), LOWER(SUBSTRING(Descripcion, 2)))');
        			$dataFin=getCampo('tipoinmuebles',
        				"WHERE idTipoInmueble='".$estado_fin."'"
        				,'CONCAT(UPPER(LEFT(Descripcion, 1)), LOWER(SUBSTRING(Descripcion, 2)))');
        		}
        		elseif($campo == "IdDestinacion")
        		{
        			$dataIni=getCampo('destinacion',
        				"WHERE Iddestinacion='".$estado_ini."'"
        				,'CONCAT(UPPER(LEFT(Nombre, 1)), LOWER(SUBSTRING(Nombre, 2)))');
        			$dataFin=getCampo('destinacion',
        				"WHERE Iddestinacion='".$estado_fin."'"
        				,'CONCAT(UPPER(LEFT(Nombre, 1)), LOWER(SUBSTRING(Nombre, 2)))');
        		}
        		elseif($campo=='IdGestion')
        		{
        			$dataIni=getCampo('gestioncomer',
        				"WHERE IdGestion='".$estado_ini."'",
        				'CONCAT(UPPER(LEFT(NombresGestion, 1)), LOWER(SUBSTRING(NombresGestion, 2)))');
        			
        			$dataFin=getCampo('gestioncomer',"WHERE IdGestion='".$estado_fin."'"
        				,'CONCAT(UPPER(LEFT(NombresGestion, 1)), LOWER(SUBSTRING(NombresGestion, 2)))');
        		}
        		elseif($campo=='ValInicial' or $campo=='ValFinal'
        		or $campo=='AreaInicial' or $campo=='AreaFin')
        		{
        			$dataIni=number_format($estado_ini);
        			$dataFin=number_format($estado_fin);
        		}
        		elseif($campo=='barrio')
        		{
        			$dataIni=getCampo('barrios',"WHERE idBarrios='".$estado_ini."'",
        				'CONCAT(UPPER(LEFT(NombreB, 1)), LOWER(SUBSTRING(NombreB, 2)))');
        			$dataFin=getCampo('barrios',
        				"WHERE idBarrios='".$estado_fin."'",
        				'CONCAT(UPPER(LEFT(NombreB, 1)), LOWER(SUBSTRING(NombreB, 2)))');
        		}
        		else
        		{
        			$dataIni=$estado_ini;
					$dataFin=$estado_fin;
        		}
     			$data[]=array(
						"nomCapo"    =>$campo_usu_g,
						"estInicial" =>$dataIni,
						"estFinal"   =>$dataFin,
						"fecha"      =>$fecha."-".$hora,
						"ipuser"     =>$ip_g,
						"usuario"    =>$nomUsuario
     				);
     		}
     		return $data;
     		
     	} catch (PDOException $e) {
    		print $e->getMessage ();
     	}
     }
	 public function descartaDemandas($usr_dem,$inmu_dem,$tp_dem,$res_dem,$inmob_dem,$ip_dem,$hor_dem,$fec_dem,$c_dem,$observacion)
     {	
	 	///Danny
		
     	$connPDO= new Conexion();

     	try
     	{
     		
			$conse =consecutivo('conse_dem','descarta_demandas');
			$exito=0;//"$conse$usr_dem,$inmu_dem,$tp_dem,$res_dem,$inmob_dem,$ip_dem,$hor_dem,$fec_dem";
			$stmt=$connPDO->prepare("insert into descarta_demandas
			(conse_dem,usr_dem,inmu_dem,tp_dem,res_dem,
			inmob_dem,ip_dem,hor_dem,fec_dem,cod_dem,observa_descarta)
			values
			(:conse_dem,:usr_dem,:inmu_dem,:tp_dem,:res_dem,
			:inmob_dem,:ip_dem,:hor_dem,:fec_dem,:cod_dem,:observacion)");
			
   			if($stmt->execute(array(
						':conse_dem' 	=> $conse,
						':usr_dem'     	=> $usr_dem,    
						':inmu_dem'  	=> $inmu_dem, 
						':tp_dem'   	=> $tp_dem,  
						':res_dem'      => $res_dem,       
						':inmob_dem'    => $inmob_dem,         
						':ip_dem'       => $ip_dem,      
						':hor_dem'      => $hor_dem,      
						':fec_dem'      => $fec_dem,
						':cod_dem'		=> $c_dem,
						':observacion'  => $observacion
					)))
			{
				$exito=1;
			}
   			
   			return $exito;
   			// echo $seDemandas;
     	}
     	catch (PDOException $e) 
		{
			print $e->getMessage ();
     	}
     }
	 
//echo $codinmu."fdafdsf";

	public function datosDemandas($iddem)
     {
		 //Danny
		$connPDO= new Conexion();
		
		 $stmt = $connPDO->prepare("SELECT IdDemanda,IdPromotor,FechaLimite,Calle,
					AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
					IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
					Descripcion,Estado,IdBarrio,grupo_dem,CedulaIInt
        FROM demandainmuebles i
        WHERE i.IdDemanda = :ideman");
		$stmt->bindParam(':ideman',$iddem);
		$stmt->execute();
		//$stmt->debugDumpParams();
		$listnegocios=array();
		while($row=$stmt->fetch())
		{	
				
			$listnegocios[]=array(
								"IdDemanda"     	=> $row["IdDemanda"],
								"IdPromotor"  		=> $row["IdPromotor"],
								"Promotor"  		=> ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$row["IdPromotor"]."'",'concat(Nombres," ",apellidos)'))),
								"FechaLimite"      	=> $row["FechaLimite"],
								"Calle"  			=> $row["Calle"],
								"AlaCalle" 			=> $row["AlaCalle"],
								"Carrera"   		=> $row["Carrera"],
								"AlaCarrera"	    => $row["AlaCarrera"],
								"Gestion"	   		=> ucwords(strtolower(getCampo('gestioncomer',"where IdGestion='".$row["IdGestion"]."'",'NombresGestion'))),
								"IdGestion"	   		=> $row["IdGestion"],
								"Destinacion"	   	=> ucwords(strtolower(getCampo('destinacion',"where Iddestinacion='".$row["IdDestinacion"]."'",'Nombre'))),
								"IdDestinacion"	   	=> $row["IdDestinacion"],
								"TipoInm"	     	=> ucwords(strtolower(getCampo('tipoinmuebles',"where idTipoInmueble=".$row["IdTipoInm"],'Descripcion'))),
								"IdTipoInm"     	=> $row["IdTipoInm"],
								"ValInicial"     	=> $row["ValInicial"],
								"ValFinal"     		=> $row["ValFinal"],
								"AreaInicial"     	=> $row["AreaInicial"],
								"AreaFinal"     	=> $row["AreaFinal"],
								"Descripcion"     	=> $row["Descripcion"],
								"Estado"     		=> ucwords(strtolower(getCampo('parametros',"WHERE id_param=16 and conse_param='".$row["Estado"]."'",'desc_param'))),
								"Barrio"     		=> ucwords(strtolower(getCampo('barrios',"WHERE IdBarrios=".$row["IdBarrio"],'NombreB'))),
								"IdBarrio"     		=> $row["IdBarrio"],
								"grupo_dem"     	=> $row["grupo_dem"],
								"CedulaIInt"     	=> ucwords(strtolower(getCampo('clientes_inmobiliaria',"WHERE cedula='".$row["CedulaIInt"]."'",'nombre')))
								
							);
		}//print_r($listnegocios)."ffff";
		return $listnegocios;
     }
	 
	 public function aplicaDemandas($datos,$migrupo='',$inmoAct='')
     {
		 //Danny
		
		$connPDO= new Conexion();
		//$iddem =$_POST[];
		$Demandas= new Negocios();
		//echo $datos."---------------";
		$info=$Demandas->datosDemandas($datos);
		/*echo '<pre>';
		print_r($info);   
		echo '</pre>';*/
		foreach($info as $key => $data)
//echo "esto es carrera ".$data['Carrera']."fffff <br>";
		
		$cond="";
		if($data['AreaInicial'] && $data['AreaFinal'])
		{
			$cond .=" and i.AreaConstruida BETWEEN :AreaInicial AND :AreaFinal";
		}
		if($data['Calle'] && $data['AlaCalle'])
		{
			$cond .=" and i.Calle BETWEEN :Calle AND :AlaCalle";
		}
		if($data['Carrera'] && $data['AlaCarrera'])
		{	
			$cond .=" and i.Carrera BETWEEN :Carrera AND :AlaCarrera";
		}
		
		//echo $data['IdTipoInm']."------------<br>"; 
		if($data['IdTipoInm'])
		{
			$cond .=" and i.IdTpInm=:IdTpInm";
		}
		if($data['IdDestinacion'])
		{
			$cond .=" and i.IdDestinacion=:IdDestinacion";
		}
		/*if($data['IdPromotor'])
		{
			$cond .=" and i.IdPromotor=:IdPromotor";
		}*/
		
		if($data['IdGestion']==5)
		{
			$campo='ValorVenta';
		}
		else
		{
			$campo='ValorCanon';
		}
		if($data['ValInicial'] && $data['ValFinal'])
		{
			$cond .=" AND i.$campo BETWEEN :ValInicial AND :ValFinal";
		}
		
		if($data['IdBarrio'])
		{
			$cond .=" and i.IdBarrio=:IdBarrio";
		}
		//echo $data['IdGestion']."fffffffff";
	/*	if($data['IdGestion'])
	   {
		   if($data['IdGestion']!=2)
		   {
			$cond .=" and i.IdGestion=:IdGestion";
		   }
		   else
		   {
			   $cond .=" and i.IdGestion != 3";
		   }
	   }*/
		
		
		if($data['idInm'])
		{ //echo "ingresa a codDemanda ".$_GET['codDemanda'];
		$cond .=" and i.idInm=:idInm";
		}
		
		if($data['perfil']==3 or $data['perfil']==10)
		{
		
		}
		
		
		////condicion que verifica los grupos existentes y omite la condicion para inmobiliarias TAE
		if($inmoAct!=1 and $inmoAct!=1000 and $inmoAct!=10000 and $inmoAct!=1000000)
		{
			if(!empty($migrupo))
			{
			   $cond .=" AND i.IdInmobiliaria  IN ($migrupo)";
			}
										 
		}
		
		$hoy=date("Y-m-d");
		
		 $stmt = $connPDO->prepare("SELECT idInm,IdGestion,IdDestinacion,ValorVenta,ValorCanon,
		 			IdPromotor,IdTpInm,IdPromotor,linkvideo,IdInmobiliaria,
					Calle,Carrera,ValorCanon,ValorVenta,Estrato,
					AreaLote,AreaConstruida,descripcionlarga,EdadInmueble,Administracion,IdBarrio
					FROM inmuebles i 
					WHERE i.politica_comp!=''
					and i.idEstadoinmueble<=2
					$cond");
		
		if($data['AreaInicial'] && $data['AreaFinal'])
		{
			$stmt->bindParam(":AreaInicial",$data['AreaInicial']);
			$stmt->bindParam(":AreaFinal",$data['AreaFinal']);
		}
		
		if($data['Calle'] && $data['AlaCalle'])
		{
			$stmt->bindParam(":Calle",$data['Calle']);
			$stmt->bindParam(":AlaCalle",$data['AlaCalle']);
		}
		if($data['Carrera'] && $data['AlaCarrera'])
		{
			$stmt->bindParam(":Carrera",$data['Carrera']);
			$stmt->bindParam(":AlaCarrera",$data['AlaCarrera']);
		}
		if($data['IdDestinacion'])
		{
			$stmt->bindParam(':IdDestinacion',$data["IdDestinacion"]);
		}
		if($data['IdTipoInm'])
		{
			$stmt->bindParam(":IdTpInm",$data["IdTipoInm"]);
		}
		/*if($data['IdPromotor'])
		{
			$stmt->bindParam(":IdPromotor",$data["IdPromotor"]);
		}*/
		if($data['ValInicial'] && $data['ValFinal'])
		{
			$stmt->bindParam(":ValInicial",$data['ValInicial']);
			$stmt->bindParam(":ValFinal",$data['ValFinal']);
		}
		if($data['IdBarrio'])
		{ 
			$stmt->bindParam(":IdBarrio",$data['IdBarrio']);
		}
		if($data['idInm'])
		{ //echo "ingresa a codDemanda ".$_GET['codDemanda'];
			$stmt->bindParam(":idInm",$data["idInm"]);
		}
		if($data['IdGestion'])
	   {
		   if($data['IdGestion']!=2)
		   {
			$cond .=" and i.IdGestion=:IdGestion";
		   }
		   else
		   {
			   $cond .=" and i.IdGestion != 3";
		   }
	   }
		
		
		
		
		if($data['perfil']==3 or $data['perfil']==10)
		{
		
		}
		//echo $cond;
		//$idinmob=1;
		//$cond="hola condicion";
		//$stmt->bindParam(":migrupo",$migrupo);
		$stmt->execute();
		
		/*function pdo_debugStrParams($stmt) 
		{
		  ob_start();
		  $stmt->debugDumpParams();
		  $r = ob_get_contents();
		  ob_end_clean();
		  return $r;
		}

// omitted: connect to the database and prepare a statement
echo '<pre>'.htmlspecialchars(pdo_debugStrParams($stmt)).'</pre>';*/
		
		$listnegocios=array();
		$existe=$stmt->rowCount();
		//echo "<br>".$existe."<br>";
		while($row=$stmt->fetch())
		{	
			///boton para descartar demanda
			$descartarBtn=($row["RESULTADO"] == 1)?"":"<button class='btn btn-danger btn-sm dct' data-toggle='modal' data-target='#modal1-id' title='Descartar Demanda'><span class='glyphicon fa fa-times-circle'></span></button>";
			
			$negociarBtn=($row["RESULTADO"] == 0)?"":"<button class='btn btn-success btn-sm' 	><span class='glyphicon fa fa-check-square-o'></span></button>";
			$editarBtn="<button class='btn btn-warning btn-sm dct1' title='Buscar Demanda' data-toggle='modal' data-target='#modal2-id'><span class='fa fa-search'></span></button>";
			$cambiosBtn="<button class='btn btn-info btn-sm' toggle='modal' data-target='#modal-id'><span class='glyphicon glyphicon-eye-open'></span></button>";
			$seguimientoBtn="<button class='btn btn-danger btn-sm'><span class='fa fa-sign-in'></span></button>";
			// $ofertabtn=($this->seguiDemandas($row["IdDemanda"])==1)?"<button class='btn btn-success btn-sm'><span class='glyphicon glyphicon-ok'></span></button>":"";
			$dataModal=$descartarBtn.$negociarBtn.$editarBtn.$cambiosBtn.$seguimientoBtn;
			$tipoinm	= ucwords(strtolower(getCampo('tipoinmuebles',"where idTipoInmueble='".$row["IdTipoInm"],'Descripcion')));
			$barr		= ucwords(strtolower(getCampo('barrios',"WHERE ]IdBarrios=".$row["IdBarrio"],'NombreB')));
			$edad		= $row["EdadInmueble"];
			$Estrato	= $row["Estrato"];
			$Canon		= "$".number_format($row["ValorCanon"]);
			$Ventas		= "$".number_format($row["ValorVenta"]);
			$Admon		= "$".number_format($row["Administracion"]);
			$idInm		= $row["idInm"];
			$existe		= $Demandas->validar_acepta($idInm);
			$gestion	= ucwords(strtolower(getCampo('gestioncomer',"where IdGestion='".$row["IdGestion"]."'",'NombresGestion')));
			$logoAA		= "<img style='max-width:80px; height:auto;' src='http://www.simiinmobiliarias.com/logos/".str_replace('../logos/','',getCampo('clientessimi',"where IdInmobiliaria=".$row["IdInmobiliaria"],'logo'))."' alt=Inmobiliaria'  border='0' />";
			$botonAcepta="";
			
			if($existe>0)
			{
				$botonAcepta="<img style='max-width:80px; height:auto;' src='http://www.simiinmobiliarias.com/mcomercialweb/images/cruce de manos.jpg' alt=Negocios'  border='0' />";
			}
			$listaCaracteristicas="<ul>
									<li>Gestión: $gestion</li>
									<li>Tipo: $tipoinm</li>
									<li>Barrio: $barr</li>
									<li>Edad: $edad</li>
									<li>Estrato: $Estrato</li>
									<li>Canon: $Canon</li>
									<li>Venta: $Ventas</li>
									<li>Admon: $Admon</li>
								   </ul>";
			$ficha	= "<button class='btn btn-info btn-sm' onclick='abrirFicha($idInm)' ><span class='glyphicon glyphicon-eye-open'></span></button>";
			
			//$dem=$Demandas->buscarDemandas1($row["idInm"]);
			//echo $dem;
				
			$listnegocios[]=array(
					"idInm"     		=> $row["idInm"],
					"IdPromotor"  		=> ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$row["IdPromotor"]."'",'concat(Nombres," ",apellidos'))),
					"Calle"  			=> $row["Calle"],
					"Carrera"   		=> $row["Carrera"],
					"linkvideo"	    	=> $row["linkvideo"],
					"inmobiliaria" 		=> ucwords(strtolower(getCampo('clientessimi',"where IdInmobiliaria='".$row["IdInmobiliaria"]."'",'Nombre'))),
					"logo"	   			=> $logoAA,
					"IdDestinacion"	   	=> ucwords(strtolower(getCampo('destinacion',"where Iddestinacion='".$row["IdDestinacion"]."'",'Nombre'))),
					"listaC"	     	=> $listaCaracteristicas,
					"AreaConstruida"    => number_format($row["AreaConstruida"]),
					"AreaLote"     		=> number_format($row["AreaLote"]),
					"Descripcion"     	=> $row["descripcionlarga"],
					"botonFicha"		=> $ficha,
					"botonAcepta"		=> $botonAcepta
					);
		}//print_r($listnegocios)."ffff";
		return $listnegocios;
     }
	 function validar_acepta($idInm)
	{
		$connPDO= new Conexion();
		 $stmt = $connPDO->prepare("select id_inmu_cm
			from condiciones_compartir_inm
			where id_inmu_cm=:idInm");
		
		$stmt->bindParam(':idInm',$idInm);
		$stmt->execute();
		$existe=$stmt->rowCount();
		
		return $existe;
	}
	function buscarDemandas1($codinmu,$migrupo='')
	{
	   //include('../mcomercialweb/grupoinmobiliaria.class.php');
	   //include('../mcomercialweb/emailtae2.class.php');
	   //echo "$migrupo esto es mi grupo";
		$f_actual=date('Y-m-d');
		$Demandas= new Negocios();
		if($control==1)
		{
			global $f_actual,$w_conexion;
		}
		else
		{
			$w_conexion = new MySQL();
			global $f_actual;
		}
		
		/*$grupoinmo= new GrupoInmobiliario;
		$grupoinmo->MiGrupo();
		$migrupo=$grupoinmo->InmMiGrupo();
		$otrosgrupos=$grupoinmo->InmOtrosGrupos();	*/		  
		list($aa,$codinm2)=explode("-",$codinmu);			  
		$condicionInmob1="";
		if($_SESSION['IdInmmo']!=1 and $_SESSION['IdInmmo']!=1000 and $_SESSION['IdInmmo']!=10000 and $_SESSION['IdInmmo']!=1000000)   
		{
			if(!empty($migrupo))
			{
			   $condicionInmob1  .=" AND d.IdInmobiliaria  IN ($migrupo)";
			}
			else if(!empty($otrosgrupos))
			{
			   $condicionInmob1  .=" AND d.IdInmobiliaria  NOT IN ($otrosgrupos)";
			}	
			else
			{
			   $condicionInmob1  .=" AND d.IdInmobiliaria  IN ($aa)";
			}						 
		}
	  /*  echo $migrupo;
		echo "----------- ".$otrosgrupos;*/
	   $condSimi="and d.IdInmobiliaria not in (1,1000,1000000)";
	   if($_SESSION['IdInmmo']==1)
	   {
		   $condSimi='';
	   }
	   
	   
	   
	   $validaGlobal=0;
	   $flag=0;
	   $varTexto="";
	   
		$aplicademandas = "SELECT *	
			FROM demandainmuebles d
			WHERE  d.Estado=0
			and FechaLimite>='$f_actual'
			#and IdDemanda=605
			$condSimi
			$condicionInmob1";
		
	   //echo $aplicademandas."<hr>";
	   $res3=$w_conexion->ResultSet($aplicademandas);
	   $existe3=$w_conexion->FilasAfectadas($res3);
	   //echo $existe3." existe";
	  
	   if ($existe3>0)
	   {
		   $varTexto .="<ul>";
		 while ($fila77=$w_conexion->FilaSiguienteArray($res3))
		 {		
				
			$IdDemanda		=$fila77['IdDemanda'];
			$IdGestion		= $fila77['IdGestion'];
			$IdTpInm		= $fila77['IdTipoInm'];
			$IdBarrio		= $fila77['IdBarrio'];
			$ValorVenta		= $fila77['ValorVenta'];
			$ValorCanon		= $fila77['ValorCanon'];	
			$Administracion	= $fila77['Administracion'];	
			$Estrato		= $fila77['Estrato'];
			$politica_comp	= $fila77['politica_comp'];
			$IdDestinacion	= $fila77['IdDestinacion'];
			$AreaConstruida	= $fila77['AreaConstruida'];
			$AreaLote		= $fila77['AreaLote'];
			$Carrera		= $fila77['Carrera'];
			$Calle			= $fila77['Calle'];
			$AlaCalle		= $fila77['AlaCalle'];	
			$AlaCarrera		= $fila77['AlaCarrera'];
			$ValInicial		= $fila77['ValInicial'];
			$ValFinal		= $fila77['ValFinal'];
			$AreaInicial	= $fila77['AreaInicial'];
			$AreaFinal		= $fila77['AreaFinal'];
			$IdCiudad		= $fila77['IdCiudad'];
			$IdBarrio		= $fila77['IdBarrio'];
			$IdInmobiliaria	= $fila77['IdInmobiliaria'];
					  
			 // echo "$IdDemanda $IdCiudad <br>";
			if($IdGestion==2)
			{
			  $condG="1,2,5";
			}
			else
			{
			  $condG="$IdGestion,2";
			}
			if($IdTpInm==1 or $IdTpInm==11)
			{
			  $tpInm="1,11";
			}
			else
			{
				$tpInm=$IdTpInm;
			}
			//echo "$IdDemanda --- $tpInm=$IdTpInm <br>";
			//echo "$IdCiudad $IdDemanda  **  $tpInm,$condG,$Calle,$AlaCalle,$Carrera,$AlaCarrera,$IdDestinacion,$ValorVenta,$ValorCanon | $IdBarrio |,$AreaInicial,$AreaFinal,1,<br><br>";
			
			
			$conTodos=$Demandas->aplicaDemanda($codinmu,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$AlaCalle,$Carrera,$AlaCarrera,$IdDestinacion,$ValInicial,$ValFinal,$IdBarrio,$AreaInicial,$AreaFinal,1,'',0);
			//echo $conTodos."<br>------------------------------<br>";
			$conBarrio=$Demandas->aplicaDemanda($codinmu,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$AlaCalle,$Carrera,$AlaCarrera,$IdDestinacion,$ValInicial,$ValFinal,$IdBarrio,$AreaInicial,$AreaFinal,2,$conTodos);
			//echo $conBarrio." barr<br>------------------------------<br>";
			$conArea=$Demandas->aplicaDemanda($codinmu,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$AlaCalle,$Carrera,$AlaCarrera,$IdDestinacion,$ValInicial,$ValFinal,$IdBarrio,$AreaInicial,$AreaFinal,3,$conTodos);
			//echo $conArea." area<br>------------------------------<br>";
			$conDestinacion=$Demandas->aplicaDemanda($codinmu,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$AlaCalle,$Carrera,$AlaCarrera,$IdDestinacion,$ValInicial,$ValFinal,$IdBarrio,$AreaInicial,$AreaFinal,4,$conTodos,10);
			$conDir=$Demandas->aplicaDemanda($codinmu,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$AlaCalle,$Carrera,$AlaCarrera,$IdDestinacion,$ValInicial,$ValFinal,$IdBarrio,$AreaInicial,$AreaFinal,5,$conTodos);
			//echo $conDestinacion." dest<br>------------------------------<br>";
			if($conTodos==1)
			{
				//echo "<br>La demanda $IdDemanda cumple al 100% <br>";
				$varTexto .="<li>La demanda $IdDemanda cumple al 100% </li>";
				$flag=1;
			}
			$ciudOr=getCampo('barrios b,inmuebles i',"where b.IdBarrios=i.IdBarrio and i.idInm='".$codinmu."'",'b.IdCiudad',0);
			//echo "ciudOr $ciudOr <br>";
			//echo "$IdDemanda $conTodos==todos and $conBarrio==barrio and $conArea==area and $conDestinacion==destinacion $conDir==dir <br>";
			if($conTodos==0 and $conBarrio==0 and $conArea==0 and $conDestinacion==0 and $IdCiudad==0 and $conDir==0){ //echo "$IdDemanda no coincide";
			
			}
			
			if($conTodos==0 and ($conBarrio==1 or $conArea==1 or $conDestinacion==1 or $conDir==1))// or $IdCiudad==$ciudOr))
			{ 
				if($IdCiudad==$ciudOr){ $conCiud=1;}
				$var1=""; 
				$var2=""; 
				$var3="";
				$var4="";
				$validaGlobal=$conBarrio+$conArea+$conDestinacion+$conCiud+$conDir; //echo "<br>La demanda $IdDemanda ";
				$varTexto .="<li>La demanda $IdDemanda ";
				if($conBarrio==1){ $var1=" Barrio, ";} else { $var1="";}
				if($conArea==1){ $var2=" Area, ";} else { $var2="";}
				if($conDestinacion==1){ $var3=" Destinacion, ";} else { $var3="";}
				if($IdCiudad>0){ $var4=" Ciudad, ";} else { $var4="";}
				if($conDir==1){ $var5=" Direccion, ";} else { $var3="";}
				//echo "<table class='TablaDatos' width='80%' align='center'><tr><td class='tituloFormulario'>Demandas Coincidentes</td></tr> <tr><td>";
				if($validaGlobal==1) { //echo "Coincide en Valor, Tipo de Inmueble, Gestion,  $var1 $var2 $var3 $var4 $var5 Cumple al 44.4%<br>"; 			
				$varTexto = $varTexto."Coincide en Tipo de Inmueble, Gestion,  $var1 $var2 $var3 $var4 $var5 Cumple al 44.4%</li>"; $flag++;}
					
				if($validaGlobal==2) { //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 55.5%<br>"; 			
				$varTexto = $varTexto."Coincide en Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 55.5%</li>"; $flag++;}
				
				if($validaGlobal==3) { //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 66.6%<br>"; 			
				$varTexto = $varTexto."Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 66.6%</li>"; $flag++;}
				
				if($validaGlobal==4) { //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 77.7%<br>"; 			
				$varTexto = $varTexto."Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 77.7%</li>"; $flag++;}
				
				if($validaGlobal==5) { //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89%<br>"; 			
				$varTexto = $varTexto."Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89%</li>"; $flag++;}
				//echo "</td></tr></table>";
				//$flag=1;
				
			}
			// echo "Entra a alguna $IdDemanda  - $flag";
		}//while
		//echo $flag."flag";
		$varTexto .="</ul>";
		if($flag>0)
		{  //echo "---".$varTexto."---";
			 //$envioemail = new EmailTae2();
			 //$envioemail->EnvioEmailDemandaL($varTexto,$codinmu);
			 //$enviado= $envioemail->ResultadoCorreoDemadaLocalizada();
			 //echo $varTexto."--------------";
			 return $varTexto;
		}
	  }
	  else
	  {
		  //echo "No Hay demandas coincidentes";
	  }
	}
	
	
		function aplicaDemanda($codinmu,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$AlaCalle,$Carrera,$AlaCarrera,$IdDestinacion,$ValInicial,$ValFinal,$IdBarrio,$AreaInicial,$AreaFinal,$evaluar,$completos,$mostrar='')
		{	$w_conexion = new MySQL();
			global $f_actual;
			$condicionInmob="";
			$cond2="";
			
			
			 if($_SESSION['IdInmmo']==1 or $_SESSION['IdInmmo']==1000 or $_SESSION['IdInmmo']==10000 or $_SESSION['IdInmmo']==1000000)
			{
				$condicionInmob="";
			}
			else
			{
				$condicionInmob="and i.idInmobiliaria not in (1,1000,10000,100000)";
			}
			
		   
			if($evaluar==1)
			{
				if($Calle>0) { $cond2 .=" AND i.Calle between '$Calle' AND '$AlaCalle'";}
				if($Carrera>0){ $cond2  .=" AND i.Carrera between '$Carrera' AND '$AlaCarrera'";}
				if($IdDestinacion>0){$cond2  .=" AND  i.IdDestinacion = '$IdDestinacion'";}
				if($ValorCanon>0)
				{
					if($IdGestion==1 or $IdGestion==2){  $cond2  .=" AND i.ValorCanon between '$ValInicial' AND '$ValFinal'"; } 
				}
				if($ValorVenta>0)
				{
					if($IdGestion==5) { $cond2 .=" AND i.ValorVenta '$ValInicial' AND '$ValFinal'"; } 
				}
				if(!empty($AreaConstruida)) {  $cond2  .=" AND i.AreaConstruida between '$AreaInicial' AND '$AreaFinal'"; } 
				//if(!empty($IdCiudad)){$aplicademandas1=$aplicademandas1." AND b.IdCiudad='".$IdCiudai."'";} 
				if(!empty($IdBarrio)) { $cond2  .=" AND i.IdBarrio='$IdBarrio'"; }
			}
			if($evaluar==2)
			{
				if($IdBarrio>0) { $cond2  .=" AND i.IdBarrio='$IdBarrio'"; }
				//echo "$ValorCanon -- $IdGestion <br>";
				if($ValorCanon>0)
				{
					if($IdGestion==1 or $IdGestion==2){ $cond2  .=" AND i.ValorCanon between '$ValInicial' AND '$ValFinal'"; } 
				}
				if($ValorVenta>0)
				{
					if($IdGestion==5) { $cond2 .=" AND i.ValorVenta '$ValInicial' AND '$ValFinal'"; } 
				}
			}
			if($evaluar==3)
			{
				if($AreaConstruida>0) { $cond2  .=" AND i.AreaConstruida between '$AreaInicial' AND '$AreaFinal'"; }
				if($ValorCanon>0)
				{
					if($IdGestion==1 or $IdGestion==2){  $cond2  .=" AND i.ValorCanon between '$ValInicial' AND '$ValFinal'"; } 
				}
				if($ValorVenta>0)
				{
					if($IdGestion==5) { $cond2 .=" AND i.ValorVenta '$ValInicial' AND '$ValFinal'"; } 
				}
			}
			if($evaluar==4)
			{
				if($IdDestinacion>0){$cond2  .=" AND  i.IdDestinacion = '$IdDestinacion'";}
				if($ValorCanon>0)
				{
					if($IdGestion==1 or $IdGestion==2){  $cond2  .=" AND i.ValorCanon between '$ValInicial' AND '$ValFinal'"; } 
				}
				if($ValorVenta>0)
				{
					if($IdGestion==5) { $cond2 .=" AND i.ValorVenta '$ValInicial' AND '$ValFinal'"; } 
				}
			}
			if($evaluar==5)
			{
				if($Calle>0) { $cond2 .=" AND i.Calle between '$Calle' AND '$AlaCalle'";}
				if($Carrera>0){ $cond2  .=" AND i.Carrera between '$Carrera' AND '$AlaCarrera'";}
				if($IdDestinacion>0){$cond2  .=" AND  i.IdDestinacion = '$IdDestinacion'";}
				if($ValorCanon>0)
				{
					if($IdGestion==1 or $IdGestion==2){  $cond2  .=" AND i.ValorCanon between '$ValInicial' AND '$ValFinal'"; } 
				}
				if($ValorVenta>0)
				{
					if($IdGestion==5) { $cond2 .=" AND i.ValorVenta '$ValInicial' AND '$ValFinal'"; } 
				}
			}
			if($IdCiudad>0)
			{
				 $cond2 .=" AND b.IdCiudad ='$IdCiudad'";  
			}
			
			
			$aplicademandas1 ="SELECT *	
				FROM inmuebles i,barrios b
				WHERE i.IdTpInm in ($tpInm)
				AND i.IdGestion in ($condG)
				and i.idInm='$codinmu'
				AND i.IdBarrio=b.IdBarrios
				$cond2
				$condicionInmob";
				//echo $aplicademandas1."<br><br>";
			if($mostrar==1)
			{
				echo $aplicademandas1."<br><br>";
			}
			$res=$w_conexion->ResultSet($aplicademandas1);
			{
				$existe=$w_conexion->FilasAfectadas($res);
				//echo $existe." existe";
				if ($existe>0)
				{
				   /* while($ff=$w_conexion->FilaSiguienteArray($res))
					{
						//$idDem=  $ff['idInm'];
						//$idDemT= $idDem.",".$idDemT;
					}*/
					//$idDemT=substr($idDemT,0,-1);
					return $existe;
				}
			}
		}
        ////////////
        
        
        
        
        
        
        
        
    function buscarInmuebles($coddem,$aa,$migrupo='')
	{
	   //include('../mcomercialweb/grupoinmobiliaria.class.php');
	   //include('../mcomercialweb/emailtae2.class.php');
	   //echo "$migrupo esto es mi grupo";
		$f_actual=date('Y-m-d');
		$Demandas= new Negocios();
		if($control==1)
		{
			global $f_actual,$w_conexion;
		}
		else
		{
			$w_conexion = new MySQL();
			global $f_actual;
		}
		
		/*$grupoinmo= new GrupoInmobiliario;
		$grupoinmo->MiGrupo();
		$migrupo=$grupoinmo->InmMiGrupo();
		$otrosgrupos=$grupoinmo->InmOtrosGrupos();	*/		  
		//list($aa,$codinm2)=explode("-",$codinmu);	
        		  
		$condicionInmob1="";
		if($_SESSION['IdInmmo']!=1 and $_SESSION['IdInmmo']!=1000 and $_SESSION['IdInmmo']!=10000 and $_SESSION['IdInmmo']!=1000000)   
		{
			if(!empty($migrupo))
			{
			   $condicionInmob1  .=" AND d.IdInmobiliaria  IN ($migrupo)";
			}
			else if(!empty($otrosgrupos))
			{
			   $condicionInmob1  .=" AND d.IdInmobiliaria  NOT IN ($otrosgrupos)";
			}	
			else
			{
			   $condicionInmob1  .=" AND d.IdInmobiliaria  IN ($aa)";
			}						 
		}
	  /*  echo $migrupo;
		echo "----------- ".$otrosgrupos;*/
	   $condSimi="and d.IdInmobiliaria not in (1,1000,1000000)";
	   if($_SESSION['IdInmmo']==1)
	   {
		   $condSimi='';
	   }
	   
	   
	   
	   $validaGlobal=0;
	   $flag=0;
	   $varTexto="";
	   
		$aplicademandas = "SELECT idInm,IdGestion,IdTpInm,IdBarrio,ValorVenta,
            ValorCanon,Estrato,IdDestinacion,AreaConstruida,AreaLote,
            Carrera,Calle,IdInmobiliaria	
			FROM inmuebles d
			WHERE  idEstadoinmueble<=2
            and politica_comp!=''
			$condSimi
			$condicionInmob1";
		
	   //echo $aplicademandas."<hr>";
	   $res3=$w_conexion->ResultSet($aplicademandas);
	   $existe3=$w_conexion->FilasAfectadas($res3);
	   //echo $existe3." existe";
	  $IdGestion=0;
	   if ($existe3>0)
	   {
		   $varTexto .="<ul>";
		 while ($fila77=$w_conexion->FilaSiguienteArray($res3))
		 {		
				
			$idInm   		= $fila77['idInm'];
			$IdGestion		= $fila77['IdGestion'];
			$IdTpInm	    = $fila77['IdTpInm'];
			$IdBarrio		= $fila77['IdBarrio'];
			$ValorVenta		= $fila77['ValorVenta'];
			$ValorCanon		= $fila77['ValorCanon'];	
			$Administracion	= $fila77['Administracion'];	
			$Estrato		= $fila77['Estrato'];
			$politica_comp	= $fila77['politica_comp'];
			$IdDestinacion	= $fila77['IdDestinacion'];
			$AreaConstruida	= $fila77['AreaConstruida'];
			$AreaLote		= $fila77['AreaLote'];
			$Carrera		= $fila77['Carrera'];
			$Calle			= $fila77['Calle'];
			$IdInmobiliaria	= $fila77['IdInmobiliaria'];
            $IdCiudad       = getCampo('barrios',"where IdBarrios=$IdBarrio",'IdCiudad');
					  
			 // echo "$IdDemanda $IdCiudad <br>";
			if($IdGestion==2)
			{
			  $condG="1,2,5";
			}
            elseif($IdGestion<=0)
			{
			  $condG="0,2";
			}
			else
			{
			  $condG="$IdGestion,2";
			}
           // echo "$idInm IdGestion $IdGestion --- $condG <br>";
			if($IdTpInm==1 or $IdTpInm==11)
			{
			  $tpInm="1,11";
			}
			else
			{
				$tpInm=$IdTpInm;
			}
			//echo "$IdDemanda --- $tpInm=$IdTpInm <br>";
			//echo "$IdCiudad $IdDemanda  **  $tpInm,$condG,$Calle,$AlaCalle,$Carrera,$AlaCarrera,$IdDestinacion,$ValorVenta,$ValorCanon | $IdBarrio |,$AreaInicial,$AreaFinal,1,<br><br>";
			$descartarBtn="<button class='btn btn-xs btn-danger btn-sm dct' data-toggle='modal' data-target='#modal1-id' title='Descartar Inmueble'><span class='glyphicon fa fa-times-circle'></span></button> ";
			
			$conTodos=$Demandas->aplicaInmueble($coddem,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$Carrera,$IdDestinacion,$ValInicial,$IdBarrio,$AreaInicial,1,'',0);
			//echo $conTodos."<br>------------------------------<br>";
			$conBarrio=$Demandas->aplicaInmueble($coddem,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$Carrera,$IdDestinacion,$ValInicial,$IdBarrio,$AreaInicial,2,$conTodos);
			//echo $conBarrio." barr<br>------------------------------<br>";
			$conArea=$Demandas->aplicaInmueble($coddem,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$Carrera,$IdDestinacion,$ValInicial,$IdBarrio,$AreaInicial,3,$conTodos);
			//echo $conArea." area<br>------------------------------<br>";
			$conDestinacion=$Demandas->aplicaInmueble($coddem,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$Carrera,$IdDestinacion,$ValInicial,$IdBarrio,$AreaInicial,4,$conTodos,10);
			$conDir=$Demandas->aplicaInmueble($coddem,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$Carrera,$IdDestinacion,$ValInicial,$IdBarrio,$AreaInicial,5,$conTodos);
			//echo $conDestinacion." dest<br>------------------------------<br>";
			if($conTodos==1)
			{
				//echo "<br>La demanda $IdDemanda cumple al 100% <br>";
				$varTexto .="<li><button title='Aplicar al Inmueble' type='button' id='aplicarInm' class='btn btn-xs btn-primary aplicarInm'  data-toggle='modal'  data-target='.mymodales1'><span class='fa fa-check-square-o'></span></button>".$descartarBtn. " El Inmueble $idInm cumple al 100% <input type='hidden' class='c_demd' value='".$idInm."' ><input type='hidden'  class='c_inmu' value='".$coddem."' > <i class='fa fa-star' title='Calificacion de la Oportunidad' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
				$flag=1;
			}
			$ciudOr=getCampo('demandainmuebles',"where IdBarrio and IdDemanda='".$coddem."'",'IdCiudad',0);
			//echo "ciudOr $ciudOr <br>";
			//echo "$IdDemanda $conTodos==todos and $conBarrio==barrio and $conArea==area and $conDestinacion==destinacion $conDir==dir <br>";
			if($conTodos==0 and $conBarrio==0 and $conArea==0 and $conDestinacion==0 and $IdCiudad==0 and $conDir==0){ //echo "$IdDemanda no coincide";
			
			}
			
			if($conTodos==0 and ($conBarrio==1 or $conArea==1 or $conDestinacion==1 or $conDir==1))// or $IdCiudad==$ciudOr))
			{ 
				if($IdCiudad==$ciudOr){ $conCiud=1;}
				$var1=""; 
				$var2=""; 
				$var3="";
				$var4="";
				$validaGlobal=$conBarrio+$conArea+$conDestinacion+$conCiud+$conDir; //echo "<br>La demanda $IdDemanda ";
				$varTexto .='<li> <button title="Aplicar al Inmueble" type="button" id="aplicarInm" class="btn btn-xs btn-primary aplicarInm"  data-toggle="modal"  data-target=".mymodales1"><span class="fa fa-check-square-o"></span></button>'.$descartarBtn. 'La demanda '.$idInm.'<input type="hidden" class="c_demd" value="'.$idInm.'" > <input type="hidden"  class="c_inmu" value="'.$coddem.'" >';
				if($conBarrio==1){ $var1=" Barrio, ";} else { $var1="";}
				if($conArea==1){ $var2=" Area, ";} else { $var2="";}
				if($conDestinacion==1){ $var3=" Destinacion, ";} else { $var3="";}
				if($IdCiudad>0){ $var4=" Ciudad, ";} else { $var4="";}
				if($conDir==1){ $var5=" Direccion, ";} else { $var3="";}
				//echo "<table class='TablaDatos' width='80%' align='center'><tr><td class='tituloFormulario'>Demandas Coincidentes</td></tr> <tr><td>";
				if($validaGlobal==1) { //echo "Coincide en Valor, Tipo de Inmueble, Gestion,  $var1 $var2 $var3 $var4 $var5 Cumple al 44.4%<br>"; 			
				$varTexto = $varTexto."Coincide en Tipo de Inmueble, Gestion,  $var1 $var2 $var3 $var4 $var5 Cumple al 44.4% <i class='fa fa-star' style='color:#ec6459'></i></li>"; $flag++;}
					
				if($validaGlobal==2) { //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 55.5%<br>"; 			
				$varTexto = $varTexto."Coincide en Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 55.5% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>"; $flag++;}
				
				if($validaGlobal==3) { //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 66.6%<br>"; 			
				$varTexto = $varTexto."Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 66.6% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>"; $flag++;}
				
				if($validaGlobal==4) { //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 77.7%<br>"; 			
				$varTexto = $varTexto."Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 77.7% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>"; $flag++;}
				
				if($validaGlobal==5) { //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89%<br>"; 			
				$varTexto = $varTexto."Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>"; $flag++;}
				//echo "</td></tr></table>";
				//$flag=1;
				
			}
			// echo "Entra a alguna $IdDemanda  - $flag";
		}//while
		//echo $flag."flag";
		$varTexto .="</ul>";
		if($flag>0)
		{  //echo "---".$varTexto."---";
			 //$envioemail = new EmailTae2();
			 //$envioemail->EnvioEmailDemandaL($varTexto,$codinmu);
			 //$enviado= $envioemail->ResultadoCorreoDemadaLocalizada();
			 //echo $varTexto."--------------";
			 return $varTexto;
		}
	  }
	  else
	  {
		  //echo "No Hay demandas coincidentes";
	  }
	}

	
		function aplicaInmueble($codinmu,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$Carrera,$IdDestinacion,$ValInicial,$IdBarrio,$AreaInicial,$evaluar,$completos,$mostrar='')
		{	$w_conexion = new MySQL();
			global $f_actual;
			$condicionInmob="";
			$cond2="";
			
			
			 if($_SESSION['IdInmmo']==1 or $_SESSION['IdInmmo']==1000 or $_SESSION['IdInmmo']==10000 or $_SESSION['IdInmmo']==1000000)
			{
				$condicionInmob="";
			}
			else
			{
				$condicionInmob="and i.idInmobiliaria not in (1,1000,10000,100000)";
			}
			
		   
			if($evaluar==1)
			{
				if($Calle>0) { $cond2 .=" AND i.Calle >= '$Calle' AND AlaCalle <='$Calle' ";}
				if($Carrera>0){ $cond2  .=" AND i.Carrera >= '$Carrera' AND AlaCarrera<='$Carrera'";}
				if($IdDestinacion>0){$cond2  .=" AND  i.IdDestinacion = '$IdDestinacion'";}
				if($ValorCanon>0)
				{
					if($IdGestion==1 or $IdGestion==2){  $cond2  .=" AND i.ValInicial>= '$ValInicial' AND ValFinal<='$ValInicial'"; } 
				}
				if($ValorVenta>0)
				{
					if($IdGestion==5) { $cond2 .=" AND i.ValInicial>= '$ValInicial' AND ValFinal<='$ValInicial'"; } 
				}
				if(!empty($AreaConstruida)) {  $cond2  .=" AND i.AreaInicial >= '$AreaInicial' AND AreaFinal <='$AreaInicial'"; } 
				//if(!empty($IdCiudad)){$aplicademandas1=$aplicademandas1." AND b.IdCiudad='".$IdCiudai."'";} 
				if(!empty($IdBarrio)) { $cond2  .=" AND i.IdBarrio='$IdBarrio'"; }
			}
			if($evaluar==2)
			{
				if($IdBarrio>0) { $cond2  .=" AND i.IdBarrio='$IdBarrio'"; }
				//echo "$ValorCanon -- $IdGestion <br>";
				if($ValorCanon>0)
				{
					if($IdGestion==1 or $IdGestion==2){ $cond2  .=" AND i.ValInicial>= '$ValInicial' AND ValFinal<='$ValInicial'"; } 
				}
				if($ValorVenta>0)
				{
					if($IdGestion==5) { $cond2 .=" AND i.ValInicial>= '$ValInicial' AND ValFinal<='$ValInicial'"; } 
				}
			}
			if($evaluar==3)
			{
				if($AreaConstruida>0) { $cond2  .=" AND i.AreaInicial >= '$AreaInicial' AND AreaFinal <='$AreaInicial'"; }
				if($ValorCanon>0)
				{
					if($IdGestion==1 or $IdGestion==2){  $cond2  .=" AND i.ValInicial>= '$ValInicial' AND ValFinal<='$ValInicial'"; } 
				}
				if($ValorVenta>0)
				{
					if($IdGestion==5) { $cond2 .=" AND i.ValInicial>= '$ValInicial' AND ValFinal<='$ValInicial'"; } 
				}
			}
			if($evaluar==4)
			{
				if($IdDestinacion>0){$cond2  .=" AND  i.IdDestinacion = '$IdDestinacion'";}
				if($ValorCanon>0)
				{
					if($IdGestion==1 or $IdGestion==2){  $cond2  .=" AND i.ValInicial>= '$ValInicial' AND ValFinal<='$ValInicial'"; } 
				}
				if($ValorVenta>0)
				{
					if($IdGestion==5) { $cond2 .=" AND i.ValInicial>= '$ValInicial' AND ValFinal<='$ValInicial'"; } 
				}
			}
			if($evaluar==5)
			{
				if($Calle>0) { $cond2 .=" AND i.Calle >= '$Calle' AND AlaCalle <='$Calle'";}
				if($Carrera>0){ $cond2  .=" AND i.Carrera >= '$Carrera' AND AlaCarrera<='$Carrera'";}
				if($IdDestinacion>0){$cond2  .=" AND  i.IdDestinacion = '$IdDestinacion'";}
				if($ValorCanon>0)
				{
					if($IdGestion==1 or $IdGestion==2){  $cond2  .=" AND i.ValInicial>= '$ValInicial' AND ValFinal<='$ValInicial'"; } 
				}
				if($ValorVenta>0)
				{
					if($IdGestion==5) { $cond2 .=" AND i.ValInicial>= '$ValInicial' AND ValFinal<='$ValInicial'"; } 
				}
			}
			if($IdCiudad>0)
			{
				 $cond2 .=" AND i.IdCiudad ='$IdCiudad'";  
			}
			
			
			$aplicainmuebles1 ="SELECT *	
				FROM demandainmuebles i
				WHERE i.IdTipoInm in ($tpInm)
				AND i.IdGestion in ($condG)
				and i.IdDemanda='$codinmu'
				$cond2
				$condicionInmob";
				//echo $aplicademandas1."<br><br>";
			if($mostrar==1)
			{
				echo $aplicainmuebles1."<br><br>";
			}
			$res=$w_conexion->ResultSet($aplicainmuebles1) or die("error evaluar inm <br>".mysql_error());
			{
				$existe=$w_conexion->FilasAfectadas($res);
				//echo $existe." existe";
				if ($existe>0)
				{
				   /* while($ff=$w_conexion->FilaSiguienteArray($res))
					{
						//$idDem=  $ff['idInm'];
						//$idDemT= $idDem.",".$idDemT;
					}*/
					//$idDemT=substr($idDemT,0,-1);
					return $existe;
				}
			}
		}
     public function creaSeguimientoOferta($inmob,$data)
	{
 		$w_conexion = new MySQL(); 
		include('../mcomercialweb/PHPMailer_5.2.2/class.phpmailer.php');
        include('../mcomercialweb/PHPMailer_5.2.2/class.smtp.php');
        include('../mcomercialweb/emailtae2.class.php');
        
        $IdDemanda          = $data['idDemanda'];
        $tseguimiento       = $data['tseguimiento'];
        $descripcion        = $data['descripcion'];
        $frec               = $data['frec'];
        $horarec            = $data['hora_ini'];
        $agendar            = $data['agendar'];
        $agendarc            = $data['agendarc'];
        $mailcop            = $data['mailcop'];
        $resseg             = $data['resseg'];
        
	    $conse=consecutivo('idSeguimiento','SeguimientoSolicitudNvo');
		$sql="INSERT INTO SeguimientoSolicitudNvo
			(idSeguimiento,idDemanda,tseguimiento,observacion,
			frec,horarec,agendar,agendarc,mailcop,resseg)
			VALUES
			('$conse','$IdDemanda','$tseguimiento','$descripcion',
			'$frec','$horarec','$agendar','$agendarc','$mailcop','$resseg')";
			$res=$w_conexion->RecordSet($sql);
			if($res)
			{
				//echo "res $sql";
				//echo $idDemanda;
                return 1;
				$envioemail = new EmailTae();
   			 	$envioemail->EnvioEmailDemandas($IdPromotor,$f_sis,$frec,$numced,$IdDemanda);
		   	 	$enviado= $envioemail->ResultadoCorreoDemada();
			}
    }
	
}
?>
