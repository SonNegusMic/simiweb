<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

@include("../funciones/connPDO.php");

class cCuadras
{
    public function __construct($conn = "")
    {
        $this->db = $conn;
    }

    private $id_usuario;

    public function infoInmobiliaria($codInmo)
    {
        //echo $codInmu."ff";

        if (($consulta = $this->db->prepare(" select 
		IdInmobiliaria,Nombre,Direccion,Correo, Telefonos
		from clientessimi
        where IdInmobiliaria=?
		"))
        ) {
            $consulta->bind_param("i", $codInmo);

            $consulta->execute();
//				echo "$qry";

            if ($consulta->bind_result($IdInmobiliaria, $Nombre, $Direccion, $Correo, $Telefonos))
                while ($consulta->fetch()) {
                    $arreglo[] = array(
                        "IdInmobiliaria" => $IdInmobiliaria,
                        "Nombre" => ucwords(strtolower($Nombre)),
                        "Direccion" => $Direccion,
                        "Correo" => $Correo,
                        "Telefonos" => $Telefonos
                    );

                }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            echo "error --" . $conn->error;
        }

    }

    public function infoInmueble($codInmu)
    {

        $connPDO = new Conexion();
        $fotos = array();
        $fotos[1] = array('url' => "http://www.simiinmobiliarias.com/mcomercialweb/Fotos/1/1-14058/0233.jpg", 'tipo' => 'I');
        $fotos[2] = array('url' => 'http://www.simiinmobiliarias.com/mcomercialweb/Fotos/1/1-14058/02m.jpg', 'tipo' => 'I');
        $fotos[3] = array('url' => 'http://www.simiinmobiliarias.com/mcomercialweb/Fotos/1/1-14058/03.jpg', 'tipo' => 'I');
        $fotos[4] = array('url' => 'https://www.youtube.com/watch?v=WwJ7goq7GFc&list=PLmozF_njWpT_42xCjbZCxHN_7w7rKX8UG&index=8', 'tipo' => 'V');

        $stmt = $connPDO->prepare("SELECT d.IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,Zona,
		longitud,EdadInmueble,NombreInmo,logo,IdBarrios,NombreProm,IdPromotor,Video,Direccion,amoblado
		from datos_call d
		where Codigo_Inmueble=?");
        $stmt->bindParam(1, $codInmu, PDO::PARAM_STR);

        $arregloFotos = $this->fotosInmueble($codInmu);

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $IdInmobiliaria = $row['IdInmobiliaria'];
                $Administracion = $row['Administracion'];
                $IdGestion = $row['IdGestion'];
                $Estrato = ($row['Estrato'] > 0) ? ($row['Estrato'] - 1) : 0;
                $IdTpInm = $row['IdTpInm'];
                $Codigo_Inmueble = $row['Codigo_Inmueble'];
                $Tipo_InmuebleCC = getCampo('tipoinmuebles', "where idTipoInmueble=" . $row['IdTpInm'], 'tpccuadras');
                $Venta = $row['Venta'];
                $Canon = $row['Canon'];
                $descripcionlarga = $row['descripcionlarga'];
                $Barrio = $row['Barrio'];
                $Gestion = getCampo('gestioncomer', "where IdGestion=" . $row['IdGestion'], 'gesccuadras');
                $AreaConstruida = $row['AreaConstruida'];
                $AreaLote = $row['AreaLote'];
                $IdBarrios = $row['IdBarrios'];
                $Codigo_Inmueble = $row['Codigo_Inmueble'];
                $latitud = $row['latitud'];
                $longitud = $row['longitud'];
                $Zona = $row['Zona'];

                $cocina = "";
                $aire = "";
                $pisos = "";

                $alcobas = trae_carac1(15, $Codigo_Inmueble, 'Cantidad', 'alcoba', 0);
                $aire_acondicionado = evalua_carac1(31, $Codigo_Inmueble, 'Cantidad', 'aire acondicionado');
                $aire .= ($aire_acondicionado == 1) ? ' Si ' : 'No';
                $cocina_americana = evalua_carac_exacto1(13, $Codigo_Inmueble, 'Cantidad', 'AMERICANA ');

                $integral = evalua_carac1(13, $Codigo_Inmueble, 'Cantidad', 'integral');

                //$tp_cocina					= trae_carac1(13,$Codigo_Inmueble,'Descripcion','');
                $cocina .= ($integral == 1) ? 'Integral ' : '';
                $cocina .= ($cocina_americana == 1) ? 'Americana ' : '';
                $estacionamiento = evalua_carac1('23,37', $Codigo_Inmueble, 'Cantidad', 'parqueadero');
                $vigilancia = ucwords(strtolower(trae_carac1(24, $Codigo_Inmueble, 'Descripcion', 'horas')));
                $madera = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'madera');
                $alfombra = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'alfombra');
                $ceramica = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'ceramica');
                $porcelanato = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'porcelanato');
                $marmol = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'marmol');
                $baldosa = trae_carac1(9, $Codigo_Inmueble, 'hmnc', 'baldosa');
                $pisos .= ($madera == 1) ? 'Madera ' : '';
                $pisos .= ($alfombra == 1) ? 'Alfombra ' : '';
                $pisos .= ($ceramica == 1) ? 'Ceramica ' : '';
                $pisos .= ($porcelanato == 1) ? 'Porcelanato ' : '';
                $pisos .= ($marmol == 1) ? 'Marmol ' : '';
                $pisos .= ($baldosa == 1) ? 'Baldosa ' : '';
                $aire = trae_carac1(31, $Codigo_Inmueble, 'Cantidad', 'aire acondicionado', 0);
                $closets = trae_carac1(45, $Codigo_Inmueble, 'Cantidad', 'closet', 0);
                $banos = trae_carac1(16, $Codigo_Inmueble, 'Cantidad', utf8_decode('baño'), 0);
                $cantGaraje = trae_carac1(37, $Codigo_Inmueble, 'Cantidad', 'parqueadero', 0);
                $vigilancia = trae_carac1(24, $Codigo_Inmueble, 'Cantidad', 'hora', 0);
                $alarma = trae_carac1(24, $Codigo_Inmueble, 'Cantidad', 'alarma', 0);
                $balcon = trae_carac1(10, $Codigo_Inmueble, 'Cantidad', 'Barra americana', 0);
                $Barraestiloamerican = trae_carac1(13, $Codigo_Inmueble, 'Cantidad', 'balcon', 0);
                $Calentador = trae_carac1(13, $Codigo_Inmueble, 'Cantidad', 'Calentador', 0);
                $Chimenea = trae_carac1(27, $Codigo_Inmueble, 'Cantidad', 'Chimenea', 0);
                $CocinaIntegral = trae_carac1(13, $Codigo_Inmueble, 'Cantidad', 'Integral', 0);
                $CocinatipoAmericano = trae_carac1(13, $Codigo_Inmueble, 'Cantidad', 'AMERICANA', 0);
                $DepositoBodega = evalua_carac1(20, $Codigo_Inmueble, 'Cantidad', 'deposito', 0);
                $Despensa = trae_carac1(13, $Codigo_Inmueble, 'Cantidad', 'Despensa', 0);
                $balcon = evalua_carac1(10, $Codigo_Inmueble, 'Cantidad', 'balcon', 0);
                $gas = trae_carac1(13, $Codigo_Inmueble, 'Cantidad', 'gas n');
                $electrica = trae_carac1(13, $Codigo_Inmueble, 'Descripcion', 'ELECTRICA');
                $tp_cocina = $gas . $electrica;
                $sauna = evalua_carac1(26, $Codigo_Inmueble, 'Cantidad', 'sauna');
                $ZonaRopas = evalua_carac1(44, $Codigo_Inmueble, 'Cantidad', 'ZONA DE ROPAS');
                $turco = evalua_carac1(26, $Codigo_Inmueble, 'Cantidad', 'turco');
                $jacuzzi = evalua_carac1(16, $Codigo_Inmueble, 'Cantidad', 'jacuz');
                if ($sauna > 0 or $turco > 0 or $jacuzzi > 0) {
                    $saturja = 'Sauna Turco Jacuzzi';
                }
                $Ascensor = evalua_carac1(25, $Codigo_Inmueble, 'Cantidad', 'Ascensor', 0);
                $CanchaSquash = trae_carac1(26, $Codigo_Inmueble, 'Cantidad', 'Squash', 0);
                $CanchadeTenis = trae_carac1(26, $Codigo_Inmueble, 'Cantidad', 'Tennis', 0);
                $CanchasDeportivas = trae_carac2(26, $Codigo_Inmueble, 'Cantidad', 'Canchas Deportivas', 0);
                $CircuitoCerradoTV = trae_carac1(24, $Codigo_Inmueble, 'Cantidad', 'Circuito', 0);
                $Gimnasio = trae_carac1(26, $Codigo_Inmueble, 'Cantidad', 'Gimnasio', 0);
                $Jardin = trae_carac1(12, $Codigo_Inmueble, 'Cantidad', 'Jardin', 0);
                $JauladeGolf = trae_carac1(26, $Codigo_Inmueble, 'Cantidad', 'Golf', 0);
                $ParqueaderodeVisitantes = evalua_carac1(23, $Codigo_Inmueble, 'Cantidad', 'Visitantes', 0);
                $Piscina = evalua_carac1(26, $Codigo_Inmueble, 'Cantidad', 'Piscina', 0);
                $PlantaElectrica = evalua_carac1(22, $Codigo_Inmueble, 'Cantidad', 'Planta', 0);
                $PorteriaRecepcion = trae_carac1(25, $Codigo_Inmueble, 'Cantidad', 'Recepcion', 0);
                $SalonComunal = evalua_carac1(26, $Codigo_Inmueble, 'Cantidad', 'salon Comunal', 0);
                $Terraza = trae_carac1(10, $Codigo_Inmueble, 'Cantidad', 'Terraza', 0);
                $Vigilancia = trae_carac1(24, $Codigo_Inmueble, 'Cantidad', 'horas', 0);
                $SobreViaPrincipal = trae_carac1(29, $Codigo_Inmueble, 'Cantidad', 'Via Principal', 0);
                $Sobreviasecundaria = trae_carac1(29, $Codigo_Inmueble, 'Cantidad', 'via secundaria', 0);
                $ZonaCampestre = trae_carac1(35, $Codigo_Inmueble, 'Cantidad', 'camping', 0);
                $ZonaComercial = trae_carac1(35, $Codigo_Inmueble, 'Descripcion', 'comercio y', 0);
                $ZonaIndustrial = trae_carac1(35, $Codigo_Inmueble, 'Descripcion', 'INDUSTRIAL', 0);
                $ControlIndencios = trae_carac1(25, $Codigo_Inmueble, 'Cantidad', 'incendio', 0);
                $SalaComedorIndependiente = trae_carac1(6, $Codigo_Inmueble, 'Cantidad', 'SALA COMEDOR', 0);
                $Citofono = trae_carac1(24, $Codigo_Inmueble, 'Cantidad', 'Citofono', 0);
                $CuartodeServicio = evalua_carac1(13, $Codigo_Inmueble, 'Cantidad', 'cuarto y', 0);
                $ZonaResidencial = trae_carac1(35, $Codigo_Inmueble, 'Descripcion', 'residenci', 0);
                $uso = $ZonaComercial . $ZonaIndustrial . $ZonaResidencial;
                $Estudio = trae_carac1(27, $Codigo_Inmueble, 'Cantidad', 'Estudio', 0);
                $HalldeAlcobas = trae_carac1(5, $Codigo_Inmueble, 'Cantidad', 'hall', 0);
                $Patio = trae_carac1(44, $Codigo_Inmueble, 'Cantidad', 'Patio', 0);
                $Cocineta = trae_carac1(13, $Codigo_Inmueble, 'Cantidad', 'Cocineta', 0);
                $ZonaBBQ = trae_carac1(26, $Codigo_Inmueble, 'Cantidad', 'BBQ', 0);
                $Salondeconferencias = trae_carac1(26, $Codigo_Inmueble, 'Cantidad', 'conferencias', 0);
                $EdificioInteligente = trae_carac2(28, $Codigo_Inmueble, 'Cantidad', 'Edificio Inteligente', 0);
                $parq = ($cantGaraje == 0) ? 'Garajes' : '';
                $inm_frente = getCampo('inmuebles', "where idInm='" . $Codigo_Inmueble . "'", 'Frente');
                $inm_fondo = getCampo('inmuebles', "where idInm='" . $Codigo_Inmueble . "'", 'Fondo');

                list($aa, $codinm) = explode("-", $Codigo_Inmueble);
//                
                $error = 0;
                $msgerror = array();
                $inmalcobas = array(
                    '1',
                    '8',
                    '2',
                    '11',
                    '12',
                    '13',
                    '10'
                );
                $inmbanios = array(
                    '1',
                    '8',
                    '7',
                    '6',
                    '5',
                    '4',
                    '3',
                    '2',
                    '10',
                    '11',
                    '12',
                    '13',
                );
                if (empty($AreaLote) || $AreaLote == '0') {
                    $error++;
                    array_push($msgerror, 'Debe ingresar un area del lote.');
                }
                if (empty($Gestion) || $Gestion == '0') {
                    $error++;
                    array_push($msgerror, 'Debe ingresar un tipo de gestion.');
                }
                if (empty($Tipo_InmuebleCC) || $Tipo_InmuebleCC == '0') {
                    $error++;
                    array_push($msgerror, 'Debe ingresar un tipo de Inmueble.');
                }
                if (empty($Venta) || $Venta == '0') {
                    $error++;
                    array_push($msgerror, 'Debe ingresar un precio al inmueble.');
                }
                if (count($arregloFotos) < 5) {
                    $error++;
                    array_push($msgerror, 'Debe ingresar minimo 5 fotos del inmueble.');
                }
                if (in_array($Tipo_InmuebleCC, $inmalcobas)) {
                    if (empty($alcobas) || $alcobas == '0') {
                        $error++;
                        array_push($msgerror, 'Debe ingresar alcobas al inmueble.');
                    }
                }
                if (in_array($Tipo_InmuebleCC, $inmbanios)) {
                    if (empty($banos) || $banos == '0') {
                        $error++;
                        array_push($msgerror, 'Debe ingresar baños al inmueble.');
                    }
                }
                $datos_inmueble_entrada = array(
                    "datos_inmueble_entrada" => array(
                        "id" => $aa . $codinm,
                        "id_usuario_cliente" => '4102a06cffd756d65e65632c73f04499:5dnqofomew1r1Np3jE6ooMqsw0TsnGo7',
                        "id_usuario" => '7347',
                        "nombres" => ucwords(strtolower($row['NombreInmo'])),
                        "celular" => "$telprom",
                        "telefono" => $inm_telefono,
                        "correo" => $inmobiliaria_correo,//"desarrolloweb@tae-ltda.com",
                        "logo" => $logoinm,
                        "pagina_web" => $paginaweb,
                        "tipo_inmueble" => $Tipo_InmuebleCC,
                        "tipo_transaccion" => ucwords(strtolower($Gestion)),
                        "ciudad" => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                        "localidad" => $Zona,
                        "barrio" => ucwords(strtolower($Barrio)),
                        "departamento" => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                        "direccion" => ucwords(strtolower($row['Direccion'])),
                        "ocultar_direccion" => 1,
                        "estrato" => $Estrato,
                        "latitud" => $latitud,
                        "longitud" => $longitud,
                        "precio_venta" => $Venta,
                        "canon_arrendamiento" => $Canon,
                        "num_habitaciones" => $alcobas,
                        "num_banos" => $banos,
                        "num_parqueaderos" => $cantGaraje,
                        "tipo_parqueadero" => $tp_parqueaderos,
                        "espacios_cerrados" => '',
                        "altura" => $inm_altura,
                        "frente" => $inm_frente,
                        "fondo" => $inm_fondo,
                        "piscina_privada" => $Piscina,
                        "area" => $AreaLote,
                        "area_lote" => $AreaLote,
                        "area_construida" => $AreaConstruida,
                        "antiguedad" => date('Y') - $row['EdadInmueble'],
                        "descripcion" => utf8_encode($descripcionlarga),
                        "nombre_edificio" => '',
                        "remodelado" => 0,
                        "proyecto" => 0,
                        "etapa_proyecto" => 0,
                        "nombre_proyecto" => '',
                        "valor_administracion" => $Administracion,
                        "amoblado" => $row['amoblado'],
                        "vista" => $visto,
                        "cableado_estructurado" => '',
                        "tipo_comedor" => '',
                        "tipo_cocina" => $tp_cocina,
                        "cuarto_servicio" => $CuartodeServicio,
                        "bano_servicio" => $CuartodeServicio,
                        "zona_lavanderia" => $ZonaRopas,
                        "tipo_pisos" => $pisos,
                        "kva" => '',
                        "equipado_con" => '',
                        "electrodomesticos" => '',
                        "tipo_estufa" => '',
                        "tipo_calentador" => $Calentador,
                        "aire_acondicionado" => $aire,
                        "num_terraza" => $Terraza,
                        "area_terraza" => '',
                        "num_balcones" => $balcon,
                        "num_depositos" => $DepositoBodega,
                        "acceso_tractomulas" => '',
                        "muelle" => '',
                        "uso_lote" => $uso,
                        "num_pisos" => $num_pisos,
                        "num_apartamentos" => '',
                        "num_ascensores" => $Ascensor,
                        "vigilancia" => $Vigilancia,
                        "num_parqueadero_visita" => $ParqueaderodeVisitantes,
                        "recepcion" => $PorteriaRecepcion,
                        "circuito_cerrado_tv" => $CircuitoCerradoTV,
                        "sede_social" => $SalonComunal,
                        "planta_electrica" => $PlantaElectrica,
                        "salon_comunal" => $SalonComunal,
                        "zona_infantil" => $parque_infantil,
                        "zonas_verdes" => $ZonaCampestre,
                        "activo" => 0,
                        "piscina_comunal" => '',
                        "gimnasio" => $Gimnasio,
                        "instalacion_electrica" => '',
                        "precio_desde" => 0,
                        "codigo" => 0,
                        "ubicacion_lote" => '',
                        "area_bodega" => '',
                        "area_oficina" => '',
                        "vitrina" => '',
                        "capacidad_piso" => '',
                        "para_estrenar" => '',
                        "areas_desde" => '',
                        "acabados" => ''
                    ),
                    "datos_inmueble_fotos" => $arregloFotos

                );
                //array_push($arreglo,$arregloFotos);

            }

            if ($error > 0) {
                return array('error' => 1, 'msgerror' => $msgerror);
            } else {
                return $datos_inmueble_entrada;
            }

        } else {
            // return $stmt->errorInfo();
            // $stmt = null;
        }

    }

    public function fotosInmueble($codInmueble)
    {
        if (($consulta = $this->db->prepare(" select foto
		from fotos_nvo
        where id_inmft=?"))
        ) {
            if (!$consulta->bind_param("s", $codInmueble)) {
                echo "error bind";
            } else {
                $consulta->execute();
//				echo "$qry";
                if ($consulta->bind_result($foto))
                    $arreglo = array();
                $c = 0;
                while ($consulta->fetch()) {
                    $c++;
                    $arreglo[$c] = array(
                        "url" => "http://www.simiinmobiliarias.com/mcomercialweb/" . $foto,
                        'tipo' => 'I'
                    );

                }
                return $arreglo;

            }
        } else {
            echo "error img" . $conn->error;
        }

    }

}

?>
