<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include("../funciones/connPDO.php");


class Zonas
{
    public function __construct($conn=""){
		$this->db=$conn;
	}
    
    private $id_usuario;
    
	public function ZonasAllCiudad($data)
	{
		$connPDO = new Conexion();
		$stmt = $connPDO->prepare("SELECT IdZona,NombreZ 
								   FROM zonas 
								   WHERE IdCiudad = :IdCiudad 
								   ORDER BY NombreZ");
		$stmt->bindParam(":IdCiudad",$data['ciudad']);
		if($stmt->execute()) 
		{
			$data=array();
			$stmt->bindColumn(1,$cod);
			$stmt->bindColumn(2,$nom);
			$connPDO -> exec("SET NAMES 'utf8'");
			while($row=$stmt->fetch(PDO::FETCH_BOUND))
			{
				$data[]=array(
							"id" => $cod,
							"Nombre" => utf8_decode(ucwords(strtolower($nom)))
						);
				
			}
			return $data;
			
		}
		else
		{
			// print_r($stmt->errorInfo());

			$response=array("error"=>$stmt->errorInfo(),
							"Info"=>$data);
			echo json_encode($response);

		}
		$stmt=NULL;
	}
}
?>