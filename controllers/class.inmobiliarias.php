<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include "../funciones/connPDO.php";

class Inmobiliarias
{
    public function __construct($conn = '')
    {
        $this->db = $conn;
    }

    private $id_usuario;

    public function insertInmobiliaria($data)
    {
        $connPDO = new Conexion();

        $inmo         = $data['codigoInmo'];
        $telinmo      = $data['telInmob'];
        $telfax       = $data['faxInmob'];
        $Email        = $data['emailInmob'];
        $paginm       = $data['webInmob'];
        $lapsoinm     = $data['lCitaInmob'];
        $codAsgiInmob = $data['codAsgiInmob'];
        $analisInmob  = $data['analisInmob'];
        $nombreinm    = $data['rSocial'];
        $Direccion    = $data['direccInmob'];
        $IdInicial    = substr($nombreinm, 0, 1);
        $nit          = $data['nit'];
        $ip_creacion  = $_SERVER['REMOTE_ADDR'];
        $usu_creacion = $_SESSION['Id_Usuarios'];
        $f_creacion   = date('Y-m-d');
        $h_creacion   = date('H:i:s');
        $MA           = $data['mArrendador'];

        $stmt = $connPDO->prepare("INSERT INTO inmobiliaria (IdInmobiliaria,NombreInm,DireccionInm,TelefonoInm,Nit,Email,NroContrato,
		usu_creacion,f_creacion,h_creacion,ip_creacion,NroMatricula,Telefax)
		 VALUES (:inmo,:Nombre,:Direccion,:telinmo,:nit,:Email,:inmo,:usu_creacion,:f_creacion,:h_creacion,:ip_creacion,:MA,:Telefax)");

        if ($stmt->execute(array(
            ':inmo'         => $inmo,
            ':Nombre'       => $nombreinm,
            ':Direccion'    => $Direccion,
            ':telinmo'      => $telinmo,
            ':logo'         => $logo,
            ':Telefax'      => $telfax,
            ':nit'          => $nit,
            ':Email'        => $Email,
            ':ip_creacion'  => $ip_creacion,
            ':usu_creacion' => $usu_creacion,
            ':f_creacion'   => $f_creacion,
            ':h_creacion'   => $h_creacion,
            ':MA'           => $MA,

        ))) {

            return 1;

            $stmt = null;
        } else {
            return print_r($stmt->errorInfo()); ///retornar 0 en produccion
            $stmt = null;
        }
    }
    public function insertClientesSimi($data)
    {
        $connPDO = new Conexion();

        $inmo         = $data['codigoInmo'];
        $telinmo      = $data['telInmob'];
        $telfax       = $data['faxInmob'];
        $nit          = $data['nit'];
        $Email        = $data['emailInmob'];
        $paginm       = $data['webInmob'];
        $lapsoinm     = $data['lCitaInmob'];
        $codAsgiInmob = $data['codAsgiInmob'];
        $analisInmob  = 0;
        $nombreinm    = $data['rSocial'];
        $Direccion    = $data['direccInmob'];

        $indicativo_inm = $data['indicativo_inm'];
        $ciudad         = $data['ciudad_inm'];
        $IdInicial      = substr($nombreinm, 0, 1);

        $logo = '';
        $stmt = $connPDO->prepare("INSERT INTO clientessimi (IdInmobiliaria,IdInicial,Nombre,Direccion,Telefonos,logo,Web,nit,Correo,asignado_a,indicativo_inm,Ciudad)
		 VALUES (:inmo,:IdInicial,:Nombre,:Direccion,:telinmo,:logo,:paginm,:nit,:Email,:asignado_a,:indicativo_inm,:Ciudad)");
        if ($stmt->execute(array(
            ':inmo'           => $inmo,
            ':IdInicial'      => $IdInicial,
            ':Nombre'         => $nombreinm,
            ':Direccion'      => $Direccion,
            ':telinmo'        => $telinmo,
            ':logo'           => $logo,
            ':paginm'         => $paginm,
            ':nit'            => $nit,
            ':Email'          => $Email,
            ':asignado_a'     => $analisInmob,
            ':indicativo_inm' => $indicativo_inm,
            ':Ciudad'         => $ciudad,
        ))) {
            return 1;

        } else {
            return print_r($stmt->errorInfo());

        }
    }
    public function insertUser($data)
    {
        $connPDO = new Conexion();

        $inmo          = $data['inmo'];
        $nomrl         = $data['repNom'];
        $cedrl         = $data['repCed'];
        $direrl        = $data['repDir'];
        $telrl         = $data['repTel'];
        $celrl         = $data['repCelu'];
        $emrl          = $data['repCorreo'];
        $clave         = '12345';
        $ape           = '';
        $estado        = '0';
        $operador      = '2';
        $perfil        = '3';
        $tpdoc         = '1';
        $editacita     = '1';
        $coloragenda   = '21';
        $descargaexcel = '1';
        $agente        = '0';
        $conAnalis     = '0';
        $iduser        = consecutivo('iduser', 'usuarios');

        $stmt = $connPDO->prepare("insert ignore INTO usuarios
			  (iduser,Id_Usuarios,Nombres,apellidos,Login,
			  Clave,Estado,Estado_Old,Telefono,Correo,
			  Operador,IdInmmo,perfil,Celular,IdTipoDocumento,
			  razon_social,editacita,coloragenda,descargaexcel,agente,
			  conAnalis,Direccion)
			  VALUES
			    (:iduser,:repCed,:repNom,:ape,:repCed1,
			    :clave,:estado,:estado1,:repTel,:repCorreo,
			    :operador,:inmo,:perfil,:repCelu,:tpdoc,
			    :repNom1,:editacita,:coloragenda,:descargaexcel,:agente,
			    :conAnalis,:repDir)");
        if ($stmt->execute(array(
            ':iduser'        => $iduser,
            ':repCed'        => $cedrl,
            ':repNom'        => $nomrl,
            ':ape'           => $ape,
            ':repCed1'       => $cedrl,
            ':clave'         => $cedrl,
            ':estado'        => $estado,
            ':estado1'       => $estado,
            ':repTel'        => $telrl,
            ':repCorreo'     => $emrl,
            ':operador'      => $operador,
            ':inmo'          => $inmo,
            ':perfil'        => $perfil,
            ':repCelu'       => $celrl,
            ':tpdoc'         => $tpdoc,
            ':repNom1'       => $nomrl,
            ':editacita'     => $editacita,
            ':coloragenda'   => $coloragenda,
            ':descargaexcel' => $descargaexcel,
            ':agente'        => $agente,
            ':conAnalis'     => $conAnalis,
            ':repDir'        => $direrl,
        ))) {
            return 1;

            $stmt = null;
        } else {
            return print_r($response = array("error" => $stmt->errorInfo(),
                "status"                                 => "function insertUser",
                "data"                                   => $data));
            $stmt = null;
        }
    }
    public function insertUserPermisos($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("replace INTO sj_acces
			  (sj_acc_usr,sj_acc_pro)
			  VALUES
			    (:cedrl, 1),
				(:cedrl, 2),
				(:cedrl, 3),
				(:cedrl, 5),
				(:cedrl, 6),
				(:cedrl, 7),
				(:cedrl, 8),
				(:cedrl, 9),
				(:cedrl, 12),
				(:cedrl, 13),
				(:cedrl, 14),
				(:cedrl, 15),
				(:cedrl, 20),
				(:cedrl, 21),
				(:cedrl, 22),
				(:cedrl, 23),
				(:cedrl, 24),
				(:cedrl, 25),
				(:cedrl, 26),
				(:cedrl, 27),
				(:cedrl, 28),
				(:cedrl, 29),
				(:cedrl, 30),
				(:cedrl, 31),
				(:cedrl, 33),
				(:cedrl, 34),
				(:cedrl, 35),
				(:cedrl, 36),
				(:cedrl, 37),
				(:cedrl, 40),
				(:cedrl, 41),
				(:cedrl, 43),
				(:cedrl, 44),
				(:cedrl, 46),
				(:cedrl, 52),
				(:cedrl, 100),
				(:cedrl, 97),
				(:cedrl, 45)");
        if ($stmt->execute(array(
            ':cedrl' => $data,
        ))) {
            return 1;

            $stmt = null;
        } else {
            return print_r($stmt->errorInfo());
            $stmt = null;
        }
    }

    public function updateInmobiliariaGral($data)
    {
        $connPDO = new Conexion();

        $inmo     = $data['inmo'];
        $telinmo  = $data['telinmo'];
        $telfax   = $data['telfax'];
        $Email    = $data['Email'];
        $paginm   = $data['paginm'];
        $lapsoinm = $data['lapsoinm'];

        $stmt = $connPDO->prepare("UPDATE inmobiliaria
				SET  TelefonoInm  		= :telinmo,
				Telefax 				= :telfax,
				Email 					= :emailinm,
				PaginaWeb 				= :paginm,
				lapsocita 				= :lapsoinm
				WHERE IdInmobiliaria	= :inmo");
        if ($stmt->execute(array(
            ':telinmo'  => $telinmo,
            ':telfax'   => $telfax,
            ':Email'    => $Email,
            ':paginm'   => $paginm,
            ':lapsoinm' => $lapsoinm,
            ':inmo'     => $inmo,
        ))) {
            $this->updateInmobiliariaGral2($data2);
            return 1;

            $stmt = null;
        } else {
            return print_r($stmt->errorInfo());
            $stmt = null;
        }
    }
    public function updateInmobiliariaGral2($data)
    {
        $connPDO = new Conexion();

        $inmo     = $data['inmo'];
        $emailinm = $data['emailinm'];
        $analista = $data['analista'];
        $Email    = $data['Email'];
        $paginm   = $data['paginm'];
        $lapsoinm = $data['lapsoinm'];

        $stmt = $connPDO->prepare("UPDATE clientessimi
				SET
				Correo 					= :emailinm,
				asignado_a				= :analista,
				Correo1 				= :emailinm'
				WHERE IdInmobiliaria	= :inmo");
        if ($stmt->execute(array(
            ':emailinm' => $emailinm,
            ':analista' => $analista,
            ':emailinm' => $emailinm,
            ':inmo'     => $inmo,
        ))) {
            return 1;
            $stmt = null;
        } else {
            return print_r($stmt->errorInfo());
            $stmt = null;
        }
    }
    public function updateRL($data)
    {
        $connPDO = new Conexion();

        $inmo   = $data['inmo'];
        $nomrl  = $data['repNom'];
        $cedrl  = $data['repCed'];
        $direrl = $data['repDir'];
        $telrl  = $data['repTel'];
        $celrl  = $data['repCelu'];
        $emrl   = $data['repCorreo'];

        $stmt = $connPDO->prepare("UPDATE inmobiliaria
				SET  RepresentanteL 		= :nomrl,
				CedulaRL 					= :cedrl,
				DireccionRL					= :direrl,
				TelefonoRL 					= :telrl,
				CelularRL 					= :celrl,
				EmailRL 					= :emrl
				WHERE IdInmobiliaria		= :inmo");
        if ($stmt->execute(array(
            ':nomrl'  => $nomrl,
            ':cedrl'  => $cedrl,
            ':direrl' => $direrl,
            ':telrl'  => $telrl,
            ':celrl'  => $celrl,
            ':emrl'   => $emrl,
            ':inmo'   => $inmo,
        ))) {
            return $cedrl;

        } else {
            // return print_r($stmt->errorInfo());
            return $inmo;

        }
        $stmt = null;
    }
    public function updateGerente($data)
    {
        $connPDO = new Conexion();

        $inmo = $data['inmo'];
        $nomc = $data['comNombre'];
        $telc = $data['comTel'];
        $emc  = $data['comCorreo'];

        $stmt = $connPDO->prepare("UPDATE inmobiliaria
				SET   Contacto 			= :nomc,
				TelefonoC 				= :telc,
				EmailC 					= :emc
				WHERE IdInmobiliaria	= :inmo");
        if ($stmt->execute(array(
            ':inmo' => $inmo,
            ':nomc' => $nomc,
            ':telc' => $telc,
            ':emc'  => $emc,
        ))) {
            return 1;
            $stmt = null;
        } else {
            return print_r($stmt->errorInfo());
            $stmt = null;
        }
    }
    public function dataInmobiliaria($id)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("Select TelefonoInm,Telefax,Email,PaginaWeb,lapsocita,asignado_a,
                consecinmueble,LaGuia,VivaReal,RepresentanteL,i.Nit,
                CedulaRL,DireccionRL,TelefonoRL,CelularRL,EmailRL,
                Contacto,TelefonoC,EmailC,i.IdInmobiliaria,sincronizap,
                activarn,ecorreo,c.logo,inmobsmp,c.Nombre,c.Direccion,c.indicativo_inm,c.MA,i.estado_inmo,i.razon_desactiva,i.republicaauto,c.Ciudad
				from inmobiliaria i,clientessimi c
				where i.IdInmobiliaria=c.IdInmobiliaria
				and i.IdInmobiliaria= :idinmo ");
        if ($stmt->execute(array(
            ":idinmo" => $id,
        ))) {

            while ($fila78 = $stmt->fetch()) {

                $data[] = array(
                    "TeleInm"         => $fila78['TelefonoInm'],
                    "NombreInm"       => $fila78['Nombre'],
                    "NitInm"          => $fila78['Nit'],
                    "TelefaxInm"      => $fila78['Telefax'],
                    "EmailInm"        => $fila78['Email'],
                    "PagWebInm"       => $fila78['PaginaWeb'],
                    "LapsoCitaInm"    => $fila78['lapsocita'],
                    "Consecutivo"     => $fila78['consecinmueble'],
                    "NomRL"           => $fila78['RepresentanteL'],
                    "CeduRL"          => $fila78['CedulaRL'],
                    "DireccionRL"     => $fila78['DireccionRL'],
                    "Direccion"       => $fila78['Direccion'],
                    "indicativo_inm"  => $fila78['indicativo_inm'],
                    "MA"              => $fila78['MA'],
                    "TelRL"           => $fila78['TelefonoRL'],
                    "CeluRL"          => $fila78['CelularRL'],
                    "EmailRL"         => $fila78['EmailRL'],
                    "Contacto"        => $fila78['Contacto'],
                    "TelefonoC"       => $fila78['Telefax'],
                    "EmailC"          => $fila78['EmailC'],
                    "idInmo"          => $fila78['IdInmobiliaria'],
                    "actup"           => $fila78['sincronizap'],
                    "activarn"        => $fila78['activarn'],
                    "activarml"       => $fila78['ecorreo'],
                    "asignado_a"      => $fila78['asignado_a'],
                    "logo"            => $fila78['logo'],
                    "inmobsmp"        => $fila78['inmobsmp'],
                    "estado_inmo"     => $fila78['estado_inmo'],
                    "razon_desactiva" => $fila78['razon_desactiva'],
                    "republicaAuto"   => $fila78['republicaauto'],
                    "Ciudad"          => $fila78['Ciudad'],
                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function dataInmobiliariaPortales($data)
    {
        $connPDO   = new Conexion();
        $id        = $data['inmo'];
        $codPortal = $data['codPortal'];
        $cond      = "";
        if ($codPortal > 0) {
            $cond = " and i.IdPortal=:codPortal";
        }
        $stmt = $connPDO->prepare("SELECT i.IdPortal,i.IdInmobiliaria,i.mailventa,i.mailrenta,
                p.NomPortal,p.imagenp,p.pago,telrenta,telventa,p.ordport
                from Portales p, PublicaPortales i
                where i.IdPortal=p.IdPortal
				and i.IdInmobiliaria= :idinmo
				$cond
				order by p.ordport");
        if ($codPortal > 0) {
            $stmt->bindParam(":codPortal", $codPortal);
        }
        $stmt->bindParam(":idinmo", $id);
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {

                $data[] = array(
                    "ordport"   => $row['ordport'],
                    "IdPortal"  => $row['IdPortal'],
                    "mailventa" => $row['mailventa'],
                    "mailrenta" => $row['mailrenta'],
                    "telrenta"  => $row['telrenta'],
                    "telventa"  => $row['telventa'],
                    "NomPortal" => $row['NomPortal'],
                    "imagenp"   => $row['imagenp'],
                    "pago"      => $row['pago'],
                    "activo"    => $this->verifyPortales($id, $row['IdPortal']),
                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function dataInmobiliariaPortalesTodos($id = 0)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT p.IdPortal,
                p.NomPortal,p.imagenp,p.pago,p.ordport,p.mail_port
                from Portales p
                where  p.estport=1
                order by p.ordport");
        if ($stmt->execute()) {

            while ($row = $stmt->fetch()) {

                $data[] = array(
                    "ordport"   => $row['ordport'],
                    "IdPortal"  => $row['IdPortal'],
                    "NomPortal" => $row['NomPortal'],
                    "imagenp"   => $row['imagenp'],
                    "pago"      => $row['pago'],
                    "mail_port" => $row['mail_port'],
                    "activo"    => $this->verifyPortales($id, $row['IdPortal']),
                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }

    public function verifyPortales($id, $idPortal)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("Select p.IdPortal,p.IdInmobiliaria
                from PublicaPortales p
                where p.IdInmobiliaria= :idinmo
                and IdPortal=:portal");
        if ($stmt->execute(array(
            ":idinmo" => $id,
            ":portal" => $idPortal,
        ))) {
            $existe = $stmt->rowCount();
            return $existe;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function verifyInmobiliaria($id)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT IdInmobiliaria
                from clientessimi
                where IdInmobiliaria = :idinmo");
        if ($stmt->execute(array(
            ":idinmo" => $id['codigoInmo'],
        ))) {
            $existe = $stmt->rowCount();
            if ($existe > 0) {
                return 1;
            } else {
                return 0;
            }

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function updateInmobiliariaPortales($data)
    {
        $connPDO   = new Conexion();
        $inmo      = $data['inmo'];
        $IdPortal  = $data['IdPortal'];
        $mailventa = $data['mailventa'];
        $mailrenta = $data['mailrenta'];
        $telrenta  = $data['telrenta'];
        $telventa  = $data['telventa'];

        $stmt = $connPDO->prepare("
        replace into PublicaPortales(IdPortal,mailventa,mailrenta,IdInmobiliaria,telrenta,telventa)
        values(:IdPortal,:mailventa,:mailrenta,:idinmo,:telrenta,:telventa)");
        if ($stmt->execute(array(
            ":idinmo"    => $inmo,
            ":IdPortal"  => $IdPortal,
            ":mailventa" => $mailventa,
            ":mailrenta" => $mailrenta,
            ":telrenta"  => $telrenta,
            ":telventa"  => $telventa,
        ))) {

            return 1;
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function deleteInmobiliariaPortales($data)
    {
        $connPDO  = new Conexion();
        $inmo     = $data['inmo'];
        $IdPortal = $data['IdPortal'];

        $stmt = $connPDO->prepare("
        delete from  PublicaPortales
        where IdPortal=:IdPortal
        and IdInmobiliaria=:idinmo");
        if ($stmt->execute(array(
            ":idinmo"   => $inmo,
            ":IdPortal" => $IdPortal,
        ))) {

            $data[] = array(
                "IdPortal"  => $IdPortal,
                "mailventa" => $mailventa,
                "mailrenta" => $mailrenta,
                "telrenta"  => $telrenta,
                "telventa"  => $telventa,
                "idinmo"    => $inmo,
            );

            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function updateDataInterno($data)
    {
        $connPDO = new Conexion();

        $inmo            = $data['inmo'];
        $codSimipedia    = $data['codSimipedia'];
        $consecinmueble  = $data['consecinmueble'];
        $actSincroAuto   = $data['actSincroAuto'];
        $actSincroOpc    = $data['actSincroOpc'];
        $actMailDoc      = $data['actMailDoc'];
        $estado_inmo     = $data['estado_inmo'];
        $razon_desactiva = $data['razon_desactiva'];
        $republicaauto   = $data['republicaAuto'];
        $actInterfaceNvo = $data['actInterfaceNvo'];
        $cambio          = $data['valActInmob'];

        $stmt = $connPDO->prepare("UPDATE inmobiliaria
				SET
				inmobsmp 				= :codSimipedia,
				consecinmueble 			= :consecinmueble,
				sincronizap 			= :sincronizap,
				activarn 				= :activarn,
				ecorreo 				= :ecorreo,
				estado_inmo 			= :estado_inmo,
				republicaauto 			= :republicaAauto,
				razon_desactiva 		= :razon_desactiva
				WHERE IdInmobiliaria   	= :inmo");
        if ($stmt->execute(array(
            ':inmo'            => $inmo,
            ':consecinmueble'  => $consecinmueble,
            ':codSimipedia'    => $codSimipedia,
            ':sincronizap'     => $actSincroAuto,
            ':activarn'        => $actSincroOpc,
            ':ecorreo'         => $actMailDoc,
            ':republicaAauto'  => $republicaauto,
            ':estado_inmo'     => $estado_inmo,
            ':razon_desactiva' => $razon_desactiva,
        ))) {
            if ($cambio == 1) {
                $this->insertaLogRetiroInmo($inmo, $estado_inmo, $razon_desactiva);
            }
            return 1;
            $stmt = null;
        } else {
            return print_r($stmt->errorInfo());
            $stmt = null;
        }
    }
    public function insertaLogRetiroInmo($inmo, $estado_inmo, $razon_desactiva)
    {
        $connPDO  = new Conexion();
        $conse_li = consecutivo('conse_li', 'log_retira_inmo');
        $usu_li   = $_SESSION['iduser'];
        $fec_li   = date('Y-m-d');
        $hor_li   = date('H:i:s');
        $ip_li    = $_SERVER['REMOTE_ADDR'];

        $stmt = $connPDO->prepare("
        insert into log_retira_inmo
        (conse_li,inmob_li,usu_li,fec_li,hor_li,ip_li,raz_li,nvo_est_li)
        values
        (:conse_li,:inmob_li,:usu_li,:fec_li,:hor_li,:ip_li,:raz_li,:nvo_est_li)");
        if ($stmt->execute(array(
            ":conse_li"   => $conse_li,
            ":inmob_li"   => $inmo,
            ":usu_li"     => $usu_li,
            ":fec_li"     => $fec_li,
            ":hor_li"     => $hor_li,
            ":ip_li"      => $ip_li,
            ":raz_li"     => $razon_desactiva,
            ":nvo_est_li" => $estado_inmo,
        ))) {

            return 1;
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function updateDataAsignacion($data)
    {
        $connPDO = new Conexion();

        $inmo      = $data['inmo'];
        $asignadoA = $data['asignadoA'];

        $stmt = $connPDO->prepare("UPDATE clientessimi
				SET   asignado_a 			= :asignadoA
				WHERE IdInmobiliaria	= :inmo");
        if ($stmt->execute(array(
            ':inmo'      => $inmo,
            ':asignadoA' => $asignadoA,
        ))) {
            return 1;

        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function updateInmobiliaria($data)
    {
        $connPDO = new Conexion();

        $inmo         = $data['codigoInmo'];
        $telinmo      = $data['telInmob'];
        $telfax       = $data['faxInmob'];
        $Email        = $data['emailInmob'];
        $lapsoinm     = $data['lCitaInmob'];
        $codAsgiInmob = $data['codAsgiInmob'];
        $paginm       = $data['webInmob'];
        $Direccion    = $data['direccInmob'];
        $MA           = $data['mArrendador'];

        $stmt = $connPDO->prepare("update inmobiliaria
			set
			DireccionInm			=:Direccion,
			TelefonoInm				=:telinmo,
			Email					=:Email,
			PaginaWeb				=:paginm,
			lapsocita				=:lapso,
			Telefax  				=:telfax,
			NroMatricula			=:MA
			where IdInmobiliaria	=:inmo");

        if ($stmt->execute(array(
            ':inmo'      => $inmo,
            ':Direccion' => $Direccion,
            ':telinmo'   => $telinmo,
            ':telfax'    => $telfax,
            ':lapso'     => $lapsoinm,
            ':paginm'    => $paginm,
            ':Email'     => $Email,
            ':MA'        => $MA,

        ))) {

            return 1;

            $stmt = null;
        } else {
            return print_r($stmt->errorInfo()); ///retornar 0 en produccion
            $stmt = null;
        }
    }
    public function updateClientesSimi($data)
    {
        $connPDO = new Conexion();

        $inmo           = $data['codigoInmo'];
        $telinmo        = $data['telInmob'];
        $telfax         = $data['faxInmob'];
        $Email          = $data['emailInmob'];
        $paginm         = $data['webInmob'];
        $lapsoinm       = $data['lCitaInmob'];
        $codAsgiInmob   = $data['codAsgiInmob'];
        $analisInmob    = 0;
        $Direccion      = $data['direccInmob'];
        $indicativo_inm = $data['indicativo_inm'];
        $MA             = $data['mArrendador'];
        $ciudad         = $data['ciudad_inm'];

        $stmt = $connPDO->prepare("update clientessimi
	    	set
	    	Direccion				=:Direccion,
	    	Telefonos				=:telinmo,
	    	Web						=:paginm,
	    	Correo					=:Email,
	    	asignado_a				=:asignado_a,
	    	indicativo_inm			=:indicativo_inm,
	    	MA  					=:MA,
	    	Ciudad  				=:Ciudad
	    	where IdInmobiliaria	=:inmo");
        if ($stmt->execute(array(
            ':Direccion'      => $Direccion,
            ':telinmo'        => $telinmo,
            ':paginm'         => $paginm,
            ':Email'          => $Email,
            ':asignado_a'     => $analisInmob,
            ':indicativo_inm' => $indicativo_inm,
            ':MA'             => $MA,
            ':inmo'           => $inmo,
            ':Ciudad'         => $ciudad,
        ))) {
            return 1;
        } else {
            return print_r($stmt->errorInfo());
        }
    }
    public function updateContactoPortales($data)
    {
        $connPDO  = new Conexion();
        $inmo     = $data['Inmo'];
        $IdPortal = $data['codPortal'];
        $aserNom  = $data['aserNom'];
        $aseMail  = $data['aserMail'];
        $telase   = $data['aserTel'];
        $celase   = $data['aserCel'];
        $cupo     = $data['aserCupo'];

        $stmt = $connPDO->prepare("
        update PublicaPortales
		SET
		aseportal			 	= :aserNom,
		mailase					= :aseMail,
		telase				 	= :telase,
		celase				 	= :celase,
		cupoportal  			= :cupo
		where IdInmobiliaria  	= :idinmo
		and IdPortal 		  	= :IdPortal");
        if ($stmt->execute(array(
            ":idinmo"   => $inmo,
            ":IdPortal" => $IdPortal,
            ":aserNom"  => $aserNom,
            ":aseMail"  => $aseMail,
            ":telase"   => $telase,
            ":celase"   => $celase,
            ":cupo"     => $cupo,
        ))) {

            return 1;
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function getContactoPortales($data)
    {
        $connPDO  = new Conexion();
        $inmo     = $data['inmo'];
        $IdPortal = $data['IdPortal'];

        $stmt = $connPDO->prepare("
        select aseportal,telase,celase,cupoportal,mailase
        from  PublicaPortales
        where IdPortal=:IdPortal
        and IdInmobiliaria=:idinmo");
        if ($stmt->execute(array(
            ":idinmo"   => $inmo,
            ":IdPortal" => $IdPortal,
        ))) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "nombresAse" => $row['aseportal'],
                    "mailAse"    => $row['mailase'],
                    "telAse"     => $row['telase'],
                    "celAse"     => $row['celase'],
                    "cupoPortal" => $row['cupoportal'],
                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function dataInmobiliariaLogRetiro($data)
    {
        $connPDO = new Conexion();
        $inmo    = $data['inmo'];
        $cond    = '';
        if ($inmo == 1) {
            $cond = '';
        } else {
            $cond = 'where  inmob_li=:inmob_li';
        }

        $stmt = $connPDO->prepare("SELECT conse_li,inmob_li,usu_li,fec_li,hor_li,ip_li,raz_li,nvo_est_li
                from log_retira_inmo
                $cond");
        if ($inmo != 1) {
            $stmt->bindParam(':inmob_li', $inmo);
        }
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {

                $estado = ($row['nvo_est_li'] == 0) ? '<label class="label label-danger">Inactiva</label>' : '<label class="label label-success">Activa</label>';
                $data[] = array(
                    "conse_li"   => $row['conse_li'],
                    "inmob_li"   => "<b>" . $row['inmob_li'] . " </b>" . ucwords(strtolower(getCampo('inmobiliaria', "where IdInmobiliaria='" . $row['inmob_li'] . "'", 'NombreInm'))),
                    "usu_li"     => getCampo('usuarios', "where iduser='" . $row['usu_li'] . "'", 'concat(Nombres," ",apellidos)'),
                    "fec_li"     => $row['fec_li'],
                    "hor_li"     => $row['hor_li'],
                    "ip_li"      => $row['ip_li'],
                    "raz_li"     => $row['raz_li'],
                    "nvo_est_li" => $estado,

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function dataInmobiliariaRepublicaM2($data)
    {
        $connPDO = new Conexion();
        $inmo    = $data['inmo'];
        $cond    = '';
        if ($inmo == 1) {
            $cond = '';
        } else {
            $cond = 'and  inmo_bit=:inmo_bit';
        }

        $stmt = $connPDO->prepare("SELECT conse_bit,inmo_bit,inmu_bit,usu_bit,fec_bit,hor_bit,ip_bit,cambio_bit
                from cambios_inmueble
                where tipo_bit=21
                $cond
                ");
        if ($inmo != 1) {
            $stmt->bindParam(':inmo_bit', $inmo);
        }
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $cadena1 = wordwrap(utf8_encode($row['cambio_bit']), 80, "<br>", true);

                $data[] = array(
                    "conse_bit"  => $row['conse_bit'],
                    "inmo_bit"   => "<b>" . $row['inmo_bit'] . " </b>" . ucwords(strtolower(getCampo('inmobiliaria', "where IdInmobiliaria='" . $row['inmo_bit'] . "'", 'NombreInm'))),
                    "nomusu_bit" => getCampo('usuarios', "where Id_Usuarios='" . $row['usu_bit'] . "'", 'concat(Nombres," ",apellidos)'),
                    "inmu_bit"   => $row['inmu_bit'],
                    "usu_bit"    => $row['usu_bit'],
                    "fec_bit"    => $row['fec_bit'],
                    "hor_bit"    => $$row['hor_bit'],
                    "ip_bit"     => $row['ip_bit'],
                    "cambio_bit" => $cadena1,

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }

    public function saveSucursales($data)
    {
        $connPDO = new Conexion();
        $inmo    = $_SESSION['IdInmmo'];
        $cond    = '';

        $stmt = $connPDO->prepare("INSERT INTO sucursales_inmobiliaria (inmob_scr,cod_suc_scr,nombre_scr,dir_scr,tel_scr,mail_scr,estado_scr,gerente_scr,ciudad_scr,celular_scr,indicativo_scr) VALUES (:inmob_scr,:cod_suc_scr,:nombre_scr,:dir_scr,:tel_scr,:mail_scr,:estado_scr,:gerente_scr,:ciudad_scr,:celular_scr,:indicativo_scr)");

        if ($stmt->execute(array(
            ":inmob_scr"      => $data['inmobi'],
            ":cod_suc_scr"    => $data['sucursal_suc'],
            ":nombre_scr"     => $data['nombre_suc'],
            ":dir_scr"        => $data['direccion_suc'],
            ":tel_scr"        => $data['telefono_suc'],
            ":mail_scr"       => $data['mail_suc'],
            ":estado_scr"     => $data['actSucursales'],
            ":cod_suc_scr"    => $data['codigo_suc'],
            ":gerente_scr"    => $data['sucurAsesor'],
            ":ciudad_scr"     => $data['ciudad_suc'],
            ":celular_scr"    => $data['celular_suc'],
            ":indicativo_scr" => $data['Indicativo_src'],
        ))) {
            if ($stmt->rowCount() > 0) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function updateSucursales($data)
    {
        $connPDO = new Conexion();

        $inmo = $_SESSION['IdInmmo'];

        $stmt = $connPDO->prepare("UPDATE sucursales_inmobiliaria
        SET
        nombre_scr=:nombre_scr,
        dir_scr=:dir_scr,
        tel_scr=:tel_scr,
        mail_scr=:mail_scr,
        estado_scr=:estado_scr,
        cod_suc_scr=:cod_suc_scr,
        gerente_scr=:gerente_scr,
        ciudad_scr=:ciudad_scr,
		celular_scr=:celular_scr,
		indicativo_scr=:indicativo_scr
        WHERE conse_scr=:conse_scr
        AND inmob_scr=:inmob_scr");
        if ($stmt->execute(array(
            ":conse_scr"   	  => $data['conseSucu'],
            ":inmob_scr"   	  => $data['inmobi'],
            ":cod_suc_scr" 	  => $data['codigo_suc'],
            ":nombre_scr"  	  => $data['nombre_suc'],
            ":dir_scr"     	  => $data['direccion_suc'],
            ":tel_scr"     	  => $data['telefono_suc'],
            ":mail_scr"    	  => $data['mail_suc'],
            ":estado_scr"  	  => $data['actSucursales'],
            ":gerente_scr" 	  => $data['sucurAsesor'],
            ":ciudad_scr"     => $data['ciudad_suc'],
            ":celular_scr"    => $data['celular_suc'],
            ":indicativo_scr" => $data['Indicativo_src'],

        ))) {
            if ($stmt->rowCount() > 0) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return print_r($stmt->errorInfo());

        }
    }
    public function getSucursales($data)
    {

        $connPDO = new Conexion();
        $inmo    = ($data['inmobi'] > 0) ? $data['inmobi'] : $_SESSION['IdInmmo'];

        $sql = '';
        if (!empty($data['conseSucur'])) {

            $sql .= ' AND conse_scr = :conse_scr';
        }

        $stmt = $connPDO->prepare("SELECT  conse_scr,cod_suc_scr,nombre_scr,dir_scr,tel_scr,mail_scr,estado_scr,gerente_scr,ciudad_scr,celular_scr
		    ,indicativo_scr
        	FROM sucursales_inmobiliaria
        	where inmob_scr=:inmob_scr
        	$sql");
        if (!empty($data['conseSucur'])) {

            $stmt->bindParam(':conse_scr', $data['conseSucur']);
        }
        $stmt->bindParam(':inmob_scr', $inmo);
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $cadena1   = wordwrap(utf8_encode($row['cambio_bit']), 80, "<br>", true);
                $btnEdit   = '<button type="button"  data-placement="bottom" title="Editar Sucursal" data-toggle="modal" data-target="#modal-sucursal" class=" btn-xs btn btn-warning editSucur"><i class="fa fa-edit"></i></button>';
                $btnDelete = '<button type="button"  data-placement="bottom" title="Eliminar Sucursal" class=" btn-xs btn btn-danger deleteSucur"><i class="fa fa-remove"></i></button>';
                $idcod     = '<input type="hidden" name="conseSucur" id="conseSucur" class="conseSucur" value="' . $row['conse_scr'] . '">';
                $estado    = ($row['estado_scr'] == 1) ? '<span class="label label-success">Activo</span>' : '<span class="label label-danger">Inactivo</span>';

                $data[] = array(
                    "consecutivo"    => $row['conse_scr'],
                    "codigoSucur"    => $row['cod_suc_scr'],
                    "codigoSucurest" => "<b>" . $row['cod_suc_scr'] . "</b>",
                    "nombre"         => $row['nombre_scr'],
                    "direccion"      => $row['dir_scr'],
                    "telefono"       => $row['tel_scr'],
                    "mail"           => $row['mail_scr'],
                    "estado"         => $row['estado_scr'],
                    "btnAcc"         => $btnEdit . $btnDelete . $idcod,
                    "estadoText"     => $estado,
                    "encargado"      => ucwords(strtolower(getCampo('usuarios', "where iduser='" . $row['gerente_scr'] . "'", 'concat(Nombres," ",apellidos)'))),
                    "iduser"         => $row['gerente_scr'],
                    "ciudad_scr"=>$row['ciudad_scr'],
					"celular_scr"=>$row['celular_scr'],
					"indicativo_scr"=>$row['indicativo_scr']

                );
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());

        }

    }
    public function deleteSucursales($data)
    {
        $connPDO = new Conexion();
        $inmo    = $_SESSION['IdInmmo'];
        $stmt    = $connPDO->prepare("DELETE FROM  sucursales_inmobiliaria
        	where inmob_scr=:inmob_scr
        	AND conse_scr = :conse_scr");
        $stmt->bindParam(':conse_scr', $data['conseSucur']);
        $stmt->bindParam(':inmob_scr', $data['inmobi']);
        if ($stmt->execute()) {
            return 1;
        } else {
            return print_r($stmt->errorInfo());
        }

    }

}
