<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '0');


class Usuarios
{
    public function __construct($conn){
    $this->db=$conn;
  }
    
    private $id_usuario;
    
    public function datosUsuario($id_usuario)
    {
        if(($consulta=$this->db->prepare("select iduser,Id_Usuarios,Nombres,apellidos,Login,Clave,
    Foto,Estado,Direccion,Telefono,Correo,
    Operador,IdInmmo,Huella,perfil,Celular,
    IdTipoDocumento,nombre1,nombre2,apellido1,apellido2,
    razon_social,telefono2,telefono3,celular2,pais_u,
    depto_u,ciudad_u,ciudad_utext,zonausu,editacita,
    coloragenda,descargaexcel,ext1,ext2,ext3
        from usuarios
        where Id_Usuarios=?
")))
    {   
      if(!$consulta->bind_param("i",$id_usuario))
      {
        echo "error bind";
      }
      else
      { 
        $consulta->execute();
//        echo "$qry";
        
        if($consulta->bind_result($iduser,$Id_Usuarios,$Nombres,$apellidos,$Login,$Clave,
                    $Foto,$Estado,$Direccion,$Telefono,$Correo,
                    $Operador,$IdInmmo,$Huella,$perfil,$Celular,
                    $IdTipoDocumento,$nombre1,$nombre2,$apellido1,$apellido2,
                    $razon_social,$telefono2,$telefono3,$celular2,$pais_u,
                    $depto_u,$ciudad_u,$ciudad_utext,$zonausu,$editacita,
                    $coloragenda,$descargaexcel,$ext1,$ext2,$ext))
        while($consulta->fetch())
        {
           $arreglo[]=$iduser;//0
           $arreglo[]=$Id_Usuarios;//1
                     $arreglo[]=$Nombres;//2
                     $arreglo[]=$apellidos;//3
                     $arreglo[]=$Login;//4
           if(strlen($Foto)<5)
           {
                      $arreglo[]="https://www.simiinmobiliarias.com/sj/img/def_avatar.gif";//5
           }
           else
           {
                      $arreglo[]="https://www.simiinmobiliarias.com/mcomercialweb/".$Foto;//5
           }
           $arreglo[]=$Estado;//6
                     $arreglo[]=$Direccion;//7
                     $arreglo[]=$Telefono;//8
                     $arreglo[]=$Correo;//9
           $arreglo[]=$Celular;//10
           $arreglo[]=$perfil;//11
                     $arreglo[]=getCampo("grupos_usuarios","where Id_Grupo=".$perfil,"Nombre");//12
                     $arreglo[]=$IdInmmo;//13
                     $arreglo[]=$nombre1;//14
                     $arreglo[]=$nombre2;//15
                     $arreglo[]=$apellido1;//16
                     $arreglo[]=$apellido2;//17
                     $arreglo[]=$razon_social;//18
                     $arreglo[]=$pais_u;//19
                     $arreglo[]=$ciudad_u;//20
                     $arreglo[]=$depto_u;//21
        }
               // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
                //print_r($arreglo);
                return $arreglo;
      }
    }
    else
    {
      echo  "error --".$conn->error;
    }
        
    }
    public function validaExisteUsuario($Login,$inmob='')
    {
        $w_conexion = new MySQL();
        $condInmob="";
        if($inmob)
        {
          $condInmob="and IdInmmo=$inmob";  
        }
        $consExiste="select Login
        from usuarios
        where Login='$Login'
        $condInmob";
        //echo $consExiste;
    $res=$w_conexion->ResultSet($consExiste);
    $existe=$w_conexion->FilasAfectadas($res);
        //echo $existe."------->".$Login;
        return $existe;
        
    }
    public function validarIngreso($usuario,$password,$fte='')
    {
        $w_conexion = new MySQL();
    $master="T1emwc";
        $consExiste="SELECT u.iduser,u.Id_Usuarios,u.Nombres,u.Foto,u.Estado,u.Login,
            u.Clave,u.IdInmmo,u.apellidos,u.perfil,g.Nombre AS  perfildes 
            FROM usuarios u 
            INNER JOIN grupos_usuarios g ON u.perfil=g.Id_Grupo
            WHERE u.Login='$usuario' 
            AND (u.Clave='".$password."' or '".$password."' ='$master') 
            and u.Estado <> 9";
       // echo "<br>".$consExiste."<br>";
    $res=$w_conexion->ResultSet($consExiste);
    $existe=$w_conexion->FilasAfectadas($res);
        if($existe==0)
        {
            echo "Por Favor Confirme su Informacion y Vuelva a Intentar";
      ?>
            <script>
            //window.location.href="../mwc/index.php?error=1";
            </script>
            <?php
        }
        else
        {
      while($row_rsPass=$w_conexion->FilaSiguienteArray($res))
      {
        //Registrar sesión
        $ip_visitante   = $_SERVER['REMOTE_ADDR'];
        $fecha      = date('Y-m-d');
        $hora       = date('H:i:s');
        $f_sis      = date('Y-m-d');
        $idUsu      =$row_rsPass['Id_Usuarios'];
        $idinmo     =$row_rsPass['IdInmmo'];
        $hor1     =date('H');
        if($password==$master)
        {
          $origen=2;
        }
        else
        {
          $origen=1;
        }
        $hor=$hor1+0;
        
        $min=date('i');
        $seg=date('s');
        $hora = "$hor:$min:$seg";
        $sql_log="INSERT INTO sj_log_singin (sj_log_usr, sj_log_ip, sj_log_dat, sj_log_hre) 
        VALUES
        ('$idUsu', '$ip_visitante', '$fecha', '$hora')";
        $rs_log=$w_conexion->RecordSet($sql_log);
        
        $ssql_ing = "insert LOW_PRIORITY into control_ingresos  (ip, fecha,hora, id_inmobiliaria,id_usuario,origen,nva) 
        values 
        ('$ip_visitante', '$fecha','$hora','$idinmo','$idUsu','$origen',1)"; 
        
        $rs_log2=$w_conexion->RecordSet($ssql_ing);
        $_SESSION='';
        $_SESSION['codUser']     = $row_rsPass['iduser'];//0
        $_SESSION['id_actual']   = $row_rsPass['Id_Usuarios'];    //ID
        $_SESSION['nickname']  = $row_rsPass['Login'];          //Nick o nombre de usuario
        $_SESSION['nombre']    = $row_rsPass['Nombres'];  //Nombre real
        $_SESSION['email']     = $row_rsPass['Correo']; //Dirección de correo electrónico
        $_SESSION['Id_Usuarios'] = $row_rsPass['Id_Usuarios'];//5
        $_SESSION['Login']       = $row_rsPass['Login'];//6
        $_SESSION['IdInmmo']     = $row_rsPass['IdInmmo'];//7
        $_SESSION['Id_Usuarios'] = $row_rsPass['Id_Usuarios'];//8
        $_SESSION['Nombres']     = $row_rsPass['Nombres'] . ' ' . $row_rsPass['apellidos'];//9
        $_SESSION['Foto']        = $row_rsPass['Foto'];//10
        $_SESSION['Estado']      = $row_rsPass['Estado'];//11
        $_SESSION['Login']       = $row_rsPass['Login'];//12
        $_SESSION['Clave']       = $row_rsPass['Clave'];//13
        $_SESSION['IdInmmo']     = $row_rsPass['IdInmmo'];//14
        $_SESSION['apellidos']   = $row_rsPass['apellidos'];//15
        $_SESSION['perfil']      = $row_rsPass['perfil'];//16
        $_SESSION['perfildes']   = $row_rsPass['perfildes'];//17
        $_SESSION['NombreInm']   = $row_rsPass['NombreInm'];//18
        $_SESSION['iduser']      = $row_rsPass['iduser'];
        $_SESSION['auth']      = 1;
        $_SESSION['nvoSt']     = 1;
        
        
        //print_r($_SESSION['codUser']); echo "$existe <br> usuario autenticado"; //exit();
        
        ///validar usuarios metro y finca
         $conPortal1="select flag_portal
                      from clientessimi
                      where IdInmobiliaria='$idinmo'";
        //echo $conPortal;
        $resp1=$w_conexion->ResultSet($conPortal1);
        while($fp=$w_conexion->FilaSiguienteArray($resp1))  
        {
            $flag_portal = $fp['flag_portal'];
        }



          echo "Ingresando...";//.$_SESSION['Id_Usuarios']; //exit();

            if($flag_portal>0)
            {
               ?>
                <script>
                window.location.href="../mwc/simipedia/listadofilter_pqr.php";
                </script>
                <?php
            }
            else
            {
                  if($fte==1)  
                  {
                      ?>
                      <script>
                      window.location.href="../mwc/cargaArchivos.php";
                      </script>
                      <?php
                  }
                  else
                  {
                      ?>
                      <script>
                      window.location.href="../mwc/mainBoard.php";
                      </script>
                      <?php
                  }
              }
          }
       }
            
    }
    
    
    public function crearUsuario1($documento,$Login,$Clave,$Foto,
        $Estado,$Direccion,$IdEmpresa,$perfil,$IdTipoDocumento,
        $nombre1,$nombre2,$apellido1,$apellido2,$razon_social,
        $pais_u,$depto_u,$ciudad_u,$Nombres, $apellidos)
    {
        $Id_Usuarios=consecutivo('Id_Usuarios','usuarios');
        //echo $Id_Usuarios."---------------------------";
        if(($consulta=$this->db->prepare("INSERT INTO usuarios (Id_Usuarios,Login,Clave,Foto,
        Estado,Direccion,IdInmmo,perfil,IdTipoDocumento,
        nombre1,nombre2,apellido1,apellido2,razon_social,
        pais_u,depto_u,ciudad_u,Nombres, apellidos) 
        values(?,?,?,?,
        ?,?,?,?,?,
        ?,?,?,?,?,
        ?,?,?,?,?)
")))
    {   
      if(!$consulta->bind_param("iissisiiissssssssss",$Id_Usuarios,$Login,$Clave,$Foto,
            $Estado,$Direccion,$IdEmpresa,$perfil,$IdTipoDocumento,
            $nombre1,$nombre2,$apellido1,$apellido2,$razon_social,
            $pais_u,$depto_u,$ciudad_u,$Nombres, $apellidos))
      {
        echo "error bind";
      }
      else
      { 
        $consulta->execute();
//        echo "$qry";
        
        //if($consulta->bind_result($Login))
               //echo "El usuario $Login ha sido Creado";
               return 1;
      }
        }
    } 
    public function crearUsuario($documento,$Login,$Clave,$Foto,
        $Estado,$Direccion,$IdEmpresa,$perfil,$IdTipoDocumento,
        $nombre1,$nombre2,$apellido1,$apellido2,$razon_social,
        $pais_u,$depto_u,$ciudad_u,$Nombres, $apellidos)
     {
        $w_conexion = new MySQL();
        $id_user=consecutivo('iduser','usuarios');
        //echo $Id_Usuarios."---------------------------";
        $consulta="INSERT INTO usuarios (iduser,Id_Usuarios,Login,Clave,Foto,
        Estado,Direccion,IdInmmo,perfil,IdTipoDocumento,
        nombre1,nombre2,apellido1,apellido2,razon_social,
        pais_u,depto_u,ciudad_u,Nombres, apellidos) 
        values('$id_user','$documento','$documento','$Clave','$Foto',
            '$Estado','$Direccion','$IdEmpresa','$perfil','$IdTipoDocumento',
            '$nombre1','$nombre2','$apellido1','$apellido2','$razon_social',
            '$pais_u','$depto_u','$ciudad_u','$Nombres', '$apellidos')";
        
    if(!$res=$w_conexion->RecordSet($consulta))
    {
      echo "El Usuario Ya existe. Por favor confirme sus digite su usuario y clave";
    }
    else
    { 
      
           echo "El usuario $Login ha sido Creado <br>";
           return 1;
        }
        
    }
    public function fotoPerfil($data)
    {
      if(isset($_POST['imagebase64'])){
        $data = $_POST['imagebase64'];

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
            
        $data = base64_decode($data);
        $d=mt_rand(4,30);
         $data=file_put_contents('demo/'.$d.'.png', $data);
         return $data;
      }
    }
}
?>