<?php  
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
require_once('/home/simiinmobiliaria/public_html/PHPMailer_5.2.2/class.phpmailer.php');
require_once('/home/simiinmobiliaria/public_html/PHPMailer_5.2.2/class.smtp.php');
include("class.mailCard3.php");


class mailSender
{
	public function __construct($conn="",$pdo){
		$this->db=$conn;
		$this->pdo=$pdo;
	}
	public function envioMailCita($IdInmobiliaria,$body,$sendto,$sendCopy,$asunto)
	{
		$mailCard= New MailCard();
		$a=$datosMail=$mailCard->getCard($IdInmobiliaria);
		foreach($a as $key => $value)
		{}

		$mail             = new PHPMailer(); 
		$mail->IsSMTP();
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = "tls";
		$mail->Host       = $value['host_ml']; //Servidor de Correo 
		$mail->Port       = $value['port_ml'];
		$mail->Username   = $value['usr_ml'];//usuario perteneciente al servidor
		$mail->Password   = $value['pass_ml'];
		$mail->From       = $value['from_ml'];
		$mail->FromName   = "Agente Virtual SimiWeb";
		$mail->AddBCC($sendto, $sendto);
		if($sendCopy)
		{
			$mail->AddCC($sendCopy, $sendCopy);
		}
		//$mail->AddBCC("desarrolloweb@tae-ltda.com", "desarrolloweb@tae-ltda.com");
		// $mail->AddBCC("auxadmin@tae-ltda.com", "auxadmin@tae-ltda.com");
		$mail->Subject = $asunto;
		$mail->MsgHTML($body);
		$mail->IsHTML(true);
		$mail->SMTPDebug =0;
		$mail->Send();
	}


	public function envioMailCitaCal($IdInmobiliaria,$body,$sendto,$sendCopy,$asunto,$data,$datas,$mail)
	{
			//echo "llego hotmail";
			$mailCard= New MailCard();
			$a=$datosMail=$mailCard->getCard($IdInmobiliaria);
			foreach($a as $key => $value)
			{}

			$nombre="Promotor";
			$email=$sendto;

					
				
			$hor_act=$data["citaHora"];	

			$location = $datas["dirInmu"];
			$start_date = $data["citaFecha"]." ".$data["citaHora"];
			$time = date('H:i:s', strtotime($start_date.'+1 hour'));
			$end_date = $data["citaFecha"]." ".$time;
			
			$meetingstamp = strtotime($data["citaFecha"] . " UTC");     
			$meetingstampb = strtotime($end_date . " UTC");     
			$dtstart= gmdate("Ymd\THis\Z",$meetingstamp +(18000)); 
			$dtend= gmdate("Ymd\THis\Z",$meetingstampb +(18000));
			
			$description = "Codigo Inmueble: ".$inmueble."\nDireccion Inmueble: ".$datas["dirInmu"]."\nNombre Cliente: ".$nombrecliente."\nUbicacion: ".$direccion."\nCelular Cliente: ".$datas["telefonsCli"]."\nE-mail Cliente: ".$mailicliente."\nObservaciones: ".$data["citaObser"]."\n";
			
		
			$description = str_replace(array("\r\n", "\n"), array('=0D=0A', '=0D=0A'), $description);
			
			if( $mail == 1 or $mail == 3 ){

				$file_name = "google".microtime(true) .'.vcs';
				$path_file = "../mwc/citas/files/".$file_name;

				$text_vcs='BEGIN:VCALENDAR
					PRODID:-//Google Inc//Google Calendar 70.9054//EN
					VERSION:2.0
					CALSCALE:GREGORIAN
					METHOD:REQUEST
					BEGIN:VEVENT
					DTSTART:'.$dtstart.'
					DTEND:'.$dtend.'
					DTSTAMP:'.$todaystamp.'
					ORGANIZER;CN= prueba :mailto:'.$sendto.'
					UID:'.$cal_uid.'
					DESCRIPTION:'.$description.'
					LAST-MODIFIED:'.$todaystamp.'
					LOCATION:'.$direccion.'
					SEQUENCE:0
					STATUS:CONFIRMED
					SUMMARY:'.$asunto.'
					TRANSP:OPAQUE
					END:VEVENT
					END:VCALENDAR';

			}else{
				$file_name = "hotmail".microtime(true) .'.vcs';
				$path_file = "../mwc/citas/files/".$file_name;
				$text_vcs = '';
				$text_vcs .= "BEGIN:VCALENDAR"."\n";
				$text_vcs .= "PRODID:-//Microsoft Corporation//Outlook 11.0 MIMEDIR//EN"."\n";
				$text_vcs .= "VERSION:1.0"."\n";
				$text_vcs .= "PRODID:-//Microsoft Corporation//Outlook 11.0 MIMEDIR//EN"."\n";
				$text_vcs .= "VERSION:1.0"."\n";
				$text_vcs .= "BEGIN:VEVENT"."\n";
		
				$text_vcs .= "DTSTART;TZID='SA Pacific Standard Time':".$dtstart."\n";
				$text_vcs .= "DTEND;TZID='SA Pacific Standard Time':". $dtend ."\n";
				$text_vcs .= "LOCATION;ENCODING=QUOTED-PRINTABLE:". $direccion ."\n";
				$text_vcs .= "UID:040000008200E00074C5B7101A82E00800000000109E60CBFACAC7010000000000000000100"."\n";
				$text_vcs .= " 000004844976B382CE44C937F56D327FEE354"."\n";
				$text_vcs .= "DESCRIPTION;ENCODING=QUOTED-PRINTABLE:". $description ."=0D=0A"."\n";
				$text_vcs .= "SUMMARY;ENCODING=QUOTED-PRINTABLE: Cita programada.".$data["citaCodInm"]."\n";
				$text_vcs .= "END:VEVENT"."\n";
				$text_vcs .= "END:VCALENDAR"."\n";

			}
	


			/*$text_vcs = 'BEGIN:VCALENDAR
			VERSION:2.0
			CALSCALE:GREGORIAN
			METHOD:REQUEST
			BEGIN:VEVENT
			DTSTART:20170302T121000Z
			DTEND:20170302T131000Z
			DTSTAMP:20110525T075116Z
			ORGANIZER;CN=From Name:mailto:from email id
			UID:12345678
			ATTENDEE;PARTSTAT=NEEDS-ACTION;RSVP= TRUE;CN=Sample:mailto:sample@test.com
			DESCRIPTION:This is a test of iCalendar event invitation.
			LOCATION: Kochi
			SEQUENCE:0
			STATUS:CONFIRMED
			SUMMARY:Test iCalendar
			TRANSP:OPAQUE
			END:VEVENT
			END:VCALENDAR';*/
	
			ini_set('zlib.output_compression','Off');
	
			header('Pragma: public');
			header("Expires: Sat, 26 Jul 2017 05:00:00 GMT");
			header('Last-Modified: '. gmdate('D, d M Y H:i:s') . ' GMT');
			header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
			header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
			header("Pragma: no-cache");
			header("Expires: 0");
			header('Content-Transfer-Encoding: none');
			//header('Content-Type: application/force-download;');
			//header('Content-Disposition: attachment; filename="'. $file_name .'"');
			$fh = fopen ("../mwc/citas/files/$file_name", 'w');
			fwrite($fh, $text_vcs);
			fclose($fh);

			try
			{
				//echo "esto es asesor";
				//mail($mailicliente,$asunto,$cuerpo,$headers);
				$mailA2234 = new PHPMailer();
				$mailA2234->IsSMTP();
				$mailA2234->SMTPAuth = true;
				$mailA2234->SMTPSecure = "tls";
				$mailA2234->CharSet = 'UTF-8';
		/*		__________________________________________________________*/
				$mailA2234->Host = $value['host_ml']; //Servidor de Correo 
				$mailA2234->Port = $value['port_ml'];
				$mailA2234->Username = $value['usr_ml'];//usuario perteneciente al servidor
				$mailA2234->Password = $value['pass_ml'];
				$mailA2234->From = $value['from_ml'];
		/*		__________________________________________________________*/
				//$mailA2234->From = "no-reply@siminmueble.com";
				$mailA2234->FromName = "Agente Virtual SimiWeb";
				$mailA2234->Subject = $asunto." ";
				$mailA2234->MsgHTML($body);
				if( $mail == 2 )
				{
					$mailA2234->AddAttachment("../mwc/citas/files/".$file_name, "Outlook_event".microtime(true).".vcs"); 
				}

				if( $mail == 1 )
				{
					$mailA2234->AddAttachment("../mwc/citas/files/".$file_name, "Google_event".microtime(true).".vcs"); 
				}
				$mailA2234->AddAddress($sendto, $sendto);
				$mailA2234->AddAddress("andrescenit@hotmail.com", "andrescenit@hotmail.com");
				if($_SESSION['IdInmmo']==670 )
				{
					//$mailA2234->AddBCC("desarrolloweb@tae-ltda.com", "$mailcomercial");
				}
				// if($_POST['comercial']=="comercial")
				// {
				// 	$mailA2234->AddCC("$mailcomercial", "$mailcomercial");
				// 	if($_SESSION['IdInmmo']==1)
				// 	{
				// 		$mailA2234->AddBCC("adrianacamargo@tae-ltda.com", "adrianacamargo@tae-ltda.com");
				// 	}
				// 	//$mailA2234->AddCC("desarrolloweb@tae-ltda.com", "$mailcomercial");
				// }
				$mailA2234->SMTPDebug=2;
				$mailA2234->IsHTML(true);
				$mailA2234->Send();
			}
			catch(Error $err)
			{	
				print("No pudo ser enviado el correo");
			}


	}

	public function envioMailCitaGoogle($IdInmobiliaria,$body,$sendto,$sendCopy,$asunto,$data,$datas,$mail){

		
			$mailCard= New MailCard();
			$a=$datosMail=$mailCard->getCard($IdInmobiliaria);
			foreach($a as $key => $value)
			{}

			$nombre="Promotor";
			$email=$sendto;


					
				
			$hor_act=$data["citaHora"];	

			$location = $datas["dirInmu"];
			$start_date = $data["citaFecha"]." ".$data["citaHora"];
			$time = date('H:i:s', strtotime($start_date.'+1 hour'));
			$end_date = $data["citaFecha"]." ".$time;
			
			$meetingstamp = strtotime($data["citaFecha"] . " UTC");     
			$meetingstampb = strtotime($end_date . " UTC");     
			$dtstart= gmdate("Ymd\THis\Z",$meetingstamp +(18000)); 
			$dtend= gmdate("Ymd\THis\Z",$meetingstampb +(18000));
			
			$description = "Codigo Inmueble: ".$datas["citaCodInm"]."\nDireccion Inmueble: ".$datas["dirInmu"]."\nNombre Cliente: ".$data["nombreCliente"]."\nUbicacion: ".$datas["dirInmu"]."\nCelular Cliente: ".$datas["telefonsCli"]."\nE-mail Cliente: ".$mailicliente."\nObservaciones: ".$data["citaObser"]."\n";
			$summary = "Codigo Inmueble: ".$datas["citaCodInm"].", Nombre Cliente: ".$datas["nombreCliente"].", Dirección Inmueble: ".$datas["dirInmu"].", Celular Cliente: ".$datas["telefonsCli"].", E-mail Cliente: ".$datas["mailCliente"].", Observaciones: ".$data["citaObser"].", ";
		
			//$description = str_replace(array("\r\n", "\n"), array('=0D=0A', '=0D=0A'), $description);

			$mailA2234 = new PHPMailer();
			$mailA2234->IsSMTP();
			$mailA2234->SMTPAuth = true;
			$mailA2234->SMTPSecure = "tls";
			$mailA2234->CharSet = 'UTF-8';
	/*		__________________________________________________________*/
			$mailA2234->Host = $value['host_ml']; //Servidor de Correo 
			$mailA2234->Port = $value['port_ml'];
			$mailA2234->Username = $value['usr_ml'];//usuario perteneciente al servidor
			$mailA2234->Password = $value['pass_ml'];
			$mailA2234->From = $value['from_ml'];
	/*		__________________________________________________________*/
			//$mailA2234->From = "no-reply@siminmueble.com";
			$mailA2234->FromName = "Agente Virtual SimiWeb";
			$mailA2234->Subject = " Nueva cita asignada ";
			$mailA2234->MsgHTML($body);
			$cal_uid = date('Ymd').'T'.date('His')."-".rand()."@google.com"; 
			$cal_uid = $datas["citaCodInm"]."".$data["idcita"]."@google.com"; 
			$cal_uid = date('Ymd').date('His');

			if( $mail == 1 )
			{
				//$mailA2234->AddAttachment("../mwc/citas/files/google1484172065.22.ics"); 
				$ical ='BEGIN:VCALENDAR
PRODID:-//Google Inc//Google Calendar 70.9054//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:REQUEST
BEGIN:VEVENT
DTSTART:20170111T140000
DTEND:20170111T145900
DTSTAMP:20130410T110314Z
ORGANIZER;CN='.$value['usr_ml'].':MAILTO:'.$value['usr_ml'].'
UID:'.$cal_uid.'@simiinmobiliarias.com
ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN='.$sendto.';X-NUM-GUESTS=0:MAILTO:'.$sendto.'
CREATED:20130410T110314Z
DESCRIPTION:
    '.$description.'
LAST-MODIFIED:20170111T140000
LOCATION:'.$datas["dirInmu"].'
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:'.$summary.'
TRANSP: OPAQUE
END:VEVENT
END:VCALENDAR';
				$mailA2234->AddStringAttachment($ical, "event.ics", "7bit", "text/calendar; charset=utf-8; method=REQUEST");

			}
			
			$mailA2234->AddAddress($sendto, $sendto);
			//$mailA2234->AddAddress("oscalber22@gmail.com", "oscalber22@gmail.com");
			//$mailA2234->AddAddress("andrescenit@hotmail.com", "andrescenit@hotmail.com");
			if($_SESSION['IdInmmo']==670 )
			{
				//$mailA2234->AddBCC("desarrolloweb@tae-ltda.com", "$mailcomercial");
			}
			// if($_POST['comercial']=="comercial")
			// {
			// 	$mailA2234->AddCC("$mailcomercial", "$mailcomercial");
			// 	if($_SESSION['IdInmmo']==1)
			// 	{
			// 		$mailA2234->AddBCC("adrianacamargo@tae-ltda.com", "adrianacamargo@tae-ltda.com");
			// 	}
			// 	//$mailA2234->AddCC("desarrolloweb@tae-ltda.com", "$mailcomercial");
			// }
			$mailA2234->SMTPDebug=2;
			$mailA2234->IsHTML(true);
			$mailA2234->Send();


	}

	public function envioMailCitaCal2($IdInmobiliaria,$body,$sendto,$sendCopy,$asunto,$data,$datas,$mail){
		$mailCard= New MailCard();
		$a=$datosMail=$mailCard->getCard($IdInmobiliaria);
		foreach($a as $key => $value)
		{}

		// event params
		$event_id = 1234;
		$sequence = 0;
		$status = 'TENTATIVE';//'TENTATIVE', 'CONFIRMED' or 'CANCELLED'. 
		$summary = 'Summary of the event';
		$venue = 'Simbawanga';
		$start = '20170820';
		$start_time = '160630';
		$end = '20170820';
		$end_time = '180630';

		//PHPMailer
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		//$mail->SMTPAuth = true;
		$mail->SMTPSecure = "tls";
		$mail->CharSet = 'UTF-8';
/*		__________________________________________________________*/
		$mail->Host = $value['host_ml']; //Servidor de Correo 
		$mail->Port = $value['port_ml'];
		$mail->Username = $value['usr_ml'];//usuario perteneciente al servidor
		$mail->Password = $value['pass_ml'];
		$mail->SMTPAuth = false;
		$mail->IsHTML(false);
		$mail->setFrom($value['usr_ml'], $value['pass_ml']);
		$mail->addReplyTo('andrescenit@hotmail.com', 'andrescenit@hotmail.com');
		$mail->addAddress('oscalber_22@hotmail.com','oscalber_22@hotmail.com');
		$mail->ContentType = 'text/calendar';


		$mail->Subject = "Outlooked Event";
		$mail->addCustomHeader('MIME-version',"1.0");
		$mail->addCustomHeader('Content-type',"text/calendar; method=REQUEST; charset=UTF-8");
		$mail->addCustomHeader('Content-Transfer-Encoding',"7bit");
		$mail->addCustomHeader('X-Mailer',"Microsoft Office Outlook 12.0");
		$mail->addCustomHeader("Content-class: urn:content-classes:calendarmessage");

		$ical = "BEGIN:VCALENDAR\r\n";
		$ical .= "VERSION:2.0\r\n";
		$ical .= "PRODID:-//YourCassavaLtd//EateriesDept//EN\r\n";
		$ical .= "METHOD:REQUEST\r\n";
		$ical .= "BEGIN:VEVENT\r\n";
		$ical .= "ORGANIZER;SENT-BY=\"MAILTO:andrescenit@hotmail.com\":MAILTO:oscalber_22@hotmail.com\r\n";
		$ical .= "ATTENDEE;CN=them@kaserver.com;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;RSVP=TRUE:mailto:organizer@kaserver.com\r\n";
		$ical .= "UID:".strtoupper(md5($event_id))."-kaserver.com\r\n";
		$ical .= "SEQUENCE:".$sequence."\r\n";
		$ical .= "STATUS:".$status."\r\n";
		$ical .= "DTSTAMPTZID=Africa/Nairobi:".date('Ymd').'T'.date('His')."\r\n";
		$ical .= "DTSTART:".$start."T".$start_time."\r\n";
		$ical .= "DTEND:".$end."T".$end_time."\r\n";
		$ical .= "LOCATION:".$venue."\r\n";
		$ical .= "SUMMARY:".$summary."\r\n";
		$ical .= "DESCRIPTION:".$event['description']."\r\n";
		$ical .= "BEGIN:VALARM\r\n";
		$ical .= "TRIGGER:-PT15M\r\n";
		$ical .= "ACTION:DISPLAY\r\n";
		$ical .= "DESCRIPTION:Reminder\r\n";
		$ical .= "END:VALARM\r\n";
		$ical .= "END:VEVENT\r\n";
		$ical .= "END:VCALENDAR\r\n";


		$mail->Body = $ical;

		echo "sdfdsfdsfd";
		//send the message, check for errors
		if(!$mail->send()) {
		echo  "Mailer Error: " . $mail->ErrorInfo;
		//return false;
		} else {
		echo "Message sent!";
		//return true;
		}

	}

	public function envioMailIframeNuevo($IdInmobiliaria,$body,$sendto,$sendCopy,$asunto)
	{
		$mailCard= New MailCard();
		$a=$datosMail=$mailCard->getCard($IdInmobiliaria);
		foreach($a as $key => $value)
		{}

		$mail             = new PHPMailer(); 
		$mail->IsSMTP();
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = "tls";
		$mail->Host       = $value['host_ml']; //Servidor de Correo 
		$mail->Port       = $value['port_ml'];
		$mail->Username   = $value['usr_ml'];//usuario perteneciente al servidor
		$mail->Password   = $value['pass_ml'];
		$mail->From       = $value['from_ml'];
		$mail->FromName   = $asunto;
		$mail->AddBCC($sendto, $sendto);
		if($sendCopy)
		{
			$mail->AddCC($sendCopy, $sendCopy);
		}
		//$mail->AddBCC("desarrolloweb@tae-ltda.com", "desarrolloweb@tae-ltda.com");
		// $mail->AddBCC("auxadmin@tae-ltda.com", "auxadmin@tae-ltda.com");
		$mail->Subject = $asunto;
		$mail->MsgHTML($body);
		$mail->IsHTML(true);
		$mail->SMTPDebug =0;
		$mail->Send();
	}


}

?>