<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
include("../funciones/fncfields2.php");
include("../funciones/fncfields3.php");
include("../funciones/cge00001.php");
include("../funciones/connPDO.php");
include("../funciones/conni.php");
include("class.mailpqr.php");
include("../funciones/myfncs.php");
$w_conexion = new MySQL();

if($_GET["data"]){
	$data = $_GET["data"];	
}else{
	$data = 0;
}


class NotificacionesEnvio
{

	public function __construct($conn=''){
		$this->db=$conn;
	}

	public function validaTicketsVencidos()
 	{

 		$connPDO= new Conexion();
		$stmt = $connPDO->prepare("SELECT conse_pet,modulo_p,nivel_p,abuelo_p,padre_p,hijo_p,conse_p,esta_pet,tipop,usu_pet,fecha_fin FROM peticiones WHERE esta_pet = 1 and fecha_fin < '".date("Y-m-d H:m:s")."' AND flagsmp = 1 AND email_enviado = 0"  );

		if($stmt->execute()){
			while ( $row = $stmt->fetch()) {
				
				if($row["fecha_fin"] != null){

					if( $row["fecha_fin"] < date("Y-m-d H:m:s")){
						//echo "<strong>".$row["fecha_fin"]."</strong><br>";
						//echo "ticket pendiente: ".$row["conse_pet"]."<br>";
						$mailPqr = new mailPqr($conn);
						$mailPqr->envioMailTicketPendiente($row["conse_pet"],"");
					}else{
						//echo "".$row["fecha_fin"]."<br>";
					}
					
				}

				$tarea = $row["conse_pet"];
				$stmt=$connPDO->prepare("UPDATE peticiones
										 SET email_enviado = 1
										 WHERE conse_pet = :conse_pet");
				if($stmt->execute(array(
						":conse_pet"   => $tarea
				))){}

			}
			return $response;
		}else{
			print_r($stmt->errorInfo());
		}

 	}

 	public function validaDevolverLlamada(){

 		$connPDO= new Conexion();
		$stmt = $connPDO->prepare("SELECT fdev,hdev,codpqr FROM seg_peticiones WHERE  fdev < '".date("Y-m-d")."' AND hdev < '".date("H:m:s")."' AND email_enviado = 1 "  );

		if($stmt->execute()){
			while ( $row = $stmt->fetch()) {
				//echo $row["codpqr"]."<br>";
				$mailPqr->envioMailLlamadaPendiente($row["codpqr"],"");
			}
		}

 	}

}

$notificaciones = new NotificacionesEnvio();
//descomentar el envio del correo a coordinador, gerente y presidente
//quitar el AND conse_pet = 8508 de la consulta
//echo "hora actual: ".date("Y-m-d H:m:s")."<br><br>";

//validar tickets vencidos
if( $data == 1 ){
	$notificaciones->validaTicketsVencidos();
}

//validar si el tiempo para realizar la llamada se ha cumplido
if( $data == 2 ){
	$notificaciones->validaDevolverLlamada();
}

?>