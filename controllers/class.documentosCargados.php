<?php

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

class documentosCargados
{

    public function __construct($connPDO, $connPDOold)
    {
        $this->connPDO = $connPDO;

        $this->connPDOold = $connPDOold;
    }

    public function listadoFacturas($data)
    {
        $aColumns = array("sucursal", "Nombre", "Fecha", "idFactura", "IdCedula", "inmu", "Total");

        $meses = array('', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

        $sWhere = "";
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($data['search']) && $data["search"]["value"] != '') {
                if ($sWhere == "") {
                    $sWhere = " AND (";
                } else {
                    $sWhere .= " OR ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($data["search"]["value"]) . "%' ";
            }
        }

        $sLimit = " LIMIT 0,10 ";
        if (isset($data['start']) && $data['length'] != '-1') {
            $sLimit = " LIMIT " . intval($data['start']) . ", " .
            intval($data['length']);
        }
        $order = " ORDER BY " . $aColumns[$data['order'][0]['column'] - 1] . " " . $data['order'][0]['dir'];

        if (!empty($data['sucursal'])) {
            $cond .= " AND f.sucursal = " . $data['sucursal'];
        }

        if (!empty($data['idestadofrom']) || !empty($data['idestadoto'])) {
            if (!empty($data['idestadofrom']) && empty($data['idestadoto'])) {
                $cond .= " AND f.idFactura BETWEEN " . $data['idestadofrom'] . " AND 9999999999";
            } elseif (empty($data['idestadofrom']) && !empty($data['idestadoto'])) {
                $cond .= " AND f.idFactura BETWEEN 0 AND " . $data['idestadoto'];
            } elseif (!empty($data['idestadofrom']) && !empty($data['idestadoto'])) {
                $cond .= " AND f.idFactura BETWEEN " . $data['idestadofrom'] . " AND " . $data['idestadoto'];
            }
        }

        if ($sWhere != "") {
            $sWhere .= ")";
        } else {
            $sWhere .= $cond;
        }
        $sql = "select f.Nombre,f.Fecha,f.idFactura,f.inmu,f.Total,f.IdCedula,f.sucursal,f.NoInm,a.correo from factura as f, arrendatario as a where NoInm = :inmo and Fecha between :f1 and :f2 and f.IdCedula = a.nit $sWhere $order $sLimit";

        $f1 = $data['anio'] . "-" . $data['mes'] . "-01";
        $f2 = $data['anio'] . "-" . $data['mes'] . "-31";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':inmo', $data['inmo']);
        $stmt->bindParam(':f1', $f1);
        $stmt->bindParam(':f2', $f2);
        $plantillafactura = $this->getCampo('clientessimi', "where IdInmobiliaria = " . $data['inmo'], 'plantillafac', 0);

        if ($stmt->execute()) {
            $this->connPDO->exec("charset='utf-8'");
            if ($stmt->rowCount() > 0) {
                $datas = array();
                while ($row = $stmt->fetch()) {
                    $Nombre    = str_replace("Ñ", "ñ", ucwords(strtolower(utf8_encode($row['Nombre']))));
                    $Fecha     = $row['Fecha'];
                    $idFactura = $row['idFactura'];
                    $inmu      = $row['inmu'];
                    $Total     = $row['Total'];
                    $IdCedula  = $row['IdCedula'];
                    $sucursal  = $row['sucursal'];

                    if (trim($row['correo']) !== "" && $this->validarEmail(trim($row['correo'])) === true || $this->validarVariosEmails(trim($row['correo']))) {
                        $btnsend  = "<button data-toggle='tooltip' data-placement='top' title='Enviar Correo' id='" . utf8_encode(trim($row['IdCedula'])) . "' class='sendemail btn btn-success btn-xs' data-nroestado='" . utf8_encode(trim($row['IdCedula'])) . "' data-email='" . utf8_encode(trim($row['correo'])) . "'><i class='fa fa-paper-plane-o' aria-hidden='true'></i></button>";
                        $checkbox = '<input type="checkbox" id="IdCedula" name="id[]" value="' . utf8_encode(trim($IdCedula)) . '" >';
                    } else {
                        $btnsend  = "";
                        $checkbox = "";
                    }

                    $btnchangeclave = "<button data-toggle='tooltip' data-nroestado='" . utf8_encode(trim($row['IdCedula'])) . "' data-inmobiliaria='" . $row['NoInm'] . "' data-placement='top' title='Cambiar Clave' class='btn btn-primary btn-xs changepass'><i class='fa fa-key' aria-hidden='true'></i></button>";

                    $btnviewhistorial = "<button data-toggle='tooltip' data-placement='top' title='Ver historial' class='btn btn-info btn-xs viewHistorial' data-fecha='" . $data . "' data-nroestado='" . utf8_encode(trim($row['IdCedula'])) . "' data-mes='" . $meses[$data['mes']] . "' data-ano='" . $data['anio'] . "' data-inmobiliaria='" . $row['NoInm'] . "'><i class='fa fa-history' aria-hidden='true'></i></button>";

                    $datas[] = array(
                        'checkbox'      => $checkbox,
                        'Nombre'        => $Nombre,
                        'Fecha'         => $Fecha,
                        'idFactura'     => $idFactura,
                        'inmu'          => $inmu,
                        'Total'         => $Total,
                        'IdCedula'      => $IdCedula,
                        'sucursal'      => $sucursal,
                        "linkdocumento" => "<a data-toggle='tooltip' data-placement='top' title='Ver Documento' class='btn btn-primary btn-xs' target='_Blank' href='http://www.simiinmobiliarias.com/mcomercialweb/facturas/" . $plantillafactura . "?cod_estado=" . $idFactura . "&f_corte=" . $Fecha . "&codinmo=" . $data['inmo'] . "&inmu=" . $inmu . "&sucursal=" . $sucursal . "'><i class='fa fa-file-pdf-o' aria-hidden='true'></i></a> $btnviewhistorial $btnsend $btnchangeclave",
                    );
                }
            }

            $Ssql = "select count(f.idFactura) as total from factura as f, arrendatario as a where NoInm = :inmo and Fecha between :f1 and :f2 and f.IdCedula = a.nit $sWhere";
            // echo '<pre>';var_dump($Ssql);die;
            $stmtcount = $this->connPDO->prepare($Ssql);
            $stmtcount->bindParam(':inmo', $data['inmo']);
            $stmtcount->bindParam(':f1', $f1);
            $stmtcount->bindParam(':f2', $f2);
            $stmtcount->execute();
            $rResultTotal = $stmtcount->fetchAll();
            $iTotal       = $rResultTotal[0]["total"];
            $output       = array(
                "sEcho"                => intval($data['sEcho']),
                "iTotalRecords"        => $iTotal,
                "iTotalDisplayRecords" => $iTotal, //$iFilteredTotal[0],
                "aaData"               => $datas,
            );
            return $output;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;

    }

    public function listadoRecibos($data)
    {
        $aColumns = array("b.Arrendatario", "b.Cedula", "b.Mes", "b.Ano", "b.Inmueble", "b.Referencia", "b.Total");

        $meses = array('', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

        $sWhere = "";
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($data['search']) && $data["search"]["value"] != '') {
                if ($sWhere == "") {
                    $sWhere = " AND (";
                } else {
                    $sWhere .= " OR ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($data["search"]["value"]) . "%' ";
            }
        }

        $sLimit = " LIMIT 0,10 ";
        if (isset($data['start']) && $data['length'] != '-1') {
            $sLimit = " LIMIT " . intval($data['start']) . ", " .
            intval($data['length']);
        }
        $order = " ORDER BY " . $aColumns[$data['order'][0]['column'] - 1] . " " . $data['order'][0]['dir'];

        if (!empty($data['referenciafrom']) || !empty($data['referenciato'])) {
            if (!empty($data['referenciafrom']) && empty($data['referenciato'])) {
                $cond .= " AND b.Referencia BETWEEN " . $data['referenciafrom'] . " AND 9999999999999999999";
            } elseif (empty($data['referenciafrom']) && !empty($data['referenciato'])) {
                $cond .= " AND b.Referencia BETWEEN 0 AND " . $data['referenciato'];
            } elseif (!empty($data['referenciafrom']) && !empty($data['referenciato'])) {
                $cond .= " AND b.Referencia BETWEEN " . $data['referenciafrom'] . " AND " . $data['referenciato'];
            }
        }

        if ($sWhere != "") {
            $sWhere .= ")";
            $sWhere .= $cond;
        } else {
            $sWhere .= $cond;
        }
        $sql = "select b.Referencia,b.Arrendatario,b.Mes,b.Ano,b.Inmueble,b.Cedula,b.IdFactura,b.inmobiliaria,a.correo from barras as b, arrendatario as a where inmobiliaria = :inmo and Mes = :mes and Ano = :ano and a.nit = b.Cedula AND a.inmo = b.inmobiliaria AND a.inm = b.Inmueble $sWhere $order $sLimit";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':inmo', $data['inmo']);
        $stmt->bindParam(':mes', $data['mes']);
        $stmt->bindParam(':ano', $data['anio']);
        $plantillafactura = $this->getCampo('anotaciones_barras', "where id_inmo = " . $data['inmo'], 'plantilla', 0);

        if ($stmt->execute()) {
            $this->connPDO->exec("charset='utf-8'");
            if ($stmt->rowCount() > 0) {
                $datas = array();
                while ($row = $stmt->fetch()) {
                    $Arrendatario = str_replace("Ñ", "ñ", ucwords(strtolower(utf8_encode($row['Arrendatario']))));
                    $Cedula       = $row['Cedula'];
                    $Mes          = $meses[$row['Mes']];
                    $Ano          = $row['Ano'];
                    $IdFactura    = $row['IdFactura'];
                    $Inmueble     = $row['Inmueble'];
                    $Referencia   = $row['Referencia'];

                    if (trim($row['correo']) !== "" && $this->validarEmail(trim($row['correo'])) === true || $this->validarVariosEmails(trim($row['correo']))) {
                        $btnsend  = "<button data-toggle='tooltip' data-placement='top' title='Enviar Correo' id='" . utf8_encode(trim($row['nit'])) . "' class='sendemail btn btn-success btn-xs' data-nroestado='" . utf8_encode(trim($row['Cedula'])) . "' data-email='" . utf8_encode(trim($row['correo'])) . "'><i class='fa fa-paper-plane-o' aria-hidden='true'></i></button>";
                        $checkbox = '<input type="checkbox" id="IdCedula" name="id[]" value="' . utf8_encode(trim($Cedula)) . '" >';
                    } else {
                        $btnsend  = "";
                        $checkbox = "";
                    }

                    $btnchangeclave = "<button data-toggle='tooltip' data-nroestado='" . utf8_encode(trim($row['Cedula'])) . "' data-inmobiliaria='" . $row['inmobiliaria'] . "' data-placement='top' title='Cambiar Clave' class='btn btn-primary btn-xs changepass'><i class='fa fa-key' aria-hidden='true'></i></button>";

                    $btnviewhistorial = "<button data-toggle='tooltip' data-placement='top' title='Ver historial' class='btn btn-info btn-xs viewHistorial' data-fecha='" . $data . "' data-nroestado='" . utf8_encode(trim($row['Cedula'])) . "' data-mes='" . $meses[$row['Mes']] . "' data-ano='" . $row['Ano'] . "' data-inmobiliaria='" . $row['inmobiliaria'] . "'><i class='fa fa-history' aria-hidden='true'></i></button>";

                    $datas[] = array(
                        'checkbox'      => $checkbox,
                        'Arrendatario'  => $Arrendatario,
                        'Cedula'        => $Cedula,
                        'Mes'           => $Mes,
                        'Ano'           => $Ano,
                        'IdFactura'     => $IdFactura,
                        'Inmueble'      => $Inmueble,
                        'Referencia'    => $Referencia,
                        "linkdocumento" => "<a data-toggle='tooltip' data-placement='top' title='Ver Documento' class='btn btn-primary btn-xs' target='_Blank' href='http://www.simiinmobiliarias.com/facts/" . $plantillafactura . "?ref=" . $Referencia . "'><i class='fa fa-file-pdf-o' aria-hidden='true'></i></a> $btnviewhistorial $btnsend $btnchangeclave",
                    );
                }
            } else {
                $datas[] = array(
                    'checkbox'      => '',
                    'Arrendatario'  => '',
                    'Cedula'        => '',
                    'Mes'           => '',
                    'Ano'           => '',
                    'IdFactura'     => '',
                    'Inmueble'      => '',
                    'Referencia'    => '',
                    "linkdocumento" => "",
                );
            }

            $Ssql = "select count(b.Referencia) as total from barras as b where inmobiliaria = :inmo and Mes = :mes and Ano = :ano $sWhere";
            // echo '<pre>';var_dump($Ssql);die;
            $stmtcount = $this->connPDO->prepare($Ssql);
            $stmtcount->bindParam(':inmo', $data['inmo']);
            $stmtcount->bindParam(':mes', $data['mes']);
            $stmtcount->bindParam(':ano', $data['anio']);
            $stmtcount->execute();
            $rResultTotal = $stmtcount->fetchAll();
            $iTotal       = $rResultTotal[0]["total"];
            $output       = array(
                "sEcho"                => intval($data['sEcho']),
                "iTotalRecords"        => $iTotal,
                "iTotalDisplayRecords" => $iTotal, //$iFilteredTotal[0],
                "aaData"               => $datas,
            );
            return $output;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;

    }
    public function listadoBarrasFactura($data)
    {
        $aColumns = array("Arrendatario", "Cedula", "Mes", "Ano", "IdFactura", "Inmueble", "Referencia", "Total");

        $meses = array('', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

        $sWhere = "";
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($data['search']) && $data["search"]["value"] != '') {
                if ($sWhere == "") {
                    $sWhere = " AND (";
                } else {
                    $sWhere .= " OR ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($data["search"]["value"]) . "%' ";
            }
        }

        $sLimit = " LIMIT 0,10 ";
        if (isset($data['start']) && $data['length'] != '-1') {
            $sLimit = " LIMIT " . intval($data['start']) . ", " .
            intval($data['length']);
        }
        $order = " ORDER BY " . $aColumns[$data['order'][0]['column'] - 1] . " " . $data['order'][0]['dir'];

        if ($sWhere != "") {
            $sWhere .= ")";
        } else {
            $sWhere .= $cond;
        }
        $sql = "select Referencia,Arrendatario,Mes,Ano,Inmueble,Cedula,IdFactura,inmobiliaria from barras where inmobiliaria = :inmo and Mes = :mes and Ano = :ano $sWhere $order $sLimit";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':inmo', $data['inmo']);
        $stmt->bindParam(':mes', $data['mes']);
        $stmt->bindParam(':ano', $data['anio']);
        $plantillafactura = $this->getCampo('anotaciones_barras', "where id_inmo = " . $data['inmo'], 'plantilla', 0);

        if ($stmt->execute()) {
            $this->connPDO->exec("charset='utf-8'");
            if ($stmt->rowCount() > 0) {
                $datas = array();
                while ($row = $stmt->fetch()) {
                    $Arrendatario = str_replace("Ñ", "ñ", ucwords(strtolower($row['Arrendatario'])));
                    $Cedula       = $row['Cedula'];
                    $Mes          = $meses[$row['Mes']];
                    $Ano          = $row['Ano'];
                    $IdFactura    = $row['IdFactura'];
                    $Inmueble     = $row['Inmueble'];
                    $Referencia   = $row['Referencia'];

                    $btnchangeclave = "<button data-toggle='tooltip' data-nroestado='" . utf8_encode(trim($row['Cedula'])) . "' data-inmobiliaria='" . $row['inmobiliaria'] . "' data-placement='top' title='Cambiar Clave' class='btn btn-primary btn-xs changepass'><i class='fa fa-key' aria-hidden='true'></i></button>";

                    $btnviewhistorial = "<button data-toggle='tooltip' data-placement='top' title='Ver historial' class='btn btn-info btn-xs viewHistorial' data-fecha='" . $data . "' data-nroestado='" . utf8_encode(trim($row['IdFactura'])) . "' data-inmobiliaria='" . $row['inmobiliaria'] . "'><i class='fa fa-history' aria-hidden='true'></i></button>";

                    $datas[] = array(
                        'checkbox'      => '<input type="checkbox" id="IdCedula" name="id[]" value="' . $IdCedula . '" >',
                        'Arrendatario'  => $Arrendatario,
                        'Cedula'        => $Cedula,
                        'Mes'           => $Mes,
                        'Ano'           => $Ano,
                        'IdFactura'     => $IdFactura,
                        'Inmueble'      => $Inmueble,
                        'Referencia'    => $Referencia,
                        "linkdocumento" => "<a data-toggle='tooltip' data-placement='top' title='Ver Documento' class='btn btn-primary btn-xs' target='_Blank' href='http://www.simiinmobiliarias.com/facts/" . $plantillafactura . "?ref=" . $Referencia . "'><i class='fa fa-file-pdf-o' aria-hidden='true'></i></a> $btnviewhistorial $btnchangeclave",
                    );
                }
            } else {
                $datas[] = array(
                    'checkbox'      => '',
                    'Arrendatario'  => '',
                    'Cedula'        => '',
                    'Mes'           => '',
                    'Ano'           => '',
                    'IdFactura'     => '',
                    'Inmueble'      => '',
                    'Referencia'    => '',
                    "linkdocumento" => "",
                );
            }

            $Ssql = "select count(Referencia) as total from barras where inmobiliaria = :inmo and Mes = :mes and Ano = :ano $sWhere";
            // echo '<pre>';var_dump($Ssql);die;
            $stmtcount = $this->connPDO->prepare($Ssql);
            $stmtcount->bindParam(':inmo', $data['inmo']);
            $stmtcount->bindParam(':mes', $data['mes']);
            $stmtcount->bindParam(':ano', $data['anio']);
            $stmtcount->execute();
            $rResultTotal = $stmtcount->fetchAll();
            $iTotal       = $rResultTotal[0]["total"];
            $output       = array(
                "sEcho"                => intval($data['sEcho']),
                "iTotalRecords"        => $iTotal,
                "iTotalDisplayRecords" => $iTotal, //$iFilteredTotal[0],
                "aaData"               => $datas,
            );
            return $output;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;

    }

    public function listadoEstados($data)
    {
        $aColumns = array("e.id_estados","e.inmo", "p.nombre", "e.fini", "e.ffin", "e.inm", "e.nit");

        $meses = array('', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

        $cond = "";

        $sWhere = "";
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($data['search']) && $data["search"]["value"] != '') {
                if ($sWhere == "") {
                    $sWhere = " AND (";
                } else {
                    $sWhere .= " OR ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($data["search"]["value"]) . "%' ";
            }
        }

        $sLimit = " LIMIT 0,10 ";
        if (isset($data['start']) && $data['length'] != '-1') {
            $sLimit = " LIMIT " . intval($data['start']) . ", " .
            intval($data['length']);
        }
        $order = " ORDER BY " . $aColumns[$data['order'][0]['column'] - 1] . " " . $data['order'][0]['dir'];

        // if (!empty($data['sucursal'])) {
        //     $cond .= " AND e.inmo = " . $data['sucursal'];
        // }

        if (!empty($data['idestadofrom']) || !empty($data['idestadoto'])) {
            if (!empty($data['idestadofrom']) && empty($data['idestadoto'])) {
                $cond .= " AND e.id_estados BETWEEN " . $data['idestadofrom'] . " AND 9999999999";
            } elseif (empty($data['idestadofrom']) && !empty($data['idestadoto'])) {
                $cond .= " AND e.id_estados BETWEEN 0 AND " . $data['idestadoto'];
            } elseif (!empty($data['idestadofrom']) && !empty($data['idestadoto'])) {
                $cond .= " AND e.id_estados BETWEEN " . $data['idestadofrom'] . " AND " . $data['idestadoto'];
            }
        }

        if ($sWhere != "") {
            $sWhere .= ")";
            $sWhere .= $cond;
        } else {
            $sWhere .= $cond;
        }
        $sql = "select e.id_estados, e.nit,p.nombre,p.mail_pr,e.fini,e.ffin,e.inm,e.inmo,e.mes,e.ano,e.inmo from estados e ,propietario p where e.nit= p.nit and p.inmueble=e.inm and e.mes=:mes and e.ano=:ano and e.inmo=:inmo $sWhere group by e.inm,p.nit $order $sLimit";

        // echo "<pre>";
        // print_r($sql);
        // echo "</pre>";die;

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':inmo', $data['inmo']);
        $stmt->bindParam(':mes', $meses[$data['mes']]);
        $stmt->bindParam(':ano', $data['anio']);
        $plantillafactura = $this->getCampo('clientessimi', "where IdInmobiliaria = " . $data['inmo'], 'plantillaestc', 0);

        if ($stmt->execute()) {
            $this->connPDO->exec("charset='utf-8'");
            if ($stmt->rowCount() > 0) {
                $datas = array();
                while ($row = $stmt->fetch()) {
                    $Propietario  = str_replace("Ñ", "ñ", ucwords(strtolower(utf8_encode($row['nombre']))));
                    $IdEstado     = $row['id_estados'];
                    $Sucursal     = $row['inmo'];
                    $CorteInicial = $row['fini'];
                    $CorteFinal   = $row['ffin'];
                    $Inmueble     = $row['inm'];
                    $Documento    = $row['nit'];

                    if (trim($row['mail_pr']) !== "" && $this->validarEmail(trim($row['mail_pr'])) === true || $this->validarVariosEmails(trim($row['mail_pr']))) {
                        $btnsend  = "<button data-toggle='tooltip' data-placement='top' title='Enviar Correo' id='" . utf8_encode(trim($row['nit'])) . "' class='sendemail btn btn-success btn-xs' data-nroestado='" . utf8_encode(trim($row['nit'])) . "' data-email='" . utf8_encode(trim($row['mail_pr'])) . "'><i class='fa fa-paper-plane-o' aria-hidden='true'></i></button>";
                        $checkbox = '<input type="checkbox" id="IdCedula" name="id[]" value="' . utf8_encode(trim($Documento)) . '" >';
                    } else {
                        $btnsend  = "";
                        $checkbox = "";
                    }

                    $btnchangeclave = "<button data-toggle='tooltip' data-nroestado='" . utf8_encode(trim($row['nit'])) . "' data-inmobiliaria='" . $row['inmo'] . "' data-placement='top' title='Cambiar Clave' class='btn btn-primary btn-xs changepass'><i class='fa fa-key' aria-hidden='true'></i></button>";

                    $btnviewhistorial = "<button data-toggle='tooltip' data-placement='top' title='Ver historial' class='btn btn-info btn-xs viewHistorial' data-fecha='" . $data . "' data-nroestado='" . utf8_encode(trim($row['nit'])) . "' data-mes='" . $row['mes'] . "' data-ano='" . $row['ano'] . "' data-inmobiliaria='" . $row['inmo'] . "'><i class='fa fa-history' aria-hidden='true'></i></button>";

                    $datas[] = array(
                        'checkbox'      => $checkbox,
                        'IdEstado'   => $IdEstado,
                        'Propietario'   => $Propietario,
                        'CorteInicial'  => $CorteInicial,
                        'CorteFinal'    => $CorteFinal,
                        'Inmueble'      => $Inmueble,
                        'Documento'     => $Documento,
                        "linkdocumento" => "<a data-toggle='tooltip' data-placement='top' title='Ver Documento' class='btn btn-primary btn-xs' target='_Blank' href='http://www.simiinmobiliarias.com/mcomercialweb/estados_cta/" . $plantillafactura . "?cod_estado=" . $Inmueble . "&f_corte=" . $row['ano'] . '-' . $row['mes'] . '-01' . "&codinmo=" . $data['inmo'] . "&docu=" . $Documento . "&mesc=" . $row['mes'] . "&an=" . $data['anio'] . "&codinmueble=" . $Inmueble . "'><i class='fa fa-file-pdf-o' aria-hidden='true'></i></a> $btnviewhistorial $btnsend $btnchangeclave",
                    );
                }
            } else {
                $datas[] = array(
                    'checkbox'      => '',
                    'Sucursal'      => '',
                    'IdEstado'      => '',
                    'Propietario'   => '',
                    'CorteInicial'  => '',
                    'CorteFinal'    => '',
                    'Inmueble'      => '',
                    'Documento'     => '',
                    "linkdocumento" => "",
                );
            }

            $Ssql = "select count(e.nit) as total from estados e ,propietario p where e.nit= p.nit and p.inmueble=e.inm and e.mes=:mes and e.ano=:ano and e.inmo=:inmo $sWhere group by e.inm,p.nit";
            // echo '<pre>';var_dump($Ssql);die;
            $stmtcount = $this->connPDO->prepare($Ssql);
            $stmtcount->bindParam(':inmo', $data['inmo']);
            $stmtcount->bindParam(':mes', $meses[$data['mes']]);
            $stmtcount->bindParam(':ano', $data['anio']);
            $stmtcount->execute();
            $rResultTotal = $stmtcount->fetchAll();
            // $iTotal = $rResultTotal[0]["total"];
            $iTotal = $stmtcount->rowCount();
            $output = array(
                "sEcho"                => intval($data['sEcho']),
                "iTotalRecords"        => $iTotal,
                "iTotalDisplayRecords" => $iTotal, //$iFilteredTotal[0],
                "aaData"               => $datas,
            );
            return $output;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;

    }

    private function getCampo($tabla, $parametro, $campoGet, $mostarq = '')
    {
        $connPDO = $this->connPDO;

        $sql = "SELECT $campoGet FROM $tabla " . $parametro;
        //         echo "<br>".$sql."<br>";
        if ($mostarq == 1) {
            echo "<br>" . $sql . "<br>";
        }

        $stmt = $connPDO->prepare($sql);
        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            return $result["$campoGet"];
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
    }
    private function validarEmail($str)
    {
        if (!filter_var($str, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }
    private function validarVariosEmails($str)
    {
        $emailRegEx = '/^(([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+)(([\s]*[;,\/]+[\s]*(([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+))*)$/';

        if (preg_match($emailRegEx, $str)) {
            return true;
        }

        return false;
    }
}
