<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

@include("../funciones/connPDO.php");
class Usuarios2
{
    public function __construct($conn=""){
		$this->db=$conn;
	}
    
    private $id_usuario;

    
    
   public function listadoUsuarios($data)
	{
		$connPDO=new Conexion();
        $cond="";
        
        
		$stmt=$connPDO->prepare("SELECT iduser,Id_Usuarios,Clave,Correo,concat(Nombres,' ',apellidos) as nom,Estado,Login
                    FROM usuarios 
					where IdInmmo=:idinmo 
					and perfil not in (0,5,6) 
                    ORDER BY Nombres ASC
                    limit 0,1000");
        $stmt->bindParam(':idinmo',$data['idInmo']);
        //$stmt->bindParam(':idinmo',$id);
        $flag	=$data['codPortal'];
        $inmob= $data['idInmo'];
       
		if($stmt->execute())
		{
			$data=array();
			$connPDO->exec('chartset="utf-8"');
			while ($row = $stmt->fetch()) 
            {
            	$idu=$row['Id_Usuarios'];
            	$btnPerfil="";
            	if($_SESSION['IdInmmo']==1)
            	{

                	$btnPerfil='<a href="../profile/perfilUsuarioDinamic.php?ctr=1&id='.$row['iduser'].'"><button type="button" id="gratuitos" class="btn btn-default  gratuitos  btn-xs mensajeT" alt="Perfil"  data-toggle="tooltip" data-placement="bottom" title="Perfil"><span class="glyphicon glyphicon"><i class="fa fa-user"></i></span></button></a>';
            	}
            	else
            	{
                	$btnPerfil='<a href="../profile/perfilUsuarioDinamic.php?ctr=1&id='.$row['iduser'].'"><button type="button" id="gratuitos" class="btn btn-default  gratuitos  btn-xs mensajeT" alt="Perfil"  data-toggle="tooltip" data-placement="bottom" title="Perfil"><span class="glyphicon glyphicon"><i class="fa fa-user"></i></span></button></a>';

            	}
            	$btnPermisos='<a href="../../sj/app/access.php?id='.$idu.'"><button type="button" id="gratuitos" class="btn btn-primary  gratuitos  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Permisos Usuario"><span class="glyphicon glyphicon"><i class="fa fa-key"></i></span></button></a>';
            	$existeM2=($this->evaluaUsuarioM2($idu)>0 )?'spartan':'watermelon';
            	$btnPortalesM2=($this->evaluaPortal($inmob,10) && $row['Estado']==0)?'<button type="button" id="m2" class="btn btn-'.$existeM2.'  m2  btn-xs mensajeT" data-toggle="modal" data-target="#actM2"  data-toggle="tooltip" data-placement="bottom" title="MetroCuadrado" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button>':'';
            	$existeFinca=($this->evaluaUsuarioFinca($idu)>0 )?'spartan':'watermelon';
            	$btnPortalesFinca=($this->evaluaPortal($inmob,8) && $row['Estado']==0)?'<button type="button" id="m2" data-toggle="modal" data-target="#actFR" class="btn btn-'.$existeFinca.'  finca  btn-xs mensajeT"   data-placement="bottom" title="Fincaraiz" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button>':'';
            	$bntEstado=($row['Estado']==9)?'<span class="label label-danger">'.ucwords(strtolower(getCampo('estadosusuarios',"where idEstUsu=".$row['Estado'],"descEstUsu"))).'</span>':'<span class="label label-success">'.ucwords(strtolower(getCampo('estadosusuarios',"where idEstUsu=".$row['Estado'],"descEstUsu"))).'</span>';
            	
            	
            	$botones='';
            	$inputlogin='<input type="hidden" class="idusariom2" value="'.$row['Id_Usuarios'].'">';
            	if($flag==10)
            	{
            		$botones=$btnPortalesM2;
            	}
            	elseif($flag==8)
            	{
            		$botones=$btnPortalesFinca;
            	}
            	else
            	{
            		$botones=$btnPerfil.$btnPermisos.$btnPortalesM2.$btnPortalesFinca;
            	}
            	
            		
            	$data[]=array(
                "nom" 			=> ucwords(strtolower(utf8_encode($row['nom']))),
                "Id_Usuarios"   => $row['Id_Usuarios'],
                "Login"      	=> $row['Login'],
				"Estado" 		=> $bntEstado,
                "Correo" 		=> $row['Correo'],
				"bnts" 		    => $botones.$inputlogin
                );
			}
			return $data;
			
		}
		else
		{
			return print_r($stmt->errorInfo());
			
		}
        $stmt=NULL;
	}
	public function evaluaPortal($idInmo,$idPortal)
	{
		$connPDO=new Conexion();

		$stmt=$connPDO->prepare("Select IdPortal 
						From PublicaPortales 
						Where IdPortal=:idPortal
						And IdInmobiliaria=:idinmo");

		if($stmt->execute(array(
			":idinmo" => $idInmo,
			":idPortal" => $idPortal
			)))
		{
			
			$existe=$stmt->rowCount();
			return $existe;
			
		}
		else
		{
			return print_r($stmt->errorInfo());
			
		}
        $stmt=NULL;
	}
	public function evaluaUsuarioM2($idusuario)
	{
		$connPDO=new Conexion();

		$stmt=$connPDO->prepare("Select idusariom2 
						From usuariosm2 
						Where idusariom2=:idusuario");

		if($stmt->execute(array(
			":idusuario" => $idusuario
			)))
		{
			
			$existe=$stmt->rowCount();
			return $existe;
			
		}
		else
		{
			return print_r($stmt->errorInfo());
			
		}
        $stmt=NULL;
	}
	public function evaluaUsuarioFinca($idusuario)
	{
		$connPDO=new Conexion();

		$stmt=$connPDO->prepare("Select idusguia
						From usuariosguia 
						Where idusguia=:idusguia");

		if($stmt->execute(array(
			":idusguia" => $idusuario
			)))
		{
			
			$existe=$stmt->rowCount();
			return $existe;
			
		}
		else
		{
			return print_r($stmt->errorInfo());
			
		}
        $stmt=NULL;
	}
    
}

?>
