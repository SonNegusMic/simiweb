<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include("../funciones/connPDO.php");


class functions 
{
    
    public function __construct($conn=''){
		$this->db=$conn;
		
	}	
	public function obtenerAsesores()
	{
		$connPDO = new Conexion();
		$stmt = $connPDO-> prepare('SELECT trim(Nombres) nom,Id_Usuarios,apellidos 
		 from usuarios
		 where IdInmmo = :IdInmmo 
		 and perfil not in("0","5","6")
		 and  Estado != 9 
		 order by nom');
		if($stmt->execute(array(
					":IdInmmo" => $_SESSION['IdInmmo']))
		)
		{
			$connPDO -> exec("SET NAMES 'utf8'");
			$data = array();
			while ($row = $stmt->fetch()) {
				

				$data[]=array(
						"Nombres"   => ucwords(strtolower($row['nom'])),
						"Apellidos" => ucwords(strtolower($row['apellidos'])),
						"codigo"    => $row['Id_Usuarios']
					);

			}
			return $data;
		}
		else
		{
			return print_r($stmt->errorInfo());
		}
		$stmt= NULL;

	}
	public function obtenerAsesoresIdUsers()
	{
		$connPDO = new Conexion();
		$stmt = $connPDO->prepare('SELECT trim(Nombres) nom,idUser,apellidos 
		 from usuarios
		 where IdInmmo = :IdInmmo 
		 and perfil not in("0","5","6")
		 and  Estado != 9 
		 order by nom');
		if($stmt->execute(array(
					":IdInmmo" => $_SESSION['IdInmmo']))
		)
		{
			$connPDO -> exec("SET NAMES 'utf8'");
			$data = array();
			while ($row = $stmt->fetch()) {
				

				$data[]=array(
						"Nombres"   => ucwords(strtolower($row['nom'])),
						"Apellidos" => ucwords(strtolower($row['apellidos'])),
						"codigo"    => $row['idUser']
					);

			}
			return $data;
		}
		else
		{
			return print_r($stmt->errorInfo());
		}
		$stmt= NULL;

	}
	public function obtenerAsesoresidUser($codUser = '')
	{
		$connPDO = new Conexion();
		$cond = '';
		if ($codUser) {
			$cond .= " and Id_Usuarios != :codUser ";
		}
		$stmt = $connPDO-> prepare('SELECT trim(Nombres) nom,idUser,apellidos 
		 from usuarios
		 where IdInmmo = :IdInmmo 
		 and perfil not in("0","5","6")
		 and  Estado != 9'
		 . $cond . 
		 ' order by nom');
		$stmt->bindParam(':IdInmmo', $_SESSION['IdInmmo']);

		if ($codUser) {
			$stmt->bindParam(':codUser', $codUser);
		}
		if($stmt->execute())
		{
			$connPDO -> exec("SET NAMES 'utf8'");
			$data = array();
			while ($row = $stmt->fetch()) {
				

				$data[]=array(
						"Nombres"   => ucwords(strtolower($row['nom'])),
						"Apellidos" => ucwords(strtolower($row['apellidos'])),
						"codigo"    => $row['idUser']
					);

			}
			return $data;
		}
		else
		{
			return print_r($stmt->errorInfo());
		}
		$stmt= NULL;

	}
	public function getSelect($tabla,$campoId, $campo,$parametro,$campoEstado,$estado,$complemento="",$debug=1)
	{
				$subcadena = ".";
				$camposId; 
				$verficaPunto= strpos($campoId, $subcadena);
				if($verficaPunto===true)
				{

					$posicionsubcadena = strpos ($campoId, $subcadena); 
					$campoId = substr ($campoId, ($posicionsubcadena +1));
					$camposId=$campoId;
				}
				else
				{
					$camposId=$campoId;
				}

			$parametro=str_replace("\\","" , $parametro);
			$campo=str_replace("\\","" , $campo);
			$connPDO = new Conexion();
			if($campoEstado)
			{
				$condicion="AND $campoEstado=$estado";
			}
			else
			{
				$condicion="";
			}
			$dbh ="SELECT  $camposId,$campo
					FROM $tabla ". 
			        $parametro ." ". 
			  		$condicion.$complemento;
			$stmt=$connPDO->prepare( "SELECT  $campoId,$campo
					FROM $tabla ". 
			        $parametro ." ". 
			  		$condicion.$complemento);
			if($stmt->execute())
			{	
				$data= array();
				$connPDO -> exec("SET NAMES 'utf8'");
				while ($row = $stmt->fetch()) {
					$data[]=array(
							"Codigo" => $row[$camposId],
							"Descrip"=> ucwords(strtolower(utf8_encode($row[$campo])))
						);
				}
				
				if($debug != 2)
					return $data;
				if($debug == 2)
					return print_r($dbh);
			}
			else
			{
				if($debug != 2)
				return print_r($stmt->errorInfo());
				if($debug == 2)
					return print_r($dbh);

			}
			$stmt= NULL;
		
   	}
   	public function getSelectParams($tabla,$campoId,$campoAdd, $campo,$parametro,$campoEstado,$estado,$complemento="",$debug=1)
	{
			$connPDO = new Conexion();
			if($campoEstado)
			{
				$condicion="AND $campoEstado=$estado";
			}
			else
			{
				$condicion="";
			}
			$dbh ="SELECT  $campoId,$campo,$campoAdd
					FROM $tabla ". 
			        $parametro ." ". 
			  		$condicion.$complemento;
			$stmt=$connPDO->prepare( "SELECT  $campoId,$campo,$campoAdd
					FROM $tabla ". 
			        $parametro ." ". 
			  		$condicion.$complemento);
			if($stmt->execute())
			{	
				$data= array();
				$connPDO -> exec("SET NAMES 'utf8'");
				while ($row = $stmt->fetch()) {
					$data[]=array(
							"Codigo" => utf8_decode($row[$campoId])."?".utf8_decode($row[$campoAdd]),
							"Descrip"=> ucwords(strtolower($row[$campo]))
						);
				}
				
				if($debug != 2)
					return $data;
				if($debug == 2)
					return print_r($dbh);
			}
			else
			{
				if($debug != 2)
				// return print_r($stmt->errorInfo());
				return print_r($dbh);
				if($debug == 2)
					return print_r($dbh);

			}
			$stmt= NULL;
		
   	}
   	public function getSelectParam($codigo,$campo,$parametro,$estado,$debug=1)
   	{
   		$connPDO = new Conexion();
   		$dbh="SELECT  conse_param,desc_param,$campo
			FROM parametros 
			where est_param='".$estado."' 
			and id_param='".$codigo."' ". 
			$parametro ." ". 
			$condicion;
   		
   		$stmt=$connPDO->prepare("SELECT  conse_param,desc_param,$campo
			FROM parametros 
			where est_param='".$estado."' 
			and id_param='".$codigo."' ". 
			$parametro ." ". 
			$condicion);
   		if($stmt->execute())
			{	
				$data= array();
				while ($row = $stmt->fetch()) {
					$data[]=array(
							"Codigo" => $row['conse_param'],
							"Descrip"=> $row['desc_param']
						);
				}
				if($debug != 1)
				return $data;
				if($debug == 2)
				return print_r($dbh);
			}
			else
			{
				if($debug != 2)
				return print_r($stmt->errorInfo());
				if($debug == 2)
				return print_r($dbh);

			}
			$stmt= NULL;
   	}
   	public function getCampo($tabla,$parametro,$campoGet,$debug)
   	{
   		$connPDO = new Conexion();

   		$dbh = "SELECT $campoGet
		 FROM $tabla ".
		 $parametro;

		 $stmt=$connPDO->prepare("SELECT $campoGet
		 FROM $tabla ".
		 $parametro);
		 if($stmt->execute())
		 {	
		 	// $data= array();
				while ($row = $stmt->fetch()) {
					$data= $row[$campoGet];
				}
						
				if($debug != 2)
				return $data;
				if($debug == 2)
				return print_r($dbh);
		 }
		 else
		 {
		 		if($debug != 2)
				return print_r($stmt->errorInfo());
				if($debug == 2)
				return print_r($dbh);
		 }
		 $stmt = NULL; 
   	}

   	public function getTercerosByInmueble($inmo, $inmu){
   		$connPDO = new Conexion();
   		$sql = "SELECT * FROM inmueblesterceros WHERE idinm_tr = :inmu AND inmob_tr = :inmo AND iduser_tr > 0 AND rol_tr IN(3,4) GROUP BY rol_tr";
   		$stmt = $connPDO->prepare($sql);
   		$stmt->bindParam(':inmu', $inmu);
   		$stmt->bindParam(':inmo', $inmo);
   		$data = array();
   		$rol = '';
   		if ($stmt->execute()) {
   			if ($stmt->rowCount() > 0) {
   				while ($row = $stmt->fetch()) {
   					if ($row['rol_tr'] == 1) {
   						$rol = 'Propietario';
   					}elseif($row['rol_tr'] == 2){
   						$rol = 'Apoderado';
   					}elseif($row['rol_tr'] == 3){
   						$rol = 'Asesor';
   					}elseif($row['rol_tr'] == 4){
   						$rol = 'Captador';
   					}
   					$data[] = array(
   						'idusu' => $row['rol_tr'],
   						'Nombre' => $rol
   					);
   				}
   				return array('status' => 'Ok', 'data' => $data);
   			}else{
   				return array('status' => 'Error', 'msg' => 'El inmueble no tiene Terceros');
   			}
   		}else{
   			return array('status' => 'Error', 'msg' => print_r($stmt->errorInfo()));
   		}
   	}
}
?>