<?php

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include "../funciones/connPDO.php";

class Profile
{
    public function __construct($conn)
    {
        $this->db = $conn;
    }
    public function setDataProfile($data)
    {
        $connPDO     = new Conexion();
        $colores     = array();
        $id_usuario  = $data['iduser'];
        $htmlcolores = "";
        $stmt        = $connPDO->prepare("SELECT Correo, Celular,
								 descargaexcel, editacita,
								 Telefono,perfil,zonausu,Nombres,
								 Login ,apellidos,Estado,Foto,reporta_a,coloragenda
								 FROM usuarios
								 WHERE iduser = :iduser");
        if ($stmt->execute(array(
            "iduser" => $id_usuario,
        ))) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $foto        = ($row['Foto'] == "") ? "" : "http://www.simiinmobiliarias.com/mcomercialweb/" . $row['Foto'];
                $sqlcolores  = "SELECT id_param, desc_param FROM parametros WHERE id_param = 56";
                $stmtcolores = $connPDO->prepare($sqlcolores);
                if ($stmtcolores->execute()) {
                    while ($rowcolor = $stmtcolores->fetch()) {
                        array_push($colores, $rowcolor['desc_param']);
                    }
                } else {
                    echo "<pre>";
                    print_r($stmtcolores->errorInfo());
                    echo "</pre>";
                }
                foreach ($colores as $key => $value) {
                    $htmlcolores .= '<div data-color="#' . $value . '" class="coloragenda"><div class="colagend" style="background-color: #' . $value . '"></div></div>&nbsp;';
                }
                if (empty($row['coloragenda'])) {
                    $row['coloragenda'] = "#20D2FE";
                }
                $data = array(
                    "celular"     => $row["Celular"],
                    "correo"      => $row["Correo"],
                    "descExcel"   => $row['descargaexcel'],
                    "editaCita"   => $row['editacita'],
                    "telefono"    => $row['Telefono'],
                    "perfil"      => $row['perfil'],
                    "zonaUsu"     => $row['zonausu'],
                    "nombres"     => $row['Nombres'],
                    "apellidos"   => $row['apellidos'],
                    "login"       => $row['Login'],
                    "estado"      => $row['Estado'],
                    "reporta"     => $row['reporta_a'],
                    "coloragenda" => $row['coloragenda'],
                    "htmlcolores" => $htmlcolores,
                    "foto"        => $foto,

                );
            }
            return $data;
        } else {
            return 0;
        }
    }
    public function updateFoto($data, $iduser)
    {
        $connPDO = new Conexion();

        $foto = $data;

        $id_usuario = $_SESSION["id_actual"];
        $stmt       = $connPDO->prepare("UPDATE usuarios
								  SET Foto = :Foto
								  WHERE iduser = :iduser
			");
        if ($stmt->execute(array(
            ":Foto"   => $foto,
            ":iduser" => $iduser,
        ))) {
            return 1;
        } else {
            return $stmt->errorInfo();
        }
    }
    public function updatePassword($data)
    {

        $connPDO     = new Conexion();
        $password    = $data["passAct"];
        $passwordNvo = $data["passNvo"];
        $usuario     = $data["user"];
        if ($this->confirmarPassword() != $password && !empty($password)) {
            return $this->confirmarPassword();
        } else {

            $idInmo = $_SESSION["IdInmmo"];
            $stmt   = $connPDO->prepare("UPDATE usuarios
									 SET Clave = :Clave
									 WHERE iduser = :iduser
									 AND IdInmmo = :IdInmmo
									 ");
            if ($stmt->execute(array(
                ":Clave"  => $passwordNvo,
                "IdInmmo" => $idInmo,
                ":iduser" => $usuario,
            ))) {

                return 1;
            } else {
                return $stmt->errorInfo();
            }
        }

    }

    public function confirmarPassword()
    {

        $connPDO    = new Conexion();
        $id_usuario = $_SESSION["id_actual"];
        $stmt       = $connPDO->prepare("SELECT Clave
								 FROM usuarios
								 WHERE Id_Usuarios = :Id_Usuarios");
        if ($stmt->execute(array(
            "Id_Usuarios" => $id_usuario,
        ))) {

            while ($row = $stmt->fetch()) {
                $data = $row["Clave"];
            }
            return $data;
        } else {
            return $stmt->errorInfo();
        }
    }
    public function updateProfile($data)
    {
        $connPDO     = new Conexion();
        $correo      = $data["correo"];
        $celular     = $data["celular"];
        $id_usuario  = $data['userProfile'];
        $telefono    = $data["telefono"];
        $cita        = $data["editCitaVal"];
        $descExcel   = $data["desClientesVal"];
        $perfil      = $data["perfil"];
        $zona        = $data["zona"];
        $Nombre      = $data["nombre"];
        $apellidos   = $data["apellidos"];
        $login       = $data["login"];
        $reporta     = $data["reporta"];
        $estado      = $data['estUsuarioVal'];
        $coloragenda = $data['coloragenda'];

        $Nombre = str_replace('á', 'a', $Nombre);
        $Nombre = str_replace('é', 'e', $Nombre);
        $Nombre = str_replace('í', 'i', $Nombre);
        $Nombre = str_replace('ú', 'u', $Nombre);
        $Nombre = str_replace('ó', 'o', $Nombre);

        $Nombre = str_replace('Á', 'A', $Nombre);
        $Nombre = str_replace('É', 'E', $Nombre);
        $Nombre = str_replace('Í', 'I', $Nombre);
        $Nombre = str_replace('Ó', 'O', $Nombre);
        $Nombre = str_replace('Ú', 'Ú', $Nombre);

        $apellidos = str_replace('á', 'a', $apellidos);
        $apellidos = str_replace('é', 'e', $apellidos);
        $apellidos = str_replace('í', 'i', $apellidos);
        $apellidos = str_replace('ú', 'u', $apellidos);
        $apellidos = str_replace('ó', 'o', $apellidos);

        $apellidos = str_replace('Á', 'A', $apellidos);
        $apellidos = str_replace('É', 'E', $apellidos);
        $apellidos = str_replace('Í', 'I', $apellidos);
        $apellidos = str_replace('Ó', 'O', $apellidos);
        $apellidos = str_replace('Ú', 'Ú', $apellidos);

        $login = str_replace('á', 'a', $login);
        $login = str_replace('é', 'e', $login);
        $login = str_replace('í', 'i', $login);
        $login = str_replace('ú', 'u', $login);
        $login = str_replace('ó', 'o', $login);

        $login = str_replace('Á', 'A', $login);
        $login = str_replace('É', 'E', $login);
        $login = str_replace('Í', 'I', $login);
        $login = str_replace('Ó', 'O', $login);
        $login = str_replace('Ú', 'Ú', $login);

        $stmt = $connPDO->prepare("UPDATE usuarios
											SET Correo    = :Correo,
											Celular       = :Celular,
											descargaexcel =:descargaexcel,
											editacita     =:editacita,
											Telefono      =:Telefono,
											perfil        =:perfil,
											zonausu       =:zonausu,
											Nombres      =:Nombres,
											apellidos     =:apellidos,
											Login         =:Login,
											reporta_a     =:reporta,
											Estado        =:Estado,
											coloragenda        =:coloragenda
											WHERE iduser  = :iduser");

        if ($stmt->execute(array(
            ":Correo"        => $correo,
            ":Celular"       => $celular,
            ":iduser"        => $id_usuario,
            ":descargaexcel" => $descExcel,
            ":editacita"     => $cita,
            ":Telefono"      => $telefono,
            ":perfil"        => $perfil,
            ":zonausu"       => $zona,
            ":Nombres"       => $Nombre,
            ":apellidos"     => $apellidos,
            ":Login"         => $login,
            ":reporta"       => $reporta,
            ':Estado'        => $estado,
            ':coloragenda'   => $coloragenda,
        ))) {
            return 1;
        } else {
            print_r($stmt->errorInfo());
        }

    }

}
