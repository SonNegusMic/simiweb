<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
// @include("../funciones/connPDO.php");
include 'funcionesImg.php';

class UploadImg
{
    public function __construct($conn = '')
    {
        $this->db = $conn;
    }
    /*********************************
     *    logica logo inmobiliaria   *
     *********************************/
    public function subirImagenLogo($data, $files)
    {

        if (!empty($files)) {
            $imagenes = $files["imgLogo"]["name"];
            $errorimg = $files["imgLogo"]["error"];
            @$tempimg = $files["imgLogo"]["tmp_name"];
            $typeimg  = $files["imgLogo"]["type"];

            // $tipos=array("image/jpg","image/jpeg","image/png","image/gif");

            $extension = "";

            if ($typeimg == "image/jpg" || $typeimg == "image/jpeg") {
                $extension .= ".jpg";
            }
            if ($typeimg == "image/png") {
                $extension .= ".png";
            }
            if ($typeimg == "image/gif") {
                $extension .= ".gif";
            }

            $se_subio_imagen = subir_imagenesNew($typeimg, $tempimg, $data['codigoInmo'], "../logosNew/", 400);

            return $this->saveImagenes($data['codigoInmo'], "../logosNew/" . $data['codigoInmo'] . $extension);
        }
    }
    public function saveImagenes($codigo, $ruta)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("UPDATE clientessimi
					SET  logo  		= :logo
					WHERE IdInmobiliaria	= :inmo");

        if ($stmt->execute(array(
            ":inmo" => $codigo,
            ":logo" => $ruta,

        ))) {
            return 1;
        } else {
            return print_r($stmt->errorInfo());
        }

    }
    public function subirImagenesInmueble($files, $ruta, $tamanio)
    {
        if (!empty($files)) {
            $imagenes = $files["imagenes"]["name"];
            $errorimg = $files["imagenes"]["error"];
            @$tempimg = $files["imagenes"]["tmp_name"];
            $typeimg  = $files["imagenes"]["type"];
            $j        = 1;
            for ($i = 0; $i < count($imagenes); $i++) {
                if ($errorimg[$i] == 0) {

                    if ($this->validarImagenes($typeimg[$i]) == 1) {
                        $soloNombre = $this->deleteExtseionImage($imagenes[$i]);
                        $soloNombre = $this->deleteExtseionCaracteres($soloNombre);
                        // "imagenes/".$codigo."/"
                        $imagen     = subir_imagenesNew($typeimg[$i], $tempimg[$i], $soloNombre, $ruta, $tamanio);
                        $response[] = array(
                            "status" => $soloNombre,
                            "ruta"   => $ruta . $imagen,
                            "No"     => $j++,

                        );
                    } else {
                        $response[] = array(
                            "status" => "Imagen no valida",
                            "Imagen" => $imagene,
                        );
                    }
                } else {
                    $response[] = array(
                        "status" => "Error de Imagen",
                        "error"  => $errorimg[$i],
                    );
                }
            }
        } else {
            $response = array(
                "status" => "No existen Imagenes",
                "Error"  => $files["imagenes"]["error"],
            );
        }
        if ($response) {
            return $response;
        }
    }
    public function subirImagenesInmuebleNew($files, $ruta, $tamanio)
    {
        if (!empty($files)) {
            $imagenes = $files["imagenes"]["name"];
            $errorimg = $files["imagenes"]["error"];
            @$tempimg = $files["imagenes"]["tmp_name"];
            $typeimg  = $files["imagenes"]["type"];
            $j        = 1;

            if ($errorimg == 0) {

                if ($this->validarImagenes($typeimg) == 1) {
                    $soloNombre     = $this->deleteExtseionImage($imagenes);
                    $soloNombre     = $this->deleteExtseionCaracteres($soloNombre);
                    $soloNombrecort = $this->getSubString($soloNombre, 30);
                    // "imagenes/".$codigo."/"
                    $imagen     = subir_imagenesNew($typeimg, $tempimg, $soloNombrecort, $ruta, $tamanio);
                    $response[] = array(
                        "status" => $soloNombre,
                        "ruta"   => $ruta . $imagen,
                        "No"     => $j++,

                    );
                } else {
                    $response[] = array(
                        "status" => "Imagen no valida",
                        "Imagen" => $imagene,
                    );
                }
            } else {
                $response[] = array(
                    "status" => "Error de Imagen",
                    "error"  => $errorimg[$i],
                );
            }
        } else {
            $response = array(
                "status" => "No existen Imagenes",
                "Error"  => $files["imagenes"]["error"],
            );
        }
        if ($response) {
            return $response;
        }
    }
    public function subirDocsInmueble($files, $ruta, $tamanio)
    {

        if (!empty($files)) {
            $size     = $files["imagenes"]["size"];
            $imagenes = $files["imagenes"]["name"];
            $errorimg = $files["imagenes"]["error"];
            @$tempimg = $files["imagenes"]["tmp_name"];
            $typeimg  = $files["imagenes"]["type"];
            $j        = 1;

            if ($errorimg == 0) {

                $fechaserv     = date('Ymd');
                $hora          = date('His');
                $size          = ($size) / 1024;
                $imagenes      = $this->deleteExtseionCaracteres($imagenes);
                $soloNombre    = $this->deleteExtseionImage($imagenes);
                $soloExtension = $this->getExtensionFile($imagenes);
                move_uploaded_file($tempimg, $ruta . $soloNombre . $fechaserv . $hora . "." . $soloExtension);
                $response[] = array(
                    "size"      => $size,
                    "nombre"    => $soloNombre . "." . $soloExtension,
                    "ruta"      => $ruta . $soloNombre . $fechaserv . $hora . "." . $soloExtension,
                    "No"        => $j++,
                    "extension" => $soloExtension,
                );

            } else {
                $response[] = array(
                    "status" => "Error de Imagen",
                    "Error"  => $errorimg[$i],
                );
            }
        } else {
            $response = array(
                "status" => "No existen Imagenes",
                "Error"  => $files["imagenes"]["error"],
            );
        }
        if ($response) {
            return $response;
        }
    }
    public function createFolder($ruta, $folder)
    {
        $fichero = $ruta . "/" . $folder;
        if (file_exists($fichero)) {
            return 1;
        } else {
            if (!mkdir($fichero, 0777, true)) {
                return 0;
            } else {
                return 1;
            }
        }
    }
    public function validarImagenes($imagen)
    {
        $tipos = array("image/jpg", "image/jpeg", "image/png", "image/gif");
        if (in_array($imagen, $tipos)) {
            return 1;
        }

    }
    public function deleteExtseionImage($imagen)
    {
        $tipos = array('.JPG', '.jpeg', '.JPEG', '.GIF', '.PNG', ".jpg", ".png", ".gif", ".xls", ".docx", ".pdf", ".ppt", ".doc", ".txt");
        foreach ($tipos as $key => $value) {
            $validar = strpos($imagen, $value);
            if ($validar != "") {
                $response = str_ireplace($value, "", $imagen);
            }
        }
        return $response;
    }

    public function deleteExtseionCaracteres($imagen)
    {
        $tipos    = array('(', ')', '-', '/', ' ', "*", "ñ", "Ñ", "_", "{", "}", "[", "]");
        $response = str_ireplace($tipos, '', $imagen);

        return $response;
    }
    public function deleteImage($file)
    {
        if (file_exists($file)) {
            unlink($file);
            $response = array(
                "status"  => "Se elimino el fichero",
                "archivo" => $file,
            );
        } else {
            $response = array(
                "status"  => "El Fichero No exsite",
                "archivo" => $file,
            );
        }
        if ($response) {
            return $response;
        }
    }
    public function deleteArchivos($uri)
    {
        $files = glob($uri . '/*'); // obtiene todos los archivos
        foreach ($files as $file) {
            if (is_file($file)) // si se trata de un archivo
            {
                unlink($file);
            }
            // lo elimina
        }
    }
    public function deleteArchivo($file)
    {

        if (is_file($file)) // si se trata de un archivo
        {
            unlink($file);
        }
        // lo elimina
    }
    public function rotateImages($file)
    {
        $rotang = 90;
        $image  = $file;
        $images = getimagesize($file);

        if ($images['mime'] == "image/jpeg") {

            $source = imagecreatefromjpeg($image);
            $rotate = imagerotate($source, $rotang, 0);
            unlink($image);
            imagejpeg($rotate, $image, 100);
            echo $file;

        } elseif ($images['mime'] == "image/png") {
            echo "esto es una imagen " . $images['mime'];
            $source = imagecreatefrompng($image) or die('Error opening file ' . $image);
            imagealphablending($source, false);
            imagesavealpha($source, true);
            $rotation = imagerotate($source, $rotang, imageColorAllocateAlpha($source, 0, 0, 0, 127));
            imagealphablending($rotation, false);
            imagesavealpha($rotation, true);
            unlink($image);
            imagepng($rotation, $image, 0);
            echo $file;
        } else {
            echo "error";

        }

    }
    public function getExtensionFile($file)
    {
        //$file = "prueba@leonpurpura.com";
        $subcadena = ".";
        // localicamos en que posici�n se haya la $subcadena, en nuestro caso la posicion es "7"
        $posicionsubcadena = strpos($file, $subcadena);
        // eliminamos los caracteres desde $subcadena hacia la izq, y le sumamos 1 para borrar tambien el @ en este caso
        $extension = substr($file, ($posicionsubcadena + 1));
        return $extension; // leonpurpura.com
    }
    public function getSubString($string, $length = null)
    {
        //Si no se especifica la longitud por defecto es 50
        if ($length == null) {
            $length = 50;
        }

        //Primero eliminamos las etiquetas html y luego cortamos el string
        $stringDisplay = substr(strip_tags($string), 0, $length);
        //Si el texto es mayor que la longitud se agrega puntos suspensivos
        if (strlen(strip_tags($string)) > $length) {
            $stringDisplay;
        }

        return $stringDisplay;
    }
}
