<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include("../funciones/connPDO.php");


class Barrios
{
    public function __construct($conn=""){
		$this->db=$conn;
	}
    
    private $id_usuario;
    
	public function traeBarrio($ciudad,$id1='')
	{
		$w_conexion = new MySQL(); 
		if($id1)
		{
			$id1=$id;
		}
		else
		{
			$id1='barrio';
		}
		getSelectP('barrios','IdBarrios', 'CONCAT(UPPER(LEFT(NombreB, 1)), LOWER(SUBSTRING(NombreB, 2)))',"","where IdCiudad=$ciudad and NombreB!='' order by NombreB","$id1","$IdBarrio",'',"class='form-control chosen-select' id='barrio'",'','','Seleccione Barrio');
		$w_conexion ->CerrarConexion();
	}
	public function traeBarrios($ciudad,$id1='')
	{
		$w_conexion = new MySQL(); 
		if($id1)
		{
			$id1=$id;
		}
		else
		{
			$id1='barrio';
		}
		$sql="SELECT IdBarrios,NombreB
			from barrios
			where IdCiudad=$ciudad
			order by NombreB";

		$res=$w_conexion->ResultSet($sql);
		$data=array();
		while($row=$w_conexion->FilaSiguienteArray($res))
		{
			$data[]= array(
					"barrio"  	=> ucwords(strtolower(utf8_encode($row['NombreB']))),
					"codbarrio" => $row['IdBarrios']);
			
		}
		return $data;
		$w_conexion -> CerrarConexion();
	}
	public function getBarriosAllPDO($data)
	{
		$connPDO = new Conexion();
		$cond = '';
		$stmt = $connPDO->prepare("SELECT IdBarrios AS cod,CONCAT(
										    UPPER(LEFT(NombreB, 1)),
										    LOWER(SUBSTRING(NombreB, 2))
										  ) AS nom 
			from barrios
			where IdCiudad=:ciudad
			order by NombreB");
		$stmt->bindParam(':ciudad',$data['ciudad'],PDO::PARAM_STR);
		if($stmt->execute())
		{
			$data=array();
			$stmt->bindColumn(1,$cod);
			$stmt->bindColumn(2,$nom);
			$connPDO -> exec("SET NAMES 'utf8'");
			while($row=$stmt->fetch(PDO::FETCH_BOUND))
			{
				$data[]=array(
							"id" => $cod,
							"Nombre" => utf8_encode($nom)
						);
				
			}
			return $data;
		}
		else
		{
			print_r($stmt->errorInfo());
		}
		$stmt=NULL;
	}
	public function getBarriosPDO($idInmo,$data)
	{
		$connPDO = new Conexion();
		$cond = '';
		if ($idInmo) {
			$cond .= ' AND i.`IdInmobiliaria` = :idinm ';
		}

		$stmt = $connPDO->prepare("SELECT b.IdBarrios AS cod,
										  CONCAT(
										    UPPER(LEFT(b.NombreB, 1)),
										    LOWER(SUBSTRING(b.NombreB, 2))
										  ) AS nom 
										FROM
										  inmuebles i,
										  barrios b 
										WHERE i.`IdBarrio` = b.`IdBarrios` 
										  $cond
										  AND b.IdCiudad= :ciudad
										  AND i.`idEstadoinmueble` = 2 
										GROUP BY b.`IdBarrios` 
										ORDER BY b.NombreB ");
		if ($idInmo) {
			$stmt->bindParam(':idinm',$idInmo,PDO::PARAM_STR);
		}

	
		$stmt->bindParam(':ciudad',$data['ciudad'],PDO::PARAM_STR);
		if($stmt->execute())
		{
			$data=array();
			$stmt->bindColumn(1,$cod);
			$stmt->bindColumn(2,$nom);
			$connPDO -> exec("SET NAMES 'utf8'");
			while($row=$stmt->fetch(PDO::FETCH_BOUND))
			{
				$data[]=array(
							"id" => $cod,
							"Nombre" => utf8_encode($nom)
						);
				
			}
			return $data;
		}
		else
		{
			print_r($stmt->errorInfo());
		}
		$stmt=NULL;
	}
	public function getBarriosParams($data)
	{
		// $response=array();
		$connPDO=new Conexion();
		$stmt=$connPDO->prepare(' SELECT  b.IdBarrios AS cod,
										CONCAT(
										    UPPER(LEFT(b.NombreB, 1)),
										    LOWER(SUBSTRING(b.NombreB, 2))
										) AS nom 
								  FROM    barrios b
								  WHERE b.`IdLocalidad`= :localidad
		                          ORDER BY b.NombreB ');
		if($stmt->execute(array(
			":localidad"=>$data['localidad'],
			)))
		{

			$stmt->bindColumn(1,$cod);
			$stmt->bindColumn(2,$nom);
			$connPDO -> exec("SET NAMES 'utf8'");
			while($row=$stmt->fetch(PDO::FETCH_BOUND))
			{
				$response[]=array(
							"id" => $cod,
							"title" => utf8_encode($nom)
						);
				
			}
		
		}
		else
		{
			$response[]=array("Error"=>$stmt->errorInfo(),
							   "data"=>$data);
		}
		if($response)
		{
			return $response;
		}
		else
		{
			return 0;
		}

	}
	/*********************************
	 *  Logica para autocomplete de creacion de inmueble  *
	 *********************************/
	 public function obtenerBarriosCiudad($datas)
    {
        
        
        $connPDO = new Conexion();
        $data    = array();
        $valueTerm = utf8_decode('%'.$datas['term'].'%');
      

        //$barrios = $connPDO->prepare("SELECT b.`IdBarrios`,b.`NombreB`,l.`descripcion` AS localidad, z.`NombreZ` AS zona FROM barrios b, localidad l, zonas z WHERE b.`IdLocalidad`=l.`idlocalidad` AND b.`NombreB` like :nomBarrio  AND b.`IdZona`=z.`IdZona`AND b.`IdCiudad`=:idCiudad");
        $barrios =$barrios = $connPDO->prepare("SELECT 
		  b.`IdBarrios`,
		  b.`NombreB`,
		  l.`descripcion` AS localidad,
		  z.`NombreZ` AS zona,
		   d.`Nombre` AS depto
		FROM
		  barrios b,
		  localidad l,
		  zonas z ,
		  departamento d,
		  ciudad c
		WHERE b.`IdLocalidad` = l.`idlocalidad` 
		  AND b.`NombreB` LIKE :nomBarrio
		  AND b.`IdZona` = z.`IdZona` 
		  AND b.`IdCiudad` = :idCiudad
		  AND b.`IdCiudad`= c.`IdCiudad`
		  AND c.`IdDepartamento`=d.`IdDepartamento`");

        $barrios->bindParam(':idCiudad', $datas['val']);
        $barrios->bindParam(':nomBarrio', $valueTerm, PDO::PARAM_STR);
        if ($barrios->execute()) {
            $connPDO->exec("SET NAMES 'utf8'");
            while ($rowbarrios = $barrios->fetch()) {
		        $nomBarrio=ucfirst(strtolower($rowbarrios['NombreB']));
		        $nomLocalidad=ucfirst(strtolower($rowbarrios['localidad']));
		        $nomZona=ucfirst(strtolower($rowbarrios['zona']));
                $info= utf8_encode($nomBarrio)."<br><b> Localidad: </b>".utf8_encode($nomLocalidad)."<br><b>Zona: </b>".utf8_encode($nomZona);
                $departamento = ucfirst(strtolower($rowbarrios['depto']));

                $data[]=array($rowbarrios['IdBarrios'], $info,strip_tags($info),utf8_encode($nomZona),utf8_encode($nomLocalidad),utf8_encode($nomBarrio),utf8_encode($departamento));
            }
            return $data;
        } else {
            return print_r($barrios->errorInfo());
        }

    }
    public function obtenerBarriosZona($datas)
    {
        
        
        $connPDO = new Conexion();
        $stmt = $connPDO->prepare("SELECT IdBarrios, NombreB FROM barrios 
										WHERE IdZona = :idzona 
										ORDER BY NombreB ASC");
        $stmt->bindParam(":idzona", $datas['zona']);
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    'id' => $row['IdBarrios'],
                    'Nombre' => ucfirst(strtolower(utf8_encode($row['NombreB']))),
                );
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = NULL;
    }
    /*********************************
	 *  Logica para autocomplete de barrio Call Center Cruce  *
	 *********************************/
	 public function obtenerBarriosCruce($datas)
    {
        
        
        $connPDO = new Conexion();
        $data    = array();
        $valueTerm = utf8_decode('%'.$datas['term'].'%');
      

        //$barrios = $connPDO->prepare("SELECT b.`IdBarrios`,b.`NombreB`,l.`descripcion` AS localidad, z.`NombreZ` AS zona FROM barrios b, localidad l, zonas z WHERE b.`IdLocalidad`=l.`idlocalidad` AND b.`NombreB` like :nomBarrio  AND b.`IdZona`=z.`IdZona`AND b.`IdCiudad`=:idCiudad");
        $barrios =$barrios = $connPDO->prepare("SELECT 
		  b.`IdBarrios`,
		  b.`NombreB`,
		  l.`descripcion` AS localidad,
		  z.`NombreZ` AS zona,
		  d.`Nombre` AS depto,
		  c.`Nombre` AS ciudad,
		  c.`IdCiudad` AS idciudad,
		  z.`IdZona` AS idzona,
		  l.`idlocalidad` AS idlocalidad
		FROM
		  barrios b,
		  localidad l,
		  zonas z ,
		  departamento d,
		  ciudad c
		WHERE b.`IdLocalidad` = l.`idlocalidad` 
		  AND b.`NombreB` LIKE :nomBarrio
		  AND b.`IdZona` = z.`IdZona`
		  AND b.`IdCiudad`= c.`IdCiudad`
		  AND c.`IdDepartamento`=d.`IdDepartamento`");

        // $barrios->bindParam(':idCiudad', $datas['val']);
        $barrios->bindParam(':nomBarrio', $valueTerm, PDO::PARAM_STR);
        if ($barrios->execute()) {
            $connPDO->exec("SET NAMES 'utf8'");
            while ($rowbarrios = $barrios->fetch()) {
		        $nomBarrio=ucfirst(strtolower($rowbarrios['NombreB']));
		        $nomCiudad=ucfirst(strtolower($rowbarrios['ciudad']));
		        $nomLocalidad=ucfirst(strtolower($rowbarrios['localidad']));
		        $nomZona=ucfirst(strtolower($rowbarrios['zona']));
                $info= utf8_encode($nomBarrio)."<br><b> Ciudad: </b>".utf8_encode($nomCiudad)."<br><b> Localidad: </b>".utf8_encode($nomLocalidad)."<br><b>Zona: </b>".utf8_encode($nomZona);
                $departamento = ucfirst(strtolower($rowbarrios['depto']));

                $data[]=array($rowbarrios['IdBarrios'], $info,strip_tags($info),utf8_encode($nomZona),utf8_encode($nomLocalidad),utf8_encode($nomBarrio),utf8_encode($departamento),utf8_encode($rowbarrios['idciudad']),utf8_encode($rowbarrios['idzona']),utf8_encode($rowbarrios['idlocalidad']),utf8_encode($nomCiudad));
            }
            return $data;
        } else {
            return print_r($barrios->errorInfo());
        }

    }
}
?>