<?php

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

/**
 *
 */
class Webhook
{

    public function __construct($connPDO)
    {
        $this->connPDO = $connPDO;
        $this->token   = 'EAAZArYW14kKQBACtxKPt4gzt2SigyZAXmVsNSY7BStRLRXpXFe3I6qmI5UJYc1ZCgslU6Cs01hGcpTxuGLXtSTlpvbVBlvYFK6yh34O2nXmgcLtpXZAjtrF2BxVjyZAugaYR3pIw2NdpOueE411NL8JX6d50hjHZAsZBNBBive9lAZDZD';
    }

    public function getInmuebles()
    {
        $sql = "SELECT inm.IdGestion,g.NombresGestion,tp.Descripcion,inm.AreaConstruida,inm.Estrato,f.foto FROM inmnvo inm, gestioncomer g, fotos_nvo f,tipoinmuebles tp WHERE inm.IdInmobiliaria = 1 and g.IdGestion = inm.IdGestion AND f.codinm_fto = inm.codinm AND f.aa_fto = inm.IdInmobiliaria AND tp.idTipoInmueble = inm.IdTpInm  ORDER BY inm.codinm DESC LIMIT 0,3";

        $stmt = $this->connPDO->prepare($sql);
        $data = array();
        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    'title'     => $row['Descripcion'] . ' en ' . $row['NombresGestion'],
                    'subtitle'  => 'Excelente Sitio',
                    'item_url'  => 'http://tae-ltda.com/',
                    'image_url' => 'http://www.simiinmobiliarias.com/mcomercialweb/' . $row['foto'],
                );
            }

            return $data;
        }
    }

    public function sendMessageText($sender, $message)
    {
        $data = array(
            'recipient' => array(
                'id' => $sender,
            ),
            'message'   => array(
                'text' => $message,
            ),
        );

        $this->curl('https://graph.facebook.com/v2.8/me/messages', $data);
    }

    public function primaryMessage($sender, $input = '')
    {
        $data = array(
            'recipient' => array(
                'id' => $sender,
            ),
            'message'   => array(
                'attachment' => array(
                    'type'    => 'template',
                    'payload' => array(
                        'template_type' => 'generic',
                        'elements'      => array(
                            0 => array(
                                'title'     => 'Bienvenido a Tae Ltda.',
                                'item_url'  => 'http://tae-ltda.com/',
                                'image_url' => 'http://www.simiinmobiliarias.com/logosNew/1.jpg',
                                'subtitle'  => 'Selecciona una opcion para poder ayudarte',
                                'buttons'   => array(
                                    0 => array(
                                        'type'    => 'postback',
                                        'title'   => 'Buscar Inmueble',
                                        'payload' => '1',
                                    )
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        );
        $this->curl('https://graph.facebook.com/v2.8/me/messages', $data);
    }
    public function resetFilter($sender, $primaryAnswer)
    {
        $data = array(
            'recipient' => array(
                'id' => $sender,
            ),
            'message'   => array(
                'attachment' => array(
                    'type'    => 'template',
                    'payload' => array(
                        'template_type' => 'button',
                        'text'          => 'Parece que tu anterior eleccion fue ' . $primaryAnswer . ', quieres seguir con esa opcion o reiniciar todo el filtro?',
                        'buttons'       => array(
                            0 => array(
                                'type'    => 'postback',
                                'title'   => 'Seguir con seleccion',
                                'payload' => 'continue',
                            ),
                            1 => array(
                                'type'    => 'postback',
                                'title'   => 'Reiniciar conversacion',
                                'payload' => 'reset',
                            ),
                        ),
                    ),
                ),
            ),
        );
        $this->curl('https://graph.facebook.com/v2.8/me/messages', $data);
    }

    public function welcomeMessage()
    {
        $data = array(
            'setting_type' => 'greeting',
            'greeting'     => array(
                'text' => 'Bienvenido {{user_full_name}}, Inicia una conversacion para poder ayudarte!',
            ),
        );

        $this->typingMessage($sender);

        $this->curl('https://graph.facebook.com/v2.8/me/thread_settings', $data);
    }

    public function typingMessage($senderID)
    {
        $data = array(
            'recipient'     => array(
                'id' => $senderID,
            ),
            'sender_action' => 'typing_on',
        );
        $this->curl('https://graph.facebook.com/v2.8/me/messages', $data);
    }

    public function existeRegistro($iduser)
    {
        $sql = "SELECT * FROM contactos_facebook WHERE id_fb = :iduser AND estado = 1";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':iduser', $iduser);

        if ($stmt->execute()) {
            return $stmt->rowCount();
        }
    }

    public function insertaPreguntaVacia($iduser, $tipo_qsn)
    {
        $sql = "INSERT INTO contactos_facebook (id_fb,tipo_fb,issess_fb) VALUES (:iduser,:tipo_qsn,:timecurrent)";

        $stmt = $this->connPDO->prepare($sql);

        $timecurrent = time();

        $stmt->bindParam(':iduser', $iduser);
        $stmt->bindParam(':tipo_qsn', $tipo_qsn);
        $stmt->bindParam(':timecurrent', $timecurrent);

        if ($stmt->execute()) {
            return 1;
        } else {
            return 2;
        }

    }

    public function insertaRegistro($iduser)
    {
        $sql  = "INSERT ignore INTO log_botfacebook (id_userfb,date_register) VALUES (:iduser,:date_register)";
        $stmt = $this->connPDO->prepare($sql);

        $fnow = date('Y-m-d');

        $stmt->bindParam(':iduser', $iduser);
        $stmt->bindParam(':date_register', $fnow);

        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                return 1;
            } else {
                return 2;
            }
        }
    }

    public function updateColumn($iduser, $column, $valor)
    {
        $sql  = "UPDATE log_botfacebook SET :column = :valor WHERE id_userfb = :iduser";
        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':column', $column);
        $stmt->bindParam(':valor', $valor);
        $stmt->bindParam(':iduser', $iduser);

        if ($stmt->execute()) {
            return 1;
        } else {
            return 2;
        }
    }

    public function verificarFecha($iduser)
    {
        $sql = "SELECT issess_fb FROM contactos_facebook WHERE id_fb = :iduser AND estado = 1 ORDER BY conse DESC LIMIT 0,1";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':iduser', $iduser);

        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            $timestampCurrent = time();
            $timestampSql     = $result['issess_fb'];

            //Variables de dia mes año y hora actual
            $anioCurrent   = date('Y', $timestampCurrent);
            $mesCurrent    = date('m', $timestampCurrent);
            $diaCurrent    = date('d', $timestampCurrent);
            $horaCurrent   = date('H', $timestampCurrent);
            $minutoCurrent = date('i', $timestampCurrent);

            //Variables de dia mes año y hora Consulta
            $anioSql   = date('Y', $timestampSql);
            $mesSql    = date('m', $timestampSql);
            $diaSql    = date('d', $timestampSql);
            $horaSql   = date('H', $timestampSql);
            $minutoSql = date('i', $timestampSql);

            $compareAnio   = $anioCurrent - $anioSql;
            $compareMes    = $mesCurrent - $mesSql;
            $compareDia    = $diaCurrent - $diaSql;
            $compareHora   = $horaCurrent - $horaSql;
            $compareMinuto = $minutoCurrent - $minutoSql;

            if ($compareAnio == 0 AND $compareMes == 0 AND $compareDia == 0 AND $compareHora == 0 AND $compareMinuto < 10) {
                return 1;
            }else{
                return 2;
            }
        }else{
            return 3;
        }
    }

    public function getPreguntas(){
        $sql = "SELECT * FROM questions_facebook WHERE est_qns = 1";

        $stmt = $this->connPDO->prepare($sql);

        $data = array();

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    'id' => $row['id']
                );
            }
            return $data;
        }
    }

    public function verificarPregunta($iduser,$pregunta){
        $sql = "SELECT * FROM contactos_facebook WHERE id_fb = :iduser AND estado = 1 AND tipo_fb = :pregunta";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':iduser', $iduser);
        $stmt->bindParam(':pregunta', $pregunta);

        if ($stmt->execute()) {
            return $stmt->rowCount();
        }
    }

    public function updateRespuestaPregunta($iduser,$pregunta,$respuesta){
        $sql = "UPDATE contactos_facebook SET respuesta = :respuesta WHERE id_fb = :iduser AND tipo_fb = :pregunta AND estado = 1";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':iduser', $iduser);
        $stmt->bindParam(':pregunta', $pregunta);
        $stmt->bindParam(':respuesta', $respuesta);

        if ($stmt->execute()) {
            return 1;
        }else{
            return 2;
        }

    }

    public function getRespuestasTabla($tabla,$idtable){
        $sql = "SELECT $idtable as id FROM $tabla";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->execute();

        $data = array();

        while ($row = $stmt->fetch()) {
            array_push($data,$row['id']);
        }

        return $data;
    }

    public function getTipoInmuebles($iduser,$pag = 0){
        $limit = 'LIMIT ' . $pag . ',2';

        $newPag = $pag + 2;

        $sql = "SELECT idTipoInmueble,Descripcion FROM tipoinmuebles $limit";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->execute();
        if (!$stmt->execute()) {
            return print_r($stmt->errorInfo());
        }
        $botones = array();
        while ($row = $stmt->fetch()) {
            $botones[] = array(
                'type' => 'postback',
                'title' => ucfirst(strtolower($row['Descripcion'])),
                'payload' => $row['idTipoInmueble']
            );
        }

        if ($stmt->rowCount() <= 1) {
            $botones[] = array(
                'type' => 'postback',
                'title' => 'Volver a mostrar',
                'payload' => 'pag-0'
            );
        }else{
            $botones[] = array(
                'type' => 'postback',
                'title' => 'Ver mas',
                'payload' => 'pag-' . $newPag
            );
        }
        $data = array(
            'recipient' => array(
                'id' => $iduser,
            ),
            'message'   => array(
                'attachment' => array(
                    'type'    => 'template',
                    'payload' => array(
                        'template_type' => 'button',
                        'text'          => 'Selecciona el tipo de inmueble',
                        'buttons'       => $botones
                    ),
                ),
            ),
        );
        $this->curl('https://graph.facebook.com/v2.8/me/messages', $data);

    }
    public function getGestionComercial($iduser,$pag = 0){
        $limit = 'LIMIT ' . $pag . ',2';

        $newPag = $pag + 2;

        $sql = "SELECT IdGestion,NombresGestion FROM gestioncomer $limit";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->execute();
        if (!$stmt->execute()) {
            return print_r($stmt->errorInfo());
        }
        $botones = array();
        while ($row = $stmt->fetch()) {
            $botones[] = array(
                'type' => 'postback',
                'title' => ucfirst(strtolower($row['NombresGestion'])),
                'payload' => $row['IdGestion']
            );
        }

        if ($stmt->rowCount() <= 1) {
            $botones[] = array(
                'type' => 'postback',
                'title' => 'Volver a mostrar',
                'payload' => 'pag-0'
            );
        }else{
            $botones[] = array(
                'type' => 'postback',
                'title' => 'Ver mas',
                'payload' => 'pag-' . $newPag
            );
        }
        $data = array(
            'recipient' => array(
                'id' => $iduser,
            ),
            'message'   => array(
                'attachment' => array(
                    'type'    => 'template',
                    'payload' => array(
                        'template_type' => 'button',
                        'text'          => 'Selecciona el tipo de gestion',
                        'buttons'       => $botones
                    ),
                ),
            ),
        );
        $this->curl('https://graph.facebook.com/v2.8/me/messages', $data);

    }

    private function curl($url, $data)
    {
        $dataFinal = json_encode($data);
        $ch        = curl_init($url . '?access_token=' . $this->token);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataFinal);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
    }

}
