<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
// @include("../funciones/connPDO.php");

class Temporal
{
    public function __construct($conn = "")
    {
        $this->db = $conn;
    }

    private $id_usuario;

   
    public function insertaInfoTemp($t1,$t2,$t3,$t4,$t5,$t6,$t7)
   {
         $connPDO = new Conexion();
        
        $stmt= $connPDO->prepare("REPLACE INTO  resultados_cruces 
            (user_crc,inmob_crc,codinm_crc,demanda_crc,texto_cumple_crc,
            estrellas_crc,tiene_politicas_crc)
        values(:t1,:t2,:t3,:t4,:t5,
              :t6,:t7)");

        if($stmt->execute(array(
                ":t1"=>$t1,
                ":t2"=>$t2,
                ":t3"=>$t3,
                ":t4"=>$t4,
                ":t5"=>$t5,
                ":t6"=>$t6,
                ":t7"=>$t7
            )))
        {   
            return 1;
        }
        else
        {
            return print_r($stmt->errorInfo())." ".$nombreTabla;
        }
   }
   public function deleteInfoTemp($usr)
   {
         $connPDO = new Conexion();
        
        $stmt= $connPDO->prepare("DELETE FROM resultados_cruces
            WHERE   user_crc=:usr");

        if($stmt->execute(array(
                ":usr"=>$usr
            )))
        {   
            return 1;
        }
        else
        {
            return print_r($stmt->errorInfo());
        }
   }
    public function InfoTemp($usr,$IdInmobiliaria,$codinm='',$coddem='')
   {
         $connPDO = new Conexion();
        $cond="";
        if($codinm)
        {
            $cond .= " and codinm_crc=:codinm";
        }
        if($codinm)
        {
            $cond .= " and demanda_crc=:coddem";
        }
        $stmt= $connPDO->prepare("SELECT  user_crc,inmob_crc,codinm_crc,demanda_crc,texto_cumple_crc,
            estrellas_crc,tiene_politicas_crc
            FROM resultados_cruces 
            WHERE  user_crc =:usr
            and inmob_crc   =:IdInmobiliaria
            $cond");

        $stmt->bindParam(":usr", $usr);
        $stmt->bindParam(":IdInmobiliaria", $IdInmobiliaria);
        
        if($coddem)
        {
            $stmt->bindParam(":coddem", $coddem);
        }
        if($codinm)
        {
            $stmt->bindParam(":codinm", $codinm);
        }
        
        if($stmt->execute())
        {   
            $info = array();
            while ($row = $stmt->fetch()) 
            {
                $info[] = array(
                "texto_cumple_crc"    => utf8_encode($row["texto_cumple_crc"]),
                "estrellas_crc"       => $row["estrellas_crc"],
                "demanda_crc"         => $row["demanda_crc"],
                "tiene_politicas_crc" => $row["tiene_politicas_crc"]
                );
            }
        
            return $info;
        }
        else
        {
            return print_r($stmt->errorInfo());
        }
   }

}
