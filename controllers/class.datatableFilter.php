<?php  
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

class DatatableFilter
{

	public function getFilterData($post,$get,$table,$columnsTable,$where,$index,$limit_init){

		/**datatable*/
		$connPDO= new Conexion();
		$sTable = $table;


		//columnas para el datatable
	    $aColumns = $columnsTable;

	    /* Indexed column (used for fast and accurate table cardinality) */
	    $sIndexColumn = $index;

	   

	    /*
	     * paginacion
	     */
	    $sLimit = $limit_init;
	    if ( isset( $get['start'] ) && $get['length'] != '-1' )
	    {
	        $sLimit = "LIMIT ".intval( $get['start'] ).", ".
	            intval( $get['length'] );
	    }

	    /*
	     * Ordenar
	     */
	    $sOrder = "";
	    if ( isset( $get['iSortCol_0'] ) )
	    {
	        $sOrder = "ORDER BY  ";
	        for ( $i=0 ; $i<intval( $get['iSortingCols'] ) ; $i++ )
	        {
	            if ( $get[ 'bSortable_'.intval($get['iSortCol_'.$i]) ] == "true" )
	            {
	                $sOrder .= $aColumns[ intval( $get['iSortCol_'.$i] ) ]."
	                    ".($get['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
	            }
	        }
	         
	        $sOrder = substr_replace( $sOrder, "", -2 );
	        if ( $sOrder == "ORDER BY" )
	        {
	            $sOrder = "";
	        }
	    }

	    /*
	     * Filtering
	     * NOTE this does not match the built-in DataTables filtering which does it
	     * word by word on any field. It's possible to do here, but concerned about efficiency
	     * on very large tables, and MySQL's regex functionality is very limited
	     */
	    $sWhere = "";
	    if ( isset($get['sSearch']) && $get['sSearch'] != "" )
	    {
	        $sWhere = "WHERE (";
	        for ( $i=0 ; $i<count($aColumns) ; $i++ )
	        {
	            if ( isset($get['bSearchable_'.$i]) && $get['bSearchable_'.$i] == "true" )
	            {
	                $sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string( $get['sSearch'] )."%' OR ";
	            }
	        }
	        $sWhere .= " AND (".$where.")";
	        $sWhere = substr_replace( $sWhere, "", -3 );

	        $sWhere .= ')';
	    }else{

	    	if($where!=''){
	    		$sWhere = "WHERE (";
	    		$sWhere .= $where;
	    		$sWhere .= ')';
	    	}

	    }
	     
	    /* Individual column filtering */
	    

	   
	    
	    for ( $i=0 ; $i<count($aColumns) ; $i++ )
	    {
	        if ( isset($get['columns'][$i]["searchable"]) && $get['columns'][$i]["searchable"] == "true" && $get["search"]["value"] != '' && $aColumns[$i] !=' ')
	        {
	            if ( $sWhere == "" )
	            {
	                $sWhere = "WHERE ";
	            }
	            else
	            {
	                $sWhere .= " OR ";
	            }
	            $sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string($get["search"]["value"])."%' ";
	        }
	    }



	    /*
	     * SQL queries
	     * Get data to display
	     */
	    $sQuery = "
	        SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
	        FROM   $sTable
	        $sWhere
	        $sOrder
	        $sLimit
	    ";

	    $rResult=$connPDO->prepare($sQuery);
	    $rResult->execute();
	     
	    /* Data set length after filtering */
	    $sQuery = "
	        SELECT FOUND_ROWS()
	    ";
	    $stmt=$connPDO->prepare($sQuery);
	    $stmt->execute();
	 
	    $rResultFilterTotal = $stmt->fetch();
	   
	    $iFilteredTotal = $aResultFilterTotal[0];
	     
	    /* Total data set length */
	    $sQuery = "
	        SELECT COUNT(".$sIndexColumn.")
	        FROM   $sTable
	    ";
	    $stmt=$connPDO->prepare($sQuery);
	    $stmt->execute();
	    $rResultTotal = $stmt->fetchAll();
	    
	    $iTotal = $aResultTotal[0];
	     
	     
	    /*
	     * Output
	     */
	    $output = array(
	        "sEcho" => intval($get['sEcho']),
	        "iTotalRecords" => $iTotal,
	        "iTotalDisplayRecords" => $iFilteredTotal,
	        "aaData" => array()
	    );
	     
	    while ( $aRow = $rResult->fetch() )
	    {
	        $row = array();
	        for ( $i=0 ; $i<count($aColumns) ; $i++ )
	        {
	            if ( $aColumns[$i] == "version" )
	            {
	                /* Special output formatting for 'version' column */
	                $row[$aColumns[$i]] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
	            }
	            else if ( $aColumns[$i] != ' ' )
	            {
	                /* General output */
	                $row[$aColumns[$i]] = $aRow[ $aColumns[$i] ];

	            }
	        }
	        $row["btnChange"] = '<button type="button" class="btn btn-warning btn-xs resbtn openm"  data-placement="bottom"  title="Actualizar" data-id="'.$aRow["conse_pet"].'" data-target="#myModal"><i class="fa fa-external-link-square"></i></button>';
	        $row["tema"] = "";
	        $row["esta_pet"] = ucwords(strtolower(getCampo('parametros', "where id_param=29 and conse_param=".$aRow["esta_pet"],'desc_param')));
	        $row["respuesta"] = ""
	         ;

	         $row["fechaPeticion"] = $aRow["fecha_pet"]."- <b>".$aRow["hora_pet"]."</b>";
	        $output['aaData'][] = $row;
	    }

	    return json_encode( $output );

	/**datatable*/

	}

}

?>