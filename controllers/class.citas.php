<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');


class Citas
{
	public function __construct($mysql,$pdo)
	{
		$this->mysql=$mysql;
		$this->pdo=$pdo;
	}

	public function getCitas($data)
	{
		$diaf=getCampo('parametros',"where id_param=13 and conse_param=".date('m'),'mail_param',0);
		$condi="";
		$fecha=date("Y/m/d");
		$fecha1=date('Y-m-01');		
		$fecha2=date('Y-m-').$diaf;
		if($data["fechaIni"] && empty($data["fechaFin"]))
		{
			$condi.="AND fechacita = :fechacita";
		}
		if(!empty($data["fechaIni"]) && !empty($data["fechaFin"]))
		{
			$condi.=" AND fechacita between :fechaIni and :fechaFin";
		}
		if(empty($data["fechaIni"]) && empty($data["fechaFin"]))
		{
			$condi.=" AND fechacita between :fechaIni and :fechaFin";
		}
		/*if(!empty($data["asesor"]))
		{

			$condi.=" AND qasigna=:qasigna";
		}*/
		if(!empty($data["cliente"]) or $data["cliente"] != 0)
		{

			$condi.=" AND id_cliente=:id_cliente";
		}
		if(!empty($data["tarea"]) or $data["tarea"] != 0)
		{

			$condi.=" AND id_asunto=:id_asunto";
		}
		if(!empty($data["estado"]) and $data["estado"]!=0)
		{

			$condi.=" AND estado=:estado";
		}
		if($data['codigo'] or $data["codigo"] != 0)
		{
			$condi.=" AND id_inmueble=:id_inmueble";
		}

		

		if( isset($data["codigoCita"]) and !empty($data["codigoCita"])  ){
			$condi.=" AND id_cita = :id_cita ";
		}

		if(isset($data["asesores"]))
		{
			$asesores = "";
			$asesoresArray=Array();
			for ($i=0; $i <= count($data["asesores"]) ; $i++) { 
				$asesores .= $data["asesores"][$i].",";
			}

				
			
			$asesores = trim($asesores,",");

			$condi.= " AND qasigna IN (";
			
			$condi.= $asesores;

			$condi .= ")";
			
		}

		$stmt=$this->pdo->prepare("SELECT id_inmueble,horacita,qasigna,estado,id_cliente,id_asunto,id_cita,
					observaciones, fechacita, exito_cita
					from cita
					where inmobiliaria = :inmobiliaria
					$condi
					and id_asunto!=8
					order by id_inmueble,horacita
					LIMIT 0,1000");
		//print_r($stmt);
		$stmt->bindParam(':inmobiliaria',$_SESSION['IdInmmo']);
		if($data["fechaIni"] && empty($data["fechaFin"]))
		{
			$stmt->bindParam(":fechacita",$data["fechaIni"]);
		}
		if(!empty($data["fechaIni"]) && !empty($data["fechaFin"]))
		{
			$stmt->bindParam(":fechaIni",$data["fechaIni"]);
			$stmt->bindParam(":fechaFin",$data["fechaFin"]);
		}
		if(empty($data["fechaIni"]) && empty($data["fechaFin"]))
		{
			$stmt->bindParam(":fechaIni",$fecha1);
			$stmt->bindParam(":fechaFin",$fecha2);
		}
		/*if(!empty($data["asesor"]))
		{
			$stmt->bindParam(":qasigna",$data["asesor"]);

		}*/
		if(!empty($data["cliente"]) or $data["cliente"] != 0)
		{
			$stmt->bindParam(":id_cliente",$data["cliente"]);
		}
		if(!empty($data["tarea"]) or $data["tarea"] != 0)
		{
			$stmt->bindParam(":id_asunto",$data["tarea"]);
		}
		if(!empty($data["estado"]) and $data["estado"]!=0)
		{
			$stmt->bindParam(":estado",$data["estado"]);
		}

		//$codigoCita = '%'.utf8_decode($data["codigoCita"]).'%';

		if( isset($data["codigoCita"]) and !empty($data["codigoCita"])  ){
			
			$stmt->bindParam(":id_cita",$data["codigoCita"]);
		}

		if(isset($data["asesores"]))
		{
			/*$asesores = "";
			$asesoresArray=Array();
			foreach ($data["asesores"] as $key => $value) {

				if( !in_array($value,$asesoresArray) ){
					array_push($asesoresArray,$value);
					$asesores .= $value.",";
				}
			}
			$asesores .= trim($asesores,",");*/
			//$stmt->bindParam(":qasigna",$asesores);
		}

		if($data['codigo'])
		{
			
			$stmt->bindParam(":id_inmueble",$data["codigo"]);
		}

		
				

		//print_r($stmt);
		if($stmt->execute())
		{
			$telefonosCli="";
			while ($row=$stmt->fetch()) {
				
				$observacion=($row['observaciones']=="")?"Sin observación":$row['observaciones'];
				$dataUser=$this->getDataUser($row['qasigna']);
				$dataCliente=$this->getDataCliente($row['id_cliente']);
				
				if(!empty($dataCliente['telfijo']) && !empty($dataCliente['telcelular']))
				{
						$telefonosCli=$dataCliente['telfijo']."-".$dataCliente['telcelular'];
				}
				if(!empty($dataCliente['telfijo']) && empty($dataCliente['telcelular']))
				{
						$telefonosCli=$dataCliente['telfijo'];					
				}
				if(empty($dataCliente['telfijo']) && !empty($dataCliente['telcelular']))
				{
						$telefonosCli=$dataCliente['telcelular'];
				}
				
				$tarea=getCampo('asunto_cita',"where est_asunto=1 AND id_asunto =".$row['id_asunto'],'descripcion');
				$datetime = new DateTime($row['fechacita']." ".$row['horacita']);
				if($data["type"]==2){
					$tarea = substr(utf8_encode($tarea), 0, 40)." - ".ucwords(strtolower($dataUser['nom']))." - Cliente: ".ucwords(strtolower($dataCliente['nombre']))." - Teléfono: ".trim($telefonosCli,"-");
				}else{
					$tarea = substr(utf8_encode($tarea), 0, 40);
				}

				if( $data["retro"] == 1 ){

					$cita["idcita"] = $row['id_cita'];
					$citaCount= $this->getRetroCount($cita);
					
					if( $citaCount["total"] == 0 ){

						$datas[]=array(
							"id"          =>'2',
							"idCita"      =>$row['id_cita'],
							"hora"        =>$row['horacita'],
							"fechCita"    =>$row['fechacita'],
							"title"       =>$tarea, 
							//"start"       =>$row['fechacita']."T".$row['horacita'],
							"start"       =>$datetime->format(DateTime::ATOM),
							"end"         =>'',
							"color"       =>$dataUser['coloragenda'],
							"id_inmueble" =>$row['id_inmueble'],
							"qasigna"     =>$row['qasigna'],
							"NombreAse"   =>$dataUser['nom']." ",
							"idUser"      =>$dataUser['idUser'],
							"fotoUser"    =>$dataUser['Foto'],
							"description" =>$observacion,
							"estado"      =>$row['estado'],
							"id_cliente"  =>$row['id_cliente'],
							"id_asunto"   =>$row['id_asunto'],
							"nombreCli"   =>$dataCliente['nombre'],
							"mailCli"     =>$dataCliente['email'],
							"telefonoCli" =>$telefonosCli,
							"exito_cita"  =>$row["exito_cita"]

						);

					}



				}else{

					if( $data["retro"] == 2){

						$cita["idcita"] = $row['id_cita'];
						$citaCount= $this->getRetroCount($cita);
						
						if( $citaCount["total"] > 0 ){

							$datas[]=array(
								"id"          =>'2',
								"idCita"      =>$row['id_cita'],
								"hora"        =>$row['horacita'],
								"fechCita"    =>$row['fechacita'],
								"title"       =>$tarea, 
								//"start"       =>$row['fechacita']."T".$row['horacita'],
								"start"       =>$datetime->format(DateTime::ATOM),
								"end"         =>'',
								"color"       =>$dataUser['coloragenda'],
								"id_inmueble" =>$row['id_inmueble'],
								"qasigna"     =>$row['qasigna'],
								"NombreAse"   =>$dataUser['nom']." ",
								"idUser"      =>$dataUser['idUser'],
								"fotoUser"    =>$dataUser['Foto'],
								"description" =>$observacion,
								"estado"      =>$row['estado'],
								"id_cliente"  =>$row['id_cliente'],
								"id_asunto"   =>$row['id_asunto'],
								"nombreCli"   =>$dataCliente['nombre'],
								"mailCli"     =>$dataCliente['email'],
								"telefonoCli" =>$telefonosCli,
								"exito_cita"  =>$row["exito_cita"]

							);

						}



					}else{

						$datas[]=array(
							"id"          =>'2',
							"idCita"      =>$row['id_cita'],
							"hora"        =>$row['horacita'],
							"fechCita"    =>$row['fechacita'],
							"title"       =>$tarea, 
							//"start"       =>$row['fechacita']."T".$row['horacita'],
							"start"       =>$datetime->format(DateTime::ATOM),
							"end"         =>'',
							"color"       =>$dataUser['coloragenda'],
							"id_inmueble" =>$row['id_inmueble'],
							"qasigna"     =>$row['qasigna'],
							"NombreAse"   =>$dataUser['nom']." ",
							"idUser"      =>$dataUser['idUser'],
							"fotoUser"    =>$dataUser['Foto'],
							"description" =>$observacion,
							"estado"      =>$row['estado'],
							"id_cliente"  =>$row['id_cliente'],
							"id_asunto"   =>$row['id_asunto'],
							"nombreCli"   =>$dataCliente['nombre'],
							"mailCli"     =>$dataCliente['email'],
							"telefonoCli" =>$telefonosCli,
							"exito_cita"  =>$row["exito_cita"]

						);

					}

					


				}

				

			}
			 return $datas;
			
		}
		else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$data
			);
			return $response;
		}
	}

	public function getRetroCount($data){

		$stmt=$this->pdo->prepare("SELECT count(*) as total
					from retro_cita
					where idcita=:id_cita");
		$stmt->bindParam(':id_cita',$data['idcita']);

		if($stmt->execute())
		{
			return $stmt->fetch();
		}


	}
	public function getCitasId($data)
	{
		
		

		$stmt=$this->pdo->prepare("SELECT id_inmueble,horacita,qasigna,qasignan,estado,id_cliente,id_asunto,id_cita,
					observaciones, fechacita
					from cita
					where inmobiliaria = :inmobiliaria
					and id_cita=:id_cita
					order by id_inmueble,horacita
					LIMIT 0,100");
		
		$stmt->bindParam(':inmobiliaria',$_SESSION['IdInmmo']);
		$stmt->bindParam(':id_cita',$data['idcita']);
		
		if($stmt->execute())
		{
			$telefonosCli="";
			while ($row=$stmt->fetch()) {
				
				$observacion=($row['observaciones']=="")?"Sin observación":$row['observaciones'];
				$dataUser=$this->getDataUser($row['qasigna']);
				$dataCliente=$this->getDataCliente($row['id_cliente']);
				
				if(!empty($dataCliente['telfijo']) && !empty($dataCliente['telcelular']))
				{
						$telefonosCli=$dataCliente['telfijo']."-".$dataCliente['telcelular'];
				}
				if(!empty($dataCliente['telfijo']) && empty($dataCliente['telcelular']))
				{
						$telefonosCli=$dataCliente['telfijo'];					
				}
				if(empty($dataCliente['telfijo']) && !empty($dataCliente['telcelular']))
				{
						$telefonosCli=$dataCliente['telcelular'];
				}
				
				$tarea=getCampo('asunto_cita',"where est_asunto=1 AND id_asunto =".$row['id_asunto'],'descripcion');
				$datas[]=array(
						"id"          =>'2',
						"idCita"      =>$row['id_cita'],
						"hora"        =>$row['horacita'],
						"fechCita"    =>$row['fechacita'],
						"title"       =>substr(utf8_encode($tarea), 0, 40), 
						"start"       =>$row['fechacita']."T".$row['horacita'],
						"end"         =>'',
						"color"       =>$dataUser['coloragenda'],
						"id_inmueble" =>$row['id_inmueble'],
						"qasigna"     =>$row['qasigna'],
						"qasignan"     =>$row['qasignan'],
						"NombreAse"   =>$dataUser['nom'],
						"idUser"      =>$dataUser['idUser'],
						"fotoUser"    =>$dataUser['Foto'],
						"description" =>$observacion,
						"estado"      =>$row['estado'],
						"id_cliente"  =>$row['id_cliente'],
						"id_asunto"   =>$row['id_asunto'],
						"nombreCli"   =>$dataCliente['nombre'],
						"mailCli"     =>$dataCliente['email'],
						"telefonoCli" =>$telefonosCli,

					);

			}
			 return $datas;
			
		}
		else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$data
			);
			return $response;
		}
	}
	public function getDataUser($iduser)
	{
		$stmt=$this->pdo->prepare('
				SELECT coloragenda,
				Foto,
				concat(Nombres," ",apellidos) as nom,
				iduser
				FROM usuarios
				WHERE Id_Usuarios=:Id_Usuarios
				AND IdInmmo = :IdInmmo ');

		if($stmt->execute(array(
			":Id_Usuarios"=>$iduser,
			":IdInmmo"=>$_SESSION["IdInmmo"]
			)))
		{
			while ($row = $stmt->fetch()) {
				$url="https://www.simiinmobiliarias.com/mcomercialweb/";
				$pos = strpos($row['coloragenda'], "#");
				$colorAgenda=($pos===false)?"#20D2FE":$row['coloragenda'];
				$imgProfile=($row['Foto']=="")?$url."FotosProfile/user.png":$url.$row['Foto'];
				$data=array(
						"coloragenda"=>$colorAgenda,
						"Foto"=>$imgProfile,
						"nom"=>ucwords(strtolower(utf8_encode($row['nom']))),
						"idUser"=>$row['iduser'],
					);
			}
		return $data;
		}else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$iduser
			);
			return $response;
		}
	}
	public function getDataCliente($idclinte)
	{
		$stmt=$this->pdo->prepare('
				SELECT nombre,direccion,telfijo,email,telcelular
				FROM clientes_inmobiliaria
				WHERE cedula=:cedula
				AND inmobiliaria = :inmobiliaria ');

		if($stmt->execute(array(
			":cedula"=>$idclinte,
			":inmobiliaria"=>$_SESSION["IdInmmo"]
			)))
		{
			while ($row = $stmt->fetch()) {
				$data=array(
					"nombre"     =>$row['nombre'],
					"direccion"  =>$row['direccion'],
					"telfijo"    =>$row['telfijo'],
					"email"      =>$row['email'],
					"telcelular" =>$row['telcelular'],
					);
			}
		return $data;
		}else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$iduser
			);
			return $response;
		}
	}
	public function saveCliente($data)
	{
		

		$idCliente=consecutivo("cedula","clientes_inmobiliaria");
		
		$stmt=$this->pdo->prepare('INSERT INTO clientes_inmobiliaria (cedula,nombre,emp_cli,telfijo,telcelular,email,idmedio,inmobiliaria,ase_cli,usu_crea) 
			VALUES (:cedula,:nombre,:empresa,:telfijo,:telecelular,:email,:idmedio,:idinmo,:ase_cli,:usu_crea)');
		if($stmt->execute(array(
			":cedula"=>$idCliente,
			":nombre"=>$data['nombreClient'],
			":empresa"=>$data['empresaClient'],
			":telfijo"=>$data['telfijoClient'],
			":telecelular"=>$data['telcelClient'],
			":email"=>$data['emailClient'],
			":idmedio"=>$data['nompubliClient'],
			":idinmo"=>$_SESSION['IdInmmo'],
			":ase_cli"=>$data['ase_cliClient'],
			":usu_crea"=>$_SESSION['codUser'],	
			)))
		{
			return $idCliente;
		}else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$data
			);
			return $response;
		}
	}
	public function getRetroCita($data)
	{
		$datass=10875;
		$stmt=$this->pdo->prepare('SELECT fecharetro, IdUsuRetro, idconcepto, 
								   resultado, id_descripcion, fecha_actividad  
								   FROM retro_cita
								   WHERE idcita = :idcita ');
		if($stmt->execute(array(
			":idcita"=>$data['idcita']
			)))
		{
			
			if($stmt->rowCount()>0)
			{

				while ($row = $stmt->fetch()) {
					

					$conceptocita=getCampo('ConceptosCita',"where IdConcepto=".$row['idconcepto'],'Concepto');
					$asuntocita=getCampo('cita_inmueble',"where id_descripcion=".$row['id_descripcion'],'descripcion');
					$usuarioNom=getCampo('usuarios',"where Id_Usuarios=".$row['IdUsuRetro'],'Nombres');
					$usuarioApe=getCampo('usuarios',"where Id_Usuarios=".$row['IdUsuRetro'],'apellidos');
					$datas[]=array(
						"fecharetro"     =>$row['fecharetro'],
						"IdUsuRetro"     =>ucwords(strtolower($usuarioNom." ".$usuarioApe)),
						"idconcepto"     =>$conceptocita,
						"id_descripcion" =>$asuntocita,
						"resultado"      =>($row['resultado']=="")?"":utf8_encode($row['resultado']),
						"fecha_actividad"=>$row['fecha_actividad']
						);

				}	
			}
			else
			{
				$datas=0;
			}
			return $datas;
		}
		else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$data
			);
			return $response;
		}
	}
	public function saveRetroCita($data)
	{
		

		$fechaAct=date("Y-m-d");
		$horaAct=date("H:i:s");
		$consecitaRetro=consecutivo("idretro","retro_cita");
		$stmt=$this->pdo->prepare('INSERT INTO retro_cita (idretro,idconcepto,id_descripcion,resultado,
			idcita,IdUsuRetron,IdUsuRetro,fecharetro,horaretro,fecha_actividad) VALUES (:idretro, :idconcepto, :id_descripcion, :resultado, :idcita, :IdUsuRetron, :IdUsuRetro, :fecharetro, :horaretro, :fecha_actividad)');
		if($stmt->execute(array(
				":idretro"=>$consecitaRetro,
				":idconcepto"=>$data['retroConceptoCit'],
				":id_descripcion"=>$data['retroResultCita'],
				":resultado"=>$data['retroObser'],
				":idcita"=>$data['idcita'],
				":IdUsuRetron"=>$_SESSION['codUser'],
				":IdUsuRetro"=>$_SESSION['Id_Usuarios'],
				":fecharetro"=>$fechaAct,
				":horaretro"=>$horaAct,
				":fecha_actividad"=>$data['fechaRetroSegui'],	
			)))
		{
			if($stmt->rowCount()>0)
			{
				return 1;
			}else
			{
				return 0;
			}
		}else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$consecitaRetro
			);
			return $response;
		}
	}
	public function saveCita($data)
	{
		
		$fechaAct=date("Y-m-d");
		$horaAct=date(" H:i:s");
		$consecita=consecutivo("id_cita","cita");
		$ase=getCampo('usuarios',"where  iduser =".$data['citaAsesor'],'Id_Usuarios');
		$idusuarioAse=($ase=='')?0:$ase;
		$stmt=$this->pdo->prepare("INSERT INTO cita (id_cita,id_captador,id_asunto,fechaprog,fechacita,
			horaprog,horacita,qregistran,qregistra,qasigna,qasignan,id_inmueble,
			estado,id_cliente,inmobiliaria,observaciones,idconceptollamada) 
			VALUES (:id_cita,:id_captador,:id_asunto,:fechaprog,:fechacita,
			:horaprog,:horacita,:qregistran,:qregistra,:qasigna,:qasignan,:id_inmueble,
			:estado,:id_cliente,:inmobiliaria,:observaciones,:idconceptollamada)");
				
			if($stmt->execute(array(
				":id_cita"           =>$consecita,
				":id_captador"       =>$_SESSION['Id_Usuarios'],
				":id_asunto"         =>$data['citaTarea'],
				":fechaprog"         =>$fechaAct,
				":fechacita"         =>$data['citaFecha'],
				":estado"            =>1,
				":horaprog"          =>$fechaAct,
				":horacita"          =>$data['citaHora'],
				":qregistran"        =>$_SESSION['codUser'],
				":qregistra"         =>$_SESSION['Id_Usuarios'],
				":qasigna"           =>$idusuarioAse,
				":qasignan"          =>$data['citaAsesor'],
				":id_inmueble"       =>$data['citaCodInm'],
				":id_cliente"        =>$data['citaCliente'],
				":inmobiliaria"      =>$_SESSION['IdInmmo'],
				":observaciones"     =>$data['citaObser'],
				":idconceptollamada" =>0,
				)))

			{
				if($stmt->rowCount()>0)
				{
					return $consecita;
				}else
				{
					return 0;
				}

			}else
			{
				$response=array(
				"error"=>$stmt->errorInfo(),
				"info"=>$data
				);
				return $response;
			}
	}
	public function updateCita($data)
	{
			
		//guardado de log antes de guardar la cita
				$cita = $this->getCitasId($data);
				if($cita[0]["id_cliente"] != $data['citaCliente']){
					$cons_log=consecutivo_log(2);
					$nom_campo="id_cliente";
					$fecha_sis			= date('Y-m-d');
					$hora_sis			= date("G:i:s");
					$usuario			= $_SESSION['codUser'];
					$ip					= $_SERVER['REMOTE_ADDR'];
					$id_editacita		= $data['idcita'];
					$campo_usu_g="citaCliente"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla parametros
					$this->guarda_log_gral($cons_log,2,$nom_campo,$cita[0]["id_cliente"],$data['citaCliente'],$fecha_sis,$hora_sis,$usuario,$ip,$id_editacita,$campo_usu_g,$param_g,1);
				}

				

				if($cita[0]["qasignan"] != $data['citaAsesor']){
					$cons_log=consecutivo_log(2);
					$nom_campo="qasignan";
					$fecha_sis			= date('Y-m-d');
					$hora_sis			= date("G:i:s");
					$usuario			= $_SESSION['codUser'];
					$ip					= $_SERVER['REMOTE_ADDR'];
					$id_editacita		= $data['idcita'];
					$campo_usu_g="citaAsesor"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla parametros
					$this->guarda_log_gral($cons_log,2,$nom_campo,$cita[0]["qasignan"],$data['citaAsesor'],$fecha_sis,$hora_sis,$usuario,$ip,$id_editacita,$campo_usu_g,$param_g,1);
				}

				if($cita[0]["id_asunto"] != $data['citaTarea']){
					$cons_log=consecutivo_log(2);
					$nom_campo="id_asunto";
					$fecha_sis			= date('Y-m-d');
					$hora_sis			= date("G:i:s");
					$usuario			= $_SESSION['codUser'];
					$ip					= $_SERVER['REMOTE_ADDR'];
					$id_editacita		= $data['idcita'];
					$campo_usu_g="citaTarea"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla parametros
					$this->guarda_log_gral($cons_log,2,$nom_campo,$cita[0]["id_asunto"],$data['citaTarea'],$fecha_sis,$hora_sis,$usuario,$ip,$id_editacita,$campo_usu_g,$param_g,1);
				}

				if($cita[0]["fechCita"] != $data['citaFecha']){
					$cons_log=consecutivo_log(2);
					$nom_campo="fechCita";
					$fecha_sis			= date('Y-m-d');
					$hora_sis			= date("G:i:s");
					$usuario			= $_SESSION['codUser'];
					$ip					= $_SERVER['REMOTE_ADDR'];
					$id_editacita		= $data['idcita'];
					$campo_usu_g="citaFecha"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla parametros
					$this->guarda_log_gral($cons_log,2,$nom_campo,$cita[0]["fechCita"],$data['citaFecha'],$fecha_sis,$hora_sis,$usuario,$ip,$id_editacita,$campo_usu_g,$param_g,1);
				}

				if($cita[0]["hora"] != $data['citaHora']){
					$cons_log=consecutivo_log(2);
					$nom_campo="hora";
					$fecha_sis			= date('Y-m-d');
					$hora_sis			= date("G:i:s");
					$usuario			= $_SESSION['codUser'];
					$ip					= $_SERVER['REMOTE_ADDR'];
					$id_editacita		= $data['idcita'];
					$campo_usu_g="citaHora"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla parametros
					$this->guarda_log_gral($cons_log,2,$nom_campo,$cita[0]["hora"],$data['citaHora'],$fecha_sis,$hora_sis,$usuario,$ip,$id_editacita,$campo_usu_g,$param_g,1);
				}

				if($cita[0]["id_inmueble"] != $data['citaCodInm']){
					$cons_log=consecutivo_log(2);
					$nom_campo="id_inmueble";
					$fecha_sis			= date('Y-m-d');
					$hora_sis			= date("G:i:s");
					$usuario			= $_SESSION['codUser'];
					$ip					= $_SERVER['REMOTE_ADDR'];
					$id_editacita		= $data['idcita'];
					$campo_usu_g="citaCodInm"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla parametros
					$this->guarda_log_gral($cons_log,2,$nom_campo,$cita[0]["id_inmueble"],$data['citaCodInm'],$fecha_sis,$hora_sis,$usuario,$ip,$id_editacita,$campo_usu_g,$param_g,1);
				}
				
				if($cita[0]["description"] != $data['citaObser']){
					$cons_log=consecutivo_log(2);
					$nom_campo="description";
					$fecha_sis			= date('Y-m-d');
					$hora_sis			= date("G:i:s");
					$usuario			= $_SESSION['codUser'];
					$ip					= $_SERVER['REMOTE_ADDR'];
					$id_editacita		= $data['idcita'];
					$campo_usu_g="citaObser"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla parametros
					$this->guarda_log_gral($cons_log,2,$nom_campo,$cita[0]["description"],$data['citaObser'],$fecha_sis,$hora_sis,$usuario,$ip,$id_editacita,$campo_usu_g,$param_g,1);

				}
				


		$idusuarioAse=$tarea=getCampo('usuarios',"where  iduser =".$data['citaAsesor'],'Id_Usuarios');
		$stmt=$this->pdo->prepare("UPDATE cita
		SET id_asunto=:id_asunto,
		fechacita=:fechacita,
		horacita=:horacita,
		qasigna=:qasigna,
		qasignan=:qasignan,
		id_inmueble=:id_inmueble,
		id_cliente=:id_cliente,
		observaciones=:observaciones
		WHERE id_cita=:id_cita
		AND inmobiliaria=:inmobiliaria");

		if($stmt->execute(array(
				":id_asunto"=>$data['citaTarea'],
				":fechacita"=>$data['citaFecha'],
				":horacita"=>$data['citaHora'],
				":qasigna"=>$idusuarioAse,
				":qasignan"=>$data['citaAsesor'],
				":id_inmueble"=>$data['citaCodInm'],
				":id_cliente"=>$data['citaCliente'],
				":observaciones"=>$data['citaObser'],
				":id_cita"=>$data['idcita'],
				":inmobiliaria"=>$_SESSION['IdInmmo'],
			)))
		{
			if($stmt->rowCount()>0)
			{
				


				return (int)$data['idcita'];

			}else
			{
				return "deerewrwer";
			}
		}else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$data
			);
			return $response;
		}
	}
	public function closeCita($data)
	{
		$stmt=$this->pdo->prepare("UPDATE cita 
		SET estado=2
		WHERE id_cita=:id_cita
		AND inmobiliaria=:inmobiliaria");

		if($stmt->execute(array(
			":id_cita"=>$data['idcita'],
			":inmobiliaria"=>$_SESSION['IdInmmo']
			)))
		{
			if($stmt->rowCount())
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$data
			);
			return $response;
		}
	}

	public function getDaysEnabled($data)
	{

		$day=getCampo('inmobiliaria',"where IdInmobiliaria=".$_SESSION["IdInmmo"],'rgcita');
		$resp[0] = $day;
		$resp[1] = $day;
		return $resp;

	}
	public function exitoCita($data)
	{
		$stmt=$this->pdo->prepare("UPDATE cita
			SET 
			exito_cita	= :exito
			where id_cita=:id_cita");

		if($stmt->execute(array(
			":id_cita"=>$data['idcita'],
			":exito"=>$data['exito']
			)))
		{
			if($stmt->rowCount())
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$data
			);
			return $response;
		}
	}

	public function guarda_log_gral($cons_log,$tabla,$nom_campo,$esta_ini_g,$esta_fin_g,$fecha_g,$hora_g,$usuario_g,$ip_g,$client_g,$campo_usu_g,$param_g,$mostrar='')
	{
		//global $w_conexion;
		//
		//
		$fechaAct=date("Y-m-d");
		$horaAct=date(" H:i:s");
		$consecita=consecutivo("id_cita","cita");
		$ase=getCampo('usuarios',"where  iduser =".$data['citaAsesor'],'Id_Usuarios');
		$idusuarioAse=($ase=='')?0:$ase;
		$stmt=$this->pdo->prepare("INSERT INTO lg_ch_tb (cons_log,tipo_log,campo_g,esta_ini_g,esta_fin_g,
	              fecha_g,hora_g,usuario_g,ip_g,client_g,
	              campo_usu_g,param_g)
			VALUES (:cons_log,:tipo_log,:campo_g,:esta_ini_g,:esta_fin_g,
			:fecha_g,:hora_g,:usuario_g,:ip_g,:client_g,:campo_usu_g,:param_g)");
				
			if($stmt->execute(array(
				":cons_log"           =>$cons_log,
				":tipo_log"       =>$tabla,
				":campo_g"         =>$nom_campo,
				":esta_ini_g"         =>$esta_ini_g,
				":esta_fin_g"         =>$esta_fin_g,
				":fecha_g"            =>$fecha_g,
				":hora_g"          =>$hora_g,
				":usuario_g"          =>$usuario_g,
				":client_g"        =>$client_g,
				":ip_g"        =>$ip_g,
				":campo_usu_g"         =>$campo_usu_g,
				":param_g"           =>$param_g,
				)))

				 

			{
				if($stmt->rowCount()>0)
				{
					return $consecita;
				}else
				{
					return 0;
				}

			}else
			{
				$response=array(
				"error"=>$stmt->errorInfo(),
				"info"=>$data
				);
				return $response;
			}

		
	}  

	public function logCita($data){

		$stmt=$this->pdo->prepare('
				SELECT tipo_log,campo_g,esta_ini_g,esta_fin_g, fecha_g, client_g, usuario_g
				FROM lg_ch_tb
				WHERE client_g=:client_g
				AND tipo_log = 2 ');

		if($stmt->execute(array(
			":client_g"=>$data["idcita"]
			)))
		{
			while ($row = $stmt->fetch()) {

				switch ($row['campo_g']) {
					case 'qasignan'://asesor
						$campo = "Asesor";
						break;
					case 'description'://observacion
						$campo = "Observación";
						break;
					case 'id_cliente'://asesor
						$campo = "Cliente";
						break;
					case 'id_asunto':
						$campo = "Tarea";
						break;
					case 'fechCita':
						$campo = "Fecha Cita";
						break;
					case 'hora':
						$campo = $row['campo_g'];
						break;
					case 'id_inmueble':
						$campos = "Inmueble";
						break;
					default:
						$campo = $row['campo_g'];
						break;
				}

				if( $row['campo_g'] ){

				}else{
					$campo = $row['campo_g'];
				}
				$usuarioNom=getCampo('usuarios',"where Id_Usuarios=".$row['usuario_g'],'concat(Nombres," ",apellidos)');
				//$usuarioApe=getCampo('usuarios',"where Id_Usuarios=".$row['IdUsuRetro'],'apellidos');
				$sta_ini="";
				$sta_fin="";
				if($campo=="Asesor")
				{
					
					$sta_ini=getCampo('usuarios',"where idUser=".$row['esta_ini_g'],'concat(Nombres," ",apellidos)');
					$sta_fin=getCampo('usuarios',"where idUser=".$row['esta_fin_g'],'concat(Nombres," ",apellidos)');
					
					
					
					
				}
				if($campo=="Cliente")
				{
					$sta_ini=getCampo('clientes_inmobiliaria',"where cedula=".$row['esta_ini_g'],'concat(nombre)');
					$sta_fin=getCampo('clientes_inmobiliaria',"where cedula=".$row['esta_fin_g'],'concat(nombre)');
				}
				if($campo != "Asesor" && $campo != "Cliente")
				{	
					
					$sta_ini=$row['esta_ini_g'];
					$sta_fin=$row['esta_fin_g'];
						
				}
				$datas[]=array(
					"campo_g"     =>$campo,
					"esta_ini_g"  =>$sta_ini,
					"esta_fin_g"    =>$sta_fin,
					"fecha_g"      =>$row['fecha_g'],
					"client_g" =>$row['client_g'],
					"usuario_g" =>$usuarioNom,
					);
			}
		
		return $datas;
		}else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$iduser
			);
			return $response;
		}

	}
	public function historialClienteCitas($datas){
		$stmt=$this->pdo->prepare('
				 SELECT c.estado, ci.cedula, c.id_cita AS Cita, c.id_asunto, c.id_captador, c.observaciones, u.Id_Usuarios, c.id_inmueble AS id_inmueble, ci.nombre AS Interesado, a.descripcion AS Asunto, c.fechacita AS Fecha, c.horacita AS Hora, i.Direccion, cii.descripcion,cii.id_descripcion, inm.NombreInm, re.resultado, us.Nombres, us.apellidos,re.idconcepto
			FROM cita c
			LEFT JOIN usuarios u ON c.id_captador = u.Id_Usuarios
			LEFT JOIN asunto_cita a ON c.id_asunto = a.id_asunto
			LEFT JOIN clientes_inmobiliaria ci ON c.id_cliente = ci.cedula
			LEFT JOIN retro_cita re ON c.id_cita = re.idcita
			LEFT JOIN cita_inmueble cii ON re.id_descripcion = cii.id_descripcion
			LEFT JOIN inmuebles i ON c.id_inmueble = i.idInm
			LEFT JOIN barrios b ON i.IdBarrio = b.IdBarrios
			INNER JOIN inmobiliaria inm ON c.inmobiliaria = inm.IdInmobiliaria
			INNER JOIN usuarios us ON  c.id_captador = us.Id_Usuarios
	  	    WHERE c.id_cliente=:idcliente AND c.inmobiliaria=:idInmo AND c.id_asunto NOT IN(7,8)');

		if($stmt->execute(array(
			":idcliente"=>$datas['idcliente'],
			":idInmo"=>$_SESSION['IdInmmo']
			)))
		{
			while ($row=$stmt->fetch()) {
				$response[]=array(
					"fecha"      =>$row['Fecha'],
					"hora"       =>$row['Hora'],
					"actividad"  =>$row['Asunto'],
					"idinmueble" =>$row['id_inmueble'],
					"Concepto"   => getCampo('asunto_cita',"where id_asunto=".$fila7['id_asunto'],'descripcion'),
					"atendio"    =>$row['Nombres'].$row['apellidos'],
					"resultado"  =>$row['resultado'],
					"cliente"    =>$this->getDataCliente($datas['idcliente'])
					);
			}
			return $response;
		}else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$datas
			);
			return $response;
		}


	}

}
	