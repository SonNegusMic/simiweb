<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');


class Precapta
{
    public function __construct($conn){
		$this->db=$conn;
	}
    
    private $id_usuario;
    
	public function actualizaTenedor($tel,$inmob,$nombre,$tel2,$tel3,$dir,$correo,$codInterno,$codpr)
		{
			
				 if(($consulta=$this->db->prepare("replace INTO cliente_captacion (id_cliente_capta,telefono,telefono1,telefono2,correo,
				 nombre,id_inmobiliaria,direccion) 
			values(?,?,?,?,?,
			?,?,?)
	")))
			{  	
				if(!$consulta->bind_param("isssssis",$codInterno,trim($tel),trim($tel2),trim($tel3),trim($correo),trim($nombre),$inmob,($dir)))
				{
					echo "error bind";
				}
				else
				{ 
					if($consulta->execute())
					{
						///////////--------------------
						if($codpr>0)
						{
							if(($consulta2=$this->db->prepare("update seguimiento_precapta 
																SET id_cliente_capta=?
																WHERE id_inmueble=?")))
							{  	
								if(!$consulta2->bind_param("ii",$codInterno,$codpr))
								{
									echo "error bind";
								}
								else
								{ 
									if($consulta2->execute())
									{
										echo "Registro Actualizado";
										
									}
									else
									{
										echo "error exce ".$consulta2->error;
									}
									
								}
							}
						}
						//////////---------------------
					}
					else
					{
						echo "error exce $tpc".$consulta->error;
					}
					
				}
			}
		}
		public function crearTenedor($tel,$inmob,$nombre,$tel2,$tel3,$dir,$correo,$codpr)
		{
			//$nombre=ucwords(strtolower($nombre));
			$conse=consecutivo("id_cliente_capta","cliente_captacion");
			 if(($consulta=$this->db->prepare("INSERT INTO cliente_captacion (id_cliente_capta,telefono,telefono1,telefono2,correo,
			 nombre,id_inmobiliaria,direccion) 
			values(?,?,?,?,?,
			?,?,?)
	")))
			{  	
				if(!$consulta->bind_param("iiiissis",$conse,trim($tel),trim($tel2),trim($tel3),trim($correo),trim($nombre),$inmob,trim($dir)))
				{
					echo "error bind";
				}
				else
				{ 
					if($consulta->execute())
					{
						echo "Creado Tenedor $conse";
						//echo "Registro $idCaracteristica $descc ha sido Creado $idGrupo";
						///////////--------------------
						if($codpr>0)
						{
							if(($consulta2=$this->db->prepare("update seguimiento_precapta 
																SET id_cliente_capta=?
																WHERE id_inmueble=?")))
							{  	
								if(!$consulta2->bind_param("ii",$conse,$codpr))
								{
									echo "error bind";
								}
								else
								{ 
									if($consulta2->execute())
									{
										//echo "Registro  ha sido Creado BIEN";
										
									}
									else
									{
										echo "error exce ".$consulta2->error;
									}
									
								}
							}
						}
						//////////---------------------
						
					}
					else
					{
						echo "error exce $tpc".$consulta->error;
					}
					
				}
			}
		}
    
	public function consultaTenedor($dato,$inmob)
    {
       $w_conexion = new MySQL();
	  
	   //$array[]="";
	   $cadena="";
        $sql="SELECT c.nombre,c.telefono,c.telefono1,c.telefono2,c.correo,c.direccion,id_cliente_capta
				FROM cliente_captacion c
				WHERE c.id_inmobiliaria='$inmob' 
				and c.telefono LIKE '%$dato%' 
				ORDER BY c.nombre desc";
		$res	= $w_conexion->ResultSet($sql);
		$existe	= $w_conexion->FilasAfectadas($res);
		if(!$res)
		{
			echo "error bind";
		}
		else
		{ 
			//echo $sql;
			//echo $existe. "existe";
			if($existe==0)
			{
				echo "Numero Nuevo";
			}
			else
			{
				while($ff=$w_conexion->FilaSiguienteArray($res))
				{ 
					 
					 $telefono 		= trim($ff['telefono']);
					 $nombre 		= trim($ff['nombre']);
					 $telefono1 	= trim($ff['telefono1']);
					 $telefono2 	= trim($ff['telefono2']);
					 $correo 		= trim($ff['correo']);
					 $direccion 	= trim($ff['direccion']);
					 $id_cli		= trim($ff['id_cliente_capta']);
				}
				$cadena="$telefono ? $nombre ? $telefono1 ? $telefono2 ? $correo ? $direccion ? $id_cli";
				echo $cadena;
				//print_r($array)."ff";
			}
		}
		
    }
    public function getTelefonosTenedor()
    {
       $w_conexion = new MySQL();
	  
	   //$array[]="";
	   $cadena="";
	   $idInmo= $_SESSION['IdInmmo'];
        $sql="SELECT c.nombre,c.telefono,c.telefono1,c.telefono2,c.correo,c.direccion,id_cliente_capta
				FROM cliente_captacion c
				WHERE c.id_inmobiliaria= $idInmo
				ORDER BY c.nombre desc";
		$res	= $w_conexion->ResultSet($sql);
		$existe	= $w_conexion->FilasAfectadas($res);
		if(!$res)
		{
			echo "error bind";
		}
		else
		{ 
			//echo $sql;
			//echo $existe. "existe";
			$data=array();
			if($existe==0)
			{
				echo "Numero Nuevo";
			}
			else
			{
				while($ff=$w_conexion->FilaSiguienteArray($res))
				{ 
					 
					 if($ff['telefono']  > 0)
					 {

					 	 array_push($data, (trim(intval($ff['telefono']))));
					 }
				}
				return $data;
			}
		}
		
    }
	public function crearProspecto($barrio,$fecha,$dirp,$telp,$mailp,$IdTipoInm,$idgestion,$medio,$inmob,$ase,$id_clipre,$telp2,$telc,$mailp2)
    {
        $id_inmueble=consecutivo("id_inmueble","inmueble_precapta");
		$id_estado= 8;
		
			 if(($consulta=$this->db->prepare("INSERT INTO inmueble_precapta 
			 (id_inmueble,id_inmobiliaria,id_barrio,direcion,fingreso,
			 id_estado,telefono,telefono2,telefono3,correo,
			 id_gestion,id_tipo,IdCaptador,medio,correo2
	) 
        values(?,?,?,?,?,
        ?,?,?,?,?,
        ?,?,?,?,?)")))
		{  	
			if(!$consulta->bind_param("iiississssiiiis",$id_inmueble,$inmob,$barrio,$dirp,$fecha,$id_estado,$telp,$telp2,$telc,$mailp,$idgestion,$IdTipoInm,$ase,$medio,$mailp2))
			{
				echo "error bind";
			}
			else
			{ 
				if($consulta->execute())
				{
					/////////////--------------------
					$id_seg=consecutivo("id_seguimiento","seguimiento_precapta");
					$id_usuario= $_SESSION['Id_Usuarios'];
					$est=1;
					$res=0;
					$id_clipre=0;
					if(($consulta2=$this->db->prepare("INSERT INTO seguimiento_precapta
					(id_seguimiento,id_usuario,estado_captacion,fecha,id_inmueble,
							 id_medio,id_cliente_capta,id_resultado) 
						values(?,?,?,?,?,
						?,?,?)")))
						{  	
							if(!$consulta2->bind_param("iiissiii",$id_seg,$id_usuario,$est,$fecha,$id_inmueble,$medio,$id_clipre,$res))
							{
								echo "error bind";
							}
							else
							{ 
								if($consulta2->execute())
								{
									//echo "Registro  ha sido Creado BIEN";
									
								}
								else
								{
									echo "error exce $barrio".$consulta2->error;
								}
								
							}
						}
					
					///////////////---------------------
					
					//echo "Registro $idCaracteristica $descc ha sido Creado $idGrupo";
					echo "$id_inmueble";
					
				}
				else
				{
					echo "error exce $tpc".$consulta->error;
				}
				
			}
        }
    }
	public function listadoPrecapta($asesor,$medio,$tip_inm,$barrio,$ciudad,$gestion,$emlcl,$telcli,$celcli,$est_cli,$idcap,$fecha_ini,$fecha_fin)
    {
        $w_conexion = new MySQL();
		$condicion="";

		if($asesor>0)
		{
			$condicion .=" and s.id_usuario='$asesor'";
		}
		if($medio)
		{
			$condicion .=" and s.id_medio='$medio'";
		}
		if($tip_inm>0)
		{
			$condicion .=" and i.id_tipo='$tip_inm'";
		}
		if($barrio)
		{
			$nombres_b = trim($barrio);
			$cantidad_b = sizeof(explode(" ", $nombres_b));
			$separarb = explode(" ",$nombres_b);
			for ($i=0;$i<$cantidad_b;$i++)
			{
			   $partesb=$separarb[$i];
			   $condicion .= " AND  (b.NombreB like '%$partesb%'or b.NombreB like '%$partesb%' )";
			}
		}
		if($ciudad)
		{
			$nombres_c = trim($ciudad);
			$cantidad_c = sizeof(explode(" ", $nombres_c));
			$separarc = explode(" ",$nombres_c);
			for ($i=0;$i<$cantidad_c;$i++)
			{
			   $partesc=$separarc[$i];
			   $condicion .= " AND  (c.Nombre like '%$partesc%'or c.Nombre like '%$partesc%' )";
			}
		}
		if($gestion>0)
		{
			$condicion .=" and i.id_gestion='$gestion'";
		}
		if($emlcl)
		{
			$condicion .=" and i.correo like '%$emlcl%'";
		}
		if($telcli>0)
		{
			$condicion .=" and i.telefono like '%$telcli%'";
		}
		if($celcli>0)
		{
			$condicion .=" and i.telefono2 like '%$celcli%'";
		}
		if($idcap>0)
		{
			$condicion .=" and i.id_inmueble = '$idcap'";
		}
		if($est_cli>0)
		{
			$condicion .=" and s.estado_captacion = '$est_cli'";
		}
		if($fecha_ini>0 and $fecha_fin>0)
		{
			$condicion .=" and i.fingreso between '$fecha_ini' and '$fecha_fin'";
		}
		if($fecha_ini>0 and $fecha_fin=="")
		{
			$condicion .=" and i.fingreso >= '$fecha_ini'";
		}
		if($fecha_ini=="" and $fecha_fin>0)
		{
			$condicion .=" and i.fingreso <= '$fecha_fin'";
		}
		if($_SESSION['IdInmmo']==279 or $_SESSION['IdInmmo']==2790)
		{
			$cond="WHERE i.id_inmobiliaria in (279,2790)";
		}
		else
		{
			$cond="WHERE i.id_inmobiliaria ='".$_SESSION['IdInmmo']."'";
		}
		
		$cantidadt=0;
		//echo "<br>".$counter."<br>";
		
		$arreglo1 = array();
$sqlpagos = "SELECT i.id_inmueble,i.direcion,g.NombresGestion,t.Descripcion AS tipoinmueble,m.medio_captacion,
			s.fecha,s.id_resultado,s.id_inmuebleprecapta,
			u.Nombres,u.apellidos,i.id_barrio,i.correo,i.telefono,i.telefono2,s.estado_captacion,i.telefono3
			FROM  inmueble_precapta i
			INNER JOIN seguimiento_precapta s ON s.id_inmueble=i.id_inmueble
			INNER JOIN gestioncomer g ON g.IdGestion=i.id_gestion
			INNER JOIN tipoinmuebles t ON t.idTipoInmueble=i.id_tipo
			INNER JOIN medio_captacion m ON m.id_medio=s.id_medio
			INNER JOIN usuarios u ON i.IdCaptador = u.Id_Usuarios
			$cond
			$condicion 
			group by i.id_inmueble
			Order By i.id_inmueble";
		//echo $sqlpagos."<br>";			
		$res=$w_conexion->ResultSet($sqlpagos);
		$cantidadt=$w_conexion->FilasAfectadas($res);
		if($cantidadt==0)
		{echo "No Hay Tickets para la busqueda seleccionada";}
		
		while ($fila7=$w_conexion->FilaSiguienteArray($res))
		{
			$i++;
			
			 $arreglo[0]=$fila7['id_inmueble'];
			 $arreglo[1]=$fila7['direcion'];
			 $arreglo[2]=$fila7['telefono'];
			 $arreglo[3]=$fila7['telefono2'];
			 $arreglo[4]=$fila7['correo'];
			 $arreglo[5]=ucwords(strtolower($fila7['NombresGestion']));
			 $arreglo[6]=ucwords(strtolower($fila7['tipoinmueble']));
			 $arreglo[7]=ucwords(strtolower($fila7['medio_captacion']));
			 $arreglo[8]=ucwords(strtolower(getCampo('barrios','WHERE IdBarrios='.$fila7['id_barrio'],'NombreB')));
			 $arreglo[9]=$fila7['id_barrio'];
			 $arreglo[10]= ucwords(strtolower(getCampo('ciudad','WHERE IdCiudad='.getCampo('barrios','WHERE IdBarrios='.$fila7['id_barrio'],'IdCiudad'),'Nombre')));
			 $arreglo[11]=$fila7['fecha'];
			 $arreglo[12]=ucwords(strtolower($fila7['Nombres']))." ".ucwords(strtolower($fila7["apellidos"]));  
			 $arreglo[13]=ucwords(strtolower(getCampo('resultado_seguimiento_capta','WHERE id_resultado='.$fila7['estado_captacion'],'resultado')));
			 $arreglo[14]=$fila7['estado_captacion'];
			 $arreglo[15]=$fila7['id_resultado'];
			 $arreglo[16]=$fila7['id_inmuebleprecapta'];
			 $arreglo[17]=$fila7['telefono2'];
			 
			 $arreglo1[] = $arreglo;		
		}
		return $arreglo1;
    }
	
	public function listadoProspectos($telp,$inmob)
    {
        $w_conexion = new MySQL();
		$condicion="";
	
		$arreglo1 = array();
$sqlpagos = "SELECT i.id_inmueble,i.direcion,g.NombresGestion,t.Descripcion AS tipoinmueble,m.medio_captacion,
			s.fecha,s.id_resultado,s.id_inmuebleprecapta,
			u.Nombres,u.apellidos,i.id_barrio,i.correo,i.telefono,i.telefono2,s.estado_captacion
			FROM  inmueble_precapta i
			INNER JOIN seguimiento_precapta s ON s.id_inmueble=i.id_inmueble
			INNER JOIN gestioncomer g ON g.IdGestion=i.id_gestion
			INNER JOIN tipoinmuebles t ON t.idTipoInmueble=i.id_tipo
			INNER JOIN medio_captacion m ON m.id_medio=s.id_medio
			INNER JOIN usuarios u ON i.IdCaptador = u.Id_Usuarios
			where (i.telefono='$telp' or i.telefono2='$telp' or i.telefono3='$telp')
			and i.id_inmobiliaria='$inmob'
			and i.telefono>0
			group by i.id_inmueble
			Order By i.id_inmueble";
		//echo $sqlpagos."<br>";			
		$res=$w_conexion->ResultSet($sqlpagos);
		$cantidadt=$w_conexion->FilasAfectadas($res);
		if($cantidadt==0)
		{echo "No Hay Resultados para la busqueda seleccionada";}
		
		while ($fila7=$w_conexion->FilaSiguienteArray($res))
		{
			$i++;
			
			 $arreglo[0]=$fila7['id_inmueble'];
			 $arreglo[1]=$fila7['direcion'];
			 $arreglo[2]=$fila7['telefono'];
			 $arreglo[3]=$fila7['telefono2'];
			 $arreglo[4]=$fila7['correo'];
			 $arreglo[5]=ucwords(strtolower($fila7['NombresGestion']));
			 $arreglo[6]=ucwords(strtolower($fila7['tipoinmueble']));
			 $arreglo[7]=ucwords(strtolower($fila7['medio_captacion']));
			 $arreglo[8]=ucwords(strtolower(getCampo('barrios','WHERE IdBarrios='.$fila7['id_barrio'],'NombreB')));
			 $arreglo[9]=$fila7['id_barrio'];
			 $arreglo[10]= ucwords(strtolower(getCampo('ciudad','WHERE IdCiudad='.getCampo('barrios','WHERE IdBarrios='.$fila7['id_barrio'],'IdCiudad'),'Nombre')));
			 $arreglo[11]=$fila7['fecha'];
			 $arreglo[12]=ucwords(strtolower($fila7['Nombres']))." ".ucwords(strtolower($fila7["apellidos"]));  
			 $arreglo[13]=ucwords(strtolower(getCampo('resultado_seguimiento_capta','WHERE id_resultado='.$fila7['estado_captacion'],'resultado')));
			 $arreglo[14]=$fila7['estado_captacion'];
			 $arreglo[15]=$fila7['id_resultado'];
			 $arreglo[16]=$fila7['id_inmuebleprecapta'];
			 
			 $arreglo1[] = $arreglo;		
		}
		return $arreglo1;
    }
}
?>