<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include "../funciones/connPDO.php";
class Paises
{

    public function __construct($conn = '')
    {
        $this->db->$conn;
    }

    public function obtenerPaises()
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT IdPais, Nombre FROM paises");
        if ($stmt->execute()) {
            $connPDO->exec("SET NAMES 'utf8'");
            $data = array();
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "id"     => $row['IdPais'],
                    "Nombre" => ucfirst(strtolower(utf8_encode($row['Nombre']))),
                );
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
   
    public function obtenerBarrios($idCiudad)
    {
        $connPDO = new Conexion();
        $data    = array();

        $barrios = $connPDO->prepare("SELECT b.`IdBarrios`,b.`NombreB`,l.`descripcion` AS localidad, z.`NombreZ` AS zona FROM barrios b, localidad l, zonas z WHERE b.`IdLocalidad`=l.`idlocalidad`AND b.`IdZona`=z.`IdZona`AND b.`IdCiudad`=:idCiudad");

        $barrios->bindParam(':idCiudad', $idCiudad, PDO::PARAM_STR);

        if ($barrios->execute()) {
            $connPDO->exec("SET NAMES 'utf8'");
            while ($rowbarrios = $barrios->fetch()) {
                // array_push($data, (string) '<input type="text" value="'.$rowbarrios['IdBarrios'].'">'.$rowbarrios['NombreB'].'Localidad:'.$rowbarrios['localidad']);
                // array_push($data, $rowbarrios['NombreB']);
                // array_push($data, $rowbarrios['localidad']);
                // array_push($data, $rowbarrios['zona']);
                $info= $rowbarrios['NombreB']."<br><b>Localidad</b>".$rowbarrios['localidad']."<br><b>Zona</b>".$rowbarrios['zona'];
                $data[]=array($rowbarrios['IdBarrios'],$info);
            }
            return $data;
        } else {
            return print_r($barrios->errorInfo());
        }

    }

}
