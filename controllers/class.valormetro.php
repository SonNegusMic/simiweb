<?php

/**
 * 
 */
class valorMetro {

    function __construct($connPDO) {
        $this->connPDO = $connPDO;
    }

    public function obtenerZonas($idciudad) {
        $stmt = $this->connPDO->prepare("SELECT IdZona, NombreZ FROM zonas WHERE IdCiudad = :idciudad ORDER BY NombreZ ASC");
        $stmt->bindParam(":idciudad", $idciudad);
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    'id' => $row['IdZona'],
                    'nombre' => ucfirst(strtolower($row['NombreZ'])),
                );
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = NULL;
    }

    public function obtenerBarrios($idzona) {
        $stmt = $this->connPDO->prepare("SELECT IdBarrios, NombreB FROM barrios WHERE IdZona = :idzona ORDER BY NombreB ASC");
        $stmt->bindParam(":idzona", $idzona);
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    'id' => $row['IdBarrios'],
                    'nombre' => ucfirst(strtolower($row['NombreB'])),
                );
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = NULL;
    }

    public function obtenerCiudades() {
        $stmt = $this->connPDO->prepare("SELECT IdCiudad, Nombre FROM ciudad ORDER BY Nombre ASC");
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    'id' => $row['IdCiudad'],
                    'nombre' => ucfirst(strtolower($row['Nombre'])),
                );
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = NULL;
    }

    public function obtenerGestion() {
        $stmt = $this->connPDO->prepare("SELECT IdGestion, NombresGestion FROM gestioncomer ORDER BY NombresGestion ASC");
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    'id' => $row['IdGestion'],
                    'nombre' => ucfirst(strtolower($row['NombresGestion'])),
                );
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = NULL;
    }

    public function obtenerTipoInm() {
        $stmt = $this->connPDO->prepare("SELECT idTipoInmueble, Descripcion FROM tipoinmuebles ORDER BY Descripcion ASC");
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    'id' => $row['idTipoInmueble'],
                    'nombre' => ucfirst(strtolower($row['Descripcion'])),
                );
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = NULL;
    }

    public function obtenerTotalRegistros($datapost) {
    	$cond = '';
    	if (isset($datapost['idbarrio']) && !empty($datapost['idbarrio']) && $datapost['idbarrio'] != '0') {
    		$cond .= ' AND IdBarrio = :barrio';
    	}
    	if (isset($datapost['idano']) && !empty($datapost['idano']) && $datapost['idano'] != '0') {
    		$sel_ano = $datapost['idano'];
            if ($sel_ano == 1) {
                $anactual = date("Y");
            }
            if ($sel_ano == 2) {
                $anactual = date("Y");
                $anactual = $anactual - 5;
            }
            if ($sel_ano == 3) {
                $anactual = date("Y");
                $anactualmas5 = $anactual - 5;
                $anactualmas10 = $anactualmas5 - 5;
            }
            if ($sel_ano == 4) {
                $anactual = date("Y");
                $anactualmas10 = $anactual - 10;
                $anactualmas20 = $anactualmas10 - 10;
            }
            if ($sel_ano == 5) {
                $anactual = date("Y");
                $anactualmas20 = $anactual - 20;
            }
            
            if ($sel_ano == 1)
                $cond = $cond . " and EdadInmueble = :anactual";
            if ($sel_ano == 2)
                $cond = $cond . " and EdadInmueble >= :anactual";
            if ($sel_ano == 3)
                $cond = $cond . " and EdadInmueble between :anactualmas5 and :anactualmas10";
            if ($sel_ano == 4)
                $cond = $cond . " and EdadInmueble between :anactualmas10 and :anactualmas20";
            if ($sel_ano == 5)
                $cond = $cond . " and EdadInmueble < :anactualmas20";
    	}
    	if (isset($datapost['idgestion']) && !empty($datapost['idgestion']) && $datapost['idgestion'] != '0') {
    		$cond .= ' AND IdGestion = :gestion';
    		if ($datapost['idgestion'] == '1') {
            	$cond .= ' AND ValorCanon > 100000';
            }else{
            	$cond .= ' AND ValorVenta > 100000';
            }
    	}
    	if (isset($datapost['idestrato']) && !empty($datapost['idestrato']) && $datapost['idestrato'] != '0') {
    		$cond .= ' AND Estrato = :estrato';
    	}
    	if (isset($datapost['idtipoinm']) && !empty($datapost['idtipoinm']) && $datapost['idtipoinm'] != '0') {
    		$cond .= ' AND IdTpInm = :tipoinm';
    	}

    	$sql = "SELECT count(codinm) as total FROM inmnvo WHERE idEstadoinmueble in (2,4) AND AreaLote > 0 $cond";

    	// return $sql;

        $stmt = $this->connPDO->prepare($sql);
        if (isset($datapost['idbarrio']) && !empty($datapost['idbarrio']) && $datapost['idbarrio'] != '0') {
            $stmt->bindParam(':barrio', $datapost['idbarrio'], PDO::PARAM_INT);
        }
        if (isset($datapost['idano']) && !empty($datapost['idano']) && $datapost['idano'] != '0') {
            if ($sel_ano == 1) {
                $stmt->bindParam(':anactual', $anactual, PDO::PARAM_INT);
            } elseif ($sel_ano == 2) {
                $stmt->bindParam(':anactual', $anactual, PDO::PARAM_INT);
            } elseif ($sel_ano == 3) {
                $stmt->bindParam(':anactualmas5', $anactualmas5, PDO::PARAM_INT);
                $stmt->bindParam(':anactualmas10', $anactualmas10, PDO::PARAM_INT);
            } elseif ($sel_ano == 4) {
                $stmt->bindParam(':anactualmas10', $anactualmas10, PDO::PARAM_INT);
                $stmt->bindParam(':anactualmas20', $anactualmas20, PDO::PARAM_INT);
            } elseif ($sel_ano == 5) {
                $stmt->bindParam(':anactualmas20', $anactualmas20, PDO::PARAM_INT);
            }
        }
        if (isset($datapost['idgestion']) && !empty($datapost['idgestion']) && $datapost['idgestion'] != '0') {
            $stmt->bindParam(':gestion', $datapost['idgestion'], PDO::PARAM_INT);
        }
        if (isset($datapost['idestrato']) && !empty($datapost['idestrato']) && $datapost['idestrato'] != '0') {
            $stmt->bindParam(':estrato', $datapost['idestrato'], PDO::PARAM_INT);
        }
        if (isset($datapost['idtipoinm']) && !empty($datapost['idtipoinm']) && $datapost['idtipoinm'] != '0') {
            $stmt->bindParam(':tipoinm', $datapost['idtipoinm'], PDO::PARAM_INT);
        }
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $data = array(
                    'total' => $row['total'],
                );
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = NULL;
    }

    public function obtenerRegistros($datapost) {

        $totalcond = 1;
        $cond = '';

        $idbarrio = $datapost['barrio'];

        if (isset($datapost['ano']) && !empty($datapost['ano']) && $datapost['ano'] != '0') {
            $sel_ano = $datapost['ano'];
            if ($sel_ano == 1) {
                $anactual = date("Y");
            }
            if ($sel_ano == 2) {
                $anactual = date("Y");
                $anactual = $anactual - 5;
            }
            if ($sel_ano == 3) {
                $anactual = date("Y");
                $anactualmas5 = $anactual - 5;
                $anactualmas10 = $anactualmas5 - 5;
            }
            if ($sel_ano == 4) {
                $anactual = date("Y");
                $anactualmas10 = $anactual - 10;
                $anactualmas20 = $anactualmas10 - 10;
            }
            if ($sel_ano == 5) {
                $anactual = date("Y");
                $anactualmas20 = $anactual - 20;
            }
            if ($totalcond == 0) {
                if ($sel_ano == 1)
                    $cond = $cond . " WHERE EdadInmueble = :anactual";
                if ($sel_ano == 2)
                    $cond = $cond . " WHERE EdadInmueble >= :anactual  ";
                if ($sel_ano == 3)
                    $cond = $cond . " WHERE EdadInmueble between :anactualmas5 and :anactualmas10";
                if ($sel_ano == 4)
                    $cond = $cond . " WHERE EdadInmueble between :anactualmas10 and :anactualmas20";
                if ($sel_ano == 5)
                    $cond = $cond . " WHERE EdadInmueble < :anactualmas20";
                $totalcond++;
            }else {
                if ($sel_ano == 1)
                    $cond = $cond . " and EdadInmueble = :anactual";
                if ($sel_ano == 2)
                    $cond = $cond . " and EdadInmueble >= :anactual";
                if ($sel_ano == 3)
                    $cond = $cond . " and EdadInmueble between :anactualmas5 and :anactualmas10";
                if ($sel_ano == 4)
                    $cond = $cond . " and EdadInmueble between :anactualmas10 and :anactualmas20";
                if ($sel_ano == 5)
                    $cond = $cond . " and EdadInmueble < :anactualmas20";
                $totalcond++;
            }
        }



        if (isset($datapost['gestion']) && !empty($datapost['gestion']) && $datapost['gestion'] != '0') {
            if ($totalcond == 0) {
                $cond .= " WHERE IdGestion = :idgestion";
            } else {
                $cond .= " and IdGestion = :idgestion";
                if ($datapost['gestion'] == '1') {
                	$cond .= ' and ValorCanon > 100000';
                }else{
                	$cond .= ' and ValorVenta > 100000';
                }
            }
        }
        if (isset($datapost['estrato']) && !empty($datapost['estrato']) && $datapost['estrato'] != '0') {
            if ($totalcond == 0) {
                $cond .= " WHERE Estrato = :estrato";
            } else {
                $cond .= " and Estrato = :estrato";
            }
        }
        if (isset($datapost['tipoinmueble']) && !empty($datapost['tipoinmueble']) && $datapost['tipoinmueble'] != '0') {
            if ($totalcond == 0) {
                $cond .= " WHERE IdTpInm = :tipoinmueble";
            } else {
                $cond .= " and IdTpInm = :tipoinmueble";
            }
        }
        if (isset($datapost['barrio']) && !empty($datapost['barrio']) && $datapost['barrio'] != '0') {
            if ($totalcond == 0) {
                $cond .= " WHERE IdBarrio = :barrio";
            } else {
                $cond .= " and IdBarrio = :barrio";
            }
        }

        $sql = "SELECT idInm, IdTpInm, IdGestion, IdBarrio, AreaConstruida,AreaLote, Estrato, year(now()) as ano, ValorVenta, ValorCanon, EdadInmueble  FROM inmnvo WHERE idEstadoinmueble in (2,4) AND AreaLote > 0 $cond ORDER BY idInm ASC";
        
        // return $sql;

        $stmt = $this->connPDO->prepare($sql);


        // return "SELECT idInm, IdTpInm, IdGestion, IdBarrio, AreaConstruida,AreaLote, Estrato, year(now()) as ano, ValorVenta, ValorCanon, EdadInmueble  FROM inmnvo WHERE idEstadoinmueble in (2,4) AND AreaLote > 0 $cond ORDER BY idInm ASC";


        if (isset($datapost['ano']) && !empty($datapost['ano']) && $datapost['ano'] != '0') {
            if ($sel_ano == 1) {
                $stmt->bindParam(':anactual', $anactual, PDO::PARAM_INT);
            } elseif ($sel_ano == 2) {
                $stmt->bindParam(':anactual', $anactual, PDO::PARAM_INT);
            } elseif ($sel_ano == 3) {
                $stmt->bindParam(':anactualmas5', $anactualmas5, PDO::PARAM_INT);
                $stmt->bindParam(':anactualmas10', $anactualmas10, PDO::PARAM_INT);
            } elseif ($sel_ano == 4) {
                $stmt->bindParam(':anactualmas10', $anactualmas10, PDO::PARAM_INT);
                $stmt->bindParam(':anactualmas20', $anactualmas20, PDO::PARAM_INT);
            } elseif ($sel_ano == 5) {
                $stmt->bindParam(':anactualmas20', $anactualmas20, PDO::PARAM_INT);
            }
        }
        if (isset($datapost['gestion']) && !empty($datapost['gestion']) && $datapost['gestion'] != '0') {
            $stmt->bindParam(':idgestion', $datapost['gestion'], PDO::PARAM_INT);
        }
        if (isset($datapost['estrato']) && !empty($datapost['estrato']) && $datapost['estrato'] != '0') {
            $stmt->bindParam(':estrato', $datapost['estrato'], PDO::PARAM_INT);
        }
        if (isset($datapost['tipoinmueble']) && !empty($datapost['tipoinmueble']) && $datapost['tipoinmueble'] != '0') {
            $stmt->bindParam(':tipoinmueble', $datapost['tipoinmueble'], PDO::PARAM_INT);
        }
        if (isset($datapost['barrio']) && !empty($datapost['barrio']) && $datapost['barrio'] != '0') {
            $stmt->bindParam(':barrio', $datapost['barrio'], PDO::PARAM_INT);
        }
        if ($stmt->execute()) {
            $data = array();
            $arraytotalm2 = array();
            $areatotal = 0;
            $totalcanon = 0;
            $totalm2 = 0;
            $rowcount = 0;
            while ($row = $stmt->fetch()) {
            	$areatotal += $row['AreaLote'];
                if ($row['IdGestion'] == 1) {
                    $precio = $row['ValorCanon'];
                } else {
                    $precio = $row['ValorVenta'];
                }
                if ($precio) {
                	$totalcanon += $precio;
	                $m2 = ceil($precio / $row['AreaLote']);
	                array_push($arraytotalm2, (int) $m2);
	                $totalm2 = $totalm2 + $m2;
	                $rowcount++;
                }

                // echo '<pre>';var_dump($precio);
            	
                // $idciudad = getCampo("barrios", " where IdBarrios = $idbarrio", "IdCiudad");
                // $idtipoinm = $row['IdTpInm'];

                // $data[] = array(
                //     'tipoinmueble' => ucfirst(strtolower(getCampo("tipoinmuebles", " where idTipoInmueble = $idtipoinm", "Descripcion"))),
                //     'ciudad' => ucfirst(strtolower(getCampo("ciudad", " where IdCiudad = $idciudad", "Nombre"))),
                //     'barrio' => ucfirst(strtolower(getCampo("barrios", " where IdBarrios = $idbarrio", "NombreB"))),
                //     'AreaConstruida' => $row['AreaConstruida'],
                //     'estrato' => $row['Estrato'],
                //     'ano' => ($row['ano'] - $row['EdadInmueble'] == 0) ? date('Y') : $row['ano'] - $row['EdadInmueble'],
                //     'precio' => $precio,
                // );
            }
            // echo '<pre>';var_dump($arraytotalm2);
            // die;
            $promediom2 = ceil($totalm2 / $rowcount);
            $totalmax = max($arraytotalm2);
            $totalmin = min($arraytotalm2);
            $data = array(
            	'areatotal' => number_format($areatotal,0,',','.'), 
            	'totalcanon' => number_format($totalcanon,0,',','.'), 
            	'totalm2' => number_format($totalm2,0,',','.'), 
            	'promediom2' => number_format($promediom2,0,',','.'), 
            	'totalmax' => number_format($totalmax,0,',','.'), 
            	'totalmin' => number_format($totalmin,0,',','.'));
            return $data;
        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = NULL;
    }

}

?>