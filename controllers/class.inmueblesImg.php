<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

@include "../funciones/connPDO.php";
class Inmuebles
{
    public function __construct($conn = "")
    {
        $this->db = $conn;
    }

    private $id_usuario;
    /*
    public function listadoRecibos($mes,$anio,$inmob)
    {
    $est=1;
    $f1="$anio-$mes-01";
    $f2="$anio-$mes-31";
    if(($consulta=$this->db->prepare("select Nombre,Fecha,idFactura,inmu,Total,IdCedula,sucursal
    from factura
    where NoInm=?
    and Fecha between ? and ?
    ")))

    {
    if(!$consulta->bind_param("iii",$inmob,$mes,$anio))
    {
    echo "error bind";
    }
    else
    {
    $consulta->execute();
    //                echo "$qry";
    $arreglo1 = array();
    if($consulta->bind_result($Referencia,$Arrendatario,$Mes,$Ano,$Inmueble,$Cedula,$IdFactura))
    while($row->fetch())
    {
    $data[]=array(
    "idInm"               => $arreglo1[0],
    "Direccion"          => $arreglo1[1],
    "ValorVenta"         => $arreglo1[2],
    "ValorCanon"           => $arreglo1[3],
    "IdGestion"         => $arreglo1[4],
    "Gestion"             => $arreglo1[5],
    "idprocedencia"         => $arreglo1[6],
    "procedencia"             => $arreglo1[7],
    "idtcontrato"         => $row['tipo_contrato'],
    "tcontrato"             => $arreglo1[8],
    "IdTpInm"             => $row['IdTpInm'],
    "TipoInm"             => $arreglo1[0],
    "IdBarrio"             => $row['IdBarrio'],
    "Barrio"             => $arreglo1[0],
    "ciudad"             => $arreglo1[0],
    "idEstadoinmueble"  => $row['idEstadoinmueble'],
    "Estadoinmueble"    => $arreglo1[0],
    "Administracion"    => $row['Administracion'],
    "FConsignacion"     => $row['FConsignacion'],
    "restricciones"     => $row['restricciones'],
    "RLaGuia"             => $row['RLaGuia'],
    "RVivaReal"           => $row['RVivaReal'],
    "RZonaProp"          => $row['RZonaProp'],
    "RMtoCuadrado"         => $row['RMtoCuadrado'],
    "Estrato"           => $row['Estrato'],
    "AreaConstruida"     => $row['AreaConstruida'],
    "NumLlaves"           => $row['NumLlaves'],
    "NumCasillero"      => $row['NumCasillero'],
    "PublicaM2"           => $row['PublicaM2'],
    "PublicaMeli"         => $row['PublicaMeli'],
    "PublicaMiCasa"     => $row['PublicaMiCasa'],
    "PublicaZonaProp"   => $row['PublicaZonaProp'],
    "PublicaDoomos"     => $row['PublicaDoomos'],
    "PublicaLamudi"     => $row['PublicaLamudi'],
    "PublicaGuia"         => $row['PublicaGuia'],
    "Publicagpt"           => $row['Publicagpt'],
    "AreaLote"          => $row['AreaLote'],
    "EdadInmueble"         => $row['EdadInmueble'],
    "usu_crea"           => $usucrea,
    "RNcasa"             => $row['RNcasa'],
    "bnts"             => $btnGratuitos.$btnPagos.$btnPagos.$btnPagos

    );
    }
    //echo "$inmob,$mes,$anio ------><br>";
    return $data;
    }
    }
    else
    {
    echo  "error lista".$conn->error;
    }

    }
     */
    public function infoInmobiliaria($codInmo)
    {
        //echo $codInmu."ff";

        if (($consulta = $this->db->prepare(" select
		IdInmobiliaria,Nombre,Direccion,Correo, Telefonos
		from clientessimi
        where IdInmobiliaria=?
		"))) {
            $consulta->bind_param("i", $codInmo);

            $consulta->execute();
//                echo "$qry";

            if ($consulta->bind_result($IdInmobiliaria, $Nombre, $Direccion, $Correo, $Telefonos)) {
                while ($consulta->fetch()) {
                    $arreglo[] = array(
                        "IdInmobiliaria" => $IdInmobiliaria,
                        "Nombre"         => ucwords(strtolower($Nombre)),
                        "Direccion"      => $Direccion,
                        "Correo"         => $Correo,
                        "Telefonos"      => $Telefonos,
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            echo "error --" . $conn->error;
        }

    }

    public function infoInmueble($codInmu)
    {
        //echo $codInmu."ff";

        if (($consulta = $this->db->prepare(" select
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo,IdBarrios,NombreProm,IdPromotor
		from datos_call
        where Codigo_Inmueble=?
		"))) {
            $consulta->bind_param("s", $codInmu);

            $consulta->execute();
//                echo "$qry";

            if ($consulta->bind_result($IdInmobiliaria, $Administracion, $IdGestion, $Estrato, $IdTpInm,
                $Codigo_Inmueble, $Tipo_Inmueble, $Venta, $Canon, $descripcionlarga,
                $Barrio, $Gestion, $AreaConstruida, $AreaLote, $latitud,
                $longitud, $EdadInmueble, $NombreInmo, $logo, $IdBarrios, $NombreProm, $IdPromotor)) {
                while ($consulta->fetch()) {
                    if ($IdGestion == 1) {
                        $oper   = 'rent';
                        $precio = $Canon;
                    } else {
                        $oper   = 'sale';
                        $precio = $Venta;
                    }
                    $cocina = "";
                    $aire   = "";
                    $pisos  = "";
                    //variables detalladas
                    $aire_acondicionado = evalua_carac1(31, $Codigo_Inmueble, 'Cantidad', 'aire acondicionado');
                    $aire .= ($aire_acondicionado == 1) ? ' Si ' : 'No';
                    $cocina_americana = evalua_carac_exacto1(13, $Codigo_Inmueble, 'Cantidad', 'AMERICANA ');

                    $integral = evalua_carac1(13, $Codigo_Inmueble, 'Cantidad', 'integral');

                    //$tp_cocina                    = trae_carac1(13,$Codigo_Inmueble,'Descripcion','');
                    $cocina .= ($integral == 1) ? 'Integral ' : '';
                    $cocina .= ($cocina_americana == 1) ? 'Americana ' : '';
                    $estacionamiento = evalua_carac1('23,37', $Codigo_Inmueble, 'Cantidad', 'parqueadero');
                    $vigilancia      = ucwords(strtolower(trae_carac1(24, $Codigo_Inmueble, 'Descripcion', 'horas')));
                    $madera          = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'madera');
                    $alfombra        = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'alfombra');
                    $ceramica        = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'ceramica');
                    $porcelanato     = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'porcelanato');
                    $marmol          = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'marmol');
                    $baldosa         = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'baldosa');
                    $pisos .= ($madera == 1) ? 'Madera ' : '';
                    $pisos .= ($alfombra == 1) ? 'Alfombra ' : '';
                    $pisos .= ($ceramica == 1) ? 'Ceramica ' : '';
                    $pisos .= ($porcelanato == 1) ? 'Porcelanato ' : '';
                    $pisos .= ($marmol == 1) ? 'Marmol ' : '';
                    $pisos .= ($baldosa == 1) ? 'Baldosa ' : '';

                    $closets                  = trae_carac1(45, $Codigo_Inmueble, 'Cantidad', 'closet', 0);
                    $banos                    = trae_carac1(16, $Codigo_Inmueble, 'Cantidad', utf8_decode('baño'), 0);
                    $alcobas                  = trae_carac1(15, $Codigo_Inmueble, 'Cantidad', 'alcoba', 0);
                    $cantGaraje               = trae_carac1(37, $Codigo_Inmueble, 'Cantidad', 'parqueadero', 0);
                    $parq                     = ($cantGaraje == 0) ? 'No ' : 'Si ';
                    $Ascensor                 = trae_carac1(25, $Codigo_Inmueble, 'Descripcion', 'Ascensor', 0);
                    $CanchaSquash             = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'Squash', 0);
                    $CanchadeTenis            = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'Tennis', 0);
                    $CanchasDeportivas        = trae_carac2(26, $Codigo_Inmueble, 'Descripcion', 'Canchas Deportivas', 0);
                    $CircuitoCerradoTV        = trae_carac1(24, $Codigo_Inmueble, 'Descripcion', 'Circuito', 0);
                    $Gimnasio                 = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'Gimnasio', 0);
                    $Jardin                   = trae_carac1(12, $Codigo_Inmueble, 'Descripcion', 'Jardin', 0);
                    $JauladeGolf              = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'Golf', 0);
                    $ParqueaderodeVisitantes  = trae_carac1(23, $Codigo_Inmueble, 'Descripcion', 'Visitantes', 0);
                    $Piscina                  = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'Piscina', 0);
                    $PlantaElectrica          = trae_carac1(22, $Codigo_Inmueble, 'Descripcion', 'Planta', 0);
                    $PorteriaRecepcion        = trae_carac1(25, $Codigo_Inmueble, 'Descripcion', 'Recepcion', 0);
                    $SalonComunal             = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'salon Comunal', 0);
                    $Terraza                  = trae_carac1(10, $Codigo_Inmueble, 'Descripcion', 'Terraza', 0);
                    $Vigilancia               = trae_carac1(24, $Codigo_Inmueble, 'Descripcion', 'horas', 0);
                    $SobreViaPrincipal        = trae_carac1(29, $Codigo_Inmueble, 'Cantidad', 'Via Principal', 0);
                    $Sobreviasecundaria       = trae_carac1(29, $Codigo_Inmueble, 'Cantidad', 'via secundaria', 0);
                    $ZonaCampestre            = trae_carac1(35, $Codigo_Inmueble, 'Descripcion', 'camping', 0);
                    $ZonaComercial            = trae_carac1(35, $Codigo_Inmueble, 'Descripcion', 'comercio y', 0);
                    $ZonaIndustrial           = trae_carac1(35, $Codigo_Inmueble, 'Descripcion', 'INDUSTRIAL', 0);
                    $ControlIndencios         = trae_carac1(25, $Codigo_Inmueble, 'Descripcion', 'incendio', 0);
                    $SalaComedorIndependiente = trae_carac1(6, $Codigo_Inmueble, 'Descripcion', 'SALA COMEDOR', 0);
                    $Citofono                 = trae_carac1(24, $Codigo_Inmueble, 'Descripcion', 'Citofono', 0);
                    $CuartodeServicio         = trae_carac1(13, $Codigo_Inmueble, 'Descripcion', 'cuarto y', 0);
                    $ZonaResidencial          = trae_carac1(35, $Codigo_Inmueble, 'Descripcion', 'residenci', 0);
                    $Estudio                  = trae_carac1(27, $Codigo_Inmueble, 'Descripcion', 'Estudio', 0);
                    $HalldeAlcobas            = trae_carac1(5, $Codigo_Inmueble, 'Descripcion', 'hall', 0);
                    $Patio                    = trae_carac1(44, $Codigo_Inmueble, 'Descripcion', 'Patio', 0);
                    $Cocineta                 = trae_carac1(13, $Codigo_Inmueble, 'Descripcion', 'Cocineta', 0);
                    $vestier                  = trae_carac1(45, $Codigo_Inmueble, 'Descripcion', 'vestier', 0);
                    $lavanderia               = trae_carac1(13, $Codigo_Inmueble, 'Descripcion', 'LAVANDERIA', 0);
                    $shut                     = trae_carac1(25, $Codigo_Inmueble, 'Descripcion', 'shut', 0);
                    $ZonaBBQ                  = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'BBQ', 0);
                    $Salondeconferencias      = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'conferencias', 0);
                    $EdificioInteligente      = trae_carac2(28, $Codigo_Inmueble, 'Descripcion', 'Edificio Inteligente', 0);
                    ///validar foto de usuarios.
                    $largo = strlen(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Foto'));
                    if ($largo <= 5) {
                        $fotoAsesor = "https://www.simiinmobiliarias.com/sj/img/def_avatar.gif";
                    } else {
                        $fotoAsesor = "https://www.simiinmobiliarias.com/mcomercialweb/" . getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Foto');
                    }

                    $arreglo[] = array(
                        "IdInmobiliaria"           => $IdInmobiliaria,
                        "Administracion"           => number_format($Administracion), //1
                        "IdGestion"                => $IdGestion, //2
                        "Estrato"                  => $Estrato, //3
                        "IdTpInm"                  => $IdTpInm, //4
                        "Codigo_Inmueble"          => $Codigo_Inmueble,
                        "Tipo_Inmueble"            => utf8_encode(ucwords(strtolower($Tipo_Inmueble))),
                        "Venta"                    => number_format($Venta),
                        "Canon"                    => number_format($Canon),
                        "precio"                   => number_format($precio),
                        "descripcionlarga"         => utf8_encode($descripcionlarga),
                        "Barrio"                   => utf8_encode(ucwords(strtolower($Barrio))),
                        "Gestion"                  => utf8_encode(ucwords(strtolower($Gestion))),
                        "AreaConstruida"           => number_format($AreaConstruida),
                        "AreaLote"                 => number_format($AreaLote),
                        "latitud"                  => $latitud,
                        "longitud"                 => $longitud,
                        "EdadInmueble"             => $EdadInmueble,
                        "NombreInmo"               => utf8_encode(ucwords(strtolower($NombreInmo))),
                        "ciudad"                   => utf8_encode(ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre')))),
                        "CorreoAsesor"             => utf8_encode(ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Correo')))),
                        "TelAsesor"                => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Telefono'))),
                        "CelAsesor"                => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Celular'))),
                        "FotoAsesor"               => $fotoAsesor,
                        "depto"                    => utf8_encode(ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre')))),
                        "oper"                     => utf8_encode($oper),
                        "foto1"                    => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos', "where idInm='" . $Codigo_Inmueble . "'", 'Foto1')),
                        "totaleregistros"          => $total,
                        "NombreProm"               => utf8_encode(ucwords(strtolower($NombreProm))),
                        "parqueadero"              => $parq,
                        "cocina"                   => $cocina,
                        "aire"                     => $aire,
                        "vigilancia"               => $vigilancia,
                        "pisos"                    => $pisos,
                        "closets"                  => $closets,
                        "banos"                    => $banos,
                        "alcobas"                  => $alcobas,
                        "closets"                  => $closets,
                        "vigilancia"               => $vigilancia,
                        "alarma"                   => $alarma,
                        "balcon"                   => $balcon,
                        "Barraestiloamerican"      => $Barraestiloamerican,
                        "Calentador"               => $Calentador,
                        "Chimenea"                 => $Chimenea,
                        "CocinaIntegral"           => $CocinaIntegral,
                        "CocinatipoAmericano"      => $CocinatipoAmericano,
                        "DepositoBodega"           => $DepositoBodega,
                        "Despensa"                 => $Despensa,
                        "balcon"                   => $balcon,
                        "gas"                      => $gas,
                        "sauna"                    => $sauna,
                        "turco"                    => $turco,
                        "jacuzzi"                  => $jacuzzi,
                        "Ascensor"                 => $Ascensor,
                        "CanchaSquash"             => $CanchaSquash,
                        "CanchadeTenis"            => $CanchadeTenis,
                        "CanchasDeportivas"        => $CanchasDeportivas,
                        "CircuitoCerradoTV"        => $CircuitoCerradoTV,
                        "Gimnasio"                 => $Gimnasio,
                        "Jardin"                   => $Jardin,
                        "JauladeGolf"              => $JauladeGolf,
                        "ParqueaderodeVisitantes"  => $ParqueaderodeVisitantes,
                        "Piscina"                  => $Piscina,
                        "PlantaElectrica"          => $PlantaElectrica,
                        "PorteriaRecepcion"        => $PorteriaRecepcion,
                        "SalonComunal"             => $SalonComunal,
                        "Terraza"                  => $Terraza,
                        "Vigilancia"               => $Vigilancia,
                        "SobreViaPrincipal"        => $SobreViaPrincipal,
                        "Sobreviasecundaria"       => $Sobreviasecundaria,
                        "ZonaCampestre"            => $ZonaCampestre,
                        "ZonaComercial"            => $ZonaComercial,
                        "ZonaIndustrial"           => $ZonaIndustrial,
                        "ControlIndencios"         => $ControlIndencios,
                        "SalaComedorIndependiente" => $SalaComedorIndependiente,
                        "Citofono"                 => $Citofono,
                        "CuartodeServicio"         => $CuartodeServicio,
                        "ZonaResidencial"          => $ZonaResidencial,
                        "Estudio"                  => $Estudio,
                        "HalldeAlcobas"            => $HalldeAlcobas,
                        "Patio"                    => $Patio,
                        "Cocineta"                 => $Cocineta,
                        "ZonaBBQ"                  => $ZonaBBQ,
                        "Salondeconferencias"      => $Salondeconferencias,
                        "EdificioInteligente"      => $EdificioInteligente,
                        "logo"                     => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $logo),
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            return $conn->error;
        }

    }
    public function infoInmueble2($codInmu, $idinmob)
    {
        //echo $codInmu."ff";

        if (($consulta = $this->db->prepare(" select
		idInm,codinm,IdInmobiliaria,IdGestion,IdTpInm,
		IdBarrio,banos,alcobas,garaje,Direccion,
		DirMetro,ValorVenta,ValorCanon,AdmonIncluida,Administracion,
		ValorIva,admondto,Estrato,AreaConstruida,AreaLote,EdadInmueble,chip,
		NoMatricula,CedulaCatastral,idProcedencia,idEstadoinmueble,FConsignacion,
		AvaluoCatastral,FechaAvaluoComercial,Carrera,Calle,
		ComiVenta,ComiArren,IdDestinacion,descripcionlarga,descripcionMetro,
		fingreso,fmodificacion,latitud,longitud,escritura_inmu,
		restricciones,obserinterna,ValComiVenta,ValComiArr,f_creacion,
		h_creacion,usu_creacion,permite_geo,FechaAvaluoCatastral,ValorAvaluoComercial,
		migrado,politica_comp,usu_crea,codinterno,flagm2
		from inmnvo
		where codinm=?
		and IdInmobiliaria=?
		"))) {
            $consulta->bind_param("ii", $codInmu, $idinmob);

            $consulta->execute();
//                echo "$qry";

            if ($consulta->bind_result($idInm, $codinm, $IdInmobiliaria, $IdGestion, $IdTpInm,
                $IdBarrio, $banos, $alcobas, $garaje, $Direccion,
                $DirMetro, $ValorVenta, $ValorCanon, $AdmonIncluida, $Administracion,
                $ValorIva, $admondto, $Estrato, $AreaConstruida, $AreaLote, $EdadInmueble, $chip,
                $NoMatricula, $CedulaCatastral, $idProcedencia, $idEstadoinmueble, $FConsignacion,
                $AvaluoCatastral, $FechaAvaluoComercial, $Carrera, $Calle,
                $ComiVenta, $ComiArren, $IdDestinacion, $descripcionlarga, $descripcionMetro,
                $fingreso, $fmodificacion, $latitud, $longitud, $escritura_inmu,
                $restricciones, $obserinterna, $ValComiVenta, $ValComiArr, $f_creacion,
                $h_creacion, $usu_creacion, $permite_geo, $FechaAvaluoCatastral, $ValorAvaluoComercial,
                $migrado, $politica_comp, $usu_crea, $codinterno, $flagm2)) {
                while ($consulta->fetch()) {
                    if ($IdGestion == 1) {
                        $oper   = 'rent';
                        $precio = $ValorCanon;
                    } else {
                        $oper   = 'sale';
                        $precio = $ValorVenta;
                    }
                    $cocina = "";
                    $aire   = "";
                    $pisos  = "";
                    //variables detalladas
                    $aire_acondicionado = evalua_carac1n(31, $codinm_fto, $IdInmobiliaria, 'Cantidad', 'aire acondicionado');
                    $aire .= ($aire_acondicionado == 1) ? ' Si ' : 'No';
                    $cocina_americana = evalua_carac_exacto1n(13, $codinm, $IdInmobiliaria, 'Cantidad', 'AMERICANA ');

                    $integral = evalua_carac1n(13, $codinm, $IdInmobiliaria, 'Cantidad', 'integral');

                    //$tp_cocina                    = trae_carac1(13,$codinm,$IdInmobiliaria,'Descripcion','');
                    $cocina .= ($integral == 1) ? 'Integral ' : '';
                    $cocina .= ($cocina_americana == 1) ? 'Americana ' : '';
                    $estacionamiento = evalua_carac1n('23,37', $idInm, $IdInmobiliaria, 'Cantidad', 'parqueadero');
                    $vigilancia      = ucwords(strtolower(trae_carac1(24, $idInm, $IdInmobiliaria, 'Descripcion', 'horas')));
                    $madera          = evalua_carac1n(9, $codinm, $IdInmobiliaria, 'Cantidad', 'madera');
                    $alfombra        = evalua_carac1n(9, $codinm, $IdInmobiliaria, 'Cantidad', 'alfombra');
                    $ceramica        = evalua_carac1n(9, $codinm, $IdInmobiliaria, 'Cantidad', 'ceramica');
                    $porcelanato     = evalua_carac1n(9, $codinm, $IdInmobiliaria, 'Cantidad', 'porcelanato');
                    $marmol          = evalua_carac1n(9, $codinm, $IdInmobiliaria, 'Cantidad', 'marmol');
                    $baldosa         = evalua_carac1n(9, $codinm, $IdInmobiliaria, 'Cantidad', 'baldosa');
                    $pisos .= ($madera == 1) ? 'Madera ' : '';
                    $pisos .= ($alfombra == 1) ? 'Alfombra ' : '';
                    $pisos .= ($ceramica == 1) ? 'Ceramica ' : '';
                    $pisos .= ($porcelanato == 1) ? 'Porcelanato ' : '';
                    $pisos .= ($marmol == 1) ? 'Marmol ' : '';
                    $pisos .= ($baldosa == 1) ? 'Baldosa ' : '';

                    $closets                  = trae_carac1n(45, $codinm, $IdInmobiliaria, 'Cantidad', 'closet', 0);
                    $banos                    = $banos;
                    $alcobas                  = $alcobas;
                    $cantGaraje               = $garaje;
                    $parq                     = ($cantGaraje == 0) ? 'No ' : 'Si ';
                    $closets                  = trae_carac1n(45, $codinm, $IdInmobiliaria, 'Cantidad', 'closet', 0);
                    $banos                    = trae_carac1n(16, $codinm, $IdInmobiliaria, 'Cantidad', utf8_decode('baño'), 0);
                    $alcobas                  = trae_carac1n(15, $codinm, $IdInmobiliaria, 'Cantidad', 'alcoba', 0);
                    $cantGaraje               = trae_carac1n(37, $codinm, $IdInmobiliaria, 'Cantidad', 'parqueadero', 0);
                    $Ascensor                 = trae_carac1n(25, $codinm, $IdInmobiliaria, 'Descripcion', 'Ascensor', 0);
                    $CanchaSquash             = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'Squash', 0);
                    $CanchadeTenis            = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'Tennis', 0);
                    $CanchasDeportivas        = trae_carac2n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'Canchas Deportivas', 0);
                    $CircuitoCerradoTV        = trae_carac1n(24, $codinm, $IdInmobiliaria, 'Descripcion', 'Circuito', 0);
                    $Gimnasio                 = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'Gimnasio', 0);
                    $Jardin                   = trae_carac1n(12, $codinm, $IdInmobiliaria, 'Descripcion', 'Jardin', 0);
                    $JauladeGolf              = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'Golf', 0);
                    $ParqueaderodeVisitantes  = trae_carac1n(23, $codinm, $IdInmobiliaria, 'Descripcion', 'Visitantes', 0);
                    $Piscina                  = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'Piscina', 0);
                    $PlantaElectrica          = trae_carac1n(22, $codinm, $IdInmobiliaria, 'Descripcion', 'Planta', 0);
                    $PorteriaRecepcion        = trae_carac1n(25, $codinm, $IdInmobiliaria, 'Descripcion', 'Recepcion', 0);
                    $SalonComunal             = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'salon Comunal', 0);
                    $Terraza                  = trae_carac1n(10, $codinm, $IdInmobiliaria, 'Descripcion', 'Terraza', 0);
                    $Vigilancia               = trae_carac1n(24, $codinm, $IdInmobiliaria, 'Descripcion', 'horas', 0);
                    $SobreViaPrincipal        = trae_carac1n(29, $codinm, $IdInmobiliaria, 'Cantidad', 'Via Principal', 0);
                    $Sobreviasecundaria       = trae_carac1n(29, $codinm, $IdInmobiliaria, 'Cantidad', 'via secundaria', 0);
                    $ZonaCampestre            = trae_carac1n(35, $codinm, $IdInmobiliaria, 'Descripcion', 'camping', 0);
                    $ZonaComercial            = trae_carac1n(35, $codinm, $IdInmobiliaria, 'Descripcion', 'comercio y', 0);
                    $ZonaIndustrial           = trae_carac1n(35, $codinm, $IdInmobiliaria, 'Descripcion', 'INDUSTRIAL', 0);
                    $ControlIndencios         = trae_carac1n(25, $codinm, $IdInmobiliaria, 'Descripcion', 'incendio', 0);
                    $SalaComedorIndependiente = trae_carac1n(6, $codinm, $IdInmobiliaria, 'Descripcion', 'SALA COMEDOR', 0);
                    $Citofono                 = trae_carac1n(24, $codinm, $IdInmobiliaria, 'Descripcion', 'Citofono', 0);
                    $CuartodeServicio         = trae_carac1n(13, $codinm, $IdInmobiliaria, 'Descripcion', 'cuarto y', 0);
                    $ZonaResidencial          = trae_carac1n(35, $codinm, $IdInmobiliaria, 'Descripcion', 'residenci', 0);
                    $Estudio                  = trae_carac1n(27, $codinm, $IdInmobiliaria, 'Descripcion', 'Estudio', 0);
                    $HalldeAlcobas            = trae_carac1n(5, $codinm, $IdInmobiliaria, 'Descripcion', 'hall', 0);
                    $Patio                    = trae_carac1n(44, $codinm, $IdInmobiliaria, 'Descripcion', 'Patio', 0);
                    $Cocineta                 = trae_carac1n(13, $codinm, $IdInmobiliaria, 'Descripcion', 'Cocineta', 0);
                    $vestier                  = trae_carac1n(45, $codinm, $IdInmobiliaria, 'Descripcion', 'vestier', 0);
                    $lavanderia               = trae_carac1n(13, $codinm, $IdInmobiliaria, 'Descripcion', 'LAVANDERIA', 0);
                    $shut                     = trae_carac1n(25, $codinm, $IdInmobiliaria, 'Descripcion', 'shut', 0);
                    $ZonaBBQ                  = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'BBQ', 0);
                    $Salondeconferencias      = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'conferencias', 0);
                    $EdificioInteligente      = trae_carac2n(28, $codinm, $IdInmobiliaria, 'Descripcion', 'Edificio Inteligente', 0);
                    ///validar foto de usuarios.
                    $largo = strlen(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Foto'));
                    if ($largo <= 5) {
                        $fotoAsesor = "https://www.simiinmobiliarias.com/sj/img/def_avatar.gif";
                    } else {
                        $fotoAsesor = "https://www.simiinmobiliarias.com/mcomercialweb/" . getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Foto');
                    }

                    $arreglo[] = array(
                        "IdInmobiliaria"           => $IdInmobiliaria,
                        "Administracion"           => number_format($Administracion), //1
                        "IdGestion"                => $IdGestion, //2
                        "Estrato"                  => $Estrato, //3
                        "IdTpInm"                  => $IdTpInm, //4
                        "Codigo_Inmueble"          => $idInm,
                        "Tipo_Inmueble"            => utf8_encode(ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble=$IdTpInm", 'Descripcion')))),
                        "Venta"                    => number_format($ValorVenta),
                        "Canon"                    => number_format($ValorCanon),
                        "precio"                   => number_format($precio),
                        "descripcionlarga"         => utf8_encode($descripcionlarga),
                        "Barrio"                   => utf8_encode(ucwords(strtolower(getCampo('barrios', "where IdBarrios=$IdBarrio", 'NombreB')))),
                        "Gestion"                  => utf8_encode(ucwords(strtolower(getCampo('gestioncomer', "where IdGestion=$IdGestion", 'NombresGestion')))),
                        "AreaConstruida"           => number_format($AreaConstruida),
                        "AreaLote"                 => number_format($AreaLote),
                        "latitud"                  => $latitud,
                        "longitud"                 => $longitud,
                        "EdadInmueble"             => $EdadInmueble,
                        "NombreInmo"               => utf8_encode(ucwords(strtolower(getCampo('clientessimi', "where IdInmobiliaria=$IdInmobiliaria", 'Nombre')))),
                        "ciudad"                   => utf8_encode(ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrio", 'IdCiudad'), 'Nombre')))),
                        "CorreoAsesor"             => utf8_encode(ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Correo')))),
                        "TelAsesor"                => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Telefono'))),
                        "CelAsesor"                => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Celular'))),
                        "FotoAsesor"               => $fotoAsesor,
                        "depto"                    => utf8_encode(ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrio", 'IdCiudad'), 'IdDepartamento'), 'Nombre')))),
                        "oper"                     => utf8_encode($oper),
                        "foto1"                    => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos_nvo', "where aa_fto='" . $IdInmobiliaria . "' and codinm_fto=$codinm order by posi_ft limit 0,1", 'foto')),
                        "NombreProm"               => utf8_encode(ucwords(strtolower($NombreProm))),
                        "parqueadero"              => $parq,
                        "cocina"                   => $cocina,
                        "aire"                     => $aire,
                        "vigilancia"               => $vigilancia,
                        "pisos"                    => $pisos,
                        "closets"                  => $closets,
                        "banos"                    => $banos,
                        "alcobas"                  => $alcobas,
                        "vigilancia"               => $vigilancia,
                        "alarma"                   => $alarma,
                        "balcon"                   => $balcon,
                        "Barraestiloamerican"      => $Barraestiloamerican,
                        "Calentador"               => $Calentador,
                        "Chimenea"                 => $Chimenea,
                        "CocinaIntegral"           => $CocinaIntegral,
                        "CocinatipoAmericano"      => $CocinatipoAmericano,
                        "DepositoBodega"           => $DepositoBodega,
                        "Despensa"                 => $Despensa,
                        "balcon"                   => $balcon,
                        "gas"                      => $gas,
                        "sauna"                    => $sauna,
                        "turco"                    => $turco,
                        "jacuzzi"                  => $jacuzzi,
                        "Ascensor"                 => $Ascensor,
                        "CanchaSquash"             => $CanchaSquash,
                        "CanchadeTenis"            => $CanchadeTenis,
                        "CanchasDeportivas"        => $CanchasDeportivas,
                        "CircuitoCerradoTV"        => $CircuitoCerradoTV,
                        "Gimnasio"                 => $Gimnasio,
                        "Jardin"                   => $Jardin,
                        "JauladeGolf"              => $JauladeGolf,
                        "ParqueaderodeVisitantes"  => $ParqueaderodeVisitantes,
                        "Piscina"                  => $Piscina,
                        "PlantaElectrica"          => $PlantaElectrica,
                        "PorteriaRecepcion"        => $PorteriaRecepcion,
                        "SalonComunal"             => $SalonComunal,
                        "Terraza"                  => $Terraza,
                        "Vigilancia"               => $Vigilancia,
                        "SobreViaPrincipal"        => $SobreViaPrincipal,
                        "Sobreviasecundaria"       => $Sobreviasecundaria,
                        "ZonaCampestre"            => $ZonaCampestre,
                        "ZonaComercial"            => $ZonaComercial,
                        "ZonaIndustrial"           => $ZonaIndustrial,
                        "ControlIndencios"         => $ControlIndencios,
                        "SalaComedorIndependiente" => $SalaComedorIndependiente,
                        "Citofono"                 => $Citofono,
                        "CuartodeServicio"         => $CuartodeServicio,
                        "ZonaResidencial"          => $ZonaResidencial,
                        "Estudio"                  => $Estudio,
                        "HalldeAlcobas"            => $HalldeAlcobas,
                        "Patio"                    => $Patio,
                        "Cocineta"                 => $Cocineta,
                        "ZonaBBQ"                  => $ZonaBBQ,
                        "Salondeconferencias"      => $Salondeconferencias,
                        "EdificioInteligente"      => $EdificioInteligente,
                        "logo"                     => "https://www.simiinmobiliarias.com/" . str_replace('../', '', getCampo('clientessimi', "where IdInmobiliaria=$IdInmobiliaria", 'logo')),
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            return $conn->error;
        }

    }
    public function VerificaFotos($inmuFoto)
    {
        $connPDO = new Conexion();
        $idInmo  = $data['inmo'];
        $usr     = $data['usuariom2'];
        $stmt    = $connPDO->prepare("Select idInm
			From fotos
			Where idInm=:inmuFoto");

        if ($stmt->execute(array(
            ":inmuFoto" => $inmuFoto,
        ))) {

            $existe = $stmt->rowCount();
            return $existe;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function listadoInmueblesConsignados($data)
    {
        $connPDO   = new Conexion();
        $cond      = "";
        $tabla     = "";
        $enlace    = "";
        $btnVender = "";

        if ($data['idInmu']) {
            $cond .= "  AND
					( idInm=:codinmu
					OR RVivaReal=:codinmu
					OR RLaGuia=:codinmu1
					OR RMtoCuadrado=:codinmu2
					OR RZonaProp=:codinmu3
					OR RMeli =:codinmu4
					Or codinterno=:codinmu5
                    Or codinm=:codinmu6) ";
        }
        if ($data['estadoInm']) {
            $cond .= " and i.idEstadoinmueble=:estadoInm";
        }
        if ($data['gestion']) {
            $cond .= " and i.IdGestion=:gestion";
        }
        if ($data['tContrato']) {
            $cond .= " and i.tipo_contrato=:tContrato";
        }
        if ($data['tInmueble']) {
            $cond .= " and i.IdTpInm=:tInmueble";
        }
        if ($data['barrio']) {
            $cond .= " and i.IdBarrio=:barrio";
        }
        if ($data['ciudad']) {
            $cond .= " and b.IdCiudad=:ciudad";
            $tabla  = ",barrios b";
            $enlace = " and i.IdBarrio=b.IdBarrios ";
        }
        if ($data['consignado']) {
            $cond .= " and i.IdCaptador=:consignado";
        }
        if ($data['ffecha_ini'] > 0 and $data['ffecha_fin'] > 0) {
            $cond .= " and i.FConsignacion between :fecha_inis and :fecha_fins ";
        }
        /* if($data['verAsesor']==1)
        {
        $cond .=" and i.IdPromotor=:prom";
        }*/
        $rowsPerPage = 50;
        $offset      = ($data['paginador'] - 1) * $rowsPerPage;

        $stmt = $connPDO->prepare("SELECT i.idInm, i.Direccion, i.ValorVenta, i.ValorCanon, i.IdGestion,
							i.idEstadoinmueble,i.Administracion, i.FConsignacion, i.restricciones,i.RLaGuia,
							i.RVivaReal,i.RZonaProp,i.RMtoCuadrado,i.Estrato,i.AreaConstruida ,
							i.NumLlaves,i.NumCasillero,i.descripcionlarga,i.PublicaM2,i.PublicaMeli,
							i.PublicaMiCasa,i.PublicaZonaProp,i.PublicaDoomos,i.PublicaLamudi,i.PublicaGuia,
							i.Publicagpt,i.AreaLote,i.EdadInmueble,i.usu_crea,i.RNcasa,i.IdTpInm, i.IdBarrio,
                            i.tipo_contrato,i.idProcedencia,i.RIdnd,i.IdInmobiliaria
							from inmuebles i $tabla
							where IdInmobiliaria=:idinmo " .
            $enlace . $cond . "
							limit $offset,50");
        $stmt->bindParam(':idinmo', $data['inmo']);
        //$stmt->bindParam(':idinmo',$id);
        if ($data['idInmu']) {
            $stmt->bindParam(':codinmu', $data['idInmu']);
            $stmt->bindParam(':codinmu1', $data['idInmu']);
            $stmt->bindParam(':codinmu2', $data['idInmu']);
            $stmt->bindParam(':codinmu3', $data['idInmu']);
            $stmt->bindParam(':codinmu4', $data['idInmu']);
            $stmt->bindParam(':codinmu5', $data['idInmu']);
            $stmt->bindParam(':codinmu6', $data['idInmu']);
        }
        if ($data['ciudad']) {
            $stmt->bindParam(':ciudad', $data['ciudad']);
        }
        if ($data['estadoInm']) {
            $stmt->bindParam(':estadoInm', $data['estadoInm']);
        }
        if ($data['gestion']) {
            $stmt->bindParam(':gestion', $data['gestion']);
        }
        if ($data['tContrato']) {
            $stmt->bindParam(':tContrato', $data['tContrato']);
        }
        if ($data['tInmueble']) {
            $stmt->bindParam(':tInmueble', $data['tInmueble']);
        }
        if ($data['barrio']) {
            $stmt->bindParam(':barrio', $data['barrio']);
        }
        if ($data['consignado']) {
            $stmt->bindParam(':consignado', $data['consignado']);
        }
        if ($data['ffecha_ini'] > 0 and $data['ffecha_fin'] > 0) {
            $stmt->bindParam(':fecha_inis', $data['ffecha_ini']);
            $stmt->bindParam(':fecha_fins', $data['ffecha_fin']);
        }
        /*if($data['verAsesor']==1)
        {
        $stmt->bindParam(':prom',$_SESSION['Id_Usuarios']);
        }*/
        if ($stmt->execute()) {

            $data = array();
            $connPDO->exec('chartset="utf-8"');
            while ($row = $stmt->fetch()) {
                $catidadResgistros = count($row);
                $existeFoto        = $this->VerificaFotos($row['idInm']);
                $idciud            = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], "IdCiudad");
                $btnPortales       = ($existeFoto > 0) ? '<button data-placement="bottom" title="Sincronizar a Portales" type="button" class="btn btn-warning  btnSincro  btn-xs btns "  data-toggle="modal"  data-target="#sincroPorta"><span class="glyphicon glyphicon"><i class="fa fa-upload"></i></span></button>' : '<label>Agregue Fotos para Publicar</label>';
                $btnPagos          = '<button data-placement="bottom" title="Portales" type="button" id="pagos" class="btn btn-info  pagos  btn-xs btns"  data-toggle="modal"  data-target=".mymodalpagos"><span class="glyphicon glyphicon"><i class="fa fa-files-o"></i></span></button>';
                $btnficha          = '<a href="../../mcomercialweb/inmuebles/buscainmueblesnvo2.php?codinmueble=' . $row['idInm'] . '"><button type="button" id="ficha" data-placement="bottom" title="Ver Detalles" class="btn btn-info  ficha  btn-xs btns"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button></a>';
                $btnEditar         = '<a href="../../mcomercialweb/EditarInmueble.php?inm=' . $row['idInm'] . '"><button type="button" id="edita" class="btn btn-watermelon  edita  btn-xs btns" data-placement="bottom" title="Editar Inmueble" ><span class="glyphicon glyphicon"><i class="fa fa-cogs"></i></span></button></a>';
                $btnReversar       = '<a href="../../mcomercialweb/reversar_inmueble.php?regresa=1&Inmueble=' . $row['idInm'] . '&nvoi=1"><button type="button" id="reversar" class="btn btn-spartan  reversar  btn-xs btns" data-placement="bottom" title="Cambiar Estado" ><span class="glyphicon glyphicon"><i class="fa fa-edit"></i></span></button></a>';
                if (($row['IdGestion'] == 5 || $row['IdGestion'] == 2) && $row['idEstadoinmueble'] != 3) {
                    $btnVender = '<a href="../../mcomercialweb/vender_inmueble.php?regresa=1&Inmueble=' . $row['idInm'] . '"><button type="button" id="vender" class="btn btn-info  vender  btn-xs btns" data-placement="bottom" title="Vender Inmueble" ><span class="glyphicon glyphicon"><i class="fa fa-money"></i></span></button></a>';
                }

                //$btnficha='<a href="../../mcomercialweb/buscainmueblesnvo2.php?codinmueble='.$row['idInm'].'"><button type="button" id="ficha" class="btn btn-info  ficha  btn-xs btns"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button></a>';
                $btnDuplicar        = '<button title="Duplicar Inmueble" type="button" id="ficha" class="btn btn-info  ficha  btn-xs btns" onclick="AbrirVentana(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-files-o" data-placement="bottom" ></i></span></button>';
                $btnOrdenar         = '<button  data-placement="bottom" title="Ordenar Fotos" type="button" id="ficha" class="btn btn-purple  ficha  btn-xs btns" onclick="AbrirVentanaOrdenar(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-camera-retro"></i></span></button>';
                $btnObprop          = '<button  data-placement="bottom" title="Observaciones Propietarios" type="button" id="ficha" class="btn btn-warning  ficha  btn-xs btns" onclick="AbrirVentanaProp(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-edit"></i></span></button>';
                $btnVerFicha        = '<button  data-placement="bottom" title="Ver Ficha" type="button" id="ficha" class="btn btn-default  ficha  btn-xs btns" onclick="AbrirVentanaFicha(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-file-photo-o"></i></span></button>';
                $btnVerFichaChicala = ($row['IdInmobiliaria'] == 172 || $row['IdInmobiliaria'] == 1) ? '<button  data-placement="bottom" title="Ver Ficha Chicala" type="button" id="ficha" class="btn btn-success  ficha  btn-xs btns" onclick="AbrirVentanaFichaCh(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-file-picture-o"></i></span></button>' : '';
                $btnVerFichaGomez   = ($row['IdInmobiliaria'] == 447 || $row['IdInmobiliaria'] == 1) ? '<button  data-placement="bottom" title="Ver Ficha Gomez" type="button" id="ficha" class="btn btn-primary  ficha  btn-xs btns" onclick="AbrirVentanaFichaGm(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-file-image-o"></i></span></button>' : '';
                $inputInmu          = '<input type="hidden" id="idInmu" name="idInmu" value="' . $row['idInm'] . '" >';

                ///validar cadena de pruebas metro
                $spruebas = getCampo('usuariosm2', "where inmogm2=" . $row['IdInmobiliaria'] . " and servidor=2 limit 0,1", "servidor");

                $btnCadenaMetro = ($this->verifyPortales($row['IdInmobiliaria'], 10) > 0 && $spruebas == 2) ? '<button  data-placement="bottom" title="Generar Prueba Metro" type="button" id="cadenaM2" class="btn btn-warning  ficha  btn-xs btns" onclick="AbrirCadena(' . "'" . $row['idInm'] . "'" . ',' . "'" . $row['RMtoCuadrado'] . "'" . ',' . getCampo('usuariosm2', "where inmogm2=" . $row['IdInmobiliaria'] . " limit 0,1", "idusariom2") . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-link"></i></span></button>' : '';

                $usucrea     = ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $row['usu_crea'] . "' and Id_Usuarios>0", 'concat(Nombres," ",apellidos)')));
                $cadPortales = '<ul>';
                $cadPortales .= "<li><b>" . $row['idInm'] . "</b></li>";
                if ($row['RLaGuia'] > 0) {
                    $cadPortales .= "<li>FincaRaiz " . $row['RLaGuia'] . "</li>";
                }
                if ($row['RMtoCuadrado'] > 0) {
                    $cadPortales .= "<li>Metro2 " . $row['RMtoCuadrado'] . "</li>";
                }
                if ($row['RNcasa'] > 0) {
                    $cadPortales .= "<li>Nuestra Casa " . $row['RNcasa'] . "</li>";
                }
                if ($row['RIdnd'] > 0) {
                    $cadPortales .= "<li>Idonde " . $row['RIdnd'] . "</li>";
                }
                if ($row['RVivaReal'] > 0) {
                    $cadPortales .= "<li>Interno " . $row['RVivaReal'] . "</li>";
                }
                if ($row['RZonaProp'] > 0) {
                    $cadPortales .= "<li>I24 " . $row['RZonaProp'] . "</li>";
                }
                if ($row['PublicaDoomos'] == 'Publicado') {
                    $cadPortales .= "<li>Gratuitos " . $row['RZonaProp'] . "</li>";
                }
                if ($row['PublicaLamudi'] == 'Publicado') {
                    $cadPortales .= "<li>Lamudi " . $row['PublicaLamudi'] . "</li>";
                }
                if ($row['Publicagpt'] == 'Publicado') {
                    $cadPortales .= "<li>GoPlaceit " . $row['Publicagpt'] . "</li>";
                }
                $flag = getCampo('clientessimi', "where IdInmobiliaria=" . $_SESSION['IdInmmo'], 'flag_portal');
                if ($flag == 10) {
                    $cadPortales = '<ul>';
                    $cadPortales .= "<li>Metro2 " . $row['RMtoCuadrado'] . "</li>";
                    $cadenaBotones = $btnCadenaMetro;
                } elseif ($flag == 8) {
                    $cadPortales = '<ul>';
                    $cadPortales .= "<li>FincaRaiz " . $row['RLaGuia'] . "</li>";
                } else {
                    $cadenaBotones = $btnEditar . $btnReversar . $btnPortales . $btnOrdenar . $btnVerFicha . $btnVerFichaChicala . $btnVerFichaGomez . $btnDuplicar . $btnCadenaMetro . $inputInmu;
                }

                $cadPortales .= '</ul>';

                $data[] = array(
                    "idInm"          => $cadPortales,
                    "Direccion"      => $row['Direccion'],
                    "ValorVenta"     => number_format($row['ValorVenta']),
                    "ValorCanon"     => number_format($row['ValorCanon']),
                    "Gestion"        => ucwords(strtolower(getCampo('gestioncomer', "where IdGestion=" . $row['IdGestion'], "NombresGestion"))),
                    "procedencia"    => ucwords(strtolower(getCampo('procedencia', "where IdProcedencia=" . $row['idProcedencia'], "Nombre"))),
                    "tcontrato"      => ucwords(strtolower(getCampo('tipo_contrato', "where id_contrato=" . $row['tipo_contrato'], "tipo_contrato"))),
                    "TipoInm"        => ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble=" . $row['IdTpInm'], "Descripcion"))),
                    "Barrio"         => ucwords(strtolower(getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], "NombreB"))),
                    "ciudad"         => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . $idciud, "Nombre"))),
                    "Estadoinmueble" => ucwords(strtolower(getCampo('estado_inmueble', "where idestado=" . $row['idEstadoinmueble'], 'descripcion'))),
                    "Administracion" => $row['Administracion'],
                    "FConsignacion"  => $row['FConsignacion'],
                    "Estrato"        => $row['Estrato'],
                    "AreaConstruida" => $row['AreaConstruida'],
                    "AreaLote"       => $row['AreaLote'],
                    "EdadInmueble"   => $row['EdadInmueble'],
                    "usu_crea"       => $usucrea,
                    "RNcasa"         => $row['RNcasa'],
                    "bnts"           => $cadenaBotones,

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }

    public function listadoInmueblesDestacados($codInmo, $lim)
    {
        //echo $codInmu."ff";
        if (($consulta = $this->db->prepare(" select
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo,IdBarrios
		from datos_call,settings_portales
        where Codigo_Inmueble=inmu_p
        and IdInmobiliaria=inmob_p
        and  tipo_p=4
        and vlr_p=1
        and IdInmobiliaria=?
        limit 0,?
		"))) {
            $consulta->bind_param("ii", $codInmo, $lim);

            $consulta->execute();

            if ($consulta->bind_result($IdInmobiliaria, $Administracion, $IdGestion, $Estrato, $IdTpInm,
                $Codigo_Inmueble, $Tipo_Inmueble, $Venta, $Canon, $descripcionlarga,
                $Barrio, $Gestion, $AreaConstruida, $AreaLote, $latitud,
                $longitud, $EdadInmueble, $NombreInmo, $logo, $IdBarrios)) {
                while ($consulta->fetch()) {
                    if ($IdGestion == 1) {
                        $oper   = 'rent';
                        $precio = $Canon;
                    } else {
                        $oper   = 'sale';
                        $precio = $Venta;
                    }

                    $arreglo[] = array(
                        "IdInmobiliaria"   => $IdInmobiliaria,
                        "Administracion"   => number_format($Administracion), //1
                        "IdGestion"        => $IdGestion, //2
                        "Estrato"          => $Estrato, //3
                        "IdTpInm"          => $IdTpInm, //4
                        "Codigo_Inmueble"  => $Codigo_Inmueble,
                        "Tipo_Inmueble"    => ucwords(strtolower($Tipo_Inmueble)),
                        "Venta"            => number_format($Venta),
                        "Canon"            => number_format($Canon),
                        "precio"           => number_format($precio),
                        "descripcionlarga" => $descripcionlarga,
                        "Barrio"           => ucwords(strtolower($Barrio)),
                        "Gestion"          => ucwords(strtolower($Gestion)),
                        "AreaConstruida"   => number_format($AreaConstruida),
                        "AreaLote"         => number_format($AreaLote),
                        "latitud"          => $latitud,
                        "longitud"         => $longitud,
                        "EdadInmueble"     => $EdadInmueble,
                        "NombreInmo"       => ucwords(strtolower($NombreInmo)),
                        "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                        "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                        "oper"             => $oper,
                        "foto1"            => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos', "where idInm='" . $Codigo_Inmueble . "'", 'Foto1')),
                        "totaleregistros"  => $total,
                        "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $logo),
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            return $conn->error;
        }
        $stmt = null;

    }
    public function listadoInmueblesFiltro($codInmo, $lim = 0, $tip_ope, $depto = 0, $area, $Precio, $tip_inm, $ciudad = 0)
    {
        //echo $codInmu."ff";
        //echo "$tip_ope, $depto,$area,$Precio ------<br>";

        $connPDO = new Conexion();
        $cond1   = "";
        if ($tip_ope > 0 && $tip_ope != 2) {
            $cond1 .= " and IdGestion=:tip_ope ";
        }
        if ($tip_inm > 0) {
            $cond1 .= " and IdTpInm=:tip_inm ";
        }
        if ($depto > 0) {
            $cond1 .= " and d.IdDepartamento=:depto ";
        }
        if ($ciudad > 0) {
            $cond1 .= " and c.IdCiudad=:ciud ";
        }
        if ($area > 0) {
            $cond1 .= " and AreaConstruida between :area and :area1 ";
        }
        if ($Precio > 0 && $tip_ope == 5) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 1) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon between :Precio and :Precio1 or Venta between :Precio and :Precio1) ";
        }
        //echo $cond1."ffffffff";
        $stmt = $connPDO->prepare(" select
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo,IdBarrios
		from datos_call c,departamento d,ciudad cu
        where IdInmobiliaria=:codInmo
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad
        $cond1
        limit $lim,10");

        $stmt->bindParam(':codInmo', $codInmo);
        //$stmt->bindParam(':lim',$lim);

        if ($tip_ope > 0 && $tip_ope != 2) {
            $stmt->bindParam(':tip_ope', $tip_ope);
        }
        if ($tip_inm > 0) {
            $stmt->bindParam(':tip_inm', $tip_inm);
        }
        //echo "<br> ***** ".$depto."*******<br>";
        if ($depto > 0) {
            $stmt->bindParam(':depto', $depto);
        }
        if ($ciudad > 0) {
            //echo $ciudad."ffffff--- <br>";
            $stmt->bindParam(':ciud', $ciudad);
        }
        if ($area > 0) {
            list($are, $are1) = explode("|", $this->trae_area($area));
            //echo "$are -$are1";
            $stmt->bindParam(':area', $are);
            $stmt->bindParam(':area1', $are1);
        }
        if ($Precio > 0 && $tip_ope == 1) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            //echo "<br> $Prec,$Prec1 <br>";
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        if ($Precio > 0 && $tip_ope == 5) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        //$stmt->bindParam(':Precio',$Precio);
        if ($Precio > 0 && $tip_ope == 2) {
            list($Precio, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }

        if ($stmt->execute()) {
            $existen = $stmt->rowCount();
            //echo "existen $existen <br>";
            while ($row = $stmt->fetch()) {

                /*$stmt1=$this->db->prepare("elect count(*) as total
                FROM datos_call
                where IdInmobiliaria=?");
                $stmt1->bind_param("i",$codInmo);
                $consulta1->execute();
                $consulta1->bind_result($total);*/

                if ($row['IdGestion'] == 1) {
                    $oper   = 'rent';
                    $precio = $row['Canon'];
                } else {
                    $oper   = 'sale';
                    $precio = $row['Venta'];
                }
                $IdBarrios = $row['IdBarrios'];
                $arreglo[] = array(
                    "IdInmobiliaria"   => $row['IdInmobiliaria'],
                    "Administracion"   => number_format($row['Administracion']), //1
                    "IdGestion"        => $row['IdGestion'], //2
                    "Estrato"          => $$row['Estrato'], //3
                    "IdTpInm"          => $row['IdTpInm'], //4
                    "Codigo_Inmueble"  => $row['Codigo_Inmueble'],
                    "Tipo_Inmueble"    => ucwords(strtolower($row['Tipo_Inmueble'])),
                    "Venta"            => number_format($row['Venta']),
                    "Canon"            => number_format($row['Canon']),
                    "precio"           => number_format($precio),
                    "descripcionlarga" => $row['descripcionlarga'],
                    "Barrio"           => ucwords(strtolower($row['Barrio'])),
                    "Gestion"          => ucwords(strtolower($row['Gestion'])),
                    "AreaConstruida"   => number_format($row['AreaConstruida']),
                    "AreaLote"         => number_format($row['AreaLote']),
                    "latitud"          => $row['latitud'],
                    "longitud"         => $row['longitud'],
                    "EdadInmueble"     => $row['EdadInmueble'],
                    "NombreInmo"       => ucwords(strtolower($row['NombreInmo'])),
                    "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                    "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                    "oper"             => $oper,
                    "foto1"            => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos', "where idInm='" . $row['Codigo_Inmueble'] . "'", 'Foto1')),
                    "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $row['logo']),
                );

            }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            if ($existen > 0) {
                return $arreglo;
            } else {
                return 0;
            }

        } else {
            //echo  "error ejecucion--".$stmt->errorInfo();
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function listadoInmueblesFiltroTodos($codInmo, $lim = 0, $tip_ope, $depto = 0, $area, $Precio, $tip_inm, $ciudad = 0)
    {
        //echo $codInmu."ff";
        //echo "$tip_ope, $depto,$area,$Precio ------<br>";

        $connPDO = new Conexion();
        $cond1   = "";
        if ($tip_ope > 0 && $tip_ope != 2) {
            $cond1 .= " and IdGestion=:tip_ope ";
        }
        if ($tip_inm > 0) {
            $cond1 .= " and IdTpInm=:tip_inm ";
        }
        if ($depto > 0) {
            $cond1 .= " and d.IdDepartamento=:depto ";
        }
        if ($ciudad > 0) {
            $cond1 .= " and c.IdCiudad=:ciud ";
        }
        if ($area > 0) {
            $cond1 .= " and AreaConstruida between :area and :area1 ";
        }
        if ($Precio > 0 && $tip_ope == 5) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 1) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon between :Precio and :Precio1 or Venta between :Precio and :Precio1) ";
        }
        //echo $cond1."ffffffff";
        $stmt = $connPDO->prepare(" select
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo,IdBarrios
		from datos_call c,departamento d,ciudad cu
        where IdInmobiliaria=:codInmo
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad
        $cond1
        limit $lim,1000");

        $stmt->bindParam(':codInmo', $codInmo);
        //$stmt->bindParam(':lim',$lim);

        if ($tip_ope > 0 && $tip_ope != 2) {
            $stmt->bindParam(':tip_ope', $tip_ope);
        }
        if ($tip_inm > 0) {
            $stmt->bindParam(':tip_inm', $tip_inm);
        }
        //echo "<br> ***** ".$depto."*******<br>";
        if ($depto > 0) {
            $stmt->bindParam(':depto', $depto);
        }
        if ($ciudad > 0) {
            //echo $ciudad."ffffff--- <br>";
            $stmt->bindParam(':ciud', $ciudad);
        }
        if ($area > 0) {
            list($are, $are1) = explode("|", $this->trae_area($area));
            //echo "$are -$are1";
            $stmt->bindParam(':area', $are);
            $stmt->bindParam(':area1', $are1);
        }
        if ($Precio > 0 && $tip_ope == 1) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            //echo "<br> $Prec,$Prec1 <br>";
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        if ($Precio > 0 && $tip_ope == 5) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        //$stmt->bindParam(':Precio',$Precio);
        if ($Precio > 0 && $tip_ope == 2) {
            list($Precio, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }

        if ($stmt->execute()) {
            $existen = $stmt->rowCount();
            //echo "existen $existen <br>";
            while ($row = $stmt->fetch()) {

                /*$stmt1=$this->db->prepare("elect count(*) as total
                FROM datos_call
                where IdInmobiliaria=?");
                $stmt1->bind_param("i",$codInmo);
                $consulta1->execute();
                $consulta1->bind_result($total);*/

                if ($row['IdGestion'] == 1) {
                    $oper   = 'rent';
                    $precio = $row['Canon'];
                } else {
                    $oper   = 'sale';
                    $precio = $row['Venta'];
                }
                $IdBarrios = $row['IdBarrios'];
                $arreglo[] = array(
                    "IdInmobiliaria"   => $row['IdInmobiliaria'],
                    "Administracion"   => number_format($row['Administracion']), //1
                    "IdGestion"        => $row['IdGestion'], //2
                    "Estrato"          => $$row['Estrato'], //3
                    "IdTpInm"          => $row['IdTpInm'], //4
                    "Codigo_Inmueble"  => $row['Codigo_Inmueble'],
                    "Tipo_Inmueble"    => ucwords(strtolower($row['Tipo_Inmueble'])),
                    "Venta"            => number_format($row['Venta']),
                    "Canon"            => number_format($row['Canon']),
                    "precio"           => number_format($precio),
                    "descripcionlarga" => $row['descripcionlarga'],
                    "Barrio"           => ucwords(strtolower($row['Barrio'])),
                    "Gestion"          => ucwords(strtolower($row['Gestion'])),
                    "AreaConstruida"   => number_format($row['AreaConstruida']),
                    "AreaLote"         => number_format($row['AreaLote']),
                    "latitud"          => $row['latitud'],
                    "longitud"         => $row['longitud'],
                    "EdadInmueble"     => $row['EdadInmueble'],
                    "NombreInmo"       => ucwords(strtolower($row['NombreInmo'])),
                    "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                    "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                    "oper"             => $oper,
                    "foto1"            => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos', "where idInm='" . $row['Codigo_Inmueble'] . "'", 'Foto1')),
                    "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $row['logo']),
                );

            }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            if ($existen > 0) {
                return $arreglo;
            } else {
                return 0;
            }

        } else {
            //echo  "error ejecucion--".$stmt->errorInfo();
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function listadoInmueblesFiltro2($codInmo, $lim = 0, $tip_ope, $depto = 0, $area, $area1, $Precio, $Precio1, $zona, $tip_inm, $ciudad = 0)
    {
        //echo $codInmu."ff";
        //echo "$tip_ope, $depto,$area,$Precio ------<br>";

        $connPDO = new Conexion();
        $cond1   = "";
        if ($tip_ope > 0 && $tip_ope != 2) {
            $cond1 .= " and IdGestion=:tip_ope ";
        }
        if ($tip_inm > 0) {
            $cond1 .= " and IdTpInm=:tip_inm ";
        }
        if ($depto > 0) {
            $cond1 .= " and d.IdDepartamento=:depto ";
        }
        if ($ciudad > 0) {
            $cond1 .= " and c.IdCiudad=:ciud ";
        }
        if ($zona > 0) {
            $cond1 .= " and d.IdZona=:zona ";
        }
        if ($area > 0 && $area1 > 0) {
            $cond1 .= " and AreaConstruida between :area and :area1 ";
        }
        if ($area > 0 && $area1 <= 0) {
            $cond1 .= " and AreaConstruida >= :area";
        }
        if ($area <= 0 && $area1 > 0) {
            $cond1 .= " and AreaConstruida <= :area ";
        }
        if ($tip_ope == 5) {
            $campov = "Venta";
        } else {
            $campov = "Canon";
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope != 2) {

            $cond1 .= " and $campov between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope != 2) {
            $cond1 .= " and $campov >= :Precio ";
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope != 2) {
            $cond1 .= " and $campov <= :Precio1 ";
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon between :Precio and :Precio1 or Venta between :Precio and :Precio1) ";
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon >= :Precio or Venta >= :Precio ) ";
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon <= :Precio1 or Venta <= :Precio1) ";
        }
        //echo $cond1."ffffffff";
        $stmt = $connPDO->prepare(" select
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo,IdBarrios
		from datos_call c,departamento d,ciudad cu
        where IdInmobiliaria=:codInmo
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad
        $cond1
        #if(IdGestion==5,' order by Venta',' order by Canon')
        limit $lim,10");

        $stmt->bindParam(':codInmo', $codInmo);
        //$stmt->bindParam(':lim',$lim);

        if ($tip_ope > 0 && $tip_ope != 2) {
            $stmt->bindParam(':tip_ope', $tip_ope);
        }
        if ($tip_inm > 0) {
            $stmt->bindParam(':tip_inm', $tip_inm);
        }
        //echo "<br> ***** ".$depto."*******<br>";
        if ($depto > 0) {
            $stmt->bindParam(':depto', $depto);
        }
        if ($ciudad > 0) {
            //echo $ciudad."ffffff--- <br>";
            $stmt->bindParam(':ciud', $ciudad);
        }
        if ($zona > 0) {
            $stmt->bindParam(':zona', $zona);
        }
        if ($area > 0 && $area1 > 0) {
            $stmt->bindParam(':area', $area);
            $stmt->bindParam(':area1', $area1);
        }
        if ($area > 0 && $area1 <= 0) {
            $stmt->bindParam(':area', $area);
        }
        if ($area <= 0 && $area1 > 0) {
            $stmt->bindParam(':area1', $area1);
        }

        $vlrCero = 0;
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio', $Precio);
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio', $Precio);
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio', $Precio);
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio', $Precio);
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($stmt->execute()) {
            $existen = $stmt->rowCount();
            //echo "existen $existen <br>";
            while ($row = $stmt->fetch()) {

                /*$stmt1=$this->db->prepare("elect count(*) as total
                FROM datos_call
                where IdInmobiliaria=?");
                $stmt1->bind_param("i",$codInmo);
                $consulta1->execute();
                $consulta1->bind_result($total);*/

                if ($row['IdGestion'] == 1) {
                    $oper   = 'rent';
                    $precio = $row['Canon'];
                } else {
                    $oper   = 'sale';
                    $precio = $row['Venta'];
                }
                $IdBarrios = $row['IdBarrios'];
                $arreglo[] = array(
                    "IdInmobiliaria"   => $row['IdInmobiliaria'],
                    "Administracion"   => number_format($row['Administracion']), //1
                    "IdGestion"        => $row['IdGestion'], //2
                    "Estrato"          => $$row['Estrato'], //3
                    "IdTpInm"          => $row['IdTpInm'], //4
                    "Codigo_Inmueble"  => $row['Codigo_Inmueble'],
                    "Tipo_Inmueble"    => ucwords(strtolower($row['Tipo_Inmueble'])),
                    "Venta"            => number_format($row['Venta']),
                    "Canon"            => number_format($row['Canon']),
                    "precio"           => number_format($precio),
                    "descripcionlarga" => $row['descripcionlarga'],
                    "Barrio"           => ucwords(strtolower($row['Barrio'])),
                    "Gestion"          => ucwords(strtolower($row['Gestion'])),
                    "AreaConstruida"   => number_format($row['AreaConstruida']),
                    "AreaLote"         => number_format($row['AreaLote']),
                    "latitud"          => $row['latitud'],
                    "longitud"         => $row['longitud'],
                    "EdadInmueble"     => $row['EdadInmueble'],
                    "NombreInmo"       => ucwords(strtolower($row['NombreInmo'])),
                    "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                    "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                    "oper"             => $oper,
                    "foto1"            => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos', "where idInm='" . $row['Codigo_Inmueble'] . "'", 'Foto1')),
                    "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $row['logo']),
                );

            }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            if ($existen > 0) {
                return $arreglo;
            } else {
                return 0;
            }

        } else {
            //echo  "error ejecucion--".$stmt->errorInfo();
            print_r($stmt->errorInfo());
        }
        $stmt = null;

    }
    public function totalInmueblesFiltro($codInmo, $lim = 0, $tip_ope, $depto = 0, $area, $Precio, $tip_inm, $ciudad = 0)
    {
        //echo $codInmu."ff";
        //echo "$tip_ope, $depto,$area,$Precio ------<br>";

        $connPDO = new Conexion();
        $cond1   = "";
        if ($tip_ope > 0 && $tip_ope != 2) {
            $cond1 .= " and IdGestion=:tip_ope ";
        }
        if ($tip_inm > 0) {
            $cond1 .= " and IdTpInm=:tip_inm ";
        }
        if ($depto > 0) {
            $cond1 .= " and d.IdDepartamento=:depto ";
        }
        if ($ciudad > 0) {
            $cond1 .= " and c.IdCiudad=:ciud ";
        }
        if ($area > 0) {
            $cond1 .= " and AreaConstruida between :area and :area1 ";
        }
        if ($Precio > 0 && $tip_ope == 5) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 1) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon between :Precio and :Precio1 or Venta between :Precio and :Precio1) ";
        }
        //echo $cond1."ffffffff";
        $stmt = $connPDO->prepare(" select count(*) as tot
			FROM datos_call c,departamento d,ciudad cu
        WHERE IdInmobiliaria=:codInmo
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad
		$cond1 ");

        $stmt->bindParam(':codInmo', $codInmo);
        //$stmt->bindParam(':lim',$lim);

        if ($tip_ope > 0 && $tip_ope != 2) {
            $stmt->bindParam(':tip_ope', $tip_ope);
        }
        if ($tip_inm > 0) {
            $stmt->bindParam(':tip_inm', $tip_inm);
        }
        //echo "<br> ***** ".$depto."*******<br>";
        if ($depto > 0) {
            $stmt->bindParam(':depto', $depto);
        }
        if ($ciudad > 0) {
            //echo $ciudad."ffffff--- <br>";
            $stmt->bindParam(':ciud', $ciudad);
        }
        if ($area > 0) {
            list($are, $are1) = explode("|", $this->trae_area($area));
            //echo "$are -$are1";
            $stmt->bindParam(':area', $are);
            $stmt->bindParam(':area1', $are1);
        }
        if ($Precio > 0 && $tip_ope == 1) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            //echo "<br> $Prec,$Prec1 <br>";
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        if ($Precio > 0 && $tip_ope == 5) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        //$stmt->bindParam(':Precio',$Precio);
        if ($Precio > 0 && $tip_ope == 2) {
            list($Precio, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $tot = $row['tot'];
                //echo $tot."ffff";
            }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $tot;
        } else {
            //echo  "error ejecucion--".$stmt->errorInfo();
            //print_r($stmt->errorInfo());
        }
        $stmt = null;

    }
    public function totalInmueblesFiltro2($codInmo, $lim = 0, $tip_ope, $depto = 0, $area, $area1, $Precio, $Precio1, $zona, $tip_inm, $ciudad = 0)
    {
        //echo $codInmu."ff";
        //echo "$tip_ope, $depto,$area,$Precio ------<br>";

        $connPDO = new Conexion();
        $cond1   = "";
        if ($tip_ope > 0 && $tip_ope != 2) {
            $cond1 .= " and IdGestion=:tip_ope ";
        }
        if ($tip_inm > 0) {
            $cond1 .= " and IdTpInm=:tip_inm ";
        }
        if ($depto > 0) {
            $cond1 .= " and d.IdDepartamento=:depto ";
        }
        if ($ciudad > 0) {
            $cond1 .= " and c.IdCiudad=:ciud ";
        }
        if ($zona > 0) {
            $cond1 .= " and c.IdZona=:zona ";
        }
        if ($area > 0 && $area1 > 0) {
            $cond1 .= " and AreaConstruida between :area and :area1 ";
        }
        if ($area > 0 && $area1 <= 0) {
            $cond1 .= " and AreaConstruida >= :area";
        }
        if ($area <= 0 && $area1 > 0) {
            $cond1 .= " and AreaConstruida <= :area ";
        }
        if ($tip_ope == 5) {
            $campov = "Venta";
        } else {
            $campov = "Canon";
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope != 2) {

            $cond1 .= " and $campov between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope != 2) {
            $cond1 .= " and $campov >= :Precio ";
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope != 2) {
            $cond1 .= " and $campov <= :Precio1 ";
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon between :Precio and :Precio1 or Venta between :Precio and :Precio1) ";
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon >= :Precio or Venta >= :Precio ) ";
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon <= :Precio1 or Venta <= :Precio1) ";
        }
        //echo $cond1."ffffffff";
        $stmt = $connPDO->prepare(" select count(*) as tot
			FROM datos_call c,departamento d,ciudad cu
        WHERE IdInmobiliaria=:codInmo
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad
		$cond1
		");

        $stmt->bindParam(':codInmo', $codInmo);
        //$stmt->bindParam(':lim',$lim);

        if ($tip_ope > 0 && $tip_ope != 2) {
            $stmt->bindParam(':tip_ope', $tip_ope);
        }
        if ($tip_inm > 0) {
            $stmt->bindParam(':tip_inm', $tip_inm);
        }
        //echo "<br> ***** ".$depto."*******<br>";
        if ($depto > 0) {
            $stmt->bindParam(':depto', $depto);
        }
        if ($ciudad > 0) {
            //echo $ciudad."ffffff--- <br>";
            $stmt->bindParam(':ciud', $ciudad);
        }
        if ($zona > 0) {
            $stmt->bindParam(':zona', $zona);
        }
        if ($area > 0 && $area1 > 0) {
            $stmt->bindParam(':area', $area);
            $stmt->bindParam(':area1', $area1);
        }
        if ($area > 0 && $area1 <= 0) {
            $stmt->bindParam(':area', $area);
        }
        if ($area <= 0 && $area1 > 0) {
            $stmt->bindParam(':area1', $area1);
        }
        $vlrCero = 0;
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio', $Precio);
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio', $Precio);
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio', $Precio);
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio', $Precio);
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio1', $Precio1);
        }

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $tot = $row['tot'];
                //echo $tot."ffff";
            }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $tot;
        } else {
            //echo  "error ejecucion--".$stmt->errorInfo();
            //print_r($stmt->errorInfo());
        }
        $stmt = null;

    }
    public function trae_precio($val)
    {
        if ($val == 1) {
            $are  = "0";
            $are1 = "1000000";
        }
        if ($val == 2) {
            $are  = "1000001";
            $are1 = "2000000";
        }
        if ($val == 3) {
            $are  = "2000001";
            $are1 = "4000000";
        }
        if ($val == 4) {
            $are  = "4000001";
            $are1 = "10000000";
        }
        if ($val == 5) {
            $are  = "10000001";
            $are1 = "100000000000000";
        }

        return $are . "|" . $are1;
    }
    public function trae_area($area)
    {
        if ($area == 1) {
            $are  = "0";
            $are1 = "60";
        }
        if ($area == 2) {
            $are  = "61";
            $are1 = "90";
        }
        if ($area == 3) {
            $are  = "91";
            $are1 = "150";
        }
        if ($area == 4) {
            $are  = "151";
            $are1 = "240";
        }
        if ($area == 5) {
            $are  = "241";
            $are1 = "360";
        }
        if ($area == 6) {
            $are  = "361";
            $are1 = "480";
        }
        if ($area == 7) {
            $are  = "481";
            $are1 = "600";
        }
        if ($area == 8) {
            $are  = "601";
            $are1 = "60000000000";
        }
        return $are . "|" . $are1;
    }
    public function listadoProyectos($codInmo, $lim, $tp)
    {
        //echo $codInmu."ff";
        if (($consulta = $this->db->prepare(" SELECT
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo,IdBarrios
		from datos_call
        where IdInmobiliaria=?
        and IdTpInm=?
        limit 0,?
		"))) {
            $consulta->bind_param("sii", $codInmo, $tp, $lim);

            $consulta->execute();
//                echo "$qry";

            if ($consulta->bind_result($IdInmobiliaria, $Administracion, $IdGestion, $Estrato, $IdTpInm,
                $Codigo_Inmueble, $Tipo_Inmueble, $Venta, $Canon, $descripcionlarga,
                $Barrio, $Gestion, $AreaConstruida, $AreaLote, $latitud,
                $longitud, $EdadInmueble, $NombreInmo, $logo, $IdBarrios)) {
                while ($consulta->fetch()) {
                    if ($IdGestion == 1) {
                        $oper   = 'rent';
                        $precio = $Canon;
                    } else {
                        $oper   = 'sale';
                        $precio = $Venta;
                    }

                    $arreglo[] = array(
                        "IdInmobiliaria"   => $IdInmobiliaria,
                        "Administracion"   => number_format($Administracion), //1
                        "IdGestion"        => $IdGestion, //2
                        "Estrato"          => $Estrato, //3
                        "IdTpInm"          => $IdTpInm, //4
                        "Codigo_Inmueble"  => $Codigo_Inmueble,
                        "Tipo_Inmueble"    => ucwords(strtolower($Tipo_Inmueble)),
                        "Venta"            => number_format($Venta),
                        "Canon"            => number_format($Canon),
                        "precio"           => number_format($precio),
                        "descripcionlarga" => $descripcionlarga,
                        "Barrio"           => ucwords(strtolower($Barrio)),
                        "Gestion"          => ucwords(strtolower($Gestion)),
                        "AreaConstruida"   => number_format($AreaConstruida),
                        "AreaLote"         => number_format($AreaLote),
                        "latitud"          => $latitud,
                        "longitud"         => $longitud,
                        "EdadInmueble"     => $EdadInmueble,
                        "NombreInmo"       => ucwords(strtolower($NombreInmo)),
                        "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                        "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                        "oper"             => $oper,
                        "foto1"            => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos', "where idInm='" . $Codigo_Inmueble . "'", 'Foto1')),
                        "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $logo),
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            echo "error --" . $conn->error;
        }

    }

    public function listadoInmuebles($idEmpresa, $IdGestion, $codInmu = '')
    {
        $cond = '';
        if ($codInmu) {
            $cond = "and Codigo_Inmueble=?";
        }
        if (($consulta = $this->db->prepare(" SELECT
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo
		from datos_call
        where IdInmobiliaria=?
        and IdGestion=?
		$cond
		limit 0,20"))) {
            if ($codInmu) {
                $consulta->bind_param("iis", $idEmpresa, $IdGestion, $codInmu);
            } else {
                $consulta->bind_param("ii", $idEmpresa, $IdGestion);
            }

            $consulta->execute();
//                echo "$qry";
            $arreglo1 = array();
            if ($consulta->bind_result($IdInmobiliaria, $Administracion, $IdGestion, $Estrato, $IdTpInm,
                $Codigo_Inmueble, $Tipo_Inmueble, $Venta, $Canon, $descripcionlarga,
                $Barrio, $Gestion, $AreaConstruida, $AreaLote, $latitud,
                $longitud, $EdadInmueble, $NombreInmo, $logo)) {
                while ($consulta->fetch()) {
                    $arreglo[] = array(
                        "IdInmobiliaria"   => $IdInmobiliaria,
                        "Administracion"   => $Administracion, //1
                        "IdGestion"        => $IdGestion, //2
                        "Estrato"          => $Estrato, //3
                        "IdTpInm"          => $IdTpInm, //4
                        "Codigo_Inmueble"  => $Codigo_Inmueble,
                        "Tipo_Inmueble"    => $Tipo_Inmueble,
                        "Venta"            => $Venta,
                        "Canon"            => $Canon,
                        "descripcionlarga" => $descripcionlarga,
                        "Barrio"           => $Barrio,
                        "Gestion"          => $Gestion,
                        "AreaConstruida"   => $AreaConstruida,
                        "AreaLote"         => $AreaLote,
                        "latitud"          => $latitud,
                        "longitud"         => $longitud,
                        "EdadInmueble"     => $EdadInmueble,
                        "EdadInmueble"     => $NombreInmo,
                        "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $logo),
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            echo "error --" . $conn->error;
        }
        $stmt = null;
    }
    public function fotosInmueble($codInmueble)
    {
        if (($consulta = $this->db->prepare(" select foto
		from fotos_nvo
        where id_inmft=?"))) {
            if (!$consulta->bind_param("s", $codInmueble)) {
                echo "error bind";
            } else {
                $consulta->execute();
//                echo "$qry";
                if ($consulta->bind_result($foto)) {
                    while ($consulta->fetch()) {
                        $arreglo[] = array(
                            "foto" => "https://www.simiinmobiliarias.com/mcomercialweb/" . $foto,
                        );

                    }
                }

                return $arreglo;

            }
        } else {
            echo "error img" . $conn->error;
        }
        $stmt = null;
    }
    public function inmueblesDisponibles($IdInmob)
    {
        $condperfil = "";
        $ase        = $_SESSION['Id_Usuarios'];
        if ($_SESSION['IdInmmo'] == 632) {
            $condperfil = "and IdPromotor='$ase'";
        }
        $w_conexion = new MySQL();
        $sql        = "Select count(Codigo_Inmueble) as tot
			   			From datos_call
						where IdInmobiliaria=$IdInmob
						$condperfil";

        $res = $w_conexion->ResultSet($sql);
        while ($f = $w_conexion->FilaSiguienteArray($res)) {
            $tot = $f['tot'];
        }
        return $tot;
        $w_conexion->CerrarConexion();
    }
    public function inmueblesSinFotos($IdInmob)
    {
        $cade       = "";
        $condperfil = "";
        $ase        = $_SESSION['Id_Usuarios'];
        if ($_SESSION['IdInmmo'] == 632) {
            $condperfil = "and IdPromotor='$ase'";
        }
        $arreglo1   = array();
        $w_conexion = new MySQL();
        $sql        = "SELECT i.idInm,i.IdInmobiliaria,f.Foto1
				FROM inmuebles i
				LEFT JOIN fotos f ON i.idInm=f.idInm
				WHERE i.IdInmobiliaria = '" . $IdInmob . "'
				AND (f.Foto1 IS NULL OR LENGTH(f.Foto1)=0)
				AND i.idEstadoinmueble  = 2
				$condperfil";

        $res = $w_conexion->ResultSet($sql);
        while ($f = $w_conexion->FilaSiguienteArray($res)) {
            $idInm = $f['idInm'];
            $cade  = "'" . $idInm . "'," . $cade;
        }
        return substr($cade, 0, -1);
        $w_conexion->CerrarConexion();
    }

    public function inmueblesSinVideos($IdInmob)
    {
        $cade       = "";
        $condperfil = "";
        $ase        = $_SESSION['Id_Usuarios'];
        if ($_SESSION['IdInmmo'] == 632) {
            $condperfil = "and IdPromotor='$ase'";
        }
        $arreglo1   = array();
        $w_conexion = new MySQL();
        $sql        = "SELECT idInm
						FROM inmuebles
						WHERE IdInmobiliaria = '" . $IdInmob . "'
						AND (linkvideo IS NULL OR LENGTH(linkvideo)=0)
						AND idEstadoinmueble  = 2
						$condperfil";

        $res = $w_conexion->ResultSet($sql);
        while ($f = $w_conexion->FilaSiguienteArray($res)) {
            $idInm = $f['idInm'];
            $cade  = "'" . $idInm . "'," . $cade;
        }
        return substr($cade, 0, -1);
        $w_conexion->CerrarConexion();
    }
    public function inmueblesIncompletos($IdInmob)
    {
        $w_conexion = new MySQL();
        $condperfil = "";
        $ase        = $_SESSION['Id_Usuarios'];
        if ($_SESSION['IdInmmo'] == 632) {
            $condperfil = "and IdPromotor='$ase'";
        }
        $sql = "Select count(Codigo_Inmueble) as tot
			   			From datos_call
						where IdInmobiliaria=$IdInmob
						$condperfil";

        $res = $w_conexion->ResultSet($sql);
        while ($f = $w_conexion->FilaSiguienteArray($res)) {
            $tot = $f['tot'];
        }
        return $tot;
        $w_conexion->CerrarConexion();
    }
    public function listadoInmuebles2($inmob, $codInmuebles, $nro_inm)
    {
        $cond = "";
        if ($codInmuebles) {
            $cond = "and Codigo_Inmueble in ($codInmuebles)";
        }
        if ($nro_inm) {
            $cond = "and Codigo_Inmueble in ('1-" . $nro_inm . "')";
        }
        $condperfil = "";
        $ase        = $_SESSION['Id_Usuarios'];
        if ($_SESSION['IdInmmo'] == 632 or $_SESSION['IdInmmo'] == 631 or $_SESSION['IdInmmo'] == 422 or $_SESSION['IdInmmo'] == 501 or $_SESSION['IdInmmo'] == 699 or $_SESSION['IdInmmo'] == 711 or $_SESSION['IdInmmo'] == 538 or $_SESSION['IdInmmo'] == 409 or $_SESSION['IdInmmo'] == 411 or $_SESSION['IdInmmo'] == 707 or $_SESSION['IdInmmo'] == 122) {
            $condperfil = "and IdPromotor='$ase'";
        }
        $w_conexion = new MySQL();
        $sql        = "SELECT
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo,IdPropietario, Nombrepro,IdBarrios,Ciudad,
		NombreCap,IdPromotor,NombreProm,IdCaptador
		from datos_call
        where IdInmobiliaria=$inmob
        $cond
		$condperfil
		#limit 0,20";
        //echo $sql;
        $res = $w_conexion->ResultSet($sql);
        while ($f = $w_conexion->FilaSiguienteArray($res)) {
            $arreglo[0]  = $f['IdInmobiliaria']; //0
            $arreglo[1]  = $f['Administracion']; //1
            $arreglo[2]  = $f['IdGestion']; //2
            $arreglo[3]  = $f['Estrato']; //3
            $arreglo[4]  = $f['IdTpInm']; //4
            $arreglo[5]  = $f['Codigo_Inmueble'];
            $arreglo[6]  = $f['Tipo_Inmueble'];
            $arreglo[7]  = $f['Venta'];
            $arreglo[8]  = $f['Canon'];
            $arreglo[9]  = $f['descripcionlarga'];
            $arreglo[10] = $f['Barrio'];
            $arreglo[11] = $f['Gestion'];
            $arreglo[12] = $f['AreaConstruida'];
            $arreglo[13] = $f['AreaLote'];
            $arreglo[14] = $f['latitud'];
            $arreglo[15] = $f['longitud'];
            $arreglo[16] = $f['EdadInmueble'];
            $arreglo[17] = $f['NombreInmo'];
            $arreglo[18] = "https://www.simiinmobiliarias.com/" . str_replace('../', '', $f['logo']);
            $arreglo[19] = $f['IdPropietario'];
            $arreglo[20] = $f['Nombrepro'];
            $arreglo[21] = $f['IdBarrios'];
            $arreglo[22] = $f['Ciudad'];
            $arreglo[23] = $f['IdCaptador'];
            $arreglo[24] = $f['NombreCap'];
            $arreglo[25] = $f['IdPromotor'];
            $arreglo[26] = $f['NombreProm'];
            // $arreglo[27]=$f['codinterno'];

            $arreglo1[] = $arreglo;
        }
        return $arreglo1;

    }

    ////json para api Afydi

    public function listadoInmueblesAfydi($idEmpresa, $codInmu, $usr_act)
    {

        $dInmuebles   = new Inmuebles();
        $avisoVentana = 0;

        $pservidumbre   = $dInmuebles->evalua_carac(23, $codInmu, 'Cantidad', 'SERVIDUMBRE');
        $pindependiente = $dInmuebles->evalua_carac(23, $codInmu, 'Cantidad', 'INDEPENDIENTE', 0);
        $comunal        = $dInmuebles->evalua_carac(23, $codInmu, 'Cantidad', 'COMUNAL', 0);
        $pvisitantes    = $dInmuebles->evalua_carac(23, $codInmu, 'Cantidad', 'VISITANTES', 0);
        $pcubierto      = $dInmuebles->evalua_carac(23, $codInmu, 'Cantidad', 'CUBIERTO');
        $ascensor       = $dInmuebles->evalua_carac(25, $codInmu, 'Cantidad', 'ASCENSOR');
        $banos          = $dInmuebles->trae_carac(16, $codInmu, 'Cantidad', utf8_decode('baño'), 0);
        $alcobas        = $dInmuebles->trae_carac(15, $codInmu, 'Cantidad', 'alcoba', 0);
        $nivel          = $dInmuebles->trae_carac(36, $codInmu, 'Cantidad', 'nivel', 0);
        $inm_destacado  = getCampo('settings_portales', "where inmu_p='" . $codInmu . "' and tipo_p=4", 'vlr_p');
        $sucursal       = 0;

        if (($consulta = $this->db->prepare(" select
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		idInm,ValorVenta,ValorCanon,descripcionlarga,
		IdBarrio,IdGestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,Direccion,
		IdPromotor,Fondo,Frente,
		Restricciones,idEstadoinmueble,IdDestinacion,IdPropietario,
		linkvideo,fingreso,FConsignacion,NoMatricula,ComiVenta,
		ComiArren,IdProcedencia
		from inmuebles
        where IdInmobiliaria=?
        and idInm=?
		#$cond
		limit 0,20"))) {
            if ($codInmu) {
                $consulta->bind_param("is", $idEmpresa, $codInmu);
            } else {
                $consulta->bind_param("is", $idEmpresa, $codInmu);
            }

            $consulta->execute();
//                echo "$qry";

            if ($consulta->bind_result($IdInmobiliaria, $Administracion, $IdGestion, $Estrato, $IdTpInm,
                $idInm, $ValorVenta, $ValorCanon, $descripcionlarga,
                $IdBarrio, $IdGestion, $AreaConstruida, $AreaLote, $latitud,
                $longitud, $EdadInmueble, $Direccion,
                $IdPromotor, $Fondo, $Frente, $Restricciones, $idEstadoinmueble, $IdDestinacion, $IdPropietario,
                $linkvideo, $fingreso, $FConsignacion, $NoMatricula, $ComiVenta,
                $ComiArren, $IdProcedencia)) {
                while ($consulta->fetch()) {
                    $IdCiudad = getCampo('barrios', "where IdBarrios=" . $IdBarrio, 'IdCiudad');
                    $IdZona   = getCampo('barrios', "where IdBarrios=" . $IdBarrio, 'IdZona');
                    if ($IdGestion == 5) {
                        $porc_comision = $ComiVenta;
                    } else {
                        $porc_comision = $ComiArren;
                    }
                    if ($IdProcedencia == 58) {
                        $avisoVentana = 1;
                    }
                    $IdTpInmAfydi       = (getCampo('tipoinmuebles', "where idTipoInmueble=$IdTpInm", 'conafydi', 0) == "") ? 1 : getCampo('tipoinmuebles', "where idTipoInmueble=$IdTpInm", 'conafydi', 0);
                    $IdGestionAfydi     = getCampo('gestioncomer', "where IdGestion=$IdGestion", 'convgafydi', 0);
                    $IdDestinacionAfydi = getCampo('destinacion', "where IdDestinacion=$IdDestinacion", 'convdafydi', 0);
                    $ZonaAfydi          = getCampo('zonas', "where IdZona=$IdZona", 'convzafydi', 0);
                    $barrioComun        = getCampo('barrios', "where IdBarrios=$IdBarrio", 'NombreB', 0);
                    $dateIngreso        = ($fingreso == "") ? "0" : $fingreso;
                    $inm_destacado      = ($inm_destacado == 0) ? "0" : $inm_destacado;
                    $inm                = explode("-", $idInm);
                    $nivel              = ($nivel == "") ? 1 : $nivel;
                    $linkvideo          = ($linkvideo == "") ? " " : $linkvideo;
                    $ValorCanon         = ($ValorCanon == "") ? 0 : $ValorCanon;

                    $arreglo = array(
                        "codpro"            => $inm[1],
                        "address"           => "cra 45 b 78 34",
                        "city"              => $IdCiudad,
                        "zone"              => $ZonaAfydi,
                        "type"              => $IdTpInmAfydi,
                        "biz"               => 2,
                        "area_lot"          => $AreaLote,
                        "build_year"        => $EdadInmueble,
                        "area_cons"         => $AreaConstruida,
                        "bedrooms"          => $alcobas,
                        "bathrooms"         => $banos,
                        "stratum"           => $Estrato,
                        "saleprice"         => $ValorVenta,
                        "description"       => utf8_encode("sdfsfdf"),
                        "latitude"          => "+4.715",
                        "longitude"         => "-74.10",
                        "barrio_comun"      => $barrioComun,
                        "status"            => $idEstadoinmueble,
                        "neighborhood"      => $IdBarrio,
                        "floor"             => $nivel,
                        "rent"              => $ValorCanon,
                        "administration"    => "1000000",
                        #"desc_eng"              => "",
                        #"desc_fre"              => "",
                        "resctrictions"     => $Restricciones,
                        #"comment"               => "",
                        #"comment2"              => "",
                        #"asesor"                => $IdPromotor,
                        "video"             => $linkvideo,
                        "parking"           => $pindependiente,
                        "parking_covered"   => $pcubierto,
                        "consignation_date" => $FConsignacion,
                        "propietario"       => $IdPropietario,
                        "registry_date"     => $dateIngreso,
                        #"destacado"             => $inm_destacado,
                        #"branch"                => $idEmpresa.$sucursal,
                        "window_sign"       => $avisoVentana,
                        "front"             => $Frente,
                        #"rear"                  => "",
                        #"private_area"          => "",
                        #"destinacion"           => "$IdDestinacionAfydi",
                        #"commission_percentage" => "",
                        #"matricula_number"      => "",
                        #"referencia"            => "",
                        "images"            => $dInmuebles->fotosInmuebleAfydi($codInmu),
                    );
                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            //echo json_encode($arreglo);
            return $arreglo;
            //return json_encode($arreglo);
        } else {
            echo "error --" . $conn->error;
        }
        $stmt = null;
    }
    public function fotosInmuebleAfydi($codInmueble)
    {
        $w_conexion = new MySQL();
        $urlFoto    = "https://www.simiinmobiliarias.com/mcomercialweb/";

        $consulta1 = "select
		Foto1,Foto2,Foto3,Foto4,Foto5,
		Foto6,Foto7,Foto8,Foto9,Foto10
		from fotos
        where idInm='$codInmueble'";
        $res = $w_conexion->ResultSet($consulta1);
        // if(!$consulta1->bind_param("s",$codInmueble))
        // {
        //     echo "error bind";
        // }
        // else
        // {

        while ($ff = $w_conexion->FilaSiguienteArray($res)) {

            for ($p = 1; $p < 11; $p++) {
                $Foto = $ff['Foto' . $p];
                $urlFoto . $Foto1;
                $arreglo[] = array(
                    "id"        => $p,
                    "order"     => $p,
                    "imagename" => 'Foto' . $p,
                    "imageurl"  => $urlFoto . $Foto,
                    "thumburl"  => $urlFoto . $Foto,
                    #"s3bucket"         =>"",
                    "inmueble"  => $codInmueble,
                    #"comments"         =>"",
                    #"created_at"     =>date('Y-m-d'),
                    #"updated_at"     => date('Y-m-d')
                );
            }
            //    }
            //print_r( $arreglo);
            return $arreglo;
        }
        $w_conexion->CerrarConexion();
    }
    public function updateCodeAfydi($codInmu, $afydi)
    {
        $conexion = new Conexion();
        $stmt     = $conexion->prepare('UPDATE inmuebles SET RAfydi =:RAfydi
								  WHERE idInm = :idInm');
        if ($stmt->execute(array(
            ":RAfydi" => $afydi,
            ":idInm"  => $codInmu))) {
            return 1;
        } else {
            print_r($stmt->errorInfo());
        }
    }
    public function trae_carac($grupo, $inmueble, $campo, $especifica, $muestra = "")
    {
        global $w_conexion;
        $cond = "";
        if ($especifica) {
            $cond .= " and Descripcion like '%$especifica%'";
        }
        $sqlconsulta = "SELECT  $campo as aaa
			 FROM detalleinmueble
			 INNER JOIN maestrodecaracteristicas
			 ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica
			 AND  maestrodecaracteristicas.IdGrupo in ($grupo)
			 INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble'
			 AND detalleinmueble.idinmueble = inmuebles.idinm
			 $cond";
        if ($muestra == 1) {
            echo "<br>" . $sqlconsulta . "<br>";
        }
        $res    = $w_conexion->ResultSet($sqlconsulta) or die("error " . mysql_error());
        $existe = $w_conexion->FilasAfectadas($res);
        //echo $existe."ff";
        while ($fff = $w_conexion->FilaSiguienteArray($res)) {
            //echo "hola en cliclo";
            $idcaracteristica = $fff["aaa"];
        }
        //echo ">> $idcaracteristica --- $campo <br>";
        return $idcaracteristica;
        $w_conexion->CerrarConexion();
    }
    public function evalua_carac($grupo, $inmueble, $campo, $especifica, $muestra = "")
    {
        global $w_conexion;
        $cond = "";
        if ($especifica) {
            $cond .= " and Descripcion like '%$especifica%'";
        }
        $sqlconsulta = "SELECT  $campo as aaa
			 FROM detalleinmueble
			 INNER JOIN maestrodecaracteristicas
			 ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica
			 AND  maestrodecaracteristicas.IdGrupo in ($grupo)
			 INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble'
			 AND detalleinmueble.idinmueble = inmuebles.idinm
			 $cond";
        if ($muestra == 1) {
            echo $sqlconsulta;
        }
        $res    = $w_conexion->ResultSet($sqlconsulta) or die("error " . mysql_error());
        $existe = $w_conexion->FilasAfectadas($res);
        //echo $existe."ff";

        if ($existe > 0) {
            $idcaracteristica = "1";
        } else {
            $idcaracteristica = "0";
        }
//    echo ">> $idcaracteristica --- $campo <br>";
        return $idcaracteristica;
        $w_conexion->CerrarConexion();
    }
    public function evalua_carac_exacto($grupo, $inmueble, $campo, $especifica)
    {
        global $w_conexion;
        $cond = "";
        if ($especifica) {
            $cond .= " and Descripcion = '$especifica'";
        }
        $sqlconsulta = "SELECT  $campo as aaa
			 FROM detalleinmueble
			 INNER JOIN maestrodecaracteristicas
			 ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica
			 AND  maestrodecaracteristicas.IdGrupo ='$grupo'
			 INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble'
			 AND detalleinmueble.idinmueble = inmuebles.idinm
			 $cond";
        //echo $sqlconsulta;
        $res    = $w_conexion->ResultSet($sqlconsulta) or die("error " . mysql_error());
        $existe = $w_conexion->FilasAfectadas($res);
        //echo $existe."ff";

        if ($existe > 0) {
            $idcaracteristica = "1";
        } else {
            $idcaracteristica = "0";
        }
//    echo ">> $idcaracteristica --- $campo <br>";
        return $idcaracteristica;
        $w_conexion->CerrarConexion();
    }
    public function actualizarEstado($idInmueble, $codAfydi)
    {
        if (!empty($idInmueble) && !empty($codAfydi)) {
            $connPDO = new Conexion();

            $stmt = $connPDO->prepare("UPDATE inmuebles SET
		                              PublicaAfydi='Publicado',
		                              RAfydi=:codAfydi
		                              WHERE idInm = :idInmueble");
            if ($stmt->execute(array(
                ":codAfydi"   => $codAfydi,
                ":idInmueble" => $idInmueble,
            ))) {
                return 1;
            } else {
                print_r($stmt->errorInfo());
            }
        } else {
            return 0;
        }
        $stmt = null;
    }

    public function verificaEstadoAfydi($codInmu)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT RAfydi
						   FROM inmuebles
						   where idInm = :codInmu AND
						   RAfydi>0");
        if ($stmt->execute(array(
            ":codInmu" => $codInmu,
        ))) {
            $seguimiento = $stmt->rowCount();
            if ($seguimiento > 0) {
                return 1;
            }
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;

    }
    public function obtenerCodigoAfydi($codInmu)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT RAfydi
						   FROM inmuebles
						   where idInm = :codInmu AND
						   RAfydi>0");
        if ($stmt->execute(array(
            ":codInmu" => $codInmu,
        ))) {
            $codigo = "";
            while ($row = $stmt->fetch()) {
                $codigo = $row["RAfydi"];
            }
            return $codigo;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;

    }
    public function getTipoInmuebles()
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT idTipoInmueble,Descripcion FROM tipoinmuebles");
        if ($stmt->execute()) {
            $data = array();

            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "idTipoInmueble" => $row["idTipoInmueble"],
                    "Descripcion"    => ucwords(strtolower($row["Descripcion"])),
                );
            }
            return $data;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function gestioncomer()
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT IdGestion,NombreGestion FROM gestioncomer");
        if ($stmt->execute()) {
            $data = array();

            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "IdGestion"     => $row["IdGestion"],
                    "NombreGestion" => ucwords(strtolower($row["NombreGestion"])),
                );
            }
            return $data;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function getAsesor($idInmob)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT IdPromotor,Nombres,apellidos,Foto,email,Celular
						FROM inmuebles,usuarios
						WHERE IdInmobiliaria=:idInmob
						and IdPromotor=Id_Usuarios
						AND IdPromotor>0
						GROUP BY IdPromotor");
        $stmt->bindParam(":idInmob", $idInmob);
        if ($stmt->execute()) {
            $data = array();

            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "idInmob"              => $idInmob,
                    "Nombres"              => ucwords(strtolower($row["Nombres"])),
                    "apellidos"            => ucwords(strtolower($row["apellidos"])),
                    "Foto"                 => $row["Foto"],
                    "email"                => $row["email"],
                    "NombreGesCelulartion" => $row["Celular"],
                );
            }
            return $data;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function verifyPortales($id, $idPortal)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("Select p.IdPortal,p.IdInmobiliaria
                from PublicaPortales p
                where p.IdInmobiliaria= :idinmo
                and IdPortal=:portal");
        if ($stmt->execute(array(
            ":idinmo" => $id,
            ":portal" => $idPortal,
        ))) {
            $existe = $stmt->rowCount();
            return $existe;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function cantidadlistadoInmueblesConsignados($data)
    {
        $connPDO   = new Conexion();
        $cond      = "";
        $tabla     = "";
        $enlace    = "";
        $btnVender = "";

        if ($data['idInmu']) {
            $cond .= "  AND
					( idInm=:codinmu
					OR RVivaReal=:codinmu
					OR RLaGuia=:codinmu1
					OR RMtoCuadrado=:codinmu2
					OR RZonaProp=:codinmu3
					OR RMeli =:codinmu4
					Or codinterno=:codinmu5
                    Or codinm=:codinmu6) ";
        }
        if ($data['estadoInm']) {
            $cond .= " and i.idEstadoinmueble=:estadoInm";
        }
        if ($data['gestion']) {
            $cond .= " and i.IdGestion=:gestion";
        }
        if ($data['tContrato']) {
            $cond .= " and i.tipo_contrato=:tContrato";
        }
        if ($data['tInmueble']) {
            $cond .= " and i.IdTpInm=:tInmueble";
        }
        if ($data['barrio']) {
            $cond .= " and i.IdBarrio=:barrio";
        }
        if ($data['ciudad']) {
            $cond .= " and b.IdCiudad=:ciudad";
            $tabla  = ",barrios b";
            $enlace = " and i.IdBarrio=b.IdBarrios ";
        }
        if ($data['consignado']) {
            $cond .= " and i.IdCaptador=:consignado";
        }
        if ($data['ffecha_ini'] > 0 and $data['ffecha_fin'] > 0) {
            $cond .= " and i.FConsignacion between :fecha_inis and :fecha_fins ";
        }

        $stmt = $connPDO->prepare("SELECT i.idInm, i.Direccion, i.ValorVenta, i.ValorCanon, i.IdGestion,
							i.idEstadoinmueble,i.Administracion, i.FConsignacion, i.restricciones,i.RLaGuia,
							i.RVivaReal,i.RZonaProp,i.RMtoCuadrado,i.Estrato,i.AreaConstruida ,
							i.NumLlaves,i.NumCasillero,i.descripcionlarga,i.PublicaM2,i.PublicaMeli,
							i.PublicaMiCasa,i.PublicaZonaProp,i.PublicaDoomos,i.PublicaLamudi,i.PublicaGuia,
							i.Publicagpt,i.AreaLote,i.EdadInmueble,i.usu_crea,i.RNcasa,i.IdTpInm, i.IdBarrio,
                            i.tipo_contrato,i.idProcedencia,i.RIdnd,i.IdInmobiliaria
							from inmuebles i $tabla
							where IdInmobiliaria=:idinmo " .
            $enlace . $cond . "
							#limit 0,30");
        $stmt->bindParam(':idinmo', $data['inmo']);
        //$stmt->bindParam(':idinmo',$id);
        if ($data['idInmu']) {
            $stmt->bindParam(':codinmu', $data['idInmu']);
            $stmt->bindParam(':codinmu1', $data['idInmu']);
            $stmt->bindParam(':codinmu2', $data['idInmu']);
            $stmt->bindParam(':codinmu3', $data['idInmu']);
            $stmt->bindParam(':codinmu4', $data['idInmu']);
            $stmt->bindParam(':codinmu5', $data['idInmu']);
            $stmt->bindParam(':codinmu6', $data['idInmu']);
        }
        if ($data['ciudad']) {
            $stmt->bindParam(':ciudad', $data['ciudad']);
        }
        if ($data['estadoInm']) {
            $stmt->bindParam(':estadoInm', $data['estadoInm']);
        }
        if ($data['gestion']) {
            $stmt->bindParam(':gestion', $data['gestion']);
        }
        if ($data['tContrato']) {
            $stmt->bindParam(':tContrato', $data['tContrato']);
        }
        if ($data['tInmueble']) {
            $stmt->bindParam(':tInmueble', $data['tInmueble']);
        }
        if ($data['barrio']) {
            $stmt->bindParam(':barrio', $data['barrio']);
        }
        if ($data['consignado']) {
            $stmt->bindParam(':consignado', $data['consignado']);
        }
        if ($data['ffecha_ini'] > 0 and $data['ffecha_fin'] > 0) {
            $stmt->bindParam(':fecha_inis', $data['ffecha_ini']);
            $stmt->bindParam(':fecha_fins', $data['ffecha_fin']);
        }
        if ($stmt->execute()) {
            $data        = array();
            $rowsPerPage = 50;
            $connPDO->exec('chartset="utf-8"');
            $datos         = $stmt->rowCount();
            $total_paginas = ceil($datos / $rowsPerPage);

            return $data[] = array(
                "paginas"   => $total_paginas,
                "registros" => $datos,
            );
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    /*********************************
     *          Imagenes             *
     *********************************/
    private static function saveCodFotosOLD($codInmo)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare(" INSERT IGNORE INTO fotos
						      (idInm) VALUES(:idInm)");
        if ($stmt->execute(array(
            ":idInm" => $codInmo,
        ))) {
            return 1;
        } else {
            return print_r(array(
                "error" => $stmt->errorInfo(),
                "data"  => $codInmo,
            ));
        }
        $stmt = "";
        $stmt = null;
    }
    public function saveFotosOld($codInmu, $file, $num)
    {
        $connPDO = new Conexion();

        if (Inmuebles::saveCodFotosOLD($codInmu) == 1) {

            $stmt = $connPDO->prepare("UPDATE fotos SET
					Foto$num = :imagen
					WHERE idInm = :idInm");
            if ($stmt->execute(array(
                ":imagen" => $file,
                ":idInm"  => $codInmu,
            ))) {
                $response[] = array(
                    'status' => "Imagenes Cargadas",
                    'files'  => $file,
                    'No'     => $num,
                );
            } else {
                $response[] = array(
                    'status' => $stmt->errorInfo(),
                    'files'  => $imagen,
                    'No'     => $num,
                );
            }
            $stmt = "";

        } else {
            $response = array($files);
        }

        // foreach (
        //  $value) {
        //     $response[]=array(
        //         "rutas"=>$value['ruta'],
        //         "Nos"=>$value['No']
        //         );
        // }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function saveFotosNew($files, $data)
    {
        $connPDO = new Conexion();

        foreach ($files as $key => $value) {
            // $num     = $value['No'];
            $imagen      = str_replace("../mcomercialweb/", "", $value['ruta']);
            $num         = Inmuebles::getCodPosiFoto($data['idInm']);
            $codInmu     = $data['idInm'];
            $idInmo      = $_SESSION['IdInmmo'];
            $conse       = consecutivo("conse", "fotos_nvo");
            $fec_fto     = date('Y-m-d');
            $usr_fto     = getCampo('usuarios', "where Id_Usuarios='" . $_SESSION['Id_Usuarios'] . "'", 'iduser');
            $separadores = substr_count($codInmu, "-");
           // echo $codInmu;
            if ($separadores == 1) {
                list($aa, $codinm_fto) = explode("-", $codInmu);
            } elseif ($separadores == 2) {
                list($aa, $bb, $codinm_fto) = explode("-", $codInmu);
            }
            $stmt = $connPDO->prepare("INSERT INTO fotos_nvo (
            conse,
            posi_ft,
            id_inmft,
            foto,
            est_fot,
            aa_fto,
            codinm_fto,
            fec_fto,
            usr_fto)
                values (
                :conse,
                :posi_ft,
                :id_inmft,
                :foto,
                :est_fot,
                :aa_fto,
                :codinm_fto,
                :fec_fto,
                :usr_fto)");

            if ($stmt->execute(array(
                ":conse"      => $conse,
                ":posi_ft"    => $num,
                ":id_inmft"   => $codInmu,
                ":foto"       => $imagen,
                ":est_fot"    => 1,
                ":aa_fto"     => $idInmo,
                ":codinm_fto" => $codinm_fto,
                ":fec_fto"    => $fec_fto,
                ":usr_fto"    => $usr_fto,
                ":usr_fto"    => $usr_fto,
            ))) {
                Inmuebles::selectFotosNvo($data['idInm']);
                //echo "where idInm=".;
                //exit;
                $where= "where idInm= '".$codInmu."' and IdInmobiliaria = '".$idInmo."'";
                $notificado = getCampo('inmuebles2',$where,'notificado');
                $response[] = array(
                    "status"    => 1,
                    "uri"       => $imagen,
                    "label_fto" => getCampo('fotos_nvo',"where aa_fto=$idInmo and codinm_fto=$codinm_fto and posi_ft=$num",'label_fto'),
                    "ficha_fto" => getCampo('fotos_nvo',"where aa_fto=$idInmo and codinm_fto=$codinm_fto and posi_ft=$num",'ficha_fto'),
                    "posi_ft"   => $num,
                    "inmobiliaria"   => getCampo('clientessimi',"where IdInmobiliaria=".$_SESSION["IdInmmo"],'Nombre'),
                    "notificado" => $notificado,
                    "avatar" => "http://www.simiinmobiliarias.com/".str_replace("../","",getCampo('clientessimi',"where IdInmobiliaria=".$_SESSION["IdInmmo"],'logo'))
                );

                if($notificado == 0){
                    $stmt=$connPDO->prepare("UPDATE inmuebles2 
                        SET 
                        notificado     =:notificado
                        WHERE idInm         =:idInm
                        AND IdInmobiliaria   =:IdInmobiliaria");

                    if($stmt->execute(array(
                        ":idInm"            => $codInmu,
                        ":IdInmobiliaria"    => $idInmo,
                        ":notificado"  => 1
                        )))
                    {
                        //echo "aaa".$codInmu."-".$idInmo;
                       
                    }
                }

                
            } else {
                $response[] = array("Error" => $stmt->errorInfo());
            }
        }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    private static function selectFotosNvo($cod)
    {
        $connPDO = new Conexion();

                                 $stmt = $connPDO->prepare("SELECT  posi_ft,foto
                                 FROM fotos_nvo
                                 where id_inmft=:IdInm
                                 and est_fot=1
                                 order by posi_ft");
        if ($stmt->execute(array(
            "IdInm" => $cod
        ))) {
            while ($row = $stmt->fetch()) {
                // $count++;
                $foto      = $row['foto'];
                $count     = $row['posi_ft'];
                // $foto="Foto".$count;
                Inmuebles::saveFotosOld($cod, $foto, $count);
            }
        } else {
            print_r(array(
                "status" => "Select Fotos Nvo",
                "Error"  => $stmt->errorInfo(),
            ));
        }
        $stmt = "";
        $stmt = null;
    }
     public function updateLabelFotosNew($data)
    {
        $connPDO = new Conexion();

        //foreach ($files as $key => $value) {
            // $num     = $value['No'];

            $stmt = $connPDO->prepare("UPDATE fotos_nvo
                SET
                label_fto      =:label_fto
                where posi_ft  =:posi_ft
                and aa_fto     =:IdInmobiliaria
                and codinm_fto =:codinm");

            if ($stmt->execute(array(
                ":label_fto"      => $data["label_fto"],
                ":posi_ft"        => $data['posi_ft'],
                ":IdInmobiliaria" => $data['IdInmobiliaria'],
                ":codinm"         => $data['codinm']
            ))) {
                //Inmuebles::selectFotosNvo($data['idInm']);
               return 1;
            } else {
                $response[] = array("Error" => $stmt->errorInfo());
            }
       // }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function deleteTotalFotosOld($cod)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("DELETE  FROM fotos
								WHERE idInm = :idInm ");
        if ($stmt->execute(array(
            "idInm" => $cod['codInmu'],
        ))) {
            return 1;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function deleteTotalFotosNvo($cod)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("DELETE  FROM fotos_nvo
								 WHERE id_inmft = :idInm");
        if ($stmt->execute(array(
            ":idInm" => $cod['codInmu'],
        ))) {
            return 1;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    private static function getCodPosiFoto($cod)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT  max(posi_ft) FROM fotos_nvo
			                     where id_inmft=:IdInm");

        if ($stmt->execute(array(
            ":IdInm" => $cod,
        ))) {
            $result = "";
            if ($stmt->rowCount() > 0) {
                $result = $stmt->fetch(PDO::FETCH_COLUMN);
                return $result + 1;
            }
        }
        $stmt = null;
    }
    public function getFotosOld($data)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare('SELECT * FROM fotos
								 WHERE idInm = :idInm');
        if ($stmt->execute(array(
            ":idInm" => $data["codInmue"],
        ))) {
            if ($stmt->rowCount() > 0) {

                $response = array();
                while ($row = $stmt->fetch()) {
                    for ($i = 1; $i < 31; $i++) {
                        if ($row['Foto' . $i] != "F" && !empty($row['Foto' . $i])) {
                            $response[]['Foto'] = $row['Foto' . $i];
                        } else {
                            $response[] = array('status' => "vacio", 'Foto' => "Foto" . $i);
                        }
                    }
                }

            } else {
                $response = 0;
            }
        } else {
            $response = array("status" => "Error Traer Imagenes",
                "error"                    => $stmt->errorInfo());
        }
        if ($response) {
            return $response;
        }
        $stmt = null;
    }

    public function getFotosNew($data)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare('SELECT ficha_fto,foto,label_fto,posi_ft
                                 FROM fotos_nvo
                                 WHERE id_inmft = :idInm');

        if ($stmt->execute(array(
            ":idInm" => $data["codInmu"],
        ))) {

            if ($stmt->rowCount() > 0) 
            {

                $response = array();
                while ($row = $stmt->fetch()) {


                        $fotoA = explode("/",$row['foto']);
                        $fotoancho = count($fotoA);
                        $response[] = array(
                            "status"    => 1,
                            "Foto"      => $row['foto'],
                            "fotoOrder" => $fotoA[$fotoancho-1],
                            "label_fto" => utf8_encode($row['label_fto']),
                            "ficha_fto" => $row['ficha_fto'],
                            "posi_ft"   => $row['posi_ft']
                        );
                    //} else {
                        $response[] = array('status' => "vacio", 'Foto' => "Foto" . $i);
                    //}

                }

            } 
            else 
            {
                $response = 0;
            }
        } 
        else 
        {
            $response = array("status" => "Error Traer Imagenes",
                "error"                => $stmt->errorInfo());
        }
        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function getFotosNew2($data)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare('SELECT ficha_fto,foto,label_fto,posi_ft
                                 FROM fotos_nvo
                                 WHERE codinm_fto = :idInm
                                 AND aa_fto=:IdInmobiliaria');

        if ($stmt->execute(array(
            ":idInm"          => $data["codInmue"],
            ":IdInmobiliaria" => $data["inmCod"]
        ))) {

            if ($stmt->rowCount() > 0) {

                $response = array();
                while ($row = $stmt->fetch()) {


                        $fotoA = explode("/",$row['foto']);
                        $fotoancho = count($fotoA);
                        $response[] = array(
                            "status"    => 1,
                            "Foto"      => $row['foto'],
                            "fotoOrder" => $fotoA[$fotoancho-1],
                            "label_fto" => $row['label_fto' ],
                            "ficha_fto" => $row['ficha_fto' ],
                            "posi_ft"   => $row['posi_ft']
                        );
                    //} else {
                        $response[] = array('status' => "vacio", 'Foto' => "Foto" . $i);
                    //}

                }

            } else {
                $response = 0;
            }
        } else {
            $response = array("status" => "Error Traer Imagenes",
                "error"                    => $stmt->errorInfo());
        }
        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function deleteFtosNew($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare('DELETE FROM fotos_nvo
								 WHERE  id_inmft = :idInm AND Foto = :Foto');
        if ($stmt->execute(array(
            ":idInm" => $data['codInmu'],
            ":Foto"  => $data['url'],
        ))) {
            if ($stmt->rowCount() > 0) {
                // Inmuebles::selectFotosNvo($data['codInmu']);
            } else {
                return 0;
            }
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }

    public function deleteFtosOlds($data)
    {
        $connPDO = new Conexion();
        for ($i = 1; $i < 31; $i++) {
            $stmt = $connPDO->prepare("UPDATE  fotos SET
					               Foto$i=''
                                   WHERE  idInm = :idInm
                                   AND Foto$i = :Foto");
            if ($stmt->execute(array(
                ":idInm" => $data['codInmu'],
                ":Foto"  => $data['url'],
            ))) {
                $response = array();
                if ($stmt->rowCount() > 0) {
                    $response['foto'] = 1;
                } else {
                    $response['foto'] = 0;
                }
            } else {
                print_r($smtm->errorInfo());
            }
        }
        if ($response) {
            return $response;
        }
        $stmt = null;
    }

    public function listadoInmueblesConsignadosNvo($get, $data)
    {
        $connPDO   = new Conexion();
        $cond      = "";
        $tabla     = "";
        $enlace    = "";
        $btnVender = "";

        /*datatable*/
        $aColumns = array("i.idInm", "i.Direccion", "i.ValorVenta", "i.ValorCanon", "i.IdGestion", "i.idEstadoinmueble", "i.Administracion", "i.FConsignacion", "i.restricciones", "i.RLaGuia", "i.RVivaReal", "i.RZonaProp", "i.RMtoCuadrado", "i.Estrato", "i.AreaConstruida",
            "i.NumLlaves", "i.NumCasillero", "i.descripcionlarga", "i.PublicaM2", "i.PublicaMeli",
            "i.PublicaMiCasa", "i.PublicaZonaProp", "i.PublicaDoomos", "i.PublicaLamudi", "i.PublicaGuia",
            "i.Publicagpt", "i.AreaLote", "i.EdadInmueble", "i.usu_crea", "i.RNcasa", "i.IdTpInm", "i.IdBarrio",
            "i.tipo_contrato", "i.idProcedencia", "i.RIdnd", "i.IdInmobiliaria");

        /* Individual column filtering */

        $sWhere = "";
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($get['search']) && $get["search"]["value"] != '') {
                if ($sWhere == "") {
                    $sWhere = " AND (";
                } else {
                    $sWhere .= " OR ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($get["search"]["value"]) . "%' ";
            }
        }

        if ($sWhere != "") {

            $sWhere .= ")";

        }

        /*
         * paginacion
         */
        $sLimit = " LIMIT 0,50 ";
        if (isset($get['start']) && $get['length'] != '-1') {
            $sLimit = " LIMIT " . intval($get['start']) . ", " .
            intval($get['length']);
        }

        /*datatable*/

        if ($data['idInmu']) {
            $cond .= "  AND
					( idInm=:codinmu
					OR RVivaReal=:codinmu
					OR RLaGuia=:codinmu1
					OR RMtoCuadrado=:codinmu2
					OR RZonaProp=:codinmu3
					OR RMeli =:codinmu4
					Or codinterno=:codinmu5
                    Or codinm=:codinmu6) ";
        }
        if ($data['estadoInm']) {
            $cond .= " and i.idEstadoinmueble=:estadoInm";
        }
        if ($data['gestion']) {
            $cond .= " and i.IdGestion=:gestion";
        }
        if ($data['tContrato']) {
            $cond .= " and i.tipo_contrato=:tContrato";
        }
        if ($data['tInmueble']) {
            $cond .= " and i.IdTpInm=:tInmueble";
        }
        if ($data['barrio']) {
            $cond .= " and i.IdBarrio=:barrio";
        }
        if ($data['ciudad']) {
            $cond .= " and b.IdCiudad=:ciudad";
            $tabla  = ",barrios b";
            $enlace = " and i.IdBarrio=b.IdBarrios ";
        }
        if ($data['consignado']) {
            $cond .= " and i.IdCaptador=:consignado";
        }
        if ($data['ffecha_ini'] > 0 and $data['ffecha_fin'] > 0) {
            $cond .= " and i.FConsignacion between :fecha_inis and :fecha_fins ";
        }
        $rowsPerPage = 50;
        $offset      = ($data['paginador'] - 1) * $rowsPerPage;

        $stmt = $connPDO->prepare("SELECT i.idInm, i.Direccion, i.ValorVenta, i.ValorCanon, i.IdGestion,
							i.idEstadoinmueble,i.Administracion, i.FConsignacion, i.restricciones,i.RLaGuia,
							i.RVivaReal,i.RZonaProp,i.RMtoCuadrado,i.Estrato,i.AreaConstruida ,
							i.NumLlaves,i.NumCasillero,i.descripcionlarga,i.PublicaM2,i.PublicaMeli,
							i.PublicaMiCasa,i.PublicaZonaProp,i.PublicaDoomos,i.PublicaLamudi,i.PublicaGuia,
							i.Publicagpt,i.AreaLote,i.EdadInmueble,i.usu_crea,i.RNcasa,i.IdTpInm, i.IdBarrio,
                            i.tipo_contrato,i.idProcedencia,i.RIdnd,i.IdInmobiliaria
							from inmuebles i $tabla
							where IdInmobiliaria=" . $_SESSION["IdInmmo"] .
            $enlace . $cond . $sWhere . $sLimit);
        $sql = "SELECT i.idInm, i.Direccion, i.ValorVenta, i.ValorCanon, i.IdGestion,
							i.idEstadoinmueble,i.Administracion, i.FConsignacion, i.restricciones,i.RLaGuia,
							i.RVivaReal,i.RZonaProp,i.RMtoCuadrado,i.Estrato,i.AreaConstruida ,
							i.NumLlaves,i.NumCasillero,i.descripcionlarga,i.PublicaM2,i.PublicaMeli,
							i.PublicaMiCasa,i.PublicaZonaProp,i.PublicaDoomos,i.PublicaLamudi,i.PublicaGuia,
							i.Publicagpt,i.AreaLote,i.EdadInmueble,i.usu_crea,i.RNcasa,i.IdTpInm, i.IdBarrio,
                            i.tipo_contrato,i.idProcedencia,i.RIdnd,i.IdInmobiliaria
							from inmuebles i $tabla
							where IdInmobiliaria=" . $_SESSION["IdInmmo"] .
            $enlace . $cond . $sWhere . $sLimit;
        //echo $sql;
        //print_r($data);
        //exit;

        // $stmt->bindParam(':idinmo',$_SESSION["IdInmmo"]);

        //$stmt->bindParam(':idinmo',$id);
        if ($data['idInmu']) {
            $stmt->bindParam(':codinmu', $data['idInmu']);
            $stmt->bindParam(':codinmu1', $data['idInmu']);
            $stmt->bindParam(':codinmu2', $data['idInmu']);
            $stmt->bindParam(':codinmu3', $data['idInmu']);
            $stmt->bindParam(':codinmu4', $data['idInmu']);
            $stmt->bindParam(':codinmu5', $data['idInmu']);
            $stmt->bindParam(':codinmu6', $data['idInmu']);
        }
        if ($data['ciudad']) {
            $stmt->bindParam(':ciudad', $data['ciudad']);
        }
        if ($data['estadoInm']) {
            $stmt->bindParam(':estadoInm', $data['estadoInm']);
        }
        if ($data['gestion']) {
            $stmt->bindParam(':gestion', $data['gestion']);
        }
        if ($data['tContrato']) {
            $stmt->bindParam(':tContrato', $data['tContrato']);
        }
        if ($data['tInmueble']) {
            $stmt->bindParam(':tInmueble', $data['tInmueble']);
        }
        if ($data['barrio']) {
            $stmt->bindParam(':barrio', $data['barrio']);
        }
        if ($data['consignado']) {
            $stmt->bindParam(':consignado', $data['consignado']);
        }
        if ($data['ffecha_ini'] > 0 and $data['ffecha_fin'] > 0) {
            $stmt->bindParam(':fecha_inis', $data['ffecha_ini']);
            $stmt->bindParam(':fecha_fins', $data['ffecha_fin']);
        }
        if ($stmt->execute()) {

            /*datatable*/

            $data = array();
            $connPDO->exec('chartset="utf-8"');
            while ($row = $stmt->fetch()) {
                $catidadResgistros = count($row);
                $existeFoto        = $this->VerificaFotos($row['idInm']);
                $idciud            = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], "IdCiudad");
                $btnPortales       = ($existeFoto > 0) ? '<button data-placement="bottom" title="Sincronizar a Portales" type="button" class="btn btn-warning  btnSincro  btn-xs btns "  data-toggle="modal"  data-target="#sincroPorta"><span class="glyphicon glyphicon"><i class="fa fa-upload"></i></span></button>' : '<label>Agregue Fotos para Publicar</label>';
                $btnPagos          = '<button data-placement="bottom" title="Portales" type="button" id="pagos" class="btn btn-info  pagos  btn-xs btns"  data-toggle="modal"  data-target=".mymodalpagos"><span class="glyphicon glyphicon"><i class="fa fa-files-o"></i></span></button>';
                $btnficha          = '<a href="../../mcomercialweb/inmuebles/buscainmueblesnvo2.php?codinmueble=' . $row['idInm'] . '"><button type="button" id="ficha" data-placement="bottom" title="Ver Detalles" class="btn btn-info  ficha  btn-xs btns"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button></a>';
                $btnEditar         = '<a href="../../mcomercialweb/EditarInmueble.php?inm=' . $row['idInm'] . '"><button type="button" id="edita" class="btn btn-watermelon  edita  btn-xs btns" data-placement="bottom" title="Editar Inmueble" ><span class="glyphicon glyphicon"><i class="fa fa-cogs"></i></span></button></a>';
                $btnReversar       = '<a href="../../mcomercialweb/reversar_inmueble.php?regresa=1&Inmueble=' . $row['idInm'] . '&nvoi=1"><button type="button" id="reversar" class="btn btn-spartan  reversar  btn-xs btns" data-placement="bottom" title="Cambiar Estado" ><span class="glyphicon glyphicon"><i class="fa fa-edit"></i></span></button></a>';
                if (($row['IdGestion'] == 5 || $row['IdGestion'] == 2) && $row['idEstadoinmueble'] != 3) {
                    $btnVender = '<a href="../../mcomercialweb/vender_inmueble.php?regresa=1&Inmueble=' . $row['idInm'] . '"><button type="button" id="vender" class="btn btn-info  vender  btn-xs btns" data-placement="bottom" title="Vender Inmueble" ><span class="glyphicon glyphicon"><i class="fa fa-money"></i></span></button></a>';
                }

                //$btnficha='<a href="../../mcomercialweb/buscainmueblesnvo2.php?codinmueble='.$row['idInm'].'"><button type="button" id="ficha" class="btn btn-info  ficha  btn-xs btns"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button></a>';
                $btnDuplicar        = '<button title="Duplicar Inmueble" type="button" id="ficha" class="btn btn-info  ficha  btn-xs btns" onclick="AbrirVentana(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-files-o" data-placement="bottom" ></i></span></button>';
                $btnOrdenar         = '<button  data-placement="bottom" title="Ordenar Fotos" type="button" id="ficha" class="btn btn-purple  ficha  btn-xs btns" onclick="AbrirVentanaOrdenar(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-camera-retro"></i></span></button>';
                $btnObprop          = '<button  data-placement="bottom" title="Observaciones Propietarios" type="button" id="ficha" class="btn btn-warning  ficha  btn-xs btns" onclick="AbrirVentanaProp(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-edit"></i></span></button>';
                $btnVerFicha        = '<button  data-placement="bottom" title="Ver Ficha" type="button" id="ficha" class="btn btn-default  ficha  btn-xs btns" onclick="AbrirVentanaFicha(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-file-photo-o"></i></span></button>';
                $btnVerFichaChicala = ($row['IdInmobiliaria'] == 172 || $row['IdInmobiliaria'] == 1) ? '<button  data-placement="bottom" title="Ver Ficha Chicala" type="button" id="ficha" class="btn btn-success  ficha  btn-xs btns" onclick="AbrirVentanaFichaCh(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-file-picture-o"></i></span></button>' : '';
                $btnVerFichaGomez   = ($row['IdInmobiliaria'] == 447 || $row['IdInmobiliaria'] == 1) ? '<button  data-placement="bottom" title="Ver Ficha Gomez" type="button" id="ficha" class="btn btn-primary  ficha  btn-xs btns" onclick="AbrirVentanaFichaGm(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-file-image-o"></i></span></button>' : '';
                $inputInmu          = '<input type="hidden" id="idInmu" name="idInmu" value="' . $row['idInm'] . '" >';

                ///validar cadena de pruebas metro
                $spruebas = getCampo('usuariosm2', "where inmogm2=" . $row['IdInmobiliaria'] . " and servidor=2 limit 0,1", "servidor");

                $btnCadenaMetro = ($this->verifyPortales($row['IdInmobiliaria'], 10) > 0 && $spruebas == 2) ? '<button  data-placement="bottom" title="Generar Prueba Metro" type="button" id="cadenaM2" class="btn btn-warning  ficha  btn-xs btns" onclick="AbrirCadena(' . "'" . $row['idInm'] . "'" . ',' . "'" . $row['RMtoCuadrado'] . "'" . ',' . getCampo('usuariosm2', "where inmogm2=" . $row['IdInmobiliaria'] . " limit 0,1", "idusariom2") . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-link"></i></span></button>' : '';

                $usucrea     = ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $row['usu_crea'] . "' and Id_Usuarios>0", 'concat(Nombres," ",apellidos)')));
                $cadPortales = '<ul>';
                $cadPortales .= "<li><b>" . $row['idInm'] . "</b></li>";
                if ($row['RLaGuia'] > 0) {
                    $cadPortales .= "<li>FincaRaiz " . $row['RLaGuia'] . "</li>";
                }
                if ($row['RMtoCuadrado'] > 0) {
                    $cadPortales .= "<li>Metro2 " . $row['RMtoCuadrado'] . "</li>";
                }
                if ($row['RNcasa'] > 0) {
                    $cadPortales .= "<li>Nuestra Casa " . $row['RNcasa'] . "</li>";
                }
                if ($row['RIdnd'] > 0) {
                    $cadPortales .= "<li>Idonde " . $row['RIdnd'] . "</li>";
                }
                if ($row['RVivaReal'] > 0) {
                    $cadPortales .= "<li>Interno " . $row['RVivaReal'] . "</li>";
                }
                if ($row['RZonaProp'] > 0) {
                    $cadPortales .= "<li>I24 " . $row['RZonaProp'] . "</li>";
                }
                if ($row['PublicaDoomos'] == 'Publicado') {
                    $cadPortales .= "<li>Gratuitos " . $row['RZonaProp'] . "</li>";
                }
                if ($row['PublicaLamudi'] == 'Publicado') {
                    $cadPortales .= "<li>Lamudi " . $row['PublicaLamudi'] . "</li>";
                }
                if ($row['Publicagpt'] == 'Publicado') {
                    $cadPortales .= "<li>GoPlaceit " . $row['Publicagpt'] . "</li>";
                }
                $flag = getCampo('clientessimi', "where IdInmobiliaria=" . $_SESSION['IdInmmo'], 'flag_portal');
                if ($flag == 10) {
                    $cadPortales = '<ul>';
                    $cadPortales .= "<li>Metro2 " . $row['RMtoCuadrado'] . "</li>";
                    $cadenaBotones = $btnCadenaMetro;
                } elseif ($flag == 8) {
                    $cadPortales = '<ul>';
                    $cadPortales .= "<li>FincaRaiz " . $row['RLaGuia'] . "</li>";
                } else {
                    $cadenaBotones = $btnEditar . $btnReversar . $btnPortales . $btnOrdenar . $btnVerFicha . $btnVerFichaChicala . $btnVerFichaGomez . $btnDuplicar . $btnCadenaMetro . $inputInmu;
                }

                $cadPortales .= '</ul>';

                $data[] = array(
                    "idInm"          => $cadPortales,
                    "Direccion"      => $row['Direccion'],
                    "ValorVenta"     => number_format($row['ValorVenta']),
                    "ValorCanon"     => number_format($row['ValorCanon']),
                    "Gestion"        => ucwords(strtolower(getCampo('gestioncomer', "where IdGestion=" . $row['IdGestion'], "NombresGestion"))),
                    "procedencia"    => ucwords(strtolower(getCampo('procedencia', "where IdProcedencia=" . $row['idProcedencia'], "Nombre"))),
                    "tcontrato"      => ucwords(strtolower(getCampo('tipo_contrato', "where id_contrato=" . $row['tipo_contrato'], "tipo_contrato"))),
                    "TipoInm"        => ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble=" . $row['IdTpInm'], "Descripcion"))),
                    "Barrio"         => ucwords(strtolower(getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], "NombreB"))),
                    "ciudad"         => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . $idciud, "Nombre"))),
                    "Estadoinmueble" => ucwords(strtolower(getCampo('estado_inmueble', "where idestado=" . $row['idEstadoinmueble'], 'descripcion'))),
                    "Administracion" => $row['Administracion'],
                    "FConsignacion"  => $row['FConsignacion'],
                    "Estrato"        => $row['Estrato'],
                    "AreaConstruida" => $row['AreaConstruida'],
                    "AreaLote"       => $row['AreaLote'],
                    "EdadInmueble"   => $row['EdadInmueble'],
                    "usu_crea"       => $usucrea,
                    "RNcasa"         => $row['RNcasa'],
                    "bnts"           => $cadenaBotones,

                );

            }

            /*datatable*/
            /* Data set length after filtering */
            $sQuery = "
			    SELECT FOUND_ROWS() FROM   inmuebles as i where IdInmobiliaria  =" . $_SESSION['IdInmmo'] . $sWhere
            ;
            $stmtFound = $connPDO->prepare($sQuery);
            $stmtFound->execute();

            $iFilteredTotal = $stmtFound->fetch();

            /* Total data set length */
            $sQuery = "
		        SELECT COUNT(idInm) as total
		        FROM   inmuebles as i where IdInmobiliaria =" . $_SESSION['IdInmmo'] . $sWhere;
//echo $sQuery;
            $stmtcount = $connPDO->prepare($sQuery);
            $stmtcount->execute();
            $rResultTotal = $stmtcount->fetchAll();
            //print_r($rResultTotal[0]["total"]);
            $iTotal = $rResultTotal[0]["total"];
//echo $iTotal;
            /*
             * Output
             */
            $output = array(
                "sEcho"                => intval($data['sEcho']),
                "iTotalRecords"        => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal[0],
                "aaData"               => $data,
            );

            //$output['aaData'] = $data;
            return $output;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function getCaracteristicasInmueble($data)
    {
        $connPDO = new Conexion();

        $tpInm   = $data['tpInm'];
        $idGrupo = $data['IdGrupo'];
        $tpCar   = $data['tpCar'];
        $stmt    = $connPDO->prepare("SELECT n.idCaracteristica,n.idTipoInmueble,n.IdGrupo,n.Descripcion,n.tpcar,g.DETALLE
					FROM maestrodecaracteristicasnvo n,grupocaracteristicas g
					WHERE n.IdGrupo=g.IdGrupo
					AND tpcar				=:tpCar
					AND n.idTipoInmueble	=:tpInm
					AND n.IdGrupo		    =:idGrupo

					ORDER BY DETALLE,Descripcion");
        //AND g.car_est			=1
        $stmt->bindParam(":tpCar", $tpCar);
        $stmt->bindParam(":tpInm", $tpInm);
        $stmt->bindParam(":idGrupo", $idGrupo);
        if ($stmt->execute()) {
            $data = array();

            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "idCaracteristica" => $row["idCaracteristica"],
                    "nCaracteristica"  => ucwords(strtolower($row["Descripcion"])),
                    "ngrupo"           => ucwords(strtolower($row["DETALLE"])),
                    "idTipoInmueble"   => $row["idTipoInmueble"],
                );
            }
            return $data;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function getCaracteristicasInmuebleLista($data)
    {
        $connPDO = new Conexion();

        $tpInm = $data['tpInm'];
        $tpCar = $data['tpCar'];
        $i     = 0;
        $stmt  = $connPDO->prepare("SELECT n.idCaracteristica,n.idTipoInmueble,n.IdGrupo as grup,n.Descripcion,n.tpcar,g.DETALLE
					FROM maestrodecaracteristicasnvo n,grupocaracteristicas g
					WHERE n.IdGrupo=g.IdGrupo
					AND tpcar				=:tpCar
					AND n.idTipoInmueble	=:tpInm

					GROUP BY n.IdGrupo
					ORDER BY DETALLE,Descripcion ");
        //AND g.car_est			=1
        $stmt->bindParam(":tpCar", $tpCar);
        $stmt->bindParam(":tpInm", $tpInm);
        $data = '';
        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $i++;
                $data .=
                '<ul class="listaCaracteristica"><li><div class="treeopen" data-item="#tree' . $tpCar.$i . '"><div><label><i class="fa fa-plus" aria-hidden="true"></i> ' . utf8_encode($row['DETALLE']) . ' </label></div></div>' . $this->getCaracteristicasInmuebleListaHijos($row['grup'], $tpInm, $tpCar, $i) . '</li></ul>';
            }
            return $data;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function getCaracteristicasInmuebleListaHijos($IdGrupo, $tpInm, $tpCar, $i)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT n.idCaracteristica,n.idTipoInmueble,n.IdGrupo,n.Descripcion,n.tpcar
					FROM maestrodecaracteristicasnvo n,grupocaracteristicas g
					WHERE n.IdGrupo 		=:IdGrupo
					and n.IdGrupo=g.IdGrupo
					AND n.idTipoInmueble	=:tpInm
					AND tpcar				=:tpCar
					ORDER BY Descripcion ");
        $stmt->bindParam(":IdGrupo", $IdGrupo);
        $stmt->bindParam(":tpInm", $tpInm);
        $stmt->bindParam(":tpCar", $tpCar);
        if ($stmt->execute()) {
            $data = '<ul id="tree' . $tpCar.$i . '">';

            while ($row = $stmt->fetch()) {
                $data .= '<li><div><div class="checkbox"><label><input type="checkbox" name="checkCar" class="checkCar" data-text="#text'.$row['idCaracteristica'].'" data-target="#ch'.$row['idCaracteristica'].'" value=""> <input type="hidden"  value="0" name="checkCarVal['.$row['idCaracteristica'].']" id="ch'.$row['idCaracteristica'].'">' . utf8_encode($row['Descripcion']) . '</label><input type="text" name="caracteristica['.$row['idCaracteristica'].']" class="form-control" readonly id="text'.$row['idCaracteristica'].'"></div></div></li>';
            }
            $data .= '</ul>';
            return $data;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function ubicacionInmuebleNew($data)
    {
        $connPDO = new Conexion();


        $Idinmobiliaria = $data['IdInmobiliaria'];

        $idInm          = $data['IdInmobiliaria']."-".$data['codinm'];
        $estadoin     = (getCampo('inmobiliaria',"where Idinmobiliaria=" . $data['IdInmobiliaria'],'aestado')==1)?8:2;

        $stmt=$connPDO->prepare("UPDATE inmnvo
        	set
			IdBarrio           = :barrio,
			Direccion          = :dircompleta,
			DirMetro           = :DirMetro,
			Carrera            = :carrera,
			Calle              = :calle,
			latitud            = :latitud,
			longitud           = :longitud,
			permite_geo        = :permite_geo,
			idEstadoinmueble   = :estadoin
			WHERE codinm       = :codinm
			AND IdInmobiliaria = :idinmobiliaria");

        if($stmt->execute(array(
            ":barrio"           => $data['IdBarrio'],
            ":dircompleta"      => str_ireplace("#","",$data['dircompleta1']),
            ":DirMetro"         => str_ireplace("#","",$data['DirMetro']),
            ":carrera"          => $data['carrera'],
            ":calle"            => $data['calle'],
            ":latitud"          => $data['latitud'],
            ":longitud"         => $data['longitud'],
            ":permite_geo"      => $data['permite_geo'],
            ":estadoin"         => $estadoin,
            ":codinm"           => $data['codinm'],
            ":idinmobiliaria"   => $data['IdInmobiliaria']
            )))
        {

             $this->ubicacionInmuebleOld($data);
            return 1;
        }
        else
        {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function ubicacionInmuebleOld($data)
    {
        $connPDO      = new Conexion;
        $dircompleta  = strtoupper(str_ireplace("#","",$data['dircompleta']));
        $dircompleta1 = strtoupper($data['dircompleta1']);
        $estadoin     = (getCampo('inmobiliaria',"where Idinmobilairia=" . $data['IdInmobiliaria'],'aestado')==1)?8:2;
        $barrio       = $data['barrio'];
        $fechaserv=date("Y-m-d");
        $permitegeo   = $data['permitegeo'];
        $idInm 		  = $data['IdInmobiliaria']."-".$data['codinm'];

        $stmt = $connPDO->prepare("UPDATE inmuebles2
        	set
			permite_geo        = :permite_geo,
			IdBarrio           = :barrio,
			Direccion          = :dircompleta,
			DirMetro           = :DirMetro,
			latitud            = :latitud,
			longitud           = :longitud,
			Calle              = :calle,
			Carrera            = :carrera,
			idEstadoinmueble   = :estadoin,
			DirMetro           = :DirMetro
			WHERE codinm       = :codinm
			AND IdInmobiliaria = :idinmobiliaria");

        if($stmt->execute(array(
            ":barrio"           => $data['IdBarrio'],
            ":dircompleta"      => str_ireplace("#","",$data['dircompleta1']),
            ":direccion2"       => str_ireplace("#","",$data['direccion2']),
            ":DirMetro"         => str_ireplace("#","",$data['DirMetro']),
            ":carrera"          => $data['carrera'],
            ":calle"            => $data['calle'],
            ":latitud"          => $data['latitud'],
            ":longitud"         => $data['longitud'],
            ":permite_geo"      => $data['permite_geo'],
            ":estadoin"         => $estadoin,
            ":codinm"           => $data['codinm'],
            ":idinmobiliaria"   => $data['IdInmobiliaria']
            )))
        {
            logUbicacion($data,$idInm);
            return 1;
        }
        else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }

    public function logUbicacion($data,$idInm)
    {
    	$f_sis=date('Y-m-d');
		$h_sis=date('H:i:s');
		$ip=$_SERVER['REMOTE_ADDR'];
		$IdDemanda=$idInm;
    	$connPDO      = new Conexion;
    	$stmt = $connPDO->prepare("SELECT IdBarrio,Direccion,DirMetro,latitud,
						longitud,fmodificacion,Calle,permite_geo,Carrera
						FROM inmuebles2
						WHERE idInm=:idInm");
    	$stmt->bindParam(':idInm', $idInm);

    	if ($stmt->execute()) {
    		while($row=$stmt->fetch())
			{
				$IdBarrio_or		= $row['IdBarrio'];
				$Direccion_or		= $row['Direccion'];
				$DirMetro_or		= $row['DirMetro'];
				$latitud_or			= $row['latitud'];
				$longitud_or		= $row['longitud'];
				$Calle_or			= $row['Calle'];
				$permite_geo_or		= $row['permite_geo'];
				$Carrera_or			= $row['Carrera'];

			}
			if($IdBarrio_or!=$data['IdBarrio'])
			{
				$cambio++;
				$nom_campo="IdBarrio";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="Barrio"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['IdBarrio'],$IdBarrio_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($Direccion_or!=$data['Direccion'])
			{
				$cambio++;
				$nom_campo="Direccion";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="Direccion"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['Direccion'],$Direccion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($DirMetro_or!=$data['DirMetro'])
			{
				$cambio++;
				$nom_campo="DirMetro";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="DirMetro"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['DirMetro'],$DirMetro_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($latitud_or!=$data['latitud'])
			{
				$cambio++;
				$nom_campo="latitud";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="latitud"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['latitud'],$latitud_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($longitud_or!=$data['longitud'])
			{
				$cambio++;
				$nom_campo="longitud";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="longitud"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['longitud'],$longitud_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
		if($Calle_or!=$data['calle'])
			{
				$cambio++;
				$nom_campo="Calle";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="Calle"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['calle'],$Calle_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($permite_geo_or!=$data['permite_geo'])
			{
				$cambio++;
				$nom_campo="permite_geo";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="permite_geo"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['permite_geo'],$permite_geo_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($Carrera_or!=$data['carrera'])
			{
				$cambio++;
				$nom_campo="Carrera";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="Carrera"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['carrera'],$Carrera_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			return true;
    	} else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }


    public function consecInmuNormal($inmob)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT consecinmueble from inmobiliaria where IdInmobiliaria=:inmob");
        $stmt->bindParam(":inmob", $inmob);
        if ($stmt->execute()) {
        	while ($row = $stmt->fetch()) {
        		$response = $row['consecinmueble'];
        	}
        	return $response;
        } else {
        	print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function consecInmuNew($inmob)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT MAX(codinm) as conse from inmnvo where IdInmobiliaria=:inmob");
        $stmt->bindParam(":inmob", $inmob);
        if ($stmt->execute()) {
            $cantidad = $stmt->rowCount();

            if ($cantidad <= 0) {
                $response = $this->consecInmuNormal($inmob);
            } else {
                while ($row = $stmt->fetch()) {
                    $response = ($this->consecInmuNormal($inmob) < $row['conse']) ? $row['conse'] : $this->consecInmuNormal($inmob);
                }
            }
            //$this->updateconsecInmu($response+1,$inmob);
            return $response+1;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function updateconsecInmu($consec,$inmob)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("UPDATE inmobiliaria
        	                        set consecinmueble=:consec
        	                        where IdInmobiliaria=:inmob");
        $stmt->bindParam(":inmob", $inmob);
        $stmt->bindParam(":consec", $consec);
        if ($stmt->execute()) {
            $cantidad = $stmt->rowCount();

            if ($cantidad <= 0) {
                $response = $this->consecInmuNormal($inmob);
            } else {
                while ($row = $stmt->fetch()) {
                    $response = ($this->consecInmuNormal($inmob) < $row['conse']) ? $row['conse'] : $this->consecInmuNormal($inmob);
                }
            }
            return $response+1;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function saveFinancialOld($data)
    {
        $connPDO = new Conexion();


        $codinm         = $this->consecInmuNew($data['IdInmobiliaria']);
        $idInm          = $data['IdInmobiliaria']."-".$codinm;
        $fechaserv		= date('Y-m-d');
        $usu_creacion	= $_SESSION['iduser'];
		$h_creacion     = date('Y-m-d');
		$ip             = $_SERVER['REMOTE_ADDR'];
		$estadoin     = (getCampo('inmobiliaria',"where Idinmobiliaria=" . $data['IdInmobiliaria'],'aestado')==1)?8:2;
		if($_POST['destacado']=='on'){ $destacado=1;}	else{ $destacado=0;}
        if($data['destinacion']==2)
		{
			$valIva  = $data['ValorIva'];
		}
		else
		{
			$valIva  = 0;
		}
        if($data['incluida']=='on'){ $incluida=1;}	else{ $incluida=0;}
        if($data['amoblado']=='on'){ $amoblado=1;}	else{ $amoblado=0;}
        if($data['destacado']=='on'){ $destacado=1;}	else{ $destacado=0;}
        $stmt=$connPDO->prepare("INSERT INTO inmuebles2
		 (IdGestion,FConsignacion,Estrato,AreaConstruida,AreaLote,
			EdadInmueble,NoMatricula,chip,AvaluoCatastral,CedulaCatastral,
			fingreso,amoblado,restricciones,obserinterna,codinterno,
			FechaAvaluoCatastral,ValorVenta,ValorCanon,Administracion,ValorIva,
			admondto,IdProcedencia,idCaptador,IdPromotor,IdDestinacion,
			AdmonIncluida,IdTpInm,idInm,codinm,usu_creacion,
			f_creacion,h_creacion,IdInmobiliaria,idEstadoinmueble)
			VALUES
			(:gestioncomercial,:fecha_inicio,:estrato,:areaconstruida,:arealote,
			:anoconstruccion,:nromatri,:chip,:calledavaluocatas,:cedulacatast,
			:fingreso,:amoblado,:restricciones,:obserinterna,:codinterno,
			:Favaluo,:valventa,:canon,:admon,:valIva,
			:admondto,:procedencia,:codcaptador,:idpromotor,:destinacion,
			:incluida,:tipoinmueble,:idInm,:codinm,:usu_creacion,
			:f_creacion,:h_creacion,:IdInmobiliaria,:idEstadoinmueble)");

        if($stmt->execute(array(
			":gestioncomercial"  => $data['gestioncomercial'],
			":fecha_inicio"      => $data['fecha_inicio'],
			":estrato"           => $data['estrato'],
			":areaconstruida"    => str_ireplace(".","",$data['areaconstruida']),
			":arealote"          => str_ireplace(".","",$data['arealote']),
			":anoconstruccion"   => $data['anoconstruccion'],
			":nromatri"          => $data['nromatri'],
			":chip"              => $data['chip'],
			":calledavaluocatas" => $data['calledavaluocatas'],
			":cedulacatast"      => $data['cedulacatast'],
			":fingreso"     	 => $fechaserv,
			":amoblado"          => $amoblado,
			":restricciones"     => $data['restricciones'],
			":obserinterna"      => $data['obserinterna'],
			":codinterno"        => $data['codinterno'],
			":Favaluo"           => $data['Favaluo'],
			":valventa"          => str_ireplace(".","",$data['valventa']),
			":canon"             => str_ireplace(".","",$data['canon']),
			":admon"             => str_ireplace(".","",$data['admon']),
			":valIva"            => str_ireplace(".","",$valIva),
			":admondto"          => str_ireplace(".","",$data['admondto']),
			":procedencia"       => $data['procedencia'],
			":codcaptador"       => $data['codcaptador'][0],
			":idpromotor"        => $data['idpromotor'][0],
			":destinacion"       => $data['destinacion'],
			":incluida"          => $incluida,
			":tipoinmueble"      => $data['tipoinmueble'],
			":idInm"             => $idInm,
			":codinm"            => $codinm,
			":IdInmobiliaria"    => $data['IdInmobiliaria'],
			":usu_creacion"      => $usu_creacion,
			":f_creacion"        => $fechaserv,
			":h_creacion"        => $h_creacion,
			":idEstadoinmueble"  => $estadoin
            )))
        {
           //destacados
        	//guardar_settings_portales($Idinmobiliaria,$idInm,0,4,$destacado,$_SESSION['Id_Usuarios'],$fechaserv,$h_creacion,$ip);
            //saveLogFinancial($data,$idInm);
            $this->saveFinancialNew($data,$codinm);
            $inmueb=array();
            return $inmueb[]=array("codinmu"=>$codinm);
        }
        else
        {
           return print_r($stmt->errorInfo())." error";
        }
        $stmt = null;
    }
    public function saveFinancialNew($data,$codinm)
   //  public function saveFinancialNew($data)
    {
        $connPDO = new Conexion();

        //$codinm         = $this->consecInmuNew($data['IdInmobiliaria']);
        $idInm          = $data['IdInmobiliaria']."-".$codinm;
        $fechaserv		= date('Y-m-d');
        $usu_creacion	= $_SESSION['iduser'];
		$h_creacion     = date('Y-m-d');
		$ip             = $_SERVER['REMOTE_ADDR'];
		if($_POST['destacado']=='on'){ $destacado=1;}	else{ $destacado=0;}
        if($data['destinacion']==2)
		{
			$valIva  = $data['ValorIva'];
		}
		else
		{
			$valIva  = 0;
		}
        if($data['incluida']=='on'){ $incluida=1;}	else{ $incluida=0;}

        $stmt=$connPDO->prepare("INSERT INTO inmnvo
		 (IdGestion,FConsignacion,Estrato,AreaConstruida,AreaLote,
			EdadInmueble,NoMatricula,chip,AvaluoCatastral,CedulaCatastral,
			fingreso,restricciones,obserinterna,codinterno,FechaAvaluoCatastral,
			ValorVenta,ValorCanon,Administracion,ValorIva,admondto,
			IdProcedencia,IdDestinacion,AdmonIncluida,IdTpInm,idInm,
			codinm,usu_creacion,f_creacion,h_creacion,IdInmobiliaria)
			VALUES
			(:gestioncomercial,:fecha_inicio,:estrato,:areaconstruida,:arealote,
			:anoconstruccion,:nromatri,:chip,:calledavaluocatas,:cedulacatast,
			:fingreso,:restricciones,:obserinterna,:codinterno,:Favaluo,
			:valventa,:canon,:admon,:valIva,:admondto,
			:procedencia,:destinacion,:incluida,:tipoinmueble,:idInm,
			:codinm,:usu_creacion,:f_creacion,:h_creacion,:IdInmobiliaria)");

        if($stmt->execute(array(
			":gestioncomercial"  => $data['gestioncomercial'],
			":fecha_inicio"      => $data['fecha_inicio'],
			":estrato"           => $data['estrato'],
			":areaconstruida"    => str_ireplace(".","",$data['areaconstruida']),
			":arealote"          => str_ireplace(".","",$data['arealote']),
			":anoconstruccion"   => $data['anoconstruccion'],
			":nromatri"          => $data['nromatri'],
			":chip"              => $data['chip'],
			":calledavaluocatas" => $data['calledavaluocatas'],
			":cedulacatast"      => $data['cedulacatast'],
			":fingreso"     	 => $fechaserv,
			":restricciones"     => $data['restricciones'],
			":obserinterna"      => $data['obserinterna'],
			":codinterno"        => $data['codinterno'],
			":Favaluo"           => $data['Favaluo'],
			":valventa"          => str_ireplace(".","",$data['valventa']),
			":canon"             => str_ireplace(".","",$data['canon']),
			":admon"             => str_ireplace(".","",$data['admon']),
			":valIva"            => str_ireplace(".","",$valIva),
			":admondto"          => str_ireplace(".","",$data['admondto']),
			":procedencia"       => $data['procedencia'],
			":destinacion"       => $data['destinacion'],
			":incluida"          => $incluida,
			":tipoinmueble"      => $data['tipoinmueble'],
			":idInm"             => $idInm,
			":codinm"            => $codinm,
			":IdInmobiliaria"    => $data['IdInmobiliaria'],
			":usu_creacion"      => $usu_creacion,
			":f_creacion"        => $fechaserv,
			":h_creacion"        => $h_creacion
            )))
        {

           //promotor 3 captador 4
    		for($i=0;$i<count($data['idpromotor']);$i++)
			{
				$this->saveTercero($data['IdInmobiliaria'],$codinm,3,$data['idpromotor'][$i]);
			}
			for($j=0;$j<count($data['codcaptador']);$j++)
			{
				$this->saveTercero($data['IdInmobiliaria'],$codinm,4,$data['codcaptador'][$j]);
			}

        	$inmueb=array();
            return $inmueb[]=array("codinmu"=>$codinm);
        }
        else
        {
            return print_r($stmt->errorInfo())." error";
        }
        $stmt = null;
    }
    public function saveLogFinancial($data,$idInm)
    {
        $connPDO = new Conexion();


        $Idinmobiliaria = $data['IdInmobiliaria'];
        $idInm          = "$Idinmobiliaria - $codinm";
        $f_sis		= date('Y-m-d');
        $usuario		= $_SESSION['iduser'];
		$h_sis     = date('Y-m-d');
		$ip             = $_SERVER['REMOTE_ADDR'];


        $stmt=$connPDO->prepare("SELECT IdGestion,FConsignacion,Estrato,AreaConstruida,AreaLote,
			EdadInmueble,NoMatricula,chip,AvaluoCatastral,CedulaCatastral,
			fingreso,restricciones,obserinterna,codinterno,FechaAvaluoCatastral,
			ValorVenta,ValorCanon,Administracion,ValorIva,admondto,
			IdProcedencia,IdDestinacion,AdmonIncluida,IdTpInm,idInm,
			codinm,usu_creacion,f_creacion,h_creacion
					from inmuebles2
					where idInm=:idInm");

        if($stmt->execute(array(
	     		":idInm"             => $idInm
            )))
        {

            while($row=$stmt->fetch())
			{
				$IdGestion_or        = $row['IdGestion'];
				$rowConsignacion_or  = $row['FConsignacion'];
				$Estrato_or          = $row['Estrato'];
				$AreaConstruida_or   = $row['AreaConstruida'];
				$AreaLote_or         = $row['AreaLote'];
				$rowrente_or         = $row['Frente'];
				$rowondo_or          = $row['Fondo'];
				$Ubicacion_or        = $row['Ubicacion'];
				$EdadInmueble_or     = $row['EdadInmueble'];
				$NoMatricula_or      = $row['NoMatricula'];
				$chip_or             = $row['chip'];
				$AvaluoCatastral_or  = $row['AvaluoCatastral'];
				$CedulaCatastral_or  = $row['CedulaCatastral'];
				$amoblado_or         = $row['amoblado'];
				if($amoblado_or      ==0)	{$amoblado_or="";}
				$restricciones_or    = $row['restricciones'];
				$obserinterna_or     = $row['obserinterna'];
				$Favaluo_or          = $row['FechaAvaluoCatastral'];
				$ValorVenta_or       = $f['ValorVenta'];
				$ValorCanon_or       = $f['ValorCanon'];
				$Administracion_or   = $f['Administracion'];
				$ValorIva_or         = $f['ValorIva'];
				$admondto_or         = $f['admondto'];
				$IdProcedencia_or    = $f['IdProcedencia'];
				$idCaptador_or       = $f['idCaptador'][0];
				$IdPromotor_or       = $f['IdPromotor'][0];
				$IdDestinacion_or    = $f['IdDestinacion'];
				$tipo_contrato_or    = $f['tipo_contrato'];
				$IdUbicacion_or      = $f['IdUbicacion'];
				$IdTpInm_or          = $f['IdTpInm'];
			}
			/////inserta en bitácora de cambios por campo
			$IdDemanda=$idInm;
			if($Favaluo_or!=$data['Favaluo'])
			{
				$cambio++;
				$nom_campo="Fecha Avaluo";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="Fecha Avaluo"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['Favaluo'],$Favaluo_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($IdGestion_or!=$data['gestioncomercial'])
			{
				$cambio++;
				$nom_campo="IdGestion";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="IdGestion"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['gestioncomercial'],$IdGestion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($FConsignacion_or!=$data['fecha_inicio'])
			{
				$cambio++;
				$nom_campo="FConsignacion";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="FConsignacion"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['fecha_inicio'],$FConsignacion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($Estrato_or!=$data['estrato'])
			{
				$cambio++;
				$nom_campo="Estrato";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="Estrato"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['estrato'],$Estrato_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($AreaConstruida_or!=str_ireplace(".","",$data['areaconstruida']))
			{
				$cambio++;
				$nom_campo="AreaConstruida";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="AreaConstruida"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,str_ireplace(".","",$data['areaconstruida']),$AreaConstruida_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($AreaLote_or!=str_ireplace(".","",$data['arealote']))
			{
				$cambio++;
				$nom_campo="AreaLote";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="AreaLote"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['arealote'],$AreaLote_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}

			if($EdadInmueble_or!=$data['anoconstruccion'])
			{
				$cambio++;
				$nom_campo="EdadInmueble";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="EdadInmueble"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['anoconstruccion'],$EdadInmueble_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
			}
			if($NoMatricula_or!=$data['nromatri'])
			{
				$cambio++;
				$nom_campo="NoMatricula";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="NoMatricula"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['nromatri'],$NoMatricula_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
			}
			if($chip_or!=$data['chip'])
			{
				$cambio++;
				$nom_campo="chip";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="chip"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['chip'],$chip_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
			}
			if($AvaluoCatastral_or!=$calledavaluocatas)
			{
				$cambio++;
				$nom_campo="AvaluoCatastral";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="AvaluoCatastral"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$calledavaluocatas,$AvaluoCatastral_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($CedulaCatastral_or!=$data['cedulacatast'])
			{
				$cambio++;
				$nom_campo="CedulaCatastral";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="CedulaCatastral"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['cedulacatast'],$CedulaCatastral_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}

			if($amoblado_or!=$data['amoblado'])
			{
				$cambio++;
				$nom_campo="Amoblado";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="Amoblado"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['amoblado'],$amoblado_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($restricciones_or!=$restricciones)
			{
				$cambio++;
				$nom_campo="restricciones";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="restricciones"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$restricciones,$restricciones_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($obserinterna_or!=$data['obserinterna'])
			{
				$cambio++;
				$nom_campo="obserinterna";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="obserinterna"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['obserinterna'],$obserinterna_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($ValorVenta_or!=str_ireplace(".","",$data['ValorVenta']))
			{
				$cambio++;
				$nom_campo="ValorVenta";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="ValorVenta"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,str_ireplace(".","",$data['ValorVenta']),$ValorVenta_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($ValorCanon_or!=str_ireplace(".","",$data['ValorCanon']))
			{
				$cambio++;
				$nom_campo="ValorCanon";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="ValorCanon"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,str_ireplace(".","",$data['ValorCanon']),$ValorCanon_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($ValorIva_or!=str_ireplace(".","",$data['ValorIva']))
			{
				$cambio++;
				$nom_campo="ValorIva";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="ValorIva"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,str_ireplace(".","",$data['ValorIva']),$ValorIva_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($Administracion_or!=str_ireplace(".","",$data['Administracion']))
			{
				$cambio++;
				$nom_campo="Administracion";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="Administracion"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,str_ireplace(".","",$data['Administracion']),$Administracion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($admondto_or!=str_ireplace(".","",$data['admondto']))
			{
				$cambio++;
				$nom_campo="admondto";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="admondto"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,str_ireplace(".","",$data['admondto']),$admondto_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}

			if($IdProcedencia_or!=$_POST['procedencia'])
			{
				$cambio++;
				$nom_campo="IdProcedencia";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="IdProcedencia"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['procedencia'],$IdProcedencia_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($idCaptador_or!=$data['codcaptador'])
			{
				$cambio++;
				$nom_campo="idCaptador";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="idCaptador"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['codcaptador'],$idCaptador_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
			}
			if($IdPromotor_or!=$data['idpromotor'])
			{
				$cambio++;
				$nom_campo="IdPromotor";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="IdPromotor"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['idpromotor'],$IdPromotor_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
			}
			if($IdDestinacion_or!=$data['destinacion'])
			{
				$cambio++;
				$nom_campo="IdDestinacion";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="IdDestinacion"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['destinacion'],$IdDestinacion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
			}
			if($IdTpInm_or!=$data['tipoinmueble'])
			{
				$cambio++;
				$nom_campo="IdTpInm";
				$cons_log=consecutivo_log(3);
				$campo_usu_g="IdTpInm"; //nombre del campo en el formulario
				$param_g=0; //codigo de la tabla par?metros
				guarda_log_gral($cons_log,3,$nom_campo,$data['tipoinmueble'],$IdTpInm_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);

			}

            return 1;
        }
        else
        {
            return print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
	public function saveTercero($IdInmobiliaria,$codinm,$tipoTer,$idTer)
    {
        $connPDO = new Conexion();

		$conse       = consecutivo("conse_tr", "inmueblesterceros");
		$usuario  	 = $_SESSION['iduser'];
		$par 		 = 1;
		$fec         = date('Y-m-d');

        $stmt=$connPDO->prepare("REPLACE INTO inmueblesterceros
		 (conse_tr,inmob_tr,idinm_tr,iduser_tr,rol_tr,part_tr,est_tr,idusu_tr,fec_tr)
			VALUES
			(:conse_tr,:inmob_tr,:idinm_tr,:iduser_tr,:rol_tr,:part_tr,:est_tr,:idusu_tr,:fec_tr)");

        if($stmt->execute(array(
			":conse_tr"  => $conse,
			":inmob_tr"  => $IdInmobiliaria,
			":idinm_tr"  => $codinm,
			":iduser_tr" => $idTer,
			":rol_tr"    => $tipoTer,
			":part_tr"   => $par,
			":est_tr"    => $par,
			":idusu_tr"  => $usuario,
			":fec_tr"    => $fec
            )))
        {

           return 1;
        }
        else
        {
            return print_r($stmt->errorInfo())." error";
        }
        $stmt = null;
    }
	public function caracObligatoriasTpInmueble($dato)
    {
        $connPDO = new Conexion();
        $data=$dato['tpInm'];
        $cadena='';
        if($data==1 || $data==2 || $data==8 || $data==10 || $data==11 || $data==12 || $data==13)
        {
        	$cadena .='<div class="form-group ">
           <div class="input-group">
            <label><b class="obligatorio">(*)</b> Area Construida</label>
            <input type="text" name="valAreaLote" class="form-control input-sm form-control-inline input-medium filthypillow " placeholder="(*) Area Construida">
           </div>
          </div>';
          $cadena .='<div class="form-group ">
           <div class="input-group">
            <label><b class="obligatorio">(*)</b> Area Privada Total</label>
            <input type="text" name="valAreaPrivada" class="form-control input-sm form-control-inline input-medium filthypillow " placeholder="(*) Area Privada Total">
           </div>
          </div>';
          $cadena .='<div class="form-group ">
           <div class="input-group">
            <label><b class="obligatorio">(*)</b> Nro Habitaciones</label>
            <input type="text" name="valNumHabitaciones" class="form-control input-sm form-control-inline input-medium filthypillow " placeholder="(*) Nro Habitaciones">
           </div>
          </div>';

          $cadena .='<div class="form-group ">
           <div class="input-group">
            <label><b class="obligatorio">(*)</b> Nro de Ba&ntilde;os</label>
            <input type="text" name="valNumBanos" class="form-control input-sm form-control-inline input-medium filthypillow " placeholder="(*) Nro de Ba&ntilde;os">
           </div>
          </div>';
          $cadena .='<div class="form-group ">
           <div class="input-group">
            <label>Nro de Parqueaderos</label>
            <input type="text" name="valNumParqueaderos" class="form-control input-sm form-control-inline input-medium filthypillow " placeholder="Nro de Parqueaderos">
           </div>
          </div>';

        }

        if($data==3 || $data==4 || $data==5 || $data==6 || $data==7)
        {
        	$cadena .='<div class="form-group ">
           <div class="input-group">
            <label><b class="obligatorio">(*)</b> Area Construida</label>
            <input type="text" name="valAreaLote" class="form-control input-sm form-control-inline input-medium filthypillow " placeholder="(*) Area Construida">
           </div>
          </div>';
          $cadena .='<div class="form-group ">
           <div class="input-group">
            <label><b class="obligatorio">(*)</b> Area Privada Total</label>
            <input type="text" name="valAreaPrivada" class="form-control input-sm form-control-inline input-medium filthypillow " placeholder="(*) Area Privada Total">
           </div>
          </div>';

          $cadena .='<div class="form-group ">
           <div class="input-group">
            <label><b class="obligatorio">(*)</b> Nro de Ba&ntilde;os</label>
            <input type="text" name="valNumBanos" class="form-control input-sm form-control-inline input-medium filthypillow " placeholder="(*) Nro de Ba&ntilde;os">
           </div>
          </div>';
          $cadena .='<div class="form-group ">
           <div class="input-group">
            <label>Nro de Parqueaderos</label>
            <input type="text" name="valNumParqueaderos" class="form-control input-sm form-control-inline input-medium filthypillow " placeholder="Nro de Parqueaderos">
           </div>
          </div>';

        }
        if($data==9)
        {
        	$cadena .='<div class="form-group ">
           <div class="input-group">
            <label><b class="obligatorio">(*)</b> Area Construida</label>
            <input type="text" name="valAreaLote" class="form-control input-sm form-control-inline input-medium filthypillow " placeholder="(*) Area Construida">
           </div>
          </div>';
          $cadena .='<div class="form-group ">
           <div class="input-group">
            <label><b class="obligatorio">(*)</b> Area Privada Total</label>
            <input type="text" name="valAreaPrivada" class="form-control input-sm form-control-inline input-medium filthypillow " placeholder="(*) Area Privada Total">
           </div>
          </div>';
        }
       return $cadena;
       $stmt = null;
    }
     public function politicasInmuebleOld($data){
        $connPDO = new Conexion();

        $idInm=$data['IdInmobiliaria']."-".$data['codinm'];

        $stmt = $connPDO->prepare("UPDATE inmuebles2
            set
            ComiVenta          = :ComiVenta,
            ComiArren          = :ComiArren,
            ValComiVenta       = :ValComiVenta,
            ValComiArr         = :ValComiArr,
            politica_comp      = :politica_comp
            WHERE codinm       = :codinm
            AND IdInmobiliaria = :IdInmobiliaria
            ");

        if ($stmt->execute(array(
                ":ComiVenta"      => $data['ComiVenta'],
                ":ComiArren"      => $data['ComiArren'],
                ":ValComiVenta"   => str_ireplace(".","",$data['ValComiVenta']),
                ":ValComiArr"     => str_ireplace(".","",$data['ValComiArr']),
                ":politica_comp"  => $data['politica_comp'],
                ":codinm"         => $data['codinm'],
                ":IdInmobiliaria" => $data['IdInmobiliaria']
            )))
        {
            $this->politicasInmuebleNew($data);
        	$this->logPoliticasInmueble($data, $idInm);
            return true;
        }else{
            print_r($stmt->errorInfo());
        }

    }

    public function politicasInmuebleNew($data){
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("UPDATE inmnvo
            set
            ComiVenta          = :ComiVenta,
            ComiArren          = :ComiArren,
            ValComiVenta       = :ValComiVenta,
            ValComiArr         = :ValComiArr,
            politica_comp      = :politica_comp
            WHERE codinm       = :codinm
            AND IdInmobiliaria = :IdInmobiliaria
            ");

        if ($stmt->execute(array(
                ":ComiVenta"      => $data['ComiVenta'],
                ":ComiArren"      => $data['ComiArren'],
                ":ValComiVenta"   => str_ireplace(".","",$data['ValComiVenta']),
                ":ValComiArr"     => str_ireplace(".","",$data['ValComiArr']),
                ":politica_comp"  => $data['politica_comp'],
                ":codinm"         => $data['codinm'],
                ":IdInmobiliaria" => $data['IdInmobiliaria']
            ))) {
            return true;
        }else{
            print_r($stmt->errorInfo());
        }
    }
    public function logPoliticasInmueble($data, $idInm){
        $f_sis=date('Y-m-d');
        $h_sis=date('H:i:s');
        $ip=$_SERVER['REMOTE_ADDR'];
        $usuario        = $_SESSION['iduser'];
        $IdDemanda=$idInm;
        $connPDO      = new Conexion;
        $stmt = $connPDO->prepare("SELECT ComiVenta,ComiArren,ValComiVenta,ValComiArr,
                        politica_comp
                        FROM inmuebles2
                        WHERE idInm=:idInm");
        $stmt->bindParam(':idInm', $idInm);

        if ($stmt->execute()) {
            while($row=$stmt->fetch())
            {
				$ComiVenta_or     = $row['ComiVenta'];
				$ComiArren_or     = $row['ComiArren'];
				$ValComiVenta_or  = $row['ValComiVenta'];
				$ValComiArr_or    = $row['ValComiArr'];
				$politica_comp_or = $row['politica_comp'];
            }
            if($ComiVenta_or!=$data['ComiVenta'])
            {
                $cambio++;
                $nom_campo="ComiVenta";
                $cons_log=consecutivo_log(3);
                $campo_usu_g="ComiVenta"; //nombre del campo en el formulario
                $param_g=0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log,3,$nom_campo,$data['ComiVenta'],$ComiVenta_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
            }
            if($ComiArren_or!=$data['ComiArren'])
            {
                $cambio++;
                $nom_campo="ComiArren";
                $cons_log=consecutivo_log(3);
                $campo_usu_g="ComiArren"; //nombre del campo en el formulario
                $param_g=0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log,3,$nom_campo,$data['ComiArren'],$ComiArren_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
            }
            if($ValComiVenta_or!=$data['ValComiVenta'])
            {
                $cambio++;
                $nom_campo="ValComiVenta";
                $cons_log=consecutivo_log(3);
                $campo_usu_g="ValComiVenta"; //nombre del campo en el formulario
                $param_g=0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log,3,$nom_campo,$data['ValComiVenta'],$ValComiVenta_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
            }
            if($ValComiArr_or!=$data['ValComiArr'])
            {
                $cambio++;
                $nom_campo="ValComiArr";
                $cons_log=consecutivo_log(3);
                $campo_usu_g="ValComiArr"; //nombre del campo en el formulario
                $param_g=0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log,3,$nom_campo,$data['ValComiArr'],$ValComiArr_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
            }
            if($politica_comp_or!=$data['politica_comp'])
            {
                $cambio++;
                $nom_campo="politica_comp";
                $cons_log=consecutivo_log(3);
                $campo_usu_g="politica_comp"; //nombre del campo en el formulario
                $param_g=0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log,3,$nom_campo,$data['politica_comp'],$politica_comp_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g,0);
            }
            return true;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function savePropietario($data)
   //  public function saveFinancialNew($data)
    {
        $connPDO = new Conexion();

        $iduser         = consecutivo('iduser','usuarios2');
        $idInm          = $data['IdInmobiliaria']."-".$data['codinm'];
        $fechaserv		= date('Y-m-d');

        if($data['tipodocu']!=2  )
		{
			$razon_social=$apellidos." ".$Nombres;
		}
		else
		{
			$razon_social=$data['razon_social'];
			$Nombres=$data['razon_social'];
		}
		$tel1="(".$data['indict1'].")".$data['telefonoprop'];
		if($data['indict2']>0)
		{
			$tel2="(".$data['indict2'].")".$data['telefonoprop2'];
		}
		else
		{
			$tel2="";
		}
		if($data['indict3']>0)
		{
			$tel3="(".$data['indict3'].")".$data['telefonoprop3'];
		}
		else
		{
			$tel3="";
		}
		if($data['paisd']!=169)
		{
			$deptod=0;
			$ciudad=0;
			$ciudadtext=$data['ciudadtext'];
		}
		else
		{
			$deptod=$data['deptod'];
			$ciudadtext=ucwords(strtolower(getCampo('ciudades_dian',"where id_ciudad=".$data['ciudadd'],'nombre_ciudad')));
		}
		$apellidos=trim($data['apellido1'])." ".trim($data['apellido2']);
		$Nombres=trim($data['nombre1'])." ".trim($data['nombre2']);

        $stmt=$connPDO->prepare("INSERT ignore INTO usuarios2
						(Nombres,apellidos,Direccion,nombre1,nombre2,
						apellido1,apellido2,razon_social,Telefono,telefono2,
						telefono3,Correo,Celular,celular2,pais_u,
						depto_u,ciudad_u,ext1,ext2,ext3,
						ciudad_utext,IdTipoDocumento,Id_Usuarios,iduser,IdInmmo)
						VALUES
						(:Nombres,:apellidos,:Direccion,:nombre1,:nombre2,
						:apellido1,:apellido2,:razon_social,:Telefono,:telefono2,
						:telefono3,:Correo,:Celular,:celular2,:pais_u,
						:depto_u,:ciudad_u,:ext1,:ext2,:ext3,
						:ciudad_utext,:IdTipoDocumento,:Id_Usuarios,:iduser,:IdInmmo)");

        if($stmt->execute(array(
			":Nombres"         => utf8_encode($Nombres),
			":apellidos"       => utf8_encode($apellidos),
			":Direccion"       => utf8_encode($data['direccionprop']),
			":nombre1"         => utf8_encode($data['nombre1']),
			":nombre2"         => utf8_encode($data['nombre2']),
			":apellido1"       => utf8_encode($data['apellido1']),
			":apellido2"       => utf8_encode($data['apellido2']),
			":razon_social"    => utf8_encode($razon_social),
			":Telefono"        => $tel1,
			":telefono2"       => $tel2,
			":telefono3"       => $tel3,
			":Correo"          => utf8_encode($data['emailpro']),
			":Celular"         => utf8_encode($data['celularprop']),
			":celular2"        => utf8_encode($data['celularprop2']),
			":pais_u"          => utf8_encode($data['paisd']),
			":depto_u"         => utf8_encode($deptod),
			":ciudad_u"        => utf8_encode($data['ciudadd']),
			":ext1"            => utf8_encode($data['ext1']),
			":ext2"            => utf8_encode($data['ext2']),
			":ext3"            => utf8_encode($data['ext3']),
			":ciudad_utext"    => utf8_encode($ciudadtext),
			":IdTipoDocumento" => $data['tipodocu'],
			":Id_Usuarios"     => $data['cedprop'],
			":iduser"          => $iduser,
            ":IdInmmo"          => $data['IdInmobiliaria']

            )))
        {
            //propietario 1 apoderado 4
            return $this->saveTercero($data['IdInmobiliaria'],$data['codinm'],1,$iduser);


        	//$inmueb=array();
            //return $inmueb[]=array("codinmu"=>$codinm);
        }
        else
        {
            return print_r($stmt->errorInfo())." error";
        }
        $stmt = null;
    }
    public function saveAdicionalesInmueble($data)
    {
        $connPDO = new Conexion();
        $usuario        = $_SESSION['Id_Usuarios'];
        $iduser_det        = $_SESSION['iduser'];
        //$cod         = consecutivo('IdDetalle','detalleinmueble2');
        $idInm          = $data['IdInmobiliaria']."-".$data['codinm'];
        $fechaserv      = date('Y-m-d');
        $data['cantidad_dt']=0;
        ///borramos los valores del inmueble antes de insertar
        $this->deleteAdicionalesInmueble($data['IdInmobiliaria'],$data['codinm']);
        foreach($data['checkCarVal'] as $key=>$value)
        {
            if($value==1)
            {
               $this->insertAdicionalesInmueble($data,$key,$data['caracteristica'][$key]);

            }
        }


         $stmt = null;
    }
    public function insertAdicionalesInmueble($data,$idcar,$desc)
    {
        $connPDO = new Conexion();
        $usuario        = $_SESSION['Id_Usuarios'];
        $iduser_det     = $_SESSION['iduser'];
        $cod            = consecutivo('IdDetalle','detalleinmueble2');
        $idInm          = $data['IdInmobiliaria']."-".$data['codinm'];
        $fechaserv      = date('Y-m-d');
        $data['cantidad_dt']=0;


       $stmt=$connPDO->prepare("INSERT  INTO detalleinmueble2
                                (IdDetalle,idinmueble,idusuario,idcaracteristica,obser_det,
                                inmob,codinmdet,cantidad_dt,fecha_dt,iduser_dt)
                                VALUES
                                (:IdDetalle,:idinmueble,:idusuario,:idcaracteristica,:obser_det,
                                :inmob,:codinmdet,:cantidad_dt,:fecha_dt,:iduser_dt)");

        if($stmt->execute(array(
            ":IdDetalle"        => $cod,
            ":idinmueble"       => $idInm,
            ":idusuario"        => $usuario,
            ":iduser_dt"        => $iduser_det,
            ":idcaracteristica" => $idcar,
            ":obser_det"        => $desc,
            ":inmob"            => $data['IdInmobiliaria'],
            ":codinmdet"        => $data['codinm'],
            ":cantidad_dt"      => $data['cantidad_dt'],
            ":fecha_dt"         => $fechaserv
            )))
        {
            return 1;
        }
        else
        {
            return print_r($stmt->errorInfo())." error";
        }
         $stmt = null;
    }
     public function deleteAdicionalesInmueble($IdInmobiliaria,$codinm)
    {
        $connPDO = new Conexion();

        $stmt=$connPDO->prepare("DELETE from  detalleinmueble2
                        WHERE   codinmdet=:codinm
                        AND inmob =:inmob");

        if($stmt->execute(array(
            ":inmob"            => $IdInmobiliaria,
            ":codinm"        => $codinm

            )))
        {

            return 1;
        }
        else
        {
            return print_r($stmt->errorInfo())." error";
        }
        $stmt = null;
    }
    public function insertLateralInmueble($AreaLote,$AreaConstruida,$alcobas,$banos,$garaje,$IdInmobiliaria,$codinm)
    {
        $connPDO = new Conexion();

        
        $stmt=$connPDO->prepare("UPDATE inmnvo 
                        set
                        banos              =:banos,
                        garaje             =:garaje,
                        alcobas            =:alcobas,
                        AreaLote           =:AreaLote,
                        AreaConstruida     =:AreaConstruida
                        WHERE codinm       =:codinm 
                        AND IdInmobiliaria =:inmob");

        if($stmt->execute(array(
            ":banos"          => $banos,
            ":garaje"         => $garaje,
            ":alcobas"        => $alcobas,
            ":AreaLote"       => $AreaLote,
            ":AreaConstruida" => $AreaConstruida,
            ":inmob"          => $IdInmobiliaria,
            ":codinm"         => $codinm
            
            )))
        {
           
            return 1;
        }
        else
        {
            return print_r($stmt->errorInfo())." error";
        }
        $stmt = null;
    }
     public function insertLateralInmuebleOld($AreaLote,$AreaConstruida,$alcobas,$banos,$garaje,$IdInmobiliaria,$codinm,$tpinm)
    {
        $connPDO = new Conexion();
        // insertamos baños, garajes y alcobas en detalleinmueble
        if($alcobas>0)
        {
            $idcar=$this->findAdicionalesInmueble($alcobas,$tpinm,'alcoba',15);
            $this->insertAdicionalesInmueble2($IdInmobiliaria,$codinm,$idcar,'');
        }
        if($banos>0)
        {
            $idcar=$this->findAdicionalesInmueble($banos,$tpinm,'baño',16);
            $this->insertAdicionalesInmueble2($IdInmobiliaria,$codinm,$idcar,'');
        }
        if($garaje>0)
        {
            $idcar=$this->findAdicionalesInmueble($garaje,$tpinm,'parqueadero',37);
            $this->insertAdicionalesInmueble2($IdInmobiliaria,$codinm,$idcar,'');
        }
        
        $stmt=$connPDO->prepare("UPDATE inmuebles2 
                        set
                        AreaLote           =:AreaLote,
                        AreaConstruida     =:AreaConstruida
                        WHERE codinm       =:codinm 
                        AND IdInmobiliaria =:inmob");

        if($stmt->execute(array(
            ":AreaLote"       => $AreaLote,
            ":AreaConstruida" => $AreaConstruida,
            ":inmob"          => $IdInmobiliaria,
            ":codinm"         => $codinm
            
            )))
        {
           
            return "banos $banos";
        }
        else
        {
            return print_r($stmt->errorInfo())." error";
        }
        $stmt = null;
    }
      public function findAdicionalesInmueble($cant,$TpInm,$label,$grupo)
    {
        $connPDO = new Conexion();
        if($grupo==37 and $cant>4)
        {
            $cant=4;
        }
        if($grupo==16 and $cant>10)
        {
            $cant=10;
        }
        if($grupo==15 and $cant>10)
        {
            $cant=10;
        }
        if($cant>0)
        {
            $cond="and Cantidad=:cant";
        }
        else
        {
            $cond="";
        }
        $valueTerm = '%'.utf8_decode($label).'%';
        $stmt=$connPDO->prepare("SELECT idCaracteristica
            from maestrodecaracteristicas
            where idGrupo=:grupo
            and Descripcion like :label
            $cond
            and idTipoInmueble=:TpInm");

        $stmt->bindParam(':grupo', $grupo);
        if($cant>0)
        {
            $stmt->bindParam(':cant', $cant);
        }
        $stmt->bindParam(':TpInm', $TpInm);
        $stmt->bindParam(':label', $valueTerm, PDO::PARAM_STR);
        if($stmt->execute())
        {
            
            while ($row = $stmt->fetch()) 
            {
                $caract = $row['idCaracteristica'];
            }
            return $caract;
            
        }
        else
        {
            return $label." --"; //print_r($stmt->errorInfo())." error  --";
        }
        $stmt = null;
    }
    public function documentalInmueble($data)
    {
        $connPDO = new Conexion();
        $usu_dcm        = $_SESSION['iduser'];
        $cod            = consecutivo('id_dcm','adjuntos_documental');
        $fechaserv      = date('Y-m-d');
        $hora      = date('H-i-s');

       $stmt=$connPDO->prepare("INSERT  INTO adjuntos_documental
                                (id_dcm,tp_doc_dcm,ruta_archivo_dcm,est_dcm,codinm_dcm,
                                 inmob_dcm,fec_dcm,hor_dcm,ip_dcm,usu_dcm,nom_dcm)
                                VALUES
                                (:id_dcm,:tp_doc_dcm,:ruta_archivo_dcm,:est_dcm,:codinm_dcm,
                                 :inmob_dcm,:fec_dcm,:hor_dcm,:ip_dcm,:usu_dcm,:nom_dcm)");

        if($stmt->execute(array(
            ":id_dcm"           => $cod,
            ":tp_doc_dcm"       => $data['tp_doc_dcm'],
            ":ruta_archivo_dcm" => $data['ruta_archivo_dcm'],
            ":est_dcm"          => $data['est_dcm'],
            ":codinm_dcm"       => $data['codinm'],
            ":inmob_dcm"        => $data['IdInmobiliaria'],
            ":fec_dcm"          => $fechaserv,
            ":hor_dcm"          => $hora,
            ":ip_dcm"           => $ip,
            ":usu_dcm"          => $usu_dcm,
            ":nom_dcm"          => $data['nom_dcm']
            )))
        {
            return 1;
        }
        else
        {
            return print_r($stmt->errorInfo())." error";
        }
         $stmt = null;
    }
    public function getCategoriasdocumental()
    {
        $connPDO = new Conexion();

        $fechaserv      = date('Y-m-d');
        $hora      = date('H-i-s');

       $stmt=$connPDO->prepare("SELECT conse_param,desc_param
                                 FROM parametros
                                 WHERE id_param=52
                                AND est_param=1");

        if($stmt->execute(
            //array( ":conse_param"       => $data['conse_param'])
            ))
        {
            $info=array();
            while($row=$stmt->fetch())
            {
                $info[]=array
                (
                    "conse" =>$row['conse_param'],
                    "desc"  =>$row['desc_param']
                );
            }
            return $info;
        }
        else
        {
            return print_r($stmt->errorInfo())." error";
        }
         $stmt = null;
    }
    public function getDatadocumentalInmueble($data)
    {
        $connPDO = new Conexion();

        $fechaserv      = date('Y-m-d');
        $hora      = date('H-i-s');

       $stmt=$connPDO->prepare("SELECT id_dcm,tp_doc_dcm,ruta_archivo_dcm,est_dcm,codinm_dcm,
                                 inmob_dcm,fec_dcm,hor_dcm,ip_dcm,usu_dcm,nom_dcm
                                 FROM adjuntos_documental
                                 WHERE inmob_dcm =:IdInmobiliaria
                                 AND codinm_dcm=:codinm
                                 AND tp_doc_dcm=:tp_doc_dcm
                                 AND est_dcm=1");

        if($stmt->execute(array(
            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":tp_doc_dcm"     => $data['tp_doc_dcm']
            )))
        {
            $info=array();
            while($row=$stmt->fetch())
            {
                $info[]=array
                (
                    'id_dcm'           =>$row['id_dcm'],
                    'nom_dcm'          =>$row['nom_dcm'],
                    'tp_doc_dcm'       =>$row['tp_doc_dcm'],
                    'ruta_archivo_dcm' =>$row['ruta_archivo_dcm'],
                );
            }
            return $info;
        }
        else
        {
            return print_r($stmt->errorInfo())." error";
        }
         $stmt = null;
    }
    public function deleteDatadocumentalInmueble($data)
    {
        $connPDO = new Conexion();

        $fechaserv      = date('Y-m-d');
        $hora      = date('H-i-s');

       $stmt=$connPDO->prepare("DELETE FROM adjuntos_documental
                                 WHERE inmob_dcm =:IdInmobiliaria
                                 AND codinm_dcm=:codinm
                                 AND tp_doc_dcm=:tp_doc_dcm
                                 AND id_dcm=:id_dcm");

        if($stmt->execute(array(
            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":tp_doc_dcm"     => $data['tp_doc_dcm'],
            ":id_dcm"         => $data['id_dcm']
            )))
        {

            return 1;
        }
        else
        {
            return print_r($stmt->errorInfo())." error";
        }
         $stmt = null;
    }
    public function updateDatadocumentalInmueble($data)
    {
        $connPDO = new Conexion();

        $fechaserv      = date('Y-m-d');
        $hora      = date('H-i-s');

       $stmt=$connPDO->prepare("UPDATE FROM adjuntos_documental
                                 SET ruta_archivo_dcm =:ruta_archivo_dcm,
                                 nom_dcm              =:nom_dcm
                                 WHERE inmob_dcm      =:IdInmobiliaria
                                 AND codinm_dcm       =:codinm
                                 AND tp_doc_dcm       =:tp_doc_dcm
                                 AND id_dcm           =:id_dcm");

        if($stmt->execute(array(
            ":codinm"           => $data['codinm'],
            ":IdInmobiliaria"   => $data['IdInmobiliaria'],
            ":tp_doc_dcm"       => $data['tp_doc_dcm'],
            ":id_dcm"           => $data['id_dcm'],
            ":ruta_archivo_dcm" => $data['ruta_archivo_dcm'],
            ":nom_dcm"          => $data['nom_dcm']
            )))
        {

            return 1;
        }
        else
        {
            return print_r($stmt->errorInfo())." error";
        }
         $stmt = null;
    }
     public function updateMfichaFotosNew($data)
    {
        $connPDO = new Conexion();


            $stmt = $connPDO->prepare("UPDATE fotos_nvo
                SET
                ficha_fto      =:ficha_fto
                where posi_ft  =:posi_ft
                and aa_fto     =:IdInmobiliaria
                and codinm_fto =:codinm");

            if ($stmt->execute(array(
                ":ficha_fto"      => $data["ficha_fto"],
                ":posi_ft"        => $data['posi_ft'],
                ":IdInmobiliaria" => $data['IdInmobiliaria'],
                ":codinm"         => $data['codinm']
            ))) {

               return 1;
            }
            else
            {
                $response[] = array("Error" => $stmt->errorInfo());
            }


        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function OrdenFotosNew($data)
    {
        $connPDO = new Conexion();
        $campo3  ="Foto".$data['final'];
        $campo   ="Foto".$data['inicial'];
        $archorig=getCampo('fotos',"where idInm='$idInm'","$campo");
        Inmuebles::updateOrdenFotosNew($data,$data['final'],100);
        Inmuebles::updateOrdenFotosNew($data,$data['inicial'],$data['final']);
        Inmuebles::updateOrdenFotosNew($data,100,$data['inicial']);
        Inmuebles::updateOrdenFotosOld($data,$archorig);
    }
    public function updateOrdenFotosNew($data,$actual,$nvo)
    {
        $connPDO = new Conexion();
        $campo="Foto".$data['inicial'];
        $campo3="Foto".$data['final'];
        $archnvo =$data['nombreF'];
        $idInm  =$data['IdInmobiliaria']."-".$data['codinm'];

       $stmt = $connPDO->prepare("UPDATE fotos_nvo
            SET
            posi_ft            =:nvoposi_ft
            where codinm       =:codinm
            and IdInmobiliaria =:IdInmobiliaria
            and posi_ft        =:posi_ft");

        if ($stmt->execute(array(
            ":posi_ft"        => $data['inicial'],
            ":nvoposi_ft"     => $data['nvoposi_ft'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":codinm"         => $data['codinm']
        ))) 
        {

           return 1;
        }
        else
        {
            $response[] = array("Error" => $stmt->errorInfo());
        }


        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function updateOrdenFotosOld($data,$arch)
    {
        $connPDO = new Conexion();
        $campo3  ="Foto".$data['final'];
        $campo   ="Foto".$data['inicial'];
        $idInm   =$data['IdInmobiliaria']."-".$data['codinm'];
        $archnvo =$data['nombreF'];

       $stmt = $connPDO->prepare("UPDATE fotos
                SET
                $campo      =:arch,
                $campo3     =:nvo
                where idInm =:idInm");

        if ($stmt->execute(array(
            ":idInm" => $idInm,
            ":arch"  => $arch,
            ":nvo"   => $archnvo
        ))) {

           return 1;
        }
        else
        {
            $response[] = array("Error" => $stmt->errorInfo());
        }


        if ($response) {
            return $response;
        }
        $stmt = null;
    }
     public function updateVideoOld($data)
    {
        $connPDO = new Conexion();


            $stmt = $connPDO->prepare("UPDATE inmuebles2
                SET
                linkvideo            =:linkvideo
                where IdInmobiliaria =:IdInmobiliaria
                and codinm           =:codinm");

            if ($stmt->execute(array(
                ":linkvideo"      => $data["linkvideo"],
                ":IdInmobiliaria" => $data['IdInmobiliaria'],
                ":codinm"         => $data['codinm']
            ))) {
                Inmuebles::linkvideoNew($data);
               return 1;
            }
            else
            {
                $response[] = array("Error" => $stmt->errorInfo());
            }


        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function updateVideoNew($data)
    {
        $connPDO = new Conexion();

        $data['cantidad_dt']=0;
        $cod         = consecutivo('IdDetalle','detalleinmueble2');
        $idInm          = $data['IdInmobiliaria']."-".$data['codinm'];
        $fechaserv      = date('Y-m-d');
        $desc='';
        $usuario        = $_SESSION['Id_Usuarios'];
        $iduser_det        = $_SESSION['iduser'];
        $idcar=$this->findAdicionalesInmueble($garaje,$data['tpinm'],'url',51);
        $stmt=$connPDO->prepare("INSERT  INTO detalleinmueble2
                                (IdDetalle,idinmueble,idusuario,idcaracteristica,obser_det,
                                inmob,codinmdet,cantidad_dt,fecha_dt,iduser_dt)
                                VALUES
                                (:IdDetalle,:idinmueble,:idusuario,:idcaracteristica,:obser_det,
                                :inmob,:codinmdet,:cantidad_dt,:fecha_dt,:iduser_dt)");

        if($stmt->execute(array(
            ":IdDetalle"        => $cod,
            ":idinmueble"       => $idInm,
            ":idusuario"        => $usuario,
            ":iduser_dt"        => $iduser_det,
            ":idcaracteristica" => $idcar,
            ":obser_det"        => $desc,
            ":inmob"            => $data['IdInmobiliaria'],
            ":codinmdet"        => $data['codinm'],
            ":cantidad_dt"      => $data['cantidad_dt'],
            ":fecha_dt"         => $fechaserv
            
            ))) 
        {
               
               return 1;
            }
            else
            {
                $response[] = array("Error" => $stmt->errorInfo());
            }


        if ($response) {
            return $response;
        }
        $stmt = null;
    }

}
