<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

@include "../funciones/connPDO.php";
class DocumentosRa
{
    public function __construct($conn = "")
    {
        $this->db = $conn;
    }

    private $id_usuario;
   
    public function actualizarEstado($idInmueble, $codAfydi)
    {
        if (!empty($idInmueble) && !empty($codAfydi)) {
            $connPDO = new Conexion();

            $stmt = $connPDO->prepare("UPDATE inmuebles SET
		                              PublicaAfydi='Publicado',
		                              RAfydi=:codAfydi
		                              WHERE idInm = :idInmueble");
            if ($stmt->execute(array(
                ":codAfydi"   => $codAfydi,
                ":idInmueble" => $idInmueble,
            ))) {
                return 1;
            } else {
                print_r($stmt->errorInfo());
            }
        } else {
            return 0;
        }
        $stmt = null;
    }

    
    
     public function getInfoEstados($data)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare('SELECT conse_estado,tp_estado,fec_estado,oper_estado,
                    concep_estado,desc_estado,deb_estado,cred_estado,saldo_estado,
                    adc_estado,adc2_estado,est_estado,nro_estado,f_corte_estado
                                 FROM estadosra
                                 WHERE f_corte_estado = :fcorte
                                 and tp_estado=2
                                 LIMIT 0,30000');

        if ($stmt->execute(array(
            ":fcorte" => $data//["f_corte_estado"]
        ))) 
        {

            if ($stmt->rowCount() > 0) 
            {

                $info = array();
                while ($row = $stmt->fetch()) 
                {

                    if (trim($row['adc2_estado']) !== "" && $this->validarEmail(trim($row['adc2_estado'])) === true) {
                        $btnsend = "<button data-toggle='tooltip' data-placement='top' title='Enviar Correo' id='" . utf8_encode(trim($row['oper_estado'])) . "' class='sendemail btn btn-success btn-xs' data-nroestado='" . utf8_encode(trim($row['oper_estado'])) . "' data-email='" . utf8_encode(trim($row['adc2_estado'])) . "' data-fecha='" . $data . "'><i class='fa fa-paper-plane-o' aria-hidden='true'></i></button>";
                        $checkbox = '<input type="checkbox" data-fecha="' . $row['f_corte_estado' ] . '" id="fechacorteactual" name="id[]" value="' . utf8_encode(trim($row['oper_estado'])) . '" >';
                    }else{
                        $btnsend = "";
                        $checkbox = "";
                    }

                    $btnviewhistorial = "<button data-toggle='tooltip' data-placement='top' title='Ver historial' class='btn btn-info btn-xs viewHistorial' data-fecha='" . $data . "' data-nroestado='" . utf8_encode(trim($row['oper_estado'])) . "'><i class='fa fa-history' aria-hidden='true'></i></button>";

                    $btnchangeclave = "<button data-toggle='tooltip' data-nroestado='" . utf8_encode(trim($row['nro_estado'])) . "' data-placement='top' title='Cambiar Clave' class='btn btn-primary btn-xs changepass'><i class='fa fa-key' aria-hidden='true'></i></button>";
                    
                    $info[] = array(
                        "tp_estado"      => $row['tp_estado'],
                        "fec_estado"     => utf8_encode(trim($row['fec_estado'])),
                        "sendemail"      => $checkbox,
                        "nro_estado"     => utf8_encode(trim($row['oper_estado'])),
                        "nombreEstado"   => utf8_encode(trim($row['concep_estado'])),
                        "docu_estado"    => number_format(trim($row['desc_estado'])),
                        "dir_estado"     => utf8_encode(trim($row['deb_estado' ]." ".$row['cred_estado'])),
                        "tel_estado"     => utf8_encode(trim($row['saldo_estado'])),
                        "mail_estado"    => utf8_encode(trim($row['adc2_estado'])), // con tp_estado=2 es correo
                        "f_corte_estado" => $row['f_corte_estado' ],
                        "linkdocumento"  => "<a data-toggle='tooltip' data-placement='top' title='Ver estado Cita' class='btn btn-primary btn-xs' target='_Blank' href='https://www.simiinmobiliarias.com/mcomercialweb/estadosRA/estado_cta.php?cod_estado=".$row['oper_estado']."&f_corte=".$row['f_corte_estado'] . "'><i class='fa fa-file-pdf-o' aria-hidden='true'></i></a> $btnsend $btnviewhistorial $btnchangeclave"
                    );
                }
                return $info;
            } 
        }
        else 
        {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }

    private function validarEmail($str){
        if (!filter_var($str, FILTER_VALIDATE_EMAIL)) 
        {
            return false;
        }
        return true;
    }

    public function getInfoEnvioEstados($data,$tp)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare('SELECT tp_l,mail_l,corte_l,flag_l,fec_l,hor_l
                                 FROM log_mail_ra
                                 WHERE corte_l = :fcorte
                                 and tp_l=:tp
                                 and nro_doc = :nro_doc
                                 LIMIT 0,2');

        if ($stmt->execute(array(
            ":fcorte" => $data['fecha'],//["f_corte_estado"]
            ":nro_doc" => $data['nroestado'],//["f_corte_estado"]
            ":tp" => $tp
        ))) 
        {

            if ($stmt->rowCount() > 0) 
            {

                $info = array();
                while ($row = $stmt->fetch()) 
                {
                    $flag_envio=($row['flag_l']=1)?'Enviado':'No Enviado';
                    $info[] = array(
                        'totalregisters' => $stmt->rowCount(),
                        "tp_estado"      => $row['mail_l'],
                        "mail_l"     => utf8_encode(trim($row['mail_l'])),
                        "flag_envio"     => utf8_encode(trim($row['flag_envio'])),
                        "fec_l"   => utf8_encode(trim($row['fec_l'])),
                        "hor_l"    => trim($row['hor_l'])
                    );
                }
                return $info;
            }
            else
            {
                $info[] = array('totalregisters' => $stmt->rowCount());
                return $info;
            } 
        }
        else 
        {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function getInfoFacturasArrendatario($data)
    {
        $connPDO = new Conexion();

        $fbase=$data;
        $valueTerm = utf8_decode($fbase.'%');
        $stmt    = $connPDO->prepare('SELECT tp_estado,fec_estado,oper_estado,sop_estado,nit_estado,
                    concep_estado,desc_estado,deb_estado,cred_estado,nro_estado,anio,nit,con3,con4
                                 FROM facturasra
                                 WHERE anio like :fcorte
                                 and tp_estado=2
                                 LIMIT 0,30000');

        $stmt->bindParam(':fcorte', $valueTerm, PDO::PARAM_STR);
        if ($stmt->execute()) 
        {
        
            if ($stmt->rowCount() > 0) 
            {

                $info = array();
                while ($row = $stmt->fetch()) 
                {
                    if (trim($row['con4']) !== "" && $this->validarEmail(trim($row['con4'])) === true) {
                        $btnsend = "<button data-toggle='tooltip' data-placement='top' title='Enviar Correo' id='" . utf8_encode(trim($row['oper_estado'])) . "' class='sendemail btn btn-success btn-xs' data-nroestado='" . utf8_encode(trim($row['oper_estado'])) . "' data-email='" . utf8_encode(trim($row['con4'])) . "' data-fecha='" . $data . "'><i class='fa fa-paper-plane-o' aria-hidden='true'></i></button>";
                        $checkbox = '<input type="checkbox" data-fecha="' . $row['fec_estado' ] . '" id="fechacorteactual" name="id[]" value="' . utf8_encode(trim($row['oper_estado'])) . '" >';
                    }
                    else
                    {
                        $btnsend = "";
                        $checkbox = "";
                    }

                    // $btnviewhistorial = "<button data-toggle='tooltip' data-placement='top' title='Ver historial' class='btn btn-info btn-xs viewHistorial' data-fecha='" . $data . "' data-nroestado='" . utf8_encode(trim($row['oper_estado'])) . "'><i class='fa fa-history' aria-hidden='true'></i></button>";

                    $btnchangeclave = "<button data-toggle='tooltip' data-nroestado='" . utf8_encode(trim($row['nit'])) . "' data-placement='top' title='Cambiar Clave' class='btn btn-primary btn-xs changepass'><i class='fa fa-key' aria-hidden='true'></i></button>";
                    
                    $info[] = array(
                        "tp_estado"      => $row['tp_estado'],
                        "fec_estado"     => utf8_encode(trim($row['fec_estado'])),
                        "sendemail"      => $checkbox,
                        "nro_estado"     => utf8_encode(trim($row['oper_estado']))." ".$fbase,
                        "nombreEstado"   => utf8_encode(trim($row['sop_estado'])),
                        "docu_estado"    => trim($row['nit']),
                        "dir_estado"     => utf8_encode(trim($row['cred_estado' ])),
                        "tel_estado"     => utf8_encode(trim($row['con3'])),
                        "mail_estado"    => utf8_encode(trim($row['con4'])), // con tp_estado=2 es correo
                        "f_corte_estado" => $row['fec_estado' ],
                        "linkdocumento"  => "<a data-toggle='tooltip' data-placement='top' title='Ver Factura' class='btn btn-primary btn-xs' target='_Blank' href='https://www.simiinmobiliarias.com/mcomercialweb/estadosRA/facturaRA.php?cod_estado=".$row['oper_estado']."&ano=".$row['fec_estado'] ."&f_corte=".$row['fec_estado'] . "'><i class='fa fa-file-pdf-o' aria-hidden='true'></i></a> $btnsend $btnchangeclave"
                    );
                }
                return $info;
            } 
        }
        else 
        {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function getInfoFacturasPropietario($data)
    {
        $connPDO = new Conexion();
        $fbase=$data;
        $valueTerm = utf8_decode($fbase.'%');

         
        $stmt    = $connPDO->prepare('SELECT tp_estado,fec_estado,oper_estado,sop_estado,nit_estado,
                    concep_estado,desc_estado,deb_estado,cred_estado,nro_estado,anio,nit,con3,con4
                                 FROM facturasraprop
                                 WHERE anio like :fcorte
                                 and tp_estado=2
                                 LIMIT 0,30000');

        $stmt->bindParam(':fcorte', $valueTerm, PDO::PARAM_STR);
        if ($stmt->execute()) 
        {

            if ($stmt->rowCount() > 0) 
            {

                $info = array();
                while ($row = $stmt->fetch()) 
                {

                    if (trim($row['con4']) !== "" && $this->validarEmail(trim($row['con4'])) === true) {
                        $btnsend = "<button data-toggle='tooltip' data-placement='top' title='Enviar Correo' id='" . utf8_encode(trim($row['oper_estado'])) . "' class='sendemail btn btn-success btn-xs' data-nroestado='" . utf8_encode(trim($row['oper_estado'])) . "' data-email='" . utf8_encode(trim($row['con4'])) . "' data-fecha='" . $data . "'><i class='fa fa-paper-plane-o' aria-hidden='true'></i></button>";
                        $checkbox = '<input type="checkbox" data-fecha="' . $row['fec_estado' ] . '" id="fechacorteactual" name="id[]" value="' . utf8_encode(trim($row['oper_estado'])) . '" >';
                    }else{
                        $btnsend = "";
                        $checkbox = "";
                    }

                    // $btnviewhistorial = "<button data-toggle='tooltip' data-placement='top' title='Ver historial' class='btn btn-info btn-xs viewHistorial' data-fecha='" . $data . "' data-nroestado='" . utf8_encode(trim($row['oper_estado'])) . "'><i class='fa fa-history' aria-hidden='true'></i></button>";

                    $btnchangeclave = "<button data-toggle='tooltip' data-nroestado='" . utf8_encode(trim($row['nit'])) . "'  data-placement='top' title='Cambiar Clave' class='btn btn-primary btn-xs changepass'><i class='fa fa-key' aria-hidden='true'></i></button>";
                    
                    $info[] = array(
                        "tp_estado"      => $row['tp_estado'],
                        "fec_estado"     => utf8_encode(trim($row['fec_estado'])),
                        "sendemail"      => $checkbox,
                        "nro_estado"     => utf8_encode(trim($row['oper_estado'])),
                        "nombreEstado"   => utf8_encode(trim($row['sop_estado'])),
                        "docu_estado"    => trim($row['nit']),
                        "dir_estado"     => utf8_encode(trim($row['cred_estado' ])),
                        "tel_estado"     => utf8_encode(trim($row['con3'])),
                        "mail_estado"    => utf8_encode(trim($row['con4'])), // con tp_estado=2 es correo
                        "f_corte_estado" => $row['fec_estado' ],
                        "linkdocumento"  => "<a data-toggle='tooltip' data-placement='top' title='Ver Factura' class='btn btn-primary btn-xs' target='_Blank' href='https://www.simiinmobiliarias.com/mcomercialweb/estadosRA/facturaRAProp.php?cod_estado=".$row['oper_estado']."&ano=".$row['fec_estado'] ."&f_corte=".$row['fec_estado'] . "'><i class='fa fa-file-pdf-o' aria-hidden='true'></i></a> $btnsend $btnchangeclave"
                    );
                }
                return $info;
            } 
        }
        else 
        {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function getInfoRecibos($data)
    {
        $connPDO = new Conexion();
        $fbase=substr($data,0,5);
        $stmt    = $connPDO->prepare('SELECT conse_rc,id_rc,tp_rc,anio_rc,concep_rc,
                    sop_rc,nit_rc,desc_rc,deb_rc,cred_rc,
                    tot_rc,mail_rc,dir_rc,con_rc
                                 FROM recibosra
                                 WHERE anio_rc between :fcorte1 and :fcorte2
                                 and tp_rc=2
                                 LIMIT 0,30000');

        if ($stmt->execute(array(
            ":fcorte1" => $fbase.'01',
            ":fcorte2" => $fbase.'31'
        ))) 
        {

            if ($stmt->rowCount() > 0) 
            {

                $info = array();
                while ($row = $stmt->fetch()) 
                {
                    if (trim($row['mail_rc']) !== "" && $this->validarEmail(trim($row['mail_rc'])) === true) {
                        $btnsend = "<button data-toggle='tooltip' data-placement='top' title='Enviar Correo' id='" . utf8_encode(trim($row['conse_rc'])) . "' class='sendemail btn btn-success btn-xs' data-nroestado='" . utf8_encode(trim($row['conse_rc'])) . "' data-email='" . utf8_encode(trim($row['mail_rc'])) . "' data-fecha='" . $data . "'><i class='fa fa-paper-plane-o' aria-hidden='true'></i></button>";
                        $checkbox = '<input type="checkbox" data-fecha="' . $row['f_corte_estado' ] . '" id="fechacorteactual" name="id[]" value="' . utf8_encode(trim($row['conse_rc'])) . '" >';
                    }else{
                        $btnsend = "";
                        $checkbox = "";
                    }

                    $btnchangeclave = "<button data-toggle='tooltip' data-placement='top' title='Cambiar Clave' class='btn btn-primary btn-xs changepass'><i class='fa fa-key' aria-hidden='true'></i></button>";
                    
                    $info[] = array(
                        "sendemail"      => $checkbox,
                        "tp_estado"      => $row['tp_rc'],
                        "fec_estado"     => utf8_encode(trim($row['anio_rc'])),
                        "nro_estado"     => utf8_encode(trim($row['id_rc'])),
                        "nombreEstado"   => utf8_encode(trim($row['sop_rc'])),
                        "docu_estado"    => number_format(trim($row['nit_rc'])),
                        "dir_estado"     => utf8_encode(trim($row['concep_rc' ])),
                        "tel_estado"     => utf8_encode(trim($row['con_rc'])),
                        "mail_estado"    => utf8_encode(trim($row['mail_rc'])), // con tp_estado=2 es correo
                        "f_corte_estado" => $row['anio_rc' ],
                        "linkdocumento"  => "<a data-toggle='tooltip' data-placement='top' title='Ver Recibo' class='btn btn-primary btn-xs' target='_Blank' href='https://www.simiinmobiliarias.com/mcomercialweb/estadosRA/reciboRApdf.php?cod_estado=".$row['id_rc']."&ano=".$row['anio_rc'] . "&f_corte=" . $row['anio_rc'] . "'><i class='fa fa-file-pdf-o' aria-hidden='true'></i></a> $btnsend $btnchangeclave",
                    );
                }
                return $info;
            } 
        }
        else 
        {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function updatePass($data)
    {
        $connPDO=new Conexion();

        
        $f_sis          = date('Y-m-d');
        $h_sis          = date('H-i-s');
        $ip             = $_SERVER['REMOTE_ADDR'];  
        
        if($data['clave_tra']==$data['conf'])
        {
            $conse           = $this->validaTerceroRA($data['doc_tra']);
            if($conse!=0)
            {
                $conse_dtp       = $conse;
            }
            else
            {
                $conse_dtp       = consecutivo("cons_tra", "terceros_rafaelA");
            }
            $stmt=$connPDO->prepare("REPLACE INTO terceros_rafaelA 
             (cons_tra,doc_tra,rol_tra,est_tra,clave_tra)
                VALUES(:cons_tra,:doc_tra,:rol_tra,1,:clave_tra)");
            
           
            $stmt->bindParam(":cons_tra",$conse_dtp);
            $stmt->bindParam(":doc_tra",$data['doc_tra']);
            $stmt->bindParam(":rol_tra",$data['rol_tra']);
            $stmt->bindParam(":clave_tra",$data['clave_tra']);
            if($stmt->execute())
            {
                return array('status' => 'Ok', 'msg' => 'Contraseña Cambiada exitosamente!');

            }
            else
            {
                return array(
                    'staus' => 'Error',
                    'msg' => print_r($stmt->errorInfo())
                );
                
            }
        }
        else
        {
            return array(
                'status' => 'Error',
                'msg' => 'Las claves digitadas no son iguales'
            );
        }
        
        
        $stmt=NULL;
    } 
    public function validaTerceroRA($data)
    {
        $connPDO=new Conexion();

        
        $f_sis          = date('Y-m-d');
        $h_sis          = date('H-i-s');
        $ip             = $_SERVER['REMOTE_ADDR'];  
        if($data['clave_tra']==$data['conf'])
        {
        
           
            $stmt=$connPDO->prepare("SELECT cons_tra  
                from terceros_rafaelA 
                where doc_tra=:doc_tra
                and rol_tra=1");
            
           
           
            $stmt->bindParam(":doc_tra",$data);
          
            if($stmt->execute())
            {
                while($row=$stmt->fetch())
                {
                    $info=$row['cons_tra'];
                }
                return $info;

            }
            else
            {
                return 0;
            }
        }
        else
        {
            return array(
                'status' => 'Error',
                'msg' => 'Las claves digitadas no son iguales'
            );
        }
        
        
        $stmt=NULL;
    } 
  
}
