<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '0');


class Documentos
{
    public function __construct($connsr, $connPDO){
		$this->db=$connsr;
		$this->connPDO = $connPDO;
	}
    
     
	public function respuesta($url,$inmo,$ter,$fac,$est,$barr,$mes,$ano,$ctrl,$envmail='')
	{
		//echo "hola respuesta";
		header("Location:../base/".$url."?inmo=".$inmo."&mes=".$mes."&ano=".$ano."&ctrl=".$ctrl."&envmail=".$envmail);
	}
	public function claveMaestra($claveM,$inmo)
	{
		global $connsr;
		$consulta="select inmo_cm,clave_cm
        from clavesmaestras
        where inmo_cm='$inmo'
		and clave_cm='$claveM'";
		//echo $consulta;
		$res=mysqli_query($connsr,$consulta);
		while($f=mysqli_fetch_array($res))
		{
			$clavemaestra = $f['clave_cm'];
			if($claveM==$clavemaestra)
			{
				return 1;
			}
			else
			{
				return 2;
			}
		}
	}
	public function actualizaClave($claveu,$claveu2,$inmo,$doc,$tp_terc)
	{
		global $connsr;
		$exito=0;
		if ($tp_terc == 1) {
			$claveold = $this->getCampo('proclave', ' WHERE nit=' . $doc . ' and inmo=' . $inmo, 'contrasena');
		}elseif($tp_terc == 2){
			$claveold = $this->getCampo('arreclave', ' WHERE nit=' . $doc . ' and inmo=' . $inmo, 'contrasena');
		}

		$currentdate = date('Y-m-d');

		$currenthour = date('H:i:s');

		$currentip = $_SERVER['REMOTE_ADDR'];

		$iduser = $_SESSION['iduser'];

		$sql = "INSERT INTO  log_cambio_clave (inmob_bps,doc_bps,tp_terc_bps,inicial_bps,final_bps,fec_bps,hor_bps,ip_bps,usr_bps) VALUES ('$inmo','$doc','$tp_terc','$claveold','$claveu','$currentdate','$currenthour', '$currentip', '$iduser')";

		if ($res = mysqli_query($connsr, $sql)) {
			$exito++;
		}


		$consulta="update arreclave
        SET contrasena='$claveu'
		where nit='$doc'
		and inmo='$inmo'";
		//echo $consulta;
		if($res=mysqli_query($connsr,$consulta))
		{
			$exito++;
		}
		
		$consulta1="update proclave
        SET contrasena='$claveu'
		where nit='$doc'
		and inmo='$inmo'";
		//echo $consulta1;
		if($res1=mysqli_query($connsr,$consulta1))
		{
			$exito++;
		}
		
		$consulta2="update arrendatario
        SET contrasena='$claveu'
		where nit='$doc'
		and inmo='$inmo'";
		//echo $consulta2;
		if($res2=mysqli_query($connsr,$consulta2))
		{
			$exito++;
		}
		$consulta3="update propietario
        SET contrasena='$claveu'
		where nit='$doc'
		and inmo='$inmo'";
		//echo $consulta3;
		if($res3=mysqli_query($connsr,$consulta3))
		{
			$exito++;
		}
		//echo $exito."---<br>";
		if($exito>1)
		{
			return 1;
		}
		else
		{
			return 2;
		}
	}
	/*public function verificaCargaFac($inmo,$tp)
	{
		global $connsr;
		$exito=0;
		$consulta="select 
        from control_cargas
		where nit='$doc'
		and inmo='$inmo'";
		//echo $consulta;
		$res=mysqli_query($connsr,$consulta);
		if(mysqli_num_rows($res)>0)
		{
			$exito++;
		}
		
		
		//echo $exito."---<br>";
		if($exito>1)
		{
			return 1;
		}
		else
		{
			return 2;
		}
	}*/
	private function getCampo($tabla, $parametro, $campoGet, $mostarq = '')
    {
        $connPDO = $this->connPDO;

        $sql = "SELECT $campoGet FROM $tabla " . $parametro;
        //         echo "<br>".$sql."<br>";
        if ($mostarq == 1) {
            echo "<br>" . $sql . "<br>";
        }

        $stmt = $connPDO->prepare($sql);
        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            return $result["$campoGet"];
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
    }
}
?>