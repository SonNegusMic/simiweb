<?php 
	/**
	* 
	*/
	class obtenerContactos
	{
		
		function __construct($connPDO)
		{
			$this->connPDO = $connPDO;
		}

		public function obtenerContactos(){

			$sql = "SELECT usuariom2,clavem2,prefijom2,servidor,letra FROM usuariosm2 WHERE inmogm2 = :inmo LIMIT 0,1";

			$inm = '503';

			$stmt = $this->connPDO->prepare($sql);

			$stmt->bindParam(':inmo', $inm);

			if ($stmt->execute()) {
				while ($row = $stmt->fetch()) {
					$paramsuser = array(
						'usuario' => $row['usuariom2'],
						'clave' => $row['clavem2'],
						'letra' => $row['letra'],
						'prefijo' => $row['prefijom2'],
						'servidor' => $row['servidor']
						);
				}
				// return $paramsuser;
			} else {
	            return print_r($stmt->errorInfo());
	        }


			$url = "http://www.metrocuadrado.com/axis/GeocodeWS.jws?method=obtenerContactos&op1=" . $paramsuser['usuario'] . "&op2=" . $paramsuser['clave'] . "&op3=639-M1378";
			$response=file_get_contents($url);
		 	$doc = new DOMDocument();
		    $doc->loadXML($response);
		    $explode= $doc->getElementsByTagName('obtenerContactosResponse')->item(0)->nodeValue;

		    $registros=explode(";",$explode);
		    unset($registros[0]);
		    $registers = array();
		    foreach($registros as $key => $value){
		    	if (empty($value)) {
		    		unset($registros[$key]);
		    	}else{
		    		$params = explode(",",$value);
			    	$registers[] = array(
			    		'FechaRegistro' => $params[1],
			    		'Nombre' => $params[0],
			    		'Telefono' => $params[3],
			    		'Email' => $params[4],
			    		'Ciudad' => $params[5],
			    		'Comentario' => $params[6],
			    		);
		    	}
		    }

		    return $registers;
		}
	}
?>