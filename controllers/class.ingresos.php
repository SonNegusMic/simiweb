<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include("../funciones/connPDO.php");

class Ingresos_Simi
{
    public function __construct($conn=""){
		$this->db=$conn;
	}

	public function dateEmpresasIngre($date)
	{
		try {
					$connPDO=new Conexion();

					$anio =$date['anio'];
					$mes=$date['mes'];
					$date1  = $anio."-".$mes."-01";
					$date2  = $anio."-".$mes."-30";
					$stmt=$connPDO->prepare("SELECT c.id_inmobiliaria, i.NombreInm 
									   FROM inmobiliaria i
									   inner join control_ingresos c ON c.id_inmobiliaria=i.IdInmobiliaria
									   WHERE  origen=1
									   #AND c.id_inmobiliaria=10
									   AND c.fecha BETWEEN :date1 AND :date2
									   group by (c.id_inmobiliaria)
									   order by i.IdInmobiliaria ASC");

					
					if($stmt->execute(array(
							":date1"=>"$date1",
							
							":date2"=>"$date2",
							
						)))
						{
						$data = array();
						while ($row = $stmt->fetch()) {
							$columna=$row["NombreInm"];
							$id=$row["id_inmobiliaria"];
							$data[]=array(
								"id"=>$id,
								"Nombre"=>$columna
							);
						}
						return $data;

					}
					else
					{
						 echo "\nPDO::errorInfo():\n";
						print_r($stmt->errorInfo());
					}
		} catch (Exception $e) {
			return "some fail-messages";
		}
		

		$stmt = null;
	}
	public function dateEmpresasresult()
	{
		try {
					$dia 		= date("d");
					$mes_sis 	= date("m");
					$mes_sisa 	= date("m")-1;
					$ano_sis 	= date("Y");
					$ano_ant	= $ano_sis-1;
					$connPDO=new Conexion();

					// $anio =$date['anio'];
					// $mes=$date['mes'];
					// $date1  = $anio."-".$mes."-01";
					// $date2  = $anio."-".$mes."-30";

					echo $dateAnt=$ano_sis."-01-01</br>";
					echo $dateSis=$ano_sis."-".$mes_sis."-31";
					$stmt=$connPDO->prepare("SELECT IdInmobiliaria,NombreInm,f_creacion
												from inmobiliaria,control_ingresos
												where IdInmobiliaria=id_inmobiliaria
												and fecha between :date1 AND  :date2
												group by id_inmobiliaria
												#limit 0,10");

					
					if($stmt->execute(array(
							":date1"=>"$$dateAnt",
							
							":date2"=>"$dateSis",
							
						)))
						{
						$data = array();
						while ($row = $stmt->fetch()) {
							$columna=$row["NombreInm"];
							$id=$row["id_inmobiliaria"];
							$data[]=array(
								"id"=>$id,
								"Nombre"=>$columna
							);
						}
						return $data;

					}
					else
					{
						 echo "\nPDO::errorInfo():\n";
						print_r($stmt->errorInfo());
					}
		} catch (Exception $e) {
			return "some fail-messages";
		}
		
		$stmt = null;

	}
	public function Seguimiento($data)
	{
		$connPDO = new Conexion();
		$comentario=$data["comentario"];
		$idInmo=$data["idInmo"];
		$resultado=$data["resultado"];
		$hoy = date("Y-m-d");

		$stmt=$connPDO->prepare("INSERT INTO seg_visitasSimi (Id_Inmobiliaria,
											  fecha_seg,
											  comentario,
											  resultado)
								VALUES        (:IdInmobiliaria,
											  :fecha_seg,
											  :comentario,
											  :resultado)");
		if($stmt->execute(array(
			":IdInmobiliaria"=>$idInmo,
			":fecha_seg"=>"$hoy",
			":comentario"=>"$comentario",
			":resultado"=>"$resultado",
			))){
			return 1;
		}
		else
		{
			return 0;
			echo "\nPDO::errorInfo():\n";
			print_r($stmt->errorInfo());
		}
		$stmt = null;

	}
	public function SegCitaVisitas($data)
	{
		$connPDO=new Conexion();
		$hoy = date("Y-m-d");
		$horactual=date("H:i:s");
		$fechaCita=$data["fechaCita"];
		$horaCita=$data["horaCita"];
		$comentario="Comentario para el inmueble=".$data["idInmo"].": ".$data["comentario"];
		$evento=$data["asunto-agenda"];
		$usuario=$_SESSION["Login"];

		$stmt=$connPDO->prepare("INSERT INTO cita (id_cita,
													id_asunto,
													fechaprog,
													fechacita,
													horaprog,
													horacita,
													qregistra,
													qasigna,
													observaciones
													) 
											VALUES (NULL,
													:asuntoAgenda,
													:fecha_actual,
													:fechacita,
													:horactual,
													:horacita,
													:user_regis,
													:user_regis,
													:comentario)");
		if($stmt->execute(array(
						":asuntoAgenda"=>"$evento",
						":fecha_actual"=> "$hoy",
						":fechacita"=>"$fechaCita",
						":horactual"=>"$horactual",
						":horacita"=>"$horaCita",
						":user_regis"=>"$usuario",
						":user_regis"=>"$usuario",
						":comentario"=>"$comentario")))
		{
			return 1;
		}
		else
		{
			return 0;
			// echo "\nPDO::errorInfo():\n";
			// print_r($stmt->errorInfo());
			
		}
		$stmt = null;

		
	}
	public function obtenerSeguiVisitas($data)
	{
		$connPDO=new Conexion();
		$idInm=$data["idInm"];
		$stmt=$connPDO->prepare("SELECT 
								  se.*,
								  i.NombreInm,
								  r.desc_param AS nomParam
								FROM
								  seg_visitasSimi se 
								INNER JOIN inmobiliaria i 
								    ON i.IdInmobiliaria = se.Id_Inmobiliaria 
								INNER JOIN parametros r ON r.conse_param =se.resultado
								WHERE Id_Inmobiliaria = :idinmo AND  id_param = 41");
		
		$stmt->bindParam(":idinmo",$idInm);
		if($stmt->execute())
		{
			$data=array();
			while($row=$stmt->fetch())
			{
				$data[]=array(
					"idSegui"=>$row["id_seg"],
					"IdInmobiliaria"=>$row["Id_Inmobiliaria"],
					"NombreInm"=>$row["NombreInm"],
					"fechaSeg"=>$row["fecha_seg"],
					"Comentario"=>$row["comentario"],
					"resultado"=>$row["nomParam"],
					);
			}
			return $data;
		}
		else
		{
			return  0;
		}
		$stmt = null;
	}
}