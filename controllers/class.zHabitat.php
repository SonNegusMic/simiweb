<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

@include "../funciones/connPDO.php";

class zHabitat
{
    public function __construct($conn = "")
    {
        $this->db = $conn;
    }

    private $id_usuario;

    public function infoInmobiliaria($codInmo)
    {
        //echo $codInmu."ff";

        if (($consulta = $this->db->prepare(" select
        IdInmobiliaria,Nombre,Direccion,Correo, Telefonos
        from clientessimi
        where IdInmobiliaria=?
        "))
        ) {
            $consulta->bind_param("i", $codInmo);

            $consulta->execute();
//              echo "$qry";

            if ($consulta->bind_result($IdInmobiliaria, $Nombre, $Direccion, $Correo, $Telefonos)) {
                while ($consulta->fetch()) {
                    $arreglo[] = array(
                        "IdInmobiliaria" => $IdInmobiliaria,
                        "Nombre"         => ucwords(strtolower($Nombre)),
                        "Direccion"      => $Direccion,
                        "Correo"         => $Correo,
                        "Telefonos"      => $Telefonos,
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            echo "error --" . $conn->error;
        }

    }

    public function infoInmueble($codInmu)
    {

        $connPDO   = new Conexion();
        $fechaserv = date('Y-m-d');

        $stmt = $connPDO->prepare("SELECT d.IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
        Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
        Barrio,Gestion,AreaConstruida,AreaLote,latitud,Zona,IdCiudad,
        longitud,EdadInmueble,NombreInmo,logo,IdBarrios,NombreProm,IdPromotor,Video,Direccion,amoblado
        from datos_call d
        where Codigo_Inmueble=?");
        $stmt->bindParam(1, $codInmu, PDO::PARAM_STR);

        $arregloFotos = $this->fotosInmueble($codInmu);

        if ($stmt->execute()) {
            $precio = 0;
            while ($row = $stmt->fetch()) {
                $Codigo_Inmueble   = $row['Codigo_Inmueble'];
                list($aa, $codinm) = explode("-", $Codigo_Inmueble);
                $IdInmobiliaria    = $row['IdInmobiliaria'];
                $ciusimi           = getCampo('ciudad', "where IdCiudad=" . $row['IdCiudad'], 'Nombre');
                $codinmoZh         = getCampo('clientessimi', "where IdInmobiliaria=$IdInmobiliaria", 'codzh');
                $Administracion    = $row['Administracion'];
                $IdGestion         = $row['IdGestion'];
                $Estrato           = ($row['Estrato'] > 0) ? ($row['Estrato'] - 1) : 0;
                $IdTpInm           = $row['IdTpInm'];

                $Tipo_InmuebleCC   = getCampo('tipoinmuebles', "where idTipoInmueble=" . $row['IdTpInm'], 'tipozh', 0);
                $Venta             = $row['Venta'];
                $Canon             = $row['Canon'];
                $descripcionlarga  = $row['descripcionlarga'];
                $Barrio            = $row['Barrio'];
                $Gestion           = getCampo('gestioncomer', "where IdGestion=" . $row['IdGestion'], 'geszh');
                $AreaConstruida    = $row['AreaConstruida'];
                $AreaLote          = $row['AreaLote'];
                $IdBarrios         = $row['IdBarrios'];
                $Codigo_Inmueble   = $row['Codigo_Inmueble'];
                $latitud           = $row['latitud'];
                $longitud          = $row['longitud'];
                $Zona              = $row['Zona'];
                $idlocalidad       = getCampo('barrios', "where IdBarrios=" . $row['IdBarrios'], 'IdLocalidad');
                $iddepto           = ($row['IdCiudad'] == 11001) ? 1 : getCampo('ciudad', "where IdCiudad=" . $row['IdCiudad'], 'IdDepartamento');
                $depto             = getCampo('departamento', "where IdDepartamento=" . $iddepto, 'convzh');
                $ciu               = getCampo('ciudad', "where IdCiudad=" . $row['IdCiudad'], 'conciuvzh');
                $localidadz        = getCampo('localidad', "where idLocalidad=" . $idlocalidad, 'convlzh');
                $zona              = getCampo('zonas', "where IdZona=" . $idzona, 'NombreZ');
                $alcobas           = trae_carac1(15, $Codigo_Inmueble, 'Cantidad', 'alcoba', 0);
                $Barrioz           = getCampo('barrios', "where IdBarrios=" . $row['IdBarrios'], 'convbzh');
                $precio            = ($IdGestion == 1) ? $Canon : $Venta;
                if($localidadz=='')
                {
                    $localidadz=0;
                }
                if($Barrioz=='')
                {
                    $Barrioz=0;
                }
                list($aa, $codinm) = explode("-", $Codigo_Inmueble);
//
                $error    = 0;
                $msgerror = array();

                $dependencia       = $this->getDataDetalleInmueble($codinm, $IdInmobiliaria, 1);
                $ZonaComunes       = $this->getDataDetalleInmueble($codinm, $IdInmobiliaria, 2);
                $DescripcionFisica = $this->getDataDetalleInmueble($codinm, $IdInmobiliaria, 3);
                $SitiosCercanos    = $this->getDataDetalleInmueble($codinm, $IdInmobiliaria, 4);
                // if (empty($AreaLote) || $AreaLote == '0') {
                //     $error++;
                //     array_push($msgerror, 'Debe ingresar un area del lote.');
                // }
                if (empty($Gestion) || $Gestion == '0') {
                    $error++;
                    array_push($msgerror, 'Debe ingresar un tipo de gestion.');
                }
                if (empty($Tipo_InmuebleCC) || $Tipo_InmuebleCC == '0') {
                    $error++;
                    array_push($msgerror, 'Debe ingresar un tipo de Inmueble.');
                }
                if (empty($precio) || $precio == '0') {
                    $error++;
                    array_push($msgerror, 'Debe ingresar un precio al inmueble.');
                }
                if (count($arregloFotos) < 2) {
                    // $error++;
                    // array_push($msgerror, 'Debe ingresar minimo 2 fotos del inmueble.');
                }
                
                // if (in_array($Tipo_InmuebleCC, $inmalcobas)) {
                //     if (empty($alcobas) || $alcobas == '0') {
                //         $error++;
                //         array_push($msgerror, 'Debe ingresar alcobas al inmueble.');
                //     }
                // }
                // if (in_array($Tipo_InmuebleCC, $inmbanios)) {
                //     if (empty($banos) || $banos == '0') {
                //         $error++;
                //         array_push($msgerror, 'Debe ingresar baños al inmueble.');
                //     }
                // }
                $geszh               = getCampo('gestioncomer', "where IdGestion=" . $row['IdGestion'], 'geszh');
                $tipozh              = getCampo('tipoinmuebles', "where IdTipoInmueble=" . $row['IdTpInm'], 'tipozh');
                $inm_telefono        = getCampo('clientessimi', "where idInmobiliaria=" . $row['IdInmobiliaria'], 'Telefonos');
                $inmobiliaria_correo = getCampo('clientessimi', "where idInmobiliaria=" . $row['IdInmobiliaria'], 'Correo');
                $logoinm             = getCampo('clientessimi', "where idInmobiliaria=" . $row['IdInmobiliaria'], 'logo');
                $logoinm             = str_replace('../', '', $logoinm);
                $logoinm             = "https://www.simiinmobiliarias.com/" . $logoinm;
                $paginaweb           = getCampo('clientessimi', "where idInmobiliaria=" . $row['IdInmobiliaria'], 'Web');
                // $ciu=167;
                // $depto=5;;
                //$Barrioz=14;
                
                $czh                      = getCampo('inmuebles', "where IdInmobiliaria=$IdInmobiliaria and codinm=$codinm", 'RZhabitat');
                $codpubZh                 = (strlen($czh) > 2) ? $czh : $aa . $codinm;
                $datos_inmueble_entrada[] = array(

                    'id'                  => $row['IdInmobiliaria'],
                    'codigo'              => $codpubZh,
                    'orden_compra'        => '(NULL)',
                    'idTipoU'             => '2',
                    'idUsuario'           => $codinmoZh,
                    'idEstado'            => '2',
                    'categoria'           => $geszh,
                    'idITipo'             => $tipozh,
                    'idPais'              => '169',
                    'idDpto'              => $depto,
                    'paramDesc'           => 2,
                    'idCiudad'            => $ciu,
                    'idLocalidad'         => $localidadz,
                    'idBarrio'            => $Barrioz,
                   
                    'titulo'              => utf8_encode($row['Tipo_Inmueble'] . " EN " . $row['Gestion'] . " " . $Barrio . " " . $ciusimi),
                    'urlamigable'         => utf8_encode($row['Tipo_Inmueble'] . "-" . $row['Gestion']),
                    'descripcion'         => utf8_encode($Codigo_Inmueble . " " . $row['descripcionlarga']),
                    'varriendo'           => $Canon,
                    'vventa'              => $Venta,
                    'zrural'              => '1',
                    'mapa'                => '1',
                    'info_publica'        => '1',
                    'oportunidad'         => '2',
                    'destacado'           => '1',
                    'mas_vistos'          => '2',
                    'fecha'               => $fechaserv,
                    'ttarifa'             => '13',
                    'activo'              => '2',
                    'IdSitiosCercanos'    => $SitiosCercanos,
                    'IdZonaComunes'       => $ZonaComunes,
                    'latitud'             => $latitud,
                    'longitud'            => $longitud,
                    'idDependencia'       => $dependencia,
                    'idDescripcionFisica' => $DescripcionFisica,
                );

                // array_push($datos_inmueble_entrada[0],$arrayFotos[0]);
                $imagenes=$this->getInmageInmuebles($Codigo_Inmueble);
                $j=0;
                $k=0;
                for ($i=1; $i < 30 ; $i++) 
                { 
                     if($imagenes[0]['Foto'.$i] != "F" && !empty($imagenes[0]['Foto'.$i]))
                     {
                        if($this->validExtension($imagenes[0]['Foto'.$i])<=1)
                        {
                            $j++;

                            if($this->validarImage("../mcomercialweb/".$imagenes[0]['Foto'.$i])==1)
                            {
                                $k++;
                                $datos_inmueble_entrada[0]["imagen".$k]= "https://www.simiinmobiliarias.com/mcomercialweb/".$imagenes[0]['Foto'.$i];   
                            }
                        }
                     }
                }
            }

            if ($error > 0) {
                return array('error' => 1, 'msgerror' => $msgerror);
            } else {
                return $datos_inmueble_entrada;
            }

        } else {
            return $stmt->errorInfo();
            $stmt = null;
        }

    }
    public function validExtension($file)
    {
        $extension=array(".jpg",".png",".PNG",".JPG",".jpeg",".JPEG");
        for($i=0;$i<count($extension);$i++)
        {
            $resultado = strpos($file, $extension[$i]);
            if($resultado != "")
            {
              return 1;
            }
            else
            {
              return 0;
            }
        }
    }
   
    public function getInmageInmuebles($codigo)
    {
        $connPDO = new Conexion();

        $urlF='https://www.simiinmobiliarias.com/mcomercialweb/';

        $stmt = $connPDO->prepare("SELECT *
                  FROM bdcomercial1.fotos
                  WHERE bdcomercial1.fotos.idInm=:idInm");

        if ($stmt->execute(array(

            ":idInm"         => $codigo

        ))) {

            $count=$stmt->RowCount();
            
            if($count>0)
            {
                
                $data="";

                while($row=$stmt->fetch())
                {
                    $data=array($row);
                    if($row=='')
                    {
                        break;
                    }
                }
                return $data;
            }
        } else {
            print_r($stmt->errorInfo());
        }

    }

    public function fotosInmueble($codInmueble)
    {
        if (($consulta = $this->db->prepare(" select foto
        from fotos_nvo
        where id_inmft=?"))
        ) {
            if (!$consulta->bind_param("s", $codInmueble)) {
                echo "error bind";
            } else {
                $consulta->execute();
//              echo "$qry";
                if ($consulta->bind_result($foto)) {
                    $arreglo = array();
                }

                $c = 0;
                while ($consulta->fetch()) {
                    $c++;
                    $arreglo[$c] = array(
                        "url"  => "http://www.simiinmobiliarias.com/mcomercialweb/" . $foto,
                        'tipo' => 'I',
                    );

                }
                return $arreglo;

            }
        } else {
            echo "error img" . $conn->error;
        }

    }
    public function getDataDetalleInmueble($codinm, $IdInmobiliaria, $tpzh)
    {
        $connPDO = new Conexion();

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $connPDO->prepare("SELECT  d.idcaracteristica,m.idzh,d.cantidad_dt
             FROM detalleinmueble d,maestrodecaracteristicas m
             WHERE inmob   = :IdInmobiliaria
             AND codinmdet =:codinm
             AND d.idcaracteristica=m.idcaracteristica
             AND idzh>0
             and tpzh=:tpzh");

        if ($stmt->execute(array(

            ":codinm"         => $codinm,
            ":IdInmobiliaria" => $IdInmobiliaria,
            ":tpzh"           => $tpzh,

        ))) {
            $info = array();
            $posi = 0;
            while ($row = $stmt->fetch()) {
                $posi++;
                $info[] = array(
                    "concepto" => $row['idzh'],
                    "cantidad" => $row['cantidad_dt'],

                );
            }

            return $info;
        } else {
            print_r($stmt->errorInfo());
        }

    }
    public function validarImage($nombre_fichero)
    {
        

        if (file_exists($nombre_fichero)) {
            return 1;
        } else {
            return 0;
        }
    }

}
