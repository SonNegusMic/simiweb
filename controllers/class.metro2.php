<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');



class functions 
{
    
    public function __construct($conn='',$conPDO){
		$this->db=$conn;
        $this->PDO=$conPDO;
		
	}	
	public function validarDir($path)
	{
		 $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$path);
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        echo $retValue = curl_exec($ch);                      
        curl_close($ch);
	}
    public function updateFlag($data)
    {
         
        
        $inmu= $data['inmu'];
        $vlr=$data['vlr'];
        
        $stmt=$this->PDO->prepare("UPDATE inmuebles  
                SET  flagm2        = :vlr
                WHERE idInm    = :inmu");
        if($stmt->execute(array(
                    ':vlr'        => $vlr,
                    ':inmu'      => $inmu
                )))
        {
            return 1;
        }
        else
        {
            return print_r($stmt->errorInfo());
           
        }
        $stmt=NULL;
    }
    public function getCredenciales()
    {
           $response=array();
           $usuario=$_SESSION["Id_Usuarios"];
           $inmo=$_SESSION["IdInmmo"];
        $stmt=$this->PDO->prepare("
                SELECT usuariom2,
                clavem2,
                letra,
                prefijom2
                FROM usuariosm2
                WHERE inmogm2 = :inmogm2
                group by inmogm2");
        if($stmt->execute(array(
            ":inmogm2"=>259
            )))
        {
            if($stmt->rowCount() > 0)
            {
               
                 while ($row=$stmt->fetch()) {
                    
                    $data=array(
                        "usuariom2" =>$row['usuariom2'],
                        "clavem2"   =>$row['clavem2'],
                        "letra"     =>$row['letra'],
                        "prefijom2" =>$row['prefijom2'],
                    );               
                    $response[] = $data;
                }
            }
            else
            {
                $response=array("error"=>"No cuenta con credenciales Metro Cuadrado");
            }
        }
        else
        {
            $response=array(
                "error"=>$stmt->errorInfo(),
                "data"=>$usuario." ".$inmo
                );
        }
        if($response)
        {
            return $response;
        }


    }
}