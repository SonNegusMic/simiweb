<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include("../funciones/connPDO.php");


class Propietarios
{
    public function __construct($conn=""){
		$this->db=$conn;
	}
    
    private $id_usuario;
    
	public function getPropietarios(){
		$connPDO = new Conexion();

		$sql = "SELECT IdPropietario,iduser,CONCAT(Nombres,' ',apellidos) AS nom 
				FROM inmuebles i, usuarios u
				WHERE i.`IdPropietario`=u.`Id_Usuarios`
				AND i.`IdInmobiliaria`= :idinmo
				AND IdPropietario>0
				AND Nombres != ''
				AND apellidos != ''
				GROUP BY IdPropietario ORDER BY nom ASC";

		$stmt = $connPDO->prepare($sql);

		$stmt->bindParam(':idinmo', $_SESSION['IdInmmo']);

		if ($stmt->execute()) {
			while ($row = $stmt->fetch()) {
				$data[] = array(
					'id' => $row['iduser'],
					'Nombre' => ucfirst(strtolower(str_replace('Ñ','ñ', utf8_encode($row['nom']))))
				);
			}
			return $data;
		}
		else
		{
			print_r($stmt->errorInfo());
		}
		$stmt=NULL;
	}
}
?>