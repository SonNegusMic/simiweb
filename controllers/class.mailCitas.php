<?php  
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
require_once('../PHPMailer_5.2.2/class.phpmailer.php');
require_once('../PHPMailer_5.2.2/class.smtp.php');
include("class.mailCard3.php");



class mailCitas
{
	public function __construct($conn="",$pdo){
		$this->db=$conn;
		$this->pdo=$pdo;
	}
	public function envioMailCita($data)
	{
			$mailCard= New MailCard();
			$a=$datosMail=$mailCard->getCard($_SESSION['IdInmo']);
			foreach($a as $key => $value)
			{}
	        
	    //    $data['citaTarea'],
		   // $fechaAct,
		   // ,
		   // 1,
		   // $data['citaHora'],
		   // $_SESSION['codUser'],
		   // $_SESSION['Id_Usuarios'],
		   // $idusuarioAse,
		   // $data['citaAsesor'],
		   // $data['citaCodInm'],
		   // $data['citaCliente'],
		   // $_SESSION['IdInmmo'],
		   // $data['citaObser'],
		   $style="style='color:#009CFF'";
		   $styleText="style='color:#FE3939'";
	       $NombreUsuario  	= ucwords(strtolower(getCampo('usuarios',"where iduser=".$data['citaAsesor'],'concat(Nombres," ",apellidos)')));
	       
	       $logo = getCampo('clientessimi',"where IdInmobiliaria=".$_SESSION['IdInmmo'],'logo');
	       $nomInmo = getCampo('clientessimi',"where IdInmobiliaria=".$_SESSION['IdInmmo'],'Nombre');
		   $foto = getCampo('usuarios'," where iduser=". $data['citaAsesor'],'Foto');

		   $asunto = getCampo('asunto_cita'," where id_asunto=".$data['citaTarea'].' and est_asunto=1','descripcion');

		   $titulo=($data['citaTarea']==1)?"NORM Nueva Cita para el  Inmueble ".$data['citaCodInm']:"Nueva Actividad ".$asunto;
		   $telInmobiliaria = "7525108";


		  

		   $texto=($data['citaTarea']==1)?'<span '.$style.'>Sr(a)</span> '.$nombreCliente.' , Se ha programado una cita para el ver el inmueble '.$data['citaCodInm'].' con el <span '.$style.'>asesor</span> '.$NombreUsuario:"";
	       $telefonsCli="";
	       if(!empty($data['citaCodInm']) && $data['citaTarea']==1)
	       {
	       		$latitud=getCampo('inmuebles'," where idInm=".$data['citaTarea'].'','latitud');
	       		$longitud=getCampo('inmuebles'," where idInm=".$data['citaTarea'].'','longitud');
	       		if(!empty($latitud) && !empty($longitud))
	       		{
	       			$boton='<div style="background-color:#F16D6D; width:100px; font-size:10px;padding:10px 5px 10px 5px; margin:10px 0 0 0; display:inline-block; vertical-align:top;">
				    <a style="color:white;" target="_blank" href="https://www.google.com/maps?&z=10&q='.$latitud.'+'.$longitud.'&ll='.
				    $latitud.'+'.$longitud.'"><div class="btn btn-info">Ver Ubicacion</div></a>
				 </div>&nbsp';
	       		}
	       		else
	       		{
	       			$boton="";
	       		}
	       		$botonFicha='<div style="background-color:#009CFF; width:100px; font-size:10px;padding:10px 5px 10px 5px; margin:10px 0 0 0; display:inline-block; vertical-align:top;">
				    <a style="color:white;" target="_blank" href="https://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/fichatec3.php?reg='.$data['citaCodInm'].'"><div class="btn btn-info">Ver Ficha</div></a>
				 </div>';
	       		
	       }
	        $nombreCliente = getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'nombre');
	       $telefonoCliente = getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'telfijo');
	       $celularCliente =getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'telcelular'); 
	       $mailCliente=getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'email'); 
	       if($telefonoCliente == "" && $celularCliente != ""  )
	       {
	       		$telefonsCli=$telefonoCliente;
	       }
	       if($telefonoCliente != "" && $celularCliente == ""  )
	       {
	       		$telefonsCli=$celularCliente;
	       }
	       if($telefonoCliente == "" && $celularCliente == ""  )
	       {
	       		$telefonsCli="No registra";
	       }
	       if($telefonoCliente != "" && $celularCliente != ""  )
	       {
	       		$telefonsCli=$telefonoCliente." - ".$celularCliente;
	       }
	       list($anio,$mes,$dia)=explode("-",$data['citaFecha']);
	       $fImprimir=getCampo('parametros',"where id_param=13 and conse_param=$mes",'desc_param')." "." $dia de $anio";
		   $nombreCliente=($nombreCliente=="")?"No registra":$nombreCliente;
		   $mailCliente=($mailCliente=="")?"No registra":$mailCliente;
		   $fActividad='<p><b '.$style.'>Fecha Actividad:</b> '.$fImprimir.'- '.$data['citaHora'].' </p>';
		   $infoCLiente='<p><b '.$style.'>Cliente:</b> '.$nombreCliente.' </p><p><b '.$style.'>Telefono Cliente:</b> '.$telefonsCli.'</p><p><b '.$style.'>Correo Cliente:</b> '.$mailCliente.'</p>';
		   $observaciones=($data['citaTarea']==1)?'<p><b '.$style.'>Observaciones:</b> '.$data['citaObser'].' </p>':'<p><b '.$style.'>Observaciones:</b> '.$data['citaObser'].' </p>';
		   $infoCLienteCitaCliente='<p><b '.$style.'>Telefono Cliente:</b> '.$telefonsCli.'</p><p><b '.$style.'>Correo Cliente:</b> '.$mailCliente.'</p><p><b>Número de Teléfono Inmobiliaria:</b>'.$telInmobiliaria.'</p>';

		   $correoAsignado="";
		   $verificaMailAsignado=$this->getAsignadoMail($data['citaAsesor']);
		   if($verificaMailAsignado['reporta_a']>0)
		   {
		   	  $correoAsignado=getCampo('usuarios'," where iduser=".$verificaMailAsignado['reporta_a'],'Correo');
		   }else
		   {
		   	  $correoAsignado=getCampo('clientessimi'," where IdInmobiliaria=".$_SESSION['IdInmmo'],'Correo');
		   }

	    $cadena="12345";
	    $token = hash("sha512",$cadena);


	    if( isset($data["emailPropietario"]) and $data['citaTarea']==1 ){
			$tituloPropietario=($data['citaTarea']==1)?"PROP Nueva Cita para el  Inmueble ".$data['citaCodInm']:"Nueva Actividad ".$asunto;
			$textoPropietario=($data['citaTarea']==1)?'<span '.$style.'>Sr(a)</span> '.$nombreCliente.' , Se ha programado una cita para ver el inmueble '.$data['citaCodInm'].' con el <span '.$style.'>asesor</span> '.$NombreUsuario:"";
			$infoCLientePropietario='';

			$bodyPropietario='<html> 
			<head><meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
				<title>Cita</title> 
					<style>
					</style>
			</head>
			<body>
				<table align="center"  cellpadding="0" cellspacing="0" width="100%" style="border:0;">
 				<tr>
					<td bgcolor=""  align="center">
 		 				<p class="" >
						<a><img style="height:auto; width:auto; max-height:100%; max-width:200px;"  src=http://www.simiinmobiliarias.com/logo/'.$logo.'></a>
			  			</p>
					</td>
				 </tr>
				  <tr>
					<td bgcolor="#F2F2F2" align="center" style="padding: 15px 0 0px 0;">
						<font face="verdana" color="#FFFFFF"><h4>'.$tituloPropietario.'</h4>
						</font>
					</td>
 				</tr>
 				<tr>
						<td bgcolor="#ee4c50" height="">
				</td>
 				</tr>
 				<tr>
 					<td  bgcolor="#FFFFFF" style="border:3px #909090 solid;padding: 15px 0 0px 0;" align="center">
 					<div>
						<a><img style="height:auto; width:auto; max-height:100px; max-width:100px;" src=http://www.simiinmobiliarias.com/mcomercialweb/'.$foto.'></a>	
					</div>
 					 <div>		
						<font face="verdana" color="#424242">
						<h5>
							'.$textoPropietario.'
							<p></p>
						</h5>
						</font>
					  </div>
					   <div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						<h6  align="justify">
							'.$fActividad.'
							<p><b '.$style.'>Asesor:</b> '.$NombreUsuario.' </p>
							'.$infoCLientePropietario.'
							<p><b '.$style.'>Asunto:</b> '.$asunto.' </p>
							
						</h6>
						</font> 
					  </div>
					   
					  ';
					 
					  $bodyPropietario.='<div>
						<font face="verdana" color="#424242">
						<p></p>
						   <a  target="_blank" href="http://www.simiinmobiliarias.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA.</a>
						   <p></p>
						   Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo 

						</font>
					  </div>
 					</td>
 				</tr>';

 				$mail  = new PHPMailer(); 
				$mail->IsSMTP();
				$mail->SMTPAuth = true;
				$mail->SMTPSecure = "tls";
				$mail->Host = $value['host_ml']; //Servidor de Correo 
				$mail->Port = $value['port_ml'];
				$mail->Username = $value['usr_ml'];//usuario perteneciente al servidor
				$mail->Password = $value['pass_ml'];
				$mail->From = $value['from_ml'];
				$mail->FromName = "Agente Virtual SimiWeb";
				$mail->AddBCC("desarrolloweb3@tae-ltda.com", "desarrolloweb3@tae-ltda.com");
				//$mail->AddBCC("desarrolloweb@tae-ltda.com", "desarrolloweb@tae-ltda.com");
				// $mail->AddBCC("auxadmin@tae-ltda.com", "auxadmin@tae-ltda.com");
				$mail->Subject = $asunto;
				$mail->MsgHTML($bodyPropietario);
				$mail->IsHTML(true);
				$mail->SMTPDebug=0;
				$mail->Send();
		}


		if( isset($data["emailCliente"]) and $data['citaTarea']==1 ){
			$tituloCliente=($data['citaTarea']==1)?"CLI Nueva Cita para el  Inmueble ".$data['citaCodInm']:"Nueva Actividad ".$asunto;
			$textoPropietario=($data['citaTarea']==1)?'<span '.$style.'>Sr(a)</span> '.$nombreCliente.' , Se ha programado una cita para ver el inmueble '.$data['citaCodInm'].' con el <span '.$style.'>asesor</span> '.$NombreUsuario:"";
			$infoCLientePropietario='';

			$bodyEmail='<html> 
			<head><meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
				<title>Cita</title> 
					<style>
					</style>
			</head>
			<body>
				<table align="center"  cellpadding="0" cellspacing="0" width="100%" style="border:0;">
 				<tr>
					<td bgcolor=""  align="center">
 		 				<p class="" >
						<a><img style="height:auto; width:auto; max-height:100%; max-width:200px;"  src=http://www.simiinmobiliarias.com/logo/'.$logo.'></a>
			  			</p>
					</td>
				 </tr>
				  <tr>
					<td bgcolor="#F2F2F2" align="center" style="padding: 15px 0 0px 0;">
						<font face="verdana" color="#FFFFFF"><h4>'.$tituloCliente.'</h4>
						</font>
					</td>
 				</tr>
 				<tr>
						<td bgcolor="#ee4c50" height="">
				</td>
 				</tr>
 				<tr>
 					<td  bgcolor="#FFFFFF" style="border:3px #909090 solid;padding: 15px 0 0px 0;" align="center">
 					<div>
						<a><img style="height:auto; width:auto; max-height:100px; max-width:100px;" src=http://www.simiinmobiliarias.com/mcomercialweb/'.$foto.'></a>	
					</div>
 					 <div>		
						<font face="verdana" color="#424242">
						<h5>
							'.$texto.'
							<p></p>
						</h5>
						</font>
					  </div>
					   <div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						<h6  align="justify">
							'.$fActividad.'
							<p><b '.$style.'>Asesor:</b> '.$NombreUsuario.' </p>
							'.$infoCLienteCitaCliente.'
							<p><b '.$style.'>Asunto:</b> '.$asunto.' </p>
							
							<p>Correo Electrónico Asesor: '.$correoAsignado.'</p>
						</h6>
						</font> 
					  </div>
					   <div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						<h6 align="justify">'.$observaciones.'</h6>
						</font>
					  </div>
					  ';
					  if($data['citaTarea']==1){
					   $body.='<div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						'.$boton.$botonFicha.'
						</font>
					  </div>';
						}
					  $body.='<div>
						<font face="verdana" color="#424242">
						<p></p>
						   <a  target="_blank" href="http://www.simiinmobiliarias.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA.</a>
						   <p></p>
						   Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo 

						</font>
					  </div>
 					</td>
 				</tr>';

 				$mail  = new PHPMailer(); 
				$mail->IsSMTP();
				$mail->SMTPAuth = true;
				$mail->SMTPSecure = "tls";
				$mail->Host = $value['host_ml']; //Servidor de Correo 
				$mail->Port = $value['port_ml'];
				$mail->Username = $value['usr_ml'];//usuario perteneciente al servidor
				$mail->Password = $value['pass_ml'];
				$mail->From = $value['from_ml'];
				$mail->FromName = "Agente Virtual SimiWeb";
				$mail->AddBCC("desarrolloweb3@tae-ltda.com", "desarrolloweb3@tae-ltda.com");
				//$mail->AddBCC("desarrolloweb@tae-ltda.com", "desarrolloweb@tae-ltda.com");
				// $mail->AddBCC("auxadmin@tae-ltda.com", "auxadmin@tae-ltda.com");
				$mail->Subject = $asunto;
				$mail->MsgHTML($bodyPropietario);
				$mail->IsHTML(true);
				$mail->SMTPDebug=0;
				$mail->Send();
		}
 		
		$body='<html> 
			<head><meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
				<title>Cita</title> 
					<style>
					</style>
			</head>
			<body>
				<table align="center"  cellpadding="0" cellspacing="0" width="100%" style="border:0;">
 				<tr>
					<td bgcolor=""  align="center">
 		 				<p class="" >
						<a><img style="height:auto; width:auto; max-height:100%; max-width:200px;"  src=http://www.simiinmobiliarias.com/logo/'.$logo.'></a>
			  			</p>
					</td>
				 </tr>
				  <tr>
					<td bgcolor="#F2F2F2" align="center" style="padding: 15px 0 0px 0;">
						<font face="verdana" color="#FFFFFF"><h4>'.$titulo.'</h4>
						</font>
					</td>
 				</tr>
 				<tr>
						<td bgcolor="#ee4c50" height="">
				</td>
 				</tr>
 				<tr>
 					<td  bgcolor="#FFFFFF" style="border:3px #909090 solid;padding: 15px 0 0px 0;" align="center">
 					<div>
						<a><img style="height:auto; width:auto; max-height:100px; max-width:100px;" src=http://www.simiinmobiliarias.com/mcomercialweb/'.$foto.'></a>	
					</div>
 					 <div>		
						<font face="verdana" color="#424242">
						<h5>
							'.$texto.'
							<p></p>
						</h5>
						</font>
					  </div>
					   <div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						<h6  align="justify">
							'.$fActividad.'
							<p><b '.$style.'>Asesor:</b> '.$NombreUsuario.' </p>
							'.$infoCLiente.'
							<p><b '.$style.'>Asunto:</b> '.$asunto.' </p>
							
							<p>mail de asignado: '.$correoAsignado.'</p>
						</h6>
						</font> 
					  </div>
					   <div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						<h6 align="justify">'.$observaciones.'</h6>
						</font>
					  </div>
					  ';
					  if($data['citaTarea']==1){
					   $body.='<div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						'.$boton.$botonFicha.'
						</font>
					  </div>';
						}
					  $body.='<div>
						<font face="verdana" color="#424242">
						<p></p>
						   <a  target="_blank" href="http://www.simiinmobiliarias.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA.</a>
						   <p></p>
						   Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo 

						</font>
					  </div>
 					</td>
 				</tr>

		';

				
		

		//echo $body;
		$mail  = new PHPMailer(); 
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "tls";
		$mail->Host = $value['host_ml']; //Servidor de Correo 
		$mail->Port = $value['port_ml'];
		$mail->Username = $value['usr_ml'];//usuario perteneciente al servidor
		$mail->Password = $value['pass_ml'];
		$mail->From = $value['from_ml'];
		$mail->FromName = "Agente Virtual SimiWeb";
		$mail->AddBCC("desarrolloweb3@tae-ltda.com", "desarrolloweb3@tae-ltda.com");
		$mail->AddBCC("desarrolloweb@tae-ltda.com", "desarrolloweb@tae-ltda.com");
		// $mail->AddBCC("auxadmin@tae-ltda.com", "auxadmin@tae-ltda.com");
		$mail->Subject = $asunto;
		$mail->MsgHTML($body);
		$mail->IsHTML(true);
		$mail->SMTPDebug=0;
		if(!$mail->Send()) {
			return 0;
		} else {
		    return 1;
		}
		

	
	}

	public function envioMailCitaNew($data){

		if( isset($data["emailCliente"]) and $data['citaTarea']==1 ){


		}

		if( isset($data["emailPropietario"]) and $data['citaTarea']==1 ){

		}

		if( isset($data["emailGerente"]) and $data['citaTarea']==1 ){

		}

		if( isset($data["emailCaptador"]) and $data['citaTarea']==1 ){

		}

		

		

		echo "<pre>";
		print_r($data);
		echo "</pre>";

	}
	public function getAsignadoMail($idUser)
	{
		$stmt=$this->pdo->prepare('SELECT reporta_a 
						FROM usuarios WHERE 
						iduser =:iduser
						LIMIT 0,10');
		if($stmt->execute(array(
			":iduser"=>$idUser
			)))
		{
			if($stmt->rowCount()>0)
			{
				return $stmt->fetch(PDO::FETCH_ASSOC);
			}else
			{
				return 0;
			}
		}else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$data
			);
			return $response;
		}
	}
	
}

?>