<?php

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

include('class.negociosA.php');

/**
 *
 */
class CallCenter
{

    public function __construct($connPDO,$phpmailer)
    {
        $this->connPDO = $connPDO;
        $this->phpmailer = $phpmailer;
    }

    public function filterClientes($get, $data, $viewlimit = "")
    {

        //  $connPDO   = new Conexion();
        $cond      = "";
        $tabla     = "";
        $enlace    = "";
        $btnVender = "";
        $dataN     = $data;

        /*if ($asesor) {
        $cond .= "and c.ase_cli=:asesor";
        }
        if ($cliente) {
        $cond .= " and c.nombre  LIKE :nombre";
        }
        if ($estado > 0) {
        $cond .= " and c.est_cli=:estado";
        }
        if ($medio > 0) {
        $cond .= " and c.idmedio=:medio";
        }
        if ($telefono) {
        $cond .= "and c.telfijo like :telefono";
        }
        if ($celular) {
        $cond .= "and c.telcelular like :celular";
        }
        if ($correo) {
        $cond .= "AND c.email like :correo";
        }
        if ($empresa) {
        $cond .= "and c.emp_cli like :empresa";
        }*/

        /*datatable columnas query y tabla*/
        $aColumns = array("c.ase_cli", "c.nombre", "c.est_cli", "c.idmedio", "c.telfijo", "c.telcelular", "c.email", "c.emp_cli", "c.f_crea_cli");
        /*columnas para ordenar*/
        $columnsorder = array("c.ase_cli", "c.nombre", "c.est_cli", "c.idmedio", "c.telfijo", "c.telcelular", "c.email", "c.emp_cli", "c.f_crea_cli");
        if (!empty($get)) {

            /*capturamos el orden que nos envia el datatable*/
            $condOrder = ' ORDER BY LTRIM(' . $columnsorder[$get['order'][0]['column']] . ') ' . $get['order'][0]['dir'];
            /*armamos el where segun las columnas*/
            $sWhere = "";
            for ($i = 0; $i < count($aColumns); $i++) {
                if (isset($get['search']) && $get["search"]["value"] != '') {
                    if ($sWhere == "") {
                        $sWhere = " AND (";
                    } else {
                        $sWhere .= " OR ";
                    }
                    $sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($get["search"]["value"]) . "%' ";
                }
            }

            /*
             * paginacion
             */
            $sLimit = " LIMIT 0,50 ";
            if (isset($get['start']) && $get['length'] != '-1') {
                $sLimit = " LIMIT " . intval($get['start']) . ", " .
                intval($get['length']);
            } else {
                $condOrder = ' ORDER BY f_crea_cli DESC';
            }
        } else {
            $sLimit    = "";
            $condOrder = ' ORDER BY f_crea_cli DESC';
        }

        $cliente    = (trim($data["cliente"]) != "") ? "%" . $data["cliente"] . "%" : "";
        $medio      = $data["medio"];
        $telefono   = (trim($data["telefono"]) != "") ? "%" . $data["telefono"] . "%" : "";
        $correo     = (trim($data["correo"]) != "") ? "%" . $data["correo"] . "%" : "";
        $asesor     = $data["asesor"];
        $celular    = (trim($data["celular"]) != "") ? "%" . $data["celular"] . "%" : "";
        $empresa    = (trim($data["empresa"]) != "") ? "%" . $data["empresa"] . "%" : "";
        $estado     = $data["estado"];
        $fecha_inis = $data["ffecha_ini"];
        $fecha_fins = $data["ffecha_fin"];
        $cond       = "";

        /*validamos para el filtro*/

        if ($asesor) {
            $cond .= "and c.ase_cli=:asesor";
        }
        if (!empty($cliente)) {
            $cond .= " and c.nombre  LIKE :nombre";
        }
        if ($estado > 0) {
            $cond .= " and c.est_cli=:estado";
        }
        if ($medio > 0) {
            $cond .= " and c.idmedio=:medio";
        }
        if ($telefono) {
            $cond .= "and c.telfijo like :telefono";
        }
        if ($celular) {
            $cond .= "and c.telcelular like :celular";
        }
        if ($correo) {
            $cond .= "AND c.email like :correo";
        }
        if (!empty($empresa)) {
            $cond .= "and c.emp_cli like :empresa";
        }

        /*validamos para el filtro*/

        if ($sWhere != "") {

            $sWhere .= $cond;
            $sWhere .= ")";

        } else {
            $sWhere .= "";
        }

        $rowsPerPage = 50;
        $offset      = ($data['paginador'] - 1) * $rowsPerPage;

        $sql = "SELECT c.cedula, ltrim(c.nombre) as nombre, c.direccion, c.telfijo, c.telcelular, c.email,m.descripcion,inm.NombreInm,c.est_cli,c.ase_cli,c.obser_cli, emp_cli, f_crea_cli
                            from clientes_inmobiliaria c
                            LEFT JOIN medio m ON m.idmedio = c.idmedio
                            INNER JOIN inmobiliaria inm ON c.inmobiliaria=inm.IdInmobiliaria
                            where c.inmobiliaria = :inmob " . $cond . $sWhere . $condOrder . $sLimit;

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':inmob', $_SESSION["IdInmmo"]);

        if ($asesor) {
            $stmt->bindParam(":asesor", $asesor);
        }
        if ($cliente) {
            $stmt->bindParam(":nombre", $cliente);
        }
        if ($estado > 0) {
            $stmt->bindParam(":estado", $estado);
        }
        if ($medio > 0) {
            $stmt->bindParam(":medio", $medio);
        }
        if ($telefono) {
            $stmt->bindParam(":telefono", $telefono);
        }
        if ($celular) {
            $stmt->bindParam(":celular", $celular);
        }
        if ($correo) {
            $stmt->bindParam(":correo", $correo);
        }
        if ($empresa) {
            $stmt->bindParam("empresa", $empresa);
        }

        if ($stmt->execute()) {

            $datas = array();
            $this->connPDO->exec('chartset="utf-8"');
            $cantidad = $stmt->rowCount();

            while ($row = $stmt->fetch()) {
                $catidadResgistros = count($row);
                $estado_cli        = getCampo('parametros', "WHERE id_param='15' and conse_param='" . $row['est_cli'] . "'", 'desc_param');
                $nasesor           = '';
                //if ($asesor > 0) {
                $nasesor = getCampo('usuarios', "WHERE iduser='" . $row['ase_cli'] . "'", 'concat(Nombres," ",apellidos)');
                /*} else {
                $nasesor = "";
                }*/
                //echo $row['ase_cli']."-";
                $nombref      = ucwords(strtolower($row['nombre']));
                $telefonof    = $row['telfijo'];
                $celularf     = $row['telcelular'];
                $correof      = $row['email'];
                $descripcionf = ucwords(strtolower($row['descripcion']));
                $estadof      = $estado_cli;
                $empresaf     = ucwords(strtolower($row['emp_cli']));
                $obervacionf  = $row['obser_cli'];
                $cedulaf      = $row['cedula'];

                $HistorialCitas    = array();
                $HistorialDemandas = array();

                $HistorialCitas    = $this->getHistorialCitas($row);
                $HistorialDemandas = $this->getHistorialDemanda($row);

                //print_r($HistorialCitas["data"]);
                if (count($HistorialCitas["data"]) <= 0 && empty($HistorialDemandas['data'])) {
                    $botonEditarf = '<button type="button" class="btn btn-watermelon btn-xs editarCliente" data-id="' . $row['cedula'] . '"><i class="fa fa-edit"></i></button>&nbsp;';
                } else {
                    $botonEditarf = '<button type="button" class="btn btn-watermelon btn-xs editarCliente" data-id="' . $row['cedula'] . '"><i class="fa fa-edit"></i></button>&nbsp;<button  type="button" class="btn btn-spartan btn-xs historialCliente" data-id="' . $row['cedula'] . '"><i class="fa fa-history" aria-hidden="true"></i></button>&nbsp;';
                }

                $datas[] = array(
                    "nombre"      => $nombref,
                    "telefono"    => $telefonof,
                    "celular"     => $celularf,
                    "correo"      => $correof,
                    "descripcion" => $descripcionf,
                    "estado"      => $estadof,
                    "empresa"     => $empresaf,
                    "observacion" => $obervacionf,
                    "asesor"      => $nasesor,
                    "historial"   => $botonEditarf,
                    "f_crea_cli"  => $row['f_crea_cli'],
                );
            }

            /* Total data set length */
            $sQuery = "
                SELECT COUNT(*) as total
                FROM   clientes_inmobiliaria as c where c.inmobiliaria= :inmob " . $enlace . $cond . $sWhere;

            /*echo $sQuery."<br>";
            echo $sWhere."<br>";
            echo $cond."<br>";*/
//exit;
            $stmtcount = $this->connPDO->prepare($sQuery);

            $stmtcount->bindParam(':inmob', $_SESSION["IdInmmo"]);

            if ($asesor) {
                $stmtcount->bindParam(":asesor", $asesor);
                // "ii.asesor";
            }
            if ($cliente) {
                $stmtcount->bindParam(":nombre", $cliente);
                // "ii.cliente";
            }
            if ($estado > 0) {
                $stmtcount->bindParam(":estado", $estado);
                // "ii.estado";
            }
            if ($medio > 0) {
                $stmtcount->bindParam(":medio", $medio);
                // "ii.medio";
            }
            if (!empty($telefono)) {
                $stmtcount->bindParam(":telefono", $telefono);
                // "ii.telefono: ".$telefono."-";
            }
            if (!empty($celular)) {
                $stmtcount->bindParam(":celular", $celular);
                // "ii.celular: ".$celular."-";
            }
            if (!empty($correo)) {
                $stmtcount->bindParam(":correo", $correo);
                // "ii.correo: ".$correo."-";
            }
            if ($empresa) {
                $stmtcount->bindParam("empresa", $empresa);
                // "ii.empresa";
            }

            $stmtcount->execute();
            $rResultTotal = $stmtcount->fetchAll();

            $iTotal = $rResultTotal[0]["total"];

            $output = array(
                "sEcho"                => intval($data['sEcho']),
                "iTotalRecords"        => $iTotal,
                "iTotalDisplayRecords" => $iTotal, //$iFilteredTotal[0],
                "aaData"               => $datas,
            );

            return $output;

        } else {
            return print_r($stmt->errorInfo());
        }

        /*$stmt = $this->connPDO->prepare("SELECT c.cedula, ltrim(c.nombre) as nombre, c.direccion, c.telfijo, c.telcelular,
        c.email,m.descripcion,inm.NombreInm,c.est_cli,c.ase_cli,
        c.obser_cli,emp_cli
        FROM clientes_inmobiliaria c
        LEFT JOIN medio m ON m.idmedio = c.idmedio
        INNER JOIN inmobiliaria inm ON c.inmobiliaria=inm.IdInmobiliaria
        WHERE c.inmobiliaria = :inmob
        $cond
        order by LTRIM(c.nombre)
        #limit 0,3000");
        /*echo "SELECT c.cedula, ltrim(c.nombre) as nombre, c.direccion, c.telfijo, c.telcelular,
        c.email,m.descripcion,inm.NombreInm,c.est_cli,c.ase_cli,
        c.obser_cli,emp_cli
        FROM clientes_inmobiliaria c
        LEFT JOIN medio m ON m.idmedio = c.idmedio
        INNER JOIN inmobiliaria inm ON c.inmobiliaria=inm.IdInmobiliaria
        WHERE c.inmobiliaria = :inmob
        $cond
        order by LTRIM(c.nombre)
        #limit 0,1000";*/

        // $stmt->bindParam(":inmob", $_SESSION['IdInmmo']);
        /*if ($asesor) {
        $stmt->bindParam(":asesor", $asesor);
        }
        if ($cliente) {
        $stmt->bindParam(":nombre", $cliente);
        }
        if ($estado > 0) {
        $stmt->bindParam(":estado", $estado);
        }
        if ($medio > 0) {
        $stmt->bindParam(":medio", $medio);
        }
        if ($telefono) {
        $stmt->bindParam(":telefono", $telefono);
        }
        if ($celular) {
        $stmt->bindParam(":celular", $celular);
        }
        if ($correo) {
        $stmt->bindParam(":correo", $correo);
        }
        if ($empresa) {
        $stmt->bindParam("empresa", $empresa);
        }*/

        /*if ($stmt->execute()) {
        $datas = array();
        // $stmt->debugDumpParams();
        $this->connPDO->exec("charset='utf-8'");
        while ($row = $stmt->fetch()) {

        $estado_cli = getCampo('parametros', "WHERE id_param='15' and conse_param='" . $row['est_cli'] . "'", 'desc_param');
        $nasesor = '';
        if ($asesor > 0) {
        $nasesor = getCampo('usuarios', "WHERE Id_Usuarios='$asesor'", 'Nombres');
        } else {
        $asesor = "";
        }
        $nombre      = ucwords(strtolower($row['nombre']));
        $telefono    = $row['telfijo'];
        $celular     = $row['telcelular'];
        $correo      = $row['email'];
        $descripcion = ucwords(strtolower($row['descripcion']));
        $estado      = ucwords(strtolower($row['est_cli']));
        $empresa     = ucwords(strtolower($row['emp_cli']));
        $obervacion  = $row['obser_cli'];
        $cedula      = $row['cedula'];
        $botonEditar = '<button type="button" class="btn btn-watermelon btn-xs"><i class="fa fa-edit"></i></button>';
        $data[] = array(
        "nombre"      => $nombre,
        "telefono"    => $telefono,
        "celular"     => $celular,
        "correo"      => $correo,
        "descripcion" => $descripcion,
        "estado"      => $estado_cli,
        "empresa"     => $empresa,
        "observacion" => $obervacion,
        "asesor"      => $asesor,
        "historial"   => $botonEditar . $botonEditar . $botonEditar . $botonEditar
        );
        }
        return $data;
        } else {
        return print_r($stmt->errorInfo());
        }*/
        //$stmt = NULL;
    }

    public function listaInmueblesCall($get, $data)
    {
        $cond     = '';
        $cond1    = '';
        $tabla    = '';
        $rel      = '';
        $tablap   = '';
        $relp     = '';
        $grp      = "";
        $tabladet = "";
        $condiinmob="";

        $formbarrio = ' INNER JOIN barrios AS b ON i.`IdBarrio`= b.`IdBarrios` INNER JOIN ciudad AS c ON b.`IdCiudad` = c.`IdCiudad` INNER JOIN zonas AS z ON b.`IdZona` = z.`IdZona` INNER JOIN tipoinmuebles AS tp ON i.IdTpInm = tp.idTipoInmueble';

        $codinm           = $data['codinm'];
        $codportal        = $data['codportal'];
        $tipinm           = $data['tipinm'];
        $tpinmuebles      = $data['tpinmuebles'];
        $gestion          = $data['gestion'];
        $city             = $data['city'];
        $zona             = $data['zona'];
        $barrio           = $data['barrios'];
        $idbarrio         = $data['idbarrio'];
        $barriolike       = str_replace('+', ' ', $data['barriolike']);
        $preciofrom       = str_replace('.', '', $data['preciofrom']);
        $precioto         = str_replace('.', '', $data['precioto']);
        $areafrom         = str_replace('.', '', $data['areafrom']);
        $areato           = str_replace('.', '', $data['areato']);
        $cllfrom          = $data['cllfrom'];
        $cllto            = $data['cllto'];
        $crafrom          = $data['crafrom'];
        $crato            = $data['crato'];
        $habitaciones     = $data['habitaciones'];
        $banios           = $data['banios'];
        $parqueadero      = $data['parqueadero'];
        $ascensor         = $data['ascensor'];
        $propietario      = $data['propietario'];
        $direccion        = $data['direccion'];
        $busquedarapida   = $data['busquedarapida'];
        $estrato          = $data['estrato'];
        $hiddenCodDemanda = $data['codDemanda'];
        $verRedes         = $data['verRedes'];
        $politicas         = $data['politicas'];
        $destinacion         = $data['destinacion'];
        if (isset($get['similares']) && $get['similares'] == '1') {
            if (!empty($codinm)) {
                if (strpos($codinm, ',')) {
                    $cod = explode(',', $codinm);
                    if (strpos($cod[0], '-')) {
                        $existe = getCampo('inmnvo', ' where idInm = ' . $cod[0] . ' AND IdInmobiliaria = ' . $get['a'], 'IdInmobiliaria');
                        if (!empty($existe)) {
                            $gestion = getCampo('inmnvo', ' where idInm = ' . $cod[0] . ' AND IdInmobiliaria = ' . $get['a'], 'IdGestion');
                            if ($gestion == '1') {
                                $preciofrom = getCampo('inmnvo', ' where idInm = ' . $cod[0] . ' AND IdInmobiliaria = ' . $get['a'], 'ValorCanon');
                            } else {
                                $preciofrom = getCampo('inmnvo', ' where idInm = ' . $cod[0] . ' AND IdInmobiliaria = ' . $get['a'], 'ValorVenta');
                            }
                            $areafrom = substr(getCampo('inmnvo', ' where idInm = ' . $cod[0], 'AreaLote'), 0, -3);
                        } else {
                            $codinm = '';
                        }
                    } else {
                        $existe = getCampo('inmnvo', ' where codinm = ' . $cod[0] . ' AND IdInmobiliaria = ' . $get['a'], 'IdInmobiliaria');
                        if (!empty($existe)) {
                            $gestion = getCampo('inmnvo', ' where codinm = ' . $cod[0] . ' AND IdInmobiliaria = ' . $get['a'], 'IdGestion');
                            if ($gestion == '1') {
                                $preciofrom = getCampo('inmnvo', ' where codinm = ' . $cod[0] . ' AND IdInmobiliaria = ' . $get['a'], 'ValorCanon');
                            } else {
                                $preciofrom = getCampo('inmnvo', ' where codinm = ' . $cod[0] . ' AND IdInmobiliaria = ' . $get['a'], 'ValorVenta');
                            }
                            $areafrom = substr(getCampo('inmnvo', ' where codinm = ' . $cod[0], 'AreaLote'), 0, -3);
                        } else {
                            $codinm = '';
                        }
                    }
                } else {
                    if (strpos($codinm, '-')) {
                        $existe = getCampo('inmnvo', ' where idInm = ' . $codinm . ' AND IdInmobiliaria = ' . $get['a'], 'IdInmobiliaria');
                        if (!empty($existe)) {
                            $gestion = getCampo('inmnvo', ' where idInm = ' . $codinm . ' AND IdInmobiliaria = ' . $get['a'], 'IdGestion');
                            if ($gestion == '1') {
                                $preciofrom = getCampo('inmnvo', ' where idInm = ' . $codinm . ' AND IdInmobiliaria = ' . $get['a'], 'ValorCanon');
                            } else {
                                $preciofrom = getCampo('inmnvo', ' where idInm = ' . $codinm . ' AND IdInmobiliaria = ' . $get['a'], 'ValorVenta');
                            }
                            $areafrom = substr(getCampo('inmnvo', ' where idInm = ' . $codinm, 'AreaLote'), 0, -3);
                        } else {
                            $codinm = '';
                        }
                    } else {
                        $existe = getCampo('inmnvo', ' where codinm = ' . $codinm . ' AND IdInmobiliaria = ' . $get['a'], 'IdInmobiliaria');
                        if (!empty($existe)) {
                            $gestion = getCampo('inmnvo', ' where codinm = ' . $codinm . ' AND IdInmobiliaria = ' . $get['a'], 'IdGestion');
                            if ($gestion == '1') {
                                $preciofrom = getCampo('inmnvo', ' where codinm = ' . $codinm . ' AND IdInmobiliaria = ' . $get['a'], 'ValorCanon');
                            } else {
                                $preciofrom = getCampo('inmnvo', ' where codinm = ' . $codinm . ' AND IdInmobiliaria = ' . $get['a'], 'ValorVenta');
                            }
                            $areafrom = substr(getCampo('inmnvo', ' where codinm = ' . $codinm, 'AreaLote'), 0, -3);
                        } else {
                            $codinm = '';
                        }
                    }
                }
            }
            $codinm = "";
            if ($gestion == '1') {
                $tolarren = '300000';
            } else {
                $tolarren = '10000000';
            }
            if ($preciofrom > $tolarren) {
                $preciofrom = $preciofrom - $tolarren;
            }
            if ($precioto > '0') {
                $precioto = $precioto + $tolarren;
            }
            if ($areafrom > '15') {
                $areafrom = $areafrom - 15;
            }
            if ($areato > '0') {
                $areato = $areato + 15;
            }
            if ($cllfrom > '10') {
                $cllfrom = $cllfrom - 10;
            }
            if ($cllto > '0') {
                $cllto = $cllto + 10;
            }
            if ($crafrom > '10') {
                $crafrom = $crafrom - 10;
            }
            if ($crato > '0') {
                $crato = $crato + 10;
            }
        }
        list($aa, $codsencillo) = explode("-", $codinm);
        if (strpos($codinm, '-')) 
        {
            list($inmobiliaria, $inmueble) = explode("-", $codinm);
            $condiinmob=" and i.IdInmobiliaria=$inmobiliaria";
        }
        if (!empty($codinm)) {
            ///evaluamos si viene código completo o sin guión
            if (strstr($codinm, '-') != true) {
                $inmueble = $codinm;
            }

            if (strpos($codinm, '%2C')) {
                $codigos = explode('%2C', $codinm);
                for ($i = 0; $i < count($codigos); $i++) {
                    if (empty($codigos[$i])) {
                        unset($codigos[$i]);
                    }
                }
                $condin = "";
                foreach ($codigos as $key => $value) {
                    $condin .= $value . ',';
                }
                $condin = substr($condin, 0, -1);

                $cond .= ' AND i.codinm in (' . $inmueble . ')'.$condiinmob;
            } else {
                $cond .= ' AND i.codinm in (' . $inmueble . ')'.$condiinmob;
            }
        } else {
            if (empty($tpinmuebles)) {
                if (!empty($tipinm)) {
                    $cond .= " AND i.IdTpInm = :tipinm";
                }
            }
            if (!empty($gestion) && $gestion != '0') {
                $cond .= " AND i.IdGestion = :gestion";
            }
            if (!empty($city) && $city != '0') {
                $cond .= " AND b.IdCiudad = :city";
                if (empty($formbarrio)) {
                    // $formbarrio .= ' INNER JOIN barrios AS b ON i.`IdBarrio`= b.`IdBarrios`';
                }
            }
            if (!empty($codportal)) {
                $tabladet = " , detportalesinmueble dtp";
                if (strpos($codportal, '%2C')) {
                    $codigosp = explode('%2C', $codportal);
                    for ($ii = 0; $ii < count($codportal); $ii++) {
                        if (empty($codigosp[$ii])) {
                            unset($codigosp[$ii]);
                        }
                    }
                    $condinp = "";

                    foreach ($codigosp as $key => $value) {
                        $condinp .= $value . ',';
                    }
                    $condinp = substr($condin, 0, -1);
                    $cond .= " AND dtp.cod_dtp in ('" . $condin . "') and i.codinm=dtp.idinm_dtp and i.IdInmobiliaria=dtp.inmob_dtp";
                } else {
                    $cond .= " AND dtp.cod_dtp in ('" . $codportal . "') and i.codinm=dtp.idinm_dtp and i.IdInmobiliaria=dtp.inmob_dtp";
                }

            }
            // if (!empty($zona) && $zona != '0') {
            //     $cond .= " AND b.IdZona = :zona";
            //     if (empty($formbarrio)) {
            //         $formbarrio .= ' INNER JOIN barrios AS b ON i.`IdBarrio`= b.`IdBarrios`';
            //     }
            // }
            if (!empty($barrio) && $barrio[0] != '0') {
                $wherein    = '';
                $contbarrio = count($barrio) - 1;
                for ($i = 0; $i < count($barrio); $i++) {
                    if ($i < $contbarrio) {
                        $wherein .= $barrio[$i] . ',';
                    } else {
                        $wherein .= $barrio[$i];
                    }
                }
                $cond .= " AND i.IdBarrio IN ($wherein)";
                if (empty($formbarrio)) {
                    // $formbarrio .= ' INNER JOIN barrios AS b ON i.`IdBarrio`= b.`IdBarrios`';
                }
            } elseif (!empty($zona) && $zona != 0) {
                $cond .= " AND z.IdZona = " . $zona;
            }

            if (!empty($idbarrio) && $idbarrio > 0) {
                $cond .= " AND i.IdBarrio = " . $idbarrio;
            }
            if (!empty($tpinmuebles) && $tpinmuebles[0] != '0') {
                $whereintpinmuebles = '';
                $conttpinmuebles    = count($tpinmuebles) - 1;
                for ($i = 0; $i < count($tpinmuebles); $i++) {
                    if ($i < $conttpinmuebles) {
                        $whereintpinmuebles .= $tpinmuebles[$i] . ',';
                    } else {
                        $whereintpinmuebles .= $tpinmuebles[$i];
                    }
                }
                $cond .= " AND i.IdTpInm IN ($whereintpinmuebles)";
            }
            if (!empty($barriolike)) {
                $cond .= " AND b.NombreB LIKE '%" . utf8_encode($barriolike) . "%'";
            }
            if (!empty($preciofrom) || !empty($precioto)) {
                if (!empty($preciofrom) && empty($precioto)) {
                    $cond .= " AND IF(IdGestion=1,ValorCanon,ValorVenta)  BETWEEN :preciofrom AND 10000000000";
                } elseif (empty($preciofrom) && !empty($precioto)) {
                    $cond .= " AND IF(IdGestion=1,ValorCanon,ValorVenta)  BETWEEN 0 AND :precioto";
                } elseif (!empty($preciofrom) && !empty($precioto)) {
                    $cond .= " AND IF(IdGestion=1,ValorCanon,ValorVenta)  BETWEEN :preciofrom AND :precioto";
                }
            }
            if (!empty($areafrom) || !empty($areato)) {
                if (!empty($areafrom) && empty($areato)) {
                    $cond .= " AND i.AreaConstruida BETWEEN :areafrom AND 100000";
                } elseif (empty($areafrom) && !empty($areato)) {
                    $cond .= " AND i.AreaConstruida BETWEEN 0 AND :areato";
                } elseif (!empty($areafrom) && !empty($areato)) {
                    $cond .= " AND i.AreaConstruida BETWEEN :areafrom AND :areato";
                }
            }
            if (!empty($cllfrom) || !empty($cllto)) {
                if (!empty($cllfrom) && empty($cllto)) {
                    $cond .= " AND i.Calle BETWEEN :cllfrom AND 100000";
                } elseif (empty($cllfrom) && !empty($cllto)) {
                    $cond .= " AND i.Calle BETWEEN 0 AND :cllto";
                } elseif (!empty($cllfrom) && !empty($cllto)) {
                    $cond .= " AND i.Calle BETWEEN :cllfrom AND :cllto";
                }
            }
            if (!empty($crafrom) || !empty($crato)) {
                if (!empty($crafrom) && empty($crato)) {
                    $cond .= " AND i.Carrera BETWEEN :crafrom AND 100000";
                } elseif (empty($crafrom) && !empty($crato)) {
                    $cond .= " AND i.Carrera BETWEEN 0 AND :crato";
                } elseif (!empty($crafrom) && !empty($crato)) {
                    $cond .= " AND i.Carrera BETWEEN :crafrom AND :crato";
                }
            }
            if (!empty($habitaciones) && $habitaciones != '0') {
                $cond .= " AND i.alcobas = :alcobas";
            }
            if (!empty($banios) && $banios != '0') {
                $cond .= " AND i.banos = :banios";
            }
            if (!empty($parqueadero) && $parqueadero != '0') {
                $cond .= " AND i.garaje = :parqueadero";
            }
            if (!empty($estrato) && $estrato != '0') {
                $cond .= " AND i.Estrato = :estrato";
            }
            if (!empty($ascensor) && $ascensor == '1') {
                $tabla = ' , detalleinmueble d';
                $rel   = "and i.codinm=d.codinmdet and i.IdInmobiliaria=d.inmob and idcaracteristica in (72,177,265,1016,1290,1291,1292,1293,1294,1379)";
            } elseif ($ascensor > '1') {
                $condcuenta = "";
                $tabla      = ' , detalleinmueble d';
                $rel        = "and i.codinm=d.codinmdet and i.IdInmobiliaria=d.inmob and idcaracteristica not in (72,177,265,1016,1290,1291,1292,1293,1294,1379) ";
            }
            if (!empty($propietario) && $propietario != '0') {
                $tablap = ' , inmueblesterceros dp';
                $relp   = "and i.codinm=dp.idinm_tr and i.IdInmobiliaria=dp.inmob_tr and rol_tr=1 and dp.iduser_tr=:propietario";
                //$grp=" group by i.codinm,i.IdInmobiliaria";
            }
            if (!empty($direccion)) {
                $cond .= " AND i.Direccion  like :direccion";
            }
            if (!empty($busquedarapida)) {

                $tablasotras .= ",maestrodecaracteristicas m,detalleinmueble ddd";
                $campootras = ",m.Descripcion";

                $cad               = trim($busquedarapida);
                $cantidad_palabras = sizeof(explode("+", $cad));

                for ($i = 0; $i < $cantidad_palabras; $i++) {
                    $conotras .= " AND ddd.`codinmdet`=i.codinm AND ddd.`inmob`=i.`IdInmobiliaria`
                    and m.idCaracteristica=ddd.idcaracteristica AND   (descripcionlarga like :rapida" . $i . "
                       or m.Descripcion like :rapida1" . $i . ")";
                }
            }
            if ($politicas == 2) {
                $cond .= " AND i.politica_comp <> '' ";
            }
            if ($destinacion != 0) {
                $cond .= " AND i.IdDestinacion = :destinacion";
            }

        }

        // echo "<pre>";
        // print_r($cond);
        // echo "</pre>";die;
        $aColumns = array("i.idInm", "i.IdGestion", "i.IdTpInm", "i.ValorCanon", "i.ValorVenta", "i.AreaConstruida", "c.Nombre", "i.Administracion", "i.Direccion", "i.IdBarrio", "i.Estrato", "i.idEstadoinmueble", "i.banos", "i.alcobas", "i.garaje", "b.NombreB", "z.NombreZ");


        $columnsSearch = array("i.idInm", "i.IdTpInm", "i.IdGestion", "i.ValorCanon", "i.ValorVenta", "i.AreaConstruida", "c.Nombre", "i.Direccion", "b.NombreB", "z.NombreZ", "i.Estrato", "i.Administracion", "i.alcobas", "i.banos");

        if ($hiddenCodDemanda > 0 ) {
            $columnsSearch = array("i.idInm","","", "i.IdGestion", "i.ValorCanon", "i.ValorVenta", "i.AreaConstruida", "c.Nombre", "i.Direccion", "b.NombreB", "z.NombreZ", "i.Estrato", "i.Administracion", "i.alcobas", "i.banos");
            if ($get['order'][0]['column'] != 1 && $get['order'][0]['column'] != 2) {
                $orderBy = ' ORDER BY ' . $columnsSearch[$get['order'][0]['column']] . ' ' . $get['order'][0]['dir'];
            }
        }else{
            $orderBy = ' ORDER BY ' . $columnsSearch[$get['order'][0]['column']] . ' ' . $get['order'][0]['dir'];
        }
        $sWhere = "";


        

        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($get['search']) && $get["search"]["value"] != '') {
                if ($sWhere == "") {
                    $sWhere = " AND (";
                } else {
                    $sWhere .= " OR ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($get["search"]["value"]) . "%' ";
            }
        }

        $sLimit = " LIMIT 0,10 ";
        if (isset($get['start']) && $get['length'] != '-1') {
            $sLimit = " LIMIT " . intval($get['start']) . ", " .
            intval($get['length']);
        }

        if ($sWhere != "") {

            $sWhere .= $cond;
            $sWhere .= ")";
        } else {
            $sWhere .= $cond;
        }
        $condInmobiliaria = "";
        if ($verRedes <= 0) {
            $condInmobiliaria = " AND i.IdInmobiliaria = :IdInmobiliaria";
        }else{
            $condInmobiliaria = " AND i.IdInmobiliaria NOT IN (".$_SESSION['IdInmmo'].")";
        }

        $sql = "SELECT i.idInm,i.codinm,i.IdInmobiliaria,i.IdGestion,i.IdTpInm,tp.Descripcion as tipoinmueble,i.ValorCanon as Canon,
                                i.AreaConstruida, i.AreaLote,  i.Administracion,  i.Direccion, i.IdBarrio,
                                i.ValorVenta as Venta, i.Estrato,i.idEstadoinmueble,i.banos,i.alcobas,i.garaje,
                                c.Nombre as Ciudad
                                FROM  inmnvo as i $formbarrio $tabla $tablap $tabladet $tablasotras
                                WHERE i.idEstadoinmueble in (2,4)
                                and i.IdGestion>0
                                and i.IdDestinacion>0
                                and i.IdTpInm>0
                                and (i.ValorCanon+i.ValorVenta)>0
                                and i.IdBarrio>0
                                $condInmobiliaria $sWhere  $rel $relp $conotras $grp GROUP BY idInm $orderBy $sLimit  ";

        // echo "<pre>";
        // print_r($sql);
        // echo "</pre>";die;

        $stmt = $this->connPDO->prepare($sql);
        if ($verRedes <= 0) {
            $stmt->bindParam(':IdInmobiliaria', $get['a']);
        }
        if (!empty($codinm)) {
            // if (strpos($codinm,'%2C')) {
            //     $stmt->bindParam(':condicionalIn', $condin);
            // }else{
            //     $stmt->bindParam(':condicionalIn', $codinm);
            // }
        } else {
            if (empty($tpinmuebles)) {
                if (!empty($tipinm)) {
                    $stmt->bindParam(':tipinm', $tipinm);
                }
            }
            if (!empty($gestion) && $gestion != '0') {
                $stmt->bindParam(':gestion', $gestion);
            }
            if (!empty($city) && $city != '0') {
                $stmt->bindParam(':city', $city);
            }
            // if (!empty($zona) && $zona != '0') {
            //     $stmt->bindParam(':zona', $zona);
            // }
            // if (!empty($barrio) && $barrio != '0') {
            //     $stmt->bindParam(':barrio', $barrio);
            // }
            if (!empty($preciofrom) || !empty($precioto)) {
                if (!empty($preciofrom) && empty($precioto)) {
                    $stmt->bindParam(':preciofrom', $preciofrom);
                } elseif (empty($preciofrom) && !empty($precioto)) {
                    $stmt->bindParam(':precioto', $precioto);
                } elseif (!empty($preciofrom) && !empty($precioto)) {
                    $stmt->bindParam(':preciofrom', $preciofrom);
                    $stmt->bindParam(':precioto', $precioto);
                }
            }
            if (!empty($areafrom) || !empty($areato)) {
                if (!empty($areafrom) && empty($areato)) {
                    $stmt->bindParam(':areafrom', $areafrom);
                } elseif (empty($areafrom) && !empty($areato)) {
                    $stmt->bindParam(':areato', $areato);
                } elseif (!empty($areafrom) && !empty($areato)) {
                    $stmt->bindParam(':areafrom', $areafrom);
                    $stmt->bindParam(':areato', $areato);
                }
            }
            if (!empty($cllfrom) || !empty($cllto)) {
                if (!empty($cllfrom) && empty($cllto)) {
                    $stmt->bindParam(':cllfrom', $cllfrom);
                } elseif (empty($cllfrom) && !empty($cllto)) {
                    $stmt->bindParam(':cllto', $cllto);
                } elseif (!empty($cllfrom) && !empty($cllto)) {
                    $stmt->bindParam(':cllfrom', $cllfrom);
                    $stmt->bindParam(':cllto', $cllto);
                }
            }
            if (!empty($crafrom) || !empty($crato)) {
                if (!empty($crafrom) && empty($crato)) {
                    $stmt->bindParam(':crafrom', $crafrom);
                } elseif (empty($crafrom) && !empty($crato)) {
                    $stmt->bindParam(':crato', $crato);
                } elseif (!empty($crafrom) && !empty($crato)) {
                    $stmt->bindParam(':crafrom', $crafrom);
                    $stmt->bindParam(':crato', $crato);
                }
            }
            if (!empty($habitaciones) && $habitaciones != '0') {
                $stmt->bindParam(':alcobas', $habitaciones);
            }
            if (!empty($banios) && $banios != '0') {
                $stmt->bindParam(':banios', $banios);
            }
            if (!empty($parqueadero) && $parqueadero != '0') {
                $stmt->bindParam(':parqueadero', $parqueadero);
            }
            if (!empty($estrato) && $estrato != '0') {
                $stmt->bindParam(':estrato', $estrato);
            }
            if (!empty($propietario) && $propietario != '0') {
                $stmt->bindParam(':propietario', $propietario);
            }
            if (!empty($direccion)) {
                $valueTerm = '%' . utf8_decode($direccion) . '%';
                $stmt->bindParam(':direccion', $valueTerm, PDO::PARAM_STR);
            }
            if (!empty($busquedarapida)) {
                $valueTerm         = '%' . utf8_decode($busquedarapida) . '%';
                $cad               = trim($busquedarapida);
                $cantidad_palabras = sizeof(explode("+", $cad));
                $separar           = explode("+", $cad);
                for ($i = 0; $i < $cantidad_palabras; $i++) {
                    $partesb   = $separar[$i];
                    $valueTerm = '%' . utf8_decode($partesb) . '%';

                    $stmt->bindParam(':rapida' . $i, $valueTerm, PDO::PARAM_STR);
                    $stmt->bindParam(':rapida1' . $i, $valueTerm, PDO::PARAM_STR);

                    // echo "<pre>";
                    // echo ':rapida'.$i. $valueTerm;
                    // echo "</pre>";

                }

            }
            if ($destinacion != 0) {
                $stmt->bindParam(':destinacion', $destinacion);
            }
            // if (!empty($ascensor) && $ascensor != '0') {
            //     $stmt->bindParam(':ascensor', $ascensor);
            // }
        }

        // echo "SELECT i.idInm,i.IdGestion,i.idInm,i.IdTpInm,i.ValorCanon as Canon,
        //                         i.AreaConstruida,  i.Administracion,  i.Direccion, i.IdBarrio,
        //                         i.ValorVenta as Venta, i.Estrato,i.idEstadoinmueble,i.banos,i.alcobas,i.garaje
        //                         FROM  inmnvo as i $formbarrio
        //                         WHERE i.IdInmobiliaria = :IdInmobiliaria
        //                         and i.idEstadoinmueble in (2,4)
        //                         $cond";
        //                         exit;

        $cadena_inmuebles = "";
        $negociosA = new Negocios();
        if ($stmt->execute()) {
            $info = array();
            while ($row = $stmt->fetch()) {
                $reservado    = ($row['idEstadoinmueble'] == 4) ? 'Reservado' : '';
                $Barrioresult = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], 'NombreB');
                $idciudad     = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], 'IdCiudad');
                $idzona       = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], 'IdZona');
                // $idlocalidad   =getCampo('barrios',"where IdBarrios=".$row['IdBarrio'],'IdLocalidad');
                $ciudad     = getCampo('ciudad', "where IdCiudad=" . $idciudad, 'Nombre');
                $localidad  = getCampo('localidad', "where idLocalidad=" . $idlocalidad, 'descripcion');
                $zonaresult = getCampo('zonas', "where IdZona=" . $idzona, 'NombreZ');
                $cadena_inmuebles .= $row['idInm'] . '?';
                if ($row['idEstadoinmueble'] == '4') {
                    $starreservado = '<i style="color: #e0bd37" class="fa fa-star" aria-hidden="true"></i>';
                } else {
                    $starreservado = "";
                }

                $stars = $negociosA->buscarInmueblesBetaCallCruce($row['codinm'],$hiddenCodDemanda, $row['IdInmobiliaria']);

                $logDescarte = 0;

                if ($hiddenCodDemanda != '0') {
                    $veriLogDescarte = $this->getDescarteInmuebleByDemanda($row['idInm'],$hiddenCodDemanda);
                    if ($veriLogDescarte == 1) {
                        $logDescarte = 1;
                    }
                }
                if ($logDescarte == 0) {
                    $info[] = array(
                        "Codigo_Inmueble"  => $starreservado . ' ' . $row['idInm'] . '<input type="hidden" name="" id="input" class="form-control idinmtbl" value="' . $row['idInm'] . '" > ',
                        "IdTpInm"          => $row['IdTpInm'],
                        "Tipo_Inmueble"    => ucwords(strtolower(utf8_encode($row['tipoinmueble']))),
                        "IdGestion"        => $row['IdGestion'],
                        "Gestion"          => ucwords(strtolower(utf8_encode(getCampo('gestioncomer', "where IdGestion=" . $row['IdGestion'], 'NombresGestion')))),
                        "Canon"            => '$ ' . number_format($row['Canon'], 0, ',', '.'),
                        "Venta"            => '$ ' . number_format($row['Venta'], 0, ',', '.'),
                        "Administracion"   => $row['Administracion'],
                        "AreaConstruida"   => $row['AreaConstruida'],
                        "Ciudad"           => ucwords(strtolower(utf8_encode($ciudad))),
                        "Zona"             => ucwords(strtolower(utf8_encode($zonaresult))),
                        "IdBarrio"         => $row['IdBarrio'],
                        "Barrio"           => ucwords(strtolower(utf8_encode($Barrioresult))),
                        "Direccion"        => $row['Direccion'],
                        "Estrato"          => $row['Estrato'],
                        "alcobas"          => $row['alcobas'],
                        "banos"            => $row['banos'],
                        "garaje"           => $row['garaje'],
                        "idEstadoinmueble" => $row['idEstadoinmueble'],
                        "ciudad"           => ucfirst(strtolower($row['Ciudad'])),
                        "estado"           => ucwords(strtolower(utf8_encode(getCampo('estado_inmueble', "where idestado=" . $row['IdTpInm'], 'descripcion')))),
                        "reservado"        => $reservado,
                        "texto_cumple"     => utf8_encode($stars[0]['texto_cumple']),
                        "estrellas"        => utf8_encode($stars[0]['estrellas']),
                        "porcentaje"        => utf8_encode($stars[0]['porcentaje']),
                        "tiene_politicas"  => utf8_encode($stars[0]['tiene_politicas'])
                    );
                }
            }
            if ($hiddenCodDemanda > 0 ) {
                if ($get['order'][0]['column'] == 1) {
                    foreach ($info as $key => $row) {
                        $aux[$key] = $row['porcentaje'];
                    }
                    if ($get['order'][0]['dir'] == 'asc') {
                        array_multisort($aux, SORT_ASC, $info);
                    }else{
                        array_multisort($aux, SORT_DESC, $info);
                    }
                    
                }
            }
            // foreach ($info as $key => $row) {
            //     $aux[$key] = $row['porcentaje'];
            // }
            // array_multisort($aux, SORT_DESC, $info);
            //     echo "<pre>";
            //     print_r($info);
            //     echo "</pre>";

            foreach ($info as $key => $value) {
                $info[$key]['banos'] = $info[$key]['banos'] . '<input type="hidden" name="" id="input" class="form-control seleccionados" value="' . substr($cadena_inmuebles, 0, -1) . '" >';
            }
            /* Total data set length */

            $Ssql = "SELECT i.idInm
                FROM  inmnvo as i $formbarrio $tabla $tablap $tabladet $tablasotras
                WHERE i.idEstadoinmueble in (2,4)
                and i.IdGestion>0
                and i.IdDestinacion>0
                and i.IdTpInm>0
                and (i.ValorCanon+i.ValorVenta)>0
                and i.IdBarrio>0
                $condInmobiliaria $sWhere  $rel $relp $conotras  $grp GROUP BY idInm ";
            // echo '<pre>';var_dump($Ssql);die;
            $stmtcount = $this->connPDO->prepare($Ssql);
            if ($verRedes <= 0) {
                $stmtcount->bindParam(':IdInmobiliaria', $get['a']);
            }

            $countparam = 1;
            if (!empty($codinm)) {
                // if (strpos($codinm,'%2C')) {
                //        $stmtcount->bindParam(':condicionalIn', $condin);
                //    }else{
                //        $stmtcount->bindParam(':condicionalIn', $codinm);
                //    }
            } else {
                if (empty($tpinmuebles)) {
                    if (!empty($tipinm)) {
                        $countparam++;
                        $stmtcount->bindParam(':tipinm', $tipinm);
                    }
                }
                if (!empty($gestion) && $gestion != 0) {
                    $countparam++;
                    $stmtcount->bindParam(':gestion', $gestion);
                }
                if (!empty($city) && $city != 0) {
                    $countparam++;
                    $stmtcount->bindParam(':city', $city);
                }
                //    if (!empty($zona) && $zona != '0') {
                // $countparam++;
                // // echo 'zona ' . $zona;
                //        $stmtcount->bindParam(':zona', $zona);
                //    }
                //    if (!empty($barrio) && $barrio != 0) {
                // $countparam++;
                //        $stmtcount->bindParam(':barrio', $barrio);
                //    }
                if (!empty($preciofrom) || !empty($precioto)) {
                    if (!empty($preciofrom) && empty($precioto)) {
                        $stmtcount->bindParam(':preciofrom', $preciofrom);
                    } elseif (empty($preciofrom) && !empty($precioto)) {
                        $stmtcount->bindParam(':precioto', $precioto);
                    } elseif (!empty($preciofrom) && !empty($precioto)) {
                        $stmtcount->bindParam(':preciofrom', $preciofrom);
                        $stmtcount->bindParam(':precioto', $precioto);
                    }
                }
                if (!empty($areafrom) || !empty($areato)) {
                    if (!empty($areafrom) && empty($areato)) {
                        $stmtcount->bindParam(':areafrom', $areafrom);
                    } elseif (empty($areafrom) && !empty($areato)) {
                        $stmtcount->bindParam(':areato', $areato);
                    } elseif (!empty($areafrom) && !empty($areato)) {
                        $stmtcount->bindParam(':areafrom', $areafrom);
                        $stmtcount->bindParam(':areato', $areato);
                    }
                }
                if (!empty($cllfrom) || !empty($cllto)) {
                    if (!empty($cllfrom) && empty($cllto)) {
                        $stmtcount->bindParam(':cllfrom', $cllfrom);
                    } elseif (empty($cllfrom) && !empty($cllto)) {
                        $stmtcount->bindParam(':cllto', $cllto);
                    } elseif (!empty($cllfrom) && !empty($cllto)) {
                        $stmtcount->bindParam(':cllfrom', $cllfrom);
                        $stmtcount->bindParam(':cllto', $cllto);
                    }
                }
                if (!empty($crafrom) || !empty($crato)) {
                    if (!empty($crafrom) && empty($crato)) {
                        $stmtcount->bindParam(':crafrom', $crafrom);
                    } elseif (empty($crafrom) && !empty($crato)) {
                        $stmtcount->bindParam(':crato', $crato);
                    } elseif (!empty($crafrom) && !empty($crato)) {
                        $stmtcount->bindParam(':crafrom', $crafrom);
                        $stmtcount->bindParam(':crato', $crato);
                    }
                }
                if (!empty($habitaciones) && $habitaciones != 0) {
                    $countparam++;
                    $stmtcount->bindParam(':alcobas', $habitaciones);
                }
                if (!empty($banios) && $banios != 0) {
                    $countparam++;
                    $stmtcount->bindParam(':banios', $banios);
                }
                if (!empty($parqueadero) && $parqueadero != 0) {
                    $countparam++;
                    $stmtcount->bindParam(':parqueadero', $parqueadero);
                }
                if (!empty($estrato) && $estrato != 0) {
                    $countparam++;
                    $stmtcount->bindParam(':estrato', $estrato);
                }
                if (!empty($propietario) && $propietario != '0') {
                    $stmtcount->bindParam(':propietario', $propietario);
                }
                if (!empty($direccion)) {
                    $countparam++;
                    $valueTerm = '%' . utf8_decode($direccion) . '%';
                    $stmtcount->bindParam(':direccion', $valueTerm, PDO::PARAM_STR);
                }
                if (!empty($busquedarapida)) {

                    $cad               = trim($busquedarapida);
                    $cantidad_palabras = sizeof(explode("+", $cad));
                    $separar           = explode("+", $cad);
                    for ($i = 0; $i < $cantidad_palabras; $i++) {
                        $partesb = $separar[$i];

                        $valueTerm = '%' . utf8_decode($partesb) . '%';

                        $stmtcount->bindParam(':rapida' . $i, $valueTerm, PDO::PARAM_STR);
                        $stmtcount->bindParam(':rapida1' . $i, $valueTerm, PDO::PARAM_STR);
                    }

                }
                if ($destinacion != 0) {
                    $stmtcount->bindParam(':destinacion', $destinacion);
                }
            }
            // echo "<pre>";
            // print_r($countparam);
            // echo "</pre>";die;
            $stmtcount->execute();
            $rResultTotal         = $stmtcount->fetchAll();
            $iTotal               = $stmtcount->rowCount(); //$rResultTotal[0]["total"];
            $_SESSION['RtblCall'] = $iTotal;
            if (isset($get['similares']) && $get['similares'] == '1') {
                $_SESSION['RSimilarestblCall'] = $iTotal;
            }
            // echo "<pre>";
            // print_r($rResultTotal);
            // echo "</pre>";die;
            $output = array(
                "sEcho"                => intval($data['sEcho']),
                "iTotalRecords"        => $iTotal,
                "iTotalDisplayRecords" => $iTotal, //$iFilteredTotal[0],
                "aaData"               => $info,
            );
            return $output;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }

    private function getDescarteInmuebleByDemanda($idinm,$coddemanda){
        $sql = "SELECT * FROM descarta_demandas WHERE usr_dem = :user AND inmu_dem = :idinm AND cod_dem = :coddemanda LIMIT 0,1";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':user', $_SESSION['Id_Usuarios']);
        $stmt->bindParam(':idinm', $idinm);
        $stmt->bindParam(':coddemanda', $coddemanda);

        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                return 1;
            }else{
                return 2;
            }
        }

    }

    private function strremplace($str)
    {
        $str = trim($str);

        $str = str_replace(array('Á', 'É', 'Í', 'Ó', 'Ú'), array('á', 'é', 'í', 'ó', 'ú'), $str);
        $str = str_replace(array('Ñ'), array('ñ'), $str);
        return $str;
    }

    private function getCaracteristicas($idinm)
    {

        $result = array();

        $sql = "SELECT  g.Detalle,m.descripcion,m.Cantidad,m.IdGrupo,d.obser_det
                FROM maestrodecaracteristicas m
                INNER JOIN detalleinmueble d ON m.idcaracteristica=d.idcaracteristica
                and d.idinmueble = :idInm
                INNER JOIN grupocaracteristicas g ON m.IdGrupo=g.IdGrupo
                order by g.Detalle asc";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':idInm', $idinm);

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $cn++;
                $det       = str_replace("Ñ", "ñ", ucwords(strtolower(utf8_encode($row['Detalle']))));
                $cara      = str_replace("Ñ", "ñ", ucwords(strtolower(utf8_encode($row['descripcion']))));
                $cant      = $row['Cantidad'];
                $IdGrupo   = $row['IdGrupo'];
                $obser_det = utf8_encode($row['obser_det']);
                if ($IdGrupo == 24 or $IdGrupo == 23 or $IdGrupo == 35 or $IdGrupo == 9 or $IdGrupo == 17 or $IdGrupo == 13 or $IdGrupo == 27) {
                    $car = "$det $cara $obser_det ";
                } else {
                    $car = "$cara $obser_det ";
                }
                $result[] = array(
                    'idCaracteristica' => $row['IdGrupo'],
                    'descripcion'      => $car,
                );
            }

            return $result;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }

    public function detail_inmueble($id)
    {

        list($aa, $codinm) = explode("-", $id);
        $sql               = "SELECT inv.idInm, inv.codinm, inv.IdInmobiliaria, inv.IdGestion, inv.IdTpInm, inv.IdBarrio, inv.banos, inv.alcobas, inv.garaje, inv.ValorVenta, inv.ValorCanon, inv.AreaConstruida, inv.AreaLote, inv.descripcionlarga, inv.latitud, inv.longitud, br.IdLocalidad, br.IdZona, br.IdCiudad, inv.IdBarrio, inv.Estrato, inv.ValorIva, inv.Administracion
        FROM inmnvo inv, barrios br
        WHERE codinm        = :idinm
        and IdInmobiliaria =:aa
        AND br.IdBarrios = inv.IdBarrio";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':idinm', $codinm);
        $stmt->bindParam(':aa', $aa);

        if ($stmt->execute()) {

            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            $result['ValorIva']       = number_format($result['ValorIva'], 0, ',', '.');
            $result['Administracion'] = number_format($result['Administracion'], 0, ',', '.');

            $result['descripcionlarga'] = $this->strremplace(utf8_encode($result['descripcionlarga']));

            /*
            Caracteristicas
             */

            $postCaracteristicas = array();

            $postCaracteristicas['IdInmobiliaria'] = $aa;
            $postCaracteristicas['codinm']         = $codinm;
            $postCaracteristicas['tpcar']          = 1;

            $result['caracteristicasInternas'] = $this->getDataDetalleInmuebleFull($postCaracteristicas);

            $postCaracteristicas['tpcar'] = 2;

            $result['caracteristicasExternas'] = $this->getDataDetalleInmuebleFull($postCaracteristicas);

            $postCaracteristicas['tpcar'] = 3;

            $result['caracteristicasAlrededores'] = $this->getDataDetalleInmuebleFull($postCaracteristicas);

            $result['othercaracteristicas'] = $this->getOtherCaracteristicas($id);

            /*
            Datos Propietario/Apoderado
             */

            $postPropietario = array();

            $postPropietario['IdInmobiliaria'] = $aa;
            $postPropietario['codinm']         = $codinm;
            $postPropietario['rol_tr']         = 1;

            $result['propietario'] = $this->datosInmuebleTerceros($postPropietario);

            $postPropietario['rol_tr'] = 2;

            $result['apoderado'] = $this->datosInmuebleTerceros($postPropietario);

            $postPropietario['rol_tr'] = 3;

            $result['asesor'] = $this->datosInmuebleTerceros($postPropietario);

            $postPropietario['rol_tr'] = 4;

            $result['captador'] = $this->datosInmuebleTerceros($postPropietario);

            $destinacion = getCampo('destinacion', "where Iddestinacion=" . $result['othercaracteristicas']['IdDestinacion'], "CONCAT(UPPER(LEFT(Nombre, 1)), LOWER(SUBSTRING(Nombre, 2)))", 0);

            $result['othercaracteristicas']['destinacion'] = !empty($destinacion) ? $destinacion : "";

            $procedencia = getCampo('procedencia', "where IdProcedencia=" . $result['othercaracteristicas']['IdProcedencia'], "CONCAT(UPPER(LEFT(Nombre, 1)), LOWER(SUBSTRING(Nombre, 2)))", 0);

            $result['othercaracteristicas']['procedencia'] = !empty($procedencia) ? $procedencia : "";

            $result['othercaracteristicas']['AvaluoCatastral'] = '$ ' . number_format($result['othercaracteristicas']['AvaluoCatastral'], 0, ',', '.');

            $result['othercaracteristicas']['EdadInmueble'] = number_format($result['othercaracteristicas']['EdadInmueble'], 0, ',', '.');

            $result['reserva']['idgestion'] = $this->getIdGestion($id);

            $result['reserva']['idEstadoInmueble'] = $this->getEstadoInm($id);

            if ($result['reserva']['idEstadoInmueble'] == 4) {
                $idpromreserva = getCampo('reservainmueble', "where IdEstadoReserva=1 and Inmueble='" . $id . "'", 'IdPromotor');
                $nomprom       = ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='$idpromreserva'", 'concat(Nombres," ",apellidos)')));
                $freserva      = getCampo('reservainmueble', "where IdEstadoReserva=1 and Inmueble='" . $id . "'", 'Fecha');

                $result['reserva']['nomprom'] = $nomprom;

                $result['reserva']['freserva'] = $freserva;
            }

            $result['reserva']['detailEstado'] = $this->getDetailEstado($id);

            $result['reserva']['detailEstado']['hoy'] = date('Y-m-d');

            $result['reserva']['detailEstado']['promotor'] = $_SESSION['Nombres'];

            $auth = $this->getAuthInm();

            $result['reserva']['auth'] = !empty($auth) ? $auth : 0;

            $result['politicas'] = $this->getPoliticas($id);

            $result['fichatecnica'] = getCampo('clientessimi', "where IdInmobiliaria=" . $_SESSION['IdInmmo'], "fichatec");

            $result['compartirinm'] = $this->getCompartirInm($id);

            $result['logpoliticas'] = $this->getLogPoliticas();

            $result['datoseditarcliente'] = $this->getClienteCall();
            $urlVideo                     = getCampo('detalleinmueble d, maestrodecaracteristicasnvo m', "where d.`idcaracteristica`=m.`idCaracteristica` AND m.`IdGrupo`=51 and d.inmob=" . $_SESSION['IdInmmo'] . " and d.codinmdet=$codinm", 'obser_det');
            $result['video']              = "";
            if (strstr($urlVideo, 'watch')) {
                list($urlValida, $parte) = explode('&', $urlVideo);
                $result['video']         = utf8_encode(str_replace('watch?v=', 'embed/', $urlValida));
            } elseif (strstr($urlVideo, 'youtu.be')) {
                $result['video'] = utf8_encode(str_replace('youtu.be', 'youtube.com/embed/', $urlVideo));
            }
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        return $result;
        $stmt = null;
    }

    public function getCompartirInm($idinm){
        $sql = "SELECT * FROM condiciones_compartir_inm WHERE id_inmu_cm = :idinm AND usu_cm = :usu_cm";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':idinm', $idinm);
        $stmt->bindParam(':usu_cm', $_SESSION['Id_Usuarios']);

        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                return array(
                    'status' => 'Ok',
                    'estado' => ucwords(strtolower(getCampo('parametros',' WHERE id_param = 22 AND conse_param = ' . $result['est_cm'], 'desc_param')))
                );
            }else{
                return 2;
            }
        }
    }

    public function getFotos($idinm)
    {
        $result = array();

        list($aa, $codinm) = explode("-", $idinm);
        $sql               = "SELECT foto
        FROM fotos_nvo
        WHERE codinm_fto = :idinm
        and aa_fto       =:aa";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':idinm', $codinm);
        $stmt->bindParam(':aa', $aa);

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $result[] = array(
                    'foto' => $row['foto'],
                );
            }
            return $result;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }

    private function getOtherCaracteristicas($idinm)
    {
        $result            = array();
        list($aa, $codinm) = explode("-", $idinm);
        $sql               = "SELECT IdGestion, FConsignacion, Estrato, AreaConstruida, AreaLote, EdadInmueble, NoMatricula, chip, AvaluoCatastral, CedulaCatastral, IdDestinacion, restricciones, IdProcedencia, obserinterna
                FROM inmnvo
                WHERE codinm       = :idinm
                and IdInmobiliaria =:aa";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':idinm', $codinm);
        $stmt->bindParam(':aa', $aa);

        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return $result;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }

    private function getIdGestion($idinm)
    {
        $result            = array();
        list($aa, $codinm) = explode("-", $idinm);
        $sql               = "SELECT IdGestion
                FROM inmnvo
                WHERE codinm       = :idinm
                and IdInmobiliaria =:aa";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':idinm', $codinm);
        $stmt->bindParam(':aa', $aa);

        if ($stmt->execute()) {
            $result    = $stmt->fetch(PDO::FETCH_ASSOC);
            $idgestion = $result['IdGestion'];
            return $idgestion;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }

    private function getEstadoInm($idinm)
    {
        $result = array();
        $sql    = "SELECT idEstadoInmueble FROM inmnvo WHERE idInm = :idInm";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':idInm', $idinm);

        if ($stmt->execute()) {
            $result           = $stmt->fetch(PDO::FETCH_ASSOC);
            $idEstadoInmueble = $result['idEstadoInmueble'];
            return $idEstadoInmueble;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }

    private function getDetailEstado($idinm)
    {
        $result = array();
        $sql    = "SELECT e.descripcion, e.idestado
        FROM inmnvo i
        LEFT JOIN estado_inmueble e ON i.idEstadoinmueble = e.idestado
        WHERE i.idInm LIKE :idInm
        AND i.IdInmobiliaria = :inmo AND
        i.idEstadoinmueble = '2'
        AND i.IdGestion IN (1,2)";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':inmo', $_SESSION['IdInmmo']);
        $stmt->bindParam(':idInm', $idinm);

        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return $result;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }

    private function getAuthInm()
    {
        $result = array();
        $sql    = "SELECT sj_acc_pro
        FROM sj_acces
        WHERE sj_acc_pro in ('14','15')
        and sj_acc_usr = :usuario";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':usuario', $_SESSION['Id_Usuarios']);

        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return $result;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }

    private function getPoliticas($idinm)
    {
        $result = array();
        $sql    = "select ComiVenta, ComiArren, ValComiVenta, ValComiArr, politica_comp from inmnvo where idInm = :inm";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':inm', $idinm);

        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            $data   = array(
                'porcventa'       => $result['ComiVenta'] * 100,
                'valorcomiventa'  => number_format($result['ValComiVenta'], 0, ',', '.'),
                'porcarrien'      => $result['ComiArren'] * 100,
                'valorcomiarrien' => number_format($result['ValComiArr'], 0, ',', '.'),
                'politics'        => ucfirst(strtolower(utf8_encode($result['politica_comp']))),
            );
            return $data;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }

    public function crearReserva($data)
    {
        $fecharetornar = strtotime($data['dateretornar']);
        $fechareserva  = strtotime($data['datereserv']);
        $f_act         = date('Y-m-d');
        $hor1          = date('H:i:s');
        $hor1          = date('H');
        if ($hor1 + 7 > 24) {
            $hor = ($hor1 + 7) - 24;
        } else {
            $hor = $hor1 + 7;
        }
        $min = date('i');
        $seg = date('s');

        $h_act = "$hor:$min:$seg";
        if ($fecharetornar < $fechareserva) {
            $data = array(
                'status' => 'Error',
                'msg'    => 'Debe seleccionar una Fecha mayor a la fecha de Reserva',
            );
        } else if ($fecharetornar >= $fechareserva) {
            $comments  = strtoupper($data['comments']);
            $sqlupdate = "update inmnvo set idEstadoinmueble=4 where idInm = :inm";
            $stmt      = $this->connPDO->prepare($sqlupdate);
            $stmt->bindParam(':inm', $data['inm']);
            if ($stmt->execute()) {
                $sqlinsert = "Insert Into reservainmueble (Inmueble,Fecha,IdPromotor,NroSolicitud,Cedula,FechaRetornar,Notas,IdAseguradora,IdEstadoRetro,IdEstadoReserva,f_sis,h_sis) values('" . $data['inm'] . "', '" . $data['datereserv'] . "', '" . $_SESSION['Id_Usuarios'] . "', '" . $data['numsolicitud'] . "', '" . $data['cedula'] . "', '" . $data['dateretornar'] . "', '$comments', '" . $data['aseguradora'] . "','0','1','$f_act','h_act')";
                $stmt2     = $this->connPDO->prepare($sqlinsert);

                if ($stmt2->execute()) {
                    CallCenter::updateReservaInmueblesOld($data['inm']);
                    $idpromreserva = getCampo('reservainmueble', "where IdEstadoReserva=1 and Inmueble='" . $data['inm'] . "'", 'IdPromotor');
                    $nomprom       = ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='$idpromreserva'", 'concat(Nombres," ",apellidos)')));
                    $freserva      = getCampo('reservainmueble', "where IdEstadoReserva=1 and Inmueble='" . $data['inm'] . "'", 'Fecha');
                    $data          = array(
                        'status'   => 'Ok',
                        'msg'      => 'Se ha Registrado la Reserva del Inmueble',
                        'nomprom'  => $nomprom,
                        'freserva' => $freserva,
                    );
                } else {
                    $data = array('status' => 'Error', 'msg' => print_r($stmt->errorInfo()) . " error");
                    // return print_r($stmt->errorInfo())." error";
                }
            } else {
                $data = array('status' => 'Error', 'msg' => print_r($stmt->errorInfo()) . " error");
                // return print_r($stmt->errorInfo())." error";
            }
        }
        return $data;
    }

    public function getClienteLlamada($data)
    {

        if ($data['ced_cli']) {
            $_SESSION['clicedula'] = $data['ced_cli'];
        }
        if (empty($_SESSION['clicedula'])) {
            $data = array(
                'status' => 'Error',
                'msg'    => 'PARA ALMACENAR LLAMADA PRIMERO DEBE REGISTRAR  EL NOMBRE DEL CLIENTE',
            );
        } else {
            $cc  = trim(str_replace(" ", "", $_SESSION['clicedula']));
            $sql = "SELECT nombre,direccion,telfijo,telcelular,email ,ase_cli
                FROM clientes_inmobiliaria
                WHERE cedula=:cc
                and inmobiliaria=:inmobiliaria
                limit 0,100";
            $stmt = $this->connPDO->prepare($sql);
            $stmt->bindParam(':cc', $cc);
            $stmt->bindParam(':inmobiliaria', $_SESSION['IdInmmo']);
            if ($stmt->execute()) {
                $cliente                 = $stmt->fetch(PDO::FETCH_ASSOC);
                $nombre                  = $cliente['nombre'];
                $dire                    = $cliente['direccion'];
                $telf                    = $cliente['telfijo'];
                $telcel                  = $cliente['telcelular'];
                $email                   = $cliente['email'];
                $ase_cli                 = $cliente['ase_cli'];
                $asesor                  = getCampo('usuarios', 'where Id_Usuarios = "' . $ase_cli . '"', 'iduser');
                $_SESSION['asesor_call'] = empty($asesor) ? $ase_cli : $asesor;
                $data                    = array(
                    'status'    => 'Ok',
                    'Nombre'    => $nombre,
                    'Direccion' => $dire,
                    'Telefono'  => $telf,
                    'Celular'   => $telcel,
                    'Correo'    => $email,
                    'Asesor'    => empty($asesor) ? $ase_cli : $asesor,
                    'ced_cli'   => $data['ced_cli'],
                    'tipolla'   => $data['tipolla'],
                );
            } else {
                $data = array(
                    'status' => 'Error',
                    'msg'    => print_r($stmt->errorInfo()) . ' Error',
                );
            }
        }
        return $data;
    }

    public function cearLlamada($datapost, $datarequest, $phpmailer)
    {
        $flag      = 0;
        $fechaserv = date("Y-m-d");
        $hserv     = date('G:i:s');
        $idinmo    = $datarequest['inmueble'];
        $tipolla   = $datarequest['tipolla'];
        $cita      = $datarequest[''];
        $hoy       = date("Y-m-d");
        if ($datarequest['tipolla'] == 2) {
            if ($datarequest['concepto'] == 3 || $datarequest['concepto'] == 1) {
                $insert = "insert into cita (fechaprog,id_asunto,fechacita,horacita,horaprog,inmobiliaria,id_cliente,id_inmueble,qregistra,observaciones,id_captador,estado,Idconceptollamada) VALUES ('$fechaserv',8,'$hoy','$hserv','$hserv','" . $_SESSION['IdInmmo'] . "','" . $_SESSION['clicedula'] . "','" . $idinmo . "','" . $_SESSION['Id_Usuarios'] . "','" . $datarequest['gestion'] . "-" . $datarequest['oblla'] . "','" . $datarequest['Captador'] . "',1,'" . $datarequest['concepto'] . "')";
            } else {
                $insert = "insert into cita (fechaprog,id_asunto,fechacita,horacita,horaprog,inmobiliaria,id_cliente,id_inmueble,qregistra,observaciones,id_captador,estado,Idconceptollamada) VALUES ('$fechaserv',8,'$hoy','$hserv','$hserv','" . $_SESSION['IdInmmo'] . "','" . $_SESSION['clicedula'] . "','" . $idinmo . "','" . $_SESSION['Id_Usuarios'] . "','" . $datarequest['gestion'] . "-" . $datarequest['oblla'] . "','" . $datarequest['Captador'] . "',2,'" . $datarequest['concepto'] . "')";
            }
        } else {
            if ($datarequest['concepto'] == 3 || $datarequest['concepto'] == 1) {
                $insert = "insert into cita (fechaprog,id_asunto,fechacita,horacita,horaprog,inmobiliaria,id_cliente,qregistra,observaciones,estado,Idconceptollamada) VALUES ('$fechaserv',7,'$hoy','$hserv','$hserv','" . $_SESSION['IdInmmo'] . "','" . $_SESSION['clicedula'] . "','" . $_SESSION['Id_Usuarios'] . "','" . $datarequest['gestion'] . "-" . $datarequest['oblla'] . "',2,'" . $datarequest['concepto'] . "','2')";
            } else {
                $insert = "insert into cita (fechaprog,id_asunto,fechacita,horacita,horaprog,inmobiliaria,id_cliente,qregistra,observaciones,estado,Idconceptollamada) VALUES ('$fechaserv',7,'$hoy','$hserv','$hserv','" . $_SESSION['IdInmmo'] . "','" . $_SESSION['clicedula'] . "','" . $_SESSION['Id_Usuarios'] . "','" . $datarequest['gestion'] . "-" . $datarequest['oblla'] . "',2,'" . $datarequest['concepto'] . "')";
            }
        }

        $stmt = $this->connPDO->prepare($insert);

        if ($stmt->execute()) {
            // if ($datarequest['concepto'] == 3 || $datarequest['concepto'] == 1 || $datarequest['concepto'] == 5) {
            if ($datarequest['concepto'] == 1) {
                $con = 8;
            }
            if ($datarequest['concepto'] == 3) {
                $con = 9;
            }
            if ($datarequest['concepto'] != 5) {
                $idllama    = $this->connPDO->lastInsertId();
                $sqlinsert2 = "INSERT INTO retro_cita
                    (id_descripcion,resultado,idcita,IdCaptador,fecharetro,horaretro,IdUsuRetro)
                    VALUES
                    ('$con','" . $datarequest['oblla'] . "','$idllama','" . $datarequest['Captador'] . "','" . $hoy . "','" . $hserv . "','" . $_SESSION['Id_Usuarios'] . "')";
                //print $sqlinsert2;
            }
            $nom_cli         = $datapost['nom_cli'];
            $tel_cli         = $datapost['tel_cli'];
            $cel_cli         = $datapost['cel_cli'];
            $ema_cli         = $datapost['ema_cli'];
            $oblla           = $datapost['oblla'];
            $fecha_act       = date('Y-m-d');
            $inmueble        = $idinmo;
            $trae_nom_asesor = "Select Nombres,apellidos,Correo,Foto,Telefono,Celular
                                    from usuarios
                                    where iduser=:captador";
            $stmt2 = $this->connPDO->prepare($trae_nom_asesor);
            $stmt2->bindParam(':captador', $datarequest['Captador']);
            if ($stmt2->execute()) {
                $captador     = $stmt2->fetch(PDO::FETCH_ASSOC);
                $nombrea      = $captador['Nombres'];
                $ape_a        = $captador['apellidos'];
                $mail_ase     = $captador['Correo'];
                $tel_asesor   = $captador['Telefono'];
                $cel_asesor   = $captador['Celular'];
                $Foto         = $captador['Foto'];
                $nombreasesor = "$nombrea $ape_a";
                $trae_inmo    = "Select Nombre,IdInmobiliaria,logo
                                  from clientessimi
                                  where IdInmobiliaria=:inmo";
                $stmt3 = $this->connPDO->prepare($trae_inmo);
                $stmt3->bindParam(':inmo', $_SESSION['IdInmmo']);
                if ($stmt3->execute()) {
                    $formatoFicha = getCampo('clientessimi', "where IdInmobiliaria=" . $_SESSION['IdInmmo'], "fichatec");
                    $inmo         = $stmt3->fetch(PDO::FETCH_ASSOC);
                    $nombreaa     = $inmo['Nombre'];
                    $logo         = str_replace("..", "", $inmo['logo']);
                    $nombreasesor = "$nombrea $ape_a";
                    if ($datapost['mail_asesor']) {
                        //                     $cuerpo = "
                        // <html>
                        //     <head>
                        //        <title>Retroalimentación Citas</title>
                        //     </head>
                        //     <body>
                        //     <table border='1' bordercolor='#000066' align='center'>

                        //     <tr>

                        //         <td colspan='2'>
                        // <p align='center'>
                        // <img src='http://simiinmobiliarias.com/" . $logo . "' alt='INMO' width='100' height='50' />
                        // </p>
                        // Inmobiliaria " . $nombreaa . "<br>
                        // Nueva Llamada Inmueble $inmueble  <a target'_blank' href='http://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/FichaInmueble.php?reg=$inmueble'>Ver ficha</a><br>
                        // Asesor.(a): $nombreasesor<br>
                        // Se ha recibido una llamada para el inmueble " . $inmueble . "<br>
                        // Nombre del Cliente: " . $nom_cli . "<br>
                        // Número de Celular Cliente: " . $cel_cli . "<br>
                        // Número de Teléfono fijo Cliente: " . $tel_cli . "<br>
                        // Fecha Cita: " . $fecha_act . "<br>
                        // Asunto $asuntocita<br>
                        // Observaciones Cita: " . $oblla . "<br>

                        // <br>
                        // -----<br>
                        //         Cordialmente,<br>

                        //         El Equipo Comercial de " . $nombreaa . "<br>

                        //         <br>
                        //         <a  target='_blank' href='http://www.siminmueble.com/'>© Todos los derechos Reservados Tecnología de Administración Empresarial LTDA.</a>
                        //          </p>
                        //          </td>
                        //     </tr>
                        //     </table>

                        //      ";
                        $cuerpo = '<html>
                                        <head>
                                            <meta charset="UTF-8">
                                            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                                            <title>Retroalimentación Citas</title>
                                        </head>
                                        <body>
                                            <div style="text-align: center">
                                                <img style="margin:0 auto 0 auto;height:auto; width:auto; max-height:200px; max-width:300px;"  src="http://simiinmobiliarias.com/' . $logo . '"/>
                                                <div style="width: 100%; background-color: #337AB7; text-align: center; padding: 15px 0px 15px 0px">
                                                    <h4 style="color: white">Inmobiliaria: ' . $nombreaa . '</h4>
                                                </div>
                                                <div style="border: 3px #909090 solid; padding: 15px 0 0px 0; text-align: center;">

                                                    <p><b>Asesor(a): </b></p>
                                                    <p>' . $nombreasesor . '</p>

                                                    ';
                        $fotoper = (empty($Foto)) ? '' : '<img  style="height:100px; width:100px; max-height:100px; max-width:100px;" src="http://simiinmobiliarias.com/mcomercialweb/' . $Foto . '" style="">';
                        $cuerpo .= $fotoper . '

                                                    <br>

                                                    <p>Nueva Llamada Al Inmueble: ' . $inmueble . '</p>

                                                    <br>

                                                    <a target="_blank" style="background-color:#F16D6D; display: inline-block; color:white; width:150px; font-size:14px;padding:10px 5px 10px 5px; margin:10px 0 0 0;" href="http://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/' . $formatoFicha . '?reg=' . $inmueble . '">
                                                        Ver Ficha
                                                    </a>

                                                    <p style="color: #1BA1FF"><b>Asunto: </b></p>
                                                    <p>' . getCampo("conceptollamada", "where Idconcepto = " . $datarequest['concepto'], "descripcionllamada") . '</p>

                                                    <p style="color: #1BA1FF"><b>Observaciones: </b></p>
                                                    <p>' . $oblla . '</p>

                                                    <p style="color: #1BA1FF"><b>Nombre del Cliente:</b></p>
                                                    <p>' . $nom_cli . '</p>

                                                    <p style="color: #1BA1FF"><b>N&uacute;mero de Celular Cliente:  </b></p>
                                                    <p><a href="tel:' . $cel_cli . '" target="_Blank">' . $cel_cli . '</a> </p>

                                                    <p style="color: #1BA1FF"><b>N&uacute;mero de Tel&eacute;fono fijo Cliente: </b></p>
                                                    <p>' . $tel_cli . ' </p>

                                                    <p style="color: #1BA1FF"><b>Correo Cliente: </b></p>
                                                    <p><a href="mailto:' . $ema_cli . '">' . $ema_cli . ' </a> </p>

                                                    <p style="color: #1BA1FF"><b>Fecha de Registro: </b></p>
                                                    <p>' . $fecha_act . '</p>

                                                    <a class="text-primary" target="_blank" href="http://www.siminmueble.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA. Visitenos en www.siminmueble.com</a>
                                                    <p>Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo.</p>

                                                </div>
                                            </div>
                                        </body>
                                    </html>';
                        $asunto    = "Llamada para el inmueble $inmueble ";
                        $mailA2234 = $phpmailer;
                        $mailA2234->IsSMTP();
                        $mailA2234->SMTPAuth   = true;
                        $mailA2234->SMTPSecure = "tls";
                        $mailA2234->Host       = "smtp.office365.com"; //Servidor de Correo Bienco
                        $mailA2234->Port       = 587;
                        $mailA2234->Username   = "noreply@simiinmobiliarias.com"; //usuario perteneciente al servidor
                        $mailA2234->Password   = "Desarrollo.2015";
                        $mailA2234->From       = "noreply@simiinmobiliarias.com";
                        $mailA2234->FromName   = "Agente Virtual SimiWeb";
                        $mailA2234->Subject    = $asunto . " ";
                        $mailA2234->MsgHTML($cuerpo);
                        $mailA2234->AddAddress($mail_ase, $mail_ase);
                        $mailA2234->AddCC("$mail_cnt", "$mail_cnt");
                        $mailA2234->SMTPDebug = 0;
                        $mailA2234->IsHTML(true);
                        $mailA2234->Send();
                    }
                    $stmt4 = $this->connPDO->prepare($sqlinsert2);
                    if ($stmt4->execute()) {
                        if ($con == 8) {
                            $flag++;
                            $data = array(
                                'status' => 'Ok',
                                'msg'    => 'Se ha registrado y retroalimentado la llamada, se procede a agendar Cita',
                                // 'opencita' => 1
                            );
                        } else if ($con == 9 or $con == 8) {
                            $flag++;
                            $data = array(
                                'status' => 'Ok',
                                'msg'    => 'Se ha registrado y retroalimentado la llamada',
                                // 'opencita' => 0
                            );
                        }
                    } else {
                        $data = array(
                            'status' => 'Error',
                            'msg'    => 'Error en consulta stmt4',
                        );
                    }
                } else {
                    $data = array(
                        'status' => 'Error',
                        'msg'    => 'Error en consulta stmt3',
                    );
                }
            } else {
                $data = array(
                    'status' => 'Error',
                    'msg'    => 'Error en consulta stmt2',
                );
            }
            // } else {
            //     $data = array(
            //         'status' => 'Ok',
            //         'msg' => 'Se ha registrado la llamada',
            //         // 'opencita' => 0
            //     );
            //     $flag++;
            // }
            if ($datapost['mail_asesor']) {
                $data = array(
                    'status' => 'Ok',
                    'msg'    => 'Se ha Enviado el Correo',
                    // 'opencita' => 0
                );
            } else {
                $data = array(
                    'status' => 'Ok',
                    'msg'    => 'Se ha registrado la llamada',
                    // 'opencita' => 0
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'msg'    => 'Error en consulta stmt',
            );
        }
        return $data;
    }

    public function datosInmuebleTerceros($data)
    {

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $cond  = '';
        if ($data['rol_tr'] > 0) {
            $cond .= ' and rol_tr=:rol_tr';
        }
        if ($data['iduser'] > 0) {
            $cond .= ' and iduser_tr=:iduser';
        }
        $stmt = $this->connPDO->prepare("SELECT inmob_tr,idinm_tr,rol_tr,iduser_tr,idusu_tr,CONCAT(Nombres,' ',apellidos) AS nom,u.Direccion,u.Telefono,u.Celular,u.Correo,u.Id_Usuarios,u.IdTipoDocumento
                FROM inmueblesterceros i, usuarios u
                WHERE idinm_tr =:codinm
                and inmob_tr   = :idinmo
                AND u.`iduser`=i.`iduser_tr`
                $cond");
        if ($data['rol_tr'] > 0) {
            $stmt->bindParam(":rol_tr", $data['rol_tr']);
        }
        if ($data['iduser'] > 0) {
            $stmt->bindParam(":iduser", $data['iduser']);
        }
        $stmt->bindParam(":idinmo", $data['IdInmobiliaria']);
        $stmt->bindParam(":codinm", $data['codinm']);
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "rol_tr"         => $row['rol_tr'],
                    "iduser_tr"      => $row['iduser_tr'],
                    "nrol"           => utf8_encode(getCampo('parametros', "where id_param=51 and conse_param=" . $row['rol_tr'], 'desc_param')),
                    "ntercero"       => utf8_encode($row['nom']),
                    "celular"        => utf8_encode($row['Celular']),
                    "fijo"           => utf8_encode($row['Telefono']),
                    "correo"         => utf8_encode($row['Correo']),
                    "Direccion"      => utf8_encode($row['Direccion']),
                    "cedtercero"     => $row['Id_Usuarios'],
                    "tDoctercero"    => $row['IdTipoDocumento'],
                    "NomtDoctercero" => ucwords(strtolower(utf8_encode(getCampo('tipodocumento', "where IdTipoDocumento=" . $row['IdTipoDocumento'], 'Documento')))),
                );
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = null;
    }

    public function grabarLlamada($get, $post)
    {
        $fec = date('Y-m-d');
        $hor = date('H:i:s');
        $ip  = $_SERVER['REMOTE_ADDR'];
        if ($get['guarda'] == 1) {
            /*
            Grabar Cliente y/o Actualizar
             */
            if (empty($post['direccion'])) {
                $post['direccion'] = ' ';
            }
            if (empty($post['phone'])) {
                $post['phone'] = ' ';
            }
            if (empty($post['cell'])) {
                $post['cell'] = ' ';
            }
            if (empty($post['nominmo'])) {
                $post['nominmo'] = ' ';
            }
            if (empty($post['email'])) {
                $post['email'] = ' ';
            }
            if (empty($post['name'])) {
                $data = array(
                    'status' => 'Error',
                    'msg'    => 'Debe Digitar el Nombre',
                );
                return $data;
                exit();
            }
            if ($post['numced'] != '0') {
                $cedula   = $post['numced'];
                $updateCl = "update clientes_inmobiliaria
                            set nombre      = '" . $post['name'] . "',
                            direccion       = '" . $post['direccion'] . "',
                            telfijo         = '" . $post['phone'] . "',
                            telcelular      = '" . $post['cell'] . "',
                            email           = '" . $post['email'] . "',
                            idmedio         = '" . $post['mediopubli'] . "',
                            obser_cli       = '" . $post['observaciones'] . "',
                            emp_cli         = '" . $post['company'] . "',
                            ase_cli         = '" . $post['asesor'] . "'
                            where cedula=$cedula";
                $stmtupdatecli = $this->connPDO->prepare($updateCl);
                if (!$stmtupdatecli->execute()) {
                    $data = array(
                        'status' => 'Error',
                        'msg'    => print_r($stmtupdatecli->errorInfo()) . ' Error',
                        'cedula' => $cedula
                    );
                    return $data;
                    exit();
                }
            } else {
                $cedula        = consecutivo('cedula', 'clientes_inmobiliaria');
                $f_crea        = date('Y-m-d');
                $h_crea        = date('H:i:s');
                $sql           = 'REPLACE INTO clientes_inmobiliaria(cedula,nombre, direccion, telfijo, telcelular, email, inmobiliaria, idmedio,obser_cli,emp_cli,ase_cli,f_asignado,h_asignado,f_crea_cli,h_crea_cli) VALUES("' . $cedula . '","' . $post['name'] . '", "' . $post['direccion'] . '", "' . $post['phone'] . '", "' . $post['cell'] . '", "' . $post['email'] . '", "' . $_SESSION['IdInmmo'] . '", "' . $post['mediopubli'] . '", "' . $post['observaciones'] . '", "' . $post['company'] . '", "' . $post['asesor'] . '", "' . $f_asignado . '", "' . $h_asignado . '", "' . $f_crea . '", "' . $h_crea . '")';
                $stmtinsertcli = $this->connPDO->prepare($sql);
                if (!$stmtinsertcli->execute()) {
                    $data = array(
                        'status' => 'Error',
                        'msg'    => print_r($stmtinsertcli->errorInfo()) . ' Error',
                        'cedula' => $cedula
                    );
                    return $data;
                    exit();
                }
                $_SESSION['CedulaCli'] = $cedula;
            }

            /*
            Grabar Llamada o Terminar Llamada
             */
            if ($cedula <= 0) {
                $numced = $_SESSION['CedulaCli'];
            } else {
                $numced = $cedula;
            }
            $cons                   = consecutivo('cons_call', 'duracion_llamadas');
            $sql                    = 'INSERT INTO duracion_llamadas (cons_call,id_cli_call,usu_call,fec_call,hori_call,horf_call,ip_call,inmo_call) VALUES("' . $cons . '","' . $numced . '", "' . $_SESSION['Id_Usuarios'] . '", "' . $fec . '", "' . $hor . '", "","' . $ip . '", "' . $_SESSION['IdInmmo'] . '")';
            $_SESSION['codLlamada'] = $cons;
            $stmt                   = $this->connPDO->prepare($sql);
            if ($stmt->execute()) {
                $_SESSION['NombreCli']            = $post['name'];
                $_SESSION['CedulaCli']            = $numced;
                $_SESSION['asesor_calllogin']     = $post['asesor'];
                $_SESSION['asesor_callIdUsuario'] = getCampo('usuarios', " where iduser = '" . $post['asesor'] . "'", 'Id_Usuarios');
                $data                             = array(
                    'status' => 'Ok',
                    'msg'    => 'Llamada creada correctamente',
                    'cedula' => $cedula
                );
            } else {
                $data = array(
                    'status' => 'Error',
                    'msg'    => print_r($stmt->errorInfo()) . ' Error',
                );
            }
        } else {
            $sqlupdatellamada  = 'UPDATE duracion_llamadas SET horf_call =  "' . $hor . '" WHERE cons_call= "' . $_SESSION['codLlamada'] . '"';
            $stmtupdatellamada = $this->connPDO->prepare($sqlupdatellamada);
            if ($stmtupdatellamada->execute()) {
                unset($_SESSION['NombreCli']);
                unset($_SESSION['CedulaCli']);
                $data = array(
                    'status' => 'Ok',
                    'msg'    => 'Llamada terminada',
                );
            } else {
                $data = array(
                    'status' => 'Error',
                    'msg'    => print_r($stmtupdatellamada->errorInfo()) . ' Error',
                );
            }
        }
        return $data;
    }

    public function getBarriosZona($post)
    {
        $sql  = "SELECT IdBarrios, NombreB FROM barrios WHERE IdZona = :zona";
        $stmt = $this->connPDO->prepare($sql);
        $stmt->bindParam(':zona', $post['zona']);
        $data = array();
        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                if ($row) {
                    $data[] = array(
                        'id'     => $row['IdBarrios'],
                        'Nombre' => utf8_encode(ucfirst(strtolower($row['NombreB']))),
                    );
                }
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
    }

    public function getDatosFicha()
    {
        $sql = "select email, nombre from clientes_inmobiliaria where cedula = :cedulacli and inmobiliaria = :IdInmmo order by cedula desc limit 0,1";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':cedulacli', $_SESSION['CedulaCli']);
        $stmt->bindParam(':IdInmmo', $_SESSION['IdInmmo']);

        if ($stmt->execute()) {
            $result     = $stmt->fetch(PDO::FETCH_ASSOC);
            $correoclie = $result['email'];
            $nombre     = $result['nombre'];
            $sqluser    = "select concat(Nombres,' ',apellidos) as nombre, correo as email from usuarios where IdInmmo = :inmo and Id_Usuarios = :iduser";
            $stmtuser   = $this->connPDO->prepare($sqluser);
            $stmtuser->bindParam(':inmo', $_SESSION['IdInmmo']);
            $stmtuser->bindParam(':iduser', $_SESSION['Id_Usuarios']);
            if ($stmtuser->execute()) {
                $resultuser    = $stmtuser->fetch(PDO::FETCH_ASSOC);
                $correoclieusu = $resultuser['email'];
                $nombreusu     = $resultuser['nombre'];
                $data          = array(
                    'status'        => 'Ok',
                    'correoclie'    => $correoclie,
                    'nombre'        => $nombre,
                    'correoclieusu' => $correoclieusu,
                    'nombreusu'     => $nombreusu,
                );
            } else {
                $data = array(
                    'status' => 'Error',
                    'msg'    => print_r($stmt->errorInfo()) . " error",
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'msg'    => print_r($stmt->errorInfo()) . " error",
            );
        }
        return $data;
    }

    public function enviarFicha($data, $phpmailer, $mailCard)
    {
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";die;

        list($inmo, $inmu) = explode('-', $data['inmu']);
        $inmobi            = $_SESSION['IdInmmo'];
        $a                 = $datosMail                 = $mailCard->getCard($_SESSION['IdInmo']);
        foreach ($a as $key => $value) {}
        $sqlinmobiliaria = "SELECT  EmailC,detallesimi,PaginaWeb,NombreInm  from inmobiliaria  WHERE  IdInmobiliaria = :inmo";

        $stmtinmobiliaria = $this->connPDO->prepare($sqlinmobiliaria);

        $stmtinmobiliaria->bindParam(':inmo', $_SESSION['IdInmmo']);

        if ($stmtinmobiliaria->execute()) {
            $resultinmobiliaria = $stmtinmobiliaria->fetch(PDO::FETCH_ASSOC);
            $mailcomercial      = $resultinmobiliaria['EmailC'];
            $PaginaWeb          = $resultinmobiliaria['PaginaWeb'];
            $actsimi            = $resultinmobiliaria['actsimi'];
            $detallesimi        = $resultinmobiliaria['detallesimi'];
            $NombreInm          = $resultinmobiliaria['NombreInm'];

            $sqlinmoclientessimi = "SELECT Telefonos,Direccion,inmobiliaria.EmailC,inmobiliaria.NombreInm,logo from inmobiliaria,clientessimi WHERE  inmobiliaria.IdInmobiliaria = :inmo AND inmobiliaria.IdInmobiliaria=clientessimi.IdInmobiliaria";

            $stmtinmoclientessimi = $this->connPDO->prepare($sqlinmoclientessimi);

            $stmtinmoclientessimi->bindParam(':inmo', $_SESSION['IdInmmo']);

            if ($stmtinmoclientessimi->execute()) {
                $resultinmoclientessimi = $stmtinmoclientessimi->fetch(PDO::FETCH_ASSOC);
                $logo                   = $resultinmoclientessimi['logo'];
                $telefonoInm            = $resultinmoclientessimi['Telefonos'];
                $direccionInm           = $resultinmoclientessimi['Direccion'];

                $sqlusuarios = "SELECT Nombres,apellidos,Celular,Foto FROM usuarios WHERE Id_Usuarios = :usuario";

                $stmtusuarios = $this->connPDO->prepare($sqlusuarios);

                $stmtusuarios->bindParam(':usuario', $_SESSION['Id_Usuarios']);

                if ($stmtusuarios->execute()) {
                    $resultusuario = $stmtusuarios->fetch(PDO::FETCH_ASSOC);
                    $Celular       = $resultusuario['Celular'];
                    $Nombre        = $resultusuario['Nombres'];
                    $Foto          = $resultusuario['Foto'];

                    $logo = substr(getCampo('clientessimi', ' where IdInmobiliaria = ' . $_SESSION['IdInmmo'], 'logo'), 3);

                    $logoinmo = "http://www.simiinmobiliarias.com/" . $logo;

                    $fotoinmo = "http://www.simiinmobiliarias.com/" . $Foto;

                    $destinatario = $data['emaildes'];

                    $remitente = $data['nomre'];

                    $emailre = $data['emailre'];

                    $asunto = "Inmueble de su interés";

                    $fec_lf    = date('Y-m-d');
                    $hor_lf    = date('H:i:s');
                    $ip_lf     = $_SERVER['REMOTE_ADDR'];
                    $asesor_lf = $data['selectfichatercero'];
                    if ($data['selectfichaasesor'] == '7') {
                        $asesor_lf = $_SESSION['iduser'];
                    }
                    if ($data['selectfichaasesor'] == '6') {
                        $asesor_lf = 0;
                    }
                    $iduser_lf   = $_SESSION['iduser'];
                    $enviadoa_lf = $data['cedEnvia'];
                    $fotos_lf    = $data['selectfichafotos'];

                    $fuente     = 1;
                    $inmo_envia = $_SESSION['IdInmmo'];

                    if (isset($data['fichaCruce']) && $data['fichaCruce'] == 1) {
                        $fuente = 3;
                    }

                    $sqlInserLogFicha = "INSERT INTO log_envio_fichas (inmo_lf,inmu_lf,asesor_lf,usr_lf,fec_lf,hor_lf,ip_lf,enviadoa_lf,fuente,con_fotos,inmo_envia) VALUES('" . $inmo . "','" . $inmu . "','" . $asesor_lf . "','" . $iduser_lf . "','" . $fec_lf . "','" . $hor_lf . "','" . $ip_lf . "','" . $enviadoa_lf . "'," . $fuente . ",'" . $fotos_lf . "','" . $inmo_envia . "')";

                    $stmtInsertLogFicha = $this->connPDO->prepare($sqlInserLogFicha);

                    if (!$stmtInsertLogFicha->execute()) {
                        $data = array(
                            'status' => 'Error',
                            'msg'    => print_r($stmtInsertLogFicha->errorInfo()) . " error Insertar Log Ficha",
                        );
                        return $data;
                    }

                    $lastInsertLogFicha = $this->connPDO->lastInsertId();

                    $cuerpo = '
                                <html>
                                <head><meta charset="UTF-8">
                                <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                                <title>M&Oacute;DULO COMERCIAL SIMI</title>


                                <title>Filtro de Búsqueda Clientes</title>

                                <style>

                                </style>
                                </head>
                                <body>

                                <table align="center"  cellpadding="0" cellspacing="0" width="100%" style="border:0;">

                                 <tr>

                                <td align="center" bgcolor="" >

                                 <p class="" >
                                                <img style="margin:0 auto 0 auto;height:auto; width:auto; max-height:200px; max-width:300px;"  src="' . $logoinmo . '"/>
                                              </p>

                                </td>

                                 </tr>

                                 <tr>

                                <td bgcolor="#337AB7" align="center" style="padding: 15px 0 0px 0;">


                                <font face="verdana" color="#FFFFFF"><h4>Se ha compartido la ficha t&eacute;cnica de un inmueble.</h4></font>

                                </td>

                                 </tr>

                                 <tr>

                                <td bgcolor="#ee4c50" height="">



                                </td>

                                 </tr>
                                  <tr >

                                <td bgcolor="#FFFFFF" style="border:3px #909090 solid;padding: 15px 0 0px 0;" align="center" >

                                  <div >
                                <font face="verdana" color="#424242">
                                        Sr.(a): ' . $data['nomdes'] . '.
                                </font>
                                </div>';
                    $fotoper = (empty($Foto)) ? '' : '<img  style="height:100px; width:100px; max-height:100px; max-width:100px;" src="http://simiinmobiliarias.com/mcomercialweb/' . $Foto . '" style="">';
                    $cuerpo .= '<div align="">
                                    ' . $fotoper . '
                                </div>
                                <div>
                                <font face="verdana" color="#424242">
                                    El asesor <b>' . $data['nomre'] . '</b>, <br> Ha compartido la ficha t&eacute;cnica de un inmueble de su interés.
                                    </h5>
                                 </font>
                                 </div>
                                 <div>
                                    <font face="verdana" color="#424242">
                                        <h5><b>Comentarios Adicionales del Asesor:</b>
                                        </h5>
                                    </font>
                                 </div>
                                 <div>
                                    <font face="verdana" color="#424242">
                                        <h5">' . ucfirst(strtolower($data['comen'])) . '
                                        </h5>
                                    </font>
                                 </div>
                                 <div>
                                    <font face="verdana" color="#424242">
                                        Para visualizarla por favor dar click sobre el link
                                    </font>
                                 </div>

                                 <font  face="verdana" color="#FFFFFF">
                                 <div style="background-color:#F16D6D; width:100px; font-size:10px;padding:10px 5px 10px 5px; margin:10px 0 0 0; ">';
                    $plantillaFicha = "";
                    $plantillaFicha = getCampo('fichasTecnicas', "where idFicha=" . $data['selectficha'], 'ficha');
                    if (!empty($detallesimi)) {
                        $reg         = $data['inmu'];
                        $detallesimi = $detallesimi . $reg;
                        $cuerpo .= '<a  target="_blank" style="color:white;" href="http://simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/' . $plantillaFicha . '.php?reg=' . $detallesimi . '&log=' . $lastInsertLogFicha . '">VER FICHA' . $detallesimi . '</a><br><br>';
                    } else {
                        //$urlFicha = ($formatoFicha == 'FichaInmuebleRA.php')? "FichaInmuebleRA.php?reg=".$data['inmu']."&tipo=".$data['promotor']: "'.$plantillaFicha.'?reg=".$data['inmu'];

                        $urlFicha = $data['inmu'] . "&tipo=" . $data['promotor'] . "&log=" . $lastInsertLogFicha;
                        $cuerpo .= '<a style="color:white;" target="_blank" href="http://simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/' . $plantillaFicha . ".php?reg=" . $urlFicha . '">VER FICHA</a>';

                    }

                    $cuerpo .= '
                                 </div>
                                 </font>
                                <div>
                                <font  face="verdana" color="#424242">
                                    <h5>
                                                                Correo enviado a <labe class="text-primary">' . $data['emaildes'] . '
                                                            </h5>
                                  </font>';
                    $datoc   = (empty($Celular)) ? '' : 'Celular: ' . $Celular;
                    $datoinm = (empty($telefonoInm)) ? '' : 'Tel&eacute;fono: ' . $telefonoInm;
                    $cuerpo .= '
                                <div>
                                    <font  face="verdana" color="#424242">
                                        <h5>
                                                <font  face="verdana" color="#1BA1FF">Datos del Asesor: </h5>
                                     </font>
                                </div>
                                <div>
                                    <font  face="verdana" color="#424242">
                                        <h5>

                                ' . $datoc . ' Correo: ' . $emailre . '  </h5>
                                     </font>
                                </div>
                                <div>
                                    <font  face="verdana" color="#424242">
                                        <h5>
                                                <font  face="verdana" color="#1BA1FF">Datos Inmobiliaria </h5>
                                     </font>
                                </div>
                                <div>
                                    <font  face="verdana" color="#424242">
                                        <h5>
                                                <font  face="verdana" color="#1BA1FF">' . $NombreInm . ':</h5>
                                     </font>
                                </div>
                                <div>
                                    <font  face="verdana" color="#424242">
                                        <h5>
                                                 </font> <labe ">' . $datoinm . ' Direcci&oacute;n: ' . $direccionInm . '</h5>
                                     </font>
                                </div>
                                </div>
                                <font  face="verdana" color="#424242">
                                <div>
                                    Cordialmente,
                                </div>
                                <div>
                                    El Equipo Comercial de ' . $NombreInm . '
                                </div>
                                <div>
                                    <a class="text-primary" target="_blank" href="http://www.siminmueble.com/">© Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA. Visitenos en www.siminmueble.com</a>
                                                            </p>
                                                            <p>Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo.
                                </div>
                                </font>
                                </td>
                                </table>
                                <body>';
                    $phpmailer->IsSMTP();
                    $phpmailer->SMTPAuth   = true;
                    $phpmailer->SMTPSecure = "tls";
                    /*      __________________________________________________________*/
                    //$maiAl->Host = "mail.siminmueble.com";
                    //$phpmailer->Port = 25;
                    //$phpmailer->Username = "no-reply";
                    //$phpmailer->Password = "s1m12014";
                    /*      __________________________________________________________*/
                    /*
                    $phpmailer->Host = "smtp.gmail.com";
                    $phpmailer->Port = 587;
                    $phpmailer->Username = "desarrolloweb@tae-ltda.com";
                    $phpmailer->Password = "D2s1rr4ll4S3m3";
                    $phpmailer->From = $_POST['emailre'];
                    $phpmailer->FromName = "Agente Virtual SimiWeb";*/

                    $phpmailer->FromName = "Agente Virtual SimiWeb";
                    $phpmailer->Host     = $value['host_ml']; //Servidor de Correo
                    $phpmailer->Port     = $value['port_ml'];
                    $phpmailer->Username = $value['usr_ml']; //usuario perteneciente al servidor
                    $phpmailer->Password = $value['pass_ml'];
                    $phpmailer->From     = $value['from_ml'];

                    $phpmailer->Subject = "$asunto";
                    $phpmailer->MsgHTML($cuerpo);

                    //$phpmailer->AddBCC("coordinadorsoporte@tae-ltda.com","coordinadorsoporte");
                    if (strpos($destinatario, ',') === false) {
                        $phpmailer->AddAddress($destinatario, $destinatario);
                    } else {
                        $correos = explode(",", $destinatario);

                        for ($i = 0; $i < count($correos); $i++) {
                            $phpmailer->AddAddress($correos[$i], $correos[$i]);
                        }
                    }

                    $phpmailer->AddAddress($emailre, $remitente);

                    $phpmailer->AddCC($emailre, $remitente);
                    //$phpmailer->AddBCC("desarrolloweb2@tae-ltda.com","desarrolloweb2@tae-ltda.com");
                    $phpmailer->SMTPDebug = 1;
                    $phpmailer->IsHTML(true);
                    $phpmailer->CharSet = 'UTF-8';

                    if ($phpmailer->Send()) {
                        $data = array(
                            'status'        => 'Ok',
                            'msg'           => 'Correo enviado Exitosamente',
                            'destinatarios' => strpos($destinatario, ','),
                        );
                    } else {
                        $data = array(
                            'status' => 'Error',
                            'msg'    => 'Error al enviar el correo',
                        );
                    }
                } else {
                    $data = array(
                        'status' => 'Error',
                        'msg'    => print_r($stmtusuarios->errorInfo()) . " error",
                    );
                }

            } else {
                $data = array(
                    'status' => 'Error',
                    'msg'    => print_r($stmtinmoclientessimi->errorInfo()) . " error",
                );
            }

        } else {
            $data = array(
                'status' => 'Error',
                'msg'    => print_r($stmtinmobiliaria->errorInfo()) . " error",
            );
        }
        return $data;
    }
    public function getDataDetalleInmuebleFull($data)
    {
        $connPDO = new Conexion();

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $connPDO->prepare("SELECT  d.idcaracteristica,d.obser_det,d.cantidad_dt,m.Descripcion,m.tpcar,m.icono_car
             FROM detalleinmueble d,maestrodecaracteristicasnvo m
             WHERE d.inmob          = :IdInmobiliaria
             AND d.codinmdet        =:codinm
             and m.tpcar           =:tpcar
             and d.idCaracteristica =m.idCaracteristica");

        if ($stmt->execute(array(

            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":tpcar"          => $data['tpcar'],

        ))) {
            $info = array();
            while ($row = $stmt->fetch()) {

                $info[] = array(
                    "idcaracteristica" => $row['idcaracteristica'],
                    "obser_det"        => $row['obser_det'],
                    "cantidad_dt"      => $row['cantidad_dt'],
                    "Descripcion"      => str_replace("Ñ", "ñ", ucwords(strtolower(utf8_encode($row['Descripcion'])))),
                    "icono_car"        => "<i class='fa " . $row['icono_car'] . "'></i>",
                    "tpcar"            => $row['tpcar'],
                );
            }

            return $info;
        } else {
            print_r($stmt->errorInfo());
        }

    }
    public function validaEstadoInm($data)
    {
        $connPDO = new Conexion();

        $codinm = $data['codinm'];
        if (strstr($codinm, ',')) {
            $largo = count(explode(",", $data['codinm']));
            //echo $largo."fdaf <br>";
            $codi = explode(",", $data['codinm']);
            $cont++;
            $control = 1;
            for ($r = 0; $r < $largo; $r++) {

                if ($r == 0) {
                    $condicion .= "and (idInm='" . $codi[$r] . "')";
                } else {
                    $condicion .= " or (idInm='" . $codi[$r] . "')";
                }
            }
            //echo $condicion."<br>";
        } else {
            $condicion .= " AND
            ( idInm         = ':codinm'
            OR RVivaReal    = ':codinm'
            OR RLaGuia      = ':codinm'
            OR RMtoCuadrado = ':codinm'
            OR RZonaProp    = ':codinm'
            OR RMeli        = ':codinm'
            or codinterno   = ':codinm')";
        }

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $connPDO->prepare("SELECT idEstadoInmueble,descripcion,idInm
            from inmnvo,estado_inmueble
            where idEstadoinmueble =idestado
            and IdInmobiliaria     =:IdInmobiliaria
            and codinm             =:codinm
            $condicion1");

        if ($stmt->execute(array(

            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
        ))) {
            $info   = array();
            $existe = $stmt->rowCount();
            if ($existe == 0) {
                $data = array(
                    'status' => 'Error',
                    'msg'    => 'Inmueble no Existe',
                );
                // $info[]=array("mensaje"=>"Inmueble no Existe");
            } else {
                while ($row = $stmt->fetch()) {

                    if ($row['idEstadoInmueble'] != 2) {
                        $estado = strtolower(utf8_encode(getCampo('estado_inmueble', "where idestado=" . $row['idEstadoInmueble'], 'descripcion')));
                        // $info[]=array("mensaje"=>"El Inmueble ".$row['idInm']." tiene estado ".$estado);
                        $data = array(
                            'status' => 'Error',
                            'msg'    => "El Inmueble " . $row['idInm'] . " tiene estado " . $estado,
                        );
                    } else {

                        //  $info[]=array(
                        //     "idEstadoInmueble" => $row['idEstadoInmueble'],
                        //     "idInm"            => $row['idInm'],
                        //     "Descripcion"      => $row['descripcion']
                        // );

                        $data = array(
                            'status'           => 'Ok',
                            "idEstadoInmueble" => $row['idEstadoInmueble'],
                            "idInm"            => $row['idInm'],
                            "Descripcion"      => $row['descripcion'],
                        );
                    }
                }

            }
            return $data;
        } else {
            print_r($stmt->errorInfo());
        }

    }

    public function saveObservacionPropietario($data)
    {
        $sql = "insert into log_propietario (idInm,logdes,fechacomen,IdUsuarioComen,IdInmo) VALUES (:idinmo,:oblla,:hoy,:Id_Usuarios,:IdInmmo)";

        $hoy = date("Y-m-d");

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':idinmo', $data['inmu']);
        $stmt->bindParam(':oblla', $data['oblla']);
        $stmt->bindParam(':hoy', $hoy);
        $stmt->bindParam(':Id_Usuarios', $_SESSION['Id_Usuarios']);
        $stmt->bindParam(':IdInmmo', $_SESSION['IdInmmo']);
        if ($stmt->execute()) {
            $data = array(
                'status' => 'Ok',
                'msg'    => 'Observacion guardada correctamente!',
            );
        } else {
            $data = array(
                'status' => 'Error',
                'msg'    => print_r($stmt->errorInfo()),
            );
        }

        return $data;
    }

    private function getClienteCall()
    {

        if (!isset($_SESSION['CedulaCli']) || empty($_SESSION['CedulaCli'])) {
            $data = array(
                'status' => 'Error',
                'msg'    => 'Debe ingresar un usuario en el Call Center para poder editarlo!',
            );
        } else {

            $ced_user = $_SESSION['CedulaCli'];

            $sql = 'SELECT `cedula`,`nombre`,`direccion`,`telfijo`,`telcelular`,cli_inm.`email`,inmo.`NombreInm`,`idmedio` ,cli_inm.obser_cli,cli_inm.emp_cli,cli_inm.ase_cli,cli_inm.est_cli FROM `clientes_inmobiliaria` cli_inm,`inmobiliaria` inmo WHERE  `cedula` =  :cedula AND cli_inm.`inmobiliaria` = inmo.`IdInmobiliaria`';

            $stmt = $this->connPDO->prepare($sql);

            $stmt->bindParam(':cedula', $ced_user);

            if ($stmt->execute()) {
                $result            = $stmt->fetch(PDO::FETCH_ASSOC);
                $ase_cli           = $result['ase_cli'];
                $asesor            = getCampo('usuarios', 'where Id_Usuarios = "' . $ase_cli . '"', 'iduser');
                $result['ase_cli'] = empty($asesor) ? $ase_cli : $asesor;
                $data              = array(
                    'status' => 'Ok',
                    'datos'  => $result,
                );
            } else {
                $data = array(
                    'status' => 'Error',
                    'msg'    => print_r($stmt->errorInfo()),
                );
            }
        }

        return $data;
    }

    public function updateClienteCall($data)
    {
        if (empty($data['telfijo'])) {
            $data['telfijo'] = ' ';
        }
        if (empty($data['telcel'])) {
            $data['telcel'] = ' ';
        }
        if (empty($data['email'])) {
            $data['email'] = ' ';
        }
        $sql = 'UPDATE clientes_inmobiliaria
        SET nombre    = :nombre,
        telfijo       = :telfijo,
        telcelular    = :telcel,
        email         = :email,
        emp_cli       = :emp_cli,
        ase_cli       = :ase_cli,
        obser_cli     = :obser_cli,
        est_cli       = :est_cli,
        idmedio       = :nompubli
        WHERE cedula  = :cedula';
        // echo "<pre>";
        // print_r($sql);
        // echo "</pre>";die;

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':nombre', $data['nombre']);
        $stmt->bindParam(':telfijo', $data['telfijo']);
        $stmt->bindParam(':telcel', $data['telcel']);
        $stmt->bindParam(':email', $data['email']);
        $stmt->bindParam(':emp_cli', $data['emp_cli']);
        $stmt->bindParam(':ase_cli', $data['ase_cli']);
        $stmt->bindParam(':obser_cli', $data['obser_cli']);
        $stmt->bindParam(':est_cli', $data['est_cli']);
        $stmt->bindParam(':nompubli', $data['nompubli']);
        $stmt->bindParam(':cedula', $data['cedula']);
        if ($stmt->execute()) {
            $data = array(
                'status' => 'Ok',
                'msg'    => 'Datos actualizados correctamente',
            );
        } else {
            $data = array(
                'status' => 'Error',
                'msg'    => print_r($stmt->errorInfo()),
            );
        }
        return $data;
    }
    public function updateReservaInmueblesOld($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("UPDATE inmuebles
                SET
                idEstadoInmueble            =4
                where idInm =:idInm");

        if ($stmt->execute(array(
            ":idInm" => $data,
        ))) {
            return 1;
        } else {
            $response[] = array("Error" => $stmt->errorInfo());
        }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }

    public function getSitiosInteres($data)
    {
        $response = array();
        $lat      = $_POST['lat'];
        $long     = $_POST['lng'];
        $distance = $_POST['dist'];

        $box = $this->getBoundaries($lat, $long, $distance);

        $sql = 'SELECT
            label_sit,
            latitud_sit,
            longitud_sit,
            direccion_sit,
            telefono_sit,
            info_sit,
            tp_sit,
            ntp_sit,
            ( 3959 * acos(
            cos( radians(' . $lat . ') ) '
            . '* cos( radians( latitud_sit ) ) '
            . '* cos( radians( longitud_sit ) '
            . '- radians(' . $long . ') ) '
            . '+ sin( radians(' . $lat . ') )
                    * sin( radians( latitud_sit ) ) ) )
                    AS distance
            FROM sitios_interes
            WHERE (latitud_sit BETWEEN ' . $box['min_lat'] . ' AND ' . $box['max_lat'] . ')
            AND (longitud_sit BETWEEN ' . $box['min_lng'] . ' AND ' . $box['max_lng'] . ')
            HAVING distance  <= ' . $distance . '
            ORDER BY distance ASC';

        $stmt = $this->connPDO->prepare($sql);

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $response[] = array(
                    "label_sit"     => $row['label_sit'],
                    "latitud_sit"   => $row['latitud_sit'],
                    "longitud_sit"  => $row['longitud_sit'],
                    "direccion_sit" => "Direccion: " . $row['direccion_sit'],
                    "telefono_sit"  => "Telefono: " . $row['telefono_sit'],
                    "info_sit"      => "Horario: " . $row['info_sit'],
                    "tipo_sit"      => getCampo('parametros', 'where id_param=49 and conse_param=' . $row['tp_sit'], 'desc_param') . ": " . getCampo('parametros', 'where id_param=50 and conse_param=' . $row['ntp_sit'], 'desc_param'),
                    "icon"          => 'https://simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/img/' . getCampo('parametros', 'where id_param=49 and conse_param=' . $row['tp_sit'], 'mail_param'),
                );
            }
            return $response;
        } else {
            return print_r($stmt->errorInfo());
        }

    }

    private function getBoundaries($lat, $lng, $distance = 1, $earthRadius = 6371)
    {
        $return = array();

        // Los angulos para cada dirección
        $cardinalCoords = array('north' => '0',
            'south'                         => '180',
            'east'                          => '90',
            'west'                          => '270');
        $rLat     = deg2rad($lat);
        $rLng     = deg2rad($lng);
        $rAngDist = $distance / $earthRadius;
        foreach ($cardinalCoords as $name => $angle) {
            $rAngle        = deg2rad($angle);
            $rLatB         = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB         = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));
            $return[$name] = array('lat' => (float) rad2deg($rLatB),
                'lng'                        => (float) rad2deg($rLonB));
        }
        return array('min_lat' => $return['south']['lat'],
            'max_lat'              => $return['north']['lat'],
            'min_lng'              => $return['west']['lng'],
            'max_lng'              => $return['east']['lng']);
    }

    public function crearCliente($get, $post)
    {
        $post["cedula"] = trim($post["cedula"]);
        if (empty($post["cedula"])) {
            $cedula = consecutivo('cedula', 'clientes_inmobiliaria');
        } else {
            $cedula = $post["cedula"];
        }
        $f_crea        = date('Y-m-d');
        $h_crea        = date('H:i:s');
        $sql           = 'REPLACE INTO clientes_inmobiliaria(cedula,nombre, direccion, telfijo, telcelular, email, inmobiliaria, idmedio,obser_cli,emp_cli,ase_cli,f_asignado,h_asignado,f_crea_cli,h_crea_cli) VALUES("' . $cedula . '","' . $post['name'] . '", "' . $post['direccion'] . '", "' . $post['phone'] . '", "' . $post['cell'] . '", "' . $post['email'] . '", "' . $_SESSION['IdInmmo'] . '", "' . $post['mediopubli'] . '", "' . $post['observaciones'] . '", "' . $post['company'] . '", "' . $post['asesor'] . '", "' . $f_asignado . '", "' . $h_asignado . '", "' . $f_crea . '", "' . $h_crea . '")';
        $stmtinsertcli = $this->connPDO->prepare($sql);
        $dataCliente   = array("ced_cli" => $post['cedula']);
        $dataCliente   = $this->getDataCliente($dataCliente);

        if (!$stmtinsertcli->execute()) {
            $data = array(
                'status' => 'Error',
                'msg'    => print_r($stmtinsertcli->errorInfo()) . ' Error',
            );
            return $data;
            exit();
        } else {
            $data = array(
                'status' => 'Success',
                'msg'    => ' Creado con Exito',
            );

            $dataLog = array("campo" => "nombre", "antes" => $dataCliente["Nombre"], "actual" => $post['name'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "direccion", "antes" => $dataCliente["Direccion"], "actual" => $post['direccion'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "telfijo", "antes" => $dataCliente["Telefono"], "actual" => $post['phone'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "telcelular", "antes" => $dataCliente["Celular"], "actual" => $post['cell'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "idmedio", "antes" => $dataCliente["medio"], "actual" => $post['mediopubli'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "obser_cli", "antes" => $dataCliente["observaciones"], "actual" => $post['observaciones'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "emp_cli", "antes" => $dataCliente["empresa"], "actual" => $post['company'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "ase_cli", "antes" => $dataCliente["Asesor"], "actual" => $post['asesor'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            return $data;

        }
    }

    public function ActualizarCliente($get, $post)
    {

        $f_crea        = date('Y-m-d');
        $h_crea        = date('H:i:s');
        $sql           = 'UPDATE clientes_inmobiliaria SET nombre = :nombre, direccion = :direccion, telfijo = :telfijo, telcelular = :telcelular, email = :email, idmedio = :idmedio, obser_cli = :obser_cli, emp_cli = :emp_cli, ase_cli = :ase_cli where cedula = :cedula ';
        $stmtinsertcli = $this->connPDO->prepare($sql);
        $stmtinsertcli->bindParam(':nombre', $post['name']);
        $stmtinsertcli->bindParam(':direccion', $post['direccion']);
        $stmtinsertcli->bindParam(':telfijo', $post['phone']);
        $stmtinsertcli->bindParam(':telcelular', $post['cell']);
        $stmtinsertcli->bindParam(':email', $post['email']);
        $stmtinsertcli->bindParam(':idmedio', $post['mediopubli']);
        $stmtinsertcli->bindParam(':emp_cli', $post['company']);
        $stmtinsertcli->bindParam(':ase_cli', $post['asesor']);
        $stmtinsertcli->bindParam(':obser_cli', $post['observaciones']);
        $stmtinsertcli->bindParam(':cedula', $post['cedula']);

        $dataCliente = array("ced_cli" => $post['cedula']);

        $dataCliente = $this->getDataCliente($dataCliente);

        if (!$stmtinsertcli->execute()) {
            $data = array(
                'status' => 'Error',
                'msg'    => print_r($stmtinsertcli->errorInfo()) . ' Error',
            );
            return $data;
            exit();
        } else {
            $data = array(
                'status' => 'Success',
                'msg'    => ' Actualizado con Exito',
            );

            $dataLog = array("campo" => "nombre", "antes" => $dataCliente["Nombre"], "actual" => $post['name'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "direccion", "antes" => $dataCliente["Direccion"], "actual" => $post['direccion'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "telfijo", "antes" => $dataCliente["Telefono"], "actual" => $post['phone'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "telcelular", "antes" => $dataCliente["Celular"], "actual" => $post['cell'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "idmedio", "antes" => $dataCliente["medio"], "actual" => $post['mediopubli'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "obser_cli", "antes" => $dataCliente["observaciones"], "actual" => $post['observaciones'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "emp_cli", "antes" => $dataCliente["empresa"], "actual" => $post['company'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);
            $dataLog = array("campo" => "ase_cli", "antes" => $dataCliente["Asesor"], "actual" => $post['asesor'], "cedula" => $post['cedula']);
            $this->saveLogMaestroCliente($dataLog);

            return $data;

        }
    }

    public function getDataCliente($data)
    {

        $sql = "SELECT nombre,direccion,telfijo,telcelular,email,idmedio,ase_cli,obser_cli,emp_cli,cedula
                FROM clientes_inmobiliaria
                WHERE cedula=:cc
                and inmobiliaria=:inmobiliaria
                limit 0,100";
        $stmt = $this->connPDO->prepare($sql);
        $stmt->bindParam(':cc', $data["ced_cli"]);
        $stmt->bindParam(':inmobiliaria', $_SESSION['IdInmmo']);
        if ($stmt->execute()) {
            $cliente       = $stmt->fetch(PDO::FETCH_ASSOC);
            $nombre        = $cliente['nombre'];
            $dire          = $cliente['direccion'];
            $telf          = $cliente['telfijo'];
            $telcel        = $cliente['telcelular'];
            $email         = $cliente['email'];
            $ase_cli       = $cliente['ase_cli'];
            $ase_id        = $cliente['ase_cli'];
            $asesor        = getCampo('usuarios', 'where Id_Usuarios = "' . $ase_cli . '"', 'iduser');
            $medio         = $cliente['idmedio'];
            $empresa       = $cliente['emp_cli'];
            $observaciones = $cliente['obser_cli'];
            $cedula        = $cliente['cedula'];
            // $_SESSION['asesor_call'] = empty($asesor) ? $ase_cli : $asesor;
            $data = array(
                'status'        => 'Ok',
                'Nombre'        => $nombre,
                'Direccion'     => $dire,
                'Telefono'      => $telf,
                'Celular'       => $telcel,
                'Correo'        => $email,
                'Asesor'        => empty($asesor) ? $ase_cli : $asesor,
                'ase_cli'       => $ase_id,
                'ced_cli'       => $data['ced_cli'],
                'medio'         => $medio,
                'empresa'       => $empresa,
                'observaciones' => $observaciones,
                'cedula'        => $cedula,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'msg'    => print_r($stmt->errorInfo()) . ' Error',
            );
        }

        return $data;
    }

    public function getUsuariosTercero($data)
    {
        $sql = "SELECT
                  it.iduser_tr,
                  it.rol_tr,
                  us.`Nombres`,
                  us.`apellidos`,
                  us.`iduser`
                FROM
                  inmueblesterceros it,
                  usuarios us
                WHERE it.inmob_tr = :inmo
                  AND it.idinm_tr = :inmu
                  AND it.`rol_tr` = :rol
                  AND us.`iduser` = it.`iduser_tr`";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':inmo', $data['inmo']);
        $stmt->bindParam(':inmu', $data['inmu']);
        $stmt->bindParam(':rol', $data['tercero']);

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $registros[] = array(
                    'id'     => $row['iduser'],
                    'Nombre' => ucfirst(strtolower(utf8_encode($row['Nombres'] . ' ' . $row['apellidos']))),
                );
            }
            $data = array(
                'status' => 'Ok',
                'data'   => $registros,
            );
            return $data;
        } else {
            $data = array(
                'status' => 'Error',
                'msg'    => print_r($stmt->errorInfo() . 'Error'),
            );
            return $data;
        }
    }

    public function getHistorialCitas($data)
    {

        $registros = array();
        $sql       = "SELECT c.estado, ci.cedula, c.id_cita AS Cita, c.id_asunto, c.id_captador, c.observaciones, u.Id_Usuarios, c.id_inmueble AS id_inmueble, ci.nombre AS Interesado, a.descripcion AS Asunto, c.fechacita AS Fecha, c.horacita AS Hora, i.Direccion, cii.descripcion,cii.id_descripcion, inm.NombreInm, re.resultado, us.Nombres, us.apellidos,re.idconcepto
            FROM cita c
            LEFT JOIN usuarios u ON c.id_captador = u.Id_Usuarios
            LEFT JOIN asunto_cita a ON c.id_asunto = a.id_asunto
            LEFT JOIN clientes_inmobiliaria ci ON c.id_cliente = ci.cedula
            LEFT JOIN retro_cita re ON c.id_cita = re.idcita
            LEFT JOIN cita_inmueble cii ON re.id_descripcion = cii.id_descripcion
            LEFT JOIN inmuebles i ON c.id_inmueble = i.idInm
            LEFT JOIN barrios b ON i.IdBarrio = b.IdBarrios
            INNER JOIN inmobiliaria inm ON c.inmobiliaria = inm.IdInmobiliaria
            INNER JOIN usuarios us ON  c.id_captador = us.Id_Usuarios
            WHERE c.id_cliente=:cedula AND c.inmobiliaria=:inmo AND c.id_asunto NOT IN(7,8)";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':inmo', $_SESSION['IdInmmo']);
        $stmt->bindParam(':cedula', $data['cedula']);

        $exportExcelEnabled = getCampo('usuarios', "where iduser=" . $_SESSION['codUser'], 'descargaexcel');

        if ($stmt->execute()) {
            /*echo $data["cedula"]."-";
            $row2 = $stmt->fetch();
            echo "<pre>";
            print_r($row2);
            echo "</pre>";
            exit;*/
            if ($stmt->rowCount() > 0) {

                while ($row2 = $stmt->fetch()) {
                    //echo " | - | ";
                    $registros[] = array(
                        'cedula'      => $row2['id_cliente'],
                        'Fecha'       => $row2['Fecha'],
                        'Hora'        => $row2['Hora'],
                        'id_asunto'   => getCampo('asunto_cita', "where id_asunto=" . $row2['id_asunto'], 'descripcion'),
                        'id_inmueble' => $row2['id_inmueble'],
                        'idconcepto'  => getCampo('ConceptosCita', "where IdConcepto=" . $row2['idconcepto'], 'Concepto'),
                        'Nombres'     => ucwords(strtolower($row2['Nombres'])) . " " . ucwords(strtolower($row2['apellidos'])),
                        'resultado'   => $row2['resultado'],

                    );
                }

            }
            $data = array(
                'status'             => 'Ok',
                'data'               => $registros,
                'exportExcelEnabled' => $exportExcelEnabled,
            );
            return $data;
        } else {
            $data = array(
                'status' => 'Error',
                'msg'    => print_r($stmt->errorInfo() . 'Error'),
            );
            return $data;
        }

    }

    public function getHistorialDemanda($data)
    {

        $sql = "SELECT d.IdDemanda,d.IdCiudad,d.IdGestion,d.IdTipoInm,d.IdDestinacion,g.NombresGestion,dd.Nombre,
       t.Descripcion AS tipo,d.ValFinal,d.ValInicial,d.FechaD,ciu.Nombre AS ciudad,d.Descripcion
        FROM demandainmuebles d
        LEFT JOIN ciudad ciu ON d.IdCiudad=ciu.IdCiudad
        LEFT JOIN gestioncomer g ON d.IdGestion=g.IdGestion
        LEFT JOIN destinacion dd ON d.IdDestinacion=dd.IdDestinacion
        LEFT JOIN tipoinmuebles t ON d.IdTipoInm=t.idTipoInmueble WHERE d.CedulaIInt= :cedula";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':cedula', $data['cedula']);

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $registros[] = array(
                    'ciudad'         => $row['ciudad'],
                    'NombresGestion' => $row['NombresGestion'],
                    'tipo'           => $row['tipo'],
                    'Nombre'         => $row['Nombre'],
                    'ValFinal'       => $row['ValFinal'],
                    'ValInicial'     => $row['ValInicial'],
                    'FechaD'         => $row['FechaD'],
                    'Descripcion'    => $row['Descripcion'],

                );
            }
            $data = array(
                'status' => 'Ok',
                'data'   => $registros,
            );
            return $data;
        } else {
            $data = array(
                'status' => 'Error',
                'msg'    => print_r($stmt->errorInfo() . 'Error'),
            );
            return $data;
        }

    }

    public function saveLogMaestroCliente($data)
    {

        if ($data['antes'] != $data['actual']) {

            $sql = "insert into log_maestro_cliente (id_mastercliente,campo,id_cliente,id_user,antes,actual,fecha,hora,ip) VALUES (:id_mastercliente,:campo,:id_cliente,:id_user,:antes,:actual,:fecha,:hora,:ip)";

            $hoyFecha = date("Y-m-d");
            $hoyHora  = date("H:i:s");
            $ip       = $_SERVER['REMOTE_ADDR'];

            $stmt = $this->connPDO->prepare($sql);

            $stmt->bindParam(':id_mastercliente', consecutivo('id_mastercliente', 'log_maestro_cliente'));
            $stmt->bindParam(':campo', $data['campo']);
            $stmt->bindParam(':id_user', $_SESSION["codUser"]);
            $stmt->bindParam(':id_cliente', $data["cedula"]);
            $stmt->bindParam(':antes', $data['antes']);
            $stmt->bindParam(':actual', $data['actual']);
            $stmt->bindParam(':fecha', $hoyFecha);
            $stmt->bindParam(':hora', $hoyHora);
            $stmt->bindParam(':ip', $ip);
            if ($stmt->execute()) {
                $data = array(
                    'status' => 'Ok',
                    'msg'    => 'Maestro Cliente guardado correctamente!',
                );
            } else {
                $data = array(
                    'status' => 'Error',
                    'msg'    => print_r($stmt->errorInfo()),
                );
            }

            return $data;

        }
    }
    public function getDemandaUser($data)
    {
        $sql = "SELECT IdDemanda FROM demandainmuebles WHERE CedulaIInt = :numced AND IdInmobiliaria = :inmo";

        $registros = array();

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':numced', $data['numced']);
        $stmt->bindParam(':inmo', $_SESSION['IdInmmo']);

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $registros[] = $row['IdDemanda'];
            }

            return array(
                'status'    => 'Ok',
                'registros' => $registros,
            );
        } else {
            return array(
                'status' => 'Error',
                'msg'    => print_r($stmt->errorInfo()),
            );
        }

    }

    public function getDataDemanda($data)
    {
        $sql = "SELECT * FROM demandainmuebles where IdDemanda = :iddemanda";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':iddemanda', $data['iddemanda']);

        $barrio          = "";
        $ciudad          = 0;
        $zona            = 0;
        $localidad       = 0;
        $nombreCiudad    = "";
        $nombreZona      = "";
        $nombreLocalidad = "";

        if ($stmt->execute()) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!empty($result['IdCiudad']) && $result['IdCiudad'] > 0) {
                $ciudad = $result['IdCiudad'];
            }
            if (!empty($result['localidad']) && $result['localidad'] > 0) {
                $localidad = $result['localidad'];
            }
            if (!empty($result['IdBarrio']) && $result['IdBarrio'] > 0) {
                $barrio          = getCampo('barrios', 'where IdBarrios = ' . $result['IdBarrio'], 'NombreB');
                $ciudad          = getCampo('barrios', 'where IdBarrios = ' . $result['IdBarrio'], 'IdCiudad');
                $nombreCiudad    = getCampo('ciudad', ' where IdCiudad = ' . $ciudad, 'Nombre');
                $zona            = getCampo('barrios', 'where IdBarrios = ' . $result['IdBarrio'], 'IdZona');
                $nombreZona      = getCampo('zonas', ' where IdZona = ' . $zona, 'NombreZ');
                $localidad       = getCampo('barrios', 'where IdBarrios = ' . $result['IdBarrio'], 'IdLocalidad');
                $nombreLocalidad = getCampo('localidad', ' where idlocalidad = ' . $localidad, 'descripcion');
            }
            // $zona = getCampo('barrios', ' where IdBarrios = ' . $result['IdBarrio'], 'IdZona',0);
            $datas = array(
                'cedula'          => $result['CedulaIInt'],
                'tpinmueble'      => $result['IdTipoInm'],
                'gestion'         => $result['IdGestion'],
                'idbarrio'        => $result['IdBarrio'],
                'destinacion'     => $result['IdDestinacion'],
                'ciudad'          => $ciudad,
                'zona'            => $zona,
                'barrio'          => $barrio,
                'localidad'       => $localidad,
                'preciofrom'      => number_format($result['ValInicial'], 0, ',', '.'),
                'precioto'        => number_format($result['ValFinal'], 0, ',', '.'),
                'areafrom'        => $result['AreaInicial'],
                'areato'          => $result['AreaFinal'],
                'callefrom'       => $result['Calle'],
                'calleto'         => $result['AlaCalle'],
                'carrerafrom'     => $result['Carrera'],
                'carrerato'       => $result['AlaCarrera'],
                'estrato'         => $result['estrato_dem'],
                'fechalimite'     => $result['FechaLimite'],
                'nombreCiudad'    => $nombreCiudad,
                'nombreZona'      => $nombreZona,
                'nombreLocalidad' => $nombreLocalidad,
                'descripcion'     => $result['Descripcion'],
            );

            return array('status' => 'Ok', 'registers' => $datas);
        } else {
            return array('status' => 'Ok', 'msg' => print_r($stmt->errorInfo()));
        }
    }

    public function editarDemandaCallCruce($data)
    {
        $codDemanda  = $data['hiddenCodDemanda'];
        $tipinm      = $data['tipinm'];
        $gestion     = $data['gestion'];
        $city        = $data['city'];
        $zona        = $data['zona'];
        $localidad   = $data['localidad'];
        $barrio      = $data['idbarrio'];
        $preciofrom  = str_replace('.', '', $data['preciofrom']);
        $precioto    = str_replace('.', '', $data['precioto']);
        $areafrom    = str_replace('.', '', $data['areafrom']);
        $areato      = str_replace('.', '', $data['areato']);
        $cllfrom     = $data['cllfrom'];
        $cllto       = $data['cllto'];
        $crafrom     = $data['crafrom'];
        $crato       = $data['crato'];
        $estrato     = $data['estrato'];
        $fechaLimite = $data['fechaLimite'];
        $descripcion = $data['descripcion'];
        $destinacion = $data['destinacion'];

        $sql = "UPDATE demandainmuebles
                SET IdTipoInm = :tipinm,
                IdGestion = :gestion,
                IdCiudad = :city,
                localidad = :localidad,
                IdBarrio = :barrio,
                ValInicial = :preciofrom,
                ValFinal = :precioto,
                AreaInicial = :areafrom,
                AreaFinal = :areato,
                Calle = :cllfrom,
                AlaCalle = :cllto,
                Carrera = :crafrom,
                AlaCarrera = :crato,
                estrato_dem = :estrato,
                FechaLimite = :fechaLimite,
                Descripcion = :descripcion,
                IdDestinacion = :destinacion
                WHERE IdDemanda = :codDemanda";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':tipinm', $tipinm);
        $stmt->bindParam(':gestion', $gestion);
        $stmt->bindParam(':city', $city);
        $stmt->bindParam(':localidad', $localidad);
        $stmt->bindParam(':barrio', $barrio);
        $stmt->bindParam(':preciofrom', $preciofrom);
        $stmt->bindParam(':precioto', $precioto);
        $stmt->bindParam(':areafrom', $areafrom);
        $stmt->bindParam(':areato', $areato);
        $stmt->bindParam(':cllfrom', $cllfrom);
        $stmt->bindParam(':cllto', $cllto);
        $stmt->bindParam(':crafrom', $crafrom);
        $stmt->bindParam(':crato', $crato);
        $stmt->bindParam(':estrato', $estrato);
        $stmt->bindParam(':codDemanda', $codDemanda);
        $stmt->bindParam(':fechaLimite', $fechaLimite);
        $stmt->bindParam(':descripcion', $descripcion);
        $stmt->bindParam(':destinacion', $destinacion);

        if ($stmt->execute()) {
            return array(
                'status' => 'Ok',
                'msg'    => 'Demanda actualizada correctamente!',
            );
        } else {
            return array(
                'status' => 'Error',
                'msg'    => print_r($stmt->errorInfo()),
            );
        }
    }

    public function crearDemandaCallCruce($data)
    {
        $codDemanda  = $data['hiddenCodDemanda'];
        $tipinm      = $data['tipinm'];
        $gestion     = $data['gestion'];
        $city        = $data['city'];
        $zona        = $data['zona'];
        $localidad   = $data['localidad'];
        $barrio      = $data['idbarrio'];
        $preciofrom  = str_replace('.', '', $data['preciofrom']);
        $precioto    = str_replace('.', '', $data['precioto']);
        $areafrom    = str_replace('.', '', $data['areafrom']);
        $areato      = str_replace('.', '', $data['areato']);
        $cllfrom     = $data['cllfrom'];
        $cllto       = $data['cllto'];
        $crafrom     = $data['crafrom'];
        $crato       = $data['crato'];
        $estrato     = $data['estrato'];
        $fechaLimite = $data['fechaLimite'];
        $descripcion = $data['descripcion'];
        $fechaD      = $data['fechaD'];
        $numced      = $data['numced'];
        $destinacion      = $data['destinacion'];
        $cons        = consecutivo('IdDemanda', 'demandainmuebles');

        $sql = "INSERT INTO demandainmuebles (IdDemanda, FechaD, IdPromotor, FechaLimite, CedulaIInt,Calle,AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,Descripcion,Estado,clave,IdCiudad,localidad,IdBarrio,IdInmobiliaria,grupo_dem,estrato_dem)
        VALUES(:cons,:FechaD,:IdPromotor,:fechaLimite,:numced,:cllfrom,:cllto,:crafrom,:crato,:gestion,:destinacion,:tipinm,:preciofrom,:precioto,:areafrom,:areato,:descripcion,0,0000,:city,:localidad,:barrio,:idinmo,1,:estrato)";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':cons', $cons);
        $stmt->bindParam(':FechaD', $fechaD);
        $stmt->bindParam(':IdPromotor', $_SESSION['Id_Usuarios']);
        $stmt->bindParam(':fechaLimite', $fechaLimite);
        $stmt->bindParam(':numced', $numced);
        $stmt->bindParam(':cllfrom', $cllfrom);
        $stmt->bindParam(':cllto', $cllto);
        $stmt->bindParam(':crafrom', $crafrom);
        $stmt->bindParam(':crato', $crato);
        $stmt->bindParam(':gestion', $gestion);
        $stmt->bindParam(':tipinm', $tipinm);
        $stmt->bindParam(':preciofrom', $preciofrom);
        $stmt->bindParam(':precioto', $precioto);
        $stmt->bindParam(':areafrom', $areafrom);
        $stmt->bindParam(':areato', $areato);
        $stmt->bindParam(':descripcion', $descripcion);
        $stmt->bindParam(':city', $city);
        $stmt->bindParam(':localidad', $localidad);
        $stmt->bindParam(':barrio', $barrio);
        $stmt->bindParam(':idinmo', $_SESSION['IdInmmo']);
        $stmt->bindParam(':estrato', $estrato);
        $stmt->bindParam(':destinacion', $destinacion);
        if ($stmt->execute()) {
            $idinsertado = $this->connPDO->lastInsertId();
            return array(
                'status' => 'Ok',
                'msg'    => 'Demanda creada correctamente!, Codigo: ' . $idinsertado,
                'codigo' => $idinsertado,
            );
        } else {
            return array(
                'status' => 'Error',
                'msg'    => print_r($stmt->errorInfo()),
            );
        }
    }

    public function condiDemanda($data)
    {
        $sql = "SELECT
                  inm.ValorVenta,
                  inm.ValorCanon,
                  inm.Administracion,
                  inm.Destinacion,
                  inm.ComiVenta,
                  inm.ComiArren,
                  inm.IdProcedencia,
                  inm.IdDestinacion,
                  inm.idEstadoinmueble,
                  inm.IdTpInm,
                  inm.ValComiVenta,
                  inm.ValComiArr,
                  inm.politica_comp,
                  inm.IdInmobiliaria,
                  it.iduser_tr
                FROM
                  inmnvo inm,inmueblesterceros it
                WHERE inm.idInm = :inmu
                    AND it.inmob_tr = inm.IdInmobiliaria
                    AND it.idinm_tr = inm.codinm
                    AND it.rol_tr = 3
                    LIMIT 0,1";

        $nprom        = "";
        $aprom        = "";
        $promotornoms = "";
        $fotoAsesor   = 'http://www.simiinmobiliarias.com/sj/img/def_avatar.gif';

        $stmt = $this->connPDO->prepare($sql);
        $stmt->bindParam(':inmu', $data['idInmueble']);

        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                $row              = $stmt->fetch(PDO::FETCH_ASSOC);
                $inm_vventa       = $row['ValorVenta'];
                $inm_vcanon       = $row['ValorCanon'];
                $inm_admon        = $row['Administracion'];
                $inm_destina      = $row['IdDestinacion'];
                $inm_comventa     = $row['ComiVenta'];
                $inm_comarren     = $row['ComiArren'];
                $inm_proced       = $row['IdProcedencia'];
                $inm_estado       = $row['idEstadoinmueble'];
                $tipoinmuebleed   = $row['IdTpInm'];
                $inm_ValComiVenta = $row['ValComiVenta'];
                $inm_ValComiArr   = $row['ValComiArr'];
                $politica_comp    = $row['politica_comp'];
                $IdInmobiliaria   = $row['IdInmobiliaria'];
                if ($politica_comp != "") {
                    $porceVenta     = $inm_comventa;
                    $vcominven      = $inm_ValComiVenta;
                    $porcenarriendo = $inm_comarren;
                    $vcominarr      = $inm_ValComiArr;
                    $idinmueble     = $idInmueble;

                    if (!empty($row['iduser_tr']) && $row['iduser_tr'] > 0) {
                        $nprom        = ucwords(strtolower(getCampo('usuarios', "where iduser='" . $row['iduser_tr'] . "'", 'Nombres')));
                        $aprom        = ucwords(strtolower(getCampo('usuarios', "where iduser='" . $row['iduser_tr'] . "'", 'apellidos')));
                        $promotornoms = "$nprom $aprom";

                        $fotoSoli = getCampo('usuarios', "where iduser='" . $row['iduser_tr'] . "'", 'Foto');
                        if ($fotoSoli != "" && strlen($fotoSoli) > 6) {
                            $fotoAsesor = 'http://www.simiinmobiliarias.com/mcomercialweb/' . $fotoSoli;
                        }
                    }

                    if ($this->val_image($fotoAsesor) == 404 || $this->val_image($fotoAsesor) == 403 || $this->val_image($fotoAsesor) == 500) {
                        $fotoAsesor = 'http://www.simiinmobiliarias.com/sj/img/def_avatar.gif';
                    }

                    $logoEmpresa = getCampo('clientessimi', "where IdInmobiliaria='" . $IdInmobiliaria . "'", 'logo');

                    $logoInmo = 'http://www.simiinmobiliarias.com/' . str_replace('../', '', $logoEmpresa);

                    if ($this->val_image($logoInmo) == 404 || $this->val_image($logoInmo) == 403 || $this->val_image($logoInmo) == 500) {
                        $logoInmo = 'http://www.simiinmobiliarias.com/sj/img/defaultInmo.jpg';
                    }

                    $pVenta = ($inm_comventa * 100);
                    $prenta = ($inm_comarren * 100);

                    if ($inm_ValComiVenta > 0) {
                        $vVenta = number_format($inm_ValComiVenta);
                    } else {
                        $vVenta = number_format($inm_vventa * $inm_comventa);
                    }
                    if ($inm_ValComiArr > 0) {
                        $vRenta = number_format($inm_ValComiArr);
                    } else {
                        $vRenta = number_format($inm_vcanon * $inm_comarren);
                    }
                    $registers = array(
                        "Nombre"      => $promotornoms,
                        "fotopromo"   => $fotoAsesor,
                        "venta"       => $vVenta,
                        "renta"       => $vRenta,
                        "logoEmpresa" => $logoInmo,
                        "politicaC"   => ucfirst(strtolower(utf8_encode($politica_comp))),
                    );

                    $return = array(
                        'status'    => 'Ok',
                        'registers' => $registers,
                    );

                } else {
                    $return = array(
                        'status' => 'Error',
                        'msg'    => 'El inmueble no cuenta con Politicas para compartir',
                    );
                }
            } else {
                $return = array(
                    'status' => 'Error',
                    'msg'    => 'Inmueble no encontrado.',
                );
            }
        } else {
            $return = array(
                'status' => 'Error',
                'msg'    => print_r($stmt->errorInfo()) . ' error',
            );
        }

        return $return;

    }

    public function CrearSolicitudCompartir($data)
    {
        $conse        = consecutivo('conse_cm', 'condiciones_compartir_inm');
        $usuario      = $_SESSION['Id_Usuarios'];
        $inmobiliaria = $_SESSION['IdInmmo'];
        $idinmu       = $data['codInmu'];
        $codDemanda   = $data["codDeman"];
        $f_sis        = date('Y-m-d');
        $h_sis        = date('H:i:s');
        $ip           = $_SERVER['REMOTE_ADDR'];
        $estado       = 0;
        $comiVenta    = getCampo('inmnvo', "where idInm='" . $idinmu . "'", 'ComiVenta');
        $comiArren    = getCampo('inmnvo', "where idInm='" . $idinmu . "'", 'ComiArren');
        $valComiV     = getCampo('inmnvo', "where idInm='" . $idinmu . "'", 'ValComiVenta');
        $valComiP     = getCampo('inmnvo', "where idInm='" . $idinmu . "'", 'ValComiArr');
        $politicaComp = getCampo('inmnvo', "where idInm='" . $idinmu . "'", 'politica_comp');
        $tipo_negocio = $data['tipo_negocio'];

        $stmt = $this->connPDO->prepare("INSERT INTO condiciones_compartir_inm
                                       (conse_cm,
                                        id_inmob_cm,
                                        id_inmu_cm,
                                        pct_comi_venta,
                                        pct_como_renta,
                                        vlr_comi_venta,
                                        vlr_comi_renta,
                                        condiciones_cm,
                                        cod_deman_cm,
                                        ip_cm,
                                        fecha_cm,
                                        hora_cm,
                                        usu_cm,
                                        tipo_neg_cm)
                                     values
                                     (  :conse_cm,
                                        :inmo,
                                        :inmu,
                                        :comiVenta,
                                        :comiRenta,
                                        :valorVenta,
                                        :valorRenta,
                                        :politicas,
                                        :codDemanda,
                                        :ip_cm,
                                        :fecha_cm,
                                        :hora_cm,
                                        :usu_cm,
                                        :tipo_neg_cm
                                        )
                ");
        if ($stmt->execute(array(
            ":conse_cm"    => $conse,
            ":inmo"        => $inmobiliaria,
            ":inmu"        => "$idinmu",
            ":comiVenta"   => $comiVenta,
            ":comiRenta"   => $comiArren,
            ":valorVenta"  => $valComiV,
            ":valorRenta"  => $valComiP,
            ":politicas"   => "$politicaComp",
            ":codDemanda"  => $codDemanda,
            ":ip_cm"       => "$ip",
            ":fecha_cm"    => "$f_sis",
            ":hora_cm"     => "$h_sis",
            ":usu_cm"      => $usuario,
            ":tipo_neg_cm" => $tipo_negocio,

        ))) {
            return 1;
        } else {
            // echo "\nPDO::errorInfo():\n";
            //             print_r($stmt->errorInfo());
            return 2;

        }
    }

    public function EnvioEmailOferta($idemanda, $idinmueble, $idoferta)
    {
        $sql = "SELECT
                  inm.IdInmobiliaria,
                  it.iduser_tr
                FROM
                  inmnvo inm,inmueblesterceros it
                WHERE inm.idInm = :inmu
                    AND it.inmob_tr = inm.IdInmobiliaria
                    AND it.idinm_tr = inm.codinm
                    AND it.rol_tr = 3
                    LIMIT 0,1";

        $stmt = $this->connPDO->prepare($sql);
        $stmt->bindParam(':inmu', $idinmueble);

        $fotoAsesor = 'http://www.simiinmobiliarias.com/sj/img/def_avatar.gif';

        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                $row            = $stmt->fetch(PDO::FETCH_ASSOC);

                if (!empty($row['iduser_tr']) && $row['iduser_tr'] > 0) {
                    $nprom        = ucwords(strtolower(getCampo('usuarios', "where iduser='" . $row['iduser_tr'] . "'", 'Nombres')));
                    $aprom        = ucwords(strtolower(getCampo('usuarios', "where iduser='" . $row['iduser_tr'] . "'", 'apellidos')));
                    $nombresAsesor = "$nprom $aprom";

                    $correo = getCampo('usuarios', "where iduser='" . $row['iduser_tr'] . "'", 'Correo');

                    $login = getCampo('usuarios', "where iduser='" . $row['iduser_tr'] . "'", 'Login');

                    $fotoSoli = getCampo('usuarios', "where iduser='" . $row['iduser_tr'] . "'", 'Foto');
                    if ($fotoSoli != "" && strlen($fotoSoli) > 6) {
                        $fotoAsesor = 'http://www.simiinmobiliarias.com/mcomercialweb/' . $fotoSoli;
                    }
                }

                if ($this->val_image($fotoAsesor) == 404 || $this->val_image($fotoAsesor) == 403 || $this->val_image($fotoAsesor) == 500) {
                    $fotoAsesor = 'http://www.simiinmobiliarias.com/sj/img/def_avatar.gif';
                }

                $logoEmpresa = getCampo('clientessimi', "where IdInmobiliaria='" . $row['IdInmobiliaria'] . "'", 'logo');

                $logoInmo = 'http://www.simiinmobiliarias.com/' . str_replace('../', '', $logoEmpresa);

                if ($this->val_image($logoInmo) == 404 || $this->val_image($logoInmo) == 403 || $this->val_image($logoInmo) == 500) {
                    $logoInmo = 'http://www.simiinmobiliarias.com/sj/img/defaultInmo.jpg';
                }

                // echo "<pre>";
                // print_r($fotoAsesor);
                // echo "</pre>";die;

                $inmobiliaria=ucwords(strtolower(getCampo('clientessimi', "where IdInmobiliaria='" . $row['IdInmobiliaria'] . "'", 'Nombre')));
                $mail = $this->phpmailer;

                $boton.=' <div style="background-color:#F16D6D; width:100px; font-size:10px;padding:10px 5px 10px 5px; margin:10px 0 0 0; "><a style="color:white;" target="_blank" href="http://www.simiinmobiliarias.com/mwc/negocios/cruce_negocios.php?usrSnd='.$login.'"><div class="btn btn-info">Ingresar</div></a></div>';

                $body='<html> 
                        <head><meta charset="UTF-8">
                            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
                            <title>Seguimiento Demandas</title> 
                                <style>
                                </style>
                        </head>
                        <body>
                            <table align="center"  cellpadding="0" cellspacing="0" width="90%" style="border:0;">
                            <tr>
                                <td bgcolor="" >
                                    <p class="" >
                                    <a><img style="margin:0 auto 0 35%;" width="30%" src="'.$logoInmo.'"></a>
                                    </p>
                                </td>
                             </tr>
                              <tr>
                                <td bgcolor="#337AB7" align="center" style="border:3px #909090 solid;    padding: 15px 0 0px 0;">
                                    <font face="verdana" color="#FFFFFF"><h3>Nueva Oferta Generada para el Inmueble '.$idinmueble.'</h3>
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                    <td bgcolor="#ee4c50" height="">
                            </td>
                            </tr>
                            <tr>
                                <td  bgcolor="#FFFFFF" style="border:3px #909090 solid;padding: 15px 0 0px 0;" align="center">
                                    <div>
                                        <a><img style="height:auto; width:auto; max-height:100px; max-width:100px;" src="'.$fotoAsesor.'"></a>
                                    </div>
                                    
                                 <div>      
                                    <font face="verdana" color="#424242">
                                    <h5>
                                        Sr.(a): '.ucwords(strtolower($nombresAsesor)).', Simiweb le informa que 
                                        <p></p>
                                        El (la) Se&ntilde;or(a) '.ucwords(strtolower($_SESSION['Nombres'])).',  gener&oacute; una Oferta para compartir el Inmueble '.$idinmueble.'
                                        <p></p>
                                        Favor ingrese a  www.simiinmobiliarias.com , para gestionar la Oferta.
                                        '.$boton.'
                                        <p></p>
                                        ';
                                        $correod=($correo == "")?"":'<b style="color:#337AB7">Correo</b>: '.$correo;
                                        $teld=($telefonodemanda == "")?"":'<b style="color:#337AB7">Tel. Fijo</b>:'.$telefonodemanda;
                                        $celd=($celulardemanda="")?"":'<b style="color:#337AB7">Tel. Celular</b>: '.$celulardemanda;

                                        $body .='       
                                        '.$correod.'
                                        '.$teld.'
                                        '.$celulardemanda.'<br />   
                                    </h5>
                                    </font>
                                  </div>
                                  <div>
                                    <font face="verdana" color="#424242">
                                        <p>Cordialmente,</p>
                                        
                                        <p>Tecnolog&iacute;a De Administraci&oacute;n Empresarial Ltda.</p>
                                        
                                        <p style="font-size:12px;">
                                            "Si caminas solo irás mas rápido, si usas el Cruce de Negocios llegarás más lejos." - Proverbio chino
                                         </p>
                                    </font>
                                  </div>
                                </td>
                            </tr>
                    ';

                $mail->IsSMTP();
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = "tls";
                $mail->Host = "smtp.office365.com"; //Servidor de 
                $mail->Port = 587;
                $mail->Username = "noreply@simiinmobiliarias.com";//usuario perteneciente al servidor
                $mail->Password = "Desarrollo.2015";
                $mail->From = "noreply@simiinmobiliarias.com";
                $mail->FromName = "Agente Virtual SimiWeb";
                // $address = $correo;
                // $mail->AddAddress($address, $nombresAsesor);
                // $mail->AddBCC($correo, $correo);
                // $mail->AddCC($correo,$correo);
                $mail->AddBCC("desarrolloweb@tae-ltda.com","Desarrolloweb TAE");
                $mail->AddBCC("desarrolloweb2@tae-ltda.com","Desarrolloweb TAE");
                $mail->AddBCC("desarrolloweb4@tae-ltda.com","Desarrolloweb TAE");
                $mail->AddBCC("evelioocampo@tae-ltda.com","Presidente TAE Ltda");       
                $mail->Subject = "La Inmobiliaria ".ucwords(strtolower($inmobiliaria))." Se Encuentra Interesada en Compartir el Inmueble ".$idinmueble;
                $mail->MsgHTML($body);
                $mail->IsHTML(true);
                $mail->SMTPDebug=0;
                if(!$mail->Send()) {
                    $return = 2;
                } else {
                    $return = 1;
                }

            } else {
                $return = 2;
            }
        } else {
            $return = 2;
        }

        return $return;
    }

    public function getLogPoliticas(){
        $sql = "SELECT * FROM log_seg_activa_politicas WHERE user_poli = :user_poli";

        $stmt = $this->connPDO->prepare($sql);

        $stmt->bindParam(':user_poli', $_SESSION['codUser']);

        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                return 1;
            }else{
                return 2;
            }
        }
    }

    private function val_image($url)
    {
        $fp   = curl_init($url);
        $ret  = curl_setopt($fp, CURLOPT_RETURNTRANSFER, 1);
        $ret  = curl_setopt($fp, CURLOPT_TIMEOUT, 30);
        $ret  = curl_exec($fp);
        $info = curl_getinfo($fp, CURLINFO_HTTP_CODE);
        curl_close($fp);
        return $info;
    }

}
