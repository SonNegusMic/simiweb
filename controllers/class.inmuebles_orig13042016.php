<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

@include("../funciones/connPDO.php");
class Inmuebles
{
    public function __construct($conn=""){
		$this->db=$conn;
	}
    
    private $id_usuario;
    
    public function listadoInmuebles($idEmpresa,$IdGestion,$codInmu='')
    {
        $cond='';
		if($codInmu)
		{
			$cond="and Codigo_Inmueble=?";
		}
		if(($consulta=$this->db->prepare(" select 
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo
		from datos_call
        where IdInmobiliaria=?
        and IdGestion=?
		$cond
		limit 0,20")))
		{  	
			if($codInmu)
			{
				$consulta->bind_param("iis",$idEmpresa,$IdGestion,$codInmu);
			}
			else
			{
				$consulta->bind_param("ii",$idEmpresa,$IdGestion);
			}
			 
				$consulta->execute();
//				echo "$qry";
				$arreglo1 = array();
				if($consulta->bind_result($IdInmobiliaria,$Administracion,$IdGestion,$Estrato,$IdTpInm,
		$Codigo_Inmueble,$Tipo_Inmueble,$Venta,$Canon,$descripcionlarga,
		$Barrio,$Gestion,$AreaConstruida,$AreaLote,$latitud,
		$longitud,$EdadInmueble,$NombreInmo,$logo))
				while($consulta->fetch())
				{
					 $arreglo[0]=$IdInmobiliaria;//0
                     $arreglo[1]=$Administracion;//1
                     $arreglo[2]=$IdGestion;//2
                     $arreglo[3]=$Estrato;//3
                     $arreglo[4]=$IdTpInm;//4
                     $arreglo[5]=$Codigo_Inmueble;
                     $arreglo[6]=$Tipo_Inmueble;
                     $arreglo[7]=$Venta;
					 $arreglo[8]=$Canon;
					 $arreglo[9]=$descripcionlarga;
					 $arreglo[10]=$Barrio;
					 $arreglo[11]=$Gestion;
					 $arreglo[12]=$AreaConstruida;
					 $arreglo[13]=$AreaLote;
					 $arreglo[14]=$latitud;
					 $arreglo[15]=$longitud;
					 $arreglo[16]=$EdadInmueble;
					 $arreglo[17]=$NombreInmo;
					 $arreglo[18]="http://www.simiinmobiliarias.com/".str_replace('../','',$logo);
					 
					 $arreglo1[] = $arreglo;
                     
 				}
               // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
               // print_r($arreglo1)."------------------";
                return $arreglo1;
		}
		else
		{
			echo  "error --".$conn->error;
		}
        
    }
	public function fotosInmueble($codInmueble)
    {
        if(($consulta=$this->db->prepare(" select 
		Foto1
		from fotos
        where idInm=?")))
		{  	
			if(!$consulta->bind_param("s",$codInmueble))
			{
				echo "error bind";
			}
			else
			{ 
				$consulta->execute();
//				echo "$qry";
				$arreglo1 = array();
				if($consulta->bind_result($Foto1))
				while($consulta->fetch())
				{
					 $arreglo[0]=$IdInmobiliaria;//0
                     $urlFoto="http://www.simiinmobiliarias.com/mcomercialweb/".$Foto1;
                     
 				}
               // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
               // print_r($arreglo1)."------------------";
                echo $urlFoto;
                
			}
		}
		else
		{
			echo  "error img".$conn->error;
		}
    }
	public function inmueblesDisponibles($IdInmob)
    {
        $condperfil="";
		$ase=$_SESSION['Id_Usuarios'];
		if($_SESSION['IdInmmo']==632)
		{
			$condperfil="and IdPromotor='$ase'";
		}
		$w_conexion = new MySQL();
		$sql="Select count(Codigo_Inmueble) as tot
			   			From datos_call
						where IdInmobiliaria=$IdInmob
						$condperfil";
			
		$res=$w_conexion->ResultSet($sql);
		while($f=$w_conexion->FilaSiguienteArray($res))
		{
			$tot	= $f['tot'];
		}
		return $tot;
    }
	public function inmueblesSinFotos($IdInmob)
    {
        $cade="";
		$condperfil="";
		$ase=$_SESSION['Id_Usuarios'];
		if($_SESSION['IdInmmo']==632)
		{
			$condperfil="and IdPromotor='$ase'";
		}
		$arreglo1=array();
		$w_conexion = new MySQL();
		$sql="SELECT i.idInm,i.IdInmobiliaria,f.Foto1  
				FROM inmuebles i 
				LEFT JOIN fotos f ON i.idInm=f.idInm 
				WHERE i.IdInmobiliaria = '".$IdInmob."' 
				AND (f.Foto1 IS NULL OR LENGTH(f.Foto1)=0)  
				AND i.idEstadoinmueble  = 2
				$condperfil";
			
		$res=$w_conexion->ResultSet($sql);
		while($f=$w_conexion->FilaSiguienteArray($res))
		{
			$idInm	= $f['idInm'];
			$cade="'".$idInm."',".$cade;
		}
		return substr($cade,0,-1);
    }
	
	public function inmueblesSinVideos($IdInmob)
    {
        $cade="";
		$condperfil="";
		$ase=$_SESSION['Id_Usuarios'];
		if($_SESSION['IdInmmo']==632)
		{
			$condperfil="and IdPromotor='$ase'";
		}
		$arreglo1=array();
		$w_conexion = new MySQL();
		$sql="SELECT idInm 
						FROM inmuebles  
						WHERE IdInmobiliaria = '".$IdInmob."' 
						AND (linkvideo IS NULL OR LENGTH(linkvideo)=0)  
						AND idEstadoinmueble  = 2
						$condperfil";
			
		$res=$w_conexion->ResultSet($sql);
		while($f=$w_conexion->FilaSiguienteArray($res))
		{
			$idInm	= $f['idInm'];
			$cade="'".$idInm."',".$cade;
		}
		return substr($cade,0,-1);
    }
	public function inmueblesIncompletos($IdInmob)
    {
        $w_conexion = new MySQL();
		$condperfil="";
		$ase=$_SESSION['Id_Usuarios'];
		if($_SESSION['IdInmmo']==632)
		{
			$condperfil="and IdPromotor='$ase'";
		}
		$sql="Select count(Codigo_Inmueble) as tot
			   			From datos_call
						where IdInmobiliaria=$IdInmob
						$condperfil";
			
		$res=$w_conexion->ResultSet($sql);
		while($f=$w_conexion->FilaSiguienteArray($res))
		{
			$tot	= $f['tot'];
		}
		return $tot;
    }
	public function listadoInmuebles2($inmob,$codInmuebles,$nro_inm)
    {
        $cond="";
		if($codInmuebles)
		{
			$cond="and Codigo_Inmueble in ($codInmuebles)";
		}
		if($nro_inm)
		{
			$cond="and Codigo_Inmueble in ('1-".$nro_inm."')";
		}
		$condperfil="";
		$ase=$_SESSION['Id_Usuarios'];
		if($_SESSION['IdInmmo']==632 or $_SESSION['IdInmmo']==631)
		{
			$condperfil="and IdPromotor='$ase'";
		}
		$w_conexion = new MySQL();
		$sql="SELECT 
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo,IdPropietario, Nombrepro,IdBarrios,Ciudad,
		NombreCap,IdPromotor,NombreProm,IdCaptador
		from datos_call
        where IdInmobiliaria=$inmob
        $cond
		$condperfil
		#limit 0,20";
		//echo $sql;
		$res=$w_conexion->ResultSet($sql);
		while($f=$w_conexion->FilaSiguienteArray($res))
		{
			 $arreglo[0]=$f['IdInmobiliaria'];//0
			 $arreglo[1]=$f['Administracion'];//1
			 $arreglo[2]=$f['IdGestion'];//2
			 $arreglo[3]=$f['Estrato'];//3
			 $arreglo[4]=$f['IdTpInm'];//4
			 $arreglo[5]=$f['Codigo_Inmueble'];
			 $arreglo[6]=$f['Tipo_Inmueble'];
			 $arreglo[7]=$f['Venta'];
			 $arreglo[8]=$f['Canon'];
			 $arreglo[9]=$f['descripcionlarga'];
			 $arreglo[10]=$f['Barrio'];
			 $arreglo[11]=$f['Gestion'];
			 $arreglo[12]=$f['AreaConstruida'];
			 $arreglo[13]=$f['AreaLote'];
			 $arreglo[14]=$f['latitud'];
			 $arreglo[15]=$f['longitud'];
			 $arreglo[16]=$f['EdadInmueble'];
			 $arreglo[17]=$f['NombreInmo'];
			 $arreglo[18]="http://www.simiinmobiliarias.com/".str_replace('../','',$f['logo']);
			 $arreglo[19]=$f['IdPropietario'];
			 $arreglo[20]=$f['Nombrepro'];
			 $arreglo[21]=$f['IdBarrios'];
			 $arreglo[22]=$f['Ciudad'];
			 $arreglo[23]=$f['IdCaptador'];
			 $arreglo[24]=$f['NombreCap'];
			 $arreglo[25]=$f['IdPromotor'];
			 $arreglo[26]=$f['NombreProm'];
			// $arreglo[27]=$f['codinterno'];
			 
			 $arreglo1[] = $arreglo;
		}
			return $arreglo1;
		
        
    }

    ////json para api Afydi

     public function listadoInmueblesAfydi($idEmpresa,$codInmu,$usr_act)
    {
      
		$dInmuebles		= new Inmuebles();
		$avisoVentana=0;
		
		$pservidumbre				= $dInmuebles->evalua_carac(23,$codInmu,'Cantidad','SERVIDUMBRE');
		$pindependiente				= $dInmuebles->evalua_carac(23,$codInmu,'Cantidad','INDEPENDIENTE',0);
		$comunal					= $dInmuebles->evalua_carac(23,$codInmu,'Cantidad','COMUNAL',0);
		$pvisitantes				= $dInmuebles->evalua_carac(23,$codInmu,'Cantidad','VISITANTES',0);
		$pcubierto					= $dInmuebles->evalua_carac(23,$codInmu,'Cantidad','CUBIERTO');
		$ascensor					= $dInmuebles->evalua_carac(25,$codInmu,'Cantidad','ASCENSOR');
		$banos   					= $dInmuebles->trae_carac(16,$codInmu,'Cantidad',utf8_decode('baño'),0); 
		$alcobas   					= $dInmuebles->trae_carac(15,$codInmu,'Cantidad','alcoba',0); 
		$nivel   					= $dInmuebles->trae_carac(36,$codInmu,'Cantidad','nivel',0);
		$inm_destacado				= getCampo('settings_portales',"where inmu_p='".$codInmu."' and tipo_p=4",'vlr_p');
		$sucursal=0;
		
			
		if(($consulta=$this->db->prepare(" select 
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		idInm,ValorVenta,ValorCanon,descripcionlarga,
		IdBarrio,IdGestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,Direccion, 
		IdPromotor,Fondo,Frente,
		Restricciones,idEstadoinmueble,IdDestinacion,IdPropietario,
		linkvideo,fingreso,FConsignacion,NoMatricula,ComiVenta,
		ComiArren,IdProcedencia
		from inmuebles
        where IdInmobiliaria=?
        and idInm=?
		#$cond
		limit 0,20")))
		{  	
			if($codInmu)
			{
				$consulta->bind_param("is",$idEmpresa,$codInmu);
			}
			else
			{
				$consulta->bind_param("is",$idEmpresa,$codInmu);
			}
			 
				$consulta->execute();
//				echo "$qry";
				

				if($consulta->bind_result($IdInmobiliaria,$Administracion,$IdGestion,$Estrato,$IdTpInm,
		$idInm,$ValorVenta,$ValorCanon,$descripcionlarga,
		$IdBarrio,$IdGestion,$AreaConstruida,$AreaLote,$latitud,
		$longitud,$EdadInmueble,$Direccion,
		$IdPromotor,$Fondo,$Frente,$Restricciones,$idEstadoinmueble,$IdDestinacion,$IdPropietario,
		$linkvideo,$fingreso,$FConsignacion,$NoMatricula,$ComiVenta,
		$ComiArren,$IdProcedencia))

		


		while($consulta->fetch())
		{
			$IdCiudad 	= getCampo('barrios',"where IdBarrios=".$IdBarrio,'IdCiudad');
			$IdZona		= getCampo('barrios',"where IdBarrios=".$IdBarrio,'IdZona');
			if($IdGestion==5)
			{
				$porc_comision=$ComiVenta;
			}
			else
			{
				$porc_comision=$ComiArren;
			}
			if($IdProcedencia==58)
			{
				$avisoVentana=1;
			}
			$IdTpInmAfydi = (getCampo('tipoinmuebles',"where idTipoInmueble=$IdTpInm",'conafydi',0)=="")?1:getCampo('tipoinmuebles',"where idTipoInmueble=$IdTpInm",'conafydi',0);
			$IdGestionAfydi		= getCampo('gestioncomer',"where IdGestion=$IdGestion",'convgafydi',0);
			$IdDestinacionAfydi	= getCampo('destinacion',"where IdDestinacion=$IdDestinacion",'convdafydi',0);
			$ZonaAfydi			= getCampo('zonas',"where IdZona=$IdZona",'convzafydi',0);
			$barrioComun		= getCampo('barrios',"where IdBarrios=$IdBarrio",'NombreB',0);
			$dateIngreso=($fingreso=="")?"0":$fingreso;
			$inm_destacado=($inm_destacado==0)?"0":$inm_destacado;
			$inm=explode("-",$idInm);
			$nivel = ($nivel=="")?1:$nivel;
			$linkvideo = ($linkvideo == "")?" ":$linkvideo;
			$ValorCanon = ($ValorCanon == "")?0:$ValorCanon;

			 $arreglo[]= array(
				"codpro"                => $inm[1],
				"address"               => $Direccion,
				"city"                  => $IdCiudad,
				"zone"                  => $IdZona,
				"neighborhood"          => $IdBarrio,
				"barrio_comun"          => $barrioComun,
				"type"                  => $IdTpInmAfydi,
				"biz"                   => $IdGestionAfydi,
				"area_cons"             => $AreaConstruida,
				"area_lot"              => $AreaLote,
				"floor"                 => $nivel,
				"bedrooms"              => $alcobas,
				"bathrooms"             => $banos,
				"stratum"               => $Estrato,
				"rent"                  => $ValorCanon,
				"administration"        => $Administracion,
				"saleprice"             => $ValorVenta,
				"description"           => 'descripcionlarga',
				"desc_eng"              => "",
				"desc_fre"              => "",
				"resctrictions"         => $Restricciones,
				"comment"               => "",
				"comment2"              => "",
				"asesor"                => $IdPromotor,
				"video"                 => $linkvideo,
				"parking"               => $pindependiente,
				"parking_covered"       => $pcubierto,
				"consignation_date"     => $FConsignacion.$IdPropietario,
				"registry_date"         => $dateIngreso,
				"longitude"             => $longitud,
				"latitude"              => $latitud,
				"status"                => $idEstadoinmueble,
				"build_year"            => $EdadInmueble,
				"destacado"             => $inm_destacado,
				"user"                  => $usr_act,
				"branch"                => $idEmpresa.$sucursal,
				"window_sign"           => $avisoVentana, 
				"front"                 => $Frente,
				"rear"                  => "",
				"private_area"          => "",
				"destinacion"           => "$IdDestinacionAfydi",
				"commission_percentage" => "",
				"matricula_number"      => "",
				"referencia"            => "",
				"images"                => $dInmuebles->fotosInmuebleAfydi($codInmu)
             );
			}
       // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
       // print_r($arreglo1)."------------------";
			//echo json_encode($arreglo);
			return $arreglo;
        //return json_encode($arreglo);
		}
		else
		{
			echo  "error --".$conn->error;
		}
        
    }
    public function fotosInmuebleAfydi($codInmueble)
    {
        $w_conexion = new MySQL();
		$urlFoto="http://www.simiinmobiliarias.com/mcomercialweb/";
        
        $consulta1="select 
		Foto1,Foto2,Foto3,Foto4,Foto5, 
		Foto6,Foto7,Foto8,Foto9,Foto10
		from fotos
        where idInm='$codInmueble'";
		$res=$w_conexion->ResultSet($consulta1);
			// if(!$consulta1->bind_param("s",$codInmueble))
			// {
			// 	echo "error bind";
			// }
			// else
			// { 
				
				while($ff=$w_conexion->FilaSiguienteArray($res))
				{
					 
					for($p=1;$p<11;$p++)
					{	
						 $Foto = $ff['Foto'.$p];
						 $urlFoto.$Foto1;
						 $arreglo[]=array(
						 		"id" 			=>$p,
						 		"order" 		=>$p,
						 		"imagename" 	=>'Foto'.$p,
						 		"imageurl"  	=>$urlFoto.$Foto,
						 		"thumburl" 		=>$urlFoto.$Foto,
						 		"s3bucket" 		=>"",
						 		"inmueble" 		=>$codInmueble,
						 		"comments" 		=>"",
						 		"created_at" 	=>date('Y-m-d'),
						 		"updated_at" 	=> date('Y-m-d')
						 );
					}
 			//	}
			//print_r( $arreglo);
			 return $arreglo;
		}
	}
	function trae_carac($grupo,$inmueble,$campo,$especifica,$muestra="")
{
	global $w_conexion;
	$cond="";
	if($especifica)
	{
		$cond .=" and Descripcion like '%$especifica%'";
	}
$sqlconsulta="SELECT  $campo as aaa
			 FROM detalleinmueble 
			 INNER JOIN maestrodecaracteristicas 
			 ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica 
			 AND  maestrodecaracteristicas.IdGrupo in ($grupo)
			 INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble' 
			 AND detalleinmueble.idinmueble = inmuebles.idinm
			 $cond";
			 if($muestra==1)
			 {
				echo "<br>".$sqlconsulta."<br>";
			 }
	$res=$w_conexion->ResultSet($sqlconsulta) or die("error ".mysql_error());
	$existe=$w_conexion->FilasAfectadas($res);
	//echo $existe."ff";
	while($fff=$w_conexion->FilaSiguienteArray($res))
	{	//echo "hola en cliclo";
		$idcaracteristica=$fff["aaa"];
	}
	//echo ">> $idcaracteristica --- $campo <br>";
	return $idcaracteristica;
}
function evalua_carac($grupo,$inmueble,$campo,$especifica,$muestra="")
{
	global $w_conexion;
	$cond="";
	if($especifica)
	{
		$cond .=" and Descripcion like '%$especifica%'";
	}
$sqlconsulta="SELECT  $campo as aaa
			 FROM detalleinmueble 
			 INNER JOIN maestrodecaracteristicas 
			 ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica 
			 AND  maestrodecaracteristicas.IdGrupo in ($grupo)
			 INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble' 
			 AND detalleinmueble.idinmueble = inmuebles.idinm
			 $cond";
			 if($muestra==1)
			 {
				echo $sqlconsulta;
			 }
	$res=$w_conexion->ResultSet($sqlconsulta) or die("error ".mysql_error());
	$existe=$w_conexion->FilasAfectadas($res);
	//echo $existe."ff";
	
	
	if($existe>0)
	{
		$idcaracteristica = "1"; 
	}
	else
	{
		$idcaracteristica="0";
	}
//	echo ">> $idcaracteristica --- $campo <br>";
	return $idcaracteristica;
}
function evalua_carac_exacto($grupo,$inmueble,$campo,$especifica)
{
	global $w_conexion;
	$cond="";
	if($especifica)
	{
		$cond .=" and Descripcion = '$especifica'";
	}
$sqlconsulta="SELECT  $campo as aaa
			 FROM detalleinmueble 
			 INNER JOIN maestrodecaracteristicas 
			 ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica 
			 AND  maestrodecaracteristicas.IdGrupo ='$grupo'
			 INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble' 
			 AND detalleinmueble.idinmueble = inmuebles.idinm
			 $cond";
	//echo $sqlconsulta;
	$res=$w_conexion->ResultSet($sqlconsulta) or die("error ".mysql_error());
	$existe=$w_conexion->FilasAfectadas($res);
	//echo $existe."ff";
	
	
	if($existe>0)
	{
		$idcaracteristica = "1"; 
	}
	else
	{
		$idcaracteristica="0";
	}
//	echo ">> $idcaracteristica --- $campo <br>";
	return $idcaracteristica;
} 
	public 	function actualizarEstado($idInmueble,$codAfydi)
	{
		  if(!empty($idInmueble) && !empty($codAfydi))
		  {
		  		 $connPDO = new Conexion();

		    $stmt=$connPDO->prepare("UPDATE inmuebles SET
		                              PublicaAfydi='Publicado',
		                              RAfydi=:codAfydi 
		                              WHERE idInm = :idInmueble");
		    if($stmt->execute(array(
		      ":codAfydi"  => $codAfydi,
		      ":idInmueble"  => $idInmueble
		      )))
		    {
		      return 1;
		    } 
		    else
		    {
		      print_r($stmt->errorInfo());
		    } 
		  }
		  else
		  {
		  	return 0;
		  }
	}

	public function verificaEstadoAfydi($codInmu)
	{
		$connPDO=new Conexion();
		$stmt=$connPDO->prepare("SELECT RAfydi 
						   FROM inmuebles 
						   where idInm = :codInmu AND
						   RAfydi>0");
		if($stmt->execute(array(
			":codInmu"=>$codInmu
		)))
		{
			$seguimiento=$stmt->rowCount();
			if($seguimiento>0)
			{
				return 1;
			}
		}
		else
		{
			print_r($stmt->errorInfo());
		}

	}
	public function obtenerCodigoAfydi($codInmu)
	{
		$connPDO=new Conexion();
		$stmt=$connPDO->prepare("SELECT RAfydi 
						   FROM inmuebles 
						   where idInm = :codInmu AND
						   RAfydi>0");
		if($stmt->execute(array(
			":codInmu"=>$codInmu
		)))
		{
			$codigo="";
			while ($row = $stmt->fetch()) {
				$codigo=$row["RAfydi"];
			}
			return $codigo;

		}
		else
		{
			print_r($stmt->errorInfo());
		}

	}		
}

?>
