<?php  
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
require_once('../PHPMailer_5.2.2/class.phpmailer.php');
require_once('../PHPMailer_5.2.2/class.smtp.php');
//require_once('class.inmuebles.php');

include("class.mailSender.php");

//$inmueble = new Inmuebles;
//infoInmobiliaria


class mailCitas
{
	public function __construct($conn="",$pdo){
		$this->db=$conn;
		$this->pdo=$pdo;
	}
	public function infoInmobiliariaCita($codInmo)
    {
       

       $stmt = $this->pdo->prepare(" SELECT
		IdInmobiliaria,Nombre,Direccion,Correo, Telefonos, Web
		from clientessimi
        where IdInmobiliaria=:idinmo
		");
            if($stmt->execute(array(
			":idinmo" 	=> $codInmo
			)))
		{

            while ($row=$stmt->fetch()) 
            {
                $arreglo[] = array(
                    "IdInmobiliaria" => $row['IdInmobiliaria'],
                    "Nombre"         => ucwords(strtolower(utf8_encode($row['Nombre']))),
                    "Direccion"      => utf8_encode($row['Direccion']),
                    "Correo"         => utf8_encode($row['Correo']),
                    "Telefonos"      => utf8_encode($row['Telefonos']),
                    "Web"      => utf8_encode($row['Web']),
                );

            }
           
            return $arreglo;
        } else {
            return print_r($stmt->errorInfo());
        }

    }

    public function datosInmuebleTerceros($data)
    {
        

        $idInm =$data['IdInmobiliaria']."-".$data['codinm'];
        $cond='';
        if($data['rol_tr']>0)
        {
            $cond .=' and rol_tr=:rol_tr';
        }
        if($data['iduser']>0)
        {
            $cond .=' and iduser_tr=:iduser';
        }
        $stmt=$this->pdo->prepare("SELECT inmob_tr,idinm_tr,rol_tr,iduser_tr,idusu_tr
                from inmueblesterceros
                where idinm_tr =:codinm
                and inmob_tr   = :idinmo
                $cond");
        if($data['rol_tr']>0)
        {
            $stmt->bindParam(":rol_tr",$data['rol_tr']);
        }
        if($data['iduser']>0)
        {
            $stmt->bindParam(":iduser",$data['iduser']);
        }
        $stmt->bindParam(":idinmo",$data['IdInmobiliaria']);
        $stmt->bindParam(":codinm",$data['codinm']);
        if($stmt->execute())
        {
            $data=array();
            while ($row = $stmt->fetch())
            {
                $data[]=array(
                "rol_tr"      => $row['rol_tr'],
                "iduser_tr"   => $row['iduser_tr'],
                "nrol"        => utf8_encode(getCampo('parametros',"where id_param=51 and conse_param=".$row['rol_tr'],'desc_param')),
                "ntercero"    => utf8_encode(getCampo('usuarios',"where iduser='".$row['iduser_tr']."'",'concat(Nombres," ",apellidos)')),
                "cedtercero"  => getCampo('usuarios',"where iduser='".$row['iduser_tr']."'",'Id_Usuarios'),
                "tDoctercero" => getCampo('usuarios',"where iduser='".$row['iduser_tr']."'",'IdTipoDocumento')
                );
            }
            return $data;
        }
        else
        {
            return print_r($stmt->errorInfo());
        }
        $stmt=NULL;
    }

	public function envioMailCita($data)
	{
			$mailSender= New mailSender($this->db,$this->pdo);

	    
		   $style="style='color:#009CFF'";
		   $styleText="style='color:#FE3939'";
	       $NombreUsuario  	= ucwords(strtolower(getCampo('usuarios',"where iduser=".$data['citaAsesor'],'concat(Nombres," ",apellidos)')));
	       
	       $logo = getCampo('clientessimi',"where IdInmobiliaria=".$_SESSION['IdInmmo'],'logo');
	       $nomInmo = getCampo('clientessimi',"where IdInmobiliaria=".$_SESSION['IdInmmo'],'Nombre');
		   $foto = getCampo('usuarios'," where iduser=". $data['citaAsesor'],'Foto');

		   $asunto = getCampo('asunto_cita'," where id_asunto=".$data['citaTarea'].' and est_asunto=1','descripcion');

		   $titulo=($data['citaTarea']==1)?"NORM Nueva Cita para el  Inmueble ".$data['citaCodInm']:"Nueva Actividad ".$asunto;
		   $telInmobiliaria = "7525108";
		   $prop=getCampo('inmuebles',"where idInm='".$data['citaCodInm']."'",'IdPropietario');
		   $mailPropietario=getCampo('usuarios',"where Id_Usuairios='".$prop."'",'Correo');

		  

		   $texto=($data['citaTarea']==1)?'<span '.$style.'>Sr(a)</span> '.$nombreCliente.' , Se ha programado una cita para el ver el inmueble '.$data['citaCodInm'].' con el <span '.$style.'>asesor</span> '.$NombreUsuario:"";
	       $telefonsCli="";
	       if(!empty($data['citaCodInm']) && $data['citaTarea']==1)
	       {
	       		$latitud=getCampo('inmuebles'," where idInm=".$data['citaTarea'].'','latitud');
	       		$longitud=getCampo('inmuebles'," where idInm=".$data['citaTarea'].'','longitud');
	       		if(!empty($latitud) && !empty($longitud))
	       		{
	       			$boton='<div style="background-color:#F16D6D; width:100px; font-size:10px;padding:10px 5px 10px 5px; margin:10px 0 0 0; display:inline-block; vertical-align:top;">
				    <a style="color:white;" target="_blank" href="https://www.google.com/maps?&z=10&q='.$latitud.'+'.$longitud.'&ll='.
				    $latitud.'+'.$longitud.'"><div class="btn btn-info">Como Llegar</div></a>
				 </div>&nbsp';
	       		}
	       		else
	       		{
	       			$boton="";
	       		}
	       		$botonFicha='<div style="background-color:#009CFF; width:100px; font-size:10px;padding:10px 5px 10px 5px; margin:10px 0 0 0; display:inline-block; vertical-align:top;">
				    <a style="color:white;" target="_blank" href="https://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/fichatec3.php?reg='.$data['citaCodInm'].'"><div class="btn btn-info">Ver Ficha</div></a>
				 </div>';
	       		
	       }
	       $nombreCliente = getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'nombre');
	       $telefonoCliente = getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'telfijo');
	       $celularCliente =getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'telcelular'); 
	       $mailCliente=getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'email'); 
	       if($telefonoCliente == "" && $celularCliente != ""  )
	       {
	       		$telefonsCli=$telefonoCliente;
	       }
	       if($telefonoCliente != "" && $celularCliente == ""  )
	       {
	       		$telefonsCli=$celularCliente;
	       }
	       if($telefonoCliente == "" && $celularCliente == ""  )
	       {
	       		$telefonsCli="No registra";
	       }
	       if($telefonoCliente != "" && $celularCliente != ""  )
	       {
	       		$telefonsCli=$telefonoCliente." - ".$celularCliente;
	       }
	       list($anio,$mes,$dia)=explode("-",$data['citaFecha']);
	       $fImprimir=getCampo('parametros',"where id_param=13 and conse_param=$mes",'desc_param')." "." $dia de $anio";
		   $nombreCliente=($nombreCliente=="")?"No registra":$nombreCliente;
		   $mailCliente=($mailCliente=="")?"No registra":$mailCliente;
		   $fActividad='<p><b '.$style.'>Fecha Actividad:</b> '.$fImprimir.'- '.$data['citaHora'].' </p>';
		   $infoCLiente='<p><b '.$style.'>Cliente:</b> '.$nombreCliente.' </p><p><b '.$style.'>Telefono Cliente:</b> '.$telefonsCli.'</p><p><b '.$style.'>Correo Cliente:</b> '.$mailCliente.'</p>';
		   $observaciones=($data['citaTarea']==1)?'<p><b '.$style.'>Observaciones:</b> '.$data['citaObser'].' </p>':'<p><b '.$style.'>Observaciones:</b> '.$data['citaObser'].' </p>';
		   $infoCLienteCitaCliente='<p><b '.$style.'>Telefono Cliente:</b> '.$telefonsCli.'</p><p><b '.$style.'>Correo Cliente:</b> '.$mailCliente.'</p><p><b>Número de Teléfono Inmobiliaria:</b>'.$telInmobiliaria.'</p>';

		   $correoAsignado="";
		   $verificaMailAsignado=$this->getAsignadoMail($data['citaAsesor']);
		   if($verificaMailAsignado['reporta_a']>0)
		   {
		   	  $correoAsignado=getCampo('usuarios'," where iduser=".$verificaMailAsignado['reporta_a'],'Correo');
		   }else
		   {
		   	  $correoAsignado=getCampo('clientessimi'," where IdInmobiliaria=".$_SESSION['IdInmmo'],'Correo');
		   }

	    $cadena="12345";
	    $token = hash("sha512",$cadena);


	    if( isset($data["emailPropietario"]) and $data['citaTarea']==1 ){
			$tituloPropietario=($data['citaTarea']==1)?"PROP Nueva Cita para el  Inmueble ".$data['citaCodInm']:"Nueva Actividad ".$asunto;
			$textoPropietario=($data['citaTarea']==1)?'<span '.$style.'>Sr(a)</span> '.$nombreCliente.' , Se ha programado una cita para ver el inmueble '.$data['citaCodInm'].' con el <span '.$style.'>asesor</span> '.$NombreUsuario:"";
			$infoCLientePropietario='';

			$bodyPropietario='<html> 
			<head><meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
				<title>Cita</title> 
					<style>
					</style>
			</head>
			<body>
				<table align="center"  cellpadding="0" cellspacing="0" width="100%" style="border:0;">
 				<tr>
					<td bgcolor=""  align="center">
 		 				<p class="" >
						<a><img style="height:auto; width:auto; max-height:100%; max-width:200px;"  src=http://www.simiinmobiliarias.com/logo/'.$logo.'></a>
			  			</p>
					</td>
				 </tr>
				  <tr>
					<td bgcolor="#F2F2F2" align="center" style="padding: 15px 0 0px 0;">
						<font face="verdana" color="#FFFFFF"><h4>'.$tituloPropietario.'</h4>
						</font>
					</td>
 				</tr>
 				<tr>
						<td bgcolor="#ee4c50" height="">
				</td>
 				</tr>
 				<tr>
 					<td  bgcolor="#FFFFFF" style="border:3px #909090 solid;padding: 15px 0 0px 0;" align="center">
 					<div>
						<a><img style="height:auto; width:auto; max-height:100px; max-width:100px;" src=http://www.simiinmobiliarias.com/mcomercialweb/'.$foto.'></a>	
					</div>
 					 <div>		
						<font face="verdana" color="#424242">
						<h5>
							'.$textoPropietario.'
							<p></p>
						</h5>
						</font>
					  </div>
					   <div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						<h6  align="justify">
							'.$fActividad.'
							<p><b '.$style.'>Asesor:</b> '.$NombreUsuario.' </p>
							'.$infoCLientePropietario.'
							<p><b '.$style.'>Asunto:</b> '.$asunto.' </p>
							
						</h6>
						</font> 
					  </div>
					   
					  ';
					 
					  $bodyPropietario.='<div>
						<font face="verdana" color="#424242">
						<p></p>
						   <a  target="_blank" href="http:/http://tae-ltda.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA.</a>
						   <p></p>
						   Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo 

						</font>
					  </div>
 					</td>
 				</tr>';

 				$mailSender->envioMailCita($_SESSION['IdInmmo'],$bodyPropietario,$mailPropietario,'',$asunto);
		}


		if( isset($data["emailCliente"]) and $data['citaTarea']==1 ){
			$tituloCliente=($data['citaTarea']==1)?"CLI Nueva Cita para el  Inmueble ".$data['citaCodInm']:"Nueva Actividad ".$asunto;
			$textoPropietario=($data['citaTarea']==1)?'<span '.$style.'>Sr(a)</span> '.$nombreCliente.' , Se ha programado una cita para ver el inmueble '.$data['citaCodInm'].' con el <span '.$style.'>asesor</span> '.$NombreUsuario:"";
			$infoCLientePropietario='';

			$bodyEmail='<html> 
			<head><meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
				<title>Cita</title> 
					<style>
					</style>
			</head>
			<body>
				<table align="center"  cellpadding="0" cellspacing="0" width="100%" style="border:0;">
 				<tr>
					<td bgcolor=""  align="center">
 		 				<p class="" >
						<a><img style="height:auto; width:auto; max-height:100%; max-width:200px;"  src=http://www.simiinmobiliarias.com/logo/'.$logo.'></a>
			  			</p>
					</td>
				 </tr>
				  <tr>
					<td bgcolor="#F2F2F2" align="center" style="padding: 15px 0 0px 0;">
						<font face="verdana" color="#FFFFFF"><h4>'.$tituloCliente.'</h4>
						</font>
					</td>
 				</tr>
 				<tr>
						<td bgcolor="#ee4c50" height="">
				</td>
 				</tr>
 				<tr>
 					<td  bgcolor="#FFFFFF" style="border:3px #909090 solid;padding: 15px 0 0px 0;" align="center">
 					<div>
						<a><img style="height:auto; width:auto; max-height:100px; max-width:100px;" src=http://www.simiinmobiliarias.com/mcomercialweb/'.$foto.'></a>	
					</div>
 					 <div>		
						<font face="verdana" color="#424242">
						<h5>
							'.$texto.'
							<p></p>
						</h5>
						</font>
					  </div>
					   <div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						<h6  align="justify">
							'.$fActividad.'
							<p><b '.$style.'>Asesor:</b> '.$NombreUsuario.' </p>
							'.$infoCLienteCitaCliente.'
							<p><b '.$style.'>Asunto:</b> '.$asunto.' </p>
							
							<p>Correo Electrónico Asesor: '.$correoAsignado.'</p>
						</h6>
						</font> 
					  </div>
					   <div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						<h6 align="justify">'.$observaciones.'</h6>
						</font>
					  </div>
					  ';
					  if($data['citaTarea']==1){
					   $body.='<div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						'.$boton.$botonFicha.'
						</font>
					  </div>';
						}
					  $body.='<div>
						<font face="verdana" color="#424242">
						<p></p>
						   <a  target="_blank" href="http:/http://tae-ltda.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA.</a>
						   <p></p>
						   Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo 

						</font>
					  </div>
 					</td>
 				</tr>';
 				$mailSender->envioMailCita($_SESSION['IdInmmo'],$body,$mailCliente,'',$tituloCliente);
 				
		}
 		
		$body='<html> 
			<head><meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
				<title>Cita</title> 
					<style>
					</style>
			</head>
			<body>
				<table align="center"  cellpadding="0" cellspacing="0" width="100%" style="border:0;">
 				<tr>
					<td bgcolor=""  align="center">
 		 				<p class="" >
						<a><img style="height:auto; width:auto; max-height:100%; max-width:200px;"  src=http://www.simiinmobiliarias.com/logo/'.$logo.'></a>
			  			</p>
					</td>
				 </tr>
				  <tr>
					<td bgcolor="#F2F2F2" align="center" style="padding: 15px 0 0px 0;">
						<font face="verdana" color="#FFFFFF"><h4>'.$titulo.'</h4>
						</font>
					</td>
 				</tr>
 				<tr>
						<td bgcolor="#ee4c50" height="">
				</td>
 				</tr>
 				<tr>
 					<td  bgcolor="#FFFFFF" style="border:3px #909090 solid;padding: 15px 0 0px 0;" align="center">
 					<div>
						<a><img style="height:auto; width:auto; max-height:100px; max-width:100px;" src=http://www.simiinmobiliarias.com/mcomercialweb/'.$foto.'></a>	
					</div>
 					 <div>		
						<font face="verdana" color="#424242">
						<h5>
							'.$texto.'
							<p></p>
						</h5>
						</font>
					  </div>
					   <div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						<h6  align="justify">
							'.$fActividad.'
							<p><b '.$style.'>Asesor:</b> '.$NombreUsuario.' </p>
							'.$infoCLiente.'
							<p><b '.$style.'>Asunto:</b> '.$asunto.' </p>
							
							<p>mail de asignado: '.$correoAsignado.'</p>
						</h6>
						</font> 
					  </div>
					   <div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						<h6 align="justify">'.$observaciones.'</h6>
						</font>
					  </div>
					  ';
					  if($data['citaTarea']==1){
					   $body.='<div style=" padding:5px;width:70%; background-color:#ECF0F1; border: solid 2px #337AB7">		
						<font face="verdana" color="#424242" >
						'.$boton.$botonFicha.'
						</font>
					  </div>';
						}
					  $body.='<div>
						<font face="verdana" color="#424242">
						<p></p>
						   <a  target="_blank" href="http:/http://tae-ltda.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA.</a>
						   <p></p>
						   Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo 

						</font>
					  </div>
 					</td>
 				</tr>';
 				$mailSender->envioMailCita($_SESSION['IdInmmo'],$body,$mailCliente,'',$tituloCliente);

	}
	public function getAsignadoMail($idUser)
	{
		$stmt=$this->pdo->prepare('SELECT reporta_a 
						FROM usuarios WHERE 
						iduser =:iduser
						LIMIT 0,10');
		if($stmt->execute(array(
			":iduser"=>$idUser
			)))
		{
			if($stmt->rowCount()>0)
			{
				return $stmt->fetch(PDO::FETCH_ASSOC);
			}else
			{
				return 0;
			}
		}else
		{
			$response=array(
			"error"=>$stmt->errorInfo(),
			"info"=>$data
			);
			return $response;
		}
	}

	public function envioMailCitaNew($data){

		//instanciamos mailsender
		$mailSender= New mailSender($this->db,$this->pdo);
		//variable de estilso estilos
		$style="style='color:#009CFF'";
		//estilos textos
		$styleText="style='color:#FE3939'";
		//obtenemos el nombre de usuario de asesor
		$NombreUsuario  	= ucwords(strtolower(getCampo('usuarios',"where iduser=".$data['citaAsesor'],'concat(Nombres," ",apellidos)')));
		//obtenemos el logo
		$logo = getCampo('clientessimi',"where IdInmobiliaria=".$_SESSION['IdInmmo'],'logo');
		//obtenemos nombre de inmobiliaria
		$nomInmo = getCampo('clientessimi',"where IdInmobiliaria=".$_SESSION['IdInmmo'],'Nombre');
		//obtenemos la foto del asesor
		$foto = getCampo('usuarios'," where iduser=". $data['citaAsesor'],'Foto');
		$fotoAsesor = getCampo('usuarios'," where iduser=". $data['citaAsesor'],'Foto');
		//obtenemos el asunto de la cita
		$asunto = getCampo('asunto_cita'," where id_asunto=".$data['citaTarea'].' and est_asunto=1','descripcion');
		//obtenemos el titulo para el correo
		$titulo=($data['citaTarea']==1)?"NORM Nueva Cita para el  Inmueble ".$data['citaCodInm']:"Nueva Actividad ".$asunto;
		//obtenemos el telefono de la inmobiliaria
		$telInmobiliaria = "7525108";
		//obtenemos el nombre del propietaria
		$prop=getCampo('inmuebles',"where idInm='".$data['citaCodInm']."'",'IdPropietario');
		/******************************************************************
		 *              Obtenemos el correo del propietario               *
		 *****************************************************************/
		$mailPropietario=getCampo('usuarios',"where Id_Usuarios='".$prop."'",'Correo');
		//texto bienvenida correo
		$texto=($data['citaTarea']==1)?'<span '.$style.'>Sr(a)</span> '.$nombreCliente.' , Se ha programado una cita para el ver el inmueble '.$data['citaCodInm'].' con el <span '.$style.'>asesor</span> '.$NombreUsuario:"";
		//obtenemos el telefono del cliente
		$telefonsCli="";
		//tipo tarea de la cita, si es 1 quiere decir que es mostrar inmueble
		if(!empty($data['citaCodInm']) && $data['citaTarea']==1)
		{
				$latitud=getCampo('inmuebles'," where idInm=".$data['citaTarea'].'','latitud');
				$longitud=getCampo('inmuebles'," where idInm=".$data['citaTarea'].'','longitud');
				if(!empty($latitud) && !empty($longitud))
				{
					$boton='<div style="background-color:#F16D6D; width:100px; font-size:10px;padding:10px 5px 10px 5px; margin:10px 0 0 0; display:inline-block; vertical-align:top;">
			    <a style="color:white;" target="_blank" href="https://www.google.com/maps?&z=10&q='.$latitud.'+'.$longitud.'&ll='.
			    $latitud.'+'.$longitud.'"><div class="btn btn-info">Como Llegar</div></a>
			 </div>&nbsp';
				}
				else
				{
					$boton="";
				}
				$botonFicha='<div style="background-color:#009CFF; width:100px; font-size:10px;padding:10px 5px 10px 5px; margin:10px 0 0 0; display:inline-block; vertical-align:top;">
			    <a style="color:white;" target="_blank" href="https://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/fichatec3.php?reg='.$data['citaCodInm'].'"><div class="btn btn-info">Ver Ficha</div></a>
			 </div>';
				
		}
		//obtenemos el nombre del cliente
		$nombreCliente = getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'nombre');
		//obtenemos el telefono del cliente
		$telefonoCliente = getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'telfijo');
		//obtenemos el celular del cliente
		$celularCliente =getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'telcelular'); 
		//obtenemos el email del cliente
		$mailCliente=getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'email');
		/*********************************
		 *              validamos si está vacio el telefono o celular para concaternar
		 *              o no el telefono               *
		 *********************************/
		if($telefonoCliente == "" && $celularCliente != ""  ){
			$telefonsCli=$telefonoCliente;
		}
		if($telefonoCliente != "" && $celularCliente == ""  ){
			$telefonsCli=$celularCliente;
		}
		if($telefonoCliente == "" && $celularCliente == ""  ){
			$telefonsCli="No registra";
		}
		if($telefonoCliente != "" && $celularCliente != ""  ){
			$telefonsCli=$telefonoCliente." - ".$celularCliente;
		}

		list($anio,$mes,$dia)=explode("-",$data['citaFecha']);
		//obtenemos la fecha de la cita
		$fImprimir=getCampo('parametros',"where id_param=13 and conse_param=$mes",'desc_param')." "." $dia de $anio";
		//validamos si existe el nombre del cliente sino, colocamos el texto no registra
		$nombreCliente=($nombreCliente=="")?"No registra":$nombreCliente;
		//validamos si existe el mail del cliente sino, colocamos el texto no registra
		$mailCliente=($mailCliente=="")?"No registra":$mailCliente;
		//concatenamos la fecha y hora de la cita
		$fActividad='<p><b '.$style.'>Fecha Actividad:</b> '.$fImprimir.'- '.$data['citaHora'].' </p>';
		//concatenamos el nombre del cliente con su respectivo telefono
		$infoCLiente='<p><b '.$style.'>Cliente:</b> '.$nombreCliente.' </p><p><b '.$style.'>Telefono Cliente:</b> '.$telefonsCli.'</p><p><b '.$style.'>Correo Cliente:</b> '.$mailCliente.'</p>';
		//obtenemos observaciones de la cita
		$observaciones=($data['citaTarea']==1)?'<p><b '.$style.'>Observaciones:</b> '.$data['citaObser'].' </p>':'<p><b '.$style.'>Observaciones:</b> '.$data['citaObser'].' </p>';
		//concatenamos informacion del cliente
		$infoCLienteCitaCliente='<p><b '.$style.'>Telefono Cliente:</b> '.$telefonsCli.'</p><p><b '.$style.'>Correo Cliente:</b> '.$mailCliente.'</p><p><b>Número de Teléfono Inmobiliaria:</b>'.$telInmobiliaria.'</p>';

		$correoAsignado="";
		//verificamos si existe el correo del asesor para reportar
		$verificaMailAsignado=$this->getAsignadoMail($data['citaAsesor']);

		if($verificaMailAsignado['reporta_a']>0)
		{
			  $correoAsignado=getCampo('usuarios'," where iduser=".$verificaMailAsignado['reporta_a'],'Correo');
		}else
		{
			  $correoAsignado=getCampo('clientessimi'," where IdInmobiliaria=".$_SESSION['IdInmmo'],'Correo');
		}

		$cadena="12345";
		$token = hash("sha512",$cadena);

		/*if( isset($data["emailCliente"]) and $data['citaTarea']==1 ){


		}*/

		//si el check de propietario esta activo enviamos correo al propietario
		if( isset($data["emailPropietario"]) and $data['citaTarea']==1 ){

			//$mailPropietario="desarrolloweb3@tae-ltda.com";
					$inmo = getCampo('cita',"where id_cita=".$data['idcita'],"inmobiliaria");
					$inmuebleId = getCampo('cita',"where id_cita=".$data['idcita'],"id_inmueble");

					$inmuebleId= explode("-",$inmuebleId);
					$dataProp = array("IdInmobiliaria"=>$inmo,"codinm"=>$inmuebleId[1],"rol_tr"=>1);
					$propietario=$this->datosInmuebleTerceros($dataProp);
					

					/*echo "<pre>";
					print_r($propietario);
					echo "</pre>";
					exit;*/
					foreach ($propietario as $key => $value) {

						$mailPropietario=getCampo('usuarios',"where iduser='".$value["iduser_tr"]."'",'Correo');


			
						$ase=getCampo('usuarios',"where  iduser =".$data['citaAsesor'],'concat(Nombres," ", apellidos)');
						$formatoFicha=getCampo('clientessimi',"where IdInmobiliaria=".$_SESSION['IdInmmo'],"fichatec");
						$idusuarioAse=($ase=='')?0:$ase;

						
						$inmobiliaria = $this->infoInmobiliariaCita($inmo);

						

						$namePropietario=getCampo('usuarios',"where Id_Usuarios='".$prop."'",'concat(Nombres," ", apellidos)');


						$datas = array("propietario"=>$value["ntercero"],"logo"=>str_replace("../","",$logo),"citaCodInm"=>$data['citaCodInm'],"foto"=>$foto,"asesor"=>$ase,"tarea"=>$asunto,"formatoFicha"=>$formatoFicha,"inmobiliaria"=>$inmobiliaria);
						
						$bodyPropietario = $this->mailPropietario($datas);

						//echo $bodyPropietario."<br>";
						//echo $mailPropietario."<br>";
						//$mailPropietario="soportesimi12@gmail.com";

						$send= $mailSender->envioMailCita($_SESSION['IdInmmo'],$bodyPropietario,$mailPropietario,'',$asunto);

					}
					
					
					
		

		}

		
		if( (isset($data["gmailAsesor"]) and $data['citaTarea']==1) or (isset($data["outlookAsesor"]) and $data['citaTarea']==1) ){

			$inmo = getCampo('cita',"where id_cita=".$data['idcita'],"inmobiliaria");
			$inmuebleId = getCampo('cita',"where id_cita=".$data['idcita'],"id_inmueble");
			$idInmueble = $inmuebleId;
			$inmuebleId= explode("-",$inmuebleId);
			$inmobiliaria = $this->infoInmobiliariaCita($inmo);
			// $latlong=getCampo('inmnvo'," where  idInm = '1-14892' ",' idInm ');
			$latlong = getCampo('inmnvo',' where codinm = '.$inmuebleId[1].' AND IdInmobiliaria = '.$inmuebleId[0],'concat(latitud," , ",longitud)');
			$dirInmu = getCampo('inmnvo',' where codinm = '.$inmuebleId[1].' AND IdInmobiliaria = '.$inmuebleId[0],'Direccion');
			$datas = array("propietario"=>"","logo"=>str_replace("../","",$logo),"citaCodInm"=>$data['citaCodInm'],"foto"=>$foto,"asesor"=>$ase,"tarea"=>$asunto,"formatoFicha"=>$formatoFicha,"inmobiliaria"=>$inmobiliaria,"nombreCliente"=>$nombreCliente,"mailCliente"=>$mailCliente,"telefonsCli"=>$telefonsCli,"latlong"=>$latlong,"dirInmu"=>$dirInmu);//$mailCliente
			$mailAsesor  	= ucwords(strtolower(getCampo('usuarios',"where iduser=".$data['citaAsesor'],'Correo')));
			//$mailAsesor = "oscalber_22@hotmail.com";
			//$mailAsesor = "andrescenit@hotmail.com";
			$mailExplode = explode("@",$mailAsesor);
			//print_r($mailExplode);
			
			if( strtolower($mailExplode[1]) == "gmail.com" ){

				$bodyAsesor = $this->mailAsesor($datas);

				

				$send= $mailSender->envioMailCitaGoogle($_SESSION['IdInmmo'],$bodyAsesor,$mailAsesor,'',$asunto,$data,$datas,"1");

			}else{

				if(strtolower($mailExplode[1]) == "hotmail.com" or strtolower($mailExplode[1]) == "outlook.com"){

					$bodyAsesor = $this->mailAsesor($datas);
					$send= $mailSender->envioMailCitaCal($_SESSION['IdInmmo'],$bodyAsesor,$mailAsesor,'',$asunto,$data,$datas,"2");

				}else{
					$bodyAsesor = $this->mailAsesor($datas);
					$send= $mailSender->envioMailCita($_SESSION['IdInmmo'],$bodyAsesor,$mailAsesor,'',$asunto,$data,$datas,"3");
				}

			}
			//$mailAsesor ="desarrolloweb3@tae-ltda.com";

		


		}

		if( isset($data["emailGerente"]) and $data['citaTarea']==1 ){

			$inmo = getCampo('cita',"where id_cita=".$data['idcita'],"inmobiliaria");
			$inmuebleId = getCampo('cita',"where id_cita=".$data['idcita'],"id_inmueble");
			$idInmueble = $inmuebleId;
			$inmuebleId= explode("-",$inmuebleId);
			$noenviar = 0;


			$mailGerente =getCampo('usuarios',"where iduser='".$data["citaAsesor"]."'",'reporta_a');
			$inmobiliaria = $this->infoInmobiliariaCita($inmo);

			if( empty($mailGerente) or $mailGerente == 0 ){
				$mailGerente =getCampo('inmobiliaria',"where IdInmobiliaria='".$inmo."'",'EmailC');

				if( empty($mailGerente) or $mailGerente == 0 ){
					$noenviar =1;
				}
			}

			$foto = getCampo('usuarios'," where iduser=". $value['iduser_tr'],'Foto');

			
			
			//$mailGerente = "soportesimi12@gmail.com";
			//$captador=getCampo('usuarios',"where  iduser =".$data['citaAsesor'],'concat(Nombres," ", apellidos)');
			// $latlong=getCampo('inmnvo'," where  idInm = '1-14892' ",' idInm ');
			$latlong = getCampo('inmnvo',' where codinm = '.$inmuebleId[1].' AND IdInmobiliaria = '.$inmuebleId[0],'concat(latitud," , ",longitud)');
			$datas = array("propietario"=>"","logo"=>str_replace("../","",$logo),"citaCodInm"=>$data['citaCodInm'],"foto"=>$foto,"asesor"=>"Gerente Comercial","tarea"=>$asunto,"formatoFicha"=>$formatoFicha,"inmobiliaria"=>$inmobiliaria,"nombreCliente"=>$nombreCliente,"mailCliente"=>"","telefonsCli"=>$telefonsCli,"latlong"=>$latlong);//$mailCliente

	
			//if($noenviar==0){
				//echo"entro";
				$bodyCaptador = $this->mailGerente($datas);
				
				$send= $mailSender->envioMailCita($_SESSION['IdInmmo'],$bodyCaptador,$mailGerente,'',$asunto);
			//}


			

		}

		if( isset($data["emailCaptador"]) and $data['citaTarea']==1 ){

			$inmo = getCampo('cita',"where id_cita=".$data['idcita'],"inmobiliaria");
			$inmuebleId = getCampo('cita',"where id_cita=".$data['idcita'],"id_inmueble");
			$idInmueble = $inmuebleId;
			$inmuebleId= explode("-",$inmuebleId);
			$inmobiliaria = $this->infoInmobiliariaCita($inmo);

			$dataProp = array("IdInmobiliaria"=>$inmo,"codinm"=>$inmuebleId[1],"rol_tr"=>4);
			$captador=$this->datosInmuebleTerceros($dataProp);

			/*
			"rol_tr"      => $row['rol_tr'],
                "iduser_tr"   => $row['iduser_tr'],
                "nrol"        => utf8_encode(getCampo('parametros',"where id_param=51 and conse_param=".$row['rol_tr'],'desc_param')),
                "ntercero"    => utf8_encode(getCampo('usuarios',"where iduser='".$row['iduser_tr']."'",'concat(Nombres," ",apellidos)')),
                "cedtercero"  => getCampo('usuarios',"where iduser='".$row['iduser_tr']."'",'Id_Usuarios'),
                "tDoctercero" => getCampo('usuarios',"where iduser='".$row['iduser_tr']."'",'IdTipoDocumento'
			 */

			foreach ($captador as $key => $value) {

				$foto = getCampo('usuarios'," where iduser=". $value['iduser_tr'],'Foto');

				
				$mailCaptador =getCampo('usuarios',"where iduser='".$value["iduser_tr"]."'",'Correo');
				//$mailCaptador = "soportesimi12@gmail.com";
				$captador=getCampo('usuarios',"where  iduser =".$data['citaAsesor'],'concat(Nombres," ", apellidos)');
				// $latlong=getCampo('inmnvo'," where  idInm = '1-14892' ",' idInm ');
				$latlong = getCampo('inmnvo',' where codinm = '.$inmuebleId[1].' AND IdInmobiliaria = '.$inmuebleId[0],'concat(latitud," , ",longitud)');
				$datas = array("propietario"=>"","logo"=>str_replace("../","",$logo),"citaCodInm"=>$data['citaCodInm'],"foto"=>$foto,"asesor"=>$value["ntercero"],"tarea"=>$asunto,"formatoFicha"=>$formatoFicha,"inmobiliaria"=>$inmobiliaria,"nombreCliente"=>$nombreCliente,"mailCliente"=>"","telefonsCli"=>$telefonsCli,"latlong"=>$latlong);//$mailCliente

		

				$bodyCaptador = $this->mailCaptador($datas);
				
				$send= $mailSender->envioMailCita($_SESSION['IdInmmo'],$bodyCaptador,$mailCaptador,'',$asunto);


			}

		}

		if( isset($data["emailCliente"]) and $data['citaTarea']==1 ){

			$inmo = getCampo('cita',"where id_cita=".$data['idcita'],"inmobiliaria");
			$inmuebleId = getCampo('cita',"where id_cita=".$data['idcita'],"id_inmueble");
			$idInmueble = $inmuebleId;
			$inmuebleId= explode("-",$inmuebleId);
			$mailCliente= getCampo('clientes_inmobiliaria',"where cedula=".$data['citaCliente'],'email');
			//$mailCliente ="soportesimi12@gmail.com";
			$asesorEMail = getCampo('usuarios',"where iduser=".$data['citaAsesor'],'Correo');
			$asesorCelular = getCampo('usuarios',"where iduser=".$data['citaAsesor'],'Celular');
			$inmobiliaria = $this->infoInmobiliariaCita($inmo);
			// $latlong=getCampo('inmnvo'," where  idInm = '1-14892' ",' idInm ');
			$latlong = getCampo('inmnvo',' where codinm = '.$inmuebleId[1].' AND IdInmobiliaria = '.$inmuebleId[0],'concat(latitud," , ",longitud)');
			$datas = array("propietario"=>"","logo"=>str_replace("../","",$logo),"citaCodInm"=>$data['citaCodInm'],"foto"=>$fotoAsesor,"asesor"=>$ase,"tarea"=>$asunto,"formatoFicha"=>$formatoFicha,"inmobiliaria"=>$inmobiliaria,"nombreCliente"=>$nombreCliente,"mailCliente"=>$mailCliente,"telefonsCli"=>$telefonsCli,"latlong"=>$latlong,"asesorEMail"=>$asesorEMail,"asesorCelular"=>$asesorCelular);//$mailCliente
			$bodyCaptador = $this->mailCliente($datas);
				
			$send= $mailSender->envioMailCita($_SESSION['IdInmmo'],$bodyCaptador,$mailCliente,'',$asunto);


		}


		
		return  1;
		

		

		/*echo "<pre>";
		print_r($data);
		echo "</pre>";*/


	}

	public function mailPropietario($data){


			$cuerpo = '<html>
	        <head>
	            <meta charset="UTF-8">
	            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	            <title>Retroalimentación Citas</title>
	        </head>
	        <body>
	            <div style="text-align: center">
	                <img style="margin:0 auto 0 auto;height:auto; width:auto; max-height:200px; max-width:300px;"  src="http://simiinmobiliarias.com/'.$data["logo"].'"/>
	                <div style="width: 100%; background-color: #337AB7; text-align: center; padding: 15px 0px 15px 0px">
	                    <h4 style="color: white">Sr(a) Propietario(a): '. $data["propietario"] .'</h4>
	                </div>
	                <div style="border: 3px #909090 solid; padding: 15px 0 0px 0; text-align: center;">

	                    ';
	                     $fotoper = (empty($data["foto"])) ? '' : '<img  style="height:100px; width:100px; max-height:100px; max-width:100px;" src="http://simiinmobiliarias.com/mcomercialweb/'.str_replace("../","",$data["foto"]).'" style="">' ;
	                    $cuerpo.=$fotoper.'

	                    <p><b>Asesor(a): </b></p>
	                    <p>'.$data["asesor"].'</p>
	                    <br>

	                    <p>Le informamos que se ha programado una nueva tarea de <strong>'.$data["tarea"].'</strong> para su inmueble con el codigo asignado ' .  $data["citaCodInm"] . '</p>

	                    <br>

	                    <a target="_blank" style="background-color:#F16D6D; display: inline-block; color:white; width:150px; font-size:14px;padding:10px 5px 10px 5px; margin:10px 0 0 0;" href="http://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/' . $data["formatoFicha"] . '?reg=' . $data["citaCodInm"] . '">
	                        Ver Ficha
	                    </a>

	                    
	                    <h3>Datos de la Inmobiliaria</h3>

	                    <p style="color: #1BA1FF"><b>Nombre Inmobiliaria: </b></p>
	                    <p>'.$data["nombreCliente"].'</p>

	                    <p style="color: #1BA1FF"><b>Telefonos:</b></p>
	                    <p><a href="tel:'.$data["inmobiliaria"][0]["Telefonos"].'" target="_Blank">'.$data["inmobiliaria"][0]["Telefonos"].'</a></p>

	                    <p style="color: #1BA1FF"><b>Dirección:  </b></p>
	                    <p>'.$data["inmobiliaria"][0]["Direccion"].' </p>

	                    <p style="color: #1BA1FF"><b>Página Web: </b></p>
	                    <p><a href="'.$data["inmobiliaria"][0]["Web"].'">'.$data["inmobiliaria"][0]["WEb"].'</a> </p>


	                    <a class="text-primary" target="_blank" href="http://tae-ltda.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA. Visitenos en www.siminmueble.com</a>
	                    <p>Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo.</p>

	                </div>
	            </div>
	        </body>
	    </html>';
    return $cuerpo;
	}

	public function mailAsesor($data){

		$cuerpo = '<html>
	        <head>
	            <meta charset="UTF-8">
	            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	            <title>Retroalimentación Citas</title>
	        </head>
	        <body>
	            <div style="text-align: center">
	                <img style="margin:0 auto 0 auto;height:auto; width:auto; max-height:200px; max-width:300px;"  src="http://simiinmobiliarias.com/'.$data["logo"].'"/>
	                <div style="width: 100%; background-color: #337AB7; text-align: center; padding: 15px 0px 15px 0px">
	                    <h4 style="color: white">nueva cita para el inmueble codigo: '. $data["citaCodInm"] .'</h4>
	                </div>
	                <div style="border: 3px #909090 solid; padding: 15px 0 0px 0; text-align: center;">

	                    ';
	                     $fotoper = (empty($data["foto"])) ? '' : '<img  style="height:100px; width:100px; max-height:100px; max-width:100px;" src="http://simiinmobiliarias.com/mcomercialweb/'.str_replace("../","",$data["foto"]).'" style="">' ;
	                    $cuerpo.=$fotoper.'

	                    <p><b>Asesor(a): </b></p>
	                    <p>'.$data["asesor"].'</p>
	                    <br>

	                    <p>se ha programado una cita para <strong>'.$data["tarea"].'</strong> para el inmueble con el codigo asignado ' .  $data["citaCodInm"] . '</p>

	                    <br>

	                    <a target="_blank" style="background-color:#F16D6D; display: inline-block; color:white; width:150px; font-size:14px;padding:10px 5px 10px 5px; margin:10px 0 0 0;" href="http://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/' . $data["formatoFicha"] . '?reg=' . $data["citaCodInm"] . '">
	                        Ver Ficha
	                    </a>

	                    <a target="_blank" style="background-color:#F16D6D; display: inline-block; color:white; width:150px; font-size:14px;padding:10px 5px 10px 5px; margin:10px 0 0 0;" href="http://maps.google.com/?q='.$data["latlong"].'">
	                        Ver Ubicación
	                    </a>

	                    
	                    <h3>Datos del cliente</h3>

	                    <p style="color: #1BA1FF"><b>nombre cliente: </b></p>
	                    <p>'.$data["nombreCliente"].'</p>

	                    <p style="color: #1BA1FF"><b>numero celular:</b></p>
	                    <p><a href="tel:'.$data["telefonsCli"].'" target="_Blank">'.$data["telefonsCli"].'</a></p>

	                    <p style="color: #1BA1FF"><b>Correo:  </b></p>
	                    <p>'.$data["mailCliente"].' </p>

	                    


	                    <a class="text-primary" target="_blank" href="http://tae-ltda.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA. Visitenos en www.siminmueble.com</a>
	                    <p>Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo.</p>

	                </div>
	            </div>
	        </body>
	    </html>';
	    return $cuerpo;

	}

	public function mailCaptador($data){

		$cuerpo = '<html>
	        <head>
	            <meta charset="UTF-8">
	            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	            <title>Retroalimentación Citas</title>
	        </head>
	        <body>
	            <div style="text-align: center">
	                <img style="margin:0 auto 0 auto;height:auto; width:auto; max-height:200px; max-width:300px;"  src="http://simiinmobiliarias.com/'.$data["logo"].'"/>
	                <div style="width: 100%; background-color: #337AB7; text-align: center; padding: 15px 0px 15px 0px">
	                    <h4 style="color: white">nueva cita para el inmueble codigo: '. $data["citaCodInm"] .'</h4>
	                </div>
	                <div style="border: 3px #909090 solid; padding: 15px 0 0px 0; text-align: center;">

	                    '.$cuerpo.=$fotoper.'

	                    <p><b>Captador(a): </b></p>
	                    <p>'.$data["asesor"].'</p>
	                    <br>

	                    <p>se ha programado una cita para <strong>'.$data["tarea"].'</strong> para el inmueble con el codigo asignado ' .  $data["citaCodInm"] . '</p>

	                    <br>

	                    <a target="_blank" style="background-color:#F16D6D; display: inline-block; color:white; width:150px; font-size:14px;padding:10px 5px 10px 5px; margin:10px 0 0 0;" href="http://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/' . $data["formatoFicha"] . '?reg=' . $data["citaCodInm"] . '">
	                        Ver Ficha
	                    </a>

	                    <a target="_blank" style="background-color:#F16D6D; display: inline-block; color:white; width:150px; font-size:14px;padding:10px 5px 10px 5px; margin:10px 0 0 0;" href="http://maps.google.com/?q='.$data["latlong"].'">
	                        Ver Ubicación
	                    </a>

	                    
	                    <h3>Datos del cliente</h3>

	                    <p style="color: #1BA1FF"><b>nombre cliente: </b></p>
	                    <p>'.$data["inmobiliaria"][0]["Nombre"].'</p>

	                    <p style="color: #1BA1FF"><b>numero celular:</b></p>
	                    <p><a href="tel:'.$data["inmobiliaria"][0]["Telefonos"].'" target="_Blank">'.$data["telefonsCli"].'</a></p>

	                    <p style="color: #1BA1FF"><b>Correo:  </b></p>
	                    <p>'.$data["mailCliente"].' </p>

	                    


	                    <a class="text-primary" target="_blank" href="http://tae-ltda.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA. Visitenos en www.siminmueble.com</a>
	                    <p>Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo.</p>

	                </div>
	            </div>
	        </body>
	    </html>';
	    return $cuerpo;

	}

	public function mailGerente($data){

		$cuerpo = '<html>
	        <head>
	            <meta charset="UTF-8">
	            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	            <title>Retroalimentación Citas</title>
	        </head>
	        <body>
	            <div style="text-align: center">
	                <img style="margin:0 auto 0 auto;height:auto; width:auto; max-height:200px; max-width:300px;"  src="http://simiinmobiliarias.com/'.$data["logo"].'"/>
	                <div style="width: 100%; background-color: #337AB7; text-align: center; padding: 15px 0px 15px 0px">
	                    <h4 style="color: white">nueva cita para el inmueble codigo: '. $data["citaCodInm"] .'</h4>
	                </div>
	                <div style="border: 3px #909090 solid; padding: 15px 0 0px 0; text-align: center;">

	                    '.$cuerpo.=$fotoper.'

	                    <p><b>Señor(a) Gerente: </b></p>
	                    
	                    <br>

	                    <p>se ha programado una cita para <strong>'.$data["tarea"].'</strong> para el inmueble con el codigo asignado ' .  $data["citaCodInm"] . '</p>

	                    <br>

	                    <a target="_blank" style="background-color:#F16D6D; display: inline-block; color:white; width:150px; font-size:14px;padding:10px 5px 10px 5px; margin:10px 0 0 0;" href="http://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/' . $data["formatoFicha"] . '?reg=' . $data["citaCodInm"] . '">
	                        Ver Ficha
	                    </a>

	                    <a target="_blank" style="background-color:#F16D6D; display: inline-block; color:white; width:150px; font-size:14px;padding:10px 5px 10px 5px; margin:10px 0 0 0;" href="http://maps.google.com/?q='.$data["latlong"].'">
	                        Ver Ubicación
	                    </a>

	                    
	                    <h3>Datos del cliente</h3>

	                    <p style="color: #1BA1FF"><b>nombre cliente: </b></p>
	                    <p>'.$data["inmobiliaria"][0]["Nombre"].'</p>

	                    <p style="color: #1BA1FF"><b>numero celular:</b></p>
	                    <p><a href="tel:'.$data["inmobiliaria"][0]["Telefonos"].'" target="_Blank">'.$data["telefonsCli"].'</a></p>

	                    <p style="color: #1BA1FF"><b>Correo:  </b></p>
	                    <p>'.$data["mailCliente"].' </p>

	                    


	                    <a class="text-primary" target="_blank" href="http://tae-ltda.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA. Visitenos en www.siminmueble.com</a>
	                    <p>Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo.</p>

	                </div>
	            </div>
	        </body>
	    </html>';
	    return $cuerpo;

	}

	public function mailCliente($data){

		$cuerpo = '<html>
	        <head>
	            <meta charset="UTF-8">
	            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	            <title>Retroalimentación Citas</title>
	        </head>
	        <body>
	            <div style="text-align: center">
	                <img style="margin:0 auto 0 auto;height:auto; width:auto; max-height:200px; max-width:300px;"  src="http://simiinmobiliarias.com/'.$data["logo"].'"/>
	                <div style="width: 100%; background-color: #337AB7; text-align: center; padding: 15px 0px 15px 0px">
	                    <h4 style="color: white">Cita para el inmueble el cual esta interesado</h4>
	                </div>
	                <div style="border: 3px #909090 solid; padding: 15px 0 0px 0; text-align: center;">
						<p><b>Señor(a)</b> '.$data["nombreCliente"].': </p>
						<p></p><br>
						<p>Se ha agendado la cita con el asesor:</p>
	                    <p>'.$data["asesor"].'</p><br>
	                    ';
	                     $fotoper = (empty($data["foto"])) ? '' : '<img  style="height:100px; width:100px; max-height:100px; max-width:100px;" src="http://simiinmobiliarias.com/mcomercialweb/'.str_replace("../","",$data["foto"]).'" style="">' ;
	                    $cuerpo.=$fotoper.'
	                    <br>
	                    <p><b>Teléfono:</b> '.$data["asesorCelular"].'</p>
	                    <p><b>correo:</b> '.$data["asesorEMail"].'</p><br>

	                    <a target="_blank" style="background-color:#F16D6D; display: inline-block; color:white; width:150px; font-size:14px;padding:10px 5px 10px 5px; margin:10px 0 0 0;" href="http://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/' . $data["formatoFicha"] . '?reg=' . $data["citaCodInm"] . '">
	                        Ver Ficha
	                    </a>

	                    <a target="_blank" style="background-color:#F16D6D; display: inline-block; color:white; width:150px; font-size:14px;padding:10px 5px 10px 5px; margin:10px 0 0 0;" href="http://maps.google.com/?q='.$data["latlong"].'">
	                        Ver Ubicación
	                    </a>
						
						<h3>Datos de la Inmobiliaria</h3>

	                    <p style="color: #1BA1FF"><b>Nombre Inmobiliaria: </b></p>
	                    <p>'.$data["inmobiliaria"][0]["Nombre"].'</p>

	                    <p style="color: #1BA1FF"><b>Telefonos:</b></p>
	                    <p><a href="tel:'.$data["inmobiliaria"][0]["Telefonos"].'" target="_Blank">'.$data["inmobiliaria"][0]["Telefonos"].'</a></p>

	                    <p style="color: #1BA1FF"><b>Dirección:  </b></p>
	                    <p>'.$data["inmobiliaria"][0]["Direccion"].' </p>

	                    <p style="color: #1BA1FF"><b>Página Web: </b></p>
	                    <p><a href="'.$data["inmobiliaria"][0]["Web"].'">'.$data["inmobiliaria"][0]["Web"].'</a> </p>
	                    

	                    


	                    <a class="text-primary" target="_blank" href="http://tae-ltda.com/">&copy; Todos los derechos Reservados Tecnolog&iacute;a de Administraci&oacute;n Empresarial LTDA. Visitenos en www.siminmueble.com</a>
	                    <p>Esta direcci&oacute;n es solo para notificaciones, por favor no responda este correo.</p>

	                </div>
	            </div>
	        </body>
	    </html>';
	    return $cuerpo;

	}

	
	
}

?>