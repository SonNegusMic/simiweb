<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

class GrupoInmobiliarios
{
	var $inmosmigrupo;
	var $gruposexistentes;
	var $inmootrosgrupos;
	
	public function __construct($conn=""){
		$this->db=$conn;
	}
	
	public function GrupoInmobiliario()
	{
		$this->inmosmigrupo="";
		$this->gruposexistentes="";
		$this->inmootrosgrupos="";
	}
	
    
	public function MiGrupo($data)
	{
		/*CLASE DE CONEXIÓN A LA BASE DE DATOS CON PDO*/
		$connPDO= new Conexion();	
		
			//Consulta a que grupos o grupo pertenece la inmobiliaria activa
		$stmt=$connPDO->prepare("SELECT  it.IdGrupoInmo  
								 FROM item_grupos it 
								 INNER  JOIN grupoinmobiliaria g 
								 		ON it.IdGrupoInmo = g.IdGrupoInmo
								 INNER JOIN restricciongrupo r 
								 		ON g.IdRestriccion = r.IdRestriccion
								 WHERE  it.IdInmobiliaria=:IdInmobiliaria");


 		$stmt->bindParam(":IdInmobiliaria",$data);
  // echo $consultagrupos."<br>";
   	if ($stmt->execute()) 
			{
				if ($stmt->rowCount()>0) 
				{
					$gru=' ';
					$con=0;
				  	 while ($row=$stmt->fetch())
				 	 {
						  //$res=$fila7['IdRestriccion'];
						 if($con==0)
							 {
							 	  $gru=$gru.$row['IdGrupoInmo'];
							 }
						else
							{
								   $gru ="".$gru.",".$row['IdGrupoInmo']."";
							}
						$con++;
					  }
					  //echo $gru;
					              //consulta  que inmobiliarias pertencen a ese GrupoInmobiliario
					  $cant=explode(",",$gru);
					
						$in  = str_repeat('?,', count($cant) - 1) . '?';
	$stmt2=$connPDO->prepare("SELECT  DISTINCT (IdInmobiliaria) 
										from item_grupos 
										where IdGrupoInmo IN ($in)");
    				   if ($stmt2->execute($cant)) 
					  {
						 
						  if ($stmt2->rowCount()>0) 
						  {
							// $cantidad1=$Base->num_filas($consultainmos);
							$inmobili=' ';
							$cont=0;
							//echo $cantidad1;
					   			while ($row=$stmt2->fetch())
					  			{
						  			if($cont==0)
						 			 {
						  			 $inmobili= $inmobili."'".$row['IdInmobiliaria']."'";
						  			 }
						  			else
						  			{
						  			 $inmobili= $inmobili.", '".$row['IdInmobiliaria']."'";
						  			}
						  			$cont++;
					  		  }
						 //  echo "INMOBILIARAS DEL GRUPO AL QUE PERTENEZCO".$inmobili."<hr>";
						
						$this->inmosmigrupo=$inmobili;
						
						   }
						}
					 }

			    } 	
		$stmt =null;
		$stmt2 =null;	
	}
	public function InmMiGrupo()
	{
		return $this->inmosmigrupo;
	}
	public function GruposInmobiliarios()
	{
	   return $this->gruposexistentes;
	}
	 public function InmOtrosGrupos()
	{
		return $this->inmootrosgrupos;
	}
	
}


?>