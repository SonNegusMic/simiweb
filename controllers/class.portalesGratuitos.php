<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include("../funciones/connPDO.php");


class portalesGratutios
{
    
    public function __construct($conn=''){
		$this->db=$conn;
		
	}	
	public function curlXML($path)
	{
		    $path=utf8_decode($path);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$path);
            curl_setopt($ch, CURLOPT_FAILONERROR,1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            return $retValue = curl_exec($ch);                      
            curl_close($ch);
	}
    public function inmobiliariaXML()
    {
        $connPDO = new Conexion();
        $stmt=$connPDO->prepare("SELECT c.IdInmobiliaria
            from inmobiliaria c,PublicaPortales p 
            WHERE c.IdInmobiliaria=p.IdInmobiliaria
            and p.IdPortal=1 
            and estado_inmo=1 
            #limit 0,10");
        if($stmt->execute())
        {
            $data=array();           
            while ($row=$stmt->fetch()) {
                $idInmob = $row['IdInmobiliaria'];
                $url="https://www.simiinmobiliarias.com/mcomercialweb/a_inmueblesportalesdoomos_maxtest.php?inmo=".$idInmob;

                $inmueble = str_replace(" ","%20",$url);
                $data[]=array(
                    "Codigo"=>$sXML=$this->curlXML($url),
                    );


            }
            return $data;
        }
        else
        {
            print_r($stmt->errorInfo()); 
        }
        $stmt="";
    }   
    public function inmobiliariaOlxXML()
    {
        $connPDO = new Conexion();
        $stmt=$connPDO->prepare("SELECT c.IdInmobiliaria
            from inmobiliaria c,PublicaPortales p 
            WHERE c.IdInmobiliaria=p.IdInmobiliaria
            and p.IdPortal=15 
            and estado_inmo=1 
            #limit 0,10");
        if($stmt->execute())
        {
            $data=array();           
            while ($row=$stmt->fetch()) {
                $idInmob = $row['IdInmobiliaria'];
                $url="https://www.simiinmobiliarias.com/mcomercialweb/a_inmuebles_olx.php?inmo=".$idInmob;

                $inmueble = str_replace(" ","%20",$url);
                $data[]=array(
                    "Codigo"=>$sXML=$this->curlXML($url),
                    );


            }
            return $data;
        }
        else
        {
            print_r($stmt->errorInfo()); 
        }
        $stmt="";
    }  
    public function inmobiliariaGptXML()
    {
        $connPDO = new Conexion();
        $stmt=$connPDO->prepare("SELECT c.IdInmobiliaria
            from inmobiliaria c,PublicaPortales p 
            WHERE c.IdInmobiliaria=p.IdInmobiliaria
            and p.IdPortal=6
            and estado_inmo=1 
            #limit 0,10");
        if($stmt->execute())
        {
            $data=array();           
            while ($row=$stmt->fetch()) {
                $idInmob = $row['IdInmobiliaria'];
                $url="https://www.simiinmobiliarias.com/mcomercialweb/a_creaxmlGP.php?inmob=".$idInmob;

                $inmueble = str_replace(" ","%20",$url);
                $data[]=array(
                    "Codigo"=>$sXML=$this->curlXML($url),
                    );


            }
            return $data;
        }
        else
        {
            print_r($stmt->errorInfo()); 
        }
        $stmt="";
    }  
    public function republicaM2()
    {
        $connPDO = new Conexion();
        $stmt=$connPDO->prepare("SELECT c.IdInmobiliaria
            from inmobiliaria c,PublicaPortales p 
            WHERE c.IdInmobiliaria=p.IdInmobiliaria
            and p.IdPortal=10
            #and p.IdInmobiliaria=422
            and estado_inmo=1 
            and p.IdInmobiliaria>1
            #limit 0,10");
        if($stmt->execute())
        {
            $data=array();           
            while ($row=$stmt->fetch()) {
                $idInmob = $row['IdInmobiliaria'];
                $url="https://www.simiinmobiliarias.com/mcomercialweb/republicarM2Curl.php?inmob=".$idInmob;

                $inmueble = str_replace(" ","%20",$url);
                $data[]=array(
                    "Codigo"=>$sXML=$this->curlXML($url),
                    );


            }
            return $data;
        }
        else
        {
            print_r($stmt->errorInfo()); 
        }
        $stmt="";
    }  
}