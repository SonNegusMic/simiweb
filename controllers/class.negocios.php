<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include("../funciones/connPDO.php");

class Negocios
{
    public function __construct($conn=""){
		$this->db=$conn;
	}
    
    private $id_usuario;
  
	public function consultaClienteSelect($cedula,$inmob,$idselect)
    {
	   $w_conexion = new MySQL(); 
	   getSelectP('clientes_inmobiliaria','cedula', 'CONCAT(UPPER(LEFT(nombre, 1)), LOWER(SUBSTRING(nombre, 2)))',"","where inmobiliaria=$inmob","$idselect","",'','class="form-control chosen-select" id="$idselect"','','','Seleccione Cliente');
    }
	public function consultaCliente($cedula,$inmob)
    {
 		if(($consulta=$this->db->prepare(" select telfijo,telcelular,email
		from clientes_inmobiliaria
        where cedula=?
        and inmobiliaria=?")))
		{  	
			$consulta->bind_param("ii",$cedula,$inmob);
			$consulta->execute();
//				echo "$qry";
			if($consulta->bind_result($telfijo,$telcelular,$email))
			{
				while($consulta->fetch())
				{
					$cadena=$telfijo."  ".$telcelular."?".$email;    
				}
			}
			echo $cadena;
		}
		else
		{
			echo  "error --".$conn->error;
		}
        
    }
	
	public function creaDemanda($inmob,$FechaD,$IdPromotor,$numced,$calle,$alacalle,$carrera,$alacarrera,$Ciudad,$barrio,$idgestion,$IdDestinacion,$IdTipoInm,$AreaIni,$AreaFin,$ValInicial,$ValFinal,$mgrupo,$descripcion,$fsis,$fechaLimite)
	{
 		$w_conexion = new MySQL(); 
		include('../mcomercialweb/emailtaeD.class.php');
		$f_sis=date('Y-m-d');
		$clave='0000';
		$idDemanda=consecutivo('IdDemanda','demandainmuebles',0);
		$sql="INSERT INTO demandainmuebles 
		(FechaD,IdPromotor,FechaLimite,CedulaIInt,Calle,
		 AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
		 IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
		 Descripcion,clave,IdCiudad,IdInmobiliaria,IdBarrio,
		 grupo_dem,IdDemanda)
		 VALUES ('$fsis','$IdPromotor','$fechaLimite','$numced','$calle',
			'$alacalle','$carrera','$alacarrera','$idgestion','$IdDestinacion',
			'$IdTipoInm','$ValInicial','$ValFinal','$AreaIni','$AreaFin',
			'$descripcion','$clave','$Ciudad','$inmob','$barrio',
			'$mgrupo','$idDemanda')";
			$res=$w_conexion->RecordSet($sql);
			if($res)
			{
				//echo "res $sql";
				echo $idDemanda;
				$envioemail = new EmailTae();
   			 	$envioemail->EnvioEmailDemandas($IdPromotor,$f_sis,$fechaLimite,$numced,$idDemanda);
		   	 	$enviado= $envioemail->ResultadoCorreoDemada();
			}
		/*if(($consulta=$this->db->prepare("INSERT INTO demandainmuebles 
		(FechaD,IdPromotor,FechaLimite,CedulaIInt,Calle,
		 AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
		 IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
		 Descripcion,clave,IdCiudad,IdInmobiliaria,IdBarrio,
		 grupo_dem,IdDemanda)
		 VALUES (?,?,?,?,?,
		 ?,?,?,?,?,
		 ?,?,?,?,?,
		 ?,?,?,?,?,
		 ?,?)")))
		{  	
			$consulta->bind_param("sisiiiiiiiiiiissiiiiii",$fsis,$IdPromotor,$FechaD,$numced,$inmob,
			$alacalle,$alacarrera,$idgestion,$IdDestinacion,$FechaD,
			$IdTipoInm,$ValInicial,$ValFinal,$AreaIni,$AreaFin,
			$descripcion,$clave,$Ciudad,$inmob,$barrio,
			$mgrupo,$idDemanda);
			
			$consulta->execute();
			echo $idDemanda."hola";
		}
		else
		{
			echo  "error --".$conn->error;
		}*/
    }
     public function creaDemandaNvo($inmob,$FechaD,$IdPromotor,$numced,$calle,$alacalle,$carrera,$alacarrera,$Ciudad,$barrio,$idgestion,$IdDestinacion,$IdTipoInm,$AreaIni,$AreaFin,$ValInicial,$ValFinal,$mgrupo,$descripcion,$fsis,$barrios,$localidad,$estrato,$FechaLimite)
	{
 		$w_conexion = new MySQL(); 
		 include('../mcomercialweb/emailtaeD.class.php');
		$f_sis=date('Y-m-d');
		$clave='0000';
		$idDemanda=consecutivo('IdDemanda','demandainmuebles',0);
		$sql="INSERT INTO demandainmuebles 
		(FechaD,IdPromotor,FechaLimite,CedulaIInt,Calle,
		 AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
		 IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
		 Descripcion,clave,IdCiudad,IdInmobiliaria,IdBarrio,
		 grupo_dem,IdDemanda,localidad,estrato_dem)
		 VALUES ('$fsis','$IdPromotor','$FechaLimite','$numced','$calle',
			'$alacalle','$carrera','$alacarrera','$idgestion','$IdDestinacion',
			'$IdTipoInm','$ValInicial','$ValFinal','$AreaIni','$AreaFin',
			'$descripcion','$clave','$Ciudad','$inmob','$barrio',
			'$mgrupo','$idDemanda','$localidad','$estrato')";
			$res=$w_conexion->RecordSet($sql);
		if($res)
		{
			echo  "$idDemanda";
				$envioemail = new EmailTae();
   			 	$envioemail->EnvioEmailDemandas($IdPromotor,$f_sis,$FechaLimite,$numced,$idDemanda);
		   	 	$enviado= $envioemail->ResultadoCorreoDemada();
			/*
			recibimos la cadena de barrios y los descomponemos para hacer un ciclo
			 */
			if($barrios)
			{	
				$barrios=substr($barrios,0,-1);
				$cad=explode(",",$barrios);

				for($i=0;$i<count($cad);$i++)
				{	
					list($bar1,$loc1)=explode("-",$cad[$i]);
					$conse_de=consecutivo('conse_de','demandas_barrios',0);
					$sql_bdm="INSERT INTO demandas_barrios
					(conse_de,id_demanda_de,id_ciu_de,id_loc_de,id_barrio_de,estado_de)
					VALUES('$conse_de','$idDemanda','$Ciudad','$loc1','".$bar1."','1')";
					//echo "insert $sql_bdm <br>";
					$resbdm=$w_conexion->RecordSet($sql_bdm) or die("error ".mysql_error());
				}
			}
			
		}
		else
		{
			//echo $sql;
		}
    }
	
	public function editaDemanda($inmob,$FechaD,$IdPromotor,$numced,$calle,$alacalle,$carrera,$alacarrera,$Ciudad,$barrio,$idgestion,$IdDestinacion,$IdTipoInm,$AreaIni,$AreaFin,$ValInicial,$ValFinal,$mgrupo,$descripcion,$fsis,$IdDemanda,$Estado)
	{
 		$w_conexion = new MySQL(); 
		$f_sis=date('Y-m-d');
		$h_sis=date('H:i:s');
		$usuario=$_SESSION['Id_Usuarios'];
		$ip=$_SERVER['REMOTE_ADDR'];

		
		//consulta de valores originales para guardar bitácora 2014-12-16
		$sql_orig="SELECT IdDemanda,IdPromotor,FechaLimite,Calle,
					AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
					IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
					Descripcion,Estado,IdBarrio,grupo_dem
					FROM demandainmuebles
					where IdDemanda='$IdDemanda'";
		$res_or=$w_conexion->ResultSet($sql_orig);
		while($f_or=$w_conexion->FilaSiguienteArray($res_or))
		{
			$IdPromotor_or 		= $f_or['IdPromotor']; 
			$FechaLimite_or 	= $f_or['FechaLimite']; 
			$Calle_or 			= $f_or['Calle']; 
			$AlaCalle_or 		= $f_or['AlaCalle']; 
			$Carrera_or 		= $f_or['Carrera']; 
			$AlaCarrera_or 		= $f_or['AlaCarrera']; 
			$IdGestion_or 		= $f_or['IdGestion']; 
			$IdDestinacion_or 	= $f_or['IdDestinacion']; 
			$IdTipoInm_or 		= $f_or['IdTipoInm']; 
			$ValInicial_or 		= $f_or['ValInicial']; 
			$ValFinal_or 		= $f_or['ValFinal']; 
			$AreaInicial_or 	= $f_or['AreaInicial']; 
			$AreaFinal_or 		= $f_or['AreaFinal'];
			$Descripcion_or 	= $f_or['Descripcion']; 
			$Estado_or 			= $f_or['Estado']; 
			$IdBarrio_or 		= $f_or['IdBarrio']; 
			$grupo_dem_or 		= $f_or['grupo_dem'];
		}
		
		$sql="UPDATE demandainmuebles 
				SET		
				IdPromotor			='$IdPromotor',
				FechaLimite			='$FechaD',	
				CedulaIInt			='$numced',	
				Calle				='$calle',
		 		AlaCalle			='$alacalle',
				Carrera				='$carrera',
				AlaCarrera			='$alacarrera',
				IdGestion			='$idgestion',
				IdDestinacion		='$IdDestinacion',
		 		IdTipoInm			='$IdTipoInm',
				ValInicial			='$ValInicial',
				ValFinal			='$ValFinal',
				AreaInicial			='$AreaIni',
				AreaFinal			='$AreaFin',
		 		Descripcion			='$descripcion',
				IdCiudad			='$Ciudad',
				IdBarrio			='$barrio',
		 		grupo_dem			= '$mgrupo',
				Estado				= '$Estado'	
				WHERE IdDemanda		= '$IdDemanda'";
			$res=$w_conexion->RecordSet($sql);
			if($res)
			{
				///guardar historial
				if($IdPromotor_or!=$IdPromotor)
				{
					$cambio++;
					$nom_campo="IdPromotor";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Promotor"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$IdPromotor,$IdPromotor_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($FechaLimite_or!=$FechaD)
				{
					$cambio++;
					$nom_campo="FechaLimite";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Fecha Limite"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$Fechalimite,$FechaLimite_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($Calle_or!=$calle)
				{
					$cambio++;
					$nom_campo="Calle";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Calle"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$calle,$Calle_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($AlaCalle_or!=$alacalle)
				{
					$cambio++;
					$nom_campo="AlaCalle";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="A la Calle"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$alacalle,$AlaCalle_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($Carrera_or!=$carrera)
				{
					$cambio++;
					$nom_campo="Carrera";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Carrera"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$carrera,$Carrera_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($AlaCarrera_or!=$alacarrera)
				{
					$cambio++;
					$nom_campo="AlaCarrera";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="A la Carrera"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$alacarrera,$AlaCarrera_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($IdGestion_or!=$idgestion)
				{
					$cambio++;
					$nom_campo="IdGestion";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Gestion"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$idgestion,$IdGestion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($IdDestinacion_or!=$IdDestinacion)
				{
					$cambio++;
					$nom_campo="IdDestinacion";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Destinacion"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$IdDestinacion,$IdDestinacion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($IdTipoInm_or!=$IdTipoInm)
				{
					$cambio++;
					$nom_campo="IdTipoInm";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Tipo de Inmueble"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$IdTipoInm,$IdTipoInm_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($ValInicial_or!=$ValInicial)
				{
					$cambio++;
					$nom_campo="ValInicial";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Valor Inicial"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$ValInicial,$ValInicial_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($ValFinal_or!=$ValFinal)
				{
					$cambio++;
					$nom_campo="ValFinal";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Valor Final"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$ValFinal,$ValFinal_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($AreaInicial_or!=$AreaIni)
				{
					$cambio++;
					$nom_campo="AreaInicial";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Area Inicial"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$AreaIni,$AreaInicial_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($AreaFinal_or!=$AreaFin)
				{
					$cambio++;
					$nom_campo="AreaFin";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Area Final"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$AreaFin,$AreaFinal_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($Descripcion_or!=$descripcion)
				{
					$cambio++;
					$nom_campo="Descripcion";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Descripcion"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$descripcion,$Descripcion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($Estado_or!=$Estado)
				{
					$cambio++;
					$nom_campo="estado_dem";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Estado"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$Estado,$Estado_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($IdBarrio_or!=$barrio)
				{
					if(is_numeric($barrio))
					{
						$cambio++;
						$nom_campo="barrio";
						$cons_log=consecutivo_log(1);
						$campo_usu_g="Barrio"; //nombre del campo en el formulario
						$param_g=0; //codigo de la tabla par?metros
						guarda_log_gral($cons_log,1,$nom_campo,$barrio,$IdBarrio_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
					}
				}
				if($grupo_dem_or!=$mgrupo)
				{
					$cambio++;
					$nom_campo="grupo";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Grupo"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$mgrupo,$grupo_dem_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
	
				echo $idDemanda;
	}
}
        
    
	public function datosDemanda($idDemanda,$inmob)
    {
        $w_conexion = new MySQL();
		$arreglo1 = array();
		$sqlpagos = "SELECT FechaD,IdPromotor,FechaLimite,CedulaIInt,Calle,
		 AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
		 IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
		 Descripcion,clave,IdCiudad,IdInmobiliaria,IdBarrio,
		 grupo_dem,IdDemanda,Estado,estrato_dem,localidad
		 from demandainmuebles
		 where IdDemanda='$idDemanda'";
		//echo $sqlpagos."<br>";			
		$res=$w_conexion->ResultSet($sqlpagos);
		$cantidadt=$w_conexion->FilasAfectadas($res);
		if($cantidadt==0)
		{echo "No Hay Demandas para la busqueda seleccionada";}
		
		while ($fila7=$w_conexion->FilaSiguienteArray($res))
		{
			$i++;
				
			$arreglo[0]  =$fila7['FechaD'];
			$arreglo[1]  =$fila7['IdPromotor'];
			$arreglo[2]  =$fila7['FechaLimite'];
			$arreglo[3]  =$fila7['CedulaIInt'];
			$arreglo[4]  =ucwords(strtolower(getCampo('cliente_captacion','WHERE id_cliente_capta='.$fila7['CedulaIInt'].' and id_inmobiliaria='.$inmob,'nombre')));
			$arreglo[5]  =$fila7['Calle'];
			$arreglo[6]  =$fila7['IdBarrio'];
			$arreglo[7]  = ucwords(strtolower(getCampo('ciudad','WHERE IdCiudad='.getCampo('barrios','WHERE IdBarrios='.$fila7['IdBarrio'],'IdCiudad'),'Nombre')));
			$arreglo[9]  =$fila7['AlaCalle'];
			$arreglo[10] =$fila7['Carrera'];
			$arreglo[11] =$fila7['AlaCarrera'];
			$arreglo[12] =$fila7['IdGestion'];
			$arreglo[13] =$fila7['IdDestinacion'];
			$arreglo[14] =$fila7['IdTipoInm'];
			$arreglo[15] =$fila7['ValInicial'];
			$arreglo[16] =$fila7['ValFinal'];
			$arreglo[17] =$fila7['AreaInicial'];
			$arreglo[18] =$fila7['AreaFinal'];
			$arreglo[19] =$fila7['Descripcion'];
			$arreglo[20] =$fila7['IdCiudad'];
			$arreglo[21] =$fila7['grupo_dem'];
			$arreglo[22] =$fila7['Estado'];
			$arreglo[23] =$fila7['estrato_dem'];
			$arreglo[24] =$fila7['localidad'];
			$arreglo[25] =$this->barriosDemanda($idDemanda);
			$arreglo[26] =getCampo('localidad',"where idlocalidad=".$fila7['localidad'],'idzona',0);
			$arreglo[27] =$idDemanda;
			 
			 $arreglo1[] = $arreglo;		
		}
		return $arreglo1;
    }
    public function barriosDemanda($idDemanda)
    {
        $w_conexion = new MySQL();
		$arreglo = array();
		$sqlpagos = "SELECT id_demanda_de,id_ciu_de,id_loc_de,id_barrio_de
		 from demandas_barrios
		 where id_demanda_de='$idDemanda'
		 and estado_de='1'";
		//echo $sqlpagos."<br>";			
		$res=$w_conexion->ResultSet($sqlpagos);
		$cantidadt=$w_conexion->FilasAfectadas($res);
		if($cantidadt==0)
		{echo "No Hay Barrios especificos para esta demanda";}
		
		while ($fila7=$w_conexion->FilaSiguienteArray($res))
		{
			$i++;
				
			$arreglo[]  =array(
				"idDemanda"		=>	$fila7['id_demanda_de'],
				"ciudadDem"		=>	$fila7['id_ciu_de'],
				"localidadDem"	=>	$fila7['id_loc_de'],
				"barrioDem"		=>	$fila7['id_barrio_de'],
				"nBarrioDem"	=>	ucwords(strtolower(getCampo('barrios',"where IdBarrios=".$fila7['id_barrio_de'],'NombreB'))),
			);
		}
		return $arreglo;
    }
    
    public function eliminaBarriosDemanda($data)
	{
		$connPDO=new Conexion();
		$idDemanda 	=$data['idDemanda'];
		list($idBarrio,$localidad) 	=explode("-",$data['idBarrio']);
 
		$stmt=$connPDO->prepare("
        DELETE from  demandas_barrios
        where id_demanda_de=:idDemanda
        and id_barrio_de=:idBarrio");

		if($stmt->execute(array(
			":idDemanda"    => $idDemanda,
            ":idBarrio"  	=> $idBarrio
			)))
		{
	   		return 1;
		}
		else
		{
			return print_r($stmt->errorInfo());
			
		}
        $stmt=NULL;
	}
	
	public function datosMisDemanda($tip_inm,$dest,$tip_ope,$asesor,$est_cli,$cod_inm,$ffecha_ini,$ffecha_fin,$inmob,$perfil,$usu)
    {
        $w_conexion = new MySQL();
		
		$cond="";
		$cond2 ="";
		if($tip_inm>0)
		{
			$cond .=" and IdTpInm='".$tip_inm."'";
		}
		if($dest>0)
		{
			$cond .=" and IdDestinacion='".$dest."'";
		}
		if($tip_ope>0)
		{
			if($tip_ope!=2)
			{
				$cond .=" and IdGestion='".$tip_ope."'";
			}
			else
			{
				$cond .=" and IdGestion in ($tip_ope,1,5)";
			}
		}
		if($asesor>0)
		{
			$cond .=" and IdPromotor='".$asesor."'";
		}
		if($est_cli>0)
		{
			$cond .=" and est_cm='".$est_cli."'";
		}
		if($cod_inm>0)
		{
			$cond .=" and inmuebles.idInm='".$cod_inm."'";
		}
        if($ffecha_ini>0 and $ffecha_fin>0)
		{
			$cond .=" and fecha_cm  between'".$ffecha_ini."' and '".$ffecha_fin."'";
		}
		if($inmob!=1)
		{
			$cond2 .="AND IdInmobiliaria = '".$inmob."'";
			// $cond2 .="AND IdInmobiliaria =84";
		}
		if($perfil==3 or $perfil==10)
		{
			//$cond2 .=" ";
		}
		else
		{
			$cond2 .=" AND IdPromotor = '".$usu."'";
		}
		$hoy=date("Y-m-d");
		$sqlpagos = "";
		
		$data = array();
		$sqlpagos = "Select idInm,  IdGestion,  IdTpInm, ComiArren,ComiVenta,  
					Direccion,  IdBarrio ,IdInmobiliaria,condiciones_compartir_inm.*,IdPromotor,
					ValComiVenta,ValComiArr,IdDestinacion
					FROM inmuebles,condiciones_compartir_inm
					WHERE  idInm=id_inmu_cm
					and idEstadoinmueble<=2
					$cond2
					$cond";
		//echo $sqlpagos."-------------------<br>";
//		echo $perfil."ffffffffffffff";
				
		$res=$w_conexion->ResultSet($sqlpagos);
		$cantidadt=$w_conexion->FilasAfectadas($res);
		if($cantidadt==0)
		{
			//echo "No Hay Demandas para la busqueda seleccionada";
		}
		
		while ($fila77=$w_conexion->FilaSiguienteArray($res))
		{
			$i++;
			
			$lupa='<button type="button" id="botonpre2" class="btn btn-warning botonpre2 btn-xs"  data-toggle="modal"  data-target=".mymodales"><span class="glyphicon glyphicon-edit"></span></button>';
					
			$btnCLiente='<button type="button" id="botonpre3" class="btn btn-info  botonpre3  btn-xs"  data-toggle="modal"  data-target=".mymodalescli"><span class="glyphicon glyphicon"><i class="fa fa-files-o"></i></span></button>';
					
					$botonedit = '<input type="hidden" class="idseg" value="'.$idseguimiento.'"><input type="hidden" class="idimb" value="'.$idInmueble.'">'.$lupa." ".$btnCLiente;
			
			 $data[]=array(
				"idInm"          => $fila77['idInm'],
				"Nombres"        => ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$fila77['IdPromotor']."'",'Nombres'))),
				"apellidos"      => ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$fila77['IdPromotor']."'",'apellidos'))),
				"NCompleto"      => ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$fila77['IdPromotor']."'",'concat(Nombres," ",apellidos)'))),
				"condiciones_cm" => $fila77['condiciones_cm'],
				"ComiVenta"      => $fila77['ComiVenta']*100,
				"ComiArren"      => $fila77['ComiArren']*100,
				"ValComiArr"     => number_format($fila77['ValComiArr']),
				"ValComiVenta"   => number_format($fila77['ValComiVenta']),
				"NombresGestion" => ucwords(strtolower(getCampo('gestioncomer',"where IdGestion='".$fila77['IdGestion']."'",'NombresGestion'))),
				"destinacion"    => ucwords(strtolower(getCampo('destinacion',"where Iddestinacion='".$fila77['IdDestinacion']."'",'Nombre'))),
				"tipoinmuebles"  => ucwords(strtolower(getCampo('tipoinmuebles',"where idTipoInmueble='".$fila77['IdTpInm']."'",'Descripcion'))),
				"estcm"          => getCampo('parametros',"WHERE id_param=22 and conse_param='".$fila77['est_cm']."'",'desc_param'),
				"barr"           => ucwords(strtolower(getCampo('barrios',"WHERE IdBarrios='".$fila77['IdBarrio']."'",'NombreB'))),
				"botones"        => $botonedit
			 );
			 	
		}
		echo json_encode($data);
    }
    public function getUsuariosDemandas($idInmo)
     {
      $connPDO= new Conexion();
      $stmt = $connPDO->prepare("SELECT u.Nombres,
                  						u.Id_Usuarios,
                  						u.apellidos
          						FROM usuarios u,
               						 demandainmuebles d
          						WHERE u.Id_Usuarios=d.IdPromotor
          						AND u.IdInmmo=:idInmmo
          						GROUP BY d.IdPromotor
          						ORDER BY u.Nombres");
      $stmt->bindParam(":idInmmo", $idInmo["idInmmo"]);
      $stmt->execute();
      $data=array();
      while($row=$stmt->fetch())
      {
       $data[]=array(
         "id"=>$row["Id_Usuarios"],
         "nombres"=>ucwords(strtolower($row["Nombres"])),
         "apellidos"=>ucwords(strtolower($row['apellidos'])));
      }
      return $data;

     }
     public function listDemandas($data)
     {
		  
		   $connPDO= new Conexion();
		  
		   $cond="";
		   if($data['tip_inm'])
		   {
		    $cond .=" and d.IdTipoInm=:inmueble";

		   }
		   if($data['dest'])
		   {
		    $cond .=" and d.IdDestinacion=:dest";
		   }
		   if($data['tip_ope'])
		   {
			   if($data['tip_ope']!=2)
			   {
				$cond .=" and d.IdGestion=:tip_ope";
			   }
			   else
			   {
				   $cond .=" and d.IdGestion != 3";
			   }
		   }
		   if($data['asesor'])
		   {
		    $cond .=" and d.IdPromotor=:asesor";
		   }
		   if($data['est_cli'])
		   {
		    $cond .=" and d.Estado=:est_cli";
		   }
		   if($data['codDemanda'])
		   { //echo "ingresa a codDemanda ".$_GET['codDemanda'];
		    $cond .=" and d.IdDemanda=:codDemanda";
		   }
			if($data['interesado']) 
			{
				$cond .=" and d.CedulaIInt =:interesado";
   			}
   			if($data["ffecha_ini"]>0 and $data["ffecha_fin"]>0)
			{
				$cond .=" and fecha_cm  between :ffecha_ini and :ffecha_fin";
			}
			if($data['idInmmo']!=1)
			{
				$cond2 .="AND IdInmobiliaria = :idInmmo";
				
			}
			if($data['perfil']==3 or $data['perfil']==10)
			{
			
			}
			$hoy=date("Y-m-d");
 
			 $stmt = $connPDO->prepare("SELECT d.IdDemanda, 
								d.FechaD, 
								d.IdPromotor, 
								d.FechaLimite, 
			      				d.CedulaIInt,
			      				d.Calle,
			      				d.AlaCalle,
			      				d.Carrera, 
			      				d.AlaCarrera,                                    
			      				d.IdGestion, 
			      				d.IdDestinacion, 
			      				d.IdTipoInm, 
			      				d.ValInicial, 
			      				d.ValFinal, 
			      				d.AreaInicial, 
			      				d.AreaFinal, 
			      				d.Descripcion, 
			                    c.Nombre as Interesado,
			                    c.email,
			                    c.telfijo,
			                    c.telcelular,
			                    g.NombresGestion, 
			                    t.Descripcion As TipoInmueble, 
			      				c.cedula as cedulaInte,
			      				d.Estado, 
			      				ba.NombreB, 
			      				d.grupo_dem,
			      				des.Nombre as Nombredes,
			      				IF ('$hoy'  BETWEEN d.FechaD AND d.FechaLimite,1,0) 
			      				as RESULTADO
			      		FROM 	demandainmuebles d 
			      		INNER JOIN clientes_inmobiliaria c On d.CedulaIInt = c.cedula 
			      		LEFT JOIN barrios ba ON ba.IdBarrios 			   = d.IdBarrio
			      		INNER JOIN gestioncomer g On d.IdGestion           =g.IdGestion
			      		INNER JOIN tipoinmuebles t On d.IdTipoInm =t.idTipoInmueble 
			      		INNER JOIN destinacion des on des.Iddestinacion = d.IdDestinacion 
			      		WHERE c.inmobiliaria = :idInmmo
			      		$cond2
			      		$cond");
			if($data['tip_inm'])
		   {
		    $stmt->bindParam(":inmueble",$data['tip_inm']);

		   }
		   if($data['dest'])
		   {
		    $stmt->bindParam(':dest',$data["dest"]);
		   }
		   if($data['tip_ope'])
		   {
		    $stmt->bindParam(":tip_ope",$data["tip_ope"]);
		   }
		   if($data['asesor'])
		   {
		    $stmt->bindParam(":asesor",$data["asesor"]);
		   }
		   if($data['est_cli'])
		   {
		    $stmt->bindParam(':est_cli',$data["est_cli"]);
		   }
		   if($data['codDemanda'])
		   { //echo "ingresa a codDemanda ".$_GET['codDemanda'];
		    $stmt->bindParam(":codDemanda",$data["codDemanda"]);
		   }
			if($data['interesado']) 
			{
				$stmt->bindParam(":interesado",$data["interesado"]);
   			}
   			if($data["ffecha_ini"]>0 and $data["ffecha_fin"]>0)
			{
				$stmt->bindParam(':ffecha_ini',$data["ffecha_ini"]);
				$stmt->bindParam(':ffecha_fin',$data["ffecha_fin"]);
			}
			if($data['idInmmo']!=1)
			{
				$cond2 .="AND IdInmobiliaria = :idInmmo";
				
			}
			if($data['perfil']==3 or $data['perfil']==10)
			{
			
			}
			
			$stmt->bindParam(':idInmmo',$data["idInmmo"]);
			$stmt->execute();
      		$listnegocios=array();
      		while($row=$stmt->fetch())
      		{
      			

      			$negociarBtn=($row["RESULTADO"] == 0)?"":"<button class='btn btn-success btn-sm'><span class='glyphicon glyphicon-ok'></span></button>";
      			$editarBtn="<button class='btn btn-warning btn-sm' ><span class='fa fa-edit'></span></button>";
      			$cambiosBtn="<button class='btn btn-info btn-sm' data-toggle='modal' data-target='#modal-logs'><span class='glyphicon glyphicon-eye-open'></span></button>";
      			$seguimientoBtn="<button class='btn btn-danger btn-sm' data-toggle='modal' data-target='#modal-id' alt='seguimiento'><span class='fa fa-sign-in'></span></button>";
      			// $ofertabtn=($this->seguiDemandas($row["IdDemanda"])==1)?"<button class='btn btn-success btn-sm'><span class='glyphicon glyphicon-ok'></span></button>":"";
      			$dataModal=$negociarBtn.$editarBtn.$cambiosBtn.$seguimientoBtn;
      			$calle=ucwords(strtolower("De la calle ".$row["Calle"]." a la ".$row["AlaCalle"]));
      			$carrera=ucwords(strtolower("De la carrera ".$row["Carrera"]." a la ".$row["AlaCarrera"]));
      			$dataCliente=$row['telfijo']."-".$row['telcelular'];
      				
      			$listnegocios[]=array(
									"idemanda"     =>$row["IdDemanda"],
									"destinacion"  =>ucwords(strtolower($row["Nombredes"])),
									"fechaLimite"  =>$row["FechaLimite"],
									"fechaD"       =>$row["FechaD"],
									"interesado"   =>ucwords(strtolower($row["Interesado"])),
									"barrio"       =>ucwords(strtolower($row["NombreB"])),
									"gestion"      =>ucwords(strtolower($row["NombresGestion"])),
									"tipoInmueble"  =>ucwords(strtolower($row["TipoInmueble"])),
									"valorInicial" =>"$".number_format($row["ValInicial"]),
									"valorFinal"   =>"$".number_format($row["ValFinal"]),
									"Calle"	   	   =>$calle,
									"Carrera"	   =>$carrera,
									"btnModal"     =>$dataModal,
									"telcli"	   =>$dataCliente,
									"mailcli"      =>$row["email"]         
									
      							);
      		}
      		return $listnegocios;
     } //fin function listDemandas()
	 
	 
	 public function listInmDemandas($data)
     {
		$connPDO= new Conexion();
		
		$cond="";
		if($data['tip_inm'])
		{
		$cond .=" and i.IdTpInm=:inmueble";
		
		}
		if($data['dest'])
		{
		$cond .=" and i.IdDestinacion=:dest";
		}
		if($data['tip_ope'])
	   {
		   if($data['tip_ope']!=2)
		   {
			$cond .=" and i.IdGestion=:tip_ope";
		   }
		   else
		   {
			   $cond .=" and i.IdGestion != 3";
		   }
	   }
		if($data['asesor'])
		{
		$cond .=" and i.IdPromotor=:asesor";
		}
		if($data['idInm'])
		{ //echo "ingresa a codDemanda ".$_GET['codDemanda'];
		$cond .=" and i.idInm=:idInm";
		}
		/*if($data['idInmmo']!=1)
		{
			$cond2 .="AND IdInmobiliaria = :idInmmo";
		}*/
		if($data['perfil']==3 or $data['perfil']==10)
		{
		
		}
		$hoy=date("Y-m-d");
		
		 $stmt = $connPDO->prepare("SELECT idInm,IdGestion,IdDestinacion,ValorVenta,ValorCanon,IdPromotor,IdTpInm,politica_comp
					FROM inmuebles i 
					WHERE i.IdInmobiliaria = :idInmmo
					and i.politica_comp!=''
					$cond2
					$cond");
		if($data['tip_inm'])
		{
			$stmt->bindParam(":inmueble",$data['tip_inm']);
		}
		if($data['dest'])
		{
			$stmt->bindParam(':dest',$data["dest"]);
		}
		if($data['tip_ope'])
		{
			$stmt->bindParam(":tip_ope",$data["tip_ope"]);
		}
		if($data['asesor'])
		{
			$stmt->bindParam(":asesor",$data["asesor"]);
		}
		
		if($data['idInm'])
		{ //echo "ingresa a codDemanda ".$_GET['codDemanda'];
			$stmt->bindParam(":idInm",$data["idInm"]);
		}
		
		if($data['idInmmo']!=1)
		{
			$cond2 .="AND IdInmobiliaria = :idInmmo";
			
		}
		if($data['perfil']==3 or $data['perfil']==10)
		{
		
		}
		
		$stmt->bindParam(':idInmmo',$data["idInmmo"]);
		$stmt->execute();
		$listnegocios=array();
		while($row=$stmt->fetch())
		{	
			///boton para descartar demanda
			$descartarBtn=($row["RESULTADO"] == 1)?"":"<button class='btn btn-warning btn-sm' data-toggle='modal' data-target='#modal1-id'><span class='glyphicon fa fa-thumbs-o-down'></span></button>";
			
			$negociarBtn=($row["RESULTADO"] == 0)?"":"<button class='btn btn-success btn-sm'><span class='glyphicon glyphicon-ok'></span></button>";
			$editarBtn="<button class='btn btn-warning btn-sm' ><span class='fa fa-edit'></span></button>";
			$cambiosBtn="<button class='btn btn-info btn-sm' toggle='modal' data-target='#modal-id'><span class='glyphicon glyphicon-eye-open'></span></button>";
			$seguimientoBtn="<button class='btn btn-danger btn-sm'><span class='fa fa-sign-in'></span></button>";
			// $ofertabtn=($this->seguiDemandas($row["IdDemanda"])==1)?"<button class='btn btn-success btn-sm'><span class='glyphicon glyphicon-ok'></span></button>":"";
			$dataModal=$descartarBtn.$negociarBtn.$editarBtn.$cambiosBtn.$seguimientoBtn;
			//$dem=buscarDemandas($row["idInm"]);
			//echo $dem;
				$dem=293;
			$listnegocios[]=array(
								"idInm"     	=> $row["idInm"],
								"destinacion"  	=> ucwords(strtolower(getCampo('destinacion',"where IdDestinacion=".$row["IdDestinacion"],'Nombre'))),
								"gestion"      	=> ucwords(strtolower(getCampo('gestioncomer',"where IdGestion=".$row["IdGestion"],'NombresGestion'))),
								"tipoInmueble"  => ucwords(strtolower(getCampo('tipoinmuebles',"where idTipoInmueble=".$row["IdTpInm"],'Descripcion'))),
								"ValorVenta" 	=> "$".number_format($row["ValorVenta"]),
								"ValorCanon"   	=> "$".number_format($row["ValorCanon"]),
								"politica"	    => $row["politica_comp"],
								"promotor"	   	=> ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$row["IdPromotor"]."'",'concat(Nombres," ",apellidos)'))),
								"dem"	   		=> "$dem",
								"btnModal"     	=> $dataModal
								
							);
		}//print_r($listnegocios)."ffff";
		return $listnegocios;
     } //fin function listDemandas()
	 
     public function observaDemandas($idDemanda)
     {
     	$connPDO= new Conexion();

     	$stmt=$connPDO->prepare("SELECT cons_log
								FROM    lg_ch_tb 
								WHERE tipo_log = 1 
  								AND client_g = :idDemanda 
								ORDER BY cons_log DESC ");

     	$stmt->bindParam("idDemanda",$idDemanda);
     	$stmt->execute();
     	$seguimiento=$stmt->rowCount();
     	if($seguimiento>0)
     	{
     		return 1;
     	}
     }
     public function saveTracingDemandas($sDemandas,$idSeguimiento)
     {
     	$connPDO= new Conexion();
     	try {
     		$stmt=$connPDO->prepare("INSERT INTO SeguimientoDemandaNvo
												(idSeguimiento,
												 idDemanda,tseguimiento,
												 observacion,resseg,
												 frec,horarec,
												 agendar,agendarc,
												 mailcop) 
						     		 VALUES 	(:idSeguimiento,
												 :idDemanda,:tseguimiento,
												 :observacion,:resseg,
												 :frec,:horarec,
												 :agendar,:agendarc,
												 :mailcop)");
     		if($stmt->execute(array(
						':idSeguimiento' => $idSeguimiento,
						':idDemanda'     => $sDemandas['idDemanda'],    
						':tseguimiento'  => $sDemandas['tseguimiento'], 
						':observacion'   => $sDemandas['descripcion'],  
						':resseg'        => $sDemandas['resseg'],       
						':frec'          => $sDemandas['frec'],         
						':horarec'       => $sDemandas['hora_ini'],      
						':agendar'       => $sDemandas['agendar'],      
						':agendarc'      => $sDemandas['agendarc'],
						':mailcop'       => $sDemandas['mailcop'],
					)))
					{
						return 1;
					}   
			if($sDemandas['cerrard']==1)
			{
				$this->closeDemandas($sDemandas['idDemanda']);
			}
     	}
     	catch (PDOException $e) 
		{
    		print $e->getMessage ();
     	}

     }
     public function closeDemandas($cDemandas)
     {
     	$connPDO= new Conexion();
     	try
     	{
     		$stmt=$connPDO->prepare("UPDATE demandainmuebles 
     				 			 SET Estado 	 = 1
     				             WHERE IdDemanda = :IdDemanda");
   			$stmt->bindParam(":IdDemanda",$cDemandas);
   			

   			if($stmt->execute())
   			{
   				return 1;
   			}

     	}
     	catch (PDOException $e) {
    		print $e->getMessage ();
     	}

     	
     }
     public function seguimientoDemandas($seDemandas)
     {
     	$connPDO= new Conexion();

     	try
     	{
     		$stmt=$connPDO->prepare("SELECT  idSeguimiento,										 				 
											idDemanda,
     										 tseguimiento,
     										 observacion,
     										 resseg,frec,
     										 horarec,agendar,
     										 agendarc,mailcop
									 FROM SeguimientoDemandaNvo
									 WHERE  idDemanda=:idDemanda
									 order by idSeguimiento desc");
			$stmt->bindParam(":idDemanda",$seDemandas);
   			$stmt->execute();
   			$datas=array();
   			while ($row=$stmt->fetch()) {
   				$nomTseguimiento=getCampo('parametros',"where id_param=20 and conse_param=".$row['tseguimiento'],'desc_param');
   				$reSeguimiento=getCampo('parametros',"where id_param=10 and conse_param=".$row['resseg'],'desc_param');
   				$copyCliente=getCampo('parametros',"where id_param=3 and conse_param=".$row['agendarc'],'desc_param');
   				$fechaRec=($row['frec'] != '0000-00-00')?$row['frec']." <br>". $row['horarec']:"NO";

   				
   				$datas[]=array(
   					'tiposeguimiento'=>$nomTseguimiento,
   					'observacion'	 =>$row["observacion"],
   					'resultado'		 =>$reSeguimiento,
   					'recordatorio'   =>$fechaRec,
   					'copiarCliente'  =>$copyCliente
   				);
   			}
   			return $datas;
   			// echo $seDemandas;
     	}
     	catch (PDOException $e) {
    		print $e->getMessage ();
     	}
     }
     public function logs_CruceDeNegocios($logsDemandas)
     {
     	$connPDO= new Conexion();

     	try {
     		$stmt=$connPDO->prepare("SELECT cons_log,
 										 	tipo_log,
 										 	campo_g,
 										 	esta_ini_g,
 										 	esta_fin_g,
 										 	fecha_g,
 										 	hora_g,
 										 	usuario_g,
 										 	ip_g,
 										 	client_g,
 										 	campo_usu_g,
 										 	param_g 
									FROM    lg_ch_tb 
									WHERE tipo_log = 1 
  									AND client_g = :idDemanda 
									ORDER BY cons_log DESC ");
     		
     		$stmt->bindParam(":idDemanda",$logsDemandas["idDemanda"]);

     		$stmt->execute();
     		$data=array();
     		while ($row=$stmt->fetch()) {
     			$campo        = $row['campo_g'];
				
				
				$estado_ini   = $row['esta_ini_g'];
				$estado_fin   = $row['esta_fin_g'];
				$fecha        = $row['fecha_g'];
				$hora         = $row['hora_g'];
				$usuario      = $row['usuario_g'];
        		$ip_g         = $row['ip_g'];
        		$campo_usu_g  = $row['campo_usu_g'];
        		$param_g      = $row['param_g'];
        		$nomUsuario=ucwords(strtolower(getCampo('usuarios',
        			"where Id_Usuarios='$usuario'",
        			'concat(Nombres," ",apellidos)')));
        		$nomTabla=ucwords(strtolower(getCampo('parametros',
        			"WHERE id_param=17 and conse_param=1",
        			'desc_param')));
        		if($campo == "grupo")
        		{
        		$dataIni=getCampo('parametros',
        			"WHERE id_param=3 and conse_param='".$estado_ini."'",
        			'desc_param'); 
    			$dataFin=getCampo('parametros',
    				"WHERE id_param=3 and conse_param='".$estado_fin."'",
    				'desc_param');
        		}
        		elseif($campo =="IdPromotor")
        		{
        			$dataIni=getCampo('usuarios',
        			"where Id_Usuarios='$estado_ini'",
        			'concat(Nombres," ",apellidos)');

        			$dataFin=getCampo('usuarios',
        			"where Id_Usuarios='$estado_fin'",
        			'concat(Nombres," ",apellidos)');
        		}
        		elseif($campo == "estado_dem")
        		{
        			$dataIni = getCampo('parametros',
        				"WHERE id_param=16 and conse_param='".$estado_ini."'",
        				'desc_param'); 
    				$dataFin = getCampo('parametros',
    					"WHERE id_param=16 and conse_param='".$estado_fin."'",
    					'desc_param'); 
        		}
        		elseif($campo == "IdTipoInm")
        		{
        			$dataIni=getCampo('tipoinmuebles',
        				"WHERE idTipoInmueble='".$estado_ini."'"
        				,'CONCAT(UPPER(LEFT(Descripcion, 1)), LOWER(SUBSTRING(Descripcion, 2)))');
        			$dataFin=getCampo('tipoinmuebles',
        				"WHERE idTipoInmueble='".$estado_fin."'"
        				,'CONCAT(UPPER(LEFT(Descripcion, 1)), LOWER(SUBSTRING(Descripcion, 2)))');
        		}
        		elseif($campo == "IdDestinacion")
        		{
        			$dataIni=getCampo('destinacion',
        				"WHERE Iddestinacion='".$estado_ini."'"
        				,'CONCAT(UPPER(LEFT(Nombre, 1)), LOWER(SUBSTRING(Nombre, 2)))');
        			$dataFin=getCampo('destinacion',
        				"WHERE Iddestinacion='".$estado_fin."'"
        				,'CONCAT(UPPER(LEFT(Nombre, 1)), LOWER(SUBSTRING(Nombre, 2)))');
        		}
        		elseif($campo=='IdGestion')
        		{
        			$dataIni=getCampo('gestioncomer',
        				"WHERE IdGestion='".$estado_ini."'",
        				'CONCAT(UPPER(LEFT(NombresGestion, 1)), LOWER(SUBSTRING(NombresGestion, 2)))');
        			
        			$dataFin=getCampo('gestioncomer',"WHERE IdGestion='".$estado_fin."'"
        				,'CONCAT(UPPER(LEFT(NombresGestion, 1)), LOWER(SUBSTRING(NombresGestion, 2)))');
        		}
        		elseif($campo=='ValInicial' or $campo=='ValFinal'
        		or $campo=='AreaInicial' or $campo=='AreaFin')
        		{
        			$dataIni=number_format($estado_ini);
        			$dataFin=number_format($estado_fin);
        		}
        		elseif($campo=='barrio')
        		{
        			$dataIni=getCampo('barrios',"WHERE idBarrios='".$estado_ini."'",
        				'CONCAT(UPPER(LEFT(NombreB, 1)), LOWER(SUBSTRING(NombreB, 2)))');
        			$dataFin=getCampo('barrios',
        				"WHERE idBarrios='".$estado_fin."'",
        				'CONCAT(UPPER(LEFT(NombreB, 1)), LOWER(SUBSTRING(NombreB, 2)))');
        		}
        		else
        		{
        			$dataIni=$estado_ini;
					$dataFin=$estado_fin;
        		}
     			$data[]=array(
						"nomCapo"    =>$campo_usu_g,
						"estInicial" =>$dataIni,
						"estFinal"   =>$dataFin,
						"fecha"      =>$fecha."-".$hora,
						"ipuser"     =>$ip_g,
						"usuario"    =>$nomUsuario
     				);
     		}
     		return $data;
     		
     	} catch (PDOException $e) {
    		print $e->getMessage ();
     	}
     }
     public function editaDemandaNvo($inmob,$FechaD,$IdPromotor,$numced,$calle,$alacalle,$carrera,$alacarrera,$Ciudad,$barrio,$idgestion,$IdDestinacion,$IdTipoInm,$AreaIni,$AreaFin,$ValInicial,$ValFinal,$mgrupo,$descripcion,$fsis,$IdDemanda,$Estado,$barrios,$localidad,$estrato,$FechaLimite)
	{
 		$w_conexion = new MySQL(); 
		$f_sis=date('Y-m-d');
		$h_sis=date('H:i:s');
		$usuario=$_SESSION['Id_Usuarios'];
		$ip=$_SERVER['REMOTE_ADDR'];

		
		//consulta de valores originales para guardar bitácora 2014-12-16
		$sql_orig="SELECT IdDemanda,IdPromotor,FechaLimite,Calle,
					AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
					IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
					Descripcion,Estado,IdBarrio,grupo_dem,estrato_dem,localidad
					FROM demandainmuebles
					where IdDemanda='$IdDemanda'";
		$res_or=$w_conexion->ResultSet($sql_orig);
		while($f_or=$w_conexion->FilaSiguienteArray($res_or))
		{
			$IdPromotor_or 		= $f_or['IdPromotor']; 
			$FechaLimite_or 	= $f_or['FechaLimite']; 
			$Calle_or 			= $f_or['Calle']; 
			$AlaCalle_or 		= $f_or['AlaCalle']; 
			$Carrera_or 		= $f_or['Carrera']; 
			$AlaCarrera_or 		= $f_or['AlaCarrera']; 
			$IdGestion_or 		= $f_or['IdGestion']; 
			$IdDestinacion_or 	= $f_or['IdDestinacion']; 
			$IdTipoInm_or 		= $f_or['IdTipoInm']; 
			$ValInicial_or 		= $f_or['ValInicial']; 
			$ValFinal_or 		= $f_or['ValFinal']; 
			$AreaInicial_or 	= $f_or['AreaInicial']; 
			$AreaFinal_or 		= $f_or['AreaFinal'];
			$Descripcion_or 	= $f_or['Descripcion']; 
			$Estado_or 			= $f_or['Estado']; 
			$estrato_dem_or 	= $f_or['estrato_dem'];
			$localidad_or 		= $f_or['localidad'];
			$IdBarrio_or 		= $f_or['IdBarrio']; 
			$grupo_dem_or 		= $f_or['grupo_dem'];
		}
		
		$sql="UPDATE demandainmuebles 
				SET		
				IdPromotor			='$IdPromotor',
				FechaLimite			='$FechaLimite',	
				CedulaIInt			='$numced',	
				Calle				='$calle',
		 		AlaCalle			='$alacalle',
				Carrera				='$carrera',
				AlaCarrera			='$alacarrera',
				IdGestion			='$idgestion',
				IdDestinacion		='$IdDestinacion',
		 		IdTipoInm			='$IdTipoInm',
				ValInicial			='$ValInicial',
				ValFinal			='$ValFinal',
				AreaInicial			='$AreaIni',
				AreaFinal			='$AreaFin',
		 		Descripcion			='$descripcion',
				IdCiudad			='$Ciudad',
				IdBarrio			='$barrio',
				estrato_dem			='$estrato',
				localidad			='$localidad',
		 		grupo_dem			= '$mgrupo',
				Estado				= '$Estado'	
				WHERE IdDemanda		= '$IdDemanda'";
			$res=$w_conexion->RecordSet($sql);
			if($res)
			{
				
				/*
				recibimos la cadena de barrios y los descomponemos para hacer un ciclo
				 */
				//echo $barrios."--";
				if($barrios)
				{	
					$barrios=substr($barrios,0,-1);
					$cad=explode(",",$barrios);

					for($i=0;$i<count($cad);$i++)
					{	
						list($bar1,$loc1)=explode("-",$cad[$i]);
						$conse_de=consecutivo('conse_de','demandas_barrios',0);
						$sql_bdm="REPLACE INTO demandas_barrios
						(conse_de,id_demanda_de,id_ciu_de,id_loc_de,id_barrio_de,estado_de)
						VALUES('$conse_de','$IdDemanda','$Ciudad','$loc1','".$bar1."','1')";
						echo "insert $sql_bdm <br>";
						$resbdm=$w_conexion->RecordSet($sql_bdm) or die("error ".mysql_error());
					}
				}

				///guardar historial
				if($localidad_or!=$localidad)
				{
					$cambio++;
					$nom_campo="Localidad";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Localidad"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$localidad,$localidad_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				///guardar historial
				if($IdPromotor_or!=$IdPromotor)
				{
					$cambio++;
					$nom_campo="IdPromotor";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Promotor"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$IdPromotor,$IdPromotor_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				///guardar historial
				if($estrato_dem_or!=$estrato)
				{
					$cambio++;
					$nom_campo="estrato";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="estrato"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$estrato,$estrato_dem_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($FechaLimite_or!=$FechaD)
				{
					$cambio++;
					$nom_campo="FechaLimite";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Fecha Limite"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$Fechalimite,$FechaLimite_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($Calle_or!=$calle)
				{
					$cambio++;
					$nom_campo="Calle";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Calle"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$calle,$Calle_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($AlaCalle_or!=$alacalle)
				{
					$cambio++;
					$nom_campo="AlaCalle";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="A la Calle"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$alacalle,$AlaCalle_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($Carrera_or!=$carrera)
				{
					$cambio++;
					$nom_campo="Carrera";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Carrera"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$carrera,$Carrera_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($AlaCarrera_or!=$alacarrera)
				{
					$cambio++;
					$nom_campo="AlaCarrera";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="A la Carrera"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$alacarrera,$AlaCarrera_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($IdGestion_or!=$idgestion)
				{
					$cambio++;
					$nom_campo="IdGestion";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Gestion"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$idgestion,$IdGestion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($IdDestinacion_or!=$IdDestinacion)
				{
					$cambio++;
					$nom_campo="IdDestinacion";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Destinacion"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$IdDestinacion,$IdDestinacion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($IdTipoInm_or!=$IdTipoInm)
				{
					$cambio++;
					$nom_campo="IdTipoInm";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Tipo de Inmueble"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$IdTipoInm,$IdTipoInm_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($ValInicial_or!=$ValInicial)
				{
					$cambio++;
					$nom_campo="ValInicial";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Valor Inicial"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$ValInicial,$ValInicial_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($ValFinal_or!=$ValFinal)
				{
					$cambio++;
					$nom_campo="ValFinal";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Valor Final"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$ValFinal,$ValFinal_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($AreaInicial_or!=$AreaIni)
				{
					$cambio++;
					$nom_campo="AreaInicial";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Area Inicial"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$AreaIni,$AreaInicial_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($AreaFinal_or!=$AreaFin)
				{
					$cambio++;
					$nom_campo="AreaFin";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Area Final"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$AreaFin,$AreaFinal_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($Descripcion_or!=$descripcion)
				{
					$cambio++;
					$nom_campo="Descripcion";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Descripcion"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$descripcion,$Descripcion_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($Estado_or!=$Estado)
				{
					$cambio++;
					$nom_campo="estado_dem";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Estado"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$Estado,$Estado_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
				if($IdBarrio_or!=$barrio)
				{
					if(is_numeric($barrio))
					{
						$cambio++;
						$nom_campo="barrio";
						$cons_log=consecutivo_log(1);
						$campo_usu_g="Barrio"; //nombre del campo en el formulario
						$param_g=0; //codigo de la tabla par?metros
						guarda_log_gral($cons_log,1,$nom_campo,$barrio,$IdBarrio_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
					}
				}
				if($grupo_dem_or!=$mgrupo)
				{
					$cambio++;
					$nom_campo="grupo";
					$cons_log=consecutivo_log(1);
					$campo_usu_g="Grupo"; //nombre del campo en el formulario
					$param_g=0; //codigo de la tabla par?metros
					guarda_log_gral($cons_log,1,$nom_campo,$mgrupo,$grupo_dem_or,$f_sis,$h_sis,$usuario,$ip,$IdDemanda,$campo_usu_g,$param_g);
				}
	
				echo $idDemanda;
	}
}
  
}
?>
