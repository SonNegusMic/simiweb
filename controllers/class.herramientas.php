<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

@include "../funciones/connPDO.php";

class Herramientas
{
    public function __construct($conn = "",$phpmailer)
    {
        $this->db = $conn;
        $this->phpmailer = $phpmailer;
    }

    private $id_usuario;
    public function infoListadoPropetarioEmail($get, $data)
    {

        $connPDO = new Conexion();

// $data['inmueble']='1-14058';
        // $data['IdInmobiliaria']=1;
        if (!empty($data['inmueble'])) {
            $cond .= " AND i.idInm = :inmueble";
        } else if (!empty($data['fecha_fin']) && !empty($data['fecha_2'])) {
            $cond .= " AND i.FConsignacion  BETWEEN :fecha_fin AND :fecha_2";
        }

        $aColumns = array("u.Nombres", "u.apellidos", "i.idInm", "u.Correo", "i.FConsignacion");

        $orderBy = ' ORDER BY ' . $aColumns[$get['order'][0]['column']] . ' ' . $get['order'][0]['dir'];

        $sWhere = "";

        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($get['search']) && $get["search"]["value"] != '') {
                if ($sWhere == "") {
                    $sWhere = " AND (";
                } else {
                    $sWhere .= " OR ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($get["search"]["value"]) . "%' ";
            }
        }

        $sLimit = " LIMIT 0,10 ";
        if (isset($get['start']) && $get['length'] != '-1') {
            $sLimit = " LIMIT " . intval($get['start']) . ", " .
            intval($get['length']);
        }

        if ($sWhere != "") {

            $sWhere .= $cond;
            $sWhere .= ")";
        } else {
            $sWhere .= $cond;
        }

        $idinmo = $_SESSION['IdInmmo'];

        $sql = "SELECT concat(u.Nombres,' ',u.apellidos) as Nombrepro,u.Correo,i.idInm,i.FConsignacion,RMtoCuadrado,RLaGuia,RVivaReal,RZonaProp ,PublicaDoomos,PublicaLocanto,PublicaOlx,RNcasa,Publicagpt,idInm,codinm
            FROM inmuebles i
            INNER JOIN usuarios u ON i.IdPropietario = u.Id_Usuarios
            WHERE i.IdPropietario>0
            AND i.IdInmobiliaria = $idinmo
            $sWhere
            GROUP BY i.idInm $orderBy $sLimit ";

            // echo "<pre>";
            // print_r($sql);
            // echo "</pre>";die;

        $stmt = $connPDO->prepare($sql);

        if (!empty($data['inmueble'])) {
            $stmt->bindParam(':inmueble', $data['inmueble']);
        } else if (!empty($data['fecha_fin']) && !empty($data['fecha_2'])) {
            $stmt->bindParam(':fecha_fin', $data['fecha_fin']);
            $stmt->bindParam(':fecha_2', $data['fecha_2']);
        }

        if ($stmt->execute()) {

            $dato = array();
            $connPDO->exec('chartset="utf-8"');
            $cantidad = $stmt->rowCount();
            if ($cantidad > 0) {
                $cods = array();
                while ($row = $stmt->fetch()) {
                    if (trim($row['Correo']) !== "" && $this->validarEmail(trim($row['Correo'])) === true || $this->validarVariosEmails(trim($row['Correo']))) {
                        // $btnsend  = "<button data-toggle='tooltip' data-placement='top' title='Enviar Correo' id='" . utf8_encode(trim($row['nit'])) . "' class='sendemail btn btn-success btn-xs' data-nroestado='" . utf8_encode(trim($row['Cedula'])) . "' data-email='" . utf8_encode(trim($row['correo'])) . "'><i class='fa fa-paper-plane-o' aria-hidden='true'></i></button>";
                        $checkbox = '<input type="checkbox"  name="sendMail[]" data-email="'.trim($row['Correo']).'"  id="send_' . $row["idInm"] . '" data-value="' . $row["idInm"] . '" value="'.$row['idInm'].'"  class="checkPort cheacbox-ui sendMail" /><label for="send_' . $row["idInm"] . '" class="checkProp"><span class="ui-xs"></span></label>';
                    } else {
                        // $btnsend  = "";
                        $checkbox = "";
                    }

                    $dato[] = array(
                        "Nombrepro"     => ucwords(strtolower(utf8_encode($row['Nombrepro']))),
                        "idInm"         => $row['idInm'],
                        "correopro"     => utf8_encode($row['Correo']),
                        "FConsignacion" => $row['FConsignacion'],
                        'comentario'    => '<textarea style="resize: none" name="comentario" id="coment_' . $row['idInm'] . '" class="form-control"></textarea>',
                        'enviar' => $checkbox
                    );

                    // $dato[] = array("codigos" => Herramientas::datosInmuebleCodigosPublicaciones($data['IdInmobiliaria'], $row['codinm'], ''));
                }
                $Ssql = "SELECT concat(u.Nombres,' ',u.apellidos) as Nombrepro
                    FROM inmuebles i
                    INNER JOIN usuarios u ON i.IdPropietario = u.Id_Usuarios
                    WHERE i.IdPropietario>0
                    AND i.IdInmobiliaria = $idinmo
                    $sWhere
                    GROUP BY i.idInm";

                $stmtcount = $connPDO->prepare($Ssql);
                if (!empty($data['inmueble'])) {
                    $stmtcount->bindParam(':inmueble', $data['inmueble']);
                } else if (!empty($data['fecha_fin']) && !empty($data['fecha_2'])) {
                    $stmtcount->bindParam(':fecha_fin', $data['fecha_fin']);
                    $stmtcount->bindParam(':fecha_2', $data['fecha_2']);
                }
                $stmtcount->execute();
                $rResultTotal         = $stmtcount->fetchAll();
                $iTotal               = $stmtcount->rowCount();
                $output = array(
                    "sEcho"                => intval($data['sEcho']),
                    "iTotalRecords"        => $iTotal,
                    "iTotalDisplayRecords" => $iTotal, //$iFilteredTotal[0],
                    "aaData"               => $dato,
                );
                return $output; // array_push($dato,$cods);
                //return $response=array("datos"=>array_push($dato,$cods),"status"=>0,"error"=>0);
            } else {
                return array("msn" => 'Nhay Resultados', "status" => 1, "error" => 0);
            }

        } else {
            return array("msn" => $stmt->errorInfo(), "status" => 0, "error" => 1); //print_r($stmt->errorInfo());
        }
    }
    public function infoListadoClientesEmail($data)
    {

        $connPDO = new Conexion();

        if ($data['IdInmobiliaria'] == 279 || $data['IdInmobiliaria'] == 2790) {
            $cond .= " WHERE c.Inmobiliaria in (279,2790)";
        } else {
            $cond .= " WHERE c.Inmobiliaria='" . $_SESSION['IdInmmo'] . "'";
        }
        if (!empty($data['inmueble'])) {
            $cond .= " AND c.id_inmueble = :inmueble";
        } else if (!empty($data['fecha_fin']) && !empty($data['fecha_2'])) {
            $cond .= " AND c.fechacita  BETWEEN :fecha_fin AND :fecha_2";
        }

        $stmt = $connPDO->prepare("SELECT concat(u.Nombres,' ',u.apellidos) as Nombrepro, u.Correo,c.id_inmueble,c.fechacita,idInm,u.Id_Usuarios,i.codinm,i.IdInmobiliaria
            FROM cita c
            INNER JOIN inmuebles i ON c.id_inmueble = i.idInm
            INNER JOIN usuarios u ON u.Id_Usuarios=i.IdPropietario
            $cond
             GROUP BY c.id_inmueble
             ORDER by c.fechacita DESC");

        if (!empty($data['idInm'])) {
            $stmt->bindParam(':inmueble', $data['idInm']);
        } else if (!empty($data['fecha_fin']) && !empty($data['fecha_2'])) {
            $stmt->bindParam(':fecha_fin', $data['fecha_fin']);
            $stmt->bindParam(':fecha_2', $data['fecha_2']);
        }

        if ($stmt->execute()) {

            $dato = array();
            $connPDO->exec('chartset="utf-8"');
            $cantidad = $stmt->rowCount();
            if ($cantidad > 0) {
                while ($row = $stmt->fetch()) {
                    //<input type="checkbox"  name="idportal"  id="'.$conse_pet.'" data-value="'.$conse_pet.'" value="test" '.$checked.' class="checkPort cheacbox-ui" /><label for="'.$conse_pet.'"><span class="ui-xs"></span></label>
                    $dato[] = array(
                        "correopro" => utf8_encode($row['Correo']),
                        "Nombrepro" => utf8_encode(ucwords(strtolower($row['Nombrepro']))),
                        "idInm"     => $row['idInm'],
                        "fechacita" => $row['fechacita'],
                        //"MostrarCl" => '<label for="'.$row["Id_Usuarios"].'"><span class="ui"></span></label><input type="checkbox" name="cerrarsegui" id="'.$row["Id_Usuarios"].'">',
                        "MostrarCl" => '<input type="checkbox"  name="mostrarCl"  id="cli_' . $row["Id_Usuarios"] . '" data-value="' . $row["Id_Usuarios"] . '" value="test"  class="checkPort cheacbox-ui mostrarCl" /><label for="cli_' . $row["Id_Usuarios"] . '" class="checkProp"><span class="ui-xs"></span></label>',
                        "SendMail"  => '<input type="checkbox"  name="sendMail"  id="send_' . $row["Id_Usuarios"] . '" data-value="' . $row["Id_Usuarios"] . '" value="test"  class="checkPort cheacbox-ui sendMail" /><label for="send_' . $row["Id_Usuarios"] . '" class="checkProp"><span class="ui-xs"></span></label>',
                        "Preview"   => '<a class="previewData btn btn-spartan btn-sm" data-inmo="' . $row["IdInmobiliaria"] . '" data-inmu="' . $row["codinm"] . '"><i class="fa fa-search" aria-hidden="true"></i></a>',
                    );
                }
                return $dato; //$response=array("datos"=>$dato,"status"=>0,"error"=>0);
            } else {
                return array("msn" => 'Este inmueble no tiene asociado ningun propietario', "status" => 1, "error" => 0);
            }

        } else {
            return array("msn" => $stmt->errorInfo(), "status" => 0, "error" => 1); //print_r($stmt->errorInfo());
        }
    }
    public function datosInmuebleCodigosPublicaciones($IdInmobiliaria, $codinm, $codPortal = '')
    {
        $connPDO = new Conexion();

        $idInm = $IdInmobiliaria . "-" . $codinm;
        $cond  = '';
        if ($codPortal > 0) {
            $cond = ' and portal_dtp=:codPortal';
        }

        $stmt = $connPDO->prepare("SELECT idinm_dtp,inmob_dtp,portal_dtp,
                cod_dtp,urlpublica_dtp
                from detportalesinmueble
                where idinm_dtp=:codinm
                and inmob_dtp= :idinmo
                $cond");
        if ($codPortal > 0) {
            $stmt->bindParam(":codPortal", $codPortal);
        }
        $stmt->bindParam(":idinmo", $IdInmobiliaria);
        $stmt->bindParam(":codinm", $codinm);
        if ($stmt->execute()) {
            $dato = array();
            while ($row = $stmt->fetch()) {

                $dato[] = array(
                    "portal_dtp" => utf8_encode(ucwords(strtolower(getCampo('Portales', "where IdPortal=" . $row['portal_dtp'], 'NomPortal')))),
                    "cod_dtp"    => $row['cod_dtp'],
                );
            }
            return $dato;
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function savecloneInmueble($data)
    {
        $connPDO = new Conexion();
        // $data['IdInmobiliaria']=1;
        // $data['codinm']=14058;
        $idInm         = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $fechaserv     = date('Y-m-d');
        $usu_creacion  = $_SESSION['Id_Usuarios'];
        $h_creacion    = date('Y-m-d');
        $ip            = $_SERVER['REMOTE_ADDR'];
        $nvoInm        = $this->consecInmuNew($data['IdInmobiliaria']);
        $idInmueblenvo = $data['IdInmobiliaria'] . "-" . $nvoInm;
        $stmt          = $connPDO->prepare("INSERT INTO inmuebles
                                    (idInm, codinm,IdInmobiliaria,  RefInmueble, IdGestion, IdTpInm,IdBarrio,
                                    Direccion, NoGaraje,NoDeposito,ValorVenta,ValorCanon,Administracion, ValorMinimo, Estrato,
                                    NomAdministrador, DirAdmon, TelAdmon, ValorHipoteca, AreaConstruida, AreaLote, AreaTerraza, Frente,                                     Fondo, NoPlantas, Ubicacion, EdadInmueble,Estado, Destinacion, IdUbicacion, chip, NoMatricula,
                                    CedulaCatastral, Norma, FormadeVisita, NoAlcobas, NoLineasTelefonicas, Caracteristicas,
                                    IdPropietario, idCaptador, IdPromotor, idPerito,idProcedencia,idEstadoinmueble,HoraVisitas,avisara,
                                    FConsignacion,AvaluoCatastral,fecharotacion,fechaestado,Propietario,Dir_Pro,TelParticular,
                                    TelOficina,TelCelular,Email,Cedula,IdCaptacion,Ref2,ConceptoEmp,Carrera,Calle,Telefonoinm,
                                    ComiVenta,ComiArren,Banos,DacionEnPago,ConMuebles,M2Arre,Edificio,IdDestinacion,NumLlaves,
                                    NumCasillero,M2Vent,FechaVenta,FechaArriendo,InmNuevo,Exclusivo,IdproArre,ProCorreo,TelPorteria,
                                    ERotulo,IdTipoCont,ObsVisita,FechaEnvio,FechaRetiro,RetiradoPor,RetiConcepto,UpdateWeb,NuevoWeb,
                                    FNaci,Aficiones,Club,Estadocivil,Actividad,Refer3,CarPlano,CalPlano,DNE,RefInmueble2,RefInmueble3,
                                    RefInmueble4,RefInmueble5,Direc,Nvp,Lvp,Bvp,Lbvp,Ovp,Nvs,Lvs,Bvs,Lbvs,Nn,Ovs,NroContrato,
                                    Apoderado,DirApoderado,
                                    TelApoderado,emailApoderado,cedulapoderado,celularapoderado,observacionvisita,linkvideo,
                                    descripcionlarga,tipo_contrato,fingreso,fmodificacion,num_visita,conceptohipoteca,amoblado,
                                    latitud,longitud,gradosla,minla,segla,gradoslon,minlon,seglon,escritura_inmu,restricciones,usu_creacion,f_creacion,h_creacion,usu_crea)
                                    SELECT
                                    '$idInmueblenvo','$nvoInm', IdInmobiliaria,   '$idInmueblenvo', IdGestion,   IdTpInm,IdBarrio,
                                    Direccion, NoGaraje,NoDeposito,ValorVenta,ValorCanon,Administracion, ValorMinimo, Estrato,
                                    NomAdministrador, DirAdmon, TelAdmon, ValorHipoteca, AreaConstruida, AreaLote, AreaTerraza, Frente,                                     Fondo, NoPlantas, Ubicacion, EdadInmueble,Estado, Destinacion, IdUbicacion, chip, NoMatricula,
                                    CedulaCatastral, Norma, FormadeVisita, NoAlcobas, NoLineasTelefonicas, Caracteristicas,
                                    IdPropietario, idCaptador, IdPromotor, idPerito,idProcedencia,idEstadoinmueble,HoraVisitas,avisara,
                                    FConsignacion,AvaluoCatastral,fecharotacion,fechaestado,Propietario,Dir_Pro,TelParticular,
                                    TelOficina,TelCelular,Email,Cedula,IdCaptacion,Ref2,ConceptoEmp,Carrera,Calle,Telefonoinm,
                                    ComiVenta,ComiArren,Banos,DacionEnPago,ConMuebles,M2Arre,Edificio,IdDestinacion,NumLlaves,
                                    NumCasillero,M2Vent,FechaVenta,FechaArriendo,InmNuevo,Exclusivo,IdproArre,ProCorreo,TelPorteria,
                                    ERotulo,IdTipoCont,ObsVisita,FechaEnvio,FechaRetiro,RetiradoPor,RetiConcepto,UpdateWeb,NuevoWeb,
                                    FNaci,Aficiones,Club,Estadocivil,Actividad,Refer3,CarPlano,CalPlano,DNE,RefInmueble2,RefInmueble3,
                                    RefInmueble4,RefInmueble5,Direc,Nvp,Lvp,Bvp,Lbvp,Ovp,Nvs,Lvs,Bvs,Lbvs,Nn,Ovs,'$idInmueblenvo',
                                    Apoderado,DirApoderado,
                                    TelApoderado,emailApoderado,cedulapoderado,celularapoderado,observacionvisita,linkvideo,
                                    descripcionlarga,tipo_contrato,fingreso,fmodificacion,num_visita,conceptohipoteca,amoblado,
                                    latitud,longitud,gradosla,minla,segla,gradoslon,minlon,seglon,escritura_inmu,restricciones,usu_creacion,f_creacion,h_creacion,usu_crea
                                    FROM inmuebles
                                    WHERE idInm=:id_inmueble");

        if ($stmt->execute(array(
            ":id_inmueble" => $idInm,
        ))) {

            Inmuebles::savecloneInmuebleNvo($data, $idInm, $nvoInm, $idInmueblenvo);
            Inmuebles::savecloneInmuebleDetalle($data, $idInm, $nvoInm, $idInmueblenvo);
            if (isset($data["fotos"])) {

                Inmuebles::savecloneInmuebleFotos($data, $idInm, $nvoInm, $idInmueblenvo);
                Inmuebles::savecloneInmuebleFotosNvo($data, $idInm, $nvoInm, $idInmueblenvo);

            }
            return $idInmueblenvo;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function savecloneInmuebleNvoOld($idInm)
    {
        $connPDO = new Conexion();

        $fechaserv    = date('Y-m-d');
        $usu_creacion = $_SESSION['Id_Usuarios'];
        $h_creacion   = date('Y-m-d');
        $ip           = $_SERVER['REMOTE_ADDR'];
        $banos        = Herramientas::trae_caracBanos(16, $idInm, '', 'Cantidad', 'baño');
        $garaje       = Herramientas::trae_caracBanos(37, $idInm, '', 'Cantidad', 'parqueadero');
        $alcobas      = Herramientas::trae_caracBanos(15, $idInm, '', 'Cantidad', 'alcoba');

        $stmt = $connPDO->prepare("INSERT INTO inmnvo
                                    (banos,alcobas,garaje,idInm,codinm,IdInmobiliaria,IdGestion,IdTpInm,IdBarrio,Direccion,DirMetro,Telefonoinm,ValorVenta,ValorCanon,AdmonIncluida,Administracion,ValorIva,admondto,Estrato,AreaConstruida,AreaLote,EdadInmueble,Destinacion,chip,NoMatricula,CedulaCatastral,idProcedencia,idEstadoinmueble,FConsignacion,AvaluoCatastral,FechaAvaluoComercial,Carrera,Calle,ComiVenta,ComiArren,IdDestinacion,descripcionlarga,descripcionMetro,fingreso,fmodificacion,latitud,longitud,escritura_inmu,restricciones,obserinterna,ValComiVenta,ValComiArr,f_creacion,h_creacion,usu_creacion,permite_geo,FechaAvaluoCatastral,ValorAvaluoComercial,migrado,politica_comp,usu_crea,codinterno,flagm2,leido,sede,uso)
                                    SELECT
                                    '$banos','$alcobas','$garaje',idInm,codinm, IdInmobiliaria,   IdGestion,   IdTpInm,IdBarrio,Direccion,DirMetro,Telefonoinm,ValorVenta,ValorCanon,AdmonIncluida,Administracion,ValorIva,admondto,Estrato,AreaConstruida,AreaLote,EdadInmueble,Destinacion,chip,NoMatricula,CedulaCatastral,idProcedencia,idEstadoinmueble,FConsignacion,AvaluoCatastral,FechaAvaluoComercial,Carrera,Calle,ComiVenta,ComiArren,IdDestinacion,descripcionlarga,descripcionMetro,fingreso,fmodificacion,latitud,longitud,escritura_inmu,restricciones,obserinterna,ValComiVenta,ValComiArr,f_creacion,h_creacion,usu_creacion,permite_geo,FechaAvaluoCatastral,ValorAvaluoComercial,migrado,politica_comp,usu_crea,codinterno,flagm2,leido,sede,uso
                                    FROM inmuebles
                                    WHERE idInm=:id_inmueble");

        if ($stmt->execute(array(
            ":id_inmueble" => $idInm,
        ))) {

            return $idInm;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function enviarCorreoPropietarios($data){
        $items = explode('?',$data['items']);
        $inmus = "";
        $sendCorreos = 0;
        for ($i=0; $i <= count($items) ; $i++) { 
            $inmucomentemail = explode(',',$items[$i]);
            $inmueble = $inmucomentemail[0];
            $comentario = $inmucomentemail[1];
            $correo = $inmucomentemail[2];
            $logo = str_replace('../','',getCampo('clientessimi', ' where IdInmobiliaria = ' . $_SESSION['IdInmmo'],'logo'));
            $NombreInm = ucwords(strtolower(getCampo('clientessimi', ' where IdInmobiliaria = ' . $_SESSION['IdInmmo'],'Nombre')));
            $Ficha = getCampo('clientessimi', ' where IdInmobiliaria = ' . $_SESSION['IdInmmo'],'fichatec');
            $logoinmo="http://www.simiinmobiliarias.com/".$logo;
            $email_subject="Ficha de creación de su inmueble en internet: ".$inmueble;
            $cuerpo='
            <html> 
            <head> 
            <title></title> 
            </head>
            <body>
            <font face="Arial, Helvetica, sans-serif">
            <table width="795" border="1" bordercolor="#05B5F0" cellpadding=2>
            <tr>
              <td>
                    <table border="0" align="center">
                    <tr>
                            <td colspan="2" align="center"><img src="'.$logoinmo.'"></td>
                        </tr>
                        <tr>
                        <td></td>
                        </tr>
                        <tr>
                                <td colspan="2">
                                '.$NombreInm.'., presenta a usted a continuaci&oacute;n la ficha de su inmueble en internet
                                </td>    
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>   
                        <tr>
                            <td colspan="2" align="justify">'.$comentario.'<br>
                            <a  target="_blank" href="http://simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/'.$Ficha.'?reg='.$inmueble.'">Click para ver su inmueble</a>
                            </td>
                        </tr>
                        <tr>
                                <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="2">Cordialmente,</td>
                        </tr>
                        <tr>
                            <td colspan="2">EL EQUIPO COMERCIAL  DE '.$NombreInm.'</td>
                        </tr>           
                        <tr>
                            <td colspan="2"></td>
                        </tr>
                    </table>
              </td>
            </tr>
            </table>
            </font>
            </body>
            </html>';
            $mail = $this->phpmailer;
            $mail->IsSMTP();//Configuración de que el envío será por SMTP--(Se añade en el php.ini extensión de envío de Email Smtp Localhost)
            $mail->SMTPAuth = true;//Autenticación en el servidor requerida
            $mail->SMTPSecure = "tls";//Tipo de seguridad Bienco Server
/*      __________________________________________________________*/
            $mail->Host = "smtp.office365.com"; //Servidor de Correo Bienco
            $mail->Port = 587;
            $mail->Username = "noreply@simiinmobiliarias.com";//usuario perteneciente al servidor
            $mail->Password = "Desarrollo.2015";
        /*      __________________________________________________________*/
            $mail->From = "noreply@simiinmobiliarias.com";
            $mail->FromName = "Agente Virtual Simi";
            // $mail->AddBCC($correo,$correo);
            // $mail->AddBCC('coordinadorsoporte@tae-ldta.com','coordinadorsoporte@tae-ldta.com');
            $mail->AddBCC('desarrolloweb4@tae-ltda.com','desarrolloweb4@tae-ltda.com');
            // if($_SESSION['IdInmmo']==172)
            // {
            //     $mail->AddBCC("chicalainm.comercialarriendos@hotmail.com","Director Comercial");
            // }
            $mail->Subject = $email_subject;
            $mail->MsgHTML($cuerpo);
            //$mail->AddAddress("demrdx@hotmail.com", "ALVARO ESCOBAR PRUEBA ENVÍO");
            $mail->SMTPDebug=0;
            $mail->IsHTML(true);
            if($mail->send())
            {
                $sendCorreos++;
            }
            
        }
        return $sendCorreos;
    }
    public function trae_carac($grupo, $inmueble, $campo, $especifica, $muestra = "")
    {
        global $w_conexion;
        $cond = "";
        if ($especifica) {
            $cond .= " and Descripcion like '%$especifica%'";
        }
        $sqlconsulta = "SELECT  $campo as aaa
             FROM detalleinmueble
             INNER JOIN maestrodecaracteristicas
             ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica
             AND  maestrodecaracteristicas.IdGrupo in ($grupo)
             INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble'
             AND detalleinmueble.idinmueble = inmuebles.idinm
             $cond";
        if ($muestra == 1) {
            echo "<br>" . $sqlconsulta . "<br>";
        }
        $res    = $w_conexion->ResultSet($sqlconsulta) or die("error " . mysql_error());
        $existe = $w_conexion->FilasAfectadas($res);
        //echo $existe."ff";
        while ($fff = $w_conexion->FilaSiguienteArray($res)) {
            //echo "hola en cliclo";
            $idcaracteristica = $fff["aaa"];
        }
        //echo ">> $idcaracteristica --- $campo <br>";
        return $idcaracteristica;
        $w_conexion->CerrarConexion();
    }
    public function evalua_carac($grupo, $inmueble, $campo, $especifica, $muestra = "")
    {
        global $w_conexion;
        $cond = "";
        if ($especifica) {
            $cond .= " and Descripcion like '%$especifica%'";
        }
        $sqlconsulta = "SELECT  $campo as aaa
             FROM detalleinmueble
             INNER JOIN maestrodecaracteristicas
             ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica
             AND  maestrodecaracteristicas.IdGrupo in ($grupo)
             INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble'
             AND detalleinmueble.idinmueble = inmuebles.idinm
             $cond";
        if ($muestra == 1) {
            echo $sqlconsulta;
        }
        $res    = $w_conexion->ResultSet($sqlconsulta) or die("error " . mysql_error());
        $existe = $w_conexion->FilasAfectadas($res);
        //echo $existe."ff";

        if ($existe > 0) {
            $idcaracteristica = "1";
        } else {
            $idcaracteristica = "0";
        }
//    echo ">> $idcaracteristica --- $campo <br>";
        return $idcaracteristica;
        $w_conexion->CerrarConexion();
    }
    public function trae_caracBanos($grupo, $inmueble, $campo, $especifica, $muestra = "")
    {
        global $w_conexion;
        $cond = "";
        if ($especifica) {
            $cond .= " and Descripcion like '%$especifica%'";
        }
        $sqlconsulta = "SELECT  sum(Cantidad) as aaa
             FROM detalleinmueble
             INNER JOIN maestrodecaracteristicas
             ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica
             AND  maestrodecaracteristicas.IdGrupo in ($grupo)
             INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble'
             AND detalleinmueble.idinmueble = inmuebles.idinm
             $cond";
        if ($muestra == 1) {
            echo "<br>" . $sqlconsulta . "<br>";
        }
        $res    = $w_conexion->ResultSet($sqlconsulta) or die("error " . mysql_error());
        $existe = $w_conexion->FilasAfectadas($res);
        //echo $existe."ff";
        while ($fff = $w_conexion->FilaSiguienteArray($res)) {
            //echo "hola en cliclo";
            $idcaracteristica = $fff["aaa"];
        }
        //echo ">> $idcaracteristica --- $campo <br>";
        return $idcaracteristica;
    }
    private function validarEmail($str)
    {
        if (!filter_var($str, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }
    private function validarVariosEmails($str)
    {
        $emailRegEx = '/^(([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+)(([\s]*[;,\/]+[\s]*(([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+))*)$/';

        if (preg_match($emailRegEx, $str)) {
            return true;
        }

        return false;
    }

}
