<?php

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');



class elpais
{
    public function __construct($conn = "",$connPDO)
    {
        $this->db = $conn;
        $this->pdo= $connPDO;
    }

    private $id_usuario;

    public function infoInmobiliaria($codInmo)
    {                                  

        if (($consulta =$this->db->prepare(" select
        IdInmobiliaria,Nombre,Direccion,Correo, Telefonos
        from clientessimi
        where IdInmobiliaria=?
        "))
        ) {
            $consulta->bind_param("i", $codInmo);

            $consulta->execute();

            if ($consulta->bind_result($IdInmobiliaria, $Nombre, $Direccion, $Correo, $Telefonos)) {
                while ($consulta->fetch()) {
                    $arreglo[] = array(
                        "IdInmobiliaria" => $IdInmobiliaria,
                        "Nombre"         => ucwords(strtolower($Nombre)),
                        "Direccion"      => $Direccion,
                        "Correo"         => $Correo,
                        "Telefonos"      => $Telefonos,
                    );

                }
            }
            return $arreglo;
        } else {
            echo "error --" . $conn->error;
        }

    }

    public function infoInmueble($codInmu, $debug)
    {
        list($inmo, $inmu) = explode('-', $codInmu);
        
        $stmt = $this->pdo->prepare("SELECT inv.idInm, inv.codinm, inv.IdInmobiliaria, inv.IdGestion, inv.IdTpInm, inv.IdBarrio, inv.banos, inv.alcobas, inv.garaje, inv.ValorVenta, inv.ValorCanon, inv.AreaConstruida, inv.AreaLote, inv.descripcionlarga, inv.latitud, inv.longitud, br.IdLocalidad, br.IdZona, br.IdCiudad, inv.IdBarrio, inv.Estrato, inv.ValorIva, inv.Administracion, inv.IdDestinacion,inv.EdadInmueble
        FROM inmnvo inv, barrios br
        WHERE codinm        = :inmu
        and IdInmobiliaria =:inmo
        AND br.IdBarrios = inv.IdBarrio");
         $stmt->bindParam(':inmo', $inmo);
         $stmt->bindParam(':inmu', $inmu);

        

        if ($stmt->execute()) {

                $result                      = $stmt->fetch(PDO::FETCH_ASSOC);
                
                $result['Tipo_Inmueble'] = ucfirst(strtolower(getCampo('tipoinmuebles', ' where idTipoInmueble = ' . $result['IdTpInm'], 'elpaisven')));
                $result['ciudad']        = ucfirst(strtolower(getCampo('ciudad', ' where IdCiudad = ' . getCampo('barrios', ' where IdBarrios = ' . $result['IdBarrio'], 'IdCiudad'), 'Nombre')));
                $result['depto']         = ucfirst(strtolower(getCampo('departamento', ' where IdDepartamento = ' . getCampo('ciudad', ' where IdCiudad = ' . getCampo('barrios', ' where IdBarrios = ' . $result['IdBarrio'], 'IdCiudad'), 'IdDepartamento'), 'elpaischar')));
                $result['Gestion']       = ucfirst(strtolower(getCampo('gestioncomer', ' where IdGestion = ' . $result['IdGestion'], 'charelpais')));
                $result['zona']          = ucfirst(strtolower(getCampo('zonas', ' where IdZona = ' . $result['IdZona'], 'elpaiszonas')));
                $result['localidad']     = ucfirst(strtolower(getCampo('localidad', ' where idlocalidad = ' . $result['IdLocalidad'], 'descripcion')));
                $result['barrio']        = ucfirst(strtolower(getCampo('barrios', ' where IdBarrios = ' . $result['IdBarrio'], 'NombreB')));

                $result['destinacion'] = ucfirst(strtolower(getCampo('destinacion', ' where Iddestinacion = ' . $result['IdDestinacion'], 'Nombre', 0)));
                  if ($result['IdGestion'] == 1) {
                        $result['oper']   = 'rent';
                        $valorsaldo= elpais::rangePais($result['IdGestion'],$result['ValorCanon']);
                         $result['precio'] = $result['ValorCanon'];
                    } else {
                        $result['oper']   = 'sale';
                         $valorsaldo=utf8_decode(elpais::rangePais($result['IdGestion'],$result['ValorVenta']));
                         $result['precio'] = $result['ValorVenta'];
                    }
                    
                $inm_telefono           = getCampo('clientessimi', "where idInmobiliaria=" . $inmo, 'Telefonos');
                $inmobiliaria_correo    = getCampo('clientessimi', "where idInmobiliaria=" . $inmo, 'Correo');
                $logoinm                = getCampo('clientessimi', "where idInmobiliaria=" . $inmo, 'logo');
                $logoinm                = str_replace('../', '', $logoinm);
                $logoinm                = "https://www.simiinmobiliarias.com/" . $logoinm;
                $paginaweb              = getCampo('clientessimi', "where idInmobiliaria=" . $inmo, 'Web');
                $direccionInm              = getCampo('inmobiliaria', "where idInmobiliaria=" . $row['IdInmobiliaria'], 'DireccionInm');

                 $postCaracteristicas = array();
                $postCaracteristicas['IdInmobiliaria'] = $inmo;
                $postCaracteristicas['codinm']         = $inmu;
                $postCaracteristicas['tpcar']  = 1;
                $result['caracteristicasGral']=$this->getCaracteristicas($inmo."-".$inmu);
               foreach ($result['caracteristicasGral'] as $key => $value) {
                   if($value['idCaracteristica'] == 13)
                   {
                        $cocina=$this->trae_carateristicaselpais(13, $inmo."-".$inmu,"");   
                   }
                    if($value['idCaracteristica'] == 13)
                   {
                        $cocinaServicio=$this->trae_carateristicaselpais(13, $inmo."-".$inmu,"Cuarto Y");
                        $cocinaServicio=($cocinaServicio['caractePais']==1)?'Si':'NO'; 

                   }
                    if($value['idCaracteristica'] == 44)
                   {
                        $zropas=$this->trae_carateristicaselpais(44, $inmo."-".$inmu,"ZONA DE ROPAS");
                        $zonasRopas=($patio['caractePais']==1)?'Si':'NO'; 

                        $patio=$this->trae_carateristicaselpais(44, $inmo."-".$inmu,"PATIO");
                        $patios=($patio['caractePais']==1)?'Si':'NO'; 

                   }
                    if($value['idCaracteristica'] == 16)
                   {
                        $banioS=$this->trae_carateristicaselpais(16, $inmo."-".$inmu,"social");
                        $banioSocial=($banioS['caractePais']==1)?'Si':'No'; 

                   }
                    if($value['idCaracteristica'] == 25)
                   {
                        $Asensor=$this->trae_carateristicaselpais(25, $inmo."-".$inmu,"Ascensor");
                        $Asensores=($Asensor['caractePais']==1)?'Si':'NO'; 

                   }
                   if($value['idCaracteristica'] == 26)
                   {
                        $Piscina=$this->trae_carateristicaselpais(26, $inmo."-".$inmu,"Piscina");
                        $Piscinas=($Piscina['caractePais']==1)?'Si':'NO'; 

                        $zVerdes=$this->trae_carateristicaselpais(26, $inmo."-".$inmu,"zonas verdes");
                        $zonasVerdes=($zVerdes['caractePais']==1)?'Si':'NO'; 

                   }
                   if($value['idCaracteristica'] == 46)
                   {
                        $Piso=$this->trae_carateristicaselpais(46, $inmo."-".$inmu,"Piso");
                        $Pisos=($Piso['caractePais']=="")?0:$Piso['caractePais']; 

                   }
                    if($value['idCaracteristica'] == 10)
                   {
                        $balcon=$this->trae_carateristicaselpais(10, $inmo."-".$inmu,"balcon");
                        $balcones=($balcon['caractePais']==1)?'Si':'NO'; 

                   }
                   
               }
                
                $datos_inmueble_entrada = array(
                    "attributes"            => array(
                        "País"                                 => "Colombia",
                        "Área"                                 => intval($result['AreaLote']),
                        "Texto adicional del anuncio internet" => utf8_encode($result['descripcionlarga']),
                        "Barrio"                               => "Alameda",
                        "Ciudad"                               => "Cali",
                        "departamento"                         => "Valle del Cauca",
                        "precio"                               => $result['precio'],
                        "Zona"                                 => "Sur", //ucwords(strtolower($result['zona'])),
                        "Condición"                            => "Usado",
                        "estrato"                              => $result['Estrato'],
                        "No. de Alcobas"                       => $result['alcobas'],
                        "No. de Baños"                         => $result['banos'],
                        "Parqueadero-Garaje"                   => ($result['garaje']==0)?"No":$result['garaje'] , 
                        "No. de Plantas"                       => 3,
                        "Tiempo Construido"                    => utf8_decode($this->rangeEdadInmueble($result['EdadInmueble'])),
                        "Cocina"                               => ($cocina['caractePais']=="")?"sencilla":$cocina['caractePais'],
                        "Alcoba Servicio"                      => ($cocinaServicio=="")?"No":$cocinaServicio,
                        "Baño Servicio"                        => ($cocinaServicio=="")?"No":$cocinaServicio,
                        "Zona oficios"                         => ($zonasRopas=="")?"No":$zonasRopas,
                        "Patio"                                => ($patios=="")?"No":$patios,
                        "Baño Social"                          => ($banioSocial=="")?"No":$banioSocial,
                        "Ascensor"                             => ($Asensores=="")?"No":$Asensores,
                        "No. de Piso"                          => ($Pisos>0)?$Pisos:0,
                        "Piscina"                              => ($Piscinas=="")?"No":$Piscinas,
                        "Balcón"                               => ($balcones=="")?"No":$balcones,
                        "Zonas Verdes"                         => ($zonasVerdes=="")?"No":$zonasVerdes,
                    ),
                    "contactPhone"          => $inm_telefono,
                    "contactEmail"          => $inmobiliaria_correo,
                    "contactAddress"        => $direccionInm ,
                    "contactWeb"            => $paginaweb,
                    "external_reference"    => "",
                    "customerId"            => "405797",
                    "operationType"         => $result['Gestion'] ,
                    "adviceType"            => $result['Tipo_Inmueble'],
                    "images"                => $this->fotosInmueble($inmo."-".$inmu),
                    "videos"                =>  getCampo('inmuebles', "where idInm='" . $inmo."-".$inmu. "'", 'linkvideo'),
                    "geo_latitud"           => $result['latitud'],
                    "geo_longitud"          => $result['longitud'],
                    "portalconfigurator_id" => "3",
                    "productId"             => "230",

                );
            if ($debug) {
             
            }

            if ($error > 0) {
                return array('error' => 1, 'msgerror' => $msgerror);
            } else {
                 if ($result['IdGestion'] == 1) {
                        $datos_inmueble_entrada['attributes']["Rango de precio FR alquiler"]=$valorsaldo;
                    } else {
                       $datos_inmueble_entrada['attributes']["Rango de precio FR otros"]=$valorsaldo; 
                    }
                return $datos_inmueble_entrada; 
            }

        } else {
           
        }

    }

    public function fotosInmueble($codInmueble)
    {
        if (($consulta = $this->db->prepare(" select foto
        from fotos_nvo
        where id_inmft=?"))
        ) {
            if (!$consulta->bind_param("s", $codInmueble)) {
                echo "error bind";
            } else {
                $consulta->execute();
                if ($consulta->bind_result($foto)) {
                    $arreglo = array();
                }

                $c = 0;
                $fotos="";
                while ($consulta->fetch()) {
                    $c++;
                    
                     if($c==1)
                     {

                        $fotos.="http://www.simiinmobiliarias.com/mcomercialweb/" . $foto;
                     }else
                     {
                        $fotos.=" http://www.simiinmobiliarias.com/mcomercialweb/" . $foto;

                     }
                        
                   

                }
                return $fotos;

            }
        } else {
            echo "error img" . $conn->error;
        }

    }
    public function trae_carateristicaselpais($grupo, $inmueble, $especifica)
    {
       
        $result = array();
        $connPDO  = new Conexion();
         $valueEspecifica= utf8_decode('%'.$especifica.'%');
        $cond = "";
        if ($especifica) {
            $cond .= " and Descripcion like :descripcion";
        }
        $stmt = $this->pdo->prepare("SELECT  charelpais as caractePais
             FROM detalleinmueble
             INNER JOIN maestrodecaracteristicas
             ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica
             AND  maestrodecaracteristicas.IdGrupo in (:grupo)
             INNER JOIN inmuebles ON detalleinmueble.idinmueble =:idinmuebles
             AND detalleinmueble.idinmueble = inmuebles.idinm
             #AND maestrodecaracteristicas.charelpais <> ''
             $cond");
        if ($especifica) {
         
            $stmt->bindParam(':descripcion', $valueEspecifica, PDO::PARAM_STR);
        }
        $stmt->bindParam(':idinmuebles',$inmueble);
        $stmt->bindParam(':grupo',$grupo);
        if($stmt->execute())
        {
            
            return $result  = $stmt->fetch(PDO::FETCH_ASSOC);
           

            
        }else
        {
            return $error[]=array(
                "errorInfo"=>$stmt->errorInfo()
                );
        }

       
    }
    public function getDataDetalleInmuebleFull($data, $pdo)
    {

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $this->pdo->prepare("SELECT  d.idcaracteristica,m.IdGrupo,d.obser_det,d.cantidad_dt,m.Descripcion,m.tpcar,m.icono_car
                 FROM detalleinmueble d,maestrodecaracteristicasnvo m
                 WHERE d.inmob          = :IdInmobiliaria
                 AND d.codinmdet        =:codinm
                 and m.tpcar           =:tpcar
                 and d.idCaracteristica =m.idCaracteristica");

        if ($stmt->execute(array(

            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":tpcar"          => $data['tpcar'],

        ))) {
            $info = array();
            while ($row = $stmt->fetch()) {

                $info[] = array(
                    "idcaracteristica" => $row['idcaracteristica'],
                    "obser_det"        => $row['obser_det'],
                    "cantidad_dt"      => $row['cantidad_dt'],
                    "Descripcion"      => str_replace("Ñ", "ñ", ucwords(strtolower(utf8_encode($row['Descripcion'])))),
                    "icono_car"        => "<i class='fa " . $row['icono_car'] . "'></i>",
                    "tpcar"            => $row['tpcar'],
                    "IdGrupo"            => $row['IdGrupo'],
                );
            }

            return $info;
        } else {
            print_r($stmt->errorInfo());
        }

    }

    static function rangePais($type,$valor)
    {
     
        $arriendovalor=array("1"=>"Hasta $500 Mil","2"=>"De $500 Mil a $1 Millón","3"=>"Más de $1 Millón");
        $vantavalor=array(
            "1"=>"Hasta $50 Millones",
            "2"=>"De $50 a $75 Millones",
            "3"=>"De $75 a $100 Millones",
            "4"=>"De $100 a $150 Millones",
            "5"=>"De $150 a $200 Millones",
            "6"=>"De $200 a $250 Millones",
            "7"=>"De $250 a $300 Millones",
            "8"=>"De $300 a $350 Millones",
            "9"=>"De $350 a $400 Millones",
            "10"=>"Más de $400 Millones",
        );

        if($type==1)
        {
            if($valor<=500000)
            {
                return $arriendovalor[1];
            }
            if($valor>500000 && $valor<=1000000)
            {
                return $arriendovalor[2];
            }
            if($valor>1000000)
            {
                return $arriendovalor[3];
            }
        }else
        {
            if($valor<=50000000)
            {
                return $vantavalor[1];
            }
            if($valor>50000000 && $valor<=75000000)
            {
                return $vantavalor[2];
            }
            if($valor>75000000 && $valor<=100000000)
            {
                return $vantavalor[3];
            }
            if($valor>100000000 && $valor<=150000000)
            {
                return $vantavalor[4];
            }
            if($valor>150000000 && $valor<=200000000)
            {
                return $vantavalor[5];
            }
            if($valor>200000000 && $valor<=250000000)
            {
                return $vantavalor[6];
            }
            if($valor>250000000 && $valor<=300000000)
            {
                return $vantavalor[7];
            }
            if($valor>300000000 && $valor<=350000000)
            {
                return $vantavalor[8];
            }
            if($valor>350000000 && $valor<=400000000)
            {
                return $vantavalor[9];
            }
            if($valor>400000000)
            {
                return $vantavalor[10];
            }
        }
        



    }
    public function rangeEdadInmueble($edad)
    {
         $anioActual=date("Y");

        $anioConstruccion=$anioActual-$edad;

        $tiempoConstruido=array("1"=>"Entre 0 y 5 años","2"=>"Entre 10 y 15 años","3"=>"Entre 5 y 10 años","4"=>"Más de 15 años");

        if($anioConstruccion<=5)
        {
            return $tiempoConstruido[1];
        }
        if($anioConstruccion>5 && $anioConstruccion<=10)
        {
            return $tiempoConstruido[3];   
        }
        if($anioConstruccion>10 && $anioConstruccion<=15)
        {
            return $tiempoConstruido[2];   
        }
        if($anioConstruccion>15)
        {
            return $tiempoConstruido[4];   
        }


    }
        private function getCaracteristicas($idinm) {

         $result = array();
        $sql = "SELECT  g.Detalle,m.descripcion,m.Cantidad,m.IdGrupo,d.obser_det
                FROM maestrodecaracteristicas m
                INNER JOIN detalleinmueble d ON m.idcaracteristica=d.idcaracteristica
                and d.idinmueble = :idInm
                INNER JOIN grupocaracteristicas g ON m.IdGrupo=g.IdGrupo
                order by g.Detalle asc";

        $stmt = $this->pdo->prepare($sql);

        $stmt->bindParam(':idInm', $idinm);

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $cn++;
                $det = str_replace("Ñ", "ñ", ucwords(strtolower(utf8_encode($row['Detalle']))));
                $cara = str_replace("Ñ", "ñ", ucwords(strtolower(utf8_encode($row['descripcion']))));
                $cant = $row['Cantidad'];
                $IdGrupo = $row['IdGrupo'];
                $obser_det = utf8_encode($row['obser_det']);
                if ($IdGrupo == 24 or $IdGrupo == 23 or $IdGrupo == 35 or $IdGrupo == 9 or $IdGrupo == 17 or $IdGrupo == 13 or $IdGrupo == 27) {
                    $car = "$det $cara $obser_det ";
                } else {
                    $car = "$cara $obser_det ";
                }
                $result[] = array(
                    'idCaracteristica' => $row['IdGrupo'],
                    'descripcion' => $car
                );
            }

            return $result;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
     public function deleteExtseionImage($imagen)
    {
        $tipos = array('.JPG', '.jpeg', '.JPEG', '.GIF', '.PNG', ".jpg", ".png", ".gif", ".xls", ".docx", ".pdf", ".ppt", ".doc", ".txt");
        foreach ($tipos as $key => $value) {
            $validar = strpos($imagen, $value);
            if ($validar != "") {
                $response = str_ireplace($value, "", $imagen);
            }
        }
        return $response;
    }
    public function insertElpaisdtp($idInm,$codigo)
    {
        list($inmo,$inmu)=explode("-",$idInm);
        $conse=consecutivo('conse_dtp','detportalesinmueble');
        $stmt=$this->pdo->prepare('INSERT INTO detportalesinmueble (conse_dtp,idinm_dtp,inmob_dtp,portal_dtp,cod_dtp,urlpublica_dtp) 
                                  VALUES (:conse_dtp,:idinm_dtp,:inmob_dtp,:portal_dtp,:cod_dtp,:urlpublica_dtp)');

        if($stmt->execute(array(
                ":conse_dtp"=>$conse,
                ":idinm_dtp"=>$inmu,
                ":inmob_dtp"=>$inmo,
                ":portal_dtp"=>29,
                ":cod_dtp"=>$codigo,
                ":urlpublica_dtp"=>"",
            )))
        {
            
            

            $execute=$this->insertRPais($inmo,$inmu,$codigo);
            if($execute==1)
            {
                return 1;
            }else
            {
                return $execute;
            }
           
        }else
        {
           return $response=array("Error"=>$stmt->errorInfo(),"CodSimi"=>$idInm,"CodPais"=>$codigo,"conse"=>$conse);
        }
    }
    private function  insertRPais($inmo,$inmu,$codigo)
    {
        
        
        $inmueble=$inmo."-".$inmu;
        $stmt=$this->pdo->prepare('UPDATE inmuebles
        SET RPais=:codPais
        WHERE IdInmobiliaria=:idInmob
        AND idInm = :idInmu');

        if($stmt->execute(array(
            ":codPais"=>$codigo,
            ":idInmob"=>$inmo,
            ":idInmu"=>$inmueble
            )))
        {
            return 1;
        }else
        {
           return $response=array("Error"=>$stmt->errorInfo(),"CodPais"=>$codigo);
        }

    }
    public function deleteELpaisdtp($idInm,$codigo)
    {
        list($inmo,$inmu)=explode("-",$idInm);
         $stmt=$this->pdo->prepare('DELETE FROM detportalesinmueble 
                                    WHERE idinm_dtp=:idinm_dtp
                                        AND inmob_dtp=:inmob_dtp
                                        AND  cod_dtp=:cod_dtp
                                        AND  portal_dtp=29');
         if($stmt->execute(array(
                ":idinm_dtp"=>$inmu,
                ":inmob_dtp"=>$inmo,
                ":cod_dtp"=>$codigo,

            )))
         {
            $execute=elpais::insertRPais($inmo,$inmu,0);
            if($execute==1)
            {
                return 1;
            }else
            {
                return $execute;
            }
         }else
         {

         }

    }

}
