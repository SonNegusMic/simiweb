<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

class Afydi
{
    public function __construct($conn){
		$this->db=$conn;
	}
    /*********************************
     *   Array de consulta Inmueble  *
     *********************************/
    public function listadoInmueblesAfydi($idEmpresa,$codInmu,$usr_act)
    {
      
		$dInmuebles		= new Inmuebles();
		$avisoVentana=0;
		
		$pservidumbre				= $dInmuebles->evalua_carac(23,$codInmu,'Cantidad','SERVIDUMBRE');
		$pindependiente				= $dInmuebles->evalua_carac(23,$codInmu,'Cantidad','INDEPENDIENTE',0);
		$comunal					= $dInmuebles->evalua_carac(23,$codInmu,'Cantidad','COMUNAL',0);
		$pvisitantes				= $dInmuebles->evalua_carac(23,$codInmu,'Cantidad','VISITANTES',0);
		$pcubierto					= $dInmuebles->evalua_carac(23,$codInmu,'Cantidad','CUBIERTO');
		$ascensor					= $dInmuebles->evalua_carac(25,$codInmu,'Cantidad','ASCENSOR');
		$banos   					= $dInmuebles->trae_carac(16,$codInmu,'Cantidad',utf8_decode('baño'),0); 
		$alcobas   					= $dInmuebles->trae_carac(15,$codInmu,'Cantidad','alcoba',0); 
		$nivel   					= $dInmuebles->trae_carac(36,$codInmu,'Cantidad','nivel',0);
		$inm_destacado				= getCampo('settings_portales',"where inmu_p='".$codInmu."' and tipo_p=4",'vlr_p');
		$sucursal=0;
		
			
		if(($consulta=$this->db->prepare(" select 
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		idInm,ValorVenta,ValorCanon,descripcionlarga,
		IdBarrio,IdGestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,Direccion, 
		IdPromotor,Fondo,Frente,
		Restricciones,idEstadoinmueble,IdDestinacion,IdPropietario,
		linkvideo,fingreso,FConsignacion,NoMatricula,ComiVenta,
		ComiArren,IdProcedencia
		from inmuebles
        where IdInmobiliaria=?
        and idInm=?
		#$cond
		limit 0,20")))
		{  	
				if($codInmu)
				{
					$consulta->bind_param("is",$idEmpresa,$codInmu);
				}
				else
				{
					$consulta->bind_param("is",$idEmpresa,$codInmu);
				}
			 
				$consulta->execute();

				

				if($consulta->bind_result($IdInmobiliaria,$Administracion,$IdGestion,$Estrato,
					$IdTpInm,$idInm,$ValorVenta,$ValorCanon,$descripcionlarga,
					$IdBarrio,$IdGestion,$AreaConstruida,$AreaLote,$latitud,
					$longitud,$EdadInmueble,$Direccion,$IdPromotor,$Fondo,
					$Frente,$Restricciones,$idEstadoinmueble,$IdDestinacion,
					$IdPropietario,$linkvideo,$fingreso,$FConsignacion,$NoMatricula,
					$ComiVenta,$ComiArren,$IdProcedencia))

				while($consulta->fetch())
				{
					$IdCiudad 	= getCampo('barrios',"where IdBarrios=".$IdBarrio,'IdCiudad');
					$IdZona		= getCampo('barrios',"where IdBarrios=".$IdBarrio,'IdZona');
					if($IdGestion==5)
					{
						$porc_comision=$ComiVenta;
					}
					else
					{
						$porc_comision=$ComiArren;
					}
					if($IdProcedencia==58)
					{
						$avisoVentana=1;
					}
					$IdTpInmAfydi = (getCampo('tipoinmuebles',"where idTipoInmueble=$IdTpInm",'conafydi',0)=="")?1:getCampo('tipoinmuebles',"where idTipoInmueble=$IdTpInm",'conafydi',0);
					$IdGestionAfydi		= getCampo('gestioncomer',"where IdGestion=$IdGestion",'convgafydi',0);
					$IdDestinacionAfydi	= getCampo('destinacion',"where IdDestinacion=$IdDestinacion",'convdafydi',0);
					$ZonaAfydi			= getCampo('zonas',"where IdZona=$IdZona",'zafydi',0);
					$statusAfydi		= getCampo('estado_inmueble',"where idestado=$idEstadoinmueble",'ord_est',0);
					$barrio_comun		= getCampo('barrios',"where IdBarrios=$IdBarrio",'NombreB',0);
					$dateIngreso=($fingreso=="")?"0":$fingreso;
					$inm_destacado=($inm_destacado==0)?"0":$inm_destacado;
					$inm=explode("-",$idInm);
					$nivel = ($nivel=="")?1:$nivel;
					$linkvideo = ($linkvideo == "")?" ":$linkvideo;
					$ValorCanon = ($ValorCanon == "")?0:$ValorCanon;
					$latitud=($latitud < 0)?$latitud:'+'.$latitud;
					$longitud=($longitud)?$longitud:'+'.$longitud;
					if(strlen($EdadInmueble)<=2)
					{
						$EdadInmueble1=date('Y')-$EdadInmueble;
					}
					else
					{
						$EdadInmueble1=$EdadInmueble;
					}
					

					 $arreglo= array(
						"codpro"                => $inm[0].$inm[1],
						"address"               => utf8_encode($Direccion),
						"city"                  => $IdCiudad,
						"zone"                  => $ZonaAfydi,
						"type"                  => $IdTpInmAfydi,
						"biz"                   => $IdGestionAfydi,
						"area_lot"              => $AreaLote,
						"build_year"            => $EdadInmueble1,
						"area_cons"             => $AreaConstruida,
						"bedrooms"              => $alcobas,
						"bathrooms"             => $banos,
						"stratum"               => $Estrato,
						"saleprice"             => $ValorVenta,
						"description"           => utf8_encode($descripcionlarga),
						"latitude"              => $latitud,
						"longitude"             => $longitud,
						"barrio_comun"          => $barrio_comun,
						"status"                => $statusAfydi,
						"neighborhood"          => $barrio_comun,
						"floor"                 => $nivel,
						"rent"                  => $ValorCanon,
						"administration"        => $Administracion,
						#"desc_eng"              => "",
						#"desc_fre"              => "",
						"resctrictions"         => $Restricciones,
						#"comment"               => "",
						#"comment2"              => "",
						#"asesor"                => $IdPromotor,
						"video"                 => $linkvideo,
						"parking"               => $pindependiente,
						"parking_covered"       => $pcubierto,
						"consignation_date"     => $FConsignacion,
						"propietario"					=> $IdPropietario,
						"registry_date"         => $dateIngreso,
						"destacado"             => $inm_destacado,
						#"branch"                => $idEmpresa.$sucursal,
						"window_sign"           => $avisoVentana, 
						"front"                 => $Frente,
						#"rear"                  => "",
						#"private_area"          => "",
						#"destinacion"           => "$IdDestinacionAfydi",
						#"commission_percentage" => "",
						#"matricula_number"      => "",
						#"referencia"            => "",
						"imagen1" => $this->fotosInmuebleAfydi($idInm,0), 
						
		             );
				}

			return $arreglo;

		}
		else
		{
			echo  "error --".$conn->error;
		}

        
    }
    /*********************************
     *        Array de fotos Simi    *
     *********************************/
    public function fotosInmuebleAfydi($codInmueble,$posicion)
    {
    
    	if($posicion==0)
    	{

        	@include("../../funciones/connPDO.php");
    	}
        $connPDO=new Conexion();
        $stmt=$connPDO->prepare("SELECT   foto FROM fotos_nvo
								 WHERE    id_inmft=:id_inmft
								 order by posi_ft
								 limit $posicion,1");
        if($stmt->execute(array(
        	
        	":id_inmft"=>$codInmueble
        	)))
        {
        	 $stmt->bindColumn(1, $foto);
        	 while ($rows = $stmt->fetch(PDO::FETCH_BOUND)) {
				    return "http://www.simiinmobiliarias.com/mcomercialweb/".$foto;
				}
        }
        else
        {
        	return $response=array(
        		"Error"=>$stmt->errorInfo(),
        		"data"=>$codInmueble.$posicion
        		);
        }
        
	}
	/*********************************
	 *    Actualizar codigo Afidy   *
	 *     en el inmueble Simi       *
	 *********************************/
	public function updateCodeAfydi($codInmu,$afydi)
	{
		// @include("../../funciones/connPDO.php");
		$conexion = new Conexion();
		$stmt=$conexion->prepare('UPDATE inmuebles SET RAfydi =:RAfydi
								  WHERE idInm = :idInm');
		if($stmt->execute(array(
			":RAfydi"=>$afydi,
			":idInm"=>$codInmu)))
		{
			return 1;
		}
		else
		{
			print_r($stmt->errorInfo());
		}
	}
	/*********************************
	 *     Caracteristicas Afydi     *
	 *********************************/

	public function getCasracteristicasAfydi($codInmu)
	{
		$conexion = new Conexion();

		$stmt=$conexion->prepare('
				SELECT codafydi 
        FROM detalleinmueble,maestrodecaracteristicas
        WHERE idinmueble=:idinmueble
        AND detalleinmueble.`idcaracteristica`=maestrodecaracteristicas.`idCaracteristica`
        AND codafydi>0');

        if($stmt->execute(array(
        	':idinmueble'=>$codInmu
        	)))
        {
        	$response="";
        	while ($row = $stmt->fetch()) {
        		$response.=$row['codafydi'].",";
        	}
        	return $response;
        }
        else
        {
        	$stmt->errorInfo();
        }
        $stmt="";
	}
	/*********************************
	 *        Insertar inmueble      *
	 *********************************/
	public function insertInmueble($inmueble,$tokens,$uri)
    {

        
        $config = Afydi::configAfydi($uri);
        $token='Authorization:'.$tokens;
        $service_url = $config['uri']."/inmuebles/";
        unset($inmueble[0]['codpro']);
        $curl_post_data = json_encode($inmueble);
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array('Content-Type: application/json',$token));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);
        $response = json_decode($curl_response,true);
        curl_close($curl);
        return $response;
     }
     /*********************************
	 *        Actualizar inmueble     *
	 *********************************/
     public function updateInmueble($inmueble,$tokens,$uri)
     {

        $config = Afydi::configAfydi($uri);
        $token='Authorization:'.$tokens;
        $service_url = $config['uri']."/inmuebles/".$inmueble['codpro'];
        unset($inmueble['codpro']);
        $curl_post_data = json_encode($inmueble);
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array('Content-Type: application/json',$token));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);
        $response = json_decode($curl_response,true);
        curl_close($curl);
        return $response;
     }
     /*********************************
	 *        Obtener inmueble        *
	 *********************************/
     public function getInmueble($codInmueble,$tokens,$uri)
     {

        
        $config = Afydi::configAfydi($uri);
       $token='Authorization:'.$tokens;
        $service_url = $config['uri']."/inmuebles/".$codInmueble;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array('Content-Type: application/json',$token));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        $response = json_decode($curl_response,true);
        curl_close($curl);
        return $response;
     }
     /*********************************
	 *        Borrar inmueble      *
	 *********************************/
     public function deleteInmueble($codInmueble,$tokens,$uri,$id)
     {
        
        $config = Afydi::configAfydi($uri);
        $token='Authorization:'.$tokens;
        $service_url = $config['uri']."/inmuebles/".$id;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array('Content-Type: application/json',$token));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        echo $curl_response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($curl_response,true);
        return $response;
     }
      /*********************************
	 *        Obtener Asesores      *
	 *********************************/
     public function getAsesores($tokens,$uri)
     {
        
        $config = Afydi::configAfydi($uri);
        $token='Authorization:'.$tokens;
        $service_url = $config['uri']."/brokers";
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array('Content-Type: application/json',$token));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($curl_response,true);
        return $response;
     }
     /*********************************
      *    Configracion ApiAfydi      *
      *********************************/
     private static function configAfydi($uri)
     {

     	$uri=($uri==1)?'http://api.domus.la':'http://stg.domus.la';
     	return $token =  array(
     		"uri"=>$uri
     		);
     }
 
}
?>