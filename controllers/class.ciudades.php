<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include("../funciones/connPDO.php");


class Ciudades 
{
    
    public function __construct($conn=''){
		$this->db=$conn;		
	}	
	public function obtenerCiudades()
	{
		$connPDO = new Conexion();
		$stmt = $connPDO->prepare("SELECT IdCiudad, Nombre FROM ciudad");
		if($stmt->execute())
		{
			$connPDO -> exec("SET NAMES 'utf8'");
			$data = array();
			while($row = $stmt->fetch())
						{
							$data[] = array(
								"id"     => $row['IdCiudad'],
								"Nombre" => ucfirst(strtolower(utf8_encode($row['Nombre'])))
								) ;
						}
			
					return $data;
		}
		else
		{
				return print_r($stmt->errorInfo());
		}
		$stmt= NULL;

	}
	 public function obtenerCiudadesPais($idPais)
    {
        $connPDO = new Conexion();
        $data    = array();
        $pais    = $connPDO->prepare("SELECT IdPais, Nombre FROM paises WHERE `IdPais` = :idPais");

        $pais->bindParam(':idPais', $idPais, PDO::PARAM_STR);
        if ($pais->execute()) {
            $connPDO->exec("SET NAMES 'utf8'");
            $rowpais  = $pais->fetch(PDO::FETCH_OBJ);
            $ciudades = $connPDO->prepare("
            	SELECT c.`IdCiudad`,
			        c.`Nombre`
					FROM departamento d,
			     		ciudad c
					WHERE d.`IdDepartamento`=c.`IdDepartamento`
			    	AND d.`IdPais`=:idpais
					ORDER BY Nombre ASC");
            $ciudades->bindParam(':idpais', $idPais, PDO::PARAM_INT);
            if ($ciudades->execute()) {
                $connPDO->exec("SET NAMES 'utf8'");
                while ($rowciudades = $ciudades->fetch()) {
                    $data[] = array(
                        'id'     => $rowciudades['IdCiudad'],
                        'Nombre' => ucfirst(strtolower(($rowciudades['Nombre']))),
                    );
                }
                return $data;
            }
        }
    }

	public function ciudadesInmubles($idInmu)
		{
			$connPDO = new Conexion();
			$addSql ="";
			$stmt = $connPDO->prepare("SELECT c.IdCiudad AS cod,
										  CONCAT(
										    UPPER(LEFT(c.Nombre, 1)),
										    LOWER(SUBSTRING(c.Nombre, 2))
										  ) AS nom 
										FROM
										  inmuebles i,
										  ciudad c,
										  barrios b 
										WHERE i.`IdBarrio` = b.`IdBarrios` 
										  AND b.`IdCiudad` = c.`IdCiudad` 
										  AND i.`IdInmobiliaria` = ?
										  AND i.`idEstadoinmueble` = 2 
										GROUP BY c.`IdCiudad` 
										ORDER BY c.nombre ");
			$stmt->bindParam(1,$idInmu,PDO::PARAM_STR);

			if($stmt->execute())
			{
				$data = array();
				$stmt->bindColumn(1,$cod);
				$stmt->bindColumn(2,$nom);
				$connPDO -> exec("SET NAMES 'utf8'");
				while($row = $stmt->fetch(PDO::FETCH_BOUND))
					{
						$data[]=array(
						
							"id" => $cod,
							"Nombre" => $nom
						);		
					}
					return $data;
			}
			else
			{
				return print_r($stmt->errorInfo());
			}
			$stmt = NULL;
		}

	public function ciudadesInmo($idInmo)
		{
			$connPDO = new Conexion();
			$addSql ="";
			$stmt = $connPDO->prepare("SELECT c.IdCiudad AS cod,
										  CONCAT(
										    UPPER(LEFT(c.Nombre, 1)),
										    LOWER(SUBSTRING(c.Nombre, 2))
										  ) AS nom 
										FROM
										  inmuebles i,
										  ciudad c,
										  barrios b 
										WHERE i.`IdBarrio` = b.`IdBarrios` 
										  AND b.`IdCiudad` = c.`IdCiudad` 
										  AND i.`IdInmobiliaria` = :idinmo
										  AND i.`idEstadoinmueble` = 2 
										GROUP BY c.`IdCiudad` 
										ORDER BY c.nombre ");
			//$stmt->bindParam(1,$idInmu,PDO::PARAM_STR);
			$stmt->bindParam(':idinmo', $idInmo, PDO::PARAM_INT);

			if($stmt->execute())
			{
				$data = array();
				$stmt->bindColumn(1,$cod);
				$stmt->bindColumn(2,$nom);
				$connPDO -> exec("SET NAMES 'utf8'");
				while($row = $stmt->fetch(PDO::FETCH_BOUND))
					{
						$data[]=array(
						
							"id" => $cod,
							"Nombre" => $nom
						);		
					}
					return $data;
			}
			else
			{
				return print_r($stmt->errorInfo());
			}
			$stmt = NULL;
		}
		


}

?>