<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');


class Caracteristicas
{
    public function __construct($conn){
		$this->db=$conn;
	}
    
    private $id_usuario;
    

    public function listadoGrupos()
    {
		$est=1;
		$i=0;
        if(($consulta=$this->db->prepare("select IdGrupo,DETALLE,IdTipoInm,IdLaGuia,car_est,car_ord

        from grupocaracteristicas
		where car_est=?
		order by car_ord")))
		{  	
			if(!$consulta->bind_param("i",$est))
			{
				echo "error bind";
			}
			else
			{ 
				$consulta->execute();
//				echo "$qry";
				
				if($consulta->bind_result($IdGrupo,$DETALLE,$IdTipoInm,$IdLaGuia,$car_est,$car_ord))
				
							?>
                            <table width="30%" align="center" class="TablaDatos">
                            <tr>
                            	<td class="tituloFormulario">Grupos Disponibles</td>
                            </tr>
							<?php 
				while($consulta->fetch())
				{
				     $i++;
                     ?>
                     <div class="row">
                        <div class="col-md-10">
                        
                        <tr>
                            <td>
                            <a href="form_creac.php?cod=<?php echo "$IdGrupo";?>&det=<?php echo "$DETALLE";?>"><?php echo "$IdGrupo - $DETALLE";?></a>
                            </td>
                        </tr>
                      
							
							
                        </div>
                       <div class="col-md-1" ></div>
                    </div>
                     <?php
 				}
				?>
                </table>
                <?php
 			}
		}
		else
		{
			echo  "error lista".$conn->error;
		}
        
    }
	
	public function listadoDetalles($codGrupo)
    {
		$est=1;
        if(($consulta=$this->db->prepare("select idCaracteristica,IdGrupo,Descripcion,idTipoInmueble,Cantidad,
		DETALLE,M2,Chequeo,idlaguia,idgoplc,
		IdOrdenamiento,CodCarMeli,abrevsvsa
        from maestrodecaracteristicas
		where IdGrupo=?
		order by idTipoInmueble")))
		{  	
			if(!$consulta->bind_param("i",$codGrupo))
			{
				echo "error bind";
			}
			else
			{ 
				$consulta->execute();
//				echo "$qry";
				
				if($consulta->bind_result($dCaracteristica,$IdGrupo,$Descripcion,$idTipoInmueble,$Cantidad,$DETALLE,$M2,$Chequeo,$idlaguia,$idgoplc,$IdOrdenamiento,$CodCarMeli,$abrevsvsa))
				while($consulta->fetch())
				{
                     ?>
                     <tr>
                        <td><?php echo $dCaracteristica; ?></td>
                        <td><?php 
						getSelect('tipoinmuebles','idTipoInmueble','Descripcion','',"",'tp',$idTipoInmueble,'',' id="tp" class="chosen-select form-control"','','1','',''); ?></div>
                        <td><?php CampoTxt('desc', $Descripcion,' id="desc" class="chosen-select form-control"'); ?></td>
                        <td><?php CampoNumerico('Cantidad', $Cantidad,' id="Cantidad" class="chosen-select form-control"'); ?></td>
                        <td><?php CampoNumerico('idlaguia', $idlaguia,' id="idlaguia" class="chosen-select form-control"'); ?></td>
                        <td><?php CampoTxt('CodCarMeli', $CodCarMeli,' id="CodCarMeli" class="chosen-select form-control"'); ?></td>
                     </tr>
					 <?php
 				}
 			}
		}
		else
		{
			echo  "error lista".$conn->error;
		}
        
    }
	public function listadoDetallesG($codGrupo,$detalle)
    {
		$est=1;  
		$Cantidad=0;
		$idlaguia=0;
        $w_conexion = new MySQL();
        $sql="select idCaracteristica,IdGrupo,Descripcion,idTipoInmueble,Cantidad,
			DETALLE,M2,Chequeo,idlaguia,idgoplc,
			IdOrdenamiento,CodCarMeli,abrevsvsa
			from maestrodecaracteristicas
			where IdGrupo=$codGrupo
			and Descripcion like  '%$detalle%'
			order by idTipoInmueble";
		$res=$w_conexion->ResultSet($sql);
		if(!$res)
		{
			echo "error bind";
		}
		else
		{ 
			//echo $sql;
			?>
            <table class="formatoHorizontaliz" width="100%">
            <tr>
                    <th>C&oacute;digo</th>
                    <th>Tipo Inmueble</th>
                    <th>Descripci&oacute;n</th>
                    <th>Cantidad</th>
                    <th>Id Fincaraiz</th>
                    <th>Cod Tuinmueble</th>
                    <th>Opci&oacute;n</th>
                </tr>
            <?php
			while($ff=$w_conexion->FilaSiguienteArray($res))
			{ 
				 
				 $idCaracteristica = $ff['idCaracteristica'];
				 $idTipoInmueble = $ff['idTipoInmueble'];
				 $Descripcion = $ff['Descripcion'];
				 $Cantidad = $ff['Cantidad'];
				 $idlaguia = $ff['idlaguia'];
				 $CodCarMeli = $ff['CodCarMeli'];
				 $edit=true;
				 ?> 
                 
                
				 <!--
					<td><?php echo $idCaracteristica; ?></td>
					<td><?php 
					getSelect('tipoinmuebles','idTipoInmueble','Descripcion','',"",'tp',$idTipoInmueble,'',' id="tp" class="chosen-select form-control"','','1','',''); ?></td>
					<td><?php CampoTxt('desc', $Descripcion,' id="desc" class="chosen-select form-control"'); ?></td>
					<td><?php CampoNumerico('Cantidad', $Cantidad,' id="Cantidad" class="chosen-select form-control"'); ?></td>
					<td><?php CampoNumerico('idlaguia', $idlaguia,' id="idlaguia" class="chosen-select form-control"'); ?></td>
					<td><?php CampoTxt('CodCarMeli', $CodCarMeli,' id="CodCarMeli" class="chosen-select form-control"'); ?></td>
				 -->
                 <tr>
                 	<td class="codedt" id="codedt"><?php echo $idCaracteristica; ?></td>
					<td><?php 
					getSelect('tipoinmuebles','idTipoInmueble','Descripcion','',"",'tp',$idTipoInmueble,'',' id="tp" class="chosen-select form-control"','','1','',''); ?></td>
					<td><input type="text" id="desc" name="desc" class="chosen-select form-control" value="<?php echo $Descripcion;?>"></td>
					<td><input type="number" id="Cantidad" name="Cantidad" class="chosen-select form-control" value="<?php echo $Cantidad;?>"></td>
					<td><input type="number" id="idlaguia" name="idlaguia" class="chosen-select form-control" value="<?php echo $idlaguia;?>"></td>
					<td><input type="text" id="CodCarMeli" name="CodCarMeli" class="chosen-select form-control" value="<?php echo $CodCarMeli;?>"></td>
                    <td><input type="button" id="actu" name="actu" class="chosen-select form-control actu" value="Actualizar"></td>
                    </tr>
				 <?php
			}
			?>
            </table>
            <?php
		}
		
    }
	public function listadoNvos($codGrupo)
    {
		$est=1;  
		$Cantidad=0;
		$idlaguia=0;
        $w_conexion = new MySQL();
        $sql="select *
			from tipoinmuebles";
		$res=$w_conexion->ResultSet($sql);
		if(!$res)
		{
			echo "error bind";
		}
		else
		{ 
			//echo $sql;
			?>
            <table class="formatoHorizontaliz" width="100%">
            <tr>
                    <th>C&oacute;digo</th>
                    <th>Tipo Inmueble</th>
                    <th>Descripci&oacute;n</th>
                    <th>Cantidad</th>
                    <th>Id Fincaraiz</th>
                    <th>Cod Tuinmueble</th>
                    <th>Opci&oacute;n</th>
                </tr>
            <?php
			while($ff=$w_conexion->FilaSiguienteArray($res))
			{ 
				 
				 $idTipoInmueble = $ff['idTipoInmueble'];
				 $Descripcion = $ff['Descripcion'];
				  $edit=true;
				 ?> 
                  <tr>
                 	<td class="codinvo" id="codinvo"><?php echo ""; ?></td>
					<td><?php 
					getSelect('tipoinmuebles','idTipoInmueble','Descripcion','',"where idTipoInmueble=$idTipoInmueble",'tp',$idTipoInmueble,'',' id="tp" class="chosen-select form-control"','','1','',''); ?></td>
					<td><input type="text" id="desc" name="desc" class="chosen-select form-control" value=""></td>
					<td><input type="number" id="Cantidad" name="Cantidad" class="chosen-select form-control" value="0"></td>
					<td><input type="number" id="idlaguia" name="idlaguia" class="chosen-select form-control" value="0"></td>
					<td><input type="text" id="CodCarMeli" name="CodCarMeli" class="chosen-select form-control" value="0"></td>
                    <td><input type="button" id="crearn" name="crearn" class="chosen-select form-control crearn" value="Crear"></td>
                    </tr>
				 <?php
			}
			?>
            </table>
            <?php
		}
	}
     
    public function crearCaracteristica($tpc,$descc,$Cantidadc,$idlaguiac,$CodCarMelic,$idGrupo)
    {
        $idCaracteristica=consecutivo("idCaracteristica","maestrodecaracteristicas");
		$det= ' ';
		$m2= ' ';
		$ch= ' ';
		$idgp= 0;
		$ord= 0;
		$abr= ' ';
			 if(($consulta=$this->db->prepare("INSERT INTO maestrodecaracteristicas (idCaracteristica,IdGrupo,Descripcion,idTipoInmueble,Cantidad,
			 DETALLE,M2,Chequeo,idlaguia,idgoplc,
			 IdOrdenamiento,CodCarMeli,abrevsvsa
	) 
        values(?,?,?,?,?,
        ?,?,?,?,?,
        ?,?,?)
")))
		{  	
			if(!$consulta->bind_param("iisiisssisiss",$idCaracteristica,$idGrupo,$descc,$tpc,$Cantidadc,$det,$m2,$ch,$idlaguiac,$idgp,$ord,$CodCarMelic,$abr))
			{
				echo "error bind";
			}
			else
			{ 
				if($consulta->execute())
				{
					//echo "Registro $idCaracteristica $descc ha sido Creado $idGrupo";
					echo "$idCaracteristica";
					caracteristicas::crearCaracteristicaNvo($tpc,$descc,$Cantidadc,$idlaguiac,$CodCarMelic,$idGrupo);
				}
				else
				{
					echo "error exce $tpc".$consulta->error;
				}
				
			}
        }
    }
	public function actualizarCaracteristica($idc,$descc,$Cantidadc,$idlaguiac,$CodCarMelic,$idGrupo)
    {
        //$idCaracteristica=consecutivo("idCaracteristica","maestrodecaracteristicas");
		$det= ' ';
		$m2= ' ';
		$ch= ' ';
		$idgp= 0;
		$ord= 0;
		$abr= ' ';
			 if(($consulta=$this->db->prepare("UPDATE maestrodecaracteristicas SET 
			 Descripcion =?,
			 Cantidad	=?,
			 idlaguia	=?,
			 CodCarMeli	=?
			 where idCaracteristica=?")))
		{  	
			if(!$consulta->bind_param("siisi",$descc,$Cantidadc,$idlaguiac,$CodCarMelic,$idc))
			{
				echo "error bind";
			}
			else
			{ 
				if($consulta->execute())
				{
					//echo "Registro $idCaracteristica $descc ha sido Actualizado $idGrupo";
					echo "$idCaracteristica";
					caracteristicas::actualizarCaracteristicaNvo($idc,$descc,$Cantidadc,$idlaguiac,$CodCarMelic,$idGrupo);
				}
				else
				{
					echo "error exce $tpc".$consulta->error;
				}
				
			}
        }
    }
    public function crearCaracteristicaNvo($tpc,$descc,$Cantidadc,$idlaguiac,$CodCarMelic,$idGrupo)
    {
        $idCaracteristica=consecutivo("idCaracteristica","maestrodecaracteristicas");
		$det= ' ';
		$m2= ' ';
		$ch= ' ';
		$idgp= 0;
		$ord= 0;
		$abr= ' ';
			 if(($consulta=$this->db->prepare("INSERT INTO maestrodecaracteristicasnvo 
			 	(idCaracteristica,IdGrupo,Descripcion,idTipoInmueble,obligatorio,
			 DETALLE,M2,Chequeo,idlaguia,idgoplc,
			 IdOrdenamiento,CodCarMeli,abrevsvsa
	) 
        values(?,?,?,?,?,
        ?,?,?,?,?,
        ?,?,?)
")))
		{  	
			if(!$consulta->bind_param("iisiisssisiss",$idCaracteristica,$idGrupo,$descc,$tpc,$Cantidadc,$det,$m2,$ch,$idlaguiac,$idgp,$ord,$CodCarMelic,$abr))
			{
				echo "error bind";
			}
			else
			{ 
				if($consulta->execute())
				{
					//echo "Registro $idCaracteristica $descc ha sido Creado $idGrupo";
					echo "$idCaracteristica";

				}
				else
				{
					echo "error exce $tpc".$consulta->error;
				}
				
			}
        }
    }
	public function actualizarCaracteristicaNvo($idc,$descc,$Cantidadc,$idlaguiac,$CodCarMelic,$idGrupo)
    {
        //$idCaracteristica=consecutivo("idCaracteristica","maestrodecaracteristicas");
		$det= ' ';
		$m2= ' ';
		$ch= ' ';
		$idgp= 0;
		$ord= 0;
		$abr= ' ';
			 if(($consulta=$this->db->prepare("UPDATE maestrodecaracteristicasnvo SET 
			 Descripcion =?,
			 obligatorio =?,
			 idlaguia	 =?,
			 CodCarMeli	 =?
			 where idCaracteristica=?")))
		{  	
			if(!$consulta->bind_param("siisi",$descc,$Cantidadc,$idlaguiac,$CodCarMelic,$idc))
			{
				echo "error bind";
			}
			else
			{ 
				if($consulta->execute())
				{
					//echo "Registro $idCaracteristica $descc ha sido Actualizado $idGrupo";
					echo "$idCaracteristica";
				}
				else
				{
					echo "error exce $tpc".$consulta->error;
				}
				
			}
        }
    }
	
}
?>