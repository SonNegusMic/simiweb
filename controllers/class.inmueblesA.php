<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');


class Inmuebles
{
    public function __construct($conn){
		$this->db=$conn;
	}
    
    private $id_usuario;
    
    public function listadoInmuebles($idEmpresa,$IdGestion,$codInmu='')
    {
        $cond='';
		if($codInmu)
		{
			$cond="and Codigo_Inmueble=?";
		}
		if(($consulta=$this->db->prepare(" select 
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo
		from datos_call
        where IdInmobiliaria=?
        and IdGestion=?
		$cond
		#limit 0,20")))
		{  	
			if($codInmu)
			{
				$consulta->bind_param("iis",$idEmpresa,$IdGestion,$codInmu);
			}
			else
			{
				$consulta->bind_param("ii",$idEmpresa,$IdGestion);
			}
			 
				$consulta->execute();
//				echo "$qry";
				$arreglo1 = array();
				if($consulta->bind_result($IdInmobiliaria,$Administracion,$IdGestion,$Estrato,$IdTpInm,
		$Codigo_Inmueble,$Tipo_Inmueble,$Venta,$Canon,$descripcionlarga,
		$Barrio,$Gestion,$AreaConstruida,$AreaLote,$latitud,
		$longitud,$EdadInmueble,$NombreInmo,$logo))
				while($consulta->fetch())
				{
					 $arreglo[0]=$IdInmobiliaria;//0
                     $arreglo[1]=$Administracion;//1
                     $arreglo[2]=$IdGestion;//2
                     $arreglo[3]=$Estrato;//3
                     $arreglo[4]=$IdTpInm;//4
                     $arreglo[5]=$Codigo_Inmueble;
                     $arreglo[6]=$Tipo_Inmueble;
                     $arreglo[7]=$Venta;
					 $arreglo[8]=$Canon;
					 $arreglo[9]=$descripcionlarga;
					 $arreglo[10]=$Barrio;
					 $arreglo[11]=$Gestion;
					 $arreglo[12]=$AreaConstruida;
					 $arreglo[13]=$AreaLote;
					 $arreglo[14]=$latitud;
					 $arreglo[15]=$longitud;
					 $arreglo[16]=$EdadInmueble;
					 $arreglo[17]=$NombreInmo;
					 $arreglo[18]="http://www.simiinmobiliarias.com/".str_replace('../','',$logo);
					 
					 $arreglo1[] = $arreglo;
                     
 				}
               // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
               // print_r($arreglo1)."------------------";
                return $arreglo1;
		}
		else
		{
			echo  "error --".$conn->error;
		}
        
    }
	public function fotosInmueble($codInmueble)
    {
        if(($consulta=$this->db->prepare(" select 
		Foto1
		from fotos
        where idInm=?")))
		{  	
			if(!$consulta->bind_param("s",$codInmueble))
			{
				echo "error bind";
			}
			else
			{ 
				$consulta->execute();
//				echo "$qry";
				$arreglo1 = array();
				if($consulta->bind_result($Foto1))
				while($consulta->fetch())
				{
					 $arreglo[0]=$IdInmobiliaria;//0
                     $urlFoto="http://www.simiinmobiliarias.com/mcomercialweb/".$Foto1;
                     
 				}
               // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
               // print_r($arreglo1)."------------------";
                echo $urlFoto;
                
			}
		}
		else
		{
			echo  "error img".$conn->error;
		}
    }
	public function inmueblesDisponibles($IdInmob)
    {
        $condperfil="";
		$ase=$_SESSION['Id_Usuarios'];
		if($_SESSION['IdInmmo']==632)
		{
			$condperfil="and IdPromotor='$ase'";
		}
		$w_conexion = new MySQL();
		$sql="Select count(Codigo_Inmueble) as tot
			   			From datos_call
						where IdInmobiliaria=$IdInmob
						$condperfil";
			
		$res=$w_conexion->ResultSet($sql);
		while($f=$w_conexion->FilaSiguienteArray($res))
		{
			$tot	= $f['tot'];
		}
		return $tot;
		$w_conexion->CerrarConexion();
    }
	public function inmueblesSinFotos($IdInmob)
    {
        $cade="";
		$condperfil="";
		$ase=$_SESSION['Id_Usuarios'];
		if($_SESSION['IdInmmo']==632)
		{
			$condperfil="and IdPromotor='$ase'";
		}
		$arreglo1=array();
		$w_conexion = new MySQL();
		$sql="SELECT i.idInm,i.IdInmobiliaria,f.Foto1  
				FROM inmuebles i 
				LEFT JOIN fotos f ON i.idInm=f.idInm 
				WHERE i.IdInmobiliaria = '".$IdInmob."' 
				AND (f.Foto1 IS NULL OR LENGTH(f.Foto1)=0)  
				AND i.idEstadoinmueble  = 2
				$condperfil";
			
		$res=$w_conexion->ResultSet($sql);
		while($f=$w_conexion->FilaSiguienteArray($res))
		{
			$idInm	= $f['idInm'];
			$cade="'".$idInm."',".$cade;
		}
		return substr($cade,0,-1);
		$w_conexion->CerrarConexion();
    }
	
	public function inmueblesSinVideos($IdInmob)
    {
        $cade="";
		$condperfil="";
		$ase=$_SESSION['Id_Usuarios'];
		if($_SESSION['IdInmmo']==632)
		{
			$condperfil="and IdPromotor='$ase'";
		}
		$arreglo1=array();
		$w_conexion = new MySQL();
		$sql="SELECT idInm 
						FROM inmuebles  
						WHERE IdInmobiliaria = '".$IdInmob."' 
						AND (linkvideo IS NULL OR LENGTH(linkvideo)=0)  
						AND idEstadoinmueble  = 2
						$condperfil";
			
		$res=$w_conexion->ResultSet($sql);
		while($f=$w_conexion->FilaSiguienteArray($res))
		{
			$idInm	= $f['idInm'];
			$cade="'".$idInm."',".$cade;
		}
		return substr($cade,0,-1);
		$w_conexion->CerrarConexion();
    }
	public function inmueblesIncompletos($IdInmob)
    {
        $w_conexion = new MySQL();
		$condperfil="";
		$ase=$_SESSION['Id_Usuarios'];
		if($_SESSION['IdInmmo']==632)
		{
			$condperfil="and IdPromotor='$ase'";
		}
		$sql="Select count(Codigo_Inmueble) as tot
			   			From datos_call
						where IdInmobiliaria=$IdInmob
						$condperfil";
			
		$res=$w_conexion->ResultSet($sql);
		while($f=$w_conexion->FilaSiguienteArray($res))
		{
			$tot	= $f['tot'];
		}
		return $tot;
		$w_conexion->CerrarConexion();
    }
	public function listadoInmuebles2($inmob,$codInmuebles,$nro_inm)
    {
        $cond="";
		if($codInmuebles)
		{
			$cond="and Codigo_Inmueble in ($codInmuebles)";
		}
		if($nro_inm)
		{
			$cond="and Codigo_Inmueble in ('1-".$nro_inm."')";
		}
		$condperfil="";
		$ase=$_SESSION['Id_Usuarios'];
		if($_SESSION['IdInmmo']==632 or $_SESSION['IdInmmo']==631)
		{
			$condperfil="and IdPromotor='$ase'";
		}
		$w_conexion = new MySQL();
		$sql="select 
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo,IdPropietario, Nombrepro,IdBarrios,Ciudad,
		NombreCap,IdPromotor,NombreProm,IdCaptador
		from datos_call
        where IdInmobiliaria=$inmob
        $cond
		$condperfil
		#limit 0,20";
		//echo $sql;
		$res=$w_conexion->ResultSet($sql);
		while($f=$w_conexion->FilaSiguienteArray($res))
		{
			 $arreglo[0]=$f['IdInmobiliaria'];//0
			 $arreglo[1]=$f['Administracion'];//1
			 $arreglo[2]=$f['IdGestion'];//2
			 $arreglo[3]=$f['Estrato'];//3
			 $arreglo[4]=$f['IdTpInm'];//4
			 $arreglo[5]=$f['Codigo_Inmueble'];
			 $arreglo[6]=$f['Tipo_Inmueble'];
			 $arreglo[7]=$f['Venta'];
			 $arreglo[8]=$f['Canon'];
			 $arreglo[9]=$f['descripcionlarga'];
			 $arreglo[10]=$f['Barrio'];
			 $arreglo[11]=$f['Gestion'];
			 $arreglo[12]=$f['AreaConstruida'];
			 $arreglo[13]=$f['AreaLote'];
			 $arreglo[14]=$f['latitud'];
			 $arreglo[15]=$f['longitud'];
			 $arreglo[16]=$f['EdadInmueble'];
			 $arreglo[17]=$f['NombreInmo'];
			 $arreglo[18]="http://www.simiinmobiliarias.com/".str_replace('../','',$f['logo']);
			 $arreglo[19]=$f['IdPropietario'];
			 $arreglo[20]=$f['Nombrepro'];
			 $arreglo[21]=$f['IdBarrios'];
			 $arreglo[22]=$f['Ciudad'];
			 $arreglo[23]=$f['IdCaptador'];
			 $arreglo[24]=$f['NombreCap'];
			 $arreglo[25]=$f['IdPromotor'];
			 $arreglo[26]=$f['NombreProm'];
			// $arreglo[27]=$f['codinterno'];
			 
			 $arreglo1[] = $arreglo;
		}
			return $arreglo1;
			$w_conexion->CerrarConexion();
		
        
    }
    public function inmueblesDemandas($demandas)
    {
    	$condicionInmob="";
		if($_SESSION['IdInmmo']==1 or $_SESSION['IdInmmo']==1000 or $_SESSION['IdInmmo']==10000 or $_SESSION['IdInmmo']==1000000)
		{
			$condicionInmob="";
		}
		else
		{
			$condicionInmob="and i.idInmobiliaria not in (1,1000,10000,100000)";
		}	
    }
 
}
?>