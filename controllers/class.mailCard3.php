<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
//@include("../../funciones/connPDO.php");
  

class MailCard
{
    
	public function getCard($dato)
	{
		$connPDO = new Conexion();
		//echo $dato."<br>";
		$verif=$this->verifyCard($dato);
		//echo $verif." ffff";
		$stmt=$connPDO->prepare("SELECT conse_ml,inmo_ml,host_ml,usr_ml,pass_ml,port_ml,from_ml,est_ml
 								  FROM mail_card
								  WHERE inmo_ml = :inmo");


		if($verif==0)
		{
			$stmt->bindParam(":inmo",$verif);
		}
		else
		{
			$stmt->bindParam(":inmo",$dato);
		}
		
		if($stmt->execute())
		{
			$data=array();
			while ($row = $stmt->fetch()) {
					$data[]=array(
						"host_ml" => $row['host_ml'],
						"usr_ml" => $row['usr_ml'],
						"pass_ml" => $row['pass_ml'],
						"port_ml" => $row['port_ml'],
						"from_ml" => $row['from_ml']	
						);
			}
			return $data;
		}
		else
		{
			return $stmt->errorInfo();
		}
		$stmt=NULL;
	}
	public function verifyCard($dato)
	{
		$connPDO = new Conexion();
		
		$stmt=$connPDO->prepare("SELECT conse_ml,inmo_ml,host_ml,usr_ml,pass_ml,port_ml,from_ml,est_ml
 								  FROM mail_card
								  WHERE inmo_ml = :inmo");
		
		$stmt->bindParam(":inmo",$dato);
		
		
		if($stmt->execute())
		{
			$existe=$stmt->rowCount();
			return $existe;
		}
		else
		{
			return $stmt->errorInfo();
		}
	$stmt=NULL;
	}
	
}
?>