<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include("../funciones/connPDO.php");


class ubicacionMaps
{
    public function __construct($conn=""){
		$this->db=$conn;
	}
    
    private $id_usuario;
    
	public function ubiucacionMaps()
	{
			$connPDO = new Conexion;
			$stmt = $connPDO->prepare("SELECT label_sit,latitud_sit,longitud_sit,
											  direccion_sit,telefono_sit,info_sit,tp_sit,ntp_sit 
									   FROM sitios_interes");
			if($stmt->execute())
			{
				
				$response=array();
				while($row = $stmt->fetch())
						{
							$response[]=array(
								"label_sit"    		=>$row["label_sit"],
								"latitud_sit"  		=>$row["latitud_sit"],
								"longitud_sit" 		=>$row["longitud_sit"],
								"direccion_sit"		=>"Direccion: ".$row["direccion_sit"],
								"telefono_sit" 		=>"Telefono: ".$row["telefono_sit"],
								"info_sit"     		=>"Horario: ".$row["info_sit"],
								"tipo_sit"     		=>getCampo('parametros',"where id_param=49 and conse_param=".$row["tp_sit"],'desc_param').": ".getCampo('parametros',"where id_param=50 and conse_param=".$row["ntp_sit"],'desc_param'),
								"icon"		   		=>'https://simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/img/'.getCampo('parametros',"where id_param=49 and conse_param=".$row["tp_sit"],'mail_param')
								);
						}
						return $response;
			}
			else
			{
				return $stmt->errorInfo();
				$stmt = null;
			}
	}


}
?>    