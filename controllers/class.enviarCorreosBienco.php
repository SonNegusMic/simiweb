<?php

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

/**
 *
 */
class enviarCorreosBienco
{

    public function __construct($connPDO, $connPDOold, $phpmailer)
    {
        $this->pdo = $connPDO;

        $this->connPDOold = $connPDOold;

        $this->phpmailer = $phpmailer;
    }

    public function enviarCorreoBarras($data)
    {
        $anoest     = $data['anio'];
        $mes        = $data['mes'];
        $mesest     = '';
        $cond       = '';
        $htmlCorreo = "";
        if ($mes == 1) {$mesest = 'Enero';}
        if ($mes == 2) {$mesest = 'Febrero';}
        if ($mes == 3) {$mesest = 'Marzo';}
        if ($mes == 4) {$mesest = 'Abril';}
        if ($mes == 5) {$mesest = 'Mayo';}
        if ($mes == 6) {$mesest = 'Junio';}
        if ($mes == 7) {$mesest = 'Julio';}
        if ($mes == 8) {$mesest = 'Agosto';}
        if ($mes == 9) {$mesest = 'Septiembre';}
        if ($mes == 10) {$mesest = 'Octubre';}
        if ($mes == 11) {$mesest = 'Noviembre';}
        if ($mes == 12) {$mesest = 'Diciembre';}

        // $datafactura = $this->facturaArrendatario($dataInfo);

        // echo "<pre>";
        // print_r($datafactura);
        // echo "</pre>";die;

        if (!empty($data['referenciafrom']) || !empty($data['referenciato'])) {
            if (!empty($data['referenciafrom']) && empty($data['referenciato'])) {
                $cond .= " AND b.Referencia BETWEEN " . $data['referenciafrom'] . " AND 9999999999999999999";
            } elseif (empty($data['referenciafrom']) && !empty($data['referenciato'])) {
                $cond .= " AND b.Referencia BETWEEN 0 AND " . $data['referenciato'];
            } elseif (!empty($data['referenciafrom']) && !empty($data['referenciato'])) {
                $cond .= " AND b.Referencia BETWEEN " . $data['referenciafrom'] . " AND " . $data['referenciato'];
            }
        }

        // foreach ($datafactura as $key => $value) {
        $stmt = $this->pdo->prepare("SELECT b.NomInmo,b.Arrendatario,b.Inmueble,b.Referencia,b.Cedula,b.inmobiliaria,a.nombre,a.solicitud,a.correo,c.Nombre AS nominm,c.logo
        	FROM barras b,arrendatario a, clientessimi c
			WHERE b.inmobiliaria=:inmo
			    AND a.nit=b.Cedula
			    AND b.Mes=:mes
			    AND b.Ano=:anioest
			    AND c.IdInmobiliaria = b.inmobiliaria
			    $cond
			    ORDER BY b.Inmueble DESC");
        //limit 120,120000
        $stmt->bindParam(":inmo", $data['inmo']);
        $stmt->bindParam(":mes", $mes);
        $stmt->bindParam(":anioest", $anoest);
        // $stmt->bindParam(":inmueble", $value['inmu']);
        // $stmt->bindParam(":cedula", $value['IdCedula']);
        if ($stmt->execute()) {
            if ($stmt->rowCount() <= 0) {
                $datas['msj'] = "No Hay registros para Esta consulta ";
            } else {

                $ee2 = $stmt->rowCount();

                while ($row = $stmt->fetch()) {

                    $dataInfo = array(
                        "anio"     => $anoest,
                        "mesd"     => $mes,
                        "mes"      => $mesest,
                        "inmo"     => $row['inmobiliaria'],
                        'cedula'   => $row['Cedula'],
                        'inmueble' => $row['Inmueble'],
                    );

                    if (trim($row['correo']) !== "" && $this->validarEmail(trim($row['correo'])) === true || $this->validarVariosEmails(trim($row['correo']))) {

                        $datafactura = $this->facturaArrendatario($dataInfo);

                        $ee1 = count($datafactura);

                        $datas[] = array
                            (
                            "id_estados"   => $row['id_estados'],
                            "inm"          => $row['Inmueble'],
                            "nombre"       => utf8_encode($row['nombre']),
                            "correo"       => $row['correo'],
                            "nominm"       => $row['nominm'],
                            "Cedula"       => $row['Cedula'],
                            "inmobiliaria" => $row['inmobiliaria'],
                            "logo"         => $row['logo'],
                            "idFactura"    => $datafactura['idFactura'],
                            "sucursal"     => $datafactura['sucursal'],
                            "plantillafac" => $datafactura['plantillafac'],
                            "FechaFac"     => $datafactura['Fecha'],
                            "mesParam"     => $mesest,
                            "anoEsta"      => $anoest,
                        );
                        $cuerpo = '
								<!doctype html>
								<html>
								<head>
								<meta charset="UTF-8">
								<title>Untitled Document</title>

								<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
								</head>
								<body style="width:500px;	height:1073px;	margin:0;	padding:0;">
								<tr>
									<td align="right">
								  </td>
								</tr>
							<tr>
							<td align="center">&nbsp;</td>
							</tr>
							<tr>
							  <div align="center"></div>
						    </tr>
							<table width="300" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#009FE3">
					          <tr>
					            <td colspan="3">&nbsp;</td>
					          </tr>
					          <tr>
					            <td width="4">&nbsp;</td>
					            <td width="301" align="center">
					            <table width="530" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
					              <tr>
					                  <td colspan="4" align="LEFT" ><span class="logo" style="width:178px;	height:80px; 	float:left;"><img src="http://web2.siminmueble.com/mcomercialweb/img/logo.jpg" alt=""></span></td>
					              </tr>
					                <tr>
					                  <td width="144" >&nbsp;</td>
					                  <td colspan="2">&nbsp;</td>
					                  <td width="109" >&nbsp;</td>
					              </tr>
					                <tr>
					                  <td >&nbsp;</td>
					                  <td colspan="2" bgcolor="#8EBC02">Sr(a):  ' . utf8_encode($row['nombre']) . '</td>
					                  <td >&nbsp;</td>
					                </tr>
					                <tr>
					                  <td >&nbsp;</td>
					                  <td colspan="2">&nbsp;</td>
					                  <td >&nbsp;</td>
					                </tr>
					                <tr>
					                  <td >&nbsp;</td>
					                  <td colspan="2"><p style="font-family:Myriad Pro;	font-size:14px;	text-align:justify;	margin-top:4px;	color:#000;">Ref : ';
                        if ($ee2 > 0) {$cuerpo = $cuerpo . 'Cup&#243;n de pago';}
                        if ($ee1 > 0 and $ee2 > 0) {$cuerpo = $cuerpo . ' y ';}
                        if ($ee1 > 0) {$cuerpo = $cuerpo . 'Factura';}
                        $cuerpo = $cuerpo . '</p>
					                      <p style="font-family:Myriad Pro;	font-size:14px;	text-align:justify;	margin-top:4px;	color:#000;">Con el fin de mantenerte informado en detalle de los cobros realizados, adjuntamos los enlaces para la consulta de su ';
                        if ($ee2 > 0) {$cuerpo = $cuerpo . 'Cup&#243;n de Pago';}
                        if ($ee1 > 0 and $ee2 > 0) {$cuerpo = $cuerpo . ' y ';}
                        if ($ee1 > 0) {$cuerpo = $cuerpo . 'Factura';}
                        $cuerpo = $cuerpo . '</p></td>
					                  <td >&nbsp;</td>
					                </tr>
					                <tr>
					                  <td >&nbsp;</td>
					                  <td width="158" >&nbsp;</td>
					                  <td width="199" >&nbsp;</td>
					              </tr>';
                        // if ($ee2 == 5555550) {

                            $cuerpo = $cuerpo . '<tr>
					                  <td >&nbsp;</td>
					                  <td colspan="2">Cup&#243;n de Pago:<a target="_blank" href="http://www.simiinmobiliarias.com/facts/m_fact_bienco.php?ref=' . $row['Referencia'] . '"> Descargar</a></td>
					                  <td >&nbsp;</td>
					                </tr>';
                        // }
                        $cuerpo = $cuerpo . '<tr>
					                  <td >&nbsp;</td>
					                  <td colspan="2">Factura :<a target="_blank" href="http://www.simiinmobiliarias.com/mcomercialweb/facturas/facturaBienco.php?cod_estado=' . $datafactura['idFactura'] . '&f_corte=' . $datafactura['Fecha'] . '&codinmo=' . $row['inmobiliaria'] . '&inmu=' . $row['Inmueble'] . '&sucursal=' . $datafactura['sucursal'] . '"> Descargar</a></td>
					                  <td >&nbsp;</td>
					                </tr>
					                <tr>
					                  <td >&nbsp;</td>
					                  <td >&nbsp;</td>
					                  <td >&nbsp;</td>
					                </tr>

					                <tr>
					                  <td >&nbsp;</td>
					                  <td colspan="2">&nbsp;</td>
					                  <td >&nbsp;</td>
					                </tr>
					                <tr>
					                  <td >&nbsp;</td>
					                  <td colspan="2"><p style="text-align:center;	font-family:Arial;	font-style:normal;	font-size:12px;">Consultas 	o inquietudes las resolveremos en <a href="http://www.bienco.com.co/contactenos.php">contactenos</a> o ll&aacute;menos a nuestro Call Center as&iacute;: </p></td>
					                  <td >&nbsp;</td>
					                </tr>
					                <tr>
					                  <td >&nbsp;</td>
					                  <td colspan="2">&nbsp;</td>
					                  <td >&nbsp;</td>
					                </tr>
					                <tr>
					                  <td >&nbsp;</td>
					                  <td colspan="2"><table width="110%" height="100" border="0" align="center" cellspacing="10" bgcolor="#009FE3" >
					                  <tr>
					                        <td width="33%" align="center" bgcolor="#8EBC02"><span ><font color="#FFFFFF" font size="2">BOGOT&Aacute;</font></span></td>
					                        <td width="33%" align="center" bgcolor="#8EBC02"><span ><font color="#FFFFFF" font size="2">CALI</font></span></td>
					                        <td width="34%" align="center" bgcolor="#8EBC02"><span ><font color="#FFFFFF" font size="2">BUCARAMANGA</font></span></td>
					                    </tr>
					                      <tr>
					                        <td align="center"><hr background color=#b0e1f6></hr></td>
					                        <td align="center"><hr background-color:#b0e1f6></hr></td>
					                        <td align="center"><hr background-color:#b0e1f6></hr></td>
					                      <tr>
					                        <td align="center" bgcolor="#009FE3"><span ><font color="#FFFFFF" font size="2">(57+1) 744 8888 </font></span></td>
					                        <td align="center"><span ><font color="#FFFFFF" font size="2">(57+2) 485 5656 </font></span></td>
					                        <td align="center"><span ><font color="#FFFFFF" font size="2">(57+7) 697 1717</font></span></td>
					                    <tr>
					                        <td align="center">&nbsp;</td>
					                        <td align="center" bgcolor="#8EBC02"><span ><font color="#FFFFFF" font size="1">HORARIO DE ATENCI&Oacute;N:</font></span></td>
					                      <td align="center">&nbsp;</td>
					                    <tr>
					                        <td align="center"><span><font color="#FFFFFF" font size="-1">Lunes a Viernes</font></span> </td>
					                        <td colspan="2" align="center"><span><font color="#FFFFFF" font size="-1">8:00 am - 6:00pm </font></span></td>
					                    <tr>
					                        <td align="center"><span ><font color="#FFFFFF" font size="-1">S&aacute;bados 	</font></span></td>
					                      <td colspan="2" align="center"><span ><font color="#FFFFFF" font size="-1">9:00 am - 4:00pm</font></span></td>
					                    <tr>
					                        <td align="center"><span><font color="#FFFFFF" font size="-1">Domingos 	</font></span></td>
					                      <td colspan="2" align="center"><span ><font color="#FFFFFF" font size="-1">9:00 am - 1:00pm</font></span></td>

					                    </table></td>
					                  <td >&nbsp;</td>
					                </tr>
					                <tr>
					                  <td colspan="4">&nbsp;</td>
					                </tr>
					                <tr>
					                  <td colspan="4" bgcolor="#009FE3"><div align="center">
					                    <table width="563" border="0" cellpadding="0" cellspacing="10" >
					                      <tr>
					                        <td width="94" >&nbsp;</td>
					                        <td width="320" >&nbsp;</td>
					                        <td width="109" ><p><font color="#FFFFFF" font size="-1">MA2004/092 Bogot&aacute; MA009/04 Cali MA003/2013 Bucaramanga</font></p>
					                        </td>
					                      </tr>
					                      <tr>
					                        <td >&nbsp;</td>
					                        <td bgcolor="#8EBC02" ><p align="center" ><font color="#FFFFFF" font size="6" text-decoration: none >www.bienco.com.co</font></p></td>
					                        <td >&nbsp;</td>
					                      </tr>
					                      <tr>
					                        <td >&nbsp;</td>
					                        <td >&nbsp;</td>
					                        <td >&nbsp;</td>
					                      </tr>
					                      <tr>
					                        <td colspan="3" ><p align="justify"><font color="#FFFFFF">Recibes este correo por que has hecho una transacici&oacute;n con Blenco S.A. inc o en alguno de nuestros centros/ N&uacute;meros de atenci&oacute;n. este es un mensaje informativo enviado a ' . $row['correo'] . ' por favor no respondas. Preguntas? las resolveremos en <a href="http://www.bienco.com.co/contactenos.php">contactenos</a> o en nuestro Call center </font></p></td>
					                      </tr>
					                      <tr>
					                        <td >&nbsp;</td>
					                        <td >&nbsp;</td>
					                        <td >&nbsp;</td>
					                      </tr>
					                                      </table>
					                  </div></td>
					                </tr>
					            </table></td>
					            <td width="28">&nbsp;</td>
					          </tr>
					          <tr>
					            <td colspan="3"></td>
					          </tr>
					        </table>
							<tr>
							  <p>&nbsp;</p>
							</tr>

							</body>
							</html>';
                        $htmlCorreo .= $cuerpo;
                        $asuntoCabecerac  = "Factura del Inmueble " . $row['Inmueble'];
                        $fromc            = $datafactura['NomInmo'];
                        $mailAc           = $this->phpmailer;
                        $mailAc->SMTPAuth = true;

                        if ($row['inmobiliaria'] == '303') {
                            $mailAc->Host     = "pro.turbo-smtp.com"; //Servidor Bienco
                            $mailAc->Port     = 25;
                            $mailAc->Username = "noreply@bienco.com.co"; //usuario perteneciente al servidor
                            $mailAc->Password = "SKbAiql3";
                            $mailAc->From     = "noreply@bienco.com.co";
                            $mailAc->FromName = "Equipo Bienco";
                        } else {
                            $mailAc->SMTPSecure = "tls";
                            $mailAc->Host       = "smtp.office365.com"; //Servid
                            $mailAc->Port       = 587;
                            $mailAc->Username   = "noreply@simiinmobiliarias.com"; //usuario perteneciente al servidor
                            $mailAc->Password   = "Desarrollo.2015";
                            $mailAc->From       = "noreply@simiinmobiliarias.com";
                            $mailAc->FromName   = $fromc; //"Agente Virtual SimiWeb";
                        }
                        $mailAc->Subject = $asuntoCabecerac; //"Agendamiento de Cita o Actividad $inmueble";

                        $mailAc->MsgHTML($cuerpo);

                        $posComa      = strpos($row['correo'], ',');
                        $posPuntoComa = strpos($row['correo'], ';');
                        $posSlash     = strpos($row['correo'], '/');

                        if ($posComa || $posPuntoComa || $posSlash) {
                            if ($posComa) {
                                $correos = explode(',', $row['correo']);
                            } else if ($posPuntoComa) {
                                $correos = explode(';', $row['correo']);
                            } else if ($posSlash) {
                                $correos = explode('/', $row['correo']);
                            }
                            for ($i = 0; $i < count($correos); $i++) {
                                // $mailAc->AddAddress($correos[$i], $correos[$i]);
                            }
                        } else {
                            // $mailAc->AddAddress($row['correo'], $row['correo']);
                        }
                        if ($row['inmobiliaria'] == '303') {
                            // $mailAc->AddAddress('tecnologia@bienco.com.co', 'tecnologia@bienco.com.co');
                            // $mailAc->AddBCC('desarrolloweb@tae-ltda.com', 'desarrolloweb@tae-ltda.com');
                            // $mailAc->AddAddress('soportesimi8@hotmail.com', 'soportesimi8@hotmail.com');
                        }
                        // $mailAc->AddAddress('desarrolloweb@tae-ltda.com', 'desarrolloweb@tae-ltda.com');
                        // $mailAc->AddAddress('tecnologiabienco@gmail.com', 'tecnologiabienco@gmail.com');
                        $mailAc->AddAddress('desarrolloweb4@tae-ltda.com', 'desarrolloweb4@tae-ltda.com');
                        // $mailAc->AddAddress('desarrolloweb@tae-ltda.com', 'desarrolloweb@tae-ltda.com');
                        $mailAc->SMTPDebug = 2;
                        $mailAc->IsHTML(true);
                        $existeRegistro = $this->existeRegistro($mesest, $anoest, $row['Cedula'], $row['inmobiliaria'], 'A');
                        if ($existeRegistro == 0) {
                            $insertaRegistro = $this->insertaRegistro($mesest, $anoest, $row['Cedula'], $row['inmobiliaria'], 'A', trim($row['correo']));
                        }
                        $existeRegistro1=$this->existeRegistro($mesest,$anoest,$row['Cedula'],$row['inmobiliaria'],'A');
                        if($existeRegistro1==1)
					    {
					    	if ($row['inmobiliaria'] == '303') {
					    		if ($mailAc->Send()) {
					    			$marcaEnvio=$this->marcaEnvio($mesest,$anoest,$row['Cedula'],$row['inmobiliaria'],'A',$row['correo']);
					    		}else{
					    			return "ERROR: " . $mail->ErrorInfo;
					    		}
					    	}else{
					    		if ($mailAc->Send()) {
					    		}else{
					    			return "ERROR: " . $mail->ErrorInfo;
					    		}
					    	}
					    }
                    }
                }
            }
        } else {
            $datas = array(
                "error" => $stmt->errorInfo(),
                "info"  => $data,
            );
        	return $datas;
        }
        return $htmlCorreo;
        // }
    }

    public function facturaArrendatario($data)
    {
        $mes   = $data['mes'];
        $mesd  = $data['mesd'];
        $datas = array();
        if ($mes == 'Enero') {$mesd = 1;}
        if ($mes == 'Febrero') {$mesd = 2;}
        if ($mes == 'Marzo') {$mesd = 3;}
        if ($mes == 'Abril') {$mesd = 4;}
        if ($mes == 'Mayo') {$mesd = 5;}
        if ($mes == 'Junio') {$mesd = 6;}
        if ($mes == 'Julio') {$mesd = 7;}
        if ($mes == 'Agosto') {$mesd = 8;}
        if ($mes == 'Septiembre') {$mesd = 9;}
        if ($mes == 'Octubre') {$mesd = 10;}
        if ($mes == 'Noviembre') {$mesd = 11;}
        if ($mes == 'Diciembre') {$mesd = 12;}
        $fecha_actual = date("Y-m-d");
        // $data['anio'] = 2017;
        $fecha1 = $data['anio'] . "-" . $mesd . "-1";
        $fecha2 = $data['anio'] . "-" . $mesd . "-31";
        $stmt   = $this->pdo->prepare("SELECT f.idFactura,f.sucursal,c.plantillafac,c.logo,a.nombre,c.Nombre as NomInmo,IdCedula,inmu,Fecha
					FROM factura f,arrendatario a,clientessimi c
					WHERE a.`inmo`=f.`NoInm`
					AND c.IdInmobiliaria=f.NoInm
					AND f.`IdCedula`=a.`nit`
					AND f.IdCuenta like '13%'
					AND f.fecha BETWEEN :mes1 AND :mes2
					AND f.NoInm=:inmo
					AND f.inmu=:inmueble
					AND f.IdCedula=:cedula
         			GROUP BY f.idFactura,f.Fecha,f.IdCuenta
					ORDER BY f.idFactura
					LIMIT 0,1
					");
        $stmt->bindParam(':mes1', $fecha1);
        $stmt->bindParam(':mes2', $fecha2);
        $stmt->bindParam(':inmo', $data['inmo']);
        $stmt->bindParam(':cedula', $data['cedula']);
        $stmt->bindParam(':inmueble', $data['inmueble']);
        if ($stmt->execute()) {

            while ($row = $stmt->fetch()) {
                $datas = array(
                    "idFactura"    => $row['idFactura'],
                    "sucursal"     => $row['sucursal'],
                    "plantillafac" => $row['plantillafac'],
                    "Fecha"        => $row['Fecha'],
                    "NomInmo"      => $row['NomInmo'],
                    "nombre"       => $row['nombre'],
                    "inmu"         => $row['inmu'],
                    "IdCedula"     => $row['IdCedula'],
                    "logo"         => str_replace("../", "", $row['logo']),
                );
            }
            return $datas;

        } else {
            $response = array(
                "error" => $stmt->errorInfo(),
                "info"  => $data,
            );
            return $response;
        }
    }

    private function validarEmail($str)
    {
        if (!filter_var($str, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }
    private function validarVariosEmails($str)
    {
        $emailRegEx = '/^(([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+)(([\s]*[;,\/]+[\s]*(([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+))*)$/';

        if (preg_match($emailRegEx, $str)) {
            return true;
        }

        return false;
    }
    private function existeRegistro($mesest, $anoest, $nit, $inmo, $tp)
    {
        $sql = "SELECT nit
        FROM enviocorreo
        WHERE mes=:mesest
        AND ano=:anoest
        AND nit=:nit
        and tipoter=:tp
        and inm=:inmo
        and enviado=0";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':mesest', $mesest);
        $stmt->bindParam(':anoest', $anoest);
        $stmt->bindParam(':tp', $tp);
        $stmt->bindParam(':nit', $nit);
        $stmt->bindParam(':inmo', $inmo);

        if ($stmt->execute()) {
            $info = $stmt->rowCount();
        } else {
            $info = print_r($stmt->errorInfo()) . " error";
        }

        return $info;
    }

    private function insertaRegistro($mesest, $anoest, $nit, $inmo, $tp, $destinatarioc)
    {
        $fecha_actual = date('Y-m-d');
        $conse        = $this->consecutivo('id_enviocorreo', 'enviocorreo');
        $sql          = "INSERT ignore INTO enviocorreo (id_enviocorreo,nit, tipoter, email,fecha,mes,ano,inm,enviado)
    		  VALUES (:conse,:nit, :tp, :destinatarioc,:fecha_actual,:mesest,:anoest,:inmo,0)";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':conse', $conse);
        $stmt->bindParam(':nit', $nit);
        $stmt->bindParam(':tp', $tp);
        $stmt->bindParam(':destinatarioc', $destinatarioc);
        $stmt->bindParam(':fecha_actual', $fecha_actual);
        $stmt->bindParam(':mesest', $mesest);
        $stmt->bindParam(':anoest', $anoest);
        $stmt->bindParam(':inmo', $inmo);

        if ($stmt->execute()) {
            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
    }

    private function marcaEnvio($mesest,$anoest,$nit,$inmo,$tp,$destinatarioc)
    {
        $sql = "update enviocorreo
          set
          enviado =1
          where nit   = :nit
          and tipoter=:tp
          and mes=:mesest
          and ano=:anoest
          and inm=:inmo
          and enviado=0";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':nit', $nit);
        $stmt->bindParam(':tp', $tp);
        $stmt->bindParam(':mesest', $mesest);
        $stmt->bindParam(':anoest', $anoest);
        $stmt->bindParam(':inmo', $inmo);

        if ($stmt->execute()) {
            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }

    }

    private function consecutivo($campo, $tabla)
    {
        $conse = 1;
        $sql   = "select max($campo) as consecutivo
      		from $tabla";

        $stmt = $this->pdo->prepare($sql);

        if ($stmt->execute()) {
            $result      = $stmt->fetch(PDO::FETCH_ASSOC);
            $conse       = $result['consecutivo'];
            $nuevo_conse = $conse + 1;
            return $nuevo_conse;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
    }
}
