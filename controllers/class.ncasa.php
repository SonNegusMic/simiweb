<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

@include("../../funciones/connPDO.php");
class Ncasa
{
    public function __construct($conn=""){
		$this->db=$conn;
		mysqli_query($this->db,"SET NAMES 'utf8'");
	}
    
    private $id_usuario;

    public function infoInmobiliaria($codInmo)
    {
        //echo $codInmu."ff";

		if(($consulta=$this->db->prepare(" select 
		IdInmobiliaria,Nombre,Direccion,Correo, Telefonos
		from clientessimi
        where IdInmobiliaria=?
		")))
		{  	
			$consulta->bind_param("i",$codInmo);
			
				$consulta->execute();
//				echo "$qry";
				
				if($consulta->bind_result($IdInmobiliaria,$Nombre,$Direccion,$Correo, $Telefonos))
				while($consulta->fetch())
				{
					$arreglo[]=array(
					"IdInmobiliaria"	=>$IdInmobiliaria,
                    "Nombre"	=>ucwords(strtolower($Nombre)),
                     "Direccion"		=>$Direccion,
					 "Correo"			=>$Correo,
					 "Telefonos"			=>$Telefonos
					 );
                     
 				}
               // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
               // print_r($arreglo1)."------------------";
                return $arreglo;
		}
		else
		{
			echo  "error --".$conn->error;
		}
        
    }


    public function infoInmueble1($codInmu,$token)
    {
      //  echo $codInmu."ff";
    
		if(($consulta=$this->db->prepare("SELECT 
		IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
		Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,LOWER(descripcionlarga),
		Barrio,Gestion,AreaConstruida,AreaLote,latitud,
		longitud,EdadInmueble,NombreInmo,logo,IdBarrios,NombreProm,IdPromotor,Video
		from datos_call
        where Codigo_Inmueble=?
		")))
		{  	
			$consulta->bind_param("s",$codInmu);
			
				$consulta->execute();
//				echo "$qry";
				
				if($consulta->bind_result($IdInmobiliaria,$Administracion,$IdGestion,$Estrato,$IdTpInm,
		$Codigo_Inmueble,$Tipo_Inmueble,$Venta,$Canon,$descripcionlarga,
		$Barrio,$Gestion,$AreaConstruida,$AreaLote,$latitud,
		$longitud,$EdadInmueble,$NombreInmo,$logo,$IdBarrios,$NombreProm,$IdPromotor,$Video))
				while($consulta->fetch())
				{
					 if($IdGestion==1)
					 {
					 	$oper='rent';
					 	$precio=$Canon;
					 }
					 else
					 {
					 	$oper='sale';
					 	$precio=$Venta;
					 }

					 if($longitud==0) { $longitud='';}
					 if($latitud==0) { $latitud='';}
					 $cocina = "";
					 $aire = "";
					 $pisos ="";
					 $ciu=getCampo('barrios',"where IdBarrios=$IdBarrios",'IdCiudad');
					 $loc=getCampo('barrios',"where IdBarrios=$IdBarrios",'IdLocalidad');
					 $zon=getCampo('barrios',"where IdBarrios=$IdBarrios",'IdZona');
					 $depto=getCampo('ciudad', "where IdCiudad=$ciu",'IdDepartamento');

					 if($Gestion == 1){//1=arriendo
					 	$mailinmo=getCampo('PublicaPortales',"where IdInmobiliaria=$IdInmobiliaria and IdPortal = 25",'mailrenta');
					 }else{
					 	$mailinmo=getCampo('PublicaPortales',"where IdInmobiliaria=$IdInmobiliaria and IdPortal = 25",'mailventa');
					 }

                     //$mailinmo=getCampo('inmobiliaria',"where IdInmobiliaria=$IdInmobiliaria",'EmailC');
					//variables detalladas
					$aire_acondicionado			= evalua_carac1(31,$Codigo_Inmueble,'Cantidad','aire acondicionado');
					$aire .=($aire_acondicionado==1)?' Si ':'No';
					$cocina_americana			= evalua_carac_exacto1(13,$Codigo_Inmueble,'Cantidad','AMERICANA ');
					
					$integral					= evalua_carac1(13,$Codigo_Inmueble,'Cantidad','integral');
					
					//$tp_cocina					= trae_carac1(13,$Codigo_Inmueble,'Descripcion','');
					$cocina .=($integral==1)?'Integral ':'';
					$cocina .=($cocina_americana==1)?'Americana ':'';
					$estacionamiento			= evalua_carac1('23,37',$Codigo_Inmueble,'Cantidad','parqueadero');
					$vigilancia					= ucwords(strtolower(trae_carac1(24,$Codigo_Inmueble,'Descripcion','horas')));
					$madera						= evalua_carac1(9,$Codigo_Inmueble,'Cantidad','madera');
					$alfombra					= evalua_carac1(9,$Codigo_Inmueble,'Cantidad','alfombra');
					$ceramica					= evalua_carac1(9,$Codigo_Inmueble,'Cantidad','ceramica');
					$porcelanato				= evalua_carac1(9,$Codigo_Inmueble,'Cantidad','porcelanato');
					$marmol						= evalua_carac1(9,$Codigo_Inmueble,'Cantidad','marmol');
					$baldosa					= trae_carac1(9,$Codigo_Inmueble,'hmnc','baldosa');
					$pisos .=($madera==1)?'Madera ':'';
					$pisos .=($alfombra==1)?'Alfombra ':'';
					$pisos .=($ceramica==1)?'Ceramica ':'';
					$pisos .=($porcelanato==1)?'Porcelanato ':'';
					$pisos .=($marmol==1)?'Marmol ':'';
					$pisos .=($baldosa==1)?'Baldosa ':'';
                    $aire					= trae_carac1(31,$Codigo_Inmueble,'hmnc','aire acondicionado',0);
					$closets					= trae_carac1(45,$Codigo_Inmueble,'Cantidad','closet',0);
					$banos   					= trae_carac1(16,$Codigo_Inmueble,'Cantidad',utf8_decode('baño'),0); 
					$alcobas   					= trae_carac1(15,$Codigo_Inmueble,'Cantidad','alcoba',0);
					$cantGaraje					= trae_carac1(37,$Codigo_Inmueble,'Cantidad','parqueadero',0);
                    $vigilancia                 = trae_carac1(24,$Codigo_Inmueble,'hmnc','hora',0);
                    $alarma                 	= trae_carac1(24,$Codigo_Inmueble,'hmnc','alarma',0);
                    $balcon                 	= trae_carac1(10,$Codigo_Inmueble,'hmnc','Barra americana',0);
                    $Barraestiloamerican   		= trae_carac1(13,$Codigo_Inmueble,'hmnc','balcon',0);
                    $Calentador                 = trae_carac1(13,$Codigo_Inmueble,'hmnc','Calentador',0);
                    $Chimenea                 	= trae_carac1(27,$Codigo_Inmueble,'hmnc','Chimenea',0);
                    $CocinaIntegral             = trae_carac1(13,$Codigo_Inmueble,'hmnc','Integral',0);
                    $CocinatipoAmericano        = trae_carac1(13,$Codigo_Inmueble,'hmnc','AMERICANA',0);
                    $DepositoBodega             = trae_carac1(20,$Codigo_Inmueble,'hmnc','deposito',0);
                    $Despensa                 	= trae_carac1(13,$Codigo_Inmueble,'hmnc','Despensa',0);
                    $balcon                 	= trae_carac1(10,$Codigo_Inmueble,'hmnc','balcon',0);
                    $gas						= trae_carac1(13,$Codigo_Inmueble,'hmnc','gas n');
                    $sauna						= evalua_carac1(26,$Codigo_Inmueble,'Cantidad','sauna');
                    $turco						= evalua_carac1(26,$Codigo_Inmueble,'Cantidad','turco');
                    $jacuzzi					= evalua_carac1(16,$Codigo_Inmueble,'Cantidad','jacuz');
                    if($sauna>0 or $turco>0 or $jacuzzi>0)
                    {
                    	$saturja='Sauna Turco Jacuzzi';
                    }
                    $Ascensor                 	= trae_carac1(25,$Codigo_Inmueble,'hmnc','Ascensor',0);
					$CanchaSquash               = trae_carac1(26,$Codigo_Inmueble,'hmnc','Squash',0);
					$CanchadeTenis              = trae_carac1(26,$Codigo_Inmueble,'hmnc','Tennis',0);
					$CanchasDeportivas          = trae_carac2(26,$Codigo_Inmueble,'hmnc','Canchas Deportivas',0);
					$CircuitoCerradoTV          = trae_carac1(24,$Codigo_Inmueble,'hmnc','Circuito',0);
					$Gimnasio                 	= trae_carac1(26,$Codigo_Inmueble,'hmnc','Gimnasio',0);
					$Jardin                 	= trae_carac1(12,$Codigo_Inmueble,'hmnc','Jardin',0);
					$JauladeGolf                = trae_carac1(26,$Codigo_Inmueble,'hmnc','Golf',0);
					$ParqueaderodeVisitantes    = trae_carac1(23,$Codigo_Inmueble,'hmnc','Visitantes',0);
					$Piscina                 	= trae_carac1(26,$Codigo_Inmueble,'hmnc','Piscina',0);
					$PlantaElectrica            = trae_carac1(22,$Codigo_Inmueble,'hmnc','Planta',0);
					$PorteriaRecepcion          = trae_carac1(25,$Codigo_Inmueble,'hmnc','Recepcion',0);
					$SalonComunal               = trae_carac1(26,$Codigo_Inmueble,'hmnc','salon Comunal',0);
					$Terraza                 	= trae_carac1(10,$Codigo_Inmueble,'hmnc','Terraza',0);
					$Vigilancia                 = trae_carac1(24,$Codigo_Inmueble,'hmnc','horas',0);
					$SobreViaPrincipal          = trae_carac1(29,$Codigo_Inmueble,'hmnc','Via Principal',0);
					$Sobreviasecundaria        = trae_carac1(29,$Codigo_Inmueble,'hmnc','via secundaria',0);
					$ZonaCampestre              = trae_carac1(35,$Codigo_Inmueble,'hmnc','camping',0);
					$ZonaComercial             	= trae_carac1(35,$Codigo_Inmueble,'hmnc','comercio y',0);
					$ZonaIndustrial             = trae_carac1(35,$Codigo_Inmueble,'hmnc','INDUSTRIAL',0);
					$ControlIndencios           = trae_carac1(25,$Codigo_Inmueble,'hmnc','incendio',0);
					$SalaComedorIndependiente   = trae_carac1(6,$Codigo_Inmueble,'hmnc','SALA COMEDOR',0);
					$Citofono            = trae_carac1(24,$Codigo_Inmueble,'hmnc','Citofono',0);
					$CuartodeServicio            = trae_carac1(13,$Codigo_Inmueble,'hmnc','cuarto y',0);
					$ZonaResidencial            = trae_carac1(35,$Codigo_Inmueble,'hmnc','residenci',0);
					$Estudio            = trae_carac1(27,$Codigo_Inmueble,'hmnc','Estudio',0);
					$HalldeAlcobas            = trae_carac1(5,$Codigo_Inmueble,'hmnc','hall',0);
					$Patio            = trae_carac1(44,$Codigo_Inmueble,'hmnc','Patio',0);
					$Cocineta            = trae_carac1(13,$Codigo_Inmueble,'hmnc','Cocineta',0);
					$ZonaBBQ            = trae_carac1(26,$Codigo_Inmueble,'hmnc','BBQ',0);
					$Salondeconferencias         = trae_carac1(26,$Codigo_Inmueble,'hmnc','conferencias',0);
					$EdificioInteligente = trae_carac2(28,$Codigo_Inmueble,'hmnc','Edificio Inteligente',0);
					$parq =($cantGaraje==0)?'Garajes':'';
					///validar foto de usuarios.
					 $largo=strlen(getCampo('usuarios',"where Id_Usuarios='".$IdPromotor."'",'Foto'));
					 if($largo<=5)
					 {
					 	$fotoAsesor="http://www.simiinmobiliarias.com/sj/img/def_avatar.gif";
					 }
					 else
					 {
					 	$fotoAsesor="http://www.simiinmobiliarias.com/mcomercialweb/".getCampo('usuarios',"where Id_Usuarios='".$IdPromotor."'",'Foto');	
					 }	
					 if(strlen($EdadInmueble)>3)
					 {
					 	$Edad=date('Y')-$EdadInmueble;
					 }
					 else
					 {
						$Edad=$EdadInmueble;
					 }
					 
					 $urlFoto="http://www.simiinmobiliarias.com/mcomercialweb/";
					 $urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto1');

$foto1=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto1') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto1');

$foto2=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto2') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto2');
$foto3=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto3') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto3');
$foto4=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto4') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto4');
$foto5=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto5') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto5');
$foto6=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto6') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto6');
$foto7=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto7') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto7');
$foto8=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto8') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto8');
$foto9=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto9') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto9');
$foto10=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto10') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto10');
$foto11=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto11') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto11');
$foto12=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto12') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto12');
$foto13=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto13') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto13');
$foto14=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto14') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto14');
$foto15=(getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto15') == "")?"":$urlFoto.getCampo('fotos',"where idInm='$Codigo_Inmueble'",'Foto15');


                        //$arregloc= $this->detallesinmueble($Codigo_Inmueble);
                        //echo substr($arregloc,0,-5)."->";
					 
					 $arreglo[]=array(
					 "access_token"	           =>$token,
                     "id_inmueble"	           =>"",
                     "tituloAnuncio"	       =>utf8_encode(ucwords(strtolower($Tipo_Inmueble))." en ".ucwords(strtolower($Gestion))." ".ucwords(strtolower($Barrio))." ".$Codigo_Inmueble),
                     "descripcionInmueble"	   =>utf8_encode($descripcionlarga),
                     "tipoInmueble"			   =>getCampo('tipoinmuebles',"where idTipoInmueble=$IdTpInm",'hmncasa'),//homologar
                     "tipoAnuncio"		       =>getCampo('gestioncomer',"where IdGestion=$IdGestion",'hmncasag'),//homologar
                     "estadoInmueble"		   => "1",
                     "valorInmueble"		   =>"$precio",
                     "administracion"	       =>"$Administracion",
                     "areaTotal"			   =>$AreaLote,
                     "areaConstruida"	       =>$AreaConstruida,
                     "tipoProyecto" =>"",
            		  "valorInmuebleDesde"=> "",
            		  "valorInmuebleHasta"=> "",
            		  "areaTotalDesde"=> "",
            		  "areaTotalHasta"=> "",
            		  "areaConstruidaDesde"=> "",
            		  "areaConstruidaHasta"=> "",
            		  "tituloP1"=> "",
            		  "imagenP1"=> "",
            		  "descripcionP1"=> "",
            		  "areaTotalP1"=> "",
            		  "habitacionP1"=> "",
            		  'banosP1'=> "",
            'precioP1'=> "",
            'tituloP2'=> "",
            'imagenP2'=> "",
            'descripcionP2'=> "",
            'areaTotalP2'=> "",
            'habitacionP2'=> "",
            'banosP2'=> "",
            'precioP2'=> "",
            'tituloP3'=> "",
            'imagenP3'=> "",
            'descripcionP3'=> "",
            'areaTotalP3'=> "",
            'habitacionP3'=> "",
            'banosP3' => "",
            'precioP3'=> "",
            'tituloP4'=> "",
            'imagenP4'=> "",
            'descripcionP4'=> "",
            'areaTotalP4'=> "",
            'habitacionP4'=> "",
            'banosP4'=> "",
            'precioP4'=> "",
            'tituloP5'=> "",
            'imagenP5'=> "",
            'descripcionP5'=> "",
            'areaTotalP5'=> "",
            'habitacionP5'=> "",
            'banosP5'=> "",
            'precioP5'=> "",
            "etiquetaAnuncio" =>"Nuevo Anuncio",
                     "tiempoConstruido"		   =>$Edad,
                     "piso"			           =>"1",
                     "habitaciones"			   =>$alcobas,
                     "banos"			       =>$banos,
                     "parqueadero"		       =>$cantGaraje,
                     "estadoConservacion"	   =>"Excelente",
                     "estrato"			       =>$Estrato,
                     "pais"			           =>1,
                     "departamento"			   =>utf8_encode (getCampo('departamento',"where IdDepartamento=$depto",'hmncasad')),
					 "ciudad"		           =>utf8_encode (getCampo('ciudad',"where IdCiudad=$ciu",'hmncasac')),
					 "localidad"	           =>utf8_encode (getCampo('localidad',"where idlocalidad=$loc",'descripcion')),
                     "zona"	                   =>utf8_encode (getCampo('zonas',"where IdZona=$zon",'NombreZ')),
                     "zip"	                   =>"",
                     "direccion"				=>"",
                     'barrio'					=> utf8_encode(getCampo('barrios',"where IdBarrios=$IdBarrios",'NombreB')),
                     "longitud"			       =>$longitud,
                     "latitud"			       =>$latitud,
                     "mostrarDireccion"	       =>"1",
                     "AireAcondicionado"	   =>utf8_encode($aire),
					 "Alarma"		           =>$alarma,
                     "Balcon"		           =>$Balcon,
                     "Barraestiloamerican"	   =>$Barraestiloamerican,
                     "Calentador"		       =>$Calentador,
                     "Chimenea"		           =>$Chimenea,
                     "CocinaIntegral"		   =>$CocinaIntegral,
                     "CocinatipoAmericano"	   =>$CocinatipoAmericano,
                     "Concasaprefabricada"	   =>"",
                     "Cuartodeconductores"	   =>"",
                     "DepositoBodega"		   =>utf8_encode($DepositoBodega),
                     "Despensa"		           =>$Despensa,
                     "Instalaciondegas"			=>$gas,
                     "pisobaldosa"				=> $baldosa,
                     "SaunaTurcosacuzzi"		=>$saturja,
                     "Zonadelavanderia"			=>$ropas,
                     "Ascensor"		=>$Ascensor,
                     "CanchaSquash"		=>$CanchaSquash,
                     "CanchadeTenis"		=>$CanchadeTenis,
                     "CanchasDeportivas"		=>$CanchasDeportivas,
                     "CircuitoCerradoTV"		=>$CircuitoCerradoTV,
                     "EnCondominio"		=>$EnCondominio,
                     "EnConjuntoCerrado"		=>$EnConjuntoCerrado,
                     "Garajes"		=>$Garajes,
                     "Gimnasio"		=>$Gimnasio,
                     "Jardin"		=>$Jardin,
                     "JauladeGolf"		=>$JauladeGolf,
                     "OficinadeNegocio"		=>$OficinadeNegocio,
                     "ParqueaderodeVisitantes"		=>$ParqueaderodeVisitantes,
                     "Piscina"		=>$Piscina,
                     "PlantaElectrica"		=>$PlantaElectrica,
                     "PorteriaRecepcion"		=>utf8_encode($PorteriaRecepcion),
                     "SaladeInternet"		=>$SaladeInternet,
                     "SalonComunal"		=>$SalonComunal,
                     "Terraza"		=>$Terraza,
                     "Vigilancia"		=>$Vigilancia,
                     "VistaPanoramica"		=>$VistaPanoramica,
                     "ViviendaBiFamiliar"		=>$ViviendaBiFamiliar,
                     "ViviendaMultiFamiliar"		=>$ViviendaMultiFamiliar,
                     "ViviendaBiFamiliar"		=>$ViviendaBiFamiliar,
                     "ZonaInfantil"		=>$ZonaInfantil,
                     "ZonasVerdes"		=>$ZonasVerdes,
                     "ColegioUniversidades"		=>$ColegioUniversidades,
                     "Parques"		=> $Parques,
                     "SobreViaPrincipal"		=>$SobreViaPrincipal,
                     "SupermercadosCComerciales"		=>$SupermercadosCComerciales,
                     "TransportePublicoCercano"		=>$TransportePublicoCercano,
                     "ZonaCampestre"		=>$ZonaCampestre,
                     "ZonaComercial"		=>$ZonaComercial,
                     "ZonaIndustrial"		=>$ZonaIndustrial,
                     "ControlIndencios"		=>$ControlIndencios,
                     "SalaComedorIndependiente"		=>$SalaComedorIndependiente,
                     "Amoblado"		=>$Amoblado,
                     "BanoAuxiliar"		=>$BanoAuxiliar,
                     "Citofono"		=> utf8_encode($Citofono),
                     "CuartodeServicio"		=>utf8_encode($CuartodeServicio),
                     "ZonaResidencial"		=>utf8_encode($ZonaResidencial),
                     "Estudio"		=>$Estudio,
                     "Hall de Alcobas"		=>utf8_encode($HalldeAlcobas),
                     "Patio"		=>$Patio,
                     "Cocineta"		=>$Cocineta,
                     "Caldera"		=>$Caldera,
                     "ZonaBBQ"		=>$ZonaBBQ,
                     "Salondeconferencias"		=>$Salóndeconferencias,
                     "EdificioInteligente"		=>$EdificioInteligente,
                     "Sobreviasecundaria"		=>$Sobreviasecundaria,
                     "GarajeCubierto"		=>$GarajeCubierto,
                     "Salondeconferencias"		=>$Salondeconferencias,
                     "AreaRural"		=>$AreaRural,
                     "AreaUrbana"		=>$AreaUrbana,
                     "dotrasCaracteristicas"		=>'',
                     "fuenteVideo"		=>"YouTube",
                     "idvideo"		=>$Video,
                     "correoAsesor"		=>ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$IdPromotor."'",'Correo'))),
                     "nombreAsesor"		=>utf8_encode(ucwords(substr(strtolower($NombreProm),0,49))),
                     "celularAsesor"		=>ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$IdPromotor."'",'Celular'))),
                     "estado"		=>"Publicado",
                     "user_sesion"		=>$mailinmo,
                     'ruta'=>  $foto1 = (strpos($foto1, ' '))?str_replace(' ', "%20", $foto1):$foto1,
                     'idProveedor'=> "TAE",
                     'id_inmueble_proveedor'=> "$Codigo_Inmueble",
                     'mejora'=> 4,
                     "imagen"                  => $foto1 = (strpos($foto1, ' '))?str_replace(' ', "%20", $foto1):$foto1,
                     "imagen2"                 =>$foto2 = (strpos($foto2, ' '))?str_replace(' ', "%20", $foto2):$foto2,
                     "imagen3"                 =>utf8_decode($foto3 = (strpos($foto3, ' '))?str_replace(' ', "%20", $foto3):$foto3),
                     "imagen4"                 =>$foto4 = (strpos($foto4, ' '))?str_replace(' ', "%20", $foto4):$foto4,
                     "imagen5"                 =>$foto5 = (strpos($foto5, ' '))?str_replace(' ', "%20", $foto5):$foto5,
                     "imagen6"                 =>$foto6 = (strpos($foto6, ' '))?str_replace(' ', "%20", $foto6):$foto6,
                     "imagen7"                 =>$foto7 = (strpos($foto7, ' '))?str_replace(' ', "%20", $foto7):$foto7,
                     "imagen8"                 =>$foto8 = (strpos($foto8, ' '))?str_replace(' ', "%20", $foto8):$foto8,
                     "imagen9"                 =>$foto9 = (strpos($foto9, ' '))?str_replace(' ', "%20", $foto9):$foto9,
                     "imagen10"                =>$foto10 = (strpos($foto10, ' '))?str_replace(' ', "%20", $foto10):$foto10,
                     "imagen11"                =>$foto11 = (strpos($foto11, ' '))?str_replace(' ', "%20", $foto11):$foto11,
                     "imagen12"                =>$foto12 = (strpos($foto12, ' '))?str_replace(' ', "%20", $foto12):$foto12,
                     "imagen13"                =>$foto13 = (strpos($foto13, ' '))?str_replace(' ', "%20", $foto13):$foto13,
                     "imagen14"                =>$foto14 = (strpos($foto14, ' '))?str_replace(' ', "%20", $foto14):$foto14,
                     "imagen15"                =>$foto15 = (strpos($foto15, ' '))?str_replace(' ', "%20", $foto15):$foto15,
					 );
                     
 				}
               // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
               // print_r($arreglo1)."------------------";
                return $arreglo;
		}
		else
		{
			echo  "error --".$conn->error;
		}
        
    }

    public function fotosInmueble($codInmueble)
    {
        $w_conexion = new MySQL();
		$urlFoto="http://www.simiinmobiliarias.com/mcomercialweb/";
        
        $consulta1="select 
		Foto1,Foto2,Foto3,Foto4,Foto5, 
		Foto6,Foto7,Foto8,Foto9,Foto10
		from fotos
        where idInm='$codInmueble'";
		$res=$w_conexion->ResultSet($consulta1);
			// if(!$consulta1->bind_param("s",$codInmueble))
			// {
			// 	echo "error bind";
			// }
			// else
			// { 
				
				while($ff=$w_conexion->FilaSiguienteArray($res))
				{
					 
					for($p=1;$p<15;$p++)
					{	
						 $Foto = $ff['Foto'.$p];
                         if(strlen($Foto)==0)
                         {
                            $urlFoto='';
                         }
						 
						 $arreglo[]=array(
						 		"imagen$p" 	=>$urlFoto.$Foto
						 );
					}
 			//	}
			//print_r( $arreglo);
			 return $arreglo;
		}
	}

	public 	function actualizarPublicacion($idInmueble,$estado,$Rncasa)
	{
		  if(!empty($idInmueble) && !empty($Rncasa))
		  {
		  		 $connPDO = new Conexion();

		    $stmt=$connPDO->prepare("UPDATE inmuebles SET
		                              PublicanCasa=:idestado,
		                              Rncasa=:codAfydi 
		                              WHERE idInm = :idInmueble");
		    if($stmt->execute(array(
		      ":idestado"  => $estado,
		      ":idInmueble"  => $idInmueble,
              ":Rncasa"     =>$Rncasa
		      )))
		    {
		      return 1;
		    } 
		    else
		    {
		      print_r($stmt->errorInfo());
		    } 
		  }
		  else
		  {
		  	return 0;
		  }
	}


	public function getTipoInmuebles()
	{
		$connPDO=new Conexion();
		$stmt=$connPDO->prepare("SELECT idTipoInmueble,Descripcion FROM tipoinmuebles");
		if($stmt->execute())
		{
			$data= array();

			while ($row = $stmt->fetch()) {
				$data[]=array(
					"idTipoInmueble" =>$row["idTipoInmueble"],
					"Descripcion" =>ucwords(strtolower($row["Descripcion"]))
					);
			}
			return $data;

		}
		else
		{
			print_r($stmt->errorInfo());
		}
	}
	public function gestioncomer()
	{
		$connPDO=new Conexion();
		$stmt=$connPDO->prepare("SELECT IdGestion,NombreGestion FROM gestioncomer");
		if($stmt->execute())
		{
			$data= array();

			while ($row = $stmt->fetch()) {
				$data[]=array(
					"IdGestion" =>$row["IdGestion"],
					"NombreGestion" =>ucwords(strtolower($row["NombreGestion"]))
					);
			}
			return $data;

		}
		else
		{
			print_r($stmt->errorInfo());
		}
	}
    	function trae_carac($grupo,$inmueble,$campo,$especifica,$muestra="")
{
	global $w_conexion;
	$cond="";
	if($especifica)
	{
		$cond .=" and Descripcion like '%$especifica%'";
	}
$sqlconsulta="SELECT  $campo as aaa
			 FROM detalleinmueble 
			 INNER JOIN maestrodecaracteristicas 
			 ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica 
			 AND  maestrodecaracteristicas.IdGrupo in ($grupo)
			 INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble' 
			 AND detalleinmueble.idinmueble = inmuebles.idinm
			 $cond";
			 if($muestra==1)
			 {
				echo "<br>".$sqlconsulta."<br>";
			 }
	$res=$w_conexion->ResultSet($sqlconsulta) or die("error ".mysql_error());
	$existe=$w_conexion->FilasAfectadas($res);
	//echo $existe."ff";
	while($fff=$w_conexion->FilaSiguienteArray($res))
	{	//echo "hola en cliclo";
		$idcaracteristica=$fff["aaa"];
	}
	//echo ">> $idcaracteristica --- $campo <br>";
	return $idcaracteristica;
}
function evalua_carac($grupo,$inmueble,$campo,$especifica,$muestra="")
{
	global $w_conexion;
	$cond="";
	if($especifica)
	{
		$cond .=" and Descripcion like '%$especifica%'";
	}
$sqlconsulta="SELECT  $campo as aaa
			 FROM detalleinmueble 
			 INNER JOIN maestrodecaracteristicas 
			 ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica 
			 AND  maestrodecaracteristicas.IdGrupo in ($grupo)
			 INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble' 
			 AND detalleinmueble.idinmueble = inmuebles.idinm
			 $cond";
			 if($muestra==1)
			 {
				echo $sqlconsulta;
			 }
	$res=$w_conexion->ResultSet($sqlconsulta) or die("error ".mysql_error());
	$existe=$w_conexion->FilasAfectadas($res);
	//echo $existe."ff";
	
	
	if($existe>0)
	{
		$idcaracteristica = "1"; 
	}
	else
	{
		$idcaracteristica="0";
	}
//	echo ">> $idcaracteristica --- $campo <br>";
	return $idcaracteristica;
}
    function evalua_carac_exacto($grupo,$inmueble,$campo,$especifica)
    {
    	global $w_conexion;
    	$cond="";
    	if($especifica)
    	{
    		$cond .=" and Descripcion = '$especifica'";
    	}
    $sqlconsulta="SELECT  $campo as aaa
    			 FROM detalleinmueble 
    			 INNER JOIN maestrodecaracteristicas 
    			 ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica 
    			 AND  maestrodecaracteristicas.IdGrupo ='$grupo'
    			 INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble' 
    			 AND detalleinmueble.idinmueble = inmuebles.idinm
    			 $cond";
    	//echo $sqlconsulta;
    	$res=$w_conexion->ResultSet($sqlconsulta) or die("error ".mysql_error());
    	$existe=$w_conexion->FilasAfectadas($res);
    	//echo $existe."ff";
    	
    	
    	if($existe>0)
    	{
    		$idcaracteristica = "1"; 
    	}
    	else
    	{
    		$idcaracteristica="0";
    	}
    //	echo ">> $idcaracteristica --- $campo <br>";
    	return $idcaracteristica;
    }
    public 	function detallesinmueble($idInmueble)
	{
		  if(!empty($idInmueble) )
		  {
		  		 $connPDO = new Conexion();

		    $stmt=$connPDO->prepare("SELECT hmnc FROM maestrodecaracteristicas m, detalleinmueble d
                                    WHERE m.idCaracteristica=d.idcaracteristica
                                    AND d.idinmueble=:idInmueble
                                    AND hmnc!=''");
		    if($stmt->execute(array(
		      ":idInmueble"  => $idInmueble
		      )))
		    {
		      while ($row = $stmt->fetch()) 
              {
                $car= $row['hmnc'];
                $car1=$car.$car1;
              }
              $car1=substr($car1,0,-1);
              return $car1;
		    } 
		    else
		    {
		      print_r($stmt->errorInfo());
		    } 
		  }
		  else
		  {
		  	return 0;
		  }
	}
	public function obtenerCodigoNuestraCasa($codInmu)
	{
		$connPDO=new Conexion();
		
		$stmt=$connPDO->prepare("SELECT RNcasa 
						   FROM inmuebles 
						   where idInm = :codInmu AND
						   RNcasa>0");
		if($stmt->execute(array(
			":codInmu"=>$codInmu
		)))
		{
			
			if($stmt->rowCount()>0)
			{
				
				while ($row = $stmt->fetch()) 
				{
					$codigo=$row["RNcasa"];
				}
				return $codigo;
			}
			else
			{
				return 0;
			}
			

		}
		else
		{
			return $stmt->errorInfo();
		}

	}	
	public function updateCodigoNuestraCasa($codInmu,$cod)
	{
		$connPDO=new Conexion();
		$stmt=$connPDO->prepare("UPDATE inmuebles 
								set RNcasa=:cod
						   where idInm = :codInmu AND
						   RNcasa=0");
		if($stmt->execute(array(
			":codInmu"=>$codInmu,
			":cod"=>$cod
		)))
		{
			
			return 1;

		}
		else
		{
			return $stmt->errorInfo();
		}

	}	
	public function obtenerCodNC($idInmu)
	{
		$connPDO = new Conexion();

		$stmt = $connPDO->prepare("SELECT RNcasa FROM inmuebles 
								   where idInm =  :idInm");

		if($stmt->execute(array(
				":idInm" => $idInmu
			)))
		{
			$stmt->bindColumn(1, $RNcasa); 
			while ($row = $stmt->fetch(PDO::FETCH_BOUND)) {
				return $RNcasa;
			}
		}
		else
		{
			return $stmt->errorInfo();
		}

	}
	public function validarImagenes($uriFoto)
	{
		if($uriFoto != "")
		{
			// $Foto = (strpos($uriFoto, " "))?str_replace(' ', "%20", $uriFoto1):$uriFoto;
            return $Foto;
		}
		else
		{

		}
	}
	
}

?>
