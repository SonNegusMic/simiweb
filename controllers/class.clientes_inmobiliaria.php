<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include "../funciones/connPDO.php";

class Clientes_inmobiliaria
{
    public function __construct($conn = "")
    {
        $this->db = $conn;
    }

    private $id_usuario;
    /*********************************
     *  Logica para autocomplete de creacion de inmueble  *
     *********************************/
    public function obtenerClientes($datas, $camposearch)
    {

        $connPDO   = new Conexion();
        $data      = array();
        $valueTerm = utf8_decode('%' . $datas['term'] . '%');

        //$barrios = $connPDO->prepare("SELECT b.`IdBarrios`,b.`NombreB`,l.`descripcion` AS localidad, z.`NombreZ` AS zona FROM barrios b, localidad l, zonas z WHERE b.`IdLocalidad`=l.`idlocalidad` AND b.`NombreB` like :nomBarrio  AND b.`IdZona`=z.`IdZona`AND b.`IdCiudad`=:idCiudad");
        $stmt = $stmt = $connPDO->prepare("SELECT
          `cedula`,
          `nombre`,
          `telfijo`,
          `telcelular`,
          `email`,
          `idmedio`,
          `ase_cli`,
          `emp_cli`,
          `obser_cli`
        FROM
          clientes_inmobiliaria
        WHERE inmobiliaria=:inmobiliaria
          AND `$camposearch` LIKE :nomBarrio LIMIT 0,50");

        $stmt->bindParam(':nomBarrio', $valueTerm, PDO::PARAM_STR);
        $stmt->bindParam(':inmobiliaria', $_SESSION['IdInmmo']);
        if ($stmt->execute()) {
            // $connPDO->exec("SET NAMES 'utf8'");
            while ($row = $stmt->fetch()) {
                $nomCliente        = ucfirst(strtolower($row['nombre']));
                $cedCliente        = trim($row['cedula']);
                $telfijoCliente    = trim($row['telfijo']);
                $telcelularCliente = trim($row['telcelular']);
                $emailCliente      = trim($row['email']);
                $idmedioCliente    = trim($row['idmedio']);
                $asesor            = getCampo('usuarios', 'where Id_Usuarios = ' . $row['ase_cli'], 'iduser');
                $asesorCliente     = empty($asesor) ? $row['ase_cli'] : $asesor;
                $empresaCliente    = trim($row['emp_cli']);
                $obserCliente      = trim($row['obser_cli']);
                $info              = ucwords(strtolower(utf8_encode($nomCliente)));

                if (!empty($telfijoCliente)) {
                    $info .= "<br><b> Telefono Fijo: </b>" . utf8_encode($telfijoCliente);
                }

                if (!empty($telcelularCliente)) {
                    $info .= "<br><b> Celular: </b>" . utf8_encode($telcelularCliente);
                }

                if (!empty($emailCliente)) {
                    $info .= "<br><b> Correo: </b>" . utf8_encode($emailCliente);
                }

                $data[] = array($row['cedula'], $info, strip_tags($info), utf8_encode($nomCliente), $telfijoCliente, $telcelularCliente, $emailCliente, $idmedioCliente, $asesorCliente, $empresaCliente, $obserCliente);
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());
        }

    }

}
