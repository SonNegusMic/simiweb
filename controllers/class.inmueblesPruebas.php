<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

@include("../funciones/connPDO.php");
class Inmuebles
{
    public function __construct($conn=""){
		$this->db=$conn;
	}
    
    private $id_usuario;



   public function listadoInmueblesConsignados($data)
	{
		$connPDO=new Conexion();
        $cond="";
        $tabla="";
        $enlace="";
        $btnVender="";
        if($data['idInmu'])
        {
            $cond .="  AND 
					( idInm=:codinmu 
					OR RVivaReal=:codinmu 
					OR RLaGuia=:codinmu1
					OR RMtoCuadrado=:codinmu2
					OR RZonaProp=:codinmu3
					OR RMeli =:codinmu4
					Or codinterno=:codinmu5
                    Or codinm=:codinmu6) ";
        }
        if($data['estadoInm'])
        {
            $cond .=" and i.idEstadoinmueble=:estadoInm";
        }
        if($data['gestion'])
        {
            $cond .=" and i.IdGestion=:gestion";
        }
        if($data['tContrato'])
        {
            $cond .=" and i.tipo_contrato=:tContrato";
        }
        if($data['tInmueble'])
        {
            $cond .=" and i.IdTpInm=:tInmueble"; 
        }
        if($data['barrio'])
        {
            $cond .=" and i.IdBarrio=:barrio";
        }
        if($data['ciudad'])
        {
            $cond .=" and b.IdCiudad=:ciudad";
            $tabla=",barrios b";
            $enlace=" and i.IdBarrio=b.IdBarrios ";
        }
        if($data['consignado'])
        {
            $cond .=" and i.IdCaptador=:consignado";
        }
        if($data['ffecha_ini']>0 and $data['ffecha_fin']>0)
        {
            $cond .=" and i.FConsignacion between :fecha_inis and :fecha_fins ";
        }
        
        
        
		$stmt=$connPDO->prepare("SELECT i.idInm, i.Direccion, i.ValorVenta, i.ValorCanon, i.IdGestion,
							i.idEstadoinmueble,i.Administracion, i.FConsignacion, i.restricciones,i.RLaGuia,
							i.RVivaReal,i.RZonaProp,i.RMtoCuadrado,i.Estrato,i.AreaConstruida ,
							i.NumLlaves,i.NumCasillero,i.descripcionlarga,i.PublicaM2,i.PublicaMeli,
							i.PublicaMiCasa,i.PublicaZonaProp,i.PublicaDoomos,i.PublicaLamudi,i.PublicaGuia,
							i.Publicagpt,i.AreaLote,i.EdadInmueble,i.usu_crea,i.RNcasa,i.IdTpInm, i.IdBarrio,
                            i.tipo_contrato,i.idProcedencia,i.RIdnd
							from inmuebles i $tabla
							where IdInmobiliaria=:idinmo ". 
                            $enlace.$cond."
							limit 0,3000");   
        $stmt->bindParam(':idinmo',$_SESSION['IdInmmo']);
        //$stmt->bindParam(':idinmo',$id);
        if($data['idInmu'])
        {
            $stmt->bindParam(':codinmu',$data['idInmu']);
            $stmt->bindParam(':codinmu1',$data['idInmu']);
            $stmt->bindParam(':codinmu2',$data['idInmu']);
            $stmt->bindParam(':codinmu3',$data['idInmu']);
            $stmt->bindParam(':codinmu4',$data['idInmu']);
            $stmt->bindParam(':codinmu5',$data['idInmu']);
            $stmt->bindParam(':codinmu6',$data['idInmu']);
        }
        if($data['ciudad'])
        {
            $stmt->bindParam(':ciudad',$data['ciudad']);
        }
        if($data['estadoInm'])
        {
            $stmt->bindParam(':estadoInm',$data['estadoInm']);
        }
        if($data['gestion'])
        {
            $stmt->bindParam(':gestion',$data['gestion']);
        }
        if($data['tContrato'])
        {
            $stmt->bindParam(':tContrato',$data['tContrato']);
        }
        if($data['tInmueble'])
        {
            $stmt->bindParam(':tInmueble',$data['tInmueble']);
        }
        if($data['barrio'])
        {
            $stmt->bindParam(':barrio',$data['barrio']);
        }
        if($data['consignado'])
        {
            $stmt->bindParam(':consignado',$data['consignado']);
        }
        if($data['ffecha_ini']>0 and $data['ffecha_fin']>0)
        {
            $stmt->bindParam(':fecha_inis',$data['ffecha_ini']);
            $stmt->bindParam(':fecha_fins',$data['ffecha_fin']);
        }
		if($stmt->execute())
		{
			$data=array();
			$connPDO->exec('chartset="utf-8"');
			while ($row = $stmt->fetch()) 
            {
            	$idciud=getCampo('barrios',"where IdBarrios=".$row['IdBarrio'],"IdCiudad");
            	$btnGratuitos='<button type="button" id="gratuitos" class="btn btn-info  gratuitos  btn-xs"  data-toggle="modal"  data-target=".mymodalgratuitos"><span class="glyphicon glyphicon"><i class="fa fa-files-o"></i></span></button>';
            	$btnPagos='<button type="button" id="pagos" class="btn btn-info  pagos  btn-xs"  data-toggle="modal"  data-target=".mymodalpagos"><span class="glyphicon glyphicon"><i class="fa fa-files-o"></i></span></button>';
                $btnficha='<button type="button" id="ficha" class="btn btn-info  ficha  btn-xs"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button>';
                $btnEditar='<button type="button" id="edita" class="btn btn-info  edita  btn-xs"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button>';
                $btnReversar='<button type="button" id="reversar" class="btn btn-info  reversar  btn-xs"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button>';
                $btnVender='<button type="button" id="vender" class="btn btn-info  vender  btn-xs"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button>';
                
                $btnficha='<button type="button" id="ficha" class="btn btn-info  ficha  btn-xs"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button>';

            	$usucrea=ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$row['usu_crea']."' and Id_Usuarios>0",'concat(Nombres," ",apellidos)')));
			    $data[]=array(
                "idInm"       		=> $row['idInm'],
                "bnts" 		        => $btnficha.$btnEditar.$btnReversar.$btnVender.$btnGratuitos,
                "Gestion" 			=> ucwords(strtolower(getCampo('gestioncomer',"where IdGestion=".$row['IdGestion'],"NombresGestion"))),
				"ValorVenta" 		=> number_format($row['ValorVenta']),
				"ValorCanon"       	=> number_format($row['ValorCanon']),
				"TipoInm" 			=> ucwords(strtolower(getCampo('tipoinmuebles',"where idTipoInmueble=".$row['IdTpInm'],"Descripcion"))),
				"Estadoinmueble"    => ucwords(strtolower(getCampo('estado_inmueble',"where idestado=".$row['idEstadoinmueble'],'descripcion'))),
                "Direccion"      	=> $row['Direccion'],
				"Barrio" 			=> ucwords(strtolower(getCampo('barrios',"where IdBarrios=".$row['IdBarrio'],"NombreB"))),
				"ciudad" 			=> ucwords(strtolower(getCampo('ciudad',"where IdCiudad=".$idciud,"Nombre"))),
				
               	"procedencia" 		=> ucwords(strtolower(getCampo('procedencia',"where IdProcedencia=".$row['idProcedencia'],"Nombre"))),  
                "tcontrato" 		=> ucwords(strtolower(getCampo('tipo_contrato',"where id_contrato=".$row['tipo_contrato'],"tipo_contrato"))),
				"FConsignacion" 	=> $row['FConsignacion'],
    //             "Administracion"    => $row['Administracion'],
				// "Estrato"       	=> $row['Estrato'],
				// "AreaConstruida" 	=> $row['AreaConstruida'],
				// "portales"       	=> $row['RLaGuia']." ".$row['RVivaReal']." ".$row['RZonaProp']." ".$row['RMtoCuadrado']." ".$row['PublicaDoomos']." ".$row['PublicaLamudi']." ".$row['Publicagpt'],
    //             "AreaLote"      	=> $row['AreaLote'],
				// "EdadInmueble" 		=> $row['EdadInmueble'],
				"usu_crea"       	=> $usucrea,
				// "RNcasa" 		    => $row['RNcasa'],
				
				
                );
			}
			return $data;
			
		}
		else
		{
			return print_r($stmt->errorInfo());
			
		}
        $stmt=NULL;
	}

   	
}

?>
