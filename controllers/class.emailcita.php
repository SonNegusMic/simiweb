<?php
	// session_start();
	include('../mcomercialweb/PHPMailer_5.2.2/class.phpmailer.php');
	include('../mcomercialweb/PHPMailer_5.2.2/class.smtp.php');
	include("../funciones/myfncs.php");
	include("class.mailCard.php");
	include("../funciones/connPDO.php"); 
	/**
	* Clase para enviar correos
	*/
	class Emailcita
	{

		private $mailCard;
		
		function __construct()
		{
			$this->mailCard = new MailCard();
		}

		/**
		* Metodo para enviar correos
		* Retorna True si se envio el correo, de lo contrario False
		* @param string $from Correo de origen del mensaje
		* @param string $fromName Nombre de quien envia el mensaje
		* @param string $to Para quien va dirigido el correo
		* @param string $cuerpop Cuerpo del mensaje en HTML
		* @param string $subject Asunto del mensaje
		* @return bool
		* 
		*/

		public function sendMail($from, $fromName, $to, $cuerpop = "", $subject){
			$a = $this->mailCard->getCard($_SESSION['IdInmo']);
			foreach($a as $key => $value)
			{}
			$mailA2p = new PHPMailer();
			$mailA2p->IsSMTP();
			$mailA2p->SMTPAuth = true;
			$mailA2p->SMTPSecure = "tls";
			$mailA2p->Host = $value['host_ml']; //Servidor de Correo 
			$mailA2p->Port = $value['port_ml'];
			$mailA2p->Username = $value['usr_ml'];//usuario perteneciente al servidor
			$mailA2p->Password = $value['pass_ml'];
			$mailA2p->From = (empty($from)) ? $value['from_ml'] : $from;
			$mailA2p->FromName = (empty($fromName)) ? "Agente Virtual SimiWeb" : $fromName;
			$mailA2p->Subject = (empty($subject)) ? " " : $subject;
			$mailA2p->MsgHTML($cuerpop);
			$mailA2p->AddAddress($to);
			$mailA2p->SMTPDebug=2;
			$mailA2p->IsHTML(true);
			// echo $mailpropietario;
			if ($mailA2p->Send()) {
				return true;
			}else{
				return false;
			}
			
		}
	}
?>