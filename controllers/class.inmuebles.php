<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

@include "../funciones/connPDO.php";
include("class.mailSender.php");
class Inmuebles
{
    public function __construct($conn = "")
    {
        $this->db = $conn;
    }

    private $id_usuario;
    /*
    public function listadoRecibos($mes,$anio,$inmob)
    {
    $est=1;
    $f1="$anio-$mes-01";
    $f2="$anio-$mes-31";
    if(($consulta=$this->db->prepare("select Nombre,Fecha,idFactura,inmu,Total,IdCedula,sucursal
    from factura
    where NoInm=?
    and Fecha between ? and ?
    ")))

    {
    if(!$consulta->bind_param("iii",$inmob,$mes,$anio))
    {
    echo "error bind";
    }
    else
    {
    $consulta->execute();
    //                echo "$qry";
    $arreglo1 = array();
    if($consulta->bind_result($Referencia,$Arrendatario,$Mes,$Ano,$Inmueble,$Cedula,$IdFactura))
    while($row->fetch())
    {
    $data[]=array(
    "idInm"               => $arreglo1[0],
    "Direccion"          => $arreglo1[1],
    "ValorVenta"         => $arreglo1[2],
    "ValorCanon"           => $arreglo1[3],
    "IdGestion"         => $arreglo1[4],
    "Gestion"             => $arreglo1[5],
    "idprocedencia"         => $arreglo1[6],
    "procedencia"             => $arreglo1[7],
    "idtcontrato"         => $row['tipo_contrato'],
    "tcontrato"             => $arreglo1[8],
    "IdTpInm"             => $row['IdTpInm'],
    "TipoInm"             => $arreglo1[0],
    "IdBarrio"             => $row['IdBarrio'],
    "Barrio"             => $arreglo1[0],
    "ciudad"             => $arreglo1[0],
    "idEstadoinmueble"  => $row['idEstadoinmueble'],
    "Estadoinmueble"    => $arreglo1[0],
    "Administracion"    => $row['Administracion'],
    "FConsignacion"     => $row['FConsignacion'],
    "restricciones"     => $row['restricciones'],
    "RLaGuia"             => $row['RLaGuia'],
    "RVivaReal"           => $row['RVivaReal'],
    "RZonaProp"          => $row['RZonaProp'],
    "RMtoCuadrado"         => $row['RMtoCuadrado'],
    "Estrato"           => $row['Estrato'],
    "AreaConstruida"     => $row['AreaConstruida'],
    "NumLlaves"           => $row['NumLlaves'],
    "NumCasillero"      => $row['NumCasillero'],
    "PublicaM2"           => $row['PublicaM2'],
    "PublicaMeli"         => $row['PublicaMeli'],
    "PublicaMiCasa"     => $row['PublicaMiCasa'],
    "PublicaZonaProp"   => $row['PublicaZonaProp'],
    "PublicaDoomos"     => $row['PublicaDoomos'],
    "PublicaLamudi"     => $row['PublicaLamudi'],
    "PublicaGuia"         => $row['PublicaGuia'],
    "Publicagpt"           => $row['Publicagpt'],
    "AreaLote"          => $row['AreaLote'],
    "EdadInmueble"         => $row['EdadInmueble'],
    "usu_crea"           => $usucrea,
    "RNcasa"             => $row['RNcasa'],
    "bnts"             => $btnGratuitos.$btnPagos.$btnPagos.$btnPagos

    );
    }
    //echo "$inmob,$mes,$anio ------><br>";
    return $data;
    }
    }
    else
    {
    echo  "error lista".$conn->error;
    }

    }
     */
    public function infoInmobiliaria($codInmo)
    {
        //echo $codInmu."ff";

        if (($consulta = $this->db->prepare(" select
        IdInmobiliaria,Nombre,Direccion,Correo, Telefonos
        from clientessimi
        where IdInmobiliaria=?
        "))) {
            $consulta->bind_param("i", $codInmo);

            $consulta->execute();
//                echo "$qry";

            if ($consulta->bind_result($IdInmobiliaria, $Nombre, $Direccion, $Correo, $Telefonos)) {
                while ($consulta->fetch()) {
                    $arreglo[] = array(
                        "IdInmobiliaria" => $IdInmobiliaria,
                        "Nombre"         => ucwords(strtolower($Nombre)),
                        "Direccion"      => $Direccion,
                        "Correo"         => $Correo,
                        "Telefonos"      => $Telefonos,
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            echo "error --" . $conn->error;
        }

    }

    public function infoInmueble($codInmu)
    {
        //echo $codInmu."ff";

        if (($consulta = $this->db->prepare(" select
        IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
        Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
        Barrio,Gestion,AreaConstruida,AreaLote,latitud,
        longitud,EdadInmueble,NombreInmo,logo,IdBarrios,NombreProm,IdPromotor,Video
        from datos_call
        where Codigo_Inmueble=?
        "))) {
            $consulta->bind_param("s", $codInmu);

            $consulta->execute();
//                echo "$qry";

            if ($consulta->bind_result($IdInmobiliaria, $Administracion, $IdGestion, $Estrato, $IdTpInm,
                $Codigo_Inmueble, $Tipo_Inmueble, $Venta, $Canon, $descripcionlarga,
                $Barrio, $Gestion, $AreaConstruida, $AreaLote, $latitud,
                $longitud, $EdadInmueble, $NombreInmo, $logo, $IdBarrios, $NombreProm, $IdPromotor, $Video)) {
                while ($consulta->fetch()) {
                    if ($IdGestion == 1) {
                        $oper   = 'rent';
                        $precio = $Canon;
                    } else {
                        $oper   = 'sale';
                        $precio = $Venta;
                    }
                    $cocina = "";
                    $aire   = "";
                    $pisos  = "";
                    //variables detalladas
                    $aire_acondicionado = evalua_carac1(31, $Codigo_Inmueble, 'Cantidad', 'aire acondicionado');
                    $aire .= ($aire_acondicionado == 1) ? ' Si ' : 'No';
                    $cocina_americana = evalua_carac_exacto1(13, $Codigo_Inmueble, 'Cantidad', 'AMERICANA ');

                    $integral = evalua_carac1(13, $Codigo_Inmueble, 'Cantidad', 'integral');

                    //$tp_cocina                    = trae_carac1(13,$Codigo_Inmueble,'Descripcion','');
                    $cocina .= ($integral == 1) ? 'Integral ' : '';
                    $cocina .= ($cocina_americana == 1) ? 'Americana ' : '';
                    $estacionamiento = evalua_carac1('23,37', $Codigo_Inmueble, 'Cantidad', 'parqueadero');
                    $vigilancia      = ucwords(strtolower(trae_carac1(24, $Codigo_Inmueble, 'Descripcion', 'horas')));
                    $madera          = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'madera');
                    $alfombra        = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'alfombra');
                    $ceramica        = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'ceramica');
                    $porcelanato     = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'porcelanato');
                    $marmol          = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'marmol');
                    $baldosa         = evalua_carac1(9, $Codigo_Inmueble, 'Cantidad', 'baldosa');
                    $pisos .= ($madera == 1) ? 'Madera ' : '';
                    $pisos .= ($alfombra == 1) ? 'Alfombra ' : '';
                    $pisos .= ($ceramica == 1) ? 'Ceramica ' : '';
                    $pisos .= ($porcelanato == 1) ? 'Porcelanato ' : '';
                    $pisos .= ($marmol == 1) ? 'Marmol ' : '';
                    $pisos .= ($baldosa == 1) ? 'Baldosa ' : '';

                    $closets                  = trae_carac1(45, $Codigo_Inmueble, 'Cantidad', 'closet', 0);
                    $banos                    = trae_carac1(16, $Codigo_Inmueble, 'Cantidad', utf8_decode('baño'), 0);
                    $alcobas                  = trae_carac1(15, $Codigo_Inmueble, 'Cantidad', 'alcoba', 0);
                    $cantGaraje               = trae_carac1(37, $Codigo_Inmueble, 'Cantidad', 'parqueadero', 0);
                    $parq                     = ($cantGaraje == 0) ? 'No ' : 'Si ';
                    $Ascensor                 = trae_carac1(25, $Codigo_Inmueble, 'Descripcion', 'Ascensor', 0);
                    $CanchaSquash             = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'Squash', 0);
                    $CanchadeTenis            = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'Tennis', 0);
                    $CanchasDeportivas        = trae_carac2(26, $Codigo_Inmueble, 'Descripcion', 'Canchas Deportivas', 0);
                    $CircuitoCerradoTV        = trae_carac1(24, $Codigo_Inmueble, 'Descripcion', 'Circuito', 0);
                    $Gimnasio                 = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'Gimnasio', 0);
                    $Jardin                   = trae_carac1(12, $Codigo_Inmueble, 'Descripcion', 'Jardin', 0);
                    $JauladeGolf              = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'Golf', 0);
                    $ParqueaderodeVisitantes  = trae_carac1(23, $Codigo_Inmueble, 'Descripcion', 'Visitantes', 0);
                    $Piscina                  = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'Piscina', 0);
                    $PlantaElectrica          = trae_carac1(22, $Codigo_Inmueble, 'Descripcion', 'Planta', 0);
                    $PorteriaRecepcion        = trae_carac1(25, $Codigo_Inmueble, 'Descripcion', 'Recepcion', 0);
                    $SalonComunal             = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'salon Comunal', 0);
                    $Terraza                  = trae_carac1(10, $Codigo_Inmueble, 'Descripcion', 'Terraza', 0);
                    $Vigilancia               = trae_carac1(24, $Codigo_Inmueble, 'Descripcion', 'horas', 0);
                    $SobreViaPrincipal        = trae_carac1(29, $Codigo_Inmueble, 'Cantidad', 'Via Principal', 0);
                    $Sobreviasecundaria       = trae_carac1(29, $Codigo_Inmueble, 'Cantidad', 'via secundaria', 0);
                    $ZonaCampestre            = trae_carac1(35, $Codigo_Inmueble, 'Descripcion', 'camping', 0);
                    $ZonaComercial            = trae_carac1(35, $Codigo_Inmueble, 'Descripcion', 'comercio y', 0);
                    $ZonaIndustrial           = trae_carac1(35, $Codigo_Inmueble, 'Descripcion', 'INDUSTRIAL', 0);
                    $ControlIndencios         = trae_carac1(25, $Codigo_Inmueble, 'Descripcion', 'incendio', 0);
                    $SalaComedorIndependiente = trae_carac1(6, $Codigo_Inmueble, 'Descripcion', 'SALA COMEDOR', 0);
                    $Citofono                 = trae_carac1(24, $Codigo_Inmueble, 'Descripcion', 'Citofono', 0);
                    $CuartodeServicio         = trae_carac1(13, $Codigo_Inmueble, 'Descripcion', 'cuarto y', 0);
                    $ZonaResidencial          = trae_carac1(35, $Codigo_Inmueble, 'Descripcion', 'residenci', 0);
                    $Estudio                  = trae_carac1(27, $Codigo_Inmueble, 'Descripcion', 'Estudio', 0);
                    $HalldeAlcobas            = trae_carac1(5, $Codigo_Inmueble, 'Descripcion', 'hall', 0);
                    $Patio                    = trae_carac1(44, $Codigo_Inmueble, 'Descripcion', 'Patio', 0);
                    $Cocineta                 = trae_carac1(13, $Codigo_Inmueble, 'Descripcion', 'Cocineta', 0);
                    $vestier                  = trae_carac1(45, $Codigo_Inmueble, 'Descripcion', 'vestier', 0);
                    $lavanderia               = trae_carac1(13, $Codigo_Inmueble, 'Descripcion', 'LAVANDERIA', 0);
                    $shut                     = trae_carac1(25, $Codigo_Inmueble, 'Descripcion', 'shut', 0);
                    $ZonaBBQ                  = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'BBQ', 0);
                    $Salondeconferencias      = trae_carac1(26, $Codigo_Inmueble, 'Descripcion', 'conferencias', 0);
                    $EdificioInteligente      = trae_carac2(28, $Codigo_Inmueble, 'Descripcion', 'Edificio Inteligente', 0);
                    ///validar foto de usuarios.
                    $largo = strlen(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Foto'));
                    if ($largo <= 5) {
                        $fotoAsesor = "https://www.simiinmobiliarias.com/sj/img/def_avatar.gif";
                    } else {
                        $fotoAsesor = "https://www.simiinmobiliarias.com/mcomercialweb/" . getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Foto');
                    }

                    $arreglo[] = array(
                        "IdInmobiliaria"           => $IdInmobiliaria,
                        "Administracion"           => number_format($Administracion), //1
                        "IdGestion"                => $IdGestion, //2
                        "Estrato"                  => $Estrato, //3
                        "IdTpInm"                  => $IdTpInm, //4
                        "Codigo_Inmueble"          => $Codigo_Inmueble,
                        "Tipo_Inmueble"            => utf8_encode(ucwords(strtolower($Tipo_Inmueble))),
                        "Venta"                    => number_format($Venta),
                        "Canon"                    => number_format($Canon),
                        "precio"                   => number_format($precio),
                        "descripcionlarga"         => utf8_encode($descripcionlarga),
                        "Barrio"                   => utf8_encode(ucwords(strtolower($Barrio))),
                        "Gestion"                  => utf8_encode(ucwords(strtolower($Gestion))),
                        "AreaConstruida"           => number_format($AreaConstruida),
                        "AreaLote"                 => number_format($AreaLote),
                        "latitud"                  => $latitud,
                        "longitud"                 => $longitud,
                        "EdadInmueble"             => $EdadInmueble,
                        "NombreInmo"               => utf8_encode(ucwords(strtolower($NombreInmo))),
                        "ciudad"                   => utf8_encode(ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre')))),
                        "CorreoAsesor"             => utf8_encode(ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Correo')))),
                        "TelAsesor"                => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Telefono'))),
                        "CelAsesor"                => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Celular'))),
                        "FotoAsesor"               => $fotoAsesor,
                        "depto"                    => utf8_encode(ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre')))),
                        "oper"                     => utf8_encode($oper),
                        "foto1"                    => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos', "where idInm='" . $Codigo_Inmueble . "'", 'Foto1')),
                        "totaleregistros"          => $total,
                        "NombreProm"               => utf8_encode(ucwords(strtolower($NombreProm))),
                        "parqueadero"              => $parq,
                        "cocina"                   => $cocina,
                        "aire"                     => $aire,
                        "vigilancia"               => $vigilancia,
                        "pisos"                    => $pisos,
                        "closets"                  => $closets,
                        "banos"                    => $banos,
                        "alcobas"                  => $alcobas,
                        "closets"                  => $closets,
                        "vigilancia"               => $vigilancia,
                        "alarma"                   => $alarma,
                        "balcon"                   => $balcon,
                        "Barraestiloamerican"      => $Barraestiloamerican,
                        "Calentador"               => $Calentador,
                        "Chimenea"                 => $Chimenea,
                        "CocinaIntegral"           => $CocinaIntegral,
                        "CocinatipoAmericano"      => $CocinatipoAmericano,
                        "DepositoBodega"           => $DepositoBodega,
                        "Despensa"                 => $Despensa,
                        "balcon"                   => $balcon,
                        "gas"                      => $gas,
                        "sauna"                    => $sauna,
                        "turco"                    => $turco,
                        "jacuzzi"                  => $jacuzzi,
                        "Ascensor"                 => $Ascensor,
                        "CanchaSquash"             => $CanchaSquash,
                        "CanchadeTenis"            => $CanchadeTenis,
                        "CanchasDeportivas"        => $CanchasDeportivas,
                        "CircuitoCerradoTV"        => $CircuitoCerradoTV,
                        "Gimnasio"                 => $Gimnasio,
                        "Jardin"                   => $Jardin,
                        "JauladeGolf"              => $JauladeGolf,
                        "ParqueaderodeVisitantes"  => $ParqueaderodeVisitantes,
                        "Piscina"                  => $Piscina,
                        "PlantaElectrica"          => $PlantaElectrica,
                        "PorteriaRecepcion"        => $PorteriaRecepcion,
                        "SalonComunal"             => $SalonComunal,
                        "Terraza"                  => $Terraza,
                        "Vigilancia"               => $Vigilancia,
                        "SobreViaPrincipal"        => $SobreViaPrincipal,
                        "Sobreviasecundaria"       => $Sobreviasecundaria,
                        "ZonaCampestre"            => $ZonaCampestre,
                        "ZonaComercial"            => $ZonaComercial,
                        "ZonaIndustrial"           => $ZonaIndustrial,
                        "ControlIndencios"         => $ControlIndencios,
                        "SalaComedorIndependiente" => $SalaComedorIndependiente,
                        "Citofono"                 => $Citofono,
                        "CuartodeServicio"         => $CuartodeServicio,
                        "ZonaResidencial"          => $ZonaResidencial,
                        "Estudio"                  => $Estudio,
                        "HalldeAlcobas"            => $HalldeAlcobas,
                        "Patio"                    => $Patio,
                        "Cocineta"                 => $Cocineta,
                        "ZonaBBQ"                  => $ZonaBBQ,
                        "Video"                    => utf8_encode($Video),
                        "Salondeconferencias"      => $Salondeconferencias,
                        "EdificioInteligente"      => $EdificioInteligente,
                        "logo"                     => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $logo),
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            return $conn->error;
        }

    }
    public function infoInmueble2($codInmu, $idinmob)
    {
        //echo $codInmu."ff";

        if (($consulta = $this->db->prepare(" select
        idInm,codinm,IdInmobiliaria,IdGestion,IdTpInm,
        IdBarrio,banos,alcobas,garaje,Direccion,
        DirMetro,ValorVenta,ValorCanon,AdmonIncluida,Administracion,
        ValorIva,admondto,Estrato,AreaConstruida,AreaLote,EdadInmueble,chip,
        NoMatricula,CedulaCatastral,idProcedencia,idEstadoinmueble,FConsignacion,
        AvaluoCatastral,FechaAvaluoComercial,Carrera,Calle,
        ComiVenta,ComiArren,IdDestinacion,descripcionlarga,descripcionMetro,
        fingreso,fmodificacion,latitud,longitud,escritura_inmu,
        restricciones,obserinterna,ValComiVenta,ValComiArr,f_creacion,
        h_creacion,usu_creacion,permite_geo,FechaAvaluoCatastral,ValorAvaluoComercial,
        migrado,politica_comp,usu_crea,codinterno,flagm2
        from inmnvo
        where codinm=?
        and IdInmobiliaria=?
        "))) {
            $consulta->bind_param("ii", $codInmu, $idinmob);

            $consulta->execute();
//                echo "$qry";

            if ($consulta->bind_result($idInm, $codinm, $IdInmobiliaria, $IdGestion, $IdTpInm,
                $IdBarrio, $banos, $alcobas, $garaje, $Direccion,
                $DirMetro, $ValorVenta, $ValorCanon, $AdmonIncluida, $Administracion,
                $ValorIva, $admondto, $Estrato, $AreaConstruida, $AreaLote, $EdadInmueble, $chip,
                $NoMatricula, $CedulaCatastral, $idProcedencia, $idEstadoinmueble, $FConsignacion,
                $AvaluoCatastral, $FechaAvaluoComercial, $Carrera, $Calle,
                $ComiVenta, $ComiArren, $IdDestinacion, $descripcionlarga, $descripcionMetro,
                $fingreso, $fmodificacion, $latitud, $longitud, $escritura_inmu,
                $restricciones, $obserinterna, $ValComiVenta, $ValComiArr, $f_creacion,
                $h_creacion, $usu_creacion, $permite_geo, $FechaAvaluoCatastral, $ValorAvaluoComercial,
                $migrado, $politica_comp, $usu_crea, $codinterno, $flagm2)) {
                while ($consulta->fetch()) {
                    if ($IdGestion == 1) {
                        $oper   = 'rent';
                        $precio = $ValorCanon;
                    } else {
                        $oper   = 'sale';
                        $precio = $ValorVenta;
                    }
                    $cocina = "";
                    $aire   = "";
                    $pisos  = "";
                    //variables detalladas
                    $aire_acondicionado = evalua_carac1n(31, $codinm_fto, $IdInmobiliaria, 'Cantidad', 'aire acondicionado');
                    $aire .= ($aire_acondicionado == 1) ? ' Si ' : 'No';
                    $cocina_americana = evalua_carac_exacto1n(13, $codinm, $IdInmobiliaria, 'Cantidad', 'AMERICANA ');

                    $integral = evalua_carac1n(13, $codinm, $IdInmobiliaria, 'Cantidad', 'integral');

                    //$tp_cocina                    = trae_carac1(13,$codinm,$IdInmobiliaria,'Descripcion','');
                    $cocina .= ($integral == 1) ? 'Integral ' : '';
                    $cocina .= ($cocina_americana == 1) ? 'Americana ' : '';
                    $estacionamiento = evalua_carac1n('23,37', $idInm, $IdInmobiliaria, 'Cantidad', 'parqueadero');
                    $vigilancia      = ucwords(strtolower(trae_carac1(24, $idInm, $IdInmobiliaria, 'Descripcion', 'horas')));
                    $madera          = evalua_carac1n(9, $codinm, $IdInmobiliaria, 'Cantidad', 'madera');
                    $alfombra        = evalua_carac1n(9, $codinm, $IdInmobiliaria, 'Cantidad', 'alfombra');
                    $ceramica        = evalua_carac1n(9, $codinm, $IdInmobiliaria, 'Cantidad', 'ceramica');
                    $porcelanato     = evalua_carac1n(9, $codinm, $IdInmobiliaria, 'Cantidad', 'porcelanato');
                    $marmol          = evalua_carac1n(9, $codinm, $IdInmobiliaria, 'Cantidad', 'marmol');
                    $baldosa         = evalua_carac1n(9, $codinm, $IdInmobiliaria, 'Cantidad', 'baldosa');
                    $pisos .= ($madera == 1) ? 'Madera ' : '';
                    $pisos .= ($alfombra == 1) ? 'Alfombra ' : '';
                    $pisos .= ($ceramica == 1) ? 'Ceramica ' : '';
                    $pisos .= ($porcelanato == 1) ? 'Porcelanato ' : '';
                    $pisos .= ($marmol == 1) ? 'Marmol ' : '';
                    $pisos .= ($baldosa == 1) ? 'Baldosa ' : '';

                    $closets                  = trae_carac1n(45, $codinm, $IdInmobiliaria, 'Cantidad', 'closet', 0);
                    $banos                    = $banos;
                    $alcobas                  = $alcobas;
                    $cantGaraje               = $garaje;
                    $parq                     = ($cantGaraje == 0) ? 'No ' : 'Si ';
                    $closets                  = trae_carac1n(45, $codinm, $IdInmobiliaria, 'Cantidad', 'closet', 0);
                    $banos                    = trae_carac1n(16, $codinm, $IdInmobiliaria, 'Cantidad', utf8_decode('baño'), 0);
                    $alcobas                  = trae_carac1n(15, $codinm, $IdInmobiliaria, 'Cantidad', 'alcoba', 0);
                    $cantGaraje               = trae_carac1n(37, $codinm, $IdInmobiliaria, 'Cantidad', 'parqueadero', 0);
                    $Ascensor                 = trae_carac1n(25, $codinm, $IdInmobiliaria, 'Descripcion', 'Ascensor', 0);
                    $CanchaSquash             = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'Squash', 0);
                    $CanchadeTenis            = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'Tennis', 0);
                    $CanchasDeportivas        = trae_carac2n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'Canchas Deportivas', 0);
                    $CircuitoCerradoTV        = trae_carac1n(24, $codinm, $IdInmobiliaria, 'Descripcion', 'Circuito', 0);
                    $Gimnasio                 = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'Gimnasio', 0);
                    $Jardin                   = trae_carac1n(12, $codinm, $IdInmobiliaria, 'Descripcion', 'Jardin', 0);
                    $JauladeGolf              = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'Golf', 0);
                    $ParqueaderodeVisitantes  = trae_carac1n(23, $codinm, $IdInmobiliaria, 'Descripcion', 'Visitantes', 0);
                    $Piscina                  = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'Piscina', 0);
                    $PlantaElectrica          = trae_carac1n(22, $codinm, $IdInmobiliaria, 'Descripcion', 'Planta', 0);
                    $PorteriaRecepcion        = trae_carac1n(25, $codinm, $IdInmobiliaria, 'Descripcion', 'Recepcion', 0);
                    $SalonComunal             = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'salon Comunal', 0);
                    $Terraza                  = trae_carac1n(10, $codinm, $IdInmobiliaria, 'Descripcion', 'Terraza', 0);
                    $Vigilancia               = trae_carac1n(24, $codinm, $IdInmobiliaria, 'Descripcion', 'horas', 0);
                    $SobreViaPrincipal        = trae_carac1n(29, $codinm, $IdInmobiliaria, 'Cantidad', 'Via Principal', 0);
                    $Sobreviasecundaria       = trae_carac1n(29, $codinm, $IdInmobiliaria, 'Cantidad', 'via secundaria', 0);
                    $ZonaCampestre            = trae_carac1n(35, $codinm, $IdInmobiliaria, 'Descripcion', 'camping', 0);
                    $ZonaComercial            = trae_carac1n(35, $codinm, $IdInmobiliaria, 'Descripcion', 'comercio y', 0);
                    $ZonaIndustrial           = trae_carac1n(35, $codinm, $IdInmobiliaria, 'Descripcion', 'INDUSTRIAL', 0);
                    $ControlIndencios         = trae_carac1n(25, $codinm, $IdInmobiliaria, 'Descripcion', 'incendio', 0);
                    $SalaComedorIndependiente = trae_carac1n(6, $codinm, $IdInmobiliaria, 'Descripcion', 'SALA COMEDOR', 0);
                    $Citofono                 = trae_carac1n(24, $codinm, $IdInmobiliaria, 'Descripcion', 'Citofono', 0);
                    $CuartodeServicio         = trae_carac1n(13, $codinm, $IdInmobiliaria, 'Descripcion', 'cuarto y', 0);
                    $ZonaResidencial          = trae_carac1n(35, $codinm, $IdInmobiliaria, 'Descripcion', 'residenci', 0);
                    $Estudio                  = trae_carac1n(27, $codinm, $IdInmobiliaria, 'Descripcion', 'Estudio', 0);
                    $HalldeAlcobas            = trae_carac1n(5, $codinm, $IdInmobiliaria, 'Descripcion', 'hall', 0);
                    $Patio                    = trae_carac1n(44, $codinm, $IdInmobiliaria, 'Descripcion', 'Patio', 0);
                    $Cocineta                 = trae_carac1n(13, $codinm, $IdInmobiliaria, 'Descripcion', 'Cocineta', 0);
                    $vestier                  = trae_carac1n(45, $codinm, $IdInmobiliaria, 'Descripcion', 'vestier', 0);
                    $lavanderia               = trae_carac1n(13, $codinm, $IdInmobiliaria, 'Descripcion', 'LAVANDERIA', 0);
                    $shut                     = trae_carac1n(25, $codinm, $IdInmobiliaria, 'Descripcion', 'shut', 0);
                    $ZonaBBQ                  = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'BBQ', 0);
                    $Salondeconferencias      = trae_carac1n(26, $codinm, $IdInmobiliaria, 'Descripcion', 'conferencias', 0);
                    $EdificioInteligente      = trae_carac2n(28, $codinm, $IdInmobiliaria, 'Descripcion', 'Edificio Inteligente', 0);
                    ///validar foto de usuarios.
                    $largo = strlen(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Foto'));
                    if ($largo <= 5) {
                        $fotoAsesor = "https://www.simiinmobiliarias.com/sj/img/def_avatar.gif";
                    } else {
                        $fotoAsesor = "https://www.simiinmobiliarias.com/mcomercialweb/" . getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Foto');
                    }

                    $arreglo[] = array(
                        "IdInmobiliaria"           => $IdInmobiliaria,
                        "Administracion"           => number_format($Administracion), //1
                        "IdGestion"                => $IdGestion, //2
                        "Estrato"                  => $Estrato, //3
                        "IdTpInm"                  => $IdTpInm, //4
                        "Codigo_Inmueble"          => $idInm,
                        "Tipo_Inmueble"            => utf8_encode(ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble=$IdTpInm", 'Descripcion')))),
                        "Venta"                    => number_format($ValorVenta),
                        "Canon"                    => number_format($ValorCanon),
                        "precio"                   => number_format($precio),
                        "descripcionlarga"         => utf8_encode($descripcionlarga),
                        "Barrio"                   => utf8_encode(ucwords(strtolower(getCampo('barrios', "where IdBarrios=$IdBarrio", 'NombreB')))),
                        "Gestion"                  => utf8_encode(ucwords(strtolower(getCampo('gestioncomer', "where IdGestion=$IdGestion", 'NombresGestion')))),
                        "AreaConstruida"           => number_format($AreaConstruida),
                        "AreaLote"                 => number_format($AreaLote),
                        "latitud"                  => $latitud,
                        "longitud"                 => $longitud,
                        "EdadInmueble"             => $EdadInmueble,
                        "NombreInmo"               => utf8_encode(ucwords(strtolower(getCampo('clientessimi', "where IdInmobiliaria=$IdInmobiliaria", 'Nombre')))),
                        "ciudad"                   => utf8_encode(ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrio", 'IdCiudad'), 'Nombre')))),
                        "CorreoAsesor"             => utf8_encode(ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Correo')))),
                        "TelAsesor"                => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Telefono'))),
                        "CelAsesor"                => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Celular'))),
                        "FotoAsesor"               => $fotoAsesor,
                        "depto"                    => utf8_encode(ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrio", 'IdCiudad'), 'IdDepartamento'), 'Nombre')))),
                        "oper"                     => utf8_encode($oper),
                        "foto1"                    => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos_nvo', "where aa_fto='" . $IdInmobiliaria . "' and codinm_fto=$codinm order by posi_ft limit 0,1", 'foto')),
                        "NombreProm"               => utf8_encode(ucwords(strtolower($NombreProm))),
                        "parqueadero"              => $parq,
                        "cocina"                   => $cocina,
                        "aire"                     => $aire,
                        "vigilancia"               => $vigilancia,
                        "pisos"                    => $pisos,
                        "closets"                  => $closets,
                        "banos"                    => $banos,
                        "alcobas"                  => $alcobas,
                        "vigilancia"               => $vigilancia,
                        "alarma"                   => $alarma,
                        "balcon"                   => $balcon,
                        "Barraestiloamerican"      => $Barraestiloamerican,
                        "Calentador"               => $Calentador,
                        "Chimenea"                 => $Chimenea,
                        "CocinaIntegral"           => $CocinaIntegral,
                        "CocinatipoAmericano"      => $CocinatipoAmericano,
                        "DepositoBodega"           => $DepositoBodega,
                        "Despensa"                 => $Despensa,
                        "balcon"                   => $balcon,
                        "gas"                      => $gas,
                        "sauna"                    => $sauna,
                        "turco"                    => $turco,
                        "jacuzzi"                  => $jacuzzi,
                        "Ascensor"                 => $Ascensor,
                        "CanchaSquash"             => $CanchaSquash,
                        "CanchadeTenis"            => $CanchadeTenis,
                        "CanchasDeportivas"        => $CanchasDeportivas,
                        "CircuitoCerradoTV"        => $CircuitoCerradoTV,
                        "Gimnasio"                 => $Gimnasio,
                        "Jardin"                   => $Jardin,
                        "JauladeGolf"              => $JauladeGolf,
                        "ParqueaderodeVisitantes"  => $ParqueaderodeVisitantes,
                        "Piscina"                  => $Piscina,
                        "PlantaElectrica"          => $PlantaElectrica,
                        "PorteriaRecepcion"        => $PorteriaRecepcion,
                        "SalonComunal"             => $SalonComunal,
                        "Terraza"                  => $Terraza,
                        "Vigilancia"               => $Vigilancia,
                        "SobreViaPrincipal"        => $SobreViaPrincipal,
                        "Sobreviasecundaria"       => $Sobreviasecundaria,
                        "ZonaCampestre"            => $ZonaCampestre,
                        "ZonaComercial"            => $ZonaComercial,
                        "ZonaIndustrial"           => $ZonaIndustrial,
                        "ControlIndencios"         => $ControlIndencios,
                        "SalaComedorIndependiente" => $SalaComedorIndependiente,
                        "Citofono"                 => $Citofono,
                        "CuartodeServicio"         => $CuartodeServicio,
                        "ZonaResidencial"          => $ZonaResidencial,
                        "Estudio"                  => $Estudio,
                        "HalldeAlcobas"            => $HalldeAlcobas,
                        "Patio"                    => $Patio,
                        "Cocineta"                 => $Cocineta,
                        "ZonaBBQ"                  => $ZonaBBQ,
                        "Salondeconferencias"      => $Salondeconferencias,
                        "EdificioInteligente"      => $EdificioInteligente,
                        "logo"                     => "https://www.simiinmobiliarias.com/" . str_replace('../', '', getCampo('clientessimi', "where IdInmobiliaria=$IdInmobiliaria", 'logo')),
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            return $conn->error;
        }

    }
    public function VerificaFotos($inmuFoto)
    {
        $connPDO = new Conexion();
        $idInmo  = $data['inmo'];
        $usr     = $data['usuariom2'];
        $stmt    = $connPDO->prepare("Select idInm
            From fotos
            Where idInm=:inmuFoto");

        if ($stmt->execute(array(
            ":inmuFoto" => $inmuFoto,
        ))) {

            $existe = $stmt->rowCount();
            return $existe;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function listadoInmueblesConsignados($data)
    {
        $connPDO   = new Conexion();
        $cond      = "";
        $tabla     = "";
        $enlace    = "";
        $btnVender = "";

        if ($data['idInmu']) {
            $cond .= "  AND
                    ( idInm=:codinmu
                    OR RVivaReal=:codinmu
                    OR RLaGuia=:codinmu1
                    OR RMtoCuadrado=:codinmu2
                    OR RZonaProp=:codinmu3
                    OR RMeli =:codinmu4
                    Or codinterno=:codinmu5
                    Or codinm=:codinmu6) ";
        }
        if ($data['estadoInm']) {
            $cond .= " and i.idEstadoinmueble=:estadoInm";
        }
        if ($data['gestion']) {
            $cond .= " and i.IdGestion=:gestion";
        }
        if ($data['tContrato']) {
            $cond .= " and i.tipo_contrato=:tContrato";
        }
        if ($data['tInmueble']) {
            $cond .= " and i.IdTpInm=:tInmueble";
        }
        if ($data['barrio']) {
            $cond .= " and i.IdBarrio=:barrio";
        }
        if ($data['ciudad']) {
            $cond .= " and b.IdCiudad=:ciudad";
            $tabla  = ",barrios b";
            $enlace = " and i.IdBarrio=b.IdBarrios ";
        }
        if ($data['consignado']) {
            $cond .= " and i.IdCaptador=:consignado";
        }
        if ($data['ffecha_ini'] > 0 and $data['ffecha_fin'] > 0) {
            $cond .= " and i.FConsignacion between :fecha_inis and :fecha_fins ";
        }
        if ($data['verAsesor'] == 1) {
            $cond .= " and i.IdPromotor=:prom";
        }
        $rowsPerPage = 50;
        $offset      = ($data['paginador'] - 1) * $rowsPerPage;

        $stmt = $connPDO->prepare("SELECT  i.idInm, i.Direccion, i.ValorVenta, i.ValorCanon, i.IdGestion,
                            i.idEstadoinmueble,i.Administracion, i.FConsignacion, i.restricciones,i.RLaGuia,
                            i.RVivaReal,i.RZonaProp,i.RMtoCuadrado,i.Estrato,i.AreaConstruida ,
                            i.NumLlaves,i.NumCasillero,i.descripcionlarga,i.PublicaM2,i.PublicaMeli,
                            i.PublicaMiCasa,i.PublicaZonaProp,i.PublicaDoomos,i.PublicaLamudi,i.PublicaGuia,
                            i.Publicagpt,i.AreaLote,i.EdadInmueble,i.usu_crea,i.RNcasa,i.IdTpInm, i.IdBarrio,
                            i.tipo_contrato,i.idProcedencia,i.RIdnd,i.IdInmobiliaria, i.RCcuadras,i.RZhabitat
                            from inmuebles i $tabla
                            where IdInmobiliaria=:idinmo " .
            $enlace . $cond . "
                            limit $offset,50");
        $stmt->bindParam(':idinmo', $data['inmo']);
        //$stmt->bindParam(':idinmo',$id);
        if ($data['idInmu']) {
            $stmt->bindParam(':codinmu', $data['idInmu']);
            $stmt->bindParam(':codinmu1', $data['idInmu']);
            $stmt->bindParam(':codinmu2', $data['idInmu']);
            $stmt->bindParam(':codinmu3', $data['idInmu']);
            $stmt->bindParam(':codinmu4', $data['idInmu']);
            $stmt->bindParam(':codinmu5', $data['idInmu']);
            $stmt->bindParam(':codinmu6', $data['idInmu']);
        }
        if ($data['ciudad']) {
            $stmt->bindParam(':ciudad', $data['ciudad']);
        }
        if ($data['estadoInm']) {
            $stmt->bindParam(':estadoInm', $data['estadoInm']);
        }
        if ($data['gestion']) {
            $stmt->bindParam(':gestion', $data['gestion']);
        }
        if ($data['tContrato']) {
            $stmt->bindParam(':tContrato', $data['tContrato']);
        }
        if ($data['tInmueble']) {
            $stmt->bindParam(':tInmueble', $data['tInmueble']);
        }
        if ($data['barrio']) {
            $stmt->bindParam(':barrio', $data['barrio']);
        }
        if ($data['consignado']) {
            $stmt->bindParam(':consignado', $data['consignado']);
        }
        if ($data['ffecha_ini'] > 0 and $data['ffecha_fin'] > 0) {
            $stmt->bindParam(':fecha_inis', $data['ffecha_ini']);
            $stmt->bindParam(':fecha_fins', $data['ffecha_fin']);
        }
        if ($data['verAsesor'] == 1) {
            $stmt->bindParam(':prom', $_SESSION['Id_Usuarios']);
        }
        if ($stmt->execute()) {

            $data = array();
            $connPDO->exec('chartset="utf-8"');
            while ($row = $stmt->fetch()) {
                $catidadResgistros = count($row);
                $existeFoto        = $this->VerificaFotos($row['idInm']);
                $idciud            = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], "IdCiudad");
                $btnPortales       = ($existeFoto > 0) ? '<button data-placement="bottom" title="Sincronizar a Portales" type="button" class="btn btn-warning  btnSincro  btn-xs btns "  data-toggle="modal"  data-target="#sincroPorta"><span class="glyphicon glyphicon"><i class="fa fa-upload"></i></span></button>' : '<label>Agregue Fotos para Publicar</label>';
                $btnPortales       = ($row['idEstadoinmueble'] == 2) ? $btnPortales : '';
                $btnPagos          = '<button data-placement="bottom" title="Portales" type="button" id="pagos" class="btn btn-info  pagos  btn-xs btns"  data-toggle="modal"  data-target=".mymodalpagos"><span class="glyphicon glyphicon"><i class="fa fa-files-o"></i></span></button>';
                $btnficha          = '<a href="../../mcomercialweb/inmuebles/buscainmueblesnvo2.php?codinmueble=' . $row['idInm'] . '"><button type="button" id="ficha" data-placement="bottom" title="Ver Detalles" class="btn btn-info  ficha  btn-xs btns"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button></a>';
                if (getCampo('habilita_progs', "where inmob_pg=" . $row['IdInmobiliaria'] . " and prog_pg=1", 'prog_pg') == 1) {
                    $btnEditar = '<a href="../../mwc/inmuebles/ubicacionDinamic.php?inm=' . $row['idInm'] . '"><button type="button" id="edita" class="btn btn-watermelon  edita  btn-xs btns" data-placement="bottom" title="Editar Inmueble" ><span class="glyphicon glyphicon"><i class="fa fa-cogs"></i></span></button></a>';
                } else {
                    $btnEditar = '<a href="../../mcomercialweb/EditarInmueble.php?inm=' . $row['idInm'] . '"><button type="button" id="edita" class="btn btn-watermelon  edita  btn-xs btns" data-placement="bottom" title="Editar Inmueble" ><span class="glyphicon glyphicon"><i class="fa fa-cogs"></i></span></button></a>';
                }

                $btnReversar = '<a href="../../mcomercialweb/reversar_inmueble.php?regresa=1&Inmueble=' . $row['idInm'] . '&nvoi=1"><button type="button" id="reversar" class="btn btn-spartan  reversar  btn-xs btns" data-placement="bottom" title="Cambiar Estado" ><span class="glyphicon glyphicon"><i class="fa fa-edit"></i></span></button></a>';
                if (($row['IdGestion'] == 5 || $row['IdGestion'] == 2) && $row['idEstadoinmueble'] != 3) {
                    $btnVender = '<a href="../../mcomercialweb/vender_inmueble.php?regresa=1&Inmueble=' . $row['idInm'] . '"><button type="button" id="vender" class="btn btn-info  vender  btn-xs btns" data-placement="bottom" title="Vender Inmueble" ><span class="glyphicon glyphicon"><i class="fa fa-money"></i></span></button></a>';
                }

                //$btnficha='<a href="../../mcomercialweb/buscainmueblesnvo2.php?codinmueble='.$row['idInm'].'"><button type="button" id="ficha" class="btn btn-info  ficha  btn-xs btns"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button></a>';
                $btnDuplicar        = '<button title="Duplicar Inmueble" type="button" id="ficha" class="btn btn-info  ficha  btn-xs btns" onclick="AbrirVentana(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-files-o" data-placement="bottom" ></i></span></button>';
                $btnOrdenar         = '<button  data-placement="bottom" title="Ordenar Fotos" type="button" id="ficha" class="btn btn-purple  ficha  btn-xs btns" onclick="AbrirVentanaOrdenar(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-camera-retro"></i></span></button>';
                $btnObprop          = '<button  data-placement="bottom" title="Observaciones Propietarios" type="button" id="ficha" class="btn btn-warning  ficha  btn-xs btns btn-propietario" onclick="AbrirVentanaProp(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-edit"></i></span></button>';
                $btnVerFicha        = '<button  data-placement="bottom" title="Ver Ficha" type="button" id="ficha" class="btn btn-default  ficha  btn-xs btns" onclick="AbrirVentanaFicha(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-file-photo-o"></i></span></button>';
                $btnVerFichaChicala = ($row['IdInmobiliaria'] == 172 || $row['IdInmobiliaria'] == 1) ? '<button  data-placement="bottom" title="Ver Ficha Chicala" type="button" id="ficha" class="btn btn-success  ficha  btn-xs btns" onclick="AbrirVentanaFichaCh(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-file-picture-o"></i></span></button>' : '';
                $btnVerFichaGomez   = ($row['IdInmobiliaria'] == 447 || $row['IdInmobiliaria'] == 1) ? '<button  data-placement="bottom" title="Ver Ficha Gomez" type="button" id="ficha" class="btn btn-primary  ficha  btn-xs btns" onclick="AbrirVentanaFichaGm(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-file-image-o"></i></span></button>' : '';
                $inputInmu          = '<input type="hidden" id="idInmu" name="idInmu" value="' . $row['idInm'] . '" >';

                ///validar cadena de pruebas metro
                $spruebas = getCampo('usuariosm2', "where inmogm2=" . $row['IdInmobiliaria'] . " and servidor=2 limit 0,1", "servidor");

                $btnCadenaMetro = ($this->verifyPortales($row['IdInmobiliaria'], 10) > 0 && $spruebas == 2) ? '<button  data-placement="bottom" title="Generar Prueba Metro" type="button" id="cadenaM2" class="btn btn-warning  ficha  btn-xs btns" onclick="AbrirCadena(' . "'" . $row['idInm'] . "'" . ',' . "'" . $row['RMtoCuadrado'] . "'" . ',' . getCampo('usuariosm2', "where inmogm2=" . $row['IdInmobiliaria'] . " limit 0,1", "idusariom2") . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-link"></i></span></button>' : '';

                $usucrea     = ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $row['usu_crea'] . "' and Id_Usuarios>0", 'concat(Nombres," ",apellidos)')));
                $cadPortales = '<ul>';
                $cadPortales .= "<li><b>" . $row['idInm'] . "</b></li>";
                if ($row['RLaGuia'] > 0) {
                    $cadPortales .= "<li>FincaRaiz " . $row['RLaGuia'] . "</li>";
                }
                if ($row['RMtoCuadrado'] > 0 && $row['RMtoCuadrado'] != 0) {
                    $cadPortales .= "<li>Metro2 " . $row['RMtoCuadrado'] . "</li>";
                }
                if ($row['RNcasa'] > 0) {
                    $cadPortales .= "<li>Nuestra Casa " . $row['RNcasa'] . "</li>";
                }
                if ($row['RIdnd'] > 0) {
                    $cadPortales .= "<li>Idonde " . $row['RIdnd'] . "</li>";
                }
                if ($row['RVivaReal'] > 0) {
                    $cadPortales .= "<li>Interno " . $row['RVivaReal'] . "</li>";
                }

                if ($row['RZhabitat'] != '') {
                    $cadPortales .= "<li>Zona Habitat " . $row['RZhabitat'] . "</li>";
                }
                if ($row['RZonaProp'] > 0) {
                    $cadPortales .= "<li>I24 " . $row['RZonaProp'] . "</li>";
                }
                if ($row['PublicaDoomos'] == 'Publicado') {
                    $cadPortales .= "<li>Gratuitos "  . "</li>";
                }
                if ($row['PublicaLamudi'] == 'Publicado') {
                    $cadPortales .= "<li>Lamudi " . $row['PublicaLamudi'] . "</li>";
                }
                if ($row['Publicagpt'] == 'Publicado') {
                    $cadPortales .= "<li>GoPlaceit " . $row['Publicagpt'] . "</li>";
                }
                if ($row['RCcuadras'] > 0) {
                    $cadPortales .= "<li>Ciencuadras " . $row['RCcuadras'] . "</li>";
                }
                $flag = getCampo('clientessimi', "where IdInmobiliaria=" . $_SESSION['IdInmmo'], 'flag_portal');
                if ($flag == 10) {
                    $cadPortales = '<ul>';
                    $cadPortales .= "<li>Metro2 " . $row['RMtoCuadrado'] . "</li>";
                    $cadenaBotones = $btnCadenaMetro;
                } elseif ($flag == 8) {
                    $cadPortales = '<ul>';
                    $cadPortales .= "<li>FincaRaiz " . $row['RLaGuia'] . "</li>";
                } else {
                    $cadenaBotones = $btnEditar . $btnReversar . $btnPortales . $btnOrdenar . $btnVerFicha . $btnVerFichaChicala . $btnVerFichaGomez . $btnDuplicar . $btnCadenaMetro . $inputInmu;
                }

                $cadPortales .= '</ul>';

                $data[] = array(
                    "idInm"          => $cadPortales,
                    "Direccion"      => $row['Direccion'],
                    "ValorVenta"     => number_format($row['ValorVenta']),
                    "ValorCanon"     => number_format($row['ValorCanon']),
                    "Gestion"        => ucwords(strtolower(getCampo('gestioncomer', "where IdGestion=" . $row['IdGestion'], "NombresGestion"))),
                    "procedencia"    => ucwords(strtolower(getCampo('procedencia', "where IdProcedencia=" . $row['idProcedencia'], "Nombre"))),
                    "tcontrato"      => ucwords(strtolower(getCampo('tipo_contrato', "where id_contrato=" . $row['tipo_contrato'], "tipo_contrato"))),
                    "TipoInm"        => ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble=" . $row['IdTpInm'], "Descripcion"))),
                    "Barrio"         => ucwords(strtolower(getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], "NombreB"))),
                    "ciudad"         => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . $idciud, "Nombre"))),
                    "Estadoinmueble" => ucwords(strtolower(getCampo('estado_inmueble', "where idestado=" . $row['idEstadoinmueble'], 'descripcion'))),
                    "Administracion" => $row['Administracion'],
                    "FConsignacion"  => $row['FConsignacion'],
                    "Estrato"        => $row['Estrato'],
                    "AreaConstruida" => $row['AreaConstruida'] ." M2",
                    "AreaLote"       => $row['AreaLote']." M2",
                    "EdadInmueble"   => $row['EdadInmueble'],
                    "usu_crea"       => $usucrea,
                    "RNcasa"         => $row['RNcasa'],
                    "bnts"           => $cadenaBotones,

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }

    public function listadoInmueblesDestacados($codInmo, $lim)
    {
        //echo $codInmu."ff";
        if (($consulta = $this->db->prepare(" select
        IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
        Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
        Barrio,Gestion,AreaConstruida,AreaLote,latitud,
        longitud,EdadInmueble,NombreInmo,logo,IdBarrios
        from datos_call,settings_portales
        where Codigo_Inmueble=inmu_p
        and IdInmobiliaria=inmob_p
        and  tipo_p=4
        and vlr_p=1
        and IdInmobiliaria=?
        limit 0,?
        "))) {
            $consulta->bind_param("ii", $codInmo, $lim);

            $consulta->execute();

            if ($consulta->bind_result($IdInmobiliaria, $Administracion, $IdGestion, $Estrato, $IdTpInm,
                $Codigo_Inmueble, $Tipo_Inmueble, $Venta, $Canon, $descripcionlarga,
                $Barrio, $Gestion, $AreaConstruida, $AreaLote, $latitud,
                $longitud, $EdadInmueble, $NombreInmo, $logo, $IdBarrios)) {
                while ($consulta->fetch()) {
                    if ($IdGestion == 1) {
                        $oper   = 'rent';
                        $precio = $Canon;
                    } else {
                        $oper   = 'sale';
                        $precio = $Venta;
                    }

                    
                    $foto1=str_replace('../', '', getCampo('fotos', "where idInm='" . $Codigo_Inmueble . "'", 'Foto1'));
                    if(!empty($foto1)){
                        $foto1="https://www.simiinmobiliarias.com/mcomercialweb/".$foto1;
                    }else{
                        $foto1="https://www.simiinmobiliarias.com/images/no_image_inm.png";
                    }

                    $arreglo[] = array(
                        "IdInmobiliaria"   => $IdInmobiliaria,
                        "codinm" => $Codigo_Inmueble,
                        "Administracion"   => number_format($Administracion), //1
                        "IdGestion"        => $IdGestion, //2
                        "Estrato"          => $Estrato, //3
                        "IdTpInm"          => $IdTpInm, //4
                        "Codigo_Inmueble"  => $Codigo_Inmueble,
                        "Tipo_Inmueble"    => ucwords(strtolower($Tipo_Inmueble)),
                        "Venta"            => number_format($Venta),
                        "Canon"            => number_format($Canon),
                        "precio"           => number_format($precio),
                        "descripcionlarga" => $descripcionlarga,
                        "Barrio"           => ucwords(strtolower($Barrio)),
                        "Gestion"          => ucwords(strtolower($Gestion)),
                        "AreaConstruida"   => number_format($AreaConstruida),
                        "AreaLote"         => number_format($AreaLote),
                        "latitud"          => $latitud,
                        "longitud"         => $longitud,
                        "EdadInmueble"     => $EdadInmueble,
                        "NombreInmo"       => ucwords(strtolower($NombreInmo)),
                        "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                        "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                        "oper"             => $oper,
                        "foto1"            => $foto1,
                        "totaleregistros"  => $total,
                        "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $logo),
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            return $conn->error;
        }
        $stmt = null;

    }
    public function listadoInmueblesFiltro($codInmo, $lim = 0, $tip_ope, $depto = 0, $area, $Precio, $tip_inm, $ciudad = 0)
    {
        //echo $codInmu."ff";
        //echo "$tip_ope, $depto,$area,$Precio ------<br>";

        $connPDO = new Conexion();
        $cond1   = "";
        if ($tip_ope > 0 && $tip_ope != 2) {
            $cond1 .= " and IdGestion=:tip_ope ";
        }
        if ($tip_inm > 0) {
            $cond1 .= " and IdTpInm=:tip_inm ";
        }
        if ($depto > 0) {
            $cond1 .= " and d.IdDepartamento=:depto ";
        }
        if ($ciudad > 0) {
            $cond1 .= " and c.IdCiudad=:ciud ";
        }
        if ($area > 0) {
            $cond1 .= " and AreaConstruida between :area and :area1 ";
        }
        if ($Precio > 0 && $tip_ope == 5) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 1) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon between :Precio and :Precio1 or Venta between :Precio and :Precio1) ";
        }
        //echo $cond1."ffffffff";
        $stmt = $connPDO->prepare(" select
        IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
        Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
        Barrio,Gestion,AreaConstruida,AreaLote,latitud,
        longitud,EdadInmueble,NombreInmo,logo,IdBarrios
        from datos_call c,departamento d,ciudad cu
        where IdInmobiliaria=:codInmo
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad
        $cond1
        limit $lim,10");

        $stmt->bindParam(':codInmo', $codInmo);
        //$stmt->bindParam(':lim',$lim);

        if ($tip_ope > 0 && $tip_ope != 2) {
            $stmt->bindParam(':tip_ope', $tip_ope);
        }
        if ($tip_inm > 0) {
            $stmt->bindParam(':tip_inm', $tip_inm);
        }
        //echo "<br> ***** ".$depto."*******<br>";
        if ($depto > 0) {
            $stmt->bindParam(':depto', $depto);
        }
        if ($ciudad > 0) {
            //echo $ciudad."ffffff--- <br>";
            $stmt->bindParam(':ciud', $ciudad);
        }
        if ($area > 0) {
            list($are, $are1) = explode("|", $this->trae_area($area));
            //echo "$are -$are1";
            $stmt->bindParam(':area', $are);
            $stmt->bindParam(':area1', $are1);
        }
        if ($Precio > 0 && $tip_ope == 1) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            //echo "<br> $Prec,$Prec1 <br>";
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        if ($Precio > 0 && $tip_ope == 5) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        //$stmt->bindParam(':Precio',$Precio);
        if ($Precio > 0 && $tip_ope == 2) {
            list($Precio, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }

        if ($stmt->execute()) {
            $existen = $stmt->rowCount();
            //echo "existen $existen <br>";
            while ($row = $stmt->fetch()) {

                /*$stmt1=$this->db->prepare("elect count(*) as total
                FROM datos_call
                where IdInmobiliaria=?");
                $stmt1->bind_param("i",$codInmo);
                $consulta1->execute();
                $consulta1->bind_result($total);*/

                if ($row['IdGestion'] == 1) {
                    $oper   = 'rent';
                    $precio = $row['Canon'];
                } else {
                    $oper   = 'sale';
                    $precio = $row['Venta'];
                }
                $IdBarrios = $row['IdBarrios'];
                $arreglo[] = array(
                    "IdInmobiliaria"   => $row['IdInmobiliaria'],
                    "Administracion"   => number_format($row['Administracion']), //1
                    "IdGestion"        => $row['IdGestion'], //2
                    "Estrato"          => $$row['Estrato'], //3
                    "IdTpInm"          => $row['IdTpInm'], //4
                    "Codigo_Inmueble"  => $row['Codigo_Inmueble'],
                    "Tipo_Inmueble"    => ucwords(strtolower($row['Tipo_Inmueble'])),
                    "Venta"            => number_format($row['Venta']),
                    "Canon"            => number_format($row['Canon']),
                    "precio"           => number_format($precio),
                    "descripcionlarga" => $row['descripcionlarga'],
                    "Barrio"           => ucwords(strtolower($row['Barrio'])),
                    "Gestion"          => ucwords(strtolower($row['Gestion'])),
                    "AreaConstruida"   => number_format($row['AreaConstruida']),
                    "AreaLote"         => number_format($row['AreaLote']),
                    "latitud"          => $row['latitud'],
                    "longitud"         => $row['longitud'],
                    "EdadInmueble"     => $row['EdadInmueble'],
                    "NombreInmo"       => ucwords(strtolower($row['NombreInmo'])),
                    "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                    "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                    "oper"             => $oper,
                    "foto1"            => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos', "where idInm='" . $row['Codigo_Inmueble'] . "'", 'Foto1')),
                    "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $row['logo']),
                );

            }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            if ($existen > 0) {
                return $arreglo;
            } else {
                return 0;
            }

        } else {
            //echo  "error ejecucion--".$stmt->errorInfo();
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function listadoInmueblesFiltroTodos($codInmo, $lim = 0, $tip_ope, $depto = 0, $area, $Precio, $tip_inm, $ciudad = 0)
    {
        //echo $codInmu."ff";
        //echo "$tip_ope, $depto,$area,$Precio ------<br>";

        $connPDO = new Conexion();
        $cond1   = "";
        if ($tip_ope > 0 && $tip_ope != 2) {
            $cond1 .= " and IdGestion=:tip_ope ";
        }
        if ($tip_inm > 0) {
            $cond1 .= " and IdTpInm=:tip_inm ";
        }
        if ($depto > 0) {
            $cond1 .= " and d.IdDepartamento=:depto ";
        }
        if ($ciudad > 0) {
            $cond1 .= " and c.IdCiudad=:ciud ";
        }
        if ($area > 0) {
            $cond1 .= " and AreaConstruida between :area and :area1 ";
        }
        if ($Precio > 0 && $tip_ope == 5) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 1) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon between :Precio and :Precio1 or Venta between :Precio and :Precio1) ";
        }
        //echo $cond1."ffffffff";
        $stmt = $connPDO->prepare(" select
        IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
        Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
        Barrio,Gestion,AreaConstruida,AreaLote,latitud,
        longitud,EdadInmueble,NombreInmo,logo,IdBarrios
        from datos_call c,departamento d,ciudad cu
        where IdInmobiliaria=:codInmo
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad
        $cond1
        limit $lim,1000");

        $stmt->bindParam(':codInmo', $codInmo);
        //$stmt->bindParam(':lim',$lim);

        if ($tip_ope > 0 && $tip_ope != 2) {
            $stmt->bindParam(':tip_ope', $tip_ope);
        }
        if ($tip_inm > 0) {
            $stmt->bindParam(':tip_inm', $tip_inm);
        }
        //echo "<br> ***** ".$depto."*******<br>";
        if ($depto > 0) {
            $stmt->bindParam(':depto', $depto);
        }
        if ($ciudad > 0) {
            //echo $ciudad."ffffff--- <br>";
            $stmt->bindParam(':ciud', $ciudad);
        }
        if ($area > 0) {
            list($are, $are1) = explode("|", $this->trae_area($area));
            //echo "$are -$are1";
            $stmt->bindParam(':area', $are);
            $stmt->bindParam(':area1', $are1);
        }
        if ($Precio > 0 && $tip_ope == 1) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            //echo "<br> $Prec,$Prec1 <br>";
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        if ($Precio > 0 && $tip_ope == 5) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        //$stmt->bindParam(':Precio',$Precio);
        if ($Precio > 0 && $tip_ope == 2) {
            list($Precio, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }

        if ($stmt->execute()) {
            $existen = $stmt->rowCount();
            //echo "existen $existen <br>";
            while ($row = $stmt->fetch()) {

                /*$stmt1=$this->db->prepare("elect count(*) as total
                FROM datos_call
                where IdInmobiliaria=?");
                $stmt1->bind_param("i",$codInmo);
                $consulta1->execute();
                $consulta1->bind_result($total);*/

                if ($row['IdGestion'] == 1) {
                    $oper   = 'rent';
                    $precio = $row['Canon'];
                } else {
                    $oper   = 'sale';
                    $precio = $row['Venta'];
                }
                $IdBarrios = $row['IdBarrios'];
                $arreglo[] = array(
                    "IdInmobiliaria"   => $row['IdInmobiliaria'],
                    "Administracion"   => number_format($row['Administracion']), //1
                    "IdGestion"        => $row['IdGestion'], //2
                    "Estrato"          => $$row['Estrato'], //3
                    "IdTpInm"          => $row['IdTpInm'], //4
                    "Codigo_Inmueble"  => $row['Codigo_Inmueble'],
                    "Tipo_Inmueble"    => ucwords(strtolower($row['Tipo_Inmueble'])),
                    "Venta"            => number_format($row['Venta']),
                    "Canon"            => number_format($row['Canon']),
                    "precio"           => number_format($precio),
                    "descripcionlarga" => $row['descripcionlarga'],
                    "Barrio"           => ucwords(strtolower($row['Barrio'])),
                    "Gestion"          => ucwords(strtolower($row['Gestion'])),
                    "AreaConstruida"   => number_format($row['AreaConstruida']),
                    "AreaLote"         => number_format($row['AreaLote']),
                    "latitud"          => $row['latitud'],
                    "longitud"         => $row['longitud'],
                    "EdadInmueble"     => $row['EdadInmueble'],
                    "NombreInmo"       => ucwords(strtolower($row['NombreInmo'])),
                    "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                    "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                    "oper"             => $oper,
                    "foto1"            => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos', "where idInm='" . $row['Codigo_Inmueble'] . "'", 'Foto1')),
                    "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $row['logo']),
                );

            }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            if ($existen > 0) {
                return $arreglo;
            } else {
                return 0;
            }

        } else {
            //echo  "error ejecucion--".$stmt->errorInfo();
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function listadoInmueblesFiltro2($codInmo, $lim = 0, $tip_ope, $depto = 0, $area, $area1, $Precio, $Precio1, $zona, $tip_inm, $ciudad = 0)
    {
        //echo $codInmu."ff";
        //echo "$tip_ope, $depto,$area,$Precio ------<br>";

        $connPDO = new Conexion();
        $cond1   = "";
        if ($tip_ope > 0 && $tip_ope != 2) {
            $cond1 .= " and IdGestion=:tip_ope ";
        }
        if ($tip_inm > 0) {
            $cond1 .= " and IdTpInm=:tip_inm ";
        }
        if ($depto > 0) {
            $cond1 .= " and d.IdDepartamento=:depto ";
        }
        if ($ciudad > 0) {
            $cond1 .= " and c.IdCiudad=:ciud ";
        }
        if ($zona > 0) {
            $cond1 .= " and d.IdZona=:zona ";
        }
        if ($area > 0 && $area1 > 0) {
            $cond1 .= " and AreaConstruida between :area and :area1 ";
        }
        if ($area > 0 && $area1 <= 0) {
            $cond1 .= " and AreaConstruida >= :area";
        }
        if ($area <= 0 && $area1 > 0) {
            $cond1 .= " and AreaConstruida <= :area ";
        }
        if ($tip_ope == 5) {
            $campov = "Venta";
        } else {
            $campov = "Canon";
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope != 2) {

            $cond1 .= " and $campov between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope != 2) {
            $cond1 .= " and $campov >= :Precio ";
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope != 2) {
            $cond1 .= " and $campov <= :Precio1 ";
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon between :Precio and :Precio1 or Venta between :Precio and :Precio1) ";
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon >= :Precio or Venta >= :Precio ) ";
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon <= :Precio1 or Venta <= :Precio1) ";
        }
        //echo $cond1."ffffffff";
        $stmt = $connPDO->prepare(" select
        IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
        Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
        Barrio,Gestion,AreaConstruida,AreaLote,latitud,
        longitud,EdadInmueble,NombreInmo,logo,IdBarrios
        from datos_call c,departamento d,ciudad cu
        where IdInmobiliaria=:codInmo
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad
        $cond1
        #if(IdGestion==5,' order by Venta',' order by Canon')
        limit $lim,10");

        $stmt->bindParam(':codInmo', $codInmo);
        //$stmt->bindParam(':lim',$lim);

        if ($tip_ope > 0 && $tip_ope != 2) {
            $stmt->bindParam(':tip_ope', $tip_ope);
        }
        if ($tip_inm > 0) {
            $stmt->bindParam(':tip_inm', $tip_inm);
        }
        //echo "<br> ***** ".$depto."*******<br>";
        if ($depto > 0) {
            $stmt->bindParam(':depto', $depto);
        }
        if ($ciudad > 0) {
            //echo $ciudad."ffffff--- <br>";
            $stmt->bindParam(':ciud', $ciudad);
        }
        if ($zona > 0) {
            $stmt->bindParam(':zona', $zona);
        }
        if ($area > 0 && $area1 > 0) {
            $stmt->bindParam(':area', $area);
            $stmt->bindParam(':area1', $area1);
        }
        if ($area > 0 && $area1 <= 0) {
            $stmt->bindParam(':area', $area);
        }
        if ($area <= 0 && $area1 > 0) {
            $stmt->bindParam(':area1', $area1);
        }

        $vlrCero = 0;
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio', $Precio);
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio', $Precio);
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio', $Precio);
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio', $Precio);
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($stmt->execute()) {
            $existen = $stmt->rowCount();
            //echo "existen $existen <br>";
            while ($row = $stmt->fetch()) {

                /*$stmt1=$this->db->prepare("elect count(*) as total
                FROM datos_call
                where IdInmobiliaria=?");
                $stmt1->bind_param("i",$codInmo);
                $consulta1->execute();
                $consulta1->bind_result($total);*/

                if ($row['IdGestion'] == 1) {
                    $oper   = 'rent';
                    $precio = $row['Canon'];
                } else {
                    $oper   = 'sale';
                    $precio = $row['Venta'];
                }
                $IdBarrios = $row['IdBarrios'];
                $arreglo[] = array(
                    "IdInmobiliaria"   => $row['IdInmobiliaria'],
                    "Administracion"   => number_format($row['Administracion']), //1
                    "IdGestion"        => $row['IdGestion'], //2
                    "Estrato"          => $$row['Estrato'], //3
                    "IdTpInm"          => $row['IdTpInm'], //4
                    "Codigo_Inmueble"  => $row['Codigo_Inmueble'],
                    "Tipo_Inmueble"    => ucwords(strtolower($row['Tipo_Inmueble'])),
                    "Venta"            => number_format($row['Venta']),
                    "Canon"            => number_format($row['Canon']),
                    "precio"           => number_format($precio),
                    "descripcionlarga" => $row['descripcionlarga'],
                    "Barrio"           => ucwords(strtolower($row['Barrio'])),
                    "Gestion"          => ucwords(strtolower($row['Gestion'])),
                    "AreaConstruida"   => number_format($row['AreaConstruida']),
                    "AreaLote"         => number_format($row['AreaLote']),
                    "latitud"          => $row['latitud'],
                    "longitud"         => $row['longitud'],
                    "EdadInmueble"     => $row['EdadInmueble'],
                    "NombreInmo"       => ucwords(strtolower($row['NombreInmo'])),
                    "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                    "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                    "oper"             => $oper,
                    "foto1"            => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos', "where idInm='" . $row['Codigo_Inmueble'] . "'", 'Foto1')),
                    "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $row['logo']),
                );

            }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            if ($existen > 0) {
                return $arreglo;
            } else {
                return 0;
            }

        } else {
            //echo  "error ejecucion--".$stmt->errorInfo();
            print_r($stmt->errorInfo());
        }
        $stmt = null;

    }
    public function totalInmueblesFiltro($codInmo, $lim = 0, $tip_ope, $depto = 0, $area, $Precio, $tip_inm, $ciudad = 0)
    {
        //echo $codInmu."ff";
        //echo "$tip_ope, $depto,$area,$Precio ------<br>";

        $connPDO = new Conexion();
        $cond1   = "";
        if ($tip_ope > 0 && $tip_ope != 2) {
            $cond1 .= " and IdGestion=:tip_ope ";
        }
        if ($tip_inm > 0) {
            $cond1 .= " and IdTpInm=:tip_inm ";
        }
        if ($depto > 0) {
            $cond1 .= " and d.IdDepartamento=:depto ";
        }
        if ($ciudad > 0) {
            $cond1 .= " and c.IdCiudad=:ciud ";
        }
        if ($area > 0) {
            $cond1 .= " and AreaConstruida between :area and :area1 ";
        }
        if ($Precio > 0 && $tip_ope == 5) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 1) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon between :Precio and :Precio1 or Venta between :Precio and :Precio1) ";
        }
        //echo $cond1."ffffffff";
        $stmt = $connPDO->prepare(" select count(*) as tot
            FROM datos_call c,departamento d,ciudad cu
        WHERE IdInmobiliaria=:codInmo
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad
        $cond1 ");

        $stmt->bindParam(':codInmo', $codInmo);
        //$stmt->bindParam(':lim',$lim);

        if ($tip_ope > 0 && $tip_ope != 2) {
            $stmt->bindParam(':tip_ope', $tip_ope);
        }
        if ($tip_inm > 0) {
            $stmt->bindParam(':tip_inm', $tip_inm);
        }
        //echo "<br> ***** ".$depto."*******<br>";
        if ($depto > 0) {
            $stmt->bindParam(':depto', $depto);
        }
        if ($ciudad > 0) {
            //echo $ciudad."ffffff--- <br>";
            $stmt->bindParam(':ciud', $ciudad);
        }
        if ($area > 0) {
            list($are, $are1) = explode("|", $this->trae_area($area));
            //echo "$are -$are1";
            $stmt->bindParam(':area', $are);
            $stmt->bindParam(':area1', $are1);
        }
        if ($Precio > 0 && $tip_ope == 1) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            //echo "<br> $Prec,$Prec1 <br>";
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        if ($Precio > 0 && $tip_ope == 5) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        //$stmt->bindParam(':Precio',$Precio);
        if ($Precio > 0 && $tip_ope == 2) {
            list($Precio, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $tot = $row['tot'];
                //echo $tot."ffff";
            }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $tot;
        } else {
            //echo  "error ejecucion--".$stmt->errorInfo();
            //print_r($stmt->errorInfo());
        }
        $stmt = null;

    }
    public function totalInmueblesFiltro2($codInmo, $lim = 0, $tip_ope, $depto = 0, $area, $area1, $Precio, $Precio1, $zona, $tip_inm, $ciudad = 0)
    {
        //echo $codInmu."ff";
        //echo "$tip_ope, $depto,$area,$Precio ------<br>";

        $connPDO = new Conexion();
        $cond1   = "";
        if ($tip_ope > 0 && $tip_ope != 2) {
            $cond1 .= " and IdGestion=:tip_ope ";
        }
        if ($tip_inm > 0) {
            $cond1 .= " and IdTpInm=:tip_inm ";
        }
        if ($depto > 0) {
            $cond1 .= " and d.IdDepartamento=:depto ";
        }
        if ($ciudad > 0) {
            $cond1 .= " and c.IdCiudad=:ciud ";
        }
        if ($zona > 0) {
            $cond1 .= " and c.IdZona=:zona ";
        }
        if ($area > 0 && $area1 > 0) {
            $cond1 .= " and AreaConstruida between :area and :area1 ";
        }
        if ($area > 0 && $area1 <= 0) {
            $cond1 .= " and AreaConstruida >= :area";
        }
        if ($area <= 0 && $area1 > 0) {
            $cond1 .= " and AreaConstruida <= :area ";
        }
        if ($tip_ope == 5) {
            $campov = "Venta";
        } else {
            $campov = "Canon";
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope != 2) {

            $cond1 .= " and $campov between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope != 2) {
            $cond1 .= " and $campov >= :Precio ";
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope != 2) {
            $cond1 .= " and $campov <= :Precio1 ";
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon between :Precio and :Precio1 or Venta between :Precio and :Precio1) ";
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon >= :Precio or Venta >= :Precio ) ";
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon <= :Precio1 or Venta <= :Precio1) ";
        }
        //echo $cond1."ffffffff";
        $stmt = $connPDO->prepare(" select count(*) as tot
            FROM datos_call c,departamento d,ciudad cu
        WHERE IdInmobiliaria=:codInmo
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad
        $cond1
        ");

        $stmt->bindParam(':codInmo', $codInmo);
        //$stmt->bindParam(':lim',$lim);

        if ($tip_ope > 0 && $tip_ope != 2) {
            $stmt->bindParam(':tip_ope', $tip_ope);
        }
        if ($tip_inm > 0) {
            $stmt->bindParam(':tip_inm', $tip_inm);
        }
        //echo "<br> ***** ".$depto."*******<br>";
        if ($depto > 0) {
            $stmt->bindParam(':depto', $depto);
        }
        if ($ciudad > 0) {
            //echo $ciudad."ffffff--- <br>";
            $stmt->bindParam(':ciud', $ciudad);
        }
        if ($zona > 0) {
            $stmt->bindParam(':zona', $zona);
        }
        if ($area > 0 && $area1 > 0) {
            $stmt->bindParam(':area', $area);
            $stmt->bindParam(':area1', $area1);
        }
        if ($area > 0 && $area1 <= 0) {
            $stmt->bindParam(':area', $area);
        }
        if ($area <= 0 && $area1 > 0) {
            $stmt->bindParam(':area1', $area1);
        }
        $vlrCero = 0;
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio', $Precio);
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio', $Precio);
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio', $Precio);
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio', $Precio);
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio1', $Precio1);
        }

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $tot = $row['tot'];
                //echo $tot."ffff";
            }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $tot;
        } else {
            //echo  "error ejecucion--".$stmt->errorInfo();
            //print_r($stmt->errorInfo());
        }
        $stmt = null;

    }
    public function trae_precio($val)
    {
        if ($val == 1) {
            $are  = "0";
            $are1 = "1000000";
        }
        if ($val == 2) {
            $are  = "1000001";
            $are1 = "2000000";
        }
        if ($val == 3) {
            $are  = "2000001";
            $are1 = "4000000";
        }
        if ($val == 4) {
            $are  = "4000001";
            $are1 = "10000000";
        }
        if ($val == 5) {
            $are  = "10000001";
            $are1 = "100000000000000";
        }

        return $are . "|" . $are1;
    }
    public function trae_area($area)
    {
        if ($area == 1) {
            $are  = "0";
            $are1 = "60";
        }
        if ($area == 2) {
            $are  = "61";
            $are1 = "90";
        }
        if ($area == 3) {
            $are  = "91";
            $are1 = "150";
        }
        if ($area == 4) {
            $are  = "151";
            $are1 = "240";
        }
        if ($area == 5) {
            $are  = "241";
            $are1 = "360";
        }
        if ($area == 6) {
            $are  = "361";
            $are1 = "480";
        }
        if ($area == 7) {
            $are  = "481";
            $are1 = "600";
        }
        if ($area == 8) {
            $are  = "601";
            $are1 = "60000000000";
        }
        return $are . "|" . $are1;
    }
    public function listadoProyectos($codInmo, $lim, $tp)
    {
        //echo $codInmu."ff";
        if (($consulta = $this->db->prepare(" SELECT
        IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
        Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
        Barrio,Gestion,AreaConstruida,AreaLote,latitud,
        longitud,EdadInmueble,NombreInmo,logo,IdBarrios
        from datos_call
        where IdInmobiliaria=?
        and IdTpInm=?
        limit 0,?
        "))) {
            $consulta->bind_param("sii", $codInmo, $tp, $lim);

            $consulta->execute();
//                echo "$qry";

            if ($consulta->bind_result($IdInmobiliaria, $Administracion, $IdGestion, $Estrato, $IdTpInm,
                $Codigo_Inmueble, $Tipo_Inmueble, $Venta, $Canon, $descripcionlarga,
                $Barrio, $Gestion, $AreaConstruida, $AreaLote, $latitud,
                $longitud, $EdadInmueble, $NombreInmo, $logo, $IdBarrios)) {
                while ($consulta->fetch()) {
                    if ($IdGestion == 1) {
                        $oper   = 'rent';
                        $precio = $Canon;
                    } else {
                        $oper   = 'sale';
                        $precio = $Venta;
                    }

                    $arreglo[] = array(
                        "IdInmobiliaria"   => $IdInmobiliaria,
                        "Administracion"   => number_format($Administracion), //1
                        "IdGestion"        => $IdGestion, //2
                        "Estrato"          => $Estrato, //3
                        "IdTpInm"          => $IdTpInm, //4
                        "Codigo_Inmueble"  => $Codigo_Inmueble,
                        "Tipo_Inmueble"    => ucwords(strtolower($Tipo_Inmueble)),
                        "Venta"            => number_format($Venta),
                        "Canon"            => number_format($Canon),
                        "precio"           => number_format($precio),
                        "descripcionlarga" => $descripcionlarga,
                        "Barrio"           => ucwords(strtolower($Barrio)),
                        "Gestion"          => ucwords(strtolower($Gestion)),
                        "AreaConstruida"   => number_format($AreaConstruida),
                        "AreaLote"         => number_format($AreaLote),
                        "latitud"          => $latitud,
                        "longitud"         => $longitud,
                        "EdadInmueble"     => $EdadInmueble,
                        "NombreInmo"       => ucwords(strtolower($NombreInmo)),
                        "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                        "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                        "oper"             => $oper,
                        "foto1"            => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos', "where idInm='" . $Codigo_Inmueble . "'", 'Foto1')),
                        "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $logo),
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            echo "error --" . $conn->error;
        }

    }

    public function listadoInmuebles($idEmpresa, $IdGestion, $codInmu = '')
    {
        $cond = '';
        if ($codInmu) {
            $cond = "and Codigo_Inmueble=?";
        }
        if (($consulta = $this->db->prepare(" SELECT
        IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
        Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
        Barrio,Gestion,AreaConstruida,AreaLote,latitud,
        longitud,EdadInmueble,NombreInmo,logo
        from datos_call
        where IdInmobiliaria=?
        and IdGestion=?
        $cond
        limit 0,20"))) {
            if ($codInmu) {
                $consulta->bind_param("iis", $idEmpresa, $IdGestion, $codInmu);
            } else {
                $consulta->bind_param("ii", $idEmpresa, $IdGestion);
            }

            $consulta->execute();
//                echo "$qry";
            $arreglo1 = array();
            if ($consulta->bind_result($IdInmobiliaria, $Administracion, $IdGestion, $Estrato, $IdTpInm,
                $Codigo_Inmueble, $Tipo_Inmueble, $Venta, $Canon, $descripcionlarga,
                $Barrio, $Gestion, $AreaConstruida, $AreaLote, $latitud,
                $longitud, $EdadInmueble, $NombreInmo, $logo)) {
                while ($consulta->fetch()) {
                    $arreglo[] = array(
                        "IdInmobiliaria"   => $IdInmobiliaria,
                        "Administracion"   => $Administracion, //1
                        "IdGestion"        => $IdGestion, //2
                        "Estrato"          => $Estrato, //3
                        "IdTpInm"          => $IdTpInm, //4
                        "Codigo_Inmueble"  => $Codigo_Inmueble,
                        "Tipo_Inmueble"    => $Tipo_Inmueble,
                        "Venta"            => $Venta,
                        "Canon"            => $Canon,
                        "descripcionlarga" => $descripcionlarga,
                        "Barrio"           => $Barrio,
                        "Gestion"          => $Gestion,
                        "AreaConstruida"   => $AreaConstruida,
                        "AreaLote"         => $AreaLote,
                        "latitud"          => $latitud,
                        "longitud"         => $longitud,
                        "EdadInmueble"     => $EdadInmueble,
                        "EdadInmueble"     => $NombreInmo,
                        "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $logo),
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            echo "error --" . $conn->error;
        }
        $stmt = null;
    }
    public function fotosInmueble($codInmueble)
    {
        if (($consulta = $this->db->prepare(" select foto
        from fotos_nvo
        where id_inmft=?"))) {
            if (!$consulta->bind_param("s", $codInmueble)) {
                echo "error bind";
            } else {
                $consulta->execute();
//                echo "$qry";
                if ($consulta->bind_result($foto)) {
                    while ($consulta->fetch()) {
                        $arreglo[] = array(
                            "foto" => "https://www.simiinmobiliarias.com/mcomercialweb/" . $foto,
                        );

                    }
                }

                return $arreglo;

            }
        } else {
            echo "error img" . $conn->error;
        }
        $stmt = null;
    }
    public function fotosInmueble2($data)
    {
        $cond = "";
        if ($data['lim'] > 0) {
            $cond = " limit 0," . $data['lim'];
        }
        if (($consulta = $this->db->prepare(" select foto
        from fotos_nvo
        where aa_fto=?
        and codinm_fto=?
        $cond"))) {
            if (!$consulta->bind_param("ss", $data['IdInmobiliaria'], $data['codinm'])) {
                echo "error bind";
            } else {
                $consulta->execute();
//                echo "$qry";
                if ($consulta->bind_result($foto)) {
                    while ($consulta->fetch()) {
                        $arreglo[] = array(
                            "source" => "https://www.simiinmobiliarias.com/mcomercialweb/" . $foto,
                        );

                    }
                }

                return $arreglo;

            }
        } else {
            echo "error img" . $conn->error;
        }
        $stmt = null;
    }
    public function inmueblesDisponibles($IdInmob)
    {
        $condperfil = "";
        $ase        = $_SESSION['Id_Usuarios'];
        if ($_SESSION['IdInmmo'] == 632) {
            $condperfil = "and IdPromotor='$ase'";
        }
        $w_conexion = new MySQL();
        $sql        = "Select count(Codigo_Inmueble) as tot
                        From datos_call
                        where IdInmobiliaria=$IdInmob
                        $condperfil";

        $res = $w_conexion->ResultSet($sql);
        while ($f = $w_conexion->FilaSiguienteArray($res)) {
            $tot = $f['tot'];
        }
        return $tot;
        $w_conexion->CerrarConexion();
    }
    public function inmueblesSinFotos($IdInmob)
    {
        $cade       = "";
        $condperfil = "";
        $ase        = $_SESSION['Id_Usuarios'];
        if ($_SESSION['IdInmmo'] == 632) {
            $condperfil = "and IdPromotor='$ase'";
        }
        $arreglo1   = array();
        $w_conexion = new MySQL();
        $sql        = "SELECT i.idInm,i.IdInmobiliaria,f.Foto1
                FROM inmuebles i
                LEFT JOIN fotos f ON i.idInm=f.idInm
                WHERE i.IdInmobiliaria = '" . $IdInmob . "'
                AND (f.Foto1 IS NULL OR LENGTH(f.Foto1)=0)
                AND i.idEstadoinmueble  = 2
                $condperfil";

        $res = $w_conexion->ResultSet($sql);
        while ($f = $w_conexion->FilaSiguienteArray($res)) {
            $idInm = $f['idInm'];
            $cade  = "'" . $idInm . "'," . $cade;
        }
        return substr($cade, 0, -1);
        $w_conexion->CerrarConexion();
    }

    public function inmueblesSinVideos($IdInmob)
    {
        $cade       = "";
        $condperfil = "";
        $ase        = $_SESSION['Id_Usuarios'];
        if ($_SESSION['IdInmmo'] == 632) {
            $condperfil = "and IdPromotor='$ase'";
        }
        $arreglo1   = array();
        $w_conexion = new MySQL();
        $sql        = "SELECT idInm
                        FROM inmuebles
                        WHERE IdInmobiliaria = '" . $IdInmob . "'
                        AND (linkvideo IS NULL OR LENGTH(linkvideo)=0)
                        AND idEstadoinmueble  = 2
                        $condperfil";

        $res = $w_conexion->ResultSet($sql);
        while ($f = $w_conexion->FilaSiguienteArray($res)) {
            $idInm = $f['idInm'];
            $cade  = "'" . $idInm . "'," . $cade;
        }
        return substr($cade, 0, -1);
        $w_conexion->CerrarConexion();
    }
    public function inmueblesIncompletos($IdInmob)
    {
        $w_conexion = new MySQL();
        $condperfil = "";
        $ase        = $_SESSION['Id_Usuarios'];
        if ($_SESSION['IdInmmo'] == 632) {
            $condperfil = "and IdPromotor='$ase'";
        }
        $sql = "Select count(Codigo_Inmueble) as tot
                        From datos_call
                        where IdInmobiliaria=$IdInmob
                        $condperfil";

        $res = $w_conexion->ResultSet($sql);
        while ($f = $w_conexion->FilaSiguienteArray($res)) {
            $tot = $f['tot'];
        }
        return $tot;
        $w_conexion->CerrarConexion();
    }
    public function listadoInmuebles2($inmob, $codInmuebles, $nro_inm)
    {
        $cond = "";
        if ($codInmuebles) {
            $cond = "and Codigo_Inmueble in ($codInmuebles)";
        }
        if ($nro_inm) {
            $cond = "and Codigo_Inmueble in ('1-" . $nro_inm . "')";
        }
        $condperfil = "";
        $ase        = $_SESSION['Id_Usuarios'];
        if ($_SESSION['IdInmmo'] == 632 or $_SESSION['IdInmmo'] == 631 or $_SESSION['IdInmmo'] == 422 or $_SESSION['IdInmmo'] == 501 or $_SESSION['IdInmmo'] == 699 or $_SESSION['IdInmmo'] == 711 or $_SESSION['IdInmmo'] == 538 or $_SESSION['IdInmmo'] == 409 or $_SESSION['IdInmmo'] == 411 or $_SESSION['IdInmmo'] == 707 or $_SESSION['IdInmmo'] == 122) {
            $condperfil = "and IdPromotor='$ase'";
        }
        $w_conexion = new MySQL();
        $sql        = "SELECT
        IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
        Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
        Barrio,Gestion,AreaConstruida,AreaLote,latitud,
        longitud,EdadInmueble,NombreInmo,logo,IdPropietario, Nombrepro,IdBarrios,Ciudad,
        NombreCap,IdPromotor,NombreProm,IdCaptador
        from datos_call
        where IdInmobiliaria=$inmob
        $cond
        $condperfil
        #limit 0,20";
        //echo $sql;
        $res = $w_conexion->ResultSet($sql);
        while ($f = $w_conexion->FilaSiguienteArray($res)) {
            $arreglo[0]  = $f['IdInmobiliaria']; //0
            $arreglo[1]  = $f['Administracion']; //1
            $arreglo[2]  = $f['IdGestion']; //2
            $arreglo[3]  = $f['Estrato']; //3
            $arreglo[4]  = $f['IdTpInm']; //4
            $arreglo[5]  = $f['Codigo_Inmueble'];
            $arreglo[6]  = $f['Tipo_Inmueble'];
            $arreglo[7]  = $f['Venta'];
            $arreglo[8]  = $f['Canon'];
            $arreglo[9]  = $f['descripcionlarga'];
            $arreglo[10] = $f['Barrio'];
            $arreglo[11] = $f['Gestion'];
            $arreglo[12] = $f['AreaConstruida'];
            $arreglo[13] = $f['AreaLote'];
            $arreglo[14] = $f['latitud'];
            $arreglo[15] = $f['longitud'];
            $arreglo[16] = $f['EdadInmueble'];
            $arreglo[17] = $f['NombreInmo'];
            $arreglo[18] = "https://www.simiinmobiliarias.com/" . str_replace('../', '', $f['logo']);
            $arreglo[19] = $f['IdPropietario'];
            $arreglo[20] = $f['Nombrepro'];
            $arreglo[21] = $f['IdBarrios'];
            $arreglo[22] = $f['Ciudad'];
            $arreglo[23] = $f['IdCaptador'];
            $arreglo[24] = $f['NombreCap'];
            $arreglo[25] = $f['IdPromotor'];
            $arreglo[26] = $f['NombreProm'];
            // $arreglo[27]=$f['codinterno'];

            $arreglo1[] = $arreglo;
        }
        return $arreglo1;

    }

    ////json para api Afydi

    public function listadoInmueblesAfydi($idEmpresa, $codInmu, $usr_act)
    {

        $dInmuebles   = new Inmuebles();
        $avisoVentana = 0;

        $pservidumbre   = $dInmuebles->evalua_carac(23, $codInmu, 'Cantidad', 'SERVIDUMBRE');
        $pindependiente = $dInmuebles->evalua_carac(23, $codInmu, 'Cantidad', 'INDEPENDIENTE', 0);
        $comunal        = $dInmuebles->evalua_carac(23, $codInmu, 'Cantidad', 'COMUNAL', 0);
        $pvisitantes    = $dInmuebles->evalua_carac(23, $codInmu, 'Cantidad', 'VISITANTES', 0);
        $pcubierto      = $dInmuebles->evalua_carac(23, $codInmu, 'Cantidad', 'CUBIERTO');
        $ascensor       = $dInmuebles->evalua_carac(25, $codInmu, 'Cantidad', 'ASCENSOR');
        $banos          = $dInmuebles->trae_carac(16, $codInmu, 'Cantidad', utf8_decode('baño'), 0);
        $alcobas        = $dInmuebles->trae_carac(15, $codInmu, 'Cantidad', 'alcoba', 0);
        $nivel          = $dInmuebles->trae_carac(36, $codInmu, 'Cantidad', 'nivel', 0);
        $inm_destacado  = getCampo('settings_portales', "where inmu_p='" . $codInmu . "' and tipo_p=4", 'vlr_p');
        $sucursal       = 0;

        if (($consulta = $this->db->prepare(" select
        IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
        idInm,ValorVenta,ValorCanon,descripcionlarga,
        IdBarrio,IdGestion,AreaConstruida,AreaLote,latitud,
        longitud,EdadInmueble,Direccion,
        IdPromotor,Fondo,Frente,
        Restricciones,idEstadoinmueble,IdDestinacion,IdPropietario,
        linkvideo,fingreso,FConsignacion,NoMatricula,ComiVenta,
        ComiArren,IdProcedencia
        from inmuebles
        where IdInmobiliaria=?
        and idInm=?
        #$cond
        limit 0,20"))) {
            if ($codInmu) {
                $consulta->bind_param("is", $idEmpresa, $codInmu);
            } else {
                $consulta->bind_param("is", $idEmpresa, $codInmu);
            }

            $consulta->execute();
//                echo "$qry";

            if ($consulta->bind_result($IdInmobiliaria, $Administracion, $IdGestion, $Estrato, $IdTpInm,
                $idInm, $ValorVenta, $ValorCanon, $descripcionlarga,
                $IdBarrio, $IdGestion, $AreaConstruida, $AreaLote, $latitud,
                $longitud, $EdadInmueble, $Direccion,
                $IdPromotor, $Fondo, $Frente, $Restricciones, $idEstadoinmueble, $IdDestinacion, $IdPropietario,
                $linkvideo, $fingreso, $FConsignacion, $NoMatricula, $ComiVenta,
                $ComiArren, $IdProcedencia)) {
                while ($consulta->fetch()) {
                    $IdCiudad = getCampo('barrios', "where IdBarrios=" . $IdBarrio, 'IdCiudad');
                    $IdZona   = getCampo('barrios', "where IdBarrios=" . $IdBarrio, 'IdZona');
                    if ($IdGestion == 5) {
                        $porc_comision = $ComiVenta;
                    } else {
                        $porc_comision = $ComiArren;
                    }
                    if ($IdProcedencia == 58) {
                        $avisoVentana = 1;
                    }
                    $IdTpInmAfydi       = (getCampo('tipoinmuebles', "where idTipoInmueble=$IdTpInm", 'conafydi', 0) == "") ? 1 : getCampo('tipoinmuebles', "where idTipoInmueble=$IdTpInm", 'conafydi', 0);
                    $IdGestionAfydi     = getCampo('gestioncomer', "where IdGestion=$IdGestion", 'convgafydi', 0);
                    $IdDestinacionAfydi = getCampo('destinacion', "where IdDestinacion=$IdDestinacion", 'convdafydi', 0);
                    $ZonaAfydi          = getCampo('zonas', "where IdZona=$IdZona", 'convzafydi', 0);
                    $barrioComun        = getCampo('barrios', "where IdBarrios=$IdBarrio", 'NombreB', 0);
                    $dateIngreso        = ($fingreso == "") ? "0" : $fingreso;
                    $inm_destacado      = ($inm_destacado == 0) ? "0" : $inm_destacado;
                    $inm                = explode("-", $idInm);
                    $nivel              = ($nivel == "") ? 1 : $nivel;
                    $linkvideo          = ($linkvideo == "") ? " " : $linkvideo;
                    $ValorCanon         = ($ValorCanon == "") ? 0 : $ValorCanon;

                    $arreglo = array(
                        "codpro"            => $inm[1],
                        "address"           => "cra 45 b 78 34",
                        "city"              => $IdCiudad,
                        "zone"              => $ZonaAfydi,
                        "type"              => $IdTpInmAfydi,
                        "biz"               => 2,
                        "area_lot"          => $AreaLote,
                        "build_year"        => $EdadInmueble,
                        "area_cons"         => $AreaConstruida,
                        "bedrooms"          => $alcobas,
                        "bathrooms"         => $banos,
                        "stratum"           => $Estrato,
                        "saleprice"         => $ValorVenta,
                        "description"       => utf8_encode("sdfsfdf"),
                        "latitude"          => "+4.715",
                        "longitude"         => "-74.10",
                        "barrio_comun"      => $barrioComun,
                        "status"            => $idEstadoinmueble,
                        "neighborhood"      => $IdBarrio,
                        "floor"             => $nivel,
                        "rent"              => $ValorCanon,
                        "administration"    => "1000000",
                        #"desc_eng"              => "",
                        #"desc_fre"              => "",
                        "resctrictions"     => $Restricciones,
                        #"comment"               => "",
                        #"comment2"              => "",
                        #"asesor"                => $IdPromotor,
                        "video"             => $linkvideo,
                        "parking"           => $pindependiente,
                        "parking_covered"   => $pcubierto,
                        "consignation_date" => $FConsignacion,
                        "propietario"       => $IdPropietario,
                        "registry_date"     => $dateIngreso,
                        #"destacado"             => $inm_destacado,
                        #"branch"                => $idEmpresa.$sucursal,
                        "window_sign"       => $avisoVentana,
                        "front"             => $Frente,
                        #"rear"                  => "",
                        #"private_area"          => "",
                        #"destinacion"           => "$IdDestinacionAfydi",
                        #"commission_percentage" => "",
                        #"matricula_number"      => "",
                        #"referencia"            => "",
                        "images"            => $dInmuebles->fotosInmuebleAfydi($codInmu),
                    );
                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            //echo json_encode($arreglo);
            return $arreglo;
            //return json_encode($arreglo);
        } else {
            echo "error --" . $conn->error;
        }
        $stmt = null;
    }
    public function fotosInmuebleAfydi($codInmueble)
    {
        $w_conexion = new MySQL();
        $urlFoto    = "https://www.simiinmobiliarias.com/mcomercialweb/";

        $consulta1 = "select
        Foto1,Foto2,Foto3,Foto4,Foto5,
        Foto6,Foto7,Foto8,Foto9,Foto10
        from fotos
        where idInm='$codInmueble'";
        $res = $w_conexion->ResultSet($consulta1);
        // if(!$consulta1->bind_param("s",$codInmueble))
        // {
        //     echo "error bind";
        // }
        // else
        // {

        while ($ff = $w_conexion->FilaSiguienteArray($res)) {

            for ($p = 1; $p < 11; $p++) {
                $Foto = $ff['Foto' . $p];
                $urlFoto . $Foto1;
                $arreglo[] = array(
                    "id"        => $p,
                    "order"     => $p,
                    "imagename" => 'Foto' . $p,
                    "imageurl"  => $urlFoto . $Foto,
                    "thumburl"  => $urlFoto . $Foto,
                    #"s3bucket"         =>"",
                    "inmueble"  => $codInmueble,
                    #"comments"         =>"",
                    #"created_at"     =>date('Y-m-d'),
                    #"updated_at"     => date('Y-m-d')
                );
            }
            //    }
            //print_r( $arreglo);
            return $arreglo;
        }
        $w_conexion->CerrarConexion();
    }
    public function updateCodeAfydi($codInmu, $afydi)
    {
        $conexion = new Conexion();
        $stmt     = $conexion->prepare('UPDATE inmuebles SET RAfydi =:RAfydi
                                  WHERE idInm = :idInm');
        if ($stmt->execute(array(
            ":RAfydi" => $afydi,
            ":idInm"  => $codInmu))) {
            return 1;
        } else {
            print_r($stmt->errorInfo());
        }
    }
    public function trae_carac($grupo, $inmueble, $campo, $especifica, $muestra = "")
    {
        global $w_conexion;
        $cond = "";
        if ($especifica) {
            $cond .= " and Descripcion like '%$especifica%'";
        }
        $sqlconsulta = "SELECT  $campo as aaa
             FROM detalleinmueble
             INNER JOIN maestrodecaracteristicas
             ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica
             AND  maestrodecaracteristicas.IdGrupo in ($grupo)
             INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble'
             AND detalleinmueble.idinmueble = inmuebles.idinm
             $cond";
        if ($muestra == 1) {
            echo "<br>" . $sqlconsulta . "<br>";
        }
        $res    = $w_conexion->ResultSet($sqlconsulta) or die("error " . mysql_error());
        $existe = $w_conexion->FilasAfectadas($res);
        //echo $existe."ff";
        while ($fff = $w_conexion->FilaSiguienteArray($res)) {
            //echo "hola en cliclo";
            $idcaracteristica = $fff["aaa"];
        }
        //echo ">> $idcaracteristica --- $campo <br>";
        return $idcaracteristica;
        $w_conexion->CerrarConexion();
    }
    public function evalua_carac($grupo, $inmueble, $campo, $especifica, $muestra = "")
    {
        global $w_conexion;
        $cond = "";
        if ($especifica) {
            $cond .= " and Descripcion like '%$especifica%'";
        }
        $sqlconsulta = "SELECT  $campo as aaa
             FROM detalleinmueble
             INNER JOIN maestrodecaracteristicas
             ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica
             AND  maestrodecaracteristicas.IdGrupo in ($grupo)
             INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble'
             AND detalleinmueble.idinmueble = inmuebles.idinm
             $cond";
        if ($muestra == 1) {
            echo $sqlconsulta;
        }
        $res    = $w_conexion->ResultSet($sqlconsulta) or die("error " . mysql_error());
        $existe = $w_conexion->FilasAfectadas($res);
        //echo $existe."ff";

        if ($existe > 0) {
            $idcaracteristica = "1";
        } else {
            $idcaracteristica = "0";
        }
//    echo ">> $idcaracteristica --- $campo <br>";
        return $idcaracteristica;
        $w_conexion->CerrarConexion();
    }
    public function evalua_carac_exacto($grupo, $inmueble, $campo, $especifica)
    {
        global $w_conexion;
        $cond = "";
        if ($especifica) {
            $cond .= " and Descripcion = '$especifica'";
        }
        $sqlconsulta = "SELECT  $campo as aaa
             FROM detalleinmueble
             INNER JOIN maestrodecaracteristicas
             ON detalleinmueble.idcaracteristica = maestrodecaracteristicas.idcaracteristica
             AND  maestrodecaracteristicas.IdGrupo ='$grupo'
             INNER JOIN inmuebles ON detalleinmueble.idinmueble ='$inmueble'
             AND detalleinmueble.idinmueble = inmuebles.idinm
             $cond";
        //echo $sqlconsulta;
        $res    = $w_conexion->ResultSet($sqlconsulta) or die("error " . mysql_error());
        $existe = $w_conexion->FilasAfectadas($res);
        //echo $existe."ff";

        if ($existe > 0) {
            $idcaracteristica = "1";
        } else {
            $idcaracteristica = "0";
        }
//    echo ">> $idcaracteristica --- $campo <br>";
        return $idcaracteristica;
        $w_conexion->CerrarConexion();
    }
    public function actualizarEstado($idInmueble, $codAfydi)
    {
        if (!empty($idInmueble) && !empty($codAfydi)) {
            $connPDO = new Conexion();

            $stmt = $connPDO->prepare("UPDATE inmuebles SET
                                      PublicaAfydi='Publicado',
                                      RAfydi=:codAfydi
                                      WHERE idInm = :idInmueble");
            if ($stmt->execute(array(
                ":codAfydi"   => $codAfydi,
                ":idInmueble" => $idInmueble,
            ))) {
                return 1;
            } else {
                print_r($stmt->errorInfo());
            }
        } else {
            return 0;
        }
        $stmt = null;
    }

    public function verificaEstadoAfydi($codInmu)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT RAfydi
                           FROM inmuebles
                           where idInm = :codInmu AND
                           RAfydi>0");
        if ($stmt->execute(array(
            ":codInmu" => $codInmu,
        ))) {
            $seguimiento = $stmt->rowCount();
            if ($seguimiento > 0) {
                return 1;
            }
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;

    }
    public function obtenerCodigoAfydi($codInmu)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT RAfydi
                           FROM inmuebles
                           where idInm = :codInmu AND
                           RAfydi>0");
        if ($stmt->execute(array(
            ":codInmu" => $codInmu,
        ))) {
            $codigo = "";
            while ($row = $stmt->fetch()) {
                $codigo = $row["RAfydi"];
            }
            return $codigo;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;

    }
    public function getTipoInmuebles()
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT idTipoInmueble,Descripcion FROM tipoinmuebles");
        if ($stmt->execute()) {
            $data = array();

            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "idTipoInmueble" => $row["idTipoInmueble"],
                    "Descripcion"    => ucwords(strtolower($row["Descripcion"])),
                );
            }
            return $data;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function gestioncomer()
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT IdGestion,NombreGestion FROM gestioncomer");
        if ($stmt->execute()) {
            $data = array();

            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "IdGestion"     => $row["IdGestion"],
                    "NombreGestion" => ucwords(strtolower($row["NombreGestion"])),
                );
            }
            return $data;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function getAsesor($idInmob)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT IdPromotor,Nombres,apellidos,Foto,email,Celular
                        FROM inmuebles,usuarios
                        WHERE IdInmobiliaria=:idInmob
                        and IdPromotor=Id_Usuarios
                        AND IdPromotor>0
                        GROUP BY IdPromotor");
        $stmt->bindParam(":idInmob", $idInmob);
        if ($stmt->execute()) {
            $data = array();

            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "idInmob"              => $idInmob,
                    "Nombres"              => ucwords(strtolower($row["Nombres"])),
                    "apellidos"            => ucwords(strtolower($row["apellidos"])),
                    "Foto"                 => $row["Foto"],
                    "email"                => $row["email"],
                    "NombreGesCelulartion" => $row["Celular"],
                );
            }
            return $data;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function verifyPortales($id, $idPortal)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("Select p.IdPortal,p.IdInmobiliaria
                from PublicaPortales p
                where p.IdInmobiliaria= :idinmo
                and IdPortal=:portal");
        if ($stmt->execute(array(
            ":idinmo" => $id,
            ":portal" => $idPortal,
        ))) {
            $existe = $stmt->rowCount();
            return $existe;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function cantidadlistadoInmueblesConsignados($data)
    {
        $connPDO   = new Conexion();
        $cond      = "";
        $tabla     = "";
        $enlace    = "";
        $btnVender = "";

        if ($data['idInmu']) {
            $cond .= "  AND
                    ( idInm=:codinmu
                    OR RVivaReal=:codinmu
                    OR RLaGuia=:codinmu1
                    OR RMtoCuadrado=:codinmu2
                    OR RZonaProp=:codinmu3
                    OR RMeli =:codinmu4
                    Or codinterno=:codinmu5
                    Or codinm=:codinmu6) ";
        }
        if ($data['estadoInm']) {
            $cond .= " and i.idEstadoinmueble=:estadoInm";
        }
        if ($data['gestion']) {
            $cond .= " and i.IdGestion=:gestion";
        }
        if ($data['tContrato']) {
            $cond .= " and i.tipo_contrato=:tContrato";
        }
        if ($data['tInmueble']) {
            $cond .= " and i.IdTpInm=:tInmueble";
        }
        if ($data['barrio']) {
            $cond .= " and i.IdBarrio=:barrio";
        }
        if ($data['ciudad']) {
            $cond .= " and b.IdCiudad=:ciudad";
            $tabla  = ",barrios b";
            $enlace = " and i.IdBarrio=b.IdBarrios ";
        }
        if ($data['consignado']) {
            $cond .= " and i.IdCaptador=:consignado";
        }
        if ($data['ffecha_ini'] > 0 and $data['ffecha_fin'] > 0) {
            $cond .= " and i.FConsignacion between :fecha_inis and :fecha_fins ";
        }
        if ($data['verAsesor'] == 1) {
            $cond .= " and i.IdPromotor=:prom";
        }

        $stmt = $connPDO->prepare("SELECT i.idInm, i.Direccion, i.ValorVenta, i.ValorCanon, i.IdGestion,
                            i.idEstadoinmueble,i.Administracion, i.FConsignacion, i.restricciones,i.RLaGuia,
                            i.RVivaReal,i.RZonaProp,i.RMtoCuadrado,i.Estrato,i.AreaConstruida ,
                            i.NumLlaves,i.NumCasillero,i.descripcionlarga,i.PublicaM2,i.PublicaMeli,
                            i.PublicaMiCasa,i.PublicaZonaProp,i.PublicaDoomos,i.PublicaLamudi,i.PublicaGuia,
                            i.Publicagpt,i.AreaLote,i.EdadInmueble,i.usu_crea,i.RNcasa,i.IdTpInm, i.IdBarrio,
                            i.tipo_contrato,i.idProcedencia,i.RIdnd,i.IdInmobiliaria
                            from inmuebles i $tabla
                            where IdInmobiliaria=:idinmo " .
            $enlace . $cond . "
                            #limit 0,30");
        $stmt->bindParam(':idinmo', $data['inmo']);
        //$stmt->bindParam(':idinmo',$id);
        if ($data['idInmu']) {
            $stmt->bindParam(':codinmu', $data['idInmu']);
            $stmt->bindParam(':codinmu1', $data['idInmu']);
            $stmt->bindParam(':codinmu2', $data['idInmu']);
            $stmt->bindParam(':codinmu3', $data['idInmu']);
            $stmt->bindParam(':codinmu4', $data['idInmu']);
            $stmt->bindParam(':codinmu5', $data['idInmu']);
            $stmt->bindParam(':codinmu6', $data['idInmu']);
        }
        if ($data['ciudad']) {
            $stmt->bindParam(':ciudad', $data['ciudad']);
        }
        if ($data['estadoInm']) {
            $stmt->bindParam(':estadoInm', $data['estadoInm']);
        }
        if ($data['gestion']) {
            $stmt->bindParam(':gestion', $data['gestion']);
        }
        if ($data['tContrato']) {
            $stmt->bindParam(':tContrato', $data['tContrato']);
        }
        if ($data['tInmueble']) {
            $stmt->bindParam(':tInmueble', $data['tInmueble']);
        }
        if ($data['barrio']) {
            $stmt->bindParam(':barrio', $data['barrio']);
        }
        if ($data['consignado']) {
            $stmt->bindParam(':consignado', $data['consignado']);
        }
        if ($data['ffecha_ini'] > 0 and $data['ffecha_fin'] > 0) {
            $stmt->bindParam(':fecha_inis', $data['ffecha_ini']);
            $stmt->bindParam(':fecha_fins', $data['ffecha_fin']);
        }
        if ($data['verAsesor'] == 1) {
            $stmt->bindParam(':prom', $_SESSION['Id_Usuarios']);
        }
        if ($stmt->execute()) {
            $data        = array();
            $rowsPerPage = 50;
            $connPDO->exec('chartset="utf-8"');
            $datos         = $stmt->rowCount();
            $total_paginas = ceil($datos / $rowsPerPage);

            return $data[] = array(
                "paginas"   => $total_paginas,
                "registros" => $datos,
            );
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    /*********************************
     *          Imagenes             *
     *********************************/
    private static function saveCodFotosOLD($codInmo)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare(" INSERT IGNORE INTO fotos
                              (idInm) VALUES(:idInm)");
        if ($stmt->execute(array(
            ":idInm" => $codInmo,
        ))) {
            return 1;
        } else {
            return print_r(array(
                "error" => $stmt->errorInfo(),
                "data"  => $codInmo,
            ));
        }
        $stmt = "";
        $stmt = null;
    }
    private static function saveCodFotosOLD2($codInmo)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare(" INSERT IGNORE INTO fotos
                              (idInm) VALUES(:idInm)");
        if ($stmt->execute(array(
            ":idInm" => $codInmo,
        ))) {
            return 1;
        } else {
            return print_r(array(
                "error" => $stmt->errorInfo(),
                "data"  => $codInmo,
            ));
        }
        $stmt = "";
        $stmt = null;
    }
    public function saveFotosOld($codInmu, $file, $num)
    {
        $connPDO = new Conexion();

        if (Inmuebles::saveCodFotosOLD($codInmu) == 1) {

            $stmt = $connPDO->prepare("UPDATE fotos SET
                    Foto$num = :imagen
                    WHERE idInm = :idInm");
            if ($stmt->execute(array(
                ":imagen" => $file,
                ":idInm"  => $codInmu,
            ))) {
                $response[] = array(
                    'status' => "Imagenes Cargadas",
                    'files'  => $file,
                    'No'     => $num,
                );
            } else {
                $response[] = array(
                    'status' => $stmt->errorInfo(),
                    'files'  => $imagen,
                    'No'     => $num,
                );
            }
            $stmt = "";

        } else {
            $response = array($files);
        }

        // foreach (
        //  $value) {
        //     $response[]=array(
        //         "rutas"=>$value['ruta'],
        //         "Nos"=>$value['No']
        //         );
        // }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function saveFotosOld2($codInmu, $file, $num)
    {
        $connPDO = new Conexion();

        if (Inmuebles::saveCodFotosOLD2($codInmu) == 1) {

            $stmt = $connPDO->prepare("UPDATE fotos SET
                    Foto$num = :imagen
                    WHERE idInm = :idInm");
            if ($stmt->execute(array(
                ":imagen" => $file,
                ":idInm"  => $codInmu,
            ))) {
                $response[] = array(
                    'status' => "Imagenes Cargadas",
                    'files'  => $file,
                    'No'     => $num,
                );
            } else {
                $response[] = array(
                    'status' => $stmt->errorInfo(),
                    'files'  => $imagen,
                    'No'     => $num,
                );
            }
            $stmt = "";

        } else {
            $response = array($files);
        }

        // foreach (
        //  $value) {
        //     $response[]=array(
        //         "rutas"=>$value['ruta'],
        //         "Nos"=>$value['No']
        //         );
        // }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function saveFotosNew($files, $data)
    {
        $connPDO = new Conexion();

        foreach ($files as $key => $value) {
            // $num     = $value['No'];
            $imagen      = str_replace("../mcomercialweb/", "", $value['ruta']);
            $num         = Inmuebles::getCodPosiFoto($data['idInm']);
            $codInmu     = $data['idInm'];
            $idInmo      = $_SESSION['IdInmmo'];
            $conse       = consecutivo("conse", "fotos_nvo");
            $fec_fto     = date('Y-m-d');
            $usr_fto     = getCampo('usuarios', "where Id_Usuarios='" . $_SESSION['Id_Usuarios'] . "'", 'iduser');
            $separadores = substr_count($codInmu, "-");
            if ($separadores == 1) {
                list($aa, $codinm_fto) = explode("-", $codInmu);
            } elseif ($separadores == 2) {
                list($aa, $bb, $codinm_fto) = explode("-", $codInmu);
            }
            $stmt = $connPDO->prepare("INSERT INTO fotos_nvo (
            conse,
            posi_ft,
            id_inmft,
            foto,
            est_fot,
            aa_fto,
            codinm_fto,
            fec_fto,
            usr_fto)
                values (
                :conse,
                :posi_ft,
                :id_inmft,
                :foto,
                :est_fot,
                :aa_fto,
                :codinm_fto,
                :fec_fto,
                :usr_fto)");

            if ($stmt->execute(array(
                ":conse"      => $conse,
                ":posi_ft"    => $num,
                ":id_inmft"   => $codInmu,
                ":foto"       => $imagen,
                ":est_fot"    => 1,
                ":aa_fto"     => $idInmo,
                ":codinm_fto" => $codinm_fto,
                ":fec_fto"    => $fec_fto,
                ":usr_fto"    => $usr_fto,
            ))) {
                Inmuebles::selectFotosNvo($data['idInm']);

                $response[] = array(
                    "status"    => 1,
                    "uri"       => $imagen,
                    "label_fto" => getCampo('fotos_nvo', "where aa_fto=$idInmo and codinm_fto=$codinm_fto and posi_ft=$num", 'label_fto'),
                    "ficha_fto" => getCampo('fotos_nvo', "where aa_fto=$idInmo and codinm_fto=$codinm_fto and posi_ft=$num", 'ficha_fto'),
                    "posi_ft"   => $num,
                    "inmobiliaria" => getCampo("clientessimi","where IdInmobiliaria = ".$_SESSION["IdInmmo"],"Nombre"),
                    "avatar" => "https://www.simiinmobiliarias.com/" . str_replace("../","",getCampo("clientessimi","where IdInmobiliaria = ".$_SESSION["IdInmmo"],"logo")),
                    "link" => "http://www.simiinmobiliarias.com/mwc/callcenter/inmuebleCruce.php?idInmu=".$data['idInm'],
                    "user" => $_SESSION["codUser"]


                );
            } else {
                $response[] = array("Error" => $stmt->errorInfo());
            }
        }

        

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    private static function selectFotosNvo($cod)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT  posi_ft,foto
                                 FROM fotos_nvo
                                 where id_inmft=:IdInm
                                 and est_fot=1
                                 order by posi_ft");
        if ($stmt->execute(array(
            "IdInm" => $cod,
        ))) {
            while ($row = $stmt->fetch()) {
                // $count++;
                $foto  = $row['foto'];
                $count = $row['posi_ft'];
                // $foto="Foto".$count;
                Inmuebles::saveFotosOld($cod, $foto, $count);
            }
        } else {
            print_r(array(
                "status" => "Select Fotos Nvo",
                "Error"  => $stmt->errorInfo(),
            ));
        }
        $stmt = "";
        $stmt = null;
    }
    private static function selectFotosNvo2($cod)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT  posi_ft,foto
                                 FROM fotos_nvo
                                 where id_inmft=:IdInm
                                 and est_fot=1
                                 order by posi_ft");
        if ($stmt->execute(array(
            "IdInm" => $cod,
        ))) {
            while ($row = $stmt->fetch()) {
                // $count++;
                $foto  = $row['foto'];
                $count = $row['posi_ft'];
                // $foto="Foto".$count;
                Inmuebles::saveFotosOld2($cod, $foto, $count);
            }
        } else {
            print_r(array(
                "status" => "Select Fotos Nvo",
                "Error"  => $stmt->errorInfo(),
            ));
        }
        $stmt = "";
        $stmt = null;
    }
    public function deleteTotalFotosOld($cod)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("DELETE  FROM fotos
                                WHERE idInm = :idInm ");
        if ($stmt->execute(array(
            "idInm" => $cod['codInmu'],
        ))) {
            return 1;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function deleteTotalFotosNvo($cod)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("DELETE  FROM fotos_nvo
                                 WHERE id_inmft = :idInm");
        if ($stmt->execute(array(
            ":idInm" => $cod['codInmu'],
        ))) {
            return 1;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    private static function getCodPosiFoto($cod)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT  max(posi_ft) FROM fotos_nvo
                                 where id_inmft=:IdInm
                                 ");

        if ($stmt->execute(array(
            ":IdInm" => $cod,
        ))) {
            $result = "";
            if ($stmt->rowCount() > 0) {
                $result = $stmt->fetch(PDO::FETCH_COLUMN);
                return $result + 1;
            }
        }
        $stmt = null;
    }
    public function updateLabelFotosNew($data)
    {
        $connPDO = new Conexion();

        //foreach ($files as $key => $value) {
        // $num     = $value['No'];

        $stmt = $connPDO->prepare("UPDATE fotos_nvo
                SET
                label_fto      =:label_fto
                where posi_ft  =:posi_ft
                and aa_fto     =:IdInmobiliaria
                and codinm_fto =:codinm");

        if ($stmt->execute(array(
            ":label_fto"      => $data["label_fto"],
            ":posi_ft"        => $data['posi_ft'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":codinm"         => $data['codinm'],
        ))) {
            //Inmuebles::selectFotosNvo($data['idInm']);
            return 1;
        } else {
            $response[] = array("Error" => $stmt->errorInfo());
        }
        // }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function getFotosOld($data)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare('SELECT * FROM fotos
                                 WHERE idInm = :idInm');
        if ($stmt->execute(array(
            ":idInm" => $data["codInmue"],
        ))) {
            if ($stmt->rowCount() > 0) {

                $response = array();
                while ($row = $stmt->fetch()) {
                    for ($i = 1; $i < 31; $i++) {
                        if ($row['Foto' . $i] != "F" && !empty($row['Foto' . $i])) {
                            $response[]['Foto'] = $row['Foto' . $i];
                        } else {
                            $response[] = array('status' => "vacio", 'Foto' => "Foto" . $i);
                        }
                    }
                }

            } else {
                $response = 0;
            }
        } else {
            $response = array("status" => "Error Traer Imagenes",
                "error"                    => $stmt->errorInfo());
        }
        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function getFotosNew($data)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare('SELECT ficha_fto,foto,label_fto,posi_ft
                                 FROM fotos_nvo
                                 WHERE id_inmft = :idInm');

        if ($stmt->execute(array(
            ":idInm" => $data["inmCod"] . "-" . $data["codInmue"],
        ))) {

            if ($stmt->rowCount() > 0) {

                $response = array();
                while ($row = $stmt->fetch()) {

                    $fotoA      = explode("/", $row['foto']);
                    $fotoancho  = count($fotoA);
                    $response[] = array(
                        "status"    => 1,
                        "Foto"      => $row['foto'],
                        "fotoOrder" => $fotoA[$fotoancho - 1],
                        "label_fto" => $row['label_fto'],
                        "ficha_fto" => $row['ficha_fto'],
                        "posi_ft"   => $row['posi_ft'],
                    );
                    //} else {
                    $response[] = array('status' => "vacio", 'Foto' => "Foto" . $i);
                    //}

                }

            } else {
                $response = 0;
            }
        } else {
            $response = array("status" => "Error Traer Imagenes",
                "error"                    => $stmt->errorInfo());
        }
        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function getFotosNew2($data)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare('SELECT ficha_fto,foto,label_fto,posi_ft
                                 FROM fotos_nvo
                                 WHERE codinm_fto = :idInm
                                 AND aa_fto=:IdInmobiliaria');

        if ($stmt->execute(array(
            ":idInm"          => $data["codInmue"],
            ":IdInmobiliaria" => $data["inmCod"],
        ))) {

            if ($stmt->rowCount() > 0) {

                $response = array();
                while ($row = $stmt->fetch()) {

                    $fotoA      = explode("/", $row['foto']);
                    $fotoancho  = count($fotoA);
                    $response[] = array(
                        "status"    => 1,
                        "Foto"      => $row['foto'],
                        "fotoOrder" => $fotoA[$fotoancho - 1],
                        "label_fto" => $row['label_fto'],
                        "ficha_fto" => $row['ficha_fto'],
                        "posi_ft"   => $row['posi_ft'],
                    );
                    //} else {
                    $response[] = array('status' => "vacio", 'Foto' => "Foto" . $i);
                    //}

                }

            } else {
                $response = 0;
            }
        } else {
            $response = array("status" => "Error Traer Imagenes",
                "error"                    => $stmt->errorInfo());
        }
        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function deleteFtosNew($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare('DELETE FROM fotos_nvo
                                 WHERE  id_inmft = :idInm AND Foto = :Foto');
        if ($stmt->execute(array(
            ":idInm" => $data['codInmu'],
            ":Foto"  => $data['url'],
        ))) {
            if ($stmt->rowCount() > 0) {
                // Inmuebles::selectFotosNvo($data['codInmu']);
            } else {
                return 0;
            }
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }

    public function deleteFtosOlds($data)
    {
        $connPDO = new Conexion();
        for ($i = 1; $i < 31; $i++) {
            $stmt = $connPDO->prepare("UPDATE  fotos SET
                                   Foto$i=''
                                   WHERE  idInm = :idInm
                                   AND Foto$i = :Foto");
            if ($stmt->execute(array(
                ":idInm" => $data['codInmu'],
                ":Foto"  => $data['url'],
            ))) {
                $response = array();
                if ($stmt->rowCount() > 0) {
                    $response['foto'] = 1;
                } else {
                    $response['foto'] = 0;
                }
            } else {
                print_r($smtm->errorInfo());
            }
        }
        if ($response) {
            return $response;
        }
        $stmt = null;
    }

    public function listadoInmueblesConsignadosNvo($get, $data)
    {

        $connPDO   = new Conexion();
        $cond      = "";
        $tabla     = "";
        $enlace    = "";
        $btnVender = "";
        $dataN     = $data;

        /*datatable*/
        $aColumns = array("i.idInm", "i.Direccion", "i.ValorVenta", "i.ValorCanon", "i.IdGestion", "i.idEstadoinmueble", "i.Administracion", "i.FConsignacion", "i.restricciones", "i.RLaGuia", "i.RVivaReal", "i.RZonaProp", "i.RMtoCuadrado", "i.Estrato", "i.AreaConstruida",
            "i.NumLlaves", "i.NumCasillero", "i.descripcionlarga", "i.PublicaM2", "i.PublicaMeli",
            "i.PublicaMiCasa", "i.PublicaZonaProp", "i.PublicaDoomos", "i.PublicaLamudi", "i.PublicaGuia",
            "i.Publicagpt", "i.AreaLote", "i.EdadInmueble", "i.usu_crea", "i.RNcasa", "i.IdTpInm", "i.IdBarrio",
            "i.tipo_contrato", "i.idProcedencia", "i.RIdnd", "i.IdInmobiliaria");

        $columnsorder = array("i.codinm", "", "i.IdGestion", "i.ValorVenta", "i.ValorCanon", "i.IdTpInm", "i.idEstadoinmueble", "i.Direccion", "i.IdBarrio", "", "i.idProcedencia", "i.tipo_contrato", "i.FConsignacion", "i.usu_crea");

        $condOrder = ' ORDER BY ' . $columnsorder[$get['order'][0]['column']] . ' ' . $get['order'][0]['dir'];
        //
        // $condOrder = '';

        /* Individual column filtering */

        $sWhere = "";
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($get['search']) && $get["search"]["value"] != '') {
                if ($sWhere == "") {
                    $sWhere = " AND (";
                } else {
                    $sWhere .= " OR ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($get["search"]["value"]) . "%' ";
            }
        }

        /*
         * paginacion
         */
        $sLimit = " LIMIT 0,50 ";
        if (isset($get['start']) && $get['length'] != '-1') {
            $sLimit = " LIMIT " . intval($get['start']) . ", " .
            intval($get['length']);
        }

        /*datatable*/

        if ($data['idInmu']) {
            $cond .= "  AND
                    ( idInm=:codinmu
                    OR RVivaReal=:codinmu
                    OR RLaGuia=:codinmu1
                    OR RMtoCuadrado=:codinmu2
                    OR RZonaProp=:codinmu3
                    OR RMeli =:codinmu4
                    Or codinterno=:codinmu5
                    Or codinm=:codinmu6) ";
        }
        if ($data['estadoInm']) {
            $cond .= " and i.idEstadoinmueble=:estadoInm";
        }
        if ($data['gestion']) {
            $cond .= " and i.IdGestion=:gestion";
        }
        if ($data['tContrato']) {
            $cond .= " and i.tipo_contrato=:tContrato";
        }
        if ($data['tInmueble']) {
            $cond .= " and i.IdTpInm=:tInmueble";
        }
        if ($data['barrio']) {
            $cond .= " and i.IdBarrio=:barrio";
        }
        if ($data['ciudad']) {
            $cond .= " and b.IdCiudad=:ciudad";
            $tabla  = ",barrios b";
            $enlace = " and i.IdBarrio=b.IdBarrios ";
        }
        if ($data['consignado']) {
            $cond .= " and i.usu_crea=:consignado";//IdCaptador
        }

        $data['ffecha_ini']=urldecode($data['ffecha_ini']);
        $data['ffecha_fin']=urldecode($data['ffecha_fin']);

        if ( !empty($data['ffecha_ini']) and !empty($data['ffecha_fin']) ) {
            $cond .= " and i.FConsignacion between :fecha_inis and :fecha_fins ";
            //echo $data['ffecha_ini']." - ";
        }

        if ( !empty($data['ffecha_ini']) and empty($data['ffecha_fin']) ) {
            $cond .= " and i.FConsignacion >= :fecha_inis ";
        }

        if ( empty($data['ffecha_ini']) and !empty($data['ffecha_fin']) ) {
            $cond .= " and i.FConsignacion <= :ffecha_fin ";
        }


        if ($data['verAsesor'] == 1) {
            $cond .= " and i.IdPromotor=:prom";
            $cond2 .= "  and i.IdPromotor=:prom";
        }

        if ($sWhere != "") {

            $sWhere .= $cond;
            $sWhere .= ")";

        } else {
            $sWhere .= $cond2;
        }

        

        $rowsPerPage = 50;
        $offset      = ($data['paginador'] - 1) * $rowsPerPage;

        $stmt = $connPDO->prepare("SELECT i.idInm, i.codinm, i.Direccion, i.ValorVenta, i.ValorCanon, i.IdGestion,
                            i.idEstadoinmueble,i.Administracion, i.FConsignacion, i.restricciones,i.RLaGuia,
                            i.RVivaReal,i.RZonaProp,i.RMtoCuadrado,i.Estrato,i.AreaConstruida ,
                            i.NumLlaves,i.NumCasillero,i.descripcionlarga,i.PublicaM2,i.PublicaMeli,
                            i.PublicaMiCasa,i.PublicaZonaProp,i.PublicaDoomos,i.PublicaLamudi,i.PublicaGuia,
                            i.Publicagpt,i.AreaLote,i.EdadInmueble,i.usu_crea,i.RNcasa,i.IdTpInm, i.IdBarrio,
                            i.tipo_contrato,i.idProcedencia,i.RIdnd,i.IdInmobiliaria,i.RCcuadras,i.RZhabitat
                            from inmuebles i $tabla
                            where IdInmobiliaria=" . $_SESSION["IdInmmo"] .
            $enlace . $cond . $sWhere . $condOrder . $sLimit);

        // echo "<pre>";
        // print_r("SELECT i.idInm, i.Direccion, i.ValorVenta, i.ValorCanon, i.IdGestion,
        //                     i.idEstadoinmueble,i.Administracion, i.FConsignacion, i.restricciones,i.RLaGuia,
        //                     i.RVivaReal,i.RZonaProp,i.RMtoCuadrado,i.Estrato,i.AreaConstruida ,
        //                     i.NumLlaves,i.NumCasillero,i.descripcionlarga,i.PublicaM2,i.PublicaMeli,
        //                     i.PublicaMiCasa,i.PublicaZonaProp,i.PublicaDoomos,i.PublicaLamudi,i.PublicaGuia,
        //                     i.Publicagpt,i.AreaLote,i.EdadInmueble,i.usu_crea,i.RNcasa,i.IdTpInm, i.IdBarrio,
        //                     i.tipo_contrato,i.idProcedencia,i.RIdnd,i.IdInmobiliaria,i.RCcuadras
        //                     from inmuebles i $tabla
        //                     where IdInmobiliaria=" . $_SESSION["IdInmmo"] .
        //     $enlace . $cond . $sWhere . $condOrder . $sLimit);
        // echo "</pre>";die;

        $sql = "SELECT i.idInm, i.Direccion, i.ValorVenta, i.ValorCanon, i.IdGestion,
                            i.idEstadoinmueble,i.Administracion, i.FConsignacion, i.restricciones,i.RLaGuia,
                            i.RVivaReal,i.RZonaProp,i.RMtoCuadrado,i.Estrato,i.AreaConstruida ,
                            i.NumLlaves,i.NumCasillero,i.descripcionlarga,i.PublicaM2,i.PublicaMeli,
                            i.PublicaMiCasa,i.PublicaZonaProp,i.PublicaDoomos,i.PublicaLamudi,i.PublicaGuia,
                            i.Publicagpt,i.AreaLote,i.EdadInmueble,i.usu_crea,i.RNcasa,i.IdTpInm, i.IdBarrio,
                            i.tipo_contrato,i.idProcedencia,i.RIdnd,i.IdInmobiliaria,i.RCcuadras,i.RZhabitat
                            from inmuebles i $tabla
                            where IdInmobiliaria=:idinmo " .
            $enlace . $cond . "
                            limit $offset,50";
        //echo $sql;
        //print_r($data);
        //echo $_SESSION['Id_Usuarios']."--".$data["verAsesor"];
        //exit;

        // $stmt->bindParam(':idinmo',$_SESSION["IdInmmo"]);

        //$stmt->bindParam(':idinmo',$id);
        if ($data['idInmu']) {
            $stmt->bindParam(':codinmu', $data['idInmu']);
            $stmt->bindParam(':codinmu1', $data['idInmu']);
            $stmt->bindParam(':codinmu2', $data['idInmu']);
            $stmt->bindParam(':codinmu3', $data['idInmu']);
            $stmt->bindParam(':codinmu4', $data['idInmu']);
            $stmt->bindParam(':codinmu5', $data['idInmu']);
            $stmt->bindParam(':codinmu6', $data['idInmu']);
        }
        if ($data['ciudad']) {
            $stmt->bindParam(':ciudad', $data['ciudad']);
        }
        if ($data['estadoInm']) {
            $stmt->bindParam(':estadoInm', $data['estadoInm']);
        }
        if ($data['gestion']) {
            $stmt->bindParam(':gestion', $data['gestion']);
        }
        if ($data['tContrato']) {
            $stmt->bindParam(':tContrato', $data['tContrato']);
        }
        if ($data['tInmueble']) {
            $stmt->bindParam(':tInmueble', $data['tInmueble']);
        }
        if ($data['barrio']) {
            $stmt->bindParam(':barrio', $data['barrio']);
        }
        if ($data['consignado']) {
            $stmt->bindParam(':consignado', $data['consignado']);
        }
        if (!empty($data['ffecha_ini']) and !empty($data['ffecha_fin'])) {
            $stmt->bindParam(':fecha_inis', $data['ffecha_ini']);
            $stmt->bindParam(':fecha_fins', $data['ffecha_fin']);
        }

        if ( !empty($data['ffecha_ini']) and empty($data['ffecha_fin']) ) {
            
            $stmt->bindParam(':fecha_inis', $data['ffecha_ini']);
        }

        if ( empty($data['ffecha_ini']) and !empty($data['ffecha_fin']) ) {
            $stmt->bindParam(':ffecha_fin', $data['ffecha_fin']);
        }


        if ($data['verAsesor'] == 1) {
            $stmt->bindParam(':prom', $_SESSION['Id_Usuarios']);
        }
        if ($stmt->execute()) {

            /*datatable*/

            $datas = array();
            $connPDO->exec('chartset="utf-8"');
            $cantidad = $stmt->rowCount();
            while ($row = $stmt->fetch()) {
                $catidadResgistros     = count($row);
                $estadoInmueble = ucwords(strtolower(getCampo('estado_inmueble',' where idestado = ' . $row['idEstadoinmueble'],'descripcion')));
                $existeFoto            = $this->VerificaFotos($row['idInm']);
                $validgestioncontactos = $this->validateGestionContactos($row['RMtoCuadrado']);
                $idciud                = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], "IdCiudad");
                $btnPortales           = ($existeFoto > 0) ? '<button data-placement="bottom" title="Sincronizar a Portales" type="button" class="btn btn-warning  btnSincro  btn-xs btns "  data-toggle="modal"  data-target="#sincroPorta"><span class="glyphicon glyphicon"><i class="fa fa-upload"></i></span></button>' : '<label>Agregue Fotos para Publicar</label>';
                $btnPortales           = ($row['idEstadoinmueble'] == 2) ? $btnPortales : '';
                $btnPagos              = '<button data-placement="bottom" title="Portales" type="button" id="pagos" class="btn btn-info  pagos  btn-xs btns"  data-toggle="modal"  data-target=".mymodalpagos"><span class="glyphicon glyphicon"><i class="fa fa-files-o"></i></span></button>';
                $btnficha              = '<a href="../../mcomercialweb/inmuebles/buscainmueblesnvo2.php?codinmueble=' . $row['idInm'] . '"><button type="button" id="ficha" data-placement="bottom" title="Ver Detalles" class="btn btn-info  ficha  btn-xs btns"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button></a>';
                if ($get['ctr'] == '1') {
                    $ctr = '&ctr=1';
                } else {
                    $ctr = "";
                }
                if($get['ctr']!=1)
                {
                    if (getCampo('habilita_progs', "where inmob_pg=" . $row['IdInmobiliaria'] . " and prog_pg=1", 'prog_pg') == 1) 
                    {
                    $btnEditar = '<a href="../../mwc/inmuebles/ubicacionDinamic.php?inm=' . $row['codinm'] . $ctr . '"><button type="button" id="edita" class="btn btn-watermelon  edita  btn-xs btns" data-placement="bottom" title="Editar Inmueble" ><span class="glyphicon glyphicon"><i class="fa fa-cogs"></i></span></button></a>';
                    }
                    else
                    {
                            $btnEditar = '<a href="../../mwc/inmuebles/ubicacionDinamic.php?inm=' . $row['codinm'] . $ctr . '"><button type="button" id="edita" class="btn btn-watermelon  edita  btn-xs btns" data-placement="bottom" title="Editar Inmueble" ><span class="glyphicon glyphicon"><i class="fa fa-cogs"></i></span></button></a>';
                    }
                
                } else {
                    $btnEditar = '<a href="../../mcomercialweb/EditarInmueble.php?inm=' . $row['idInm'] . $ctr . '"><button type="button" id="edita" class="btn btn-watermelon  edita  btn-xs btns" data-placement="bottom" title="Editar Inmueble" ><span class="glyphicon glyphicon"><i class="fa fa-cogs"></i></span></button></a>';
                }

                // $btnReversar = '<a href="../../mcomercialweb/reversar_inmueble.php?regresa=1&Inmueble=' . $row['idInm'] . '&nvoi=1"><button type="button" id="reversar" class="btn btn-spartan  reversar  btn-xs btns" data-placement="bottom" title="Cambiar Estado" ><span class="glyphicon glyphicon"><i class="fa fa-edit"></i></span></button></a>';
                $btnReversar = '<button data-inm="'.$row['idInm'].'" data-estadoInm="'.$estadoInmueble.'" data-idestado="'.$row['idEstadoinmueble'].'" type="button" id="reversar" class="btn btn-spartan cambiarestado  btn-xs btns" data-placement="bottom" title="Cambiar Estado" ><span class="glyphicon glyphicon"><i class="fa fa-edit"></i></span></button>';
                if (($row['IdGestion'] == 5 || $row['IdGestion'] == 2) && $row['idEstadoinmueble'] != 3) {
                    $btnVender = '<a href="../../mcomercialweb/vender_inmueble.php?regresa=1&Inmueble=' . $row['idInm'] . '"><button type="button" id="vender" class="btn btn-info  vender  btn-xs btns" data-placement="bottom" title="Vender Inmueble" ><span class="glyphicon glyphicon"><i class="fa fa-money"></i></span></button></a>';
                }

                //$btnficha='<a href="../../mcomercialweb/buscainmueblesnvo2.php?codinmueble='.$row['idInm'].'"><button type="button" id="ficha" class="btn btn-info  ficha  btn-xs btns"  ><span class="glyphicon glyphicon"><i class="fa fa-home"></i></span></button></a>';
                $inmoExplo = explode("-",$row['idInm']);
                $btnDuplicar        = '<button title="Duplicar Inmueble" type="button" id="ficha" class="btn btn-info  ficha  btn-xs btns btn-duplicarInm" data-inmo="'.$inmoExplo[0].'" data-inmu="'.$inmoExplo[1].'" ><span class="glyphicon glyphicon"><i class="fa fa-files-o" data-placement="bottom" ></i></span></button>';//onclick="AbrirVentana('."'".$row['idInm']."'".')"
                $btnOrdenar         = '<button  data-placement="bottom" title="Ordenar Fotos" type="button" id="ficha" class="btn btn-purple  ficha  btn-xs btns" onclick="AbrirVentanaOrdenar(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-camera-retro"></i></span></button>';
                $btnObprop          = '<button  data-placement="bottom" title="Observaciones Propietarios" type="button" id="ficha" class="btn btn-warning btn-propietario  ficha  btn-xs btns" ><span class="glyphicon glyphicon"><i class="fa fa-edit"></i></span></button>';
                $btnVerFicha        = '<button  data-placement="bottom" title="Ver Ficha" type="button" id="ficha" class="btn btn-default  ficha  btn-xs btns" onclick="AbrirVentanaFicha(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-file-photo-o"></i></span></button>';
                $btnVerFichaChicala = ($row['IdInmobiliaria'] == 172 || $row['IdInmobiliaria'] == 1) ? '<button  data-placement="bottom" title="Ver Ficha Chicala" type="button" id="ficha" class="btn btn-success  ficha  btn-xs btns" onclick="AbrirVentanaFichaCh(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-file-picture-o"></i></span></button>' : '';
                $btnVerFichaGomez   = ($row['IdInmobiliaria'] == 447 || $row['IdInmobiliaria'] == 1) ? '<button  data-placement="bottom" title="Ver Ficha Gomez" type="button" id="ficha" class="btn btn-primary  ficha  btn-xs btns" onclick="AbrirVentanaFichaGm(' . "'" . $row['idInm'] . "'" . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-file-image-o"></i></span></button>' : '';
                if ($validgestioncontactos == 1) {
                    $btngestioncontactos = '<button  data-placement="bottom" title="Gestionar Contactos" type="button" id="ficha" data-inmom2="' . $row['RMtoCuadrado'] . '" class="btn btn-primary  ficha  btn-xs btns ajaxGestionContactos"><span class="glyphicon glyphicon"><i class="fa fa-envelope" aria-hidden="true"></i></span></button>';
                } else {
                    $btngestioncontactos = "";
                }
                $inputInmu = '<input type="hidden" id="idInmu" name="idInmu" value="' . $row['idInm'] . '" >';

                ///validar cadena de pruebas metro
                $spruebas = getCampo('usuariosm2', "where inmogm2=" . $row['IdInmobiliaria'] . " and servidor=2 limit 0,1", "servidor");

                $btnCadenaMetro = ($this->verifyPortales($row['IdInmobiliaria'], 10) > 0 && $spruebas == 2) ? '<button  data-placement="bottom" title="Generar Prueba Metro" type="button" id="cadenaM2" class="btn btn-warning  ficha  btn-xs btns" onclick="AbrirCadena(' . "'" . $row['idInm'] . "'" . ',' . "'" . $row['RMtoCuadrado'] . "'" . ',' . getCampo('usuariosm2', "where inmogm2=" . $row['IdInmobiliaria'] . " limit 0,1", "idusariom2") . ')"  ><span class="glyphicon glyphicon"><i class="fa fa-link"></i></span></button>' : '';

                $usucrea     = ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $row['usu_crea'] . "' and Id_Usuarios>0", 'concat(Nombres," ",apellidos)')));
                $cadPortales = '<ul>';
                $cadPortales .= "<li><b>" . $row['idInm'] . "</b></li>";
                if ($row['RLaGuia'] > 0) {
                    $cadPortales .= "<li>FincaRaiz " . $row['RLaGuia'] . "</li>";
                }
                if ($row['RMtoCuadrado'] != "" && $row['RMtoCuadrado'] != 0) {
                    $cadPortales .= "<li>Metro2 " . $row['RMtoCuadrado'] . "</li>";
                }
                if ($row['RNcasa'] > 0) {
                    $cadPortales .= "<li>Nuestra Casa " . $row['RNcasa'] . "</li>";
                }
                if ($row['RIdnd'] > 0) {
                    $cadPortales .= "<li>Idonde " . $row['RIdnd'] . "</li>";
                }
                if ($row['RVivaReal'] > 0) {
                    $cadPortales .= "<li>Interno " . $row['RVivaReal'] . "</li>";
                }
                if ($row['RZonaProp'] > 0) {
                    $cadPortales .= "<li>I24 " . $row['RZonaProp'] . "</li>";
                }
                // if ($row['RZhabitat'] != '') {
                //     $cadPortales .= "<li>Zona Habitat " . $row['RZhabitat'] . "</li>";
                // }
                if ($row['PublicaDoomos'] == 'Publicado') {
                    $cadPortales .= "<li>Gratuitos " . "</li>";
                }
                if ($row['PublicaLamudi'] == 'Publicado') {
                    $cadPortales .= "<li>Lamudi " . $row['PublicaLamudi'] . "</li>";
                }
                if ($row['RCcuadras'] > 0) {
                    $cadPortales .= "<li>Ciencuadras " . $row['RCcuadras'] . "</li>";
                }
                if ($row['Publicagpt'] == 'Publicado') {
                    $cadPortales .= "<li>GoPlaceit " . $row['Publicagpt'] . "</li>";
                }
                $flag = getCampo('clientessimi', "where IdInmobiliaria=" . $_SESSION['IdInmmo'], 'flag_portal');
                if ($flag == 10) {
                    $cadPortales = '<ul>';
                    $cadPortales .= "<li>Metro2 " . $row['RMtoCuadrado'] . "</li>";
                    $cadenaBotones = $btnCadenaMetro;
                } elseif ($flag == 8) {
                    $cadPortales = '<ul>';
                    $cadPortales .= "<li>FincaRaiz " . $row['RLaGuia'] . "</li>";
                } else {
                    $cadenaBotones = $btnEditar . $btnReversar . $btnPortales . $btnOrdenar . $btnVerFicha . $btnVerFichaChicala . $btnVerFichaGomez . $btnDuplicar . $btnCadenaMetro . $btnObprop . $btngestioncontactos . $inputInmu;
                }

                $cadPortales .= '</ul>';

                $datas[] = array(
                    "idInm"          => $cadPortales,
                    "Direccion"      => $row['Direccion'],
                    "ValorVenta"     => number_format($row['ValorVenta']),
                    "ValorCanon"     => number_format($row['ValorCanon']),
                    "Gestion"        => ucwords(strtolower(getCampo('gestioncomer', "where IdGestion=" . $row['IdGestion'], "NombresGestion"))),
                    "procedencia"    => ucwords(strtolower(getCampo('procedencia', "where IdProcedencia=" . $row['idProcedencia'], "Nombre"))),
                    "tcontrato"      => ucwords(strtolower(getCampo('tipo_contrato', "where id_contrato=" . $row['tipo_contrato'], "tipo_contrato"))),
                    "TipoInm"        => ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble=" . $row['IdTpInm'], "Descripcion"))),
                    "Barrio"         => ucwords(strtolower(getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], "NombreB"))),
                    "ciudad"         => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . $idciud, "Nombre"))),
                    "Estadoinmueble" => ucwords(strtolower(getCampo('estado_inmueble', "where idestado=" . $row['idEstadoinmueble'], 'descripcion'))),
                    "Administracion" => $row['Administracion'],
                    "FConsignacion"  => $row['FConsignacion'],
                    "Estrato"        => $row['Estrato'],
                    "AreaConstruida" => $row['AreaConstruida']." m2",
                    "AreaLote"       => $row['AreaLote']." m2",
                    "EdadInmueble"   => $row['EdadInmueble'],
                    "usu_crea"       => $usucrea,
                    "RNcasa"         => $row['RNcasa'],
                    "bnts"           => $cadenaBotones,


                );

            }

            /*datatable*/
            /* Data set length after filtering */
            $sQuery = "
                SELECT FOUND_ROWS() FROM   inmuebles as i where IdInmobiliaria  =" . $_SESSION['IdInmmo'] . $sWhere;

            $stmtFound = $connPDO->prepare($sQuery);
            if ($dataN['verAsesor'] == 1) {
                $stmtFound->bindParam(':prom', $_SESSION['Id_Usuarios']);
                //echo "entrto";

            }
            //echo $sQuery."--".$_SESSION['Id_Usuarios'];exit;
            $stmtFound->execute();

            $iFilteredTotal = $stmtFound->fetch();

            /* Total data set length */
            $sQuery = "
                SELECT COUNT(*) as total
                FROM   inmuebles as i where IdInmobiliaria=" . $_SESSION["IdInmmo"] .
                $enlace . $cond . $sWhere;

            $stmtcount = $connPDO->prepare($sQuery);
            /*if($dataN['verAsesor']==1)
            {
            $stmtcount->bindParam(':prom',$_SESSION['Id_Usuarios']);
            }*/

            if ($data['idInmu']) {
                $stmtcount->bindParam(':codinmu', $data['idInmu']);
                $stmtcount->bindParam(':codinmu1', $data['idInmu']);
                $stmtcount->bindParam(':codinmu2', $data['idInmu']);
                $stmtcount->bindParam(':codinmu3', $data['idInmu']);
                $stmtcount->bindParam(':codinmu4', $data['idInmu']);
                $stmtcount->bindParam(':codinmu5', $data['idInmu']);
                $stmtcount->bindParam(':codinmu6', $data['idInmu']);
            }
            if ($data['ciudad']) {
                $stmtcount->bindParam(':ciudad', $data['ciudad']);
            }
            if ($data['estadoInm']) {
                $stmtcount->bindParam(':estadoInm', $data['estadoInm']);
            }
            if ($data['gestion']) {
                $stmtcount->bindParam(':gestion', $data['gestion']);
            }
            if ($data['tContrato']) {
                $stmtcount->bindParam(':tContrato', $data['tContrato']);
            }
            if ($data['tInmueble']) {
                $stmtcount->bindParam(':tInmueble', $data['tInmueble']);
            }
            if ($data['barrio']) {
                $stmtcount->bindParam(':barrio', $data['barrio']);
            }
            if ($data['consignado']) {
                $stmtcount->bindParam(':consignado', $data['consignado']);
            }
            if ($data['ffecha_ini'] > 0 and $data['ffecha_fin'] > 0) {
                $stmtcount->bindParam(':fecha_inis', $data['ffecha_ini']);
                $stmtcount->bindParam(':fecha_fins', $data['ffecha_fin']);
            }
            if ($data['verAsesor'] == 1) {
                $stmtcount->bindParam(':prom', $_SESSION['Id_Usuarios']);
            }

            $stmtcount->execute();
            $rResultTotal = $stmtcount->fetchAll();
            //print_r($rResultTotal[0]["total"]);
            $iTotal = $rResultTotal[0]["total"];
//echo $iTotal;
            /*
             * Output
             */
            $output = array(
                "sEcho"                => intval($data['sEcho']),
                "iTotalRecords"        => $iTotal,
                "iTotalDisplayRecords" => $iTotal, //$iFilteredTotal[0],
                "aaData"               => $datas,
            );

            //$output['aaData'] = $data;
            return $output;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }

    private function validateGestionContactos($inmueble)
    {
        $connPDO = new Conexion();

        $sql = "SELECT usuariom2,clavem2,prefijom2,servidor,letra FROM usuariosm2 WHERE inmogm2 = :inmo  and servidor=1 LIMIT 0,1";

        $stmt = $connPDO->prepare($sql);

        $stmt->bindParam(':inmo', $_SESSION["IdInmmo"]);

        if ($stmt->execute()) {
            $existe = $stmt->rowCount();
            return $existe;
        } else {
            return print_r($stmt->errorInfo());
        }

    }
    public function getCaracteristicasInmueble($data)
    {
        $connPDO = new Conexion();

        $tpInm   = $data['tpInm'];
        $idGrupo = $data['IdGrupo'];
        $tpCar   = $data['tpCar'];
        $stmt    = $connPDO->prepare("SELECT n.idCaracteristica,n.idTipoInmueble,n.IdGrupo,n.Descripcion,n.tpcar,g.DETALLE
                    FROM maestrodecaracteristicasnvo n,grupocaracteristicas g
                    WHERE n.IdGrupo=g.IdGrupo
                    AND tpcar               =:tpCar
                    AND n.idTipoInmueble    =:tpInm
                    AND n.IdGrupo           =:idGrupo
                    and g.car_est in (1,2)
                    ORDER BY DETALLE,Descripcion");
        //AND g.car_est            =1
        $stmt->bindParam(":tpCar", $tpCar);
        $stmt->bindParam(":tpInm", $tpInm);
        $stmt->bindParam(":idGrupo", $idGrupo);
        if ($stmt->execute()) {
            $data = array();

            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "idCaracteristica" => $row["idCaracteristica"],
                    "nCaracteristica"  => ucwords(strtolower($row["Descripcion"])),
                    "ngrupo"           => ucwords(strtolower($row["DETALLE"])),
                    "idTipoInmueble"   => $row["idTipoInmueble"],
                );
            }
            return $data;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function getCaracteristicasInmuebleLista($data)
    {
        $connPDO = new Conexion();

        $tpInm = $data['tpInm'];
        $tpCar = $data['tpCar'];
        $i     = 0;
        if ($data['cambioTipo'] == 1) {

            Inmuebles::deleteAdicionalesInmueble($data['IdInmobiliaria'], $data['codinm']);
            Inmuebles::deleteCaracteristicasLateral($data['IdInmobiliaria'], $data['codinm']);
        }
        $stmt = $connPDO->prepare("SELECT n.idCaracteristica,n.idTipoInmueble,n.IdGrupo as grup,n.Descripcion,n.tpcar,g.DETALLE
                    FROM maestrodecaracteristicasnvo n,grupocaracteristicas g
                    WHERE n.IdGrupo=g.IdGrupo
                    AND tpcar               =:tpCar
                    AND n.idTipoInmueble    =:tpInm
                    and g.car_est in (1,2)
                    GROUP BY n.IdGrupo
                    ORDER BY DETALLE,Descripcion ");
        //AND g.car_est            =1
        $stmt->bindParam(":tpCar", $tpCar);
        $stmt->bindParam(":tpInm", $tpInm);
        $data = '';
        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $i++;
                $data .=
                '<ul class="listaCaracteristica"><li><div class="treeopen" data-item="#tree' . $tpCar . $i . '"><div><label><i class="fa fa-plus" aria-hidden="true"></i> ' . utf8_encode($row['DETALLE']) . ' </label></div></div>' . $this->getCaracteristicasInmuebleListaHijos($row['grup'], $tpInm, $tpCar, $i) . '</li></ul>';
            }
            return $data;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function getCaracteristicasInmuebleListaHijos($IdGrupo, $tpInm, $tpCar, $i)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT n.idCaracteristica,n.idTipoInmueble,n.IdGrupo,n.Descripcion,n.tpcar
                    FROM maestrodecaracteristicasnvo n,grupocaracteristicas g
                    WHERE n.IdGrupo         =:IdGrupo
                    and n.IdGrupo=g.IdGrupo
                    AND n.idTipoInmueble    =:tpInm
                    AND tpcar               =:tpCar
                    and g.car_est in (1,2)
                    ORDER BY Descripcion ");
        $stmt->bindParam(":IdGrupo", $IdGrupo);
        $stmt->bindParam(":tpInm", $tpInm);
        $stmt->bindParam(":tpCar", $tpCar);
        if ($stmt->execute()) {
            $info = '<ul id="tree' . $tpCar . $i . '">';

            while ($row = $stmt->fetch()) {
                $info .= '<li><div><div class="checkbox"><label><input type="checkbox" name="checkCar" class="checkCar" data-text="#text' . $row['idCaracteristica'] . '" data-target="#ch' . $row['idCaracteristica'] . '" value=""> <input type="hidden"   value="0" name="checkCarVal[' . $row['idCaracteristica'] . ']" id="ch' . $row['idCaracteristica'] . '">' . strtoupper(utf8_encode($row['Descripcion'])) . '</label><input type="text"  name="caracteristica[' . $row['idCaracteristica'] . ']" class="form-control pull-right" readonly id="text' . $row['idCaracteristica'] . '"></div></div></li>';
            }
            $info .= '</ul>';
            return $data;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function getCaracteristicasInmuebleListaEditar($data)
    {
        $connPDO = new Conexion();

        $tpInm = $data['tpInm'];
        $tpCar = $data['tpCar'];
        $i     = 0;
        if ($data['cambioTipo'] == 1) {

            Inmuebles::deleteAdicionalesInmueble($data['IdInmobiliaria'], $data['codinm']);
            Inmuebles::deleteCaracteristicasLateral($data['IdInmobiliaria'], $data['codinm']);
            Inmuebles::updateTpInmOld($data['IdInmobiliaria'], $data['codinm'], $tpInm);
        }

        $stmt = $connPDO->prepare("SELECT n.idCaracteristica,n.idTipoInmueble,n.IdGrupo as grup,n.Descripcion,n.tpcar,g.DETALLE
                    FROM maestrodecaracteristicasnvo n,grupocaracteristicas g
                    WHERE n.IdGrupo=g.IdGrupo
                    AND tpcar               =:tpCar
                    AND n.idTipoInmueble    =:tpInm
                    and g.car_est in (1,2)
                    GROUP BY n.IdGrupo
                    ORDER BY DETALLE,Descripcion ");
        //AND g.car_est         =1
        $stmt->bindParam(":tpCar", $tpCar);
        $stmt->bindParam(":tpInm", $tpInm);
        $info = '';
        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $i++;
                $info .=
                '<ul class="listaCaracteristica"><li><div class="treeopen" data-item="#tree' . $tpCar . $i . '"><div><label><i class="fa fa-plus" aria-hidden="true"></i> ' . utf8_encode($row['DETALLE']) . ' </label></div></div>' . $this->getCaracteristicasInmuebleListaHijosEditar($row['grup'], $tpInm, $tpCar, $i, $data) . '</li></ul>';
            }
            return $info;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function getCaracteristicasInmuebleListaHijosEditar($IdGrupo, $tpInm, $tpCar, $i, $data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT n.idCaracteristica,n.idTipoInmueble,n.IdGrupo,n.Descripcion,n.tpcar,n.flag_cant
                    FROM maestrodecaracteristicasnvo n,grupocaracteristicas g
                    WHERE n.IdGrupo         =:IdGrupo
                    and n.IdGrupo=g.IdGrupo
                    AND n.idTipoInmueble    =:tpInm
                    AND tpcar               =:tpCar
                    and g.car_est in (1,2)
                    ORDER BY Descripcion ");
        $stmt->bindParam(":IdGrupo", $IdGrupo);
        $stmt->bindParam(":tpInm", $tpInm);
        $stmt->bindParam(":tpCar", $tpCar);
        if ($stmt->execute()) {
            $seleccionado = '';
            $reado        = '';
            $info         = '<ul id="tree' . $tpCar . $i . '">';

            while ($row = $stmt->fetch()) {
                $flag_cantidad = $row['flag_cant'];
                if ($flag_cantidad == 1) {
                    $readoC  = '';
                    $enabled = 1;
                } else {
                    $readoC  = 'readonly';
                    $enabled = 0;
                }
                $valores = Inmuebles::getDataDetalleInmueble($data, $row['idCaracteristica']);
                foreach ($valores as $key => $value) {
                }
                if ($value['idcaracteristica'] == $row['idCaracteristica']) {
                    $seleccionado = 'checked';
                    $reado        = '';
                    $detalle      = utf8_encode($value['obser_det']);
                    $cantidad     = $value['cantidad_dt'];
                    $chequeado    = 1;
                } else {
                    $seleccionado = '';
                    $reado        = 'readonly';
                    $detalle      = '';
                    $chequeado    = 0;
                }
                $info .= '<li><div><div class="checkbox"><label ><input type="checkbox" name="checkCar" class="checkCar inputChange" data-text="#text' . $row['idCaracteristica'] . '" data-text2="#cant' . $row['idCaracteristica'] . '" data-target="#ch' . $row['idCaracteristica'] . '" value="" ' . $seleccionado . '> <input type="hidden"   value="' . $chequeado . '" name="checkCarVal[' . $row['idCaracteristica'] . ']" id="ch' . $row['idCaracteristica'] . '">' . strtoupper(utf8_encode($row['Descripcion'])) . '</label><input data-enabled="' . $enabled . '" class="form-control pull-left limitar inputChange onlyNum ct" type="text" name="cantCar[' . $row['idCaracteristica'] . ']" id="cant' . $row['idCaracteristica'] . '" ' . $readoC . ' value="' . $cantidad . '" placeholder="Cant" ><input type="text"  name="caracteristica[' . $row['idCaracteristica'] . ']" class="form-control inputChange pull-left limitar dt" ' . $reado . ' id="text' . $row['idCaracteristica'] . '" value="' . $detalle . '" placeholder="Complemento"></div></div></li>';
            }
            $info .= '</ul>';
            return $info;

        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function ubicacionInmuebleNew($data)
    {
        $connPDO = new Conexion();

        $Idinmobiliaria = $data['IdInmobiliaria'];

        $idInm    = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $estadoin = (getCampo('inmobiliaria', "where Idinmobiliaria=" . $data['IdInmobiliaria'], 'aestado') == 1) ? 8 : 2;

        $stmt = $connPDO->prepare("UPDATE inmnvo
            set
            IdBarrio           = :barrio,
            Direccion          = :dircompleta,
            DirMetro           = :DirMetro,
            Carrera            = :carrera,
            Calle              = :calle,
            latitud            = :latitud,
            dirs               = :dirs,
            longitud           = :longitud,
            permite_geo        = :permite_geo,
            idEstadoinmueble   = :estadoin
            WHERE codinm       = :codinm
            AND IdInmobiliaria = :idinmobiliaria");

        if ($stmt->execute(array(
            ":barrio"         => $data['IdBarrio'],
            ":dircompleta"    => str_ireplace("#", "", $data['dircompleta1']),
            ":DirMetro"       => str_ireplace("#", "", $data['DirMetro']),
            ":carrera"        => $data['carrera'],
            ":calle"          => $data['calle'],
            ":latitud"        => $data['latitud'],
            ":longitud"       => $data['longitud'],
            ":permite_geo"    => $data['permite_geo'],
            ":estadoin"       => $estadoin,
            ":codinm"         => $data['codinm'],
            ":idinmobiliaria" => $data['IdInmobiliaria'],
            ":dirs"           => $data['dirs'],
        ))) {

            $this->ubicacionInmuebleOld($data);
            return 1;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function ubicacionInmuebleOld($data)
    {
        $connPDO      = new Conexion;
        $dircompleta  = strtoupper(str_ireplace("#", "", $data['dircompleta']));
        $dircompleta1 = strtoupper($data['dircompleta1']);
        $estadoin     = (getCampo('inmobiliaria', "where Idinmobilairia=" . $data['IdInmobiliaria'], 'aestado') == 1) ? 8 : 2;
        $barrio       = $data['barrio'];
        $fechaserv    = date("Y-m-d");
        $permitegeo   = $data['permitegeo'];
        $idInm        = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $connPDO->prepare("UPDATE inmuebles
            set
            permite_geo        = :permite_geo,
            IdBarrio           = :barrio,
            Direccion          = :dircompleta,
            DirMetro           = :DirMetro,
            latitud            = :latitud,
            longitud           = :longitud,
            Calle              = :calle,
            Carrera            = :carrera,
            idEstadoinmueble   = :estadoin,
            DirMetro           = :DirMetro
            WHERE codinm       = :codinm
            AND IdInmobiliaria = :idinmobiliaria");

        if ($stmt->execute(array(
            ":barrio"         => $data['IdBarrio'],
            ":dircompleta"    => str_ireplace("#", "", $data['dircompleta1']),
            ":direccion2"     => str_ireplace("#", "", $data['direccion2']),
            ":DirMetro"       => str_ireplace("#", "", $data['DirMetro']),
            ":carrera"        => $data['carrera'],
            ":calle"          => $data['calle'],
            ":latitud"        => $data['latitud'],
            ":longitud"       => $data['longitud'],
            ":permite_geo"    => $data['permite_geo'],
            ":estadoin"       => $estadoin,
            ":codinm"         => $data['codinm'],
            ":idinmobiliaria" => $data['IdInmobiliaria'],
        ))) {
            Inmuebles::logUbicacion($data, $idInm);
            return 1;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }

    public function logUbicacion($data, $idInm)
    {
        $f_sis     = date('Y-m-d');
        $h_sis     = date('H:i:s');
        $ip        = $_SERVER['REMOTE_ADDR'];
        $IdDemanda = $idInm;
        $connPDO   = new Conexion;
        $stmt      = $connPDO->prepare("SELECT IdBarrio,Direccion,DirMetro,latitud,
                        longitud,fmodificacion,Calle,permite_geo,Carrera
                        FROM inmuebles
                        WHERE idInm=:idInm");
        $stmt->bindParam(':idInm', $idInm);

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $IdBarrio_or    = $row['IdBarrio'];
                $Direccion_or   = $row['Direccion'];
                $DirMetro_or    = $row['DirMetro'];
                $latitud_or     = $row['latitud'];
                $longitud_or    = $row['longitud'];
                $Calle_or       = $row['Calle'];
                $permite_geo_or = $row['permite_geo'];
                $Carrera_or     = $row['Carrera'];

            }
            if ($IdBarrio_or != $data['IdBarrio']) {
                $cambio++;
                $nom_campo   = "IdBarrio";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "Barrio"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['IdBarrio'], $IdBarrio_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($Direccion_or != $data['Direccion']) {
                $cambio++;
                $nom_campo   = "Direccion";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "Direccion"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['Direccion'], $Direccion_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($DirMetro_or != $data['DirMetro']) {
                $cambio++;
                $nom_campo   = "DirMetro";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "DirMetro"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['DirMetro'], $DirMetro_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($latitud_or != $data['latitud']) {
                $cambio++;
                $nom_campo   = "latitud";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "latitud"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['latitud'], $latitud_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($longitud_or != $data['longitud']) {
                $cambio++;
                $nom_campo   = "longitud";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "longitud"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['longitud'], $longitud_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($Calle_or != $data['calle']) {
                $cambio++;
                $nom_campo   = "Calle";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "Calle"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['calle'], $Calle_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($permite_geo_or != $data['permite_geo']) {
                $cambio++;
                $nom_campo   = "permite_geo";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "permite_geo"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['permite_geo'], $permite_geo_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($Carrera_or != $data['carrera']) {
                $cambio++;
                $nom_campo   = "Carrera";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "Carrera"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['carrera'], $Carrera_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            return true;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }

    public function consecInmuNormal($inmob)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT consecinmueble from inmobiliaria where IdInmobiliaria=:inmob");
        $stmt->bindParam(":inmob", $inmob);
        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $response = $row['consecinmueble'];
            }
            return $response;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function consecInmuNew($inmob)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT MAX(codinm) as conse from inmnvo where IdInmobiliaria=:inmob");
        $stmt->bindParam(":inmob", $inmob);
        if ($stmt->execute()) {
            $cantidad = $stmt->rowCount();

            if ($cantidad <= 0) {
                $response = $this->consecInmuNormal($inmob);
            } else {
                while ($row = $stmt->fetch()) {
                    $response = ($this->consecInmuNormal($inmob) < $row['conse']) ? $row['conse'] : $this->consecInmuNormal($inmob);
                }
            }
            //$this->updateconsecInmu($response+1,$inmob);
            return $response + 1;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function updateconsecInmu($consec, $inmob)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("UPDATE inmobiliaria
                                    set consecinmueble=:consec
                                    where IdInmobiliaria=:inmob");
        $stmt->bindParam(":inmob", $inmob);
        $stmt->bindParam(":consec", $consec);
        if ($stmt->execute()) {
            $cantidad = $stmt->rowCount();

            if ($cantidad <= 0) {
                $response = $this->consecInmuNormal($inmob);
            } else {
                while ($row = $stmt->fetch()) {
                    $response = ($this->consecInmuNormal($inmob) < $row['conse']) ? $row['conse'] : $this->consecInmuNormal($inmob);
                }
            }
            return $response + 1;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function saveFinancialOld($data)
    {
        $connPDO = new Conexion();

        $codinm       = $this->consecInmuNew($data['IdInmobiliaria']);
        $idInm        = $data['IdInmobiliaria'] . "-" . $codinm;
        $fechaserv    = date('Y-m-d');
        $usu_creacion = $_SESSION['iduser'];
        $h_creacion   = date('Y-m-d');
        $ip           = $_SERVER['REMOTE_ADDR'];
        $estadoin     = (getCampo('inmobiliaria', "where Idinmobiliaria=" . $data['IdInmobiliaria'], 'aestado') == 1) ? 8 : 2;
        if ($_POST['destacado'] == 'on') {$destacado = 1;} else { $destacado = 0;}
        if ($data['destinacion'] == 2) {
            $valIva = $data['ValorIva'];
        } else {
            $valIva = 0;
        }
        if ($data['incluida'] == 'on') {$incluida = 1;} else { $incluida = 0;}
        if ($data['amoblado'] == 'on') {$amoblado = 1;} else { $amoblado = 0;}
        if ($data['destacado'] == 'on') {$destacado = 1;} else { $destacado = 0;}
        $stmt = $connPDO->prepare("INSERT INTO inmuebles
         (IdGestion,FConsignacion,Estrato,AreaConstruida,AreaLote,
            EdadInmueble,NoMatricula,chip,AvaluoCatastral,CedulaCatastral,
            fingreso,amoblado,restricciones,obserinterna,codinterno,
            FechaAvaluoCatastral,ValorVenta,ValorCanon,Administracion,ValorIva,
            admondto,IdProcedencia,idCaptador,IdPromotor,IdDestinacion,
            AdmonIncluida,IdTpInm,idInm,codinm,usu_creacion,
            f_creacion,h_creacion,IdInmobiliaria,idEstadoinmueble,RefInmueble,sede,Telefonoinm,uso)
            VALUES
            (:gestioncomercial,:fecha_inicio,:estrato,:areaconstruida,:arealote,
            :anoconstruccion,:nromatri,:chip,:calledavaluocatas,:cedulacatast,
            :fingreso,:amoblado,:restricciones,:obserinterna,:codinterno,
            :Favaluo,:valventa,:canon,:admon,:valIva,
            :admondto,:procedencia,:codcaptador,:idpromotor,:destinacion,
            :incluida,:tipoinmueble,:idInm,:codinm,:usu_creacion,
            :f_creacion,:h_creacion,:IdInmobiliaria,:idEstadoinmueble,:RefInmueble,:sede,:Telefonoinm,:uso)");

        if ($stmt->execute(array(
            ":gestioncomercial"  => $data['gestioncomercial'],
            ":fecha_inicio"      => $data['fecha_inicio'],
            ":estrato"           => $data['estrato'],
            ":areaconstruida"    => str_ireplace(",", ".", $data['areaconstruida']),
            ":arealote"          => str_ireplace(",", ".", $data['arealote']),
            ":anoconstruccion"   => $data['anoconstruccion'],
            ":nromatri"          => $data['nromatri'],
            ":chip"              => $data['chip'],
            ":calledavaluocatas" => $data['calledavaluocatas'],
            ":cedulacatast"      => $data['cedulacatast'],
            ":fingreso"          => $fechaserv,
            ":amoblado"          => $amoblado,
            ":restricciones"     => $data['restricciones'],
            ":obserinterna"      => $data['obserinterna'],
            ":codinterno"        => $data['codinterno'],
            ":Favaluo"           => $data['Favaluo'],
            ":valventa"          => str_ireplace(".", "", $data['valventa']),
            ":canon"             => str_ireplace(".", "", $data['canon']),
            ":admon"             => str_ireplace(".", "", $data['admon']),
            ":valIva"            => str_ireplace(".", "", $valIva),
            ":admondto"          => str_ireplace(".", "", $data['admondto']),
            ":procedencia"       => $data['procedencia'],
            ":codcaptador"       => $data['codcaptador'][0],
            ":idpromotor"        => $data['idpromotor'][0],
            ":destinacion"       => $data['destinacion'],
            ":incluida"          => $incluida,
            ":tipoinmueble"      => $data['tipoinmueble'],
            ":idInm"             => $idInm,
            ":RefInmueble"       => $idInm,
            ":codinm"            => $codinm,
            ":IdInmobiliaria"    => $data['IdInmobiliaria'],
            ":Telefonoinm"       => $data['telInmueble'],
            ":uso"               => $data['usoInmueble'],
            ":sede"              => ($data['sede']=="")?'0':$data["sede"],
            ":usu_creacion"      => $usu_creacion,
            ":f_creacion"        => $fechaserv,
            ":h_creacion"        => $h_creacion,
            ":idEstadoinmueble"  => $estadoin,
        ))) {
            //destacados
            guardar_settings_portales($data['IdInmobiliaria'], $idInm, 0, 4, $destacado, $_SESSION['Id_Usuarios'], $fechaserv, $h_creacion, $ip);

            //amoblado
            guardar_settings_portales($data['IdInmobiliaria'], $idInm, 0, 6, $amoblado, $_SESSION['Id_Usuarios'], $fechaserv, $h_creacion, $ip);
            //saveLogFinancial($data,$idInm);
            $this->saveFinancialNew($data, $codinm);
            $inmueb          = array();
            return $inmueb[] = array("codinmu" => $codinm);
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function saveFinancialNew($data, $codinm)
    //  public function saveFinancialNew($data)
    {
        $connPDO = new Conexion();

        //$codinm         = $this->consecInmuNew($data['IdInmobiliaria']);
        $idInm        = $data['IdInmobiliaria'] . "-" . $codinm;
        $fechaserv    = date('Y-m-d');
        $usu_creacion = $_SESSION['iduser'];
        $h_creacion   = date('Y-m-d');
        $ip           = $_SERVER['REMOTE_ADDR'];
        if ($_POST['destacado'] == 'on') {$destacado = 1;} else { $destacado = 0;}
        if ($data['destinacion'] == 2) {
            $valIva = $data['ValorIva'];
        } else {
            $valIva = 0;
        }
        if ($data['incluida'] == 'on') {$incluida = 1;} else { $incluida = 0;}

        $stmt = $connPDO->prepare("INSERT INTO inmnvo
         (IdGestion,FConsignacion,Estrato,AreaConstruida,AreaLote,
            EdadInmueble,NoMatricula,chip,AvaluoCatastral,CedulaCatastral,
            fingreso,restricciones,obserinterna,codinterno,FechaAvaluoCatastral,
            ValorVenta,ValorCanon,Administracion,ValorIva,admondto,
            IdProcedencia,IdDestinacion,AdmonIncluida,IdTpInm,idInm,
            codinm,usu_creacion,f_creacion,h_creacion,IdInmobiliaria,sede,Telefonoinm,uso)
            VALUES
            (:gestioncomercial,:fecha_inicio,:estrato,:areaconstruida,:arealote,
            :anoconstruccion,:nromatri,:chip,:calledavaluocatas,:cedulacatast,
            :fingreso,:restricciones,:obserinterna,:codinterno,:Favaluo,
            :valventa,:canon,:admon,:valIva,:admondto,
            :procedencia,:destinacion,:incluida,:tipoinmueble,:idInm,
            :codinm,:usu_creacion,:f_creacion,:h_creacion,:IdInmobiliaria,:sede,:Telefonoinm,:uso)");

        if ($stmt->execute(array(
            ":gestioncomercial"  => $data['gestioncomercial'],
            ":fecha_inicio"      => $data['fecha_inicio'],
            ":estrato"           => $data['estrato'],
            ":areaconstruida"    => str_ireplace(".", "", $data['areaconstruida']),
            ":arealote"          => str_ireplace(".", "", $data['arealote']),
            ":anoconstruccion"   => $data['anoconstruccion'],
            ":nromatri"          => $data['nromatri'],
            ":chip"              => $data['chip'],
            ":calledavaluocatas" => $data['calledavaluocatas'],
            ":cedulacatast"      => $data['cedulacatast'],
            ":fingreso"          => $fechaserv,
            ":restricciones"     => $data['restricciones'],
            ":obserinterna"      => $data['obserinterna'],
            ":codinterno"        => $data['codinterno'],
            ":Favaluo"           => $data['Favaluo'],
            ":valventa"          => str_ireplace(".", "", $data['valventa']),
            ":canon"             => str_ireplace(".", "", $data['canon']),
            ":admon"             => str_ireplace(".", "", $data['admon']),
            ":valIva"            => str_ireplace(".", "", $valIva),
            ":admondto"          => str_ireplace(".", "", $data['admondto']),
            ":procedencia"       => $data['procedencia'],
            ":destinacion"       => $data['destinacion'],
            ":incluida"          => $incluida,
            ":tipoinmueble"      => $data['tipoinmueble'],
            ":idInm"             => $idInm,
            ":codinm"            => $codinm,
            ":IdInmobiliaria"    => $data['IdInmobiliaria'],
            ":Telefonoinm"       => $data['telInmueble'],
            ":uso"               => $data['usoInmueble'],
            ":sede"              => ($data['sede']=="")?'0':$data["sede"],
            ":usu_creacion"      => $usu_creacion,
            ":f_creacion"        => $fechaserv,
            ":h_creacion"        => $h_creacion,
        ))) {

            //promotor 3 captador 4
            for ($i = 0; $i < count($data['idpromotor']); $i++) {
                $this->saveTercero($data['IdInmobiliaria'], $codinm, 3, $data['idpromotor'][$i]);
            }
            for ($j = 0; $j < count($data['codcaptador']); $j++) {
                $this->saveTercero($data['IdInmobiliaria'], $codinm, 4, $data['codcaptador'][$j]);
            }

            $inmueb          = array();
            return $inmueb[] = array("codinmu" => $codinm);
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function saveLogFinancial($data, $idInm)
    {
        $connPDO = new Conexion();

        $Idinmobiliaria = $data['IdInmobiliaria'];
        $idInm          = "$Idinmobiliaria - $codinm";
        $f_sis          = date('Y-m-d');
        $usuario        = $_SESSION['iduser'];
        $h_sis          = date('Y-m-d');
        $ip             = $_SERVER['REMOTE_ADDR'];

        $stmt = $connPDO->prepare("SELECT IdGestion,FConsignacion,Estrato,AreaConstruida,AreaLote,
            EdadInmueble,NoMatricula,chip,AvaluoCatastral,CedulaCatastral,
            fingreso,restricciones,obserinterna,codinterno,FechaAvaluoCatastral,
            ValorVenta,ValorCanon,Administracion,ValorIva,admondto,
            IdProcedencia,IdDestinacion,AdmonIncluida,IdTpInm,idInm,
            codinm,usu_creacion,f_creacion,h_creacion
                    from inmuebles
                    where idInm=:idInm");

        if ($stmt->execute(array(
            ":idInm" => $idInm,
        ))) {

            while ($row = $stmt->fetch()) {
                $IdGestion_or       = $row['IdGestion'];
                $rowConsignacion_or = $row['FConsignacion'];
                $Estrato_or         = $row['Estrato'];
                $AreaConstruida_or  = $row['AreaConstruida'];
                $AreaLote_or        = $row['AreaLote'];
                $rowrente_or        = $row['Frente'];
                $rowondo_or         = $row['Fondo'];
                $Ubicacion_or       = $row['Ubicacion'];
                $EdadInmueble_or    = $row['EdadInmueble'];
                $NoMatricula_or     = $row['NoMatricula'];
                $chip_or            = $row['chip'];
                $AvaluoCatastral_or = $row['AvaluoCatastral'];
                $CedulaCatastral_or = $row['CedulaCatastral'];
                $amoblado_or        = $row['amoblado'];
                if ($amoblado_or == 0) {$amoblado_or = "";}
                $restricciones_or  = $row['restricciones'];
                $obserinterna_or   = $row['obserinterna'];
                $Favaluo_or        = $row['FechaAvaluoCatastral'];
                $ValorVenta_or     = $f['ValorVenta'];
                $ValorCanon_or     = $f['ValorCanon'];
                $Administracion_or = $f['Administracion'];
                $ValorIva_or       = $f['ValorIva'];
                $admondto_or       = $f['admondto'];
                $IdProcedencia_or  = $f['IdProcedencia'];
                $idCaptador_or     = $f['idCaptador'][0];
                $IdPromotor_or     = $f['IdPromotor'][0];
                $IdDestinacion_or  = $f['IdDestinacion'];
                $tipo_contrato_or  = $f['tipo_contrato'];
                $IdUbicacion_or    = $f['IdUbicacion'];
                $IdTpInm_or        = $f['IdTpInm'];
            }
            /////inserta en bitácora de cambios por campo
            $IdDemanda = $idInm;
            if ($Favaluo_or != $data['Favaluo']) {
                $cambio++;
                $nom_campo   = "Fecha Avaluo";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "Fecha Avaluo"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['Favaluo'], $Favaluo_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($IdGestion_or != $data['gestioncomercial']) {
                $cambio++;
                $nom_campo   = "IdGestion";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "IdGestion"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['gestioncomercial'], $IdGestion_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($FConsignacion_or != $data['fecha_inicio']) {
                $cambio++;
                $nom_campo   = "FConsignacion";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "FConsignacion"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['fecha_inicio'], $FConsignacion_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($Estrato_or != $data['estrato']) {
                $cambio++;
                $nom_campo   = "Estrato";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "Estrato"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['estrato'], $Estrato_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($AreaConstruida_or != str_ireplace(".", "", $data['areaconstruida'])) {
                $cambio++;
                $nom_campo   = "AreaConstruida";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "AreaConstruida"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, str_ireplace(".", "", $data['areaconstruida']), $AreaConstruida_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($AreaLote_or != str_ireplace(".", "", $data['arealote'])) {
                $cambio++;
                $nom_campo   = "AreaLote";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "AreaLote"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['arealote'], $AreaLote_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }

            if ($EdadInmueble_or != $data['anoconstruccion']) {
                $cambio++;
                $nom_campo   = "EdadInmueble";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "EdadInmueble"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['anoconstruccion'], $EdadInmueble_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($NoMatricula_or != $data['nromatri']) {
                $cambio++;
                $nom_campo   = "NoMatricula";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "NoMatricula"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['nromatri'], $NoMatricula_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($chip_or != $data['chip']) {
                $cambio++;
                $nom_campo   = "chip";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "chip"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['chip'], $chip_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($AvaluoCatastral_or != $calledavaluocatas) {
                $cambio++;
                $nom_campo   = "AvaluoCatastral";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "AvaluoCatastral"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $calledavaluocatas, $AvaluoCatastral_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($CedulaCatastral_or != $data['cedulacatast']) {
                $cambio++;
                $nom_campo   = "CedulaCatastral";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "CedulaCatastral"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['cedulacatast'], $CedulaCatastral_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }

            if ($amoblado_or != $data['amoblado']) {
                $cambio++;
                $nom_campo   = "Amoblado";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "Amoblado"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['amoblado'], $amoblado_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($restricciones_or != $restricciones) {
                $cambio++;
                $nom_campo   = "restricciones";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "restricciones"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $restricciones, $restricciones_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($obserinterna_or != $data['obserinterna']) {
                $cambio++;
                $nom_campo   = "obserinterna";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "obserinterna"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['obserinterna'], $obserinterna_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($ValorVenta_or != str_ireplace(".", "", $data['ValorVenta'])) {
                $cambio++;
                $nom_campo   = "ValorVenta";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "ValorVenta"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, str_ireplace(".", "", $data['ValorVenta']), $ValorVenta_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($ValorCanon_or != str_ireplace(".", "", $data['ValorCanon'])) {
                $cambio++;
                $nom_campo   = "ValorCanon";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "ValorCanon"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, str_ireplace(".", "", $data['ValorCanon']), $ValorCanon_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($ValorIva_or != str_ireplace(".", "", $data['ValorIva'])) {
                $cambio++;
                $nom_campo   = "ValorIva";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "ValorIva"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, str_ireplace(".", "", $data['ValorIva']), $ValorIva_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($Administracion_or != str_ireplace(".", "", $data['Administracion'])) {
                $cambio++;
                $nom_campo   = "Administracion";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "Administracion"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, str_ireplace(".", "", $data['Administracion']), $Administracion_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($admondto_or != str_ireplace(".", "", $data['admondto'])) {
                $cambio++;
                $nom_campo   = "admondto";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "admondto"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, str_ireplace(".", "", $data['admondto']), $admondto_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }

            if ($IdProcedencia_or != $_POST['procedencia']) {
                $cambio++;
                $nom_campo   = "IdProcedencia";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "IdProcedencia"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['procedencia'], $IdProcedencia_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($idCaptador_or != $data['codcaptador']) {
                $cambio++;
                $nom_campo   = "idCaptador";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "idCaptador"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['codcaptador'], $idCaptador_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($IdPromotor_or != $data['idpromotor']) {
                $cambio++;
                $nom_campo   = "IdPromotor";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "IdPromotor"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['idpromotor'], $IdPromotor_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($IdDestinacion_or != $data['destinacion']) {
                $cambio++;
                $nom_campo   = "IdDestinacion";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "IdDestinacion"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['destinacion'], $IdDestinacion_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($IdTpInm_or != $data['tipoinmueble']) {
                $cambio++;
                $nom_campo   = "IdTpInm";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "IdTpInm"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['tipoinmueble'], $IdTpInm_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);

            }

            return 1;
        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function saveTercero($IdInmobiliaria, $codinm, $tipoTer, $idTer)
    {
        $connPDO = new Conexion();

        $conse   = consecutivo("conse_tr", "inmueblesterceros");
        $usuario = $_SESSION['iduser'];
        $par     = 1;
        $fec     = date('Y-m-d');

        $stmt = $connPDO->prepare("REPLACE INTO inmueblesterceros
         (conse_tr,inmob_tr,idinm_tr,iduser_tr,rol_tr,part_tr,est_tr,idusu_tr,fec_tr)
            VALUES
            (:conse_tr,:inmob_tr,:idinm_tr,:iduser_tr,:rol_tr,:part_tr,:est_tr,:idusu_tr,:fec_tr)");

        if ($stmt->execute(array(
            ":conse_tr"  => $conse,
            ":inmob_tr"  => $IdInmobiliaria,
            ":idinm_tr"  => $codinm,
            ":iduser_tr" => $idTer,
            ":rol_tr"    => $tipoTer,
            ":part_tr"   => $par,
            ":est_tr"    => $par,
            ":idusu_tr"  => $usuario,
            ":fec_tr"    => $fec,
        ))) {

            return $idTer;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function caracObligatoriasTpInmueble($dato)
    {
        $connPDO = new Conexion();
        $data    = $dato['tpInm'];
        $cadena  = '';
        $valores = Inmuebles::getDataDetalleLateralInmueble($dato);
        foreach ($valores as $key => $value) {
            $banos   = ($value['banos'] > 0) ? $value['banos'] : '';
            $alcobas = ($value['alcobas'] > 0) ? $value['alcobas'] : '';
            $garaje  = ($value['garaje'] > 0) ? $value['garaje'] : '';
        }
        if ($data == 1 || $data == 2 || $data == 8 || $data == 10 || $data == 11 || $data == 12 || $data == 13) {
            $cadena1 .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label><b class="obligatorio">(*)</b> Area Construida</label>
            <input type="text" name="valAreaLote" data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="(*) Area Construida">
           </div>
          </div>';
            $cadena1 .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label><b class="obligatorio ">(*)</b> Area Privada Total</label>
            <input type="text" name="valAreaPrivada" data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="(*) Area Privada Total">
           </div>
          </div>';
            $cadena .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label><b class="obligatorio">(*)</b> Nro Habitaciones</label>
            <input type="text" name="valNumHabitaciones" data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="(*) Nro Habitaciones" value="' . $alcobas . '">
           </div>
          </div>';

            $cadena .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label><b class="obligatorio">(*)</b> Nro de Ba&ntilde;os</label>
            <input type="text" name="valNumBanos" data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="(*) Nro de Ba&ntilde;os" value="' . $banos . '">
           </div>
          </div>';
            $cadena .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label>Nro de Parqueaderos</label>
            <input type="text" name="valNumParqueaderos" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="Nro de Parqueaderos" value="' . $garaje . '">
           </div>
          </div>';

        }

        if ($data == 3 || $data == 4 || $data == 5 || $data == 6) {
            $cadena1 .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label><b class="obligatorio">(*)</b> Area Construida</label>
            <input type="text" name="valAreaLote" data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="(*) Area Construida">
           </div>
          </div>';
            $cadena1 .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label><b class="obligatorio">(*)</b> Area Privada Total</label>
            <input type="text" name="valAreaPrivada" data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="(*) Area Privada Total">
           </div>
          </div>';

            $cadena .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label><b class="obligatorio">(*)</b> Nro de Ba&ntilde;os</label>
            <input type="text" name="valNumBanos" data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="(*) Nro de Ba&ntilde;os" value="' . $banos . '">
           </div>
          </div>';
            $cadena .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label>Nro de Parqueaderos</label>
            <input type="text" name="valNumParqueaderos" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="Nro de Parqueaderos" value="' . $garaje . '">
           </div>
          </div>';

        }
        if ($data == 7) {
            $cadena1 .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label><b class="obligatorio">(*)</b> Area Construida</label>
            <input type="text" name="valAreaLote" data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="(*) Area Construida">
           </div>
          </div>';
            $cadena1 .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label><b class="obligatorio">(*)</b> Area Privada Total</label>
            <input type="text" name="valAreaPrivada" data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="(*) Area Privada Total">
           </div>
          </div>';

            $cadena .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label><b class="obligatorio"></b> Nro de Ba&ntilde;os</label>
            <input type="text" name="valNumBanos" data-parsley-errors-messages-disabled data-parsley-required="false" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="(*) Nro de Ba&ntilde;os" value="' . $banos . '">
           </div>
          </div>';
            $cadena .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label>Nro de Parqueaderos</label>
            <input type="text" name="valNumParqueaderos" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="Nro de Parqueaderos" value="' . $garaje . '">
           </div>
          </div>';

        }
        if ($data == 9) {
            $cadena1 .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label><b class="obligatorio">(*)</b> Area Construida</label>
            <input type="text" name="valAreaLote" data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="(*) Area Construida">
           </div>
          </div>';
            $cadena1 .= '<div class="form-group ">
           <div class="input-group col-xs-12">
            <label><b class="obligatorio">(*)</b> Area Privada Total</label>
            <input type="text" name="valAreaPrivada" data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control inputChange input-sm form-control-inline input-medium filthypillow col-xs-12 col-sm-6 onlyNum" placeholder="(*) Area Privada Total">
           </div>
          </div>';
        }
        return $cadena;
        $stmt = null;
    }
    public function politicasInmuebleOld($data)
    {
        $connPDO = new Conexion();

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $connPDO->prepare("UPDATE inmuebles
            set
            ComiVenta          = :ComiVenta,
            ComiArren          = :ComiArren,
            ValComiVenta       = :ValComiVenta,
            ValComiArr         = :ValComiArr,
            politica_comp      = :politica_comp
            WHERE codinm       = :codinm
            AND IdInmobiliaria = :IdInmobiliaria
            ");

        if ($stmt->execute(array(
            ":ComiVenta"      => ($data['ComiVenta'] / 100),
            ":ComiArren"      => ($data['ComiArren'] / 100),
            ":ValComiVenta"   => str_ireplace(".", "", $data['ValComiVenta']),
            ":ValComiArr"     => str_ireplace(".", "", $data['ValComiArr']),
            ":politica_comp"  => $data['politica_comp'],
            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
        ))) {
            $this->politicasInmuebleNew($data);
            $this->logPoliticasInmueble($data, $idInm);
            return true;
        } else {
            print_r($stmt->errorInfo());
        }

    }

    public function politicasInmuebleNew($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("UPDATE inmnvo
            set
            ComiVenta          = :ComiVenta,
            ComiArren          = :ComiArren,
            ValComiVenta       = :ValComiVenta,
            ValComiArr         = :ValComiArr,
            politica_comp      = :politica_comp
            WHERE codinm       = :codinm
            AND IdInmobiliaria = :IdInmobiliaria
            ");

        if ($stmt->execute(array(
            ":ComiVenta"      => ($data['ComiVenta'] / 100),
            ":ComiArren"      => ($data['ComiArren'] / 100),
            ":ValComiVenta"   => str_ireplace(".", "", $data['ValComiVenta']),
            ":ValComiArr"     => str_ireplace(".", "", $data['ValComiArr']),
            ":politica_comp"  => $data['politica_comp'],
            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
        ))) {
            return true;
        } else {
            print_r($stmt->errorInfo());
        }
    }
    public function logPoliticasInmueble($data, $idInm)
    {
        $f_sis     = date('Y-m-d');
        $h_sis     = date('H:i:s');
        $ip        = $_SERVER['REMOTE_ADDR'];
        $usuario   = $_SESSION['iduser'];
        $IdDemanda = $idInm;
        $connPDO   = new Conexion;
        $stmt      = $connPDO->prepare("SELECT ComiVenta,ComiArren,ValComiVenta,ValComiArr,
                        politica_comp
                        FROM inmuebles
                        WHERE idInm=:idInm");
        $stmt->bindParam(':idInm', $idInm);

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $ComiVenta_or     = $row['ComiVenta'];
                $ComiArren_or     = $row['ComiArren'];
                $ValComiVenta_or  = $row['ValComiVenta'];
                $ValComiArr_or    = $row['ValComiArr'];
                $politica_comp_or = $row['politica_comp'];
            }
            if ($ComiVenta_or != $data['ComiVenta']) {
                $cambio++;
                $nom_campo   = "ComiVenta";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "ComiVenta"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['ComiVenta'], $ComiVenta_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($ComiArren_or != $data['ComiArren']) {
                $cambio++;
                $nom_campo   = "ComiArren";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "ComiArren"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['ComiArren'], $ComiArren_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($ValComiVenta_or != $data['ValComiVenta']) {
                $cambio++;
                $nom_campo   = "ValComiVenta";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "ValComiVenta"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['ValComiVenta'], $ValComiVenta_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($ValComiArr_or != $data['ValComiArr']) {
                $cambio++;
                $nom_campo   = "ValComiArr";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "ValComiArr"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['ValComiArr'], $ValComiArr_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            if ($politica_comp_or != $data['politica_comp']) {
                $cambio++;
                $nom_campo   = "politica_comp";
                $cons_log    = consecutivo_log(3);
                $campo_usu_g = "politica_comp"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 3, $nom_campo, $data['politica_comp'], $politica_comp_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g, 0);
            }
            return true;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function savePropietario($data)
    //  public function saveFinancialNew($data)
    {
        $connPDO = new Conexion();

        $idInm     = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $fechaserv = date('Y-m-d');

        if ($data['tipodocu'] != 2) {
            $razon_social = $apellidos . " " . $Nombres;
        } else {
            $razon_social = $data['razon_social'];
            $Nombres      = $data['razon_social'];
        }
        $tel1 = "(" . $data['indict1'] . ")" . $data['telefonoprop'];
        if ($data['indict2'] > 0) {
            $tel2 = "(" . $data['indict2'] . ")" . $data['telefonoprop2'];
        } else {
            $tel2 = "";
        }
        if ($data['indict3'] > 0) {
            $tel3 = "(" . $data['indict3'] . ")" . $data['telefonoprop3'];
        } else {
            $tel3 = "";
        }
        if ($data['paisd'] != 169) {
            $deptod     = 0;
            $ciudad     = 0;
            $ciudadtext = $data['ciudadtext'];
        } else {
            $deptod     = $data['deptod'];
            $ciudadtext = ucwords(strtolower(getCampo('ciudades_dian', "where id_ciudad=" . $data['ciudadd'], 'nombre_ciudad')));
        }
        $apellidos = trim($data['apellido1']) . " " . trim($data['apellido2']);
        $Nombres   = trim($data['nombre1']) . " " . trim($data['nombre2']);
        ////valida si el tercero existe
        $existe = Inmuebles::validaInfoTercero($data);
        if ($existe == 0) {
            $iduser = consecutivo('iduser', 'usuarios2');
        } else {
            $iduser = getCampo('usuarios2', "where Id_Usuarios='" . $data['cedprop'] . "'", 'iduser');
            Inmuebles::updatePropietario($data, $iduser);
            return 0;
            exit();

        }

        $stmt = $connPDO->prepare("INSERT IGNORE INTO usuarios2
                        (Nombres,apellidos,Direccion,nombre1,nombre2,
                        apellido1,apellido2,razon_social,Telefono,telefono2,
                        telefono3,Correo,Celular,celular2,pais_u,
                        depto_u,ciudad_u,ext1,ext2,ext3,
                        ciudad_utext,IdTipoDocumento,Id_Usuarios,iduser,IdInmmo,f_nacimiento,vip)
                        VALUES
                        (:Nombres,:apellidos,:Direccion,:nombre1,:nombre2,
                        :apellido1,:apellido2,:razon_social,:Telefono,:telefono2,
                        :telefono3,:Correo,:Celular,:celular2,:pais_u,
                        :depto_u,:ciudad_u,:ext1,:ext2,:ext3,
                        :ciudad_utext,:IdTipoDocumento,:Id_Usuarios,:iduser,:IdInmmo,:f_nacimiento,:vip)");

        if ($stmt->execute(array(
            ":Nombres"         => utf8_encode($Nombres),
            ":apellidos"       => utf8_encode($apellidos),
            ":Direccion"       => utf8_encode($data['direccionprop']),
            ":nombre1"         => utf8_encode($data['nombre1']),
            ":nombre2"         => utf8_encode($data['nombre2']),
            ":apellido1"       => utf8_encode($data['apellido1']),
            ":apellido2"       => utf8_encode($data['apellido2']),
            ":razon_social"    => utf8_encode($razon_social),
            ":Telefono"        => $tel1,
            ":telefono2"       => $tel2,
            ":telefono3"       => $tel3,
            ":Correo"          => utf8_encode($data['emailpro']),
            ":Celular"         => utf8_encode($data['celularprop']),
            ":celular2"        => utf8_encode($data['celularprop2']),
            ":pais_u"          => utf8_encode($data['paisd']),
            ":depto_u"         => utf8_encode($deptod),
            ":ciudad_u"        => utf8_encode($data['ciudadd']),
            ":ext1"            => utf8_encode($data['ext1']),
            ":ext2"            => utf8_encode($data['ext2']),
            ":ext3"            => utf8_encode($data['ext3']),
            ":ciudad_utext"    => utf8_encode($ciudadtext),
            ":IdTipoDocumento" => $data['tipodocu'],
            ":f_nacimiento"    => $data['dateBirth'],
            ":vip"             => $data['vip'],
            ":Id_Usuarios"     => $data['cedprop'],
            ":iduser"          => $iduser,
            ":IdInmmo"         => $data['IdInmobiliaria'],

        ))) {
            //propietario 1 apoderado 4
            $this->saveTercero($data['IdInmobiliaria'], $data['codinm'], $data['rol_tr'], $iduser);
            return $iduser;

            //$inmueb=array();
            //return $inmueb[]=array("codinmu"=>$codinm);
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function saveAdicionalesInmueble($data)
    {
        $connPDO    = new Conexion();
        $usuario    = $_SESSION['Id_Usuarios'];
        $iduser_det = $_SESSION['iduser'];
        //$cod         = consecutivo('IdDetalle','detalleinmueble');
        $idInm     = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $fechaserv = date('Y-m-d');

        ///borramos los valores del inmueble antes de insertar
        Inmuebles::deleteAdicionalesInmueble($data['IdInmobiliaria'], $data['codinm']);

        ////insertamos valores laterales en las tablas actuales inmuebles y detalleinmueble
        Inmuebles::insertLateralInmueble($data['valAreaLote'], $data['valAreaPrivada'], $data['valNumHabitaciones'], $data['valNumBanos'], $data['valNumParqueaderos'], $data['IdInmobiliaria'], $data['codinm']);
        Inmuebles::insertLateralInmuebleold($data['valAreaLote'], $data['valAreaPrivada'], $data['valNumHabitaciones'], $data['valNumBanos'], $data['valNumParqueaderos'], $data['IdInmobiliaria'], $data['codinm'], $data['tipoInm']);

        Inmuebles::updateDescripcionOld($data['IdInmobiliaria'], $data['codinm'], $data['descripcionlarga'], $data['descripcionMetro']);
        Inmuebles::updateDescripcionNew($data['IdInmobiliaria'], $data['codinm'], $data['descripcionlarga'], $data['descripcionMetro']);

        foreach ($data['checkCarVal'] as $key => $value) {
            if ($value == 1) {
                if ($key != 30 && $key != 38) {
                    Inmuebles::insertAdicionalesInmueble($data, $key, $data['caracteristica'][$key], $data['cantCar'][$key]);
                }
            }
        }
        return 1;
        $stmt = null;
    }
    public function insertAdicionalesInmueble($data, $idcar, $desc, $cant)
    {
        $connPDO    = new Conexion();
        $usuario    = $_SESSION['Id_Usuarios'];
        $iduser_det = $_SESSION['iduser'];
        $cod        = consecutivo('IdDetalle', 'detalleinmueble');
        $idInm      = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $fechaserv  = date('Y-m-d');
        $h_sis      = date('H:i:s');
        $ip         = $_SERVER['REMOTE_ADDR'];
        $idInm      = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        if ($cant <= 0) {$cant = 0;}
        if ($data['utilizarDesc']) {$udesc = 1;} else { $udesc = 0;}

        $stmt = $connPDO->prepare("INSERT  INTO detalleinmueble
                                (IdDetalle,idinmueble,idusuario,idcaracteristica,obser_det,
                                inmob,codinmdet,cantidad_dt,fecha_dt,iduser_dt)
                                VALUES
                                (:IdDetalle,:idinmueble,:idusuario,:idcaracteristica,:obser_det,
                                :inmob,:codinmdet,:cantidad_dt,:fecha_dt,:iduser_dt)");

        if ($stmt->execute(array(
            ":IdDetalle"        => $cod,
            ":idinmueble"       => $idInm,
            ":idusuario"        => $usuario,
            ":iduser_dt"        => $iduser_det,
            ":idcaracteristica" => $idcar,
            ":obser_det"        => utf8_decode($desc),
            ":inmob"            => $data['IdInmobiliaria'],
            ":codinmdet"        => $data['codinm'],
            ":cantidad_dt"      => $cant,
            ":fecha_dt"         => $fechaserv,
        ))) {
            guardar_settings_portales($data['IdInmobiliaria'], $idInm, 0, 5, $udesc, $_SESSION['Id_Usuarios'], $fechaserv, $h_sis, $ip, 0);

            return "exitoso";
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function insertAdicionalesInmueble2($IdInmobiliaria, $codinm, $idcar, $desc)
    {
        $connPDO    = new Conexion();
        $usuario    = $_SESSION['Id_Usuarios'];
        $iduser_det = $_SESSION['iduser'];
        $cod        = consecutivo('IdDetalle', 'detalleinmueble');
        $idInm      = $IdInmobiliaria . "-" . $codinm;
        $fechaserv  = date('Y-m-d');
        if ($data['cantCar'] <= 0) {$data['cantCar'] = 0;}

        $idcara = ($idcar <= 0) ? 0 : $idcar;
        $stmt   = $connPDO->prepare("INSERT  INTO detalleinmueble
                                (IdDetalle,idinmueble,idusuario,idcaracteristica,obser_det,
                                inmob,codinmdet,cantidad_dt,fecha_dt,iduser_dt)
                                VALUES
                                (:IdDetalle,:idinmueble,:idusuario,:idcaracteristica,:obser_det,
                                :inmob,:codinmdet,:cantidad_dt,:fecha_dt,:iduser_dt)");

        if ($stmt->execute(array(
            ":IdDetalle"        => $cod,
            ":idinmueble"       => $idInm,
            ":idusuario"        => $usuario,
            ":iduser_dt"        => $iduser_det,
            ":idcaracteristica" => $idcara,
            ":obser_det"        => utf8_decode($desc),
            ":inmob"            => $IdInmobiliaria,
            ":codinmdet"        => $codinm,
            ":cantidad_dt"      => $data['cantCar'],
            ":fecha_dt"         => $fechaserv,
        ))) {
            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function deleteAdicionalesInmueble($IdInmobiliaria, $codinm)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("DELETE FROM  detalleinmueble
                        WHERE   codinmdet =:codinm
                        AND inmob         =:inmob");

        if ($stmt->execute(array(
            ":inmob"  => $IdInmobiliaria,
            ":codinm" => $codinm,

        ))) {

            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function insertLateralInmueble($AreaLote, $AreaConstruida, $alcobas, $banos, $garaje, $IdInmobiliaria, $codinm)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("UPDATE inmnvo
                        set
                        banos              =:banos,
                        garaje             =:garaje,
                        alcobas            =:alcobas
                        WHERE codinm       =:codinm
                        AND IdInmobiliaria =:inmob");

        if ($stmt->execute(array(
            ":banos"   => $banos,
            ":garaje"  => $garaje,
            ":alcobas" => $alcobas,
            ":inmob"   => $IdInmobiliaria,
            ":codinm"  => $codinm,

        ))) {

            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function insertLateralInmuebleOld($AreaLote, $AreaConstruida, $alcobas, $banos, $garaje, $IdInmobiliaria, $codinm, $tpinm)
    {
        $connPDO = new Conexion();
        // insertamos baños, garajes y alcobas en detalleinmueble
        if ($alcobas > 0) {
            $idcar = $this->findAdicionalesInmueble($alcobas, $tpinm, 'alcoba', 15);
            $this->insertAdicionalesInmueble2($IdInmobiliaria, $codinm, $idcar, '');
        }
        if ($banos > 0) {
            $idcar = $this->findAdicionalesInmueble($banos, $tpinm, 'baño', 16);
            $this->insertAdicionalesInmueble2($IdInmobiliaria, $codinm, $idcar, '');
        }
        if ($garaje > 0) {
            $idcar = $this->findAdicionalesInmueble($garaje, $tpinm, 'parqueadero', 37);
            $this->insertAdicionalesInmueble2($IdInmobiliaria, $codinm, $idcar, '');
        }

        $stmt = $connPDO->prepare("SELECT
                        AreaLote  from inmuebles
                        WHERE codinm       =:codinm
                        AND IdInmobiliaria =:inmob");

        if ($stmt->execute(array(

            ":inmob"  => $IdInmobiliaria,
            ":codinm" => $codinm,

        ))) {

            return "banos $banos";
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function findAdicionalesInmueble($cant, $TpInm, $label, $grupo)
    {
        $connPDO = new Conexion();
        if ($grupo == 37 and $cant > 4) {
            $cant = 4;
        }
        if ($grupo == 16 and $cant > 10) {
            $cant = 10;
        }
        if ($grupo == 15 and $cant > 10) {
            $cant = 10;
        }
        if ($cant > 0) {
            $cond = "and Cantidad=:cant";
        } else {
            $cond = "";
        }
        $valueTerm = '%' . utf8_decode($label) . '%';
        $stmt      = $connPDO->prepare("SELECT idCaracteristica
            from maestrodecaracteristicas
            where idGrupo=:grupo
            and Descripcion like :label
            $cond
            and idTipoInmueble=:TpInm");

        $stmt->bindParam(':grupo', $grupo);
        if ($cant > 0) {
            $stmt->bindParam(':cant', $cant);
        }
        $stmt->bindParam(':TpInm', $TpInm);
        $stmt->bindParam(':label', $valueTerm, PDO::PARAM_STR);
        if ($stmt->execute()) {

            while ($row = $stmt->fetch()) {
                $caract = $row['idCaracteristica'];
            }
            return $caract;

        } else {
            return $label . " --"; //print_r($stmt->errorInfo())." error  --";
        }
        $stmt = null;
    }
    public function documentalInmueble($files, $data)
    {
        $connPDO = new Conexion();
        foreach ($files as $key => $value) {
            $nombreArchivo = str_replace("../mcomercialweb/", "", $value['ruta']);
            $usu_dcm       = $_SESSION['iduser'];
            $cod           = consecutivo('id_dcm', 'adjuntos_documental');
            $fechaserv     = date('Y-m-d');
            $hora          = date('H:i:s');
            $ip            = $_SERVER['REMOTE_ADDR'];

            $stmt = $connPDO->prepare("INSERT  INTO adjuntos_documental
                                    (id_dcm,tp_doc_dcm,ruta_archivo_dcm,est_dcm,codinm_dcm,
                                     inmob_dcm,fec_dcm,hor_dcm,ip_dcm,usu_dcm,nom_dcm,size_dcm)
                                    VALUES
                                    (:id_dcm,:tp_doc_dcm,:ruta_archivo_dcm,:est_dcm,:codinm_dcm,
                                     :inmob_dcm,:fec_dcm,:hor_dcm,:ip_dcm,:usu_dcm,:nom_dcm,:size_dcm)");

            if ($stmt->execute(array(
                ":id_dcm"           => $cod,
                ":tp_doc_dcm"       => $data['documental'],
                ":ruta_archivo_dcm" => $nombreArchivo,
                ":est_dcm"          => 1,
                ":codinm_dcm"       => $data['codinm'],
                ":inmob_dcm"        => $data['IdInmobiliaria'],
                ":fec_dcm"          => $fechaserv,
                ":hor_dcm"          => $hora,
                ":ip_dcm"           => $ip,
                ":usu_dcm"          => $usu_dcm,
                ":nom_dcm"          => $value['nombre'],
                ":size_dcm"         => $value['size'],
            ))) {
                return $this->getDatadocumentalInmueble($data, $cod);
            } else {
                $response[] = array("error" => $stmt->errorInfo(), "data" => $files);
            }
            $stmt = null;
        }
        if ($response) {
            return $response;
        }

    }
    public function getCategoriasdocumental()
    {
        $connPDO = new Conexion();

        $fechaserv = date('Y-m-d');
        $hora      = date('H-i-s');

        $stmt = $connPDO->prepare("SELECT conse_param,desc_param
                                 FROM parametros
                                 WHERE id_param=52
                                AND est_param=1");

        if ($stmt->execute(
            //array( ":conse_param"       => $data['conse_param'])
        )) {
            $info = array();
            while ($row = $stmt->fetch()) {
                $info[] = array
                    (
                    "conse" => utf8_encode($row['conse_param']),
                    "desc"  => utf8_encode($row['desc_param']),
                );
            }
            return $info;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function getDatadocumentalInmueble($data, $cod = '')
    {
        $connPDO = new Conexion();

        $fechaserv = date('Y-m-d');
        $hora      = date('H-i-s');
        $cond      = '';
        if ($cod) {
            $cond .= ' and id_dcm=:cod';
        }
        if ($data['documental']) {
            $cond .= ' AND tp_doc_dcm=:tp_doc_dcm';
        }

        $stmt = $connPDO->prepare("SELECT id_dcm,tp_doc_dcm,ruta_archivo_dcm,est_dcm,codinm_dcm,
                                 inmob_dcm,fec_dcm,hor_dcm,ip_dcm,usu_dcm,nom_dcm,size_dcm
                                 FROM adjuntos_documental
                                 WHERE inmob_dcm =:IdInmobiliaria
                                 AND codinm_dcm=:codinm

                                 $cond
                                 AND est_dcm=1");
        if ($cod) {
            $stmt->bindParam(':cod', $cod);
        }
        if ($data['documental']) {
            $stmt->bindParam(':tp_doc_dcm', $data['documental']);
        }
        $stmt->bindParam(':codinm', $data['codinm']);
        $stmt->bindParam(':IdInmobiliaria', $data['IdInmobiliaria']);

        if ($stmt->execute()) {
            $info = array();
            while ($row = $stmt->fetch()) {
                $info[] = array
                    (
                    'id_dcm'           => $row['id_dcm'],
                    'nom_dcm'          => strtolower($row['nom_dcm']),
                    'tp_doc_dcm'       => $row['tp_doc_dcm'],
                    'ruta_archivo_dcm' => $row['ruta_archivo_dcm'],
                    'size'             => $row['size_dcm'],
                    'sizeTotal'        => Inmuebles::getSizedocumentalInmueble($data),
                );
            }
            return $info;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function getSizedocumentalInmueble($data)
    {
        $connPDO = new Conexion();

        $fechaserv = date('Y-m-d');
        $hora      = date('H-i-s');
        $cond      = '';
        // if($data['documental'])
        //  {
        //      $cond .=' AND tp_doc_dcm=:tp_doc_dcm';
        //  }

        $stmt = $connPDO->prepare("SELECT sum(size_dcm) as tot
                                 FROM adjuntos_documental
                                 WHERE inmob_dcm =:IdInmobiliaria
                                 AND codinm_dcm=:codinm
                                 $cond");

        // if($data['documental'])
        //  {
        //     $stmt->bindParam(':tp_doc_dcm',$data['documental']);;
        //  }
        $stmt->bindParam(':codinm', $data['codinm']);
        $stmt->bindParam(':IdInmobiliaria', $data['IdInmobiliaria']);
        if ($stmt->execute()) {
            $info = array();
            while ($row = $stmt->fetch()) {
                $size = $row['tot'];

            }
            return ($size) / 1024;
        } else {
            return print_r($stmt->errorInfo()) . " get size";
        }
        $stmt = null;
    }
    public function deleteDatadocumentalInmueble($data)
    {
        $connPDO = new Conexion();

        $fechaserv = date('Y-m-d');
        $hora      = date('H-i-s');

        $stmt = $connPDO->prepare("DELETE FROM adjuntos_documental
                                 WHERE inmob_dcm =:IdInmobiliaria
                                 AND codinm_dcm=:codinm

                                 AND id_dcm=:id_dcm");

        if ($stmt->execute(array(
            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],

            ":id_dcm"         => $data['id_dcm'],
        ))) {
            $peso = Inmuebles::getSizedocumentalInmueble($data);
            return $peso;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function updateDatadocumentalInmueble($data)
    {
        $connPDO = new Conexion();

        $fechaserv = date('Y-m-d');
        $hora      = date('H-i-s');

        $stmt = $connPDO->prepare("UPDATE  adjuntos_documental
                                 SET
                                 nom_dcm              =:nom_dcm
                                 WHERE inmob_dcm      =:IdInmobiliaria
                                 AND codinm_dcm       =:codinm
                                 AND id_dcm           =:id_dcm");

        if ($stmt->execute(array(
            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":id_dcm"         => $data['id_dcm'],
            ":nom_dcm"        => $data['nom_dcm'],
        ))) {

            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function updateMfichaFotosNew($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("UPDATE fotos_nvo
                SET
                ficha_fto      =:ficha_fto
                where posi_ft  =:posi_ft
                and aa_fto     =:IdInmobiliaria
                and codinm_fto =:codinm");

        if ($stmt->execute(array(
            ":ficha_fto"      => $data["ficha_fto"],
            ":posi_ft"        => $data['posi_ft'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":codinm"         => $data['codinm'],
        ))) {

            return 1;
        } else {
            $response[] = array("Error" => $stmt->errorInfo());
        }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function OrdenFotosNew($data)
    {
        $connPDO = new Conexion();
        $i       = -1;
        foreach ($data as $key => $value) {
            $i++;
            list($aa, $ruta) = explode("|", $key);
            if (strlen($ruta) > 5) {
                $ruta = str_replace("_", ".", $ruta);
                Inmuebles::updateOrdenFotosNew($i, $value[0], $ruta, $data['IdInmobiliaria'], $data['codinm']);
                // echo $value[0]."<br>";
                //echo $i.$value[0].$ruta.$data['IdInmobiliaria'].$data['codinm']."<br>";
            }
        }
        return 1;
    }
    public function updateOrdenFotosNew($posi, $label_fto, $foto, $IdInmobiliaria, $codinm)
    {
        $connPDO = new Conexion();
        $campo   = "Foto" . $data['inicial'];
        $campo3  = "Foto" . $data['final'];
        $archnvo = $data['nombreF'];
        $idInm   = $IdInmobiliaria . "-" . $codinm;

        $stmt = $connPDO->prepare("UPDATE fotos_nvo
            SET
            label_fto        =:label_fto,
            foto             =:foto
            where codinm_fto =:codinm
            and aa_fto       =:IdInmobiliaria
            and posi_ft      =:posi_ft");

        if ($stmt->execute(array(
            ":posi_ft"        => $posi,
            ":label_fto"      => $label_fto,
            ":foto"           => $foto,
            ":IdInmobiliaria" => $IdInmobiliaria,
            ":codinm"         => $codinm,
        ))) {
            Inmuebles::restoreFotosOld($idInm);

            return 1;
        } else {
            $response[] = array("Error" => $stmt->errorInfo());
        }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }

    public function restoreFotosOld($idInm)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("DELETE from  fotos where idInm=:idInm");

        if ($stmt->execute(array(
            ":idInm" => $idInm,
        ))) {

            return Inmuebles::selectFotosNvo2($idInm);
        } else {
            $response[] = array("Error" => $stmt->errorInfo());
        }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function updateVideoOld($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("UPDATE inmuebles
                SET
                linkvideo            =:linkvideo
                where IdInmobiliaria =:IdInmobiliaria
                and codinm           =:codinm");

        if ($stmt->execute(array(
            ":linkvideo"      => $data["linkvideo"],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":codinm"         => $data['codinm'],
        ))) {
            Inmuebles::updateVideoNew($data);
            return 1;
        } else {
            $response[] = array("Error" => $stmt->errorInfo());
        }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function updateVideoNew($data)
    {
        $connPDO = new Conexion();

        $data['cantidad_dt'] = 0;
        $cod                 = consecutivo('IdDetalle', 'detalleinmueble');
        $idInm               = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $fechaserv           = date('Y-m-d');
        $desc                = '';
        $usuario             = $_SESSION['Id_Usuarios'];
        $iduser_det          = $_SESSION['iduser'];
        $idcar               = $this->findAdicionalesInmueble($garaje, $data['tpinm'], 'url', 51);
        $stmt                = $connPDO->prepare("INSERT  INTO detalleinmueble
                                (IdDetalle,idinmueble,idusuario,idcaracteristica,obser_det,
                                inmob,codinmdet,cantidad_dt,fecha_dt,iduser_dt)
                                VALUES
                                (:IdDetalle,:idinmueble,:idusuario,:idcaracteristica,:obser_det,
                                :inmob,:codinmdet,:cantidad_dt,:fecha_dt,:iduser_dt)");

        if ($stmt->execute(array(
            ":IdDetalle"        => $cod,
            ":idinmueble"       => $idInm,
            ":idusuario"        => $usuario,
            ":iduser_dt"        => $iduser_det,
            ":idcaracteristica" => $idcar,
            ":obser_det"        => $desc,
            ":inmob"            => $data['IdInmobiliaria'],
            ":codinmdet"        => $data['codinm'],
            ":cantidad_dt"      => $data['cantidad_dt'],
            ":fecha_dt"         => $fechaserv,

        ))) {

            return 1;
        } else {
            $response[] = array("Error" => $stmt->errorInfo());
        }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function inmueblePortales($data)
    {
        $connPDO = new Conexion();
        $id      = $data['IdInmobiliaria'];

        $stmt = $connPDO->prepare("SELECT i.IdPortal,i.IdInmobiliaria,
                p.NomPortal,p.imagenp,p.ordport,p.mostrar_cod,p.mostrar_sincroniza,p.mostrar_infprom,p.mostrar_suc
                from Portales p, PublicaPortales i
                where i.IdPortal=p.IdPortal
                and i.IdInmobiliaria= :idinmo
                and p.estport=1
                order by p.mostrar_cod desc,p.mostrar_suc desc, p.ordport");
        //#and p.mostrar_inm=1
        if ($codPortal > 0) {
            $stmt->bindParam(":codPortal", $codPortal);
        }
        $stmt->bindParam(":idinmo", $id);
        if ($stmt->execute()) {
            //$infoPortal=$this->datosInmueblePublicaciones($data,$codPortal);
            $info = array();
            while ($row = $stmt->fetch()) {
                $info[] = array(
                    "ordport"            => $row['ordport'],
                    "IdPortal"           => $row['IdPortal'],
                    "NomPortal"          => $row['NomPortal'],
                    "imagenp"            => $row['imagenp'],
                    "mostrar_cod"        => $row['mostrar_cod'],
                    "mostrar_sincroniza" => $row['mostrar_sincroniza'],
                    "mostrar_infprom"    => $row['mostrar_infprom'],
                    "mostrar_suc"        => $row['mostrar_suc'],
                    "errores"            => $this->validarCamposObligatoriosPortales($data['IdInmobiliaria'], $data['codinm'], $row['IdPortal']),
                    "infoInmueble"       => $this->datosInmueblePublicaciones($data['IdInmobiliaria'], $data['codinm'], $row['IdPortal']),
                );
            }
            return $info;
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function datosInmueblePublicaciones($IdInmobiliaria, $codinm, $codPortal = '')
    {
        $connPDO = new Conexion();

        $idInm = $IdInmobiliaria . "-" . $codinm;
        $cond  = '';
        if ($codPortal > 0) {
            $cond = ' and portal_dtp=:codPortal';
        }
        //$codigo='<input type="text" class="form-control input-sm" name="" value="87954641">';
        $stmt = $connPDO->prepare("SELECT idinm_dtp,inmob_dtp,portal_dtp,
                cod_dtp,urlpublica_dtp
                from detportalesinmueble
                where idinm_dtp=:codinm
                and inmob_dtp= :idinmo
                $cond");
        if ($codPortal > 0) {
            $stmt->bindParam(":codPortal", $codPortal);
        }
        $stmt->bindParam(":idinmo", $IdInmobiliaria);
        $stmt->bindParam(":codinm", $codinm);
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {

                $data[] = array(
                    "portal_dtp"     => $row['portal_dtp'],
                    "cod_dtp"        => $row['cod_dtp'],
                    "estado"         => $row['cod_dtp'],
                    "urlpublica_dtp" => $row['urlpublica_dtp'],
                    "sincroniza"     => getCampo('settings_portales', "where portal_p=" . $row['portal_dtp'] . " and tipo_p=1 and inmu_p='" . $idInm . "'", 'vlr_p'),
                    "info_promotor"  => getCampo('settings_portales', "where portal_p=" . $row['portal_dtp'] . " and tipo_p=2 and inmu_p='" . $idInm . "'", 'vlr_p'),
                    "sucursal"       => getCampo('settings_portales', "where portal_p=" . $row['portal_dtp'] . " and tipo_p=3 and inmu_p='" . $idInm . "'", 'vlr_p'),
                );
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function validarCamposObligatoriosPortales($IdInmobiliaria, $codinm, $codPortal)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT IdInm,IdBarrio,banos,alcobas,garaje,
                DirMetro,ValorVenta,ValorCanon,Administracion,IdTpInm,
                Estrato,AreaConstruida,AreaLote,EdadInmueble,idEstadoInmueble,IdGestion,latitud,longitud
                from inmnvo
                where IdInmobiliaria =:IdInmobiliaria
                and codinm           =:codinm");

        $stmt->bindParam(":IdInmobiliaria", $IdInmobiliaria);
        $stmt->bindParam(":codinm", $codinm);
        if ($stmt->execute()) {
            $error  = 0;
            $cadena = '';

            while ($row = $stmt->fetch()) {
                $IdBarrios        = $row['IdBarrio'];
                $banos            = $row['banos'];
                $alcobas          = $row['alcobas'];
                $garaje           = $row['garaje'];
                $DirMetro         = $row['DirMetro'];
                $ValorVenta       = $row['ValorVenta'];
                $ValorCanon       = $row['ValorCanon'];
                $Administracion   = $row['Administracion'];
                $IdTpInm          = $row['IdTpInm'];
                $Estrato          = $row['Estrato'];
                $AreaConstruida   = $row['AreaConstruida'];
                $AreaLote         = $row['AreaLote'];
                $EdadInmueble     = $row['EdadInmueble'];
                $idEstadoInmueble = $row['idEstadoInmueble'];
                $IdGestion        = $row['IdGestion'];
                $latitud          = $row['latitud'];
                $longitud         = $row['longitud'];
            }
            $barrzp = getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdBarrZp');
            $barrml = strlen(getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdBarrMeli'));
            if ($barrml <= 3 and $IdBarrios > 0 and $codPortal == 2) {
                $error++;
                $cadena .= utf8_encode(" - Barrio No codificado en TuInmueble") . " <br>";
            }
            // if($barrzp<=0 and $IdBarrios>0 and $codPortal==5)
            // {
            //     $error++;
            //     $cadena .=utf8_encode(" - Barrio No codificado en ZonaProp")." <br>";
            // }
            if ($latitud == 0 and $codPortal != 10) {
                $error++;
                $cadena .= utf8_encode(" - Falta Latitud") . " <br>";
            }
            if ($longitud == 0 and $codPortal != 10) {
                $error++;
                $cadena .= utf8_encode(" - Falta Longitud") . " <br>";
            }
            if ($IdBarrios <= 0) {
                $error++;
                $cadena .= utf8_encode(" - Falta Barrio") . " <br>";
            }
            if ($IdGestion <= 0) {
                $error++;
                $cadena .= utf8_encode(" - Falta Gesti&oacute;n") . " <br>";
            }
            if ($banos <= 0 && $IdTpInm != 9) {
                $error++;
                $cadena .= utf8_encode(" - Debe Agregar  Ba&ntilde;os") . " <br>";
            }
            if ($alcobas <= 0 && ($IdTpInm != 3 && $IdTpInm != 4 && $IdTpInm != 5 && $IdTpInm != 6 && $IdTpInm != 7 && $IdTpInm != 9)) {
                $error++;
                $cadena .= utf8_encode(" - Debe Agregar Alcobas") . " <br>";
            }
            if ($DirMetro == '' and $codPortal == 10) {
                $error++;
                $cadena .= utf8_encode(" - Falta Falta Direcci&oacute;n Para MetroCuadrado") . " <br>";
            }
            if ($ValorVenta <= 0 and $IdGestion != 1) {
                $error++;
                $cadena .= utf8_encode(" - Falta Valor Venta") . " <br>";
            }
            if ($ValorCanon <= 0 and $IdGestion == 1) {
                $error++;
                $cadena .= utf8_encode(" - Falta Valor Canon") . " <br>";
            }
            if ($Administracion <= 0) {
                //$error++;
                //  $cadena .=" No Registra Administracion<br>";
            }
            if ($IdTpInm <= 0) {
                $error++;
                $cadena .= utf8_encode(" - Falta Tipo Inmueble") . " <br>";
            }
            if ($AreaConstruida <= 0 && $IdTpInm != 7) {
                $error++;
                $cadena .= utf8_encode(" - Falta Area Construida") . " <br>";
            }
            if ($AreaLote <= 0) {
                $error++;
                $cadena .= utf8_encode(" - Falta Area Lote") . " <br>";
            }
            if ($Estrato <= 0) {
                $error++;
                $cadena .= utf8_encode(" - Falta Estrato") . " <br>";
            }
            if ($EdadInmueble <= 0) {
                $error++;
                $cadena .= utf8_encode(" - Falta Edad del Inmueble") . " <br>";
            }
            if ($idEstadoInmueble != 2) {
                $error++;
                $cadena .= utf8_encode(" - Estado no es disponible") . " <br>";
            }
            return $cadena;
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function updateFinancialOld($data)
    {
        $connPDO = new Conexion();

        $codinm       = $data['codinm'];
        $idInm        = $data['IdInmobiliaria'] . "-" . $codinm;
        $fechaserv    = date('Y-m-d');
        $usu_creacion = $_SESSION['iduser'];
        $h_creacion   = date('Y-m-d');
        $ip           = $_SERVER['REMOTE_ADDR'];
        $estadoin     = (getCampo('inmobiliaria', "where Idinmobiliaria=" . $data['IdInmobiliaria'], 'aestado') == 1) ? 8 : 2;
        if ($_POST['destacado'] == 'on') {$destacado = 1;} else { $destacado = 0;}
        if ($data['destinacion'] == 2) {
            $valIva = $data['ValorIva'];
        } else {
            $valIva = 0;
        }
        if ($data['incluida'] == 'on') {$incluida = 1;} else { $incluida = 0;}
        if ($data['amoblado'] == 'on') {$amoblado = 1;} else { $amoblado = 0;}
        if ($data['destacado'] == 'on') {$destacado = 1;} else { $destacado = 0;}
        $stmt = $connPDO->prepare("UPDATE inmuebles
            SET
            IdGestion            =:gestioncomercial,
            FConsignacion        =:fecha_inicio,
            Estrato              =:estrato,
            AreaConstruida       =:areaconstruida,
            AreaLote             =:arealote,
            EdadInmueble         =:anoconstruccion,
            NoMatricula          =:nromatri,
            chip                 =:chip,
            AvaluoCatastral      =:calledavaluocatas,
            CedulaCatastral      =:cedulacatast,
            fingreso             =:fingreso,
            amoblado             =:amoblado,
            restricciones        =:restricciones,
            obserinterna         =:obserinterna,
            codinterno           =:codinterno,
            FechaAvaluoCatastral =:Favaluo,
            ValorVenta           =:valventa,
            ValorCanon           =:canon,
            Administracion       =:admon,
            ValorIva             =:valIva,
            admondto             =:admondto,
            IdProcedencia        =:procedencia,
            idCaptador           =:codcaptador,
            IdPromotor           =:idpromotor,
            IdDestinacion        =:destinacion,
            AdmonIncluida        =:incluida,
            IdTpInm              =:tipoinmueble,
            idEstadoinmueble     =:idEstadoinmueble,
            Telefonoinm          =:Telefonoinm,
            uso                  =:uso,
            sede                 =:sede
            WHERE codinm         =:codinm
            AND IdInmobiliaria   =:IdInmobiliaria");

        if ($stmt->execute(array(
            ":gestioncomercial"  => $data['gestioncomercial'],
            ":fecha_inicio"      => $data['fecha_inicio'],
            ":estrato"           => $data['estrato'],
            ":areaconstruida"    => str_ireplace(",", ".", $data['areaconstruida']),
            ":arealote"          => str_ireplace(",", ".", $data['arealote']),
            ":anoconstruccion"   => $data['anoconstruccion'],
            ":nromatri"          => $data['nromatri'],
            ":chip"              => $data['chip'],
            ":calledavaluocatas" => $data['calledavaluocatas'],
            ":cedulacatast"      => $data['cedulacatast'],
            ":fingreso"          => $fechaserv,
            ":amoblado"          => $amoblado,
            ":restricciones"     => $data['restricciones'],
            ":obserinterna"      => $data['obserinterna'],
            ":codinterno"        => $data['codinterno'],
            ":Favaluo"           => $data['Favaluo'],
            ":valventa"          => str_ireplace(".", "", $data['valventa']),
            ":canon"             => str_ireplace(".", "", $data['canon']),
            ":admon"             => str_ireplace(".", "", $data['admon']),
            ":valIva"            => str_ireplace(".", "", $valIva),
            ":admondto"          => str_ireplace(".", "", $data['admondto']),
            ":procedencia"       => $data['procedencia'],
            ":codcaptador"       => $data['codcaptador'][0],
            ":idpromotor"        => $data['idpromotor'][0],
            ":destinacion"       => $data['destinacion'],
            ":incluida"          => $incluida,
            ":tipoinmueble"      => $data['tipoinmueble'],
            ":codinm"            => $codinm,
            ":IdInmobiliaria"    => $data['IdInmobiliaria'],
            ":Telefonoinm"       => $data['telInmueble'],
            ":uso"               => $data['usoInmueble'],
            ":sede"              => $data['sede'],
            ":idEstadoinmueble"  => $estadoin,
        ))) {
            //destacados
            guardar_settings_portales($data['IdInmobiliaria'], $idInm, 0, 4, $destacado, $_SESSION['Id_Usuarios'], $fechaserv, $h_creacion, $ip);

            //amoblado
            guardar_settings_portales($data['IdInmobiliaria'], $idInm, 0, 6, $amoblado, $_SESSION['Id_Usuarios'], $fechaserv, $h_creacion, $ip);
            //saveLogFinancial($data,$idInm);
            Inmuebles::updateFinancialNew($data, $codinm);
            $inmueb          = array();
            return $inmueb[] = array("codinmu" => $codinm);
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function updateFinancialNew($data, $codinm)
    {
        $connPDO = new Conexion();

        //$codinm         = $this->consecInmuNew($data['IdInmobiliaria']);
        $idInm        = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $fechaserv    = date('Y-m-d');
        $usu_creacion = $_SESSION['iduser'];
        $h_creacion   = date('Y-m-d');
        $ip           = $_SERVER['REMOTE_ADDR'];
        if ($_POST['destacado'] == 'on') {$destacado = 1;} else { $destacado = 0;}
        if ($data['destinacion'] == 2) {
            $valIva = $data['ValorIva'];
        } else {
            $valIva = 0;
        }
        if ($data['incluida'] == 'on') {$incluida = 1;} else { $incluida = 0;}

        $stmt = $connPDO->prepare("UPDATE inmnvo
        SET
        IdGestion            =:gestioncomercial,
        FConsignacion        =:fecha_inicio,
        Estrato              =:estrato,
        AreaConstruida       =:areaconstruida,
        AreaLote             =:arealote,
        EdadInmueble         =:anoconstruccion,
        NoMatricula          =:nromatri,
        chip                 =:chip,
        AvaluoCatastral      =:calledavaluocatas,
        CedulaCatastral      =:cedulacatast,
        fingreso             =:fingreso,
        restricciones        =:restricciones,
        obserinterna         =:obserinterna,
        codinterno           =:codinterno,
        FechaAvaluoCatastral =:Favaluo,
        ValorVenta           =:valventa,
        ValorCanon           =:canon,
        Administracion       =:admon,
        ValorIva             =:valIva,
        admondto             =:admondto,
        IdProcedencia        =:procedencia,
        IdDestinacion        =:destinacion,
        AdmonIncluida        =:incluida,
        IdTpInm              =:tipoinmueble,
        Telefonoinm          =:Telefonoinm,
        uso                  =:uso,
        sede                 =:sede
        WHERE codinm         =:codinm
        and IdInmobiliaria   =:IdInmobiliaria");

        if ($stmt->execute(array(
            ":gestioncomercial"  => $data['gestioncomercial'],
            ":fecha_inicio"      => $data['fecha_inicio'],
            ":estrato"           => $data['estrato'],
            ":areaconstruida"    => str_ireplace(",", ".", $data['areaconstruida']),
            ":arealote"          => str_ireplace(",", ".", $data['arealote']),
            ":anoconstruccion"   => $data['anoconstruccion'],
            ":nromatri"          => $data['nromatri'],
            ":chip"              => $data['chip'],
            ":calledavaluocatas" => $data['calledavaluocatas'],
            ":cedulacatast"      => $data['cedulacatast'],
            ":fingreso"          => $fechaserv,
            ":restricciones"     => $data['restricciones'],
            ":obserinterna"      => $data['obserinterna'],
            ":codinterno"        => $data['codinterno'],
            ":Favaluo"           => $data['Favaluo'],
            ":valventa"          => str_ireplace(".", "", $data['valventa']),
            ":canon"             => str_ireplace(".", "", $data['canon']),
            ":admon"             => str_ireplace(".", "", $data['admon']),
            ":valIva"            => str_ireplace(".", "", $valIva),
            ":admondto"          => str_ireplace(".", "", $data['admondto']),
            ":procedencia"       => $data['procedencia'],
            ":destinacion"       => $data['destinacion'],
            ":incluida"          => $incluida,
            ":tipoinmueble"      => $data['tipoinmueble'],
            ":Telefonoinm"       => $data['telInmueble'],
            ":uso"               => $data['usoInmueble'],
            ":codinm"            => $data['codinm'],
            ":sede"              => $data['sede'],
            ":IdInmobiliaria"    => $data['IdInmobiliaria'],

        ))) {

            //promotor 3 captador 4
            for ($i = 0; $i < count($data['idpromotor']); $i++) {
                $this->saveTercero($data['IdInmobiliaria'], $codinm, 3, $data['idpromotor'][$i]);
            }
            for ($j = 0; $j < count($data['codcaptador']); $j++) {
                $this->saveTercero($data['IdInmobiliaria'], $codinm, 4, $data['codcaptador'][$j]);
            }

            $inmueb          = array();
            return $inmueb[] = array("codinmu" => $codinm);
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function datosInmuebleTerceros($data)
    {
        $connPDO = new Conexion();

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $cond  = '';
        if ($data['rol_tr'] > 0) {
            $cond .= ' and rol_tr=:rol_tr';
        }
        if ($data['iduser'] > 0) {
            $cond .= ' and iduser_tr=:iduser';
        }
        $stmt = $connPDO->prepare("SELECT inmob_tr,idinm_tr,rol_tr,iduser_tr,idusu_tr
                from inmueblesterceros
                where idinm_tr =:codinm
                and inmob_tr   = :idinmo
                $cond");
        if ($data['rol_tr'] > 0) {
            $stmt->bindParam(":rol_tr", $data['rol_tr']);
        }
        if ($data['iduser'] > 0) {
            $stmt->bindParam(":iduser", $data['iduser']);
        }
        $stmt->bindParam(":idinmo", $data['IdInmobiliaria']);
        $stmt->bindParam(":codinm", $data['codinm']);
        if ($stmt->execute()) {
            $data = array();
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "rol_tr"      => $row['rol_tr'],
                    "iduser_tr"   => $row['iduser_tr'],
                    "nrol"        => utf8_encode(getCampo('parametros', "where id_param=51 and conse_param=" . $row['rol_tr'], 'desc_param')),
                    "ntercero"    => utf8_encode(getCampo('usuarios', "where iduser='" . $row['iduser_tr'] . "'", 'concat(Nombres," ",apellidos)')),
                    "cedtercero"  => getCampo('usuarios', "where iduser='" . $row['iduser_tr'] . "'", 'Id_Usuarios'),
                    "tDoctercero" => getCampo('usuarios', "where iduser='" . $row['iduser_tr'] . "'", 'IdTipoDocumento'),
                );
            }
            return $data;
        } else {
            return print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function disableInmuebleTercerosOld($data)
    {
        $connPDO = new Conexion();

        //$codinm         = $this->consecInmuNew($data['IdInmobiliaria']);
        $idInm   = $data['IdInmobiliaria'] . "-" . $codinm;
        $idVacio = 0;
        Inmuebles::disableInmuebleTercerosNew($data);

        $stmt = $connPDO->prepare("UPDATE inmuebles
        SET
        IdPropietario      =:idVacio
        WHERE codinm       =:codinm
        AND IdInmobiliaria =:IdInmobiliaria
        AND IdPropietario  =:iduser_tr");

        if ($stmt->execute(array(
            ":idVacio"        => $idVacio,
            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":iduser_tr"      => $data['cedprop'],
        ))) {

            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function disableInmuebleTercerosNew($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("DELETE FROM inmueblesterceros
        WHERE idinm_tr =:codinm
        AND inmob_tr   =:IdInmobiliaria
        and rol_tr     =:rol_tr
        and iduser_tr  =:iduser");

        if ($stmt->execute(array(
            ":rol_tr"         => $data['rol_tr'],
            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":iduser"         => $data['iduser_tr'],
        ))) {
            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function updatePropietario($data, $iduser = '')
    //  public function saveFinancialNew($data)
    {
        $connPDO = new Conexion();

        $iduser    = $iduser;
        $idInm     = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $fechaserv = date('Y-m-d');

        if ($data['tipodocu'] != 2) {
            $razon_social = $apellidos . " " . $Nombres;
        } else {
            $razon_social = $data['razon_social'];
            $Nombres      = $data['razon_social'];
        }
        $tel1 = "(" . $data['indict1'] . ")" . $data['telefonoprop'];
        if ($data['indict2'] > 0) {
            $tel2 = "(" . $data['indict2'] . ")" . $data['telefonoprop2'];
        } else {
            $tel2 = "";
        }
        if ($data['indict3'] > 0) {
            $tel3 = "(" . $data['indict3'] . ")" . $data['telefonoprop3'];
        } else {
            $tel3 = "";
        }
        if ($data['paisd'] != 169) {
            $deptod     = 0;
            $ciudad     = 0;
            $ciudadtext = $data['ciudadtext'];
        } else {
            $deptod     = $data['depto_u'];
            $ciudadtext = ucwords(strtolower(getCampo('ciudades_dian', "where id_ciudad=" . $data['ciudadd'], 'nombre_ciudad')));
        }
        $apellidos = trim($data['apellido1']) . " " . trim($data['apellido2']);
        $Nombres   = trim($data['nombre1']) . " " . trim($data['nombre2']);

        $stmt = $connPDO->prepare("UPDATE usuarios2
                        SET
                        Nombres         =:Nombres,
                        apellidos       =:apellidos,
                        Direccion       =:Direccion,
                        nombre1         =:nombre1,
                        nombre2         =:nombre2,
                        apellido1       =:apellido1,
                        apellido2       =:apellido2,
                        razon_social    =:razon_social,
                        Telefono        =:Telefono,
                        telefono2       =:telefono2,
                        telefono3       =:telefono3,
                        Correo          =:Correo,
                        Celular         =:Celular,
                        celular2        =:celular2,
                        pais_u          =:pais_u,
                        depto_u         =:depto_u,
                        ciudad_u        =:ciudad_u,
                        ext1            =:ext1,
                        ext2            =:ext2,
                        ext3            =:ext3,
                        f_nacimiento    =:f_nacimiento,
                        vip             =:vip,
                        ciudad_utext    =:ciudad_utext,
                        IdTipoDocumento =:IdTipoDocumento
                        WHERE iduser    =:iduser");
        // and IdInmmo     =:IdInmmo  ":IdInmmo"         => $data['IdInmobiliaria']

        if ($stmt->execute(array(
            ":Nombres"         => utf8_encode($Nombres),
            ":apellidos"       => utf8_encode($apellidos),
            ":Direccion"       => utf8_encode($data['direccionprop']),
            ":nombre1"         => utf8_encode($data['nombre1']),
            ":nombre2"         => utf8_encode($data['nombre2']),
            ":apellido1"       => utf8_encode($data['apellido1']),
            ":apellido2"       => utf8_encode($data['apellido2']),
            ":razon_social"    => utf8_encode($razon_social),
            ":Telefono"        => $tel1,
            ":telefono2"       => $tel2,
            ":telefono3"       => $tel3,
            ":Correo"          => utf8_encode($data['emailpro']),
            ":Celular"         => utf8_encode($data['celularprop']),
            ":celular2"        => utf8_encode($data['celularprop2']),
            ":pais_u"          => utf8_encode($data['paisd']),
            ":depto_u"         => utf8_encode($deptod),
            ":ciudad_u"        => utf8_encode($data['ciudadd']),
            ":ext1"            => utf8_encode($data['ext1']),
            ":ext2"            => utf8_encode($data['ext2']),
            ":ext3"            => utf8_encode($data['ext3']),
            ":ciudad_utext"    => utf8_encode($ciudadtext),
            ":IdTipoDocumento" => $data['tipodocu'],
            ":f_nacimiento"    => $data['dateBirth'],
            ":vip"             => $data['vip'],
            ":iduser"          => $iduser,
        ))) {
            //propietario 1 apoderado 4
            Inmuebles::saveTercero($data['IdInmobiliaria'], $data['codinm'], $data['rol_tr'], $iduser);

            return 0;
            //$inmueb=array();
            //return $inmueb[]=array("codinmu"=>$codinm);
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function validaInfoTercero($data)
    //  public function saveFinancialNew($data)
    {
        $connPDO = new Conexion();
        $cond    = '';
        if ($data['IdInmobiliaria'] == 11111) {
            $cond = 'and IdInmmo     =:IdInmmo ';
        }

        $stmt = $connPDO->prepare("SELECT  Nombres,apellidos,Direccion,nombre1,nombre2,apellido1,
                        apellido2,razon_social,Telefono,telefono2,telefono3,Correo,
                        Celular,celular2,pais_u,depto_u,ciudad_u,ext1,
                        ext2,ext3,ciudad_utext,IdTipoDocumento,IdInmmo,iduser,Id_Usuarios,vip,f_nacimiento
                        FROM usuarios2
                        WHERE Id_Usuarios   =:usuario

                        $cond");
        //and IdTipoDocumento =:tipodocu
        $stmt->bindParam(':usuario', $data['cedprop']);
        //$stmt->bindParam(':tipodocu',$data['tipodocu']);
        if ($data['IdInmobiliaria'] == 11111) {
            $stmt->bindParam(':IdInmmo', $data['IdInmobiliaria']);
        }
        if ($stmt->execute()) {
            $cantidad = $stmt->rowCount();
            $info     = array();
            while ($row = $stmt->fetch()) {
                $razon_social = $row['nombre1'] . " " . $row['nombre2'] . " " . $row['apellido1'] . " " . $row['apellido2'];
                if (strstr($row['Telefono'], ')') == true) {
                    list($a, $telval1) = explode(")", $row['Telefono']);
                    $a                 = str_replace("(", "", $a);
                } else {
                    $a       = 0;
                    $telval1 = $row['Telefono'];
                }
                if (strstr($row['telefono2'], ')') == true) {
                    list($b, $telval2) = explode(")", $row['telefono2']);
                    $b                 = str_replace("(", "", $b);
                } else {
                    $b       = 0;
                    $telval2 = $row['telefono2'];
                }
                if (strstr($row['telefono3'], ')') == true) {
                    list($c, $telval3) = explode(")", $row['telefono3']);
                    $c                 = str_replace("(", "", $c);
                } else {
                    $c       = 0;
                    $telval3 = $row['telefono3'];
                }

                $info[] = array(
                    "Nombres"         => utf8_encode($row['Nombres']),
                    "apellidos"       => utf8_encode($row['apellidos']),
                    "Direccion"       => utf8_encode($row['Direccion']),
                    "nombre1"         => utf8_encode($row['nombre1']),
                    "nombre2"         => utf8_encode($row['nombre2']),
                    "apellido1"       => utf8_encode($row['apellido1']),
                    "apellido2"       => utf8_encode($row['apellido2']),
                    "razon_social"    => utf8_encode($razon_social),
                    "Telefono"        => $telval1,
                    "telefono2"       => $telval2,
                    "telefono3"       => $telval3,
                    "ind1"            => $a,
                    "ind2"            => $b,
                    "ind3"            => $c,
                    "Correo"          => utf8_encode($row['Correo']),
                    "Celular"         => utf8_encode($row['Celular']),
                    "celular2"        => utf8_encode($row['celular2']),
                    "pais_u"          => utf8_encode($row['pais_u']),
                    "depto_u"         => utf8_encode($row['depto_u']),
                    "ciudad_u"        => utf8_encode($row['ciudad_u']),
                    "ext1"            => utf8_encode($row['ext1']),
                    "ext2"            => utf8_encode($row['ext2']),
                    "ext3"            => utf8_encode($row['ext3']),
                    "ciudad_utext"    => utf8_encode($row['ciudadtext']),
                    "IdTipoDocumento" => $row['IdTipoDocumento'],
                    "vip"             => $row['vip'],
                    "f_nacimiento"    => $row['f_nacimiento'],
                    "Id_Usuarios"     => $row['Id_Usuarios'],
                    "iduser"          => $row['iduser'],
                    "IdInmmo"         => $row['IdInmmo'],
                );
            }
            if ($cantidad <= 0) {
                return 0;
            } else {
                return $info;
            }
        }
    }
    public function departamentosInmubles($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT  c.IdDepartamento as cod,CONCAT(UPPER(LEFT(d.Nombre, 1)), LOWER(SUBSTRING(d.Nombre, 2))) as nom
                                        FROM departamento d
                                        where d.IdDepartamento=depto
                                        order by d.nombre");
        $stmt->bindParam(':depto', $data['pais']);

        if ($stmt->execute()) {
            $response = array();

            while ($row = $stmt->fetch()) {
                $response[] = array(

                    "id"              => $row['cod'],
                    "nomDepartamento" => $row['nom'],
                );
            }
            return $response;
        } else {
            return $stmt->errorInfo();
            $stmt = null;
        }
    }
    public function ciudadesInmubles($data)
    {
        $connPDO = new Conexion();
        $addSql  = "";

        $stmt = $connPDO->prepare("SELECT c.IdCiudad AS cod,
                                          CONCAT(
                                            UPPER(LEFT(c.Nombre, 1)),
                                            LOWER(SUBSTRING(c.Nombre, 2))
                                          ) AS nom
                                        FROM ciudad c
                                        WHERE c.`IdDepartamento`=:depto
                                        ORDER BY c.nombre ");
        $stmt->bindParam(':depto', $data['depto']);

        if ($stmt->execute()) {
            $response = array();

            while ($row = $stmt->fetch()) {
                $response[] = array(

                    "id"              => $row['cod'],
                    "nomDepartamento" => $row['nom'],
                );
            }
            return $response;
        } else {
            return $stmt->errorInfo();
            $stmt = null;
        }
    }
    public function getDatapoliticasInmueble($data)
    {
        $connPDO = new Conexion();

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $connPDO->prepare("SELECT ComiVenta,ComiArren,ValComiVenta,ValComiArr,politica_comp
            FROM  inmnvo
            WHERE codinm       = :codinm
            AND IdInmobiliaria =:IdInmobiliaria
            ");

        if ($stmt->execute(array(

            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
        ))) {
            $info = array();
            while ($row = $stmt->fetch()) {
                $info[] = array(
                    "ComiVenta"     => ($row['ComiVenta'] * 100),
                    "ComiArren"     => ($row['ComiArren'] * 100),
                    "ValComiVenta"  => $row['ValComiVenta'],
                    "ValComiArr"    => $row['ValComiArr'],
                    "politica_comp" => $row['politica_comp'],
                );
            }

            return $info;
        } else {
            print_r($stmt->errorInfo());
        }
    }
    public function getDataUbicacionInmueble($data)
    {
        $connPDO = new Conexion();

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $connPDO->prepare("SELECT IdBarrio,Direccion,DirMetro,latitud,longitud,Carrera,Calle,dirs
            FROM  inmnvo
            WHERE codinm       = :codinm
            AND IdInmobiliaria =:IdInmobiliaria
            ");

        if ($stmt->execute(array(

            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
        ))) {
            $info = array();
            while ($row = $stmt->fetch()) {
                $idCiudad    = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], 'IdCiudad');
                $idlocalidad = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], 'IdLocalidad');
                $nbarrio     = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], 'NombreB');
                $nlocalidad  = getCampo('localidad', "where idlocalidad=" . $idlocalidad, 'descripcion');
                $idzona      = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], 'IdZona');
                $nzona       = getCampo('zonas', "where IdZona=" . $idzona, 'NombreZ');

                $iddepto                                                = getCampo('ciudad', "where IdCiudad=" . $idCiudad, 'IdDepartamento');
                $idPais                                                 = getCampo('departamento', "where IdDepartamento=" . $iddepto, 'IdPais');
                $depto                                                  = getCampo('departamento', "where IdDepartamento=" . $iddepto, 'Nombre');
                $barrio                                                 = "$nbarrio Localidad: $nlocalidad Zona: $nzona";
                list($c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10) = explode("|", str_replace("#", "", $row['dirs']));
                $info[]                                                 = array(
                    "ciudad"        => utf8_encode(getCampo('ciudad', "where IdBarrios=" . $idCiudad, 'Nombre')),
                    "idciudad"      => $idCiudad,
                    "pais"          => utf8_encode(getCampo('paises_dian', "where id_pais=$idPais", 'nombre_pais')),
                    "idpais"        => $idPais,
                    "idlocalidad"   => $idlocalidad,
                    "idzona"        => $idzona,
                    "idbarrio"      => utf8_encode($row['IdBarrio']),
                    "depto"         => ucwords(strtolower(utf8_encode($depto))),
                    "barrio"        => utf8_encode(getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], 'NombreB')),
                    "direccion"     => utf8_encode($row['Direccion']),
                    "latitud"       => $row['latitud'],
                    "longitud"      => $row['longitud'],
                    "calle"         => $row['Calle'],
                    "carrera"       => $row['Carrera'],
                    "localidad"     => ucfirst(strtolower(utf8_encode($nlocalidad))),
                    "zona"          => ucfirst(strtolower(utf8_encode($nzona))),
                    "nombre_barrio" => ucfirst(strtolower(utf8_encode($nbarrio))),
                    "DirMetro"      => utf8_encode($row['DirMetro']),
                    "c1"            => utf8_encode(trim($c1)),
                    "c2"            => utf8_encode(trim($c2)),
                    "c3"            => utf8_encode(trim($c3)),
                    "c4"            => utf8_encode(trim($c4)),
                    "c5"            => utf8_encode(trim($c5)),
                    "c6"            => utf8_encode(trim($c6)),
                    "c7"            => utf8_encode(trim($c7)),
                    "c8"            => utf8_encode(trim($c8)),
                    "c9"            => utf8_encode(trim($c9)),
                    "c10"           => utf8_encode(trim($c10)),
                );

            }

            return $info;
        } else {
            print_r($stmt->errorInfo());
        }

    }
    public function getDataFinancialInmueble($data)
    {
        $connPDO = new Conexion();

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $connPDO->prepare("SELECT IdGestion,FConsignacion,Estrato,AreaConstruida,AreaLote,
            EdadInmueble,NoMatricula,chip,AvaluoCatastral,CedulaCatastral,
            fingreso,restricciones,obserinterna,codinterno,FechaAvaluoCatastral,
            ValorVenta,ValorCanon,Administracion,ValorIva,admondto,
            IdProcedencia,IdDestinacion,AdmonIncluida,IdTpInm,idInm,
            codinm,usu_creacion,f_creacion,h_creacion,IdInmobiliaria,descripcionlarga,descripcionMetro,sede,Telefonoinm,uso
            FROM  inmnvo
            WHERE codinm       = :codinm
            AND IdInmobiliaria =:IdInmobiliaria
            ");

        if ($stmt->execute(array(

            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
        ))) {
            $info = array();
            while ($row = $stmt->fetch()) {

                $destacado = getCampo('settings_portales', "where tipo_p=4 and inmu_p='$idInm'", 'vlr_p');
                $amoblado  = getCampo('settings_portales', "where tipo_p=6 and inmu_p='$idInm'", 'vlr_p');
                $elegido   = getCampo('settings_portales', "where portal_p=0 and tipo_p=5 and inmu_p='" . $idInm . "'", 'vlr_p');
                $video     = getCampo('inmuebles', "where idInm='$idInm'", 'linkvideo');
                $info[]    = array(
                    "IdGestion"            => $row['IdGestion'],
                    "FConsignacion"        => $row['FConsignacion'],
                    "Estrato"              => $row['Estrato'],
                    "AreaConstruida"       => $row['AreaConstruida'],
                    "AreaLote"             => $row['AreaLote'],
                    "EdadInmueble"         => $row['EdadInmueble'],
                    "NoMatricula"          => $row['NoMatricula'],
                    "chip"                 => $row['chip'],
                    "AvaluoCatastral"      => $row['AvaluoCatastral'],
                    "CedulaCatastral"      => $row['CedulaCatastral'],
                    "fingreso"             => $row['fingreso'],
                    "restricciones"        => $row['restricciones'],
                    "obserinterna"         => $row['obserinterna'],
                    "codinterno"           => $row['codinterno'],
                    "FechaAvaluoCatastral" => $row['FechaAvaluoCatastral'],
                    "ValorVenta"           => $row['ValorVenta'],
                    "ValorCanon"           => $row['ValorCanon'],
                    "Administracion"       => $row['Administracion'],
                    "ValorIva"             => $row['ValorIva'],
                    "admondto"             => $row['admondto'],
                    "IdProcedencia"        => $row['IdProcedencia'],
                    "IdDestinacion"        => $row['IdDestinacion'],
                    "AdmonIncluida"        => $row['AdmonIncluida'],
                    "IdTpInm"              => $row['IdTpInm'],
                    "idInm"                => $row['idInm'],
                    "codinm"               => $row['codinm'],
                    "usu_creacion"         => $row['usu_creacion'],
                    "IdInmobiliaria"       => $row['IdInmobiliaria'],
                    "Telefonoinm"          => $row['Telefonoinm'],
                    "uso"                  => $row['uso'],
                    "amoblado"             => $amoblado,
                    "destacado"            => $destacado,
                    "video"                => utf8_encode($video),
                    "descripcionlarga"     => utf8_encode($row['descripcionlarga']),
                    "descripcionMetro"     => utf8_encode($row['descripcionMetro']),
                    "sede"                 => $row['sede'],
                    "elegido"              => $elegido,
                );
            }

            return $info;
        } else {
            print_r($stmt->errorInfo());
        }

    }
    public function deleteCaracteristicasLateral($IdInmobiliaria, $codinm)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("UPDATE inmnvo
                        set
                        banos              =0,
                        garaje             =0,
                        alcobas            =0
                        WHERE codinm       =:codinm
                        AND IdInmobiliaria =:inmob");

        if ($stmt->execute(array(
            ":inmob"  => $IdInmobiliaria,
            ":codinm" => $codinm,

        ))) {

            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function getDataDetalleInmueble($data, $idcar)
    {
        $connPDO = new Conexion();

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $connPDO->prepare("SELECT  idcaracteristica,obser_det,cantidad_dt
             FROM detalleinmueble
             WHERE inmob   = :IdInmobiliaria
             AND codinmdet =:codinm
             AND idCaracteristica=:idcar");

        if ($stmt->execute(array(

            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":idcar"          => $idcar,

        ))) {
            $info = array();
            while ($row = $stmt->fetch()) {

                $info[] = array(
                    "idcaracteristica" => $row['idcaracteristica'],
                    "obser_det"        => $row['obser_det'],
                    "cantidad_dt"      => $row['cantidad_dt'],
                );
            }

            return $info;
        } else {
            print_r($stmt->errorInfo());
        }

    }
    public function getDataDetalleLateralInmueble($data)
    {
        $connPDO = new Conexion();

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $connPDO->prepare("SELECT  banos,garaje,alcobas
             FROM inmnvo
             WHERE IdInmobiliaria = :IdInmobiliaria
             AND codinm           =:codinm");

        if ($stmt->execute(array(

            ":codinm"         => $data['codinm'],
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
        ))) {
            $info = array();
            while ($row = $stmt->fetch()) {

                $info[] = array(
                    "banos"   => $row['banos'],
                    "garaje"  => $row['garaje'],
                    "alcobas" => $row['alcobas'],
                );
            }

            return $info;
        } else {
            print_r($stmt->errorInfo());
        }

    }
    public function verifyInmobiliariaMetro($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT  inmogm2
             FROM usuariosm2
             WHERE inmogm2 = :IdInmobiliaria
             group by inmogm2");
        $stmt->bindParam(':IdInmobiliaria', $data['IdInmobiliaria']);

        if ($stmt->execute()) {
            $cantidad = $stmt->rowCount();

            return $cantidad;
        } else {
            print_r($stmt->errorInfo());
        }

    }
    public function updateInmueblesPortaleslOld($codinm, $IdInmobiliaria, $RMtoCuadrado, $RLaGuia, $RMeli, $RZonaProp, $RMiCasa, $RAfydi, $RNcasa, $RVivaReal, $RIdnd, $PublicaGuia, $PublicaMeli, $PublicaZonaProp, $PublicaVReal, $PublicaIdn)
    {
        $connPDO = new Conexion();

        $idInm = $IdInmobiliaria . "-" . $codinm;

        $stmt = $connPDO->prepare("UPDATE inmuebles
            SET
            RMtoCuadrado       = :RMtoCuadrado,
            RLaGuia            = :RLaGuia,
            RMeli              = :RMeli,
            RZonaProp          = :RZonaProp,
            PublicaM2          = :PublicaM2,
            PublicaGuia        = :PublicaGuia,
            PublicaMeli        = :PublicaMeli,
            PublicaZonaProp    = :PublicaZonaProp,
            PublicaVReal       = :PublicaVReal,
            PublicaOlx         = :PublicaOlx,
            Publicalocanto     = :Publicalocanto,
            RVivaReal          = :RVivaReal,
            RIdnd              = :RIdnd,
            Publicagpt         = :Publicagpt,
            PublicaIdn         = :PublicaIdn,
            PublicaProperati   = :PublicaProperati,
            PublicaLamudi      = :PublicaLamudi,
            PublicaPautaR      = :PublicaPautaR,
            PublicaTM          = :PublicaZonaProp,
            PublicaAbad        = :PublicaAbad,
            PublicaMiCasa      = :PublicaMiCasa,
            RMiCasa            = :RMiCasa,
            PublicaDoomos      = :PublicaDoomos,
            RAfydi             = :RAfydi,
            RNcasa             = :RNcasa
            WHERE codinm       = :codinm
            AND IdInmobiliaria = :IdInmobiliaria");

        if ($stmt->execute(array(
            ":RMtoCuadrado"     => $RMtoCuadrado,
            ":RLaGuia"          => $RLaGuia,
            ":RMeli"            => $RMeli,
            ":RZonaProp"        => $RZonaProp,
            ":PublicaM2"        => $PublicaM2,
            ":PublicaGuia"      => $PublicaGuia,
            ":PublicaMeli"      => $PublicaMeli,
            ":PublicaZonaProp"  => $PublicaZonaProp,
            ":PublicaVReal"     => $PublicaVReal,
            ":PublicaOlx"       => $PublicaOlx,
            ":Publicalocanto"   => $Publicalocanto,
            ":RVivaReal"        => $RVivaReal,
            ":RIdnd"            => $RIdnd,
            ":Publicagpt"       => $Publicagpt,
            ":PublicaIdn"       => $PublicaIdn,
            ":PublicaProperati" => $PublicaProperati,
            ":PublicaLamudi"    => $PublicaLamudi,
            ":PublicaPautaR"    => $PublicaPautaR,
            ":PublicaTM"        => $PublicaTM,
            ":PublicaAbad"      => $PublicaAbad,
            ":PublicaMiCasa"    => $PublicaMiCasa,
            ":RMiCasa"          => $RMiCasa,
            ":PublicaDoomos"    => $PublicaDoomos,
            ":RAfydi"           => $RAfydi,
            ":RNcasa"           => $RNcasa,
            ":codinm"           => $codinm,
            ":IdInmobiliaria"   => $IdInmobiliaria,
        ))) {

            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function updateInmueblesPortalesNew($codinm, $IdInmobiliaria, $Portal, $codigo, $sincroniza, $infoProm, $suc, $errorSinc, $urlpublica_dtp = '')
    {
        $connPDO = new Conexion();

        $idInm = $IdInmobiliaria . "-" . $codinm;
        $f_sis = date('Y-m-d');
        $h_sis = date('H-i-s');
        $ip    = $_SERVER['REMOTE_ADDR'];

        $conse_dtp = consecutivo("conse_dtp", "detportalesinmueble");
        $stmt      = $connPDO->prepare("REPLACE INTO detportalesinmueble
         (conse_dtp,idinm_dtp,inmob_dtp,portal_dtp,
                cod_dtp,urlpublica_dtp)
            VALUES(:conse_dtp,:idinm_dtp,:inmob_dtp,:portal_dtp,
                :cod_dtp,:urlpublica_dtp)");
        $data['urlpublica_dtp'] = ' ';
        if ($codPortal > 0) {
            $stmt->bindParam(":codPortal", $codPortal);
        }
        $stmt->bindParam(":conse_dtp", $conse_dtp);
        $stmt->bindParam(":idinm_dtp", $codinm);
        $stmt->bindParam(":inmob_dtp", $IdInmobiliaria);
        $stmt->bindParam(":portal_dtp", $Portal);
        $stmt->bindParam(":cod_dtp", $codigo);
        $stmt->bindParam(":urlpublica_dtp", $urlpublica_dtp);

        guardar_settings_portales($IdInmobiliaria, $idInm, $Portal, 1, $sincroniza, $_SESSION['Id_Usuarios'], $f_sis, $h_sis, $ip);
        //errores sincro
        guardar_settings_portales($IdInmobiliaria, $idInm, $Portal, 6, $errorSinc, $_SESSION['Id_Usuarios'], $f_sis, $h_sis, $ip);
        if ($Portal == 10 || $Portal == 8 || $Portal == 2 || $Portal == 5) {
            guardar_settings_portales($IdInmobiliaria, $idInm, $Portal, 2, $infoProm, $_SESSION['Id_Usuarios'], $f_sis, $h_sis, $ip);
            guardar_settings_portales($IdInmobiliaria, $idInm, $Portal, 3, $suc, $_SESSION['Id_Usuarios'], $f_sis, $h_sis, $ip);
        }

        if ($stmt->execute()) {
            return 1;
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function updateInmueblesPortalesNew2($data)
    {
        $connPDO = new Conexion();

        $IdPortal           = $data['IdPortal'];
        $mostrar_cod        = $data['mostrar_cod'];
        $mostrar_sincroniza = $data['mostrar_sincroniza'];
        $mostrar_infprom    = $data['mostrar_infprom'];
        $select_suc         = $data['select_suc'];
        $errorSincro        = $data['errorSincro'];
        $IdInmobiliaria     = $data['IdInmobiliaria'];
        $codinm             = $data['codinm'];

        foreach ($IdPortal as $key => $value) {
            $Portal     = $value;
            $codigo     = ($mostrar_cod[$key] != '') ? $mostrar_cod[$value] : 0;
            $sincroniza = ($mostrar_sincroniza[$key] != '') ? 1 : 0;
            $infoProm   = ($mostrar_infprom[$key] != '') ? 1 : 0;
            $suc        = ($select_suc[$key] != '') ? $select_suc[$value] : 0;
            $errorSinc  = ($errorSincro[$key] != '') ? $errorSincro[$value] : 0;
            // echo "Portal $Portal codigo $codigo sincroniza $sincroniza infoProm $infoProm  suc $suc<br>";

            if ($Portal == 1) {
                $PublicaDoomos = ($Portal == 1) ? $codigo : 'Por Publicar';
            }
            if ($Portal == 2) {
                $RMeli       = ($Portal == 2) ? $codigo : 0;
                $PublicaMeli = ($Portal == 2 && $codigo > 0) ? 'Publicado' : 'Por Publicar';
            }
            if ($Portal == 4) {
                $RMiCasa       = ($Portal == 4) ? $codigo : 0;
                $PublicaMiCasa = ($Portal == 4) ? 'Publicado' : 'Por Publicar';
            }
            if ($Portal == 5) {
                $RZonaProp       = ($Portal == 5) ? $codigo : 0;
                $PublicaZonaProp = ($Portal == 5 && $codigo > 0) ? 'Publicado' : 'Por Publicar';
            }
            if ($Portal == 6) {
                $Publicagpt = ($Portal == 6) ? 'Publicado' : 'Por Publicar';
            }
            if ($Portal == 7) {
                $RVivaReal    = ($Portal == 7) ? $codigo : 0;
                $PublicaVReal = ($Portal == 7 && $codigo > 0) ? 'Publicado' : 'Por Publicar';
            }
            if ($Portal == 8) {
                $RLaGuia     = ($Portal == 8) ? $codigo : 0;
                $PublicaGuia = ($Portal == 8 && $codigo > 0) ? 'Publicado' : 'Por Publicar';
            }
            if ($Portal == 10) {
                $RMtoCuadrado = ($Portal == 10) ? $codigo : 0;
                $PublicaM2    = ($Portal == 10 && $codigo > 0) ? 'Publicado' : 'Por Publicar';
            }
            if ($Portal == 12) {
                $PublicaProperati = ($Portal == 12) ? 'Publicado' : 'Por Publicar';
            }
            if ($Portal == 13) {
                $PublicaLamudi = ($Portal == 13) ? $codigo : 'Por Publicar';
            }
            if ($Portal == 14) {
                $RIdnd      = ($Portal == 14) ? $codigo : 0;
                $PublicaIdn = ($Portal == 14 && $codigo > 0) ? 'Publicado' : 'Por Publicar';
            }
            if ($Portal == 15) {
                $PublicaOlx = ($Portal == 15) ? 'Publicado' : 'Por Publicar';
            }
            if ($Portal == 17) {
                $PublicaTM = ($Portal == 17) ? 'Publicado' : 'Por Publicar';
            }

            if ($Portal == 18) {
                $Publicalocanto = ($Portal == 18) ? 'Publicado' : 'Por Publicar';
            }
            if ($Portal == 19) {
                $PublicaPautaR = ($Portal == 19) ? 'Publicado' : 'Por Publicar';
            }
            if ($Portal == 20) {
                $PublicaAbad = ($Portal == 20) ? $codigo : 'Por Publicar';
            }
            if ($Portal == 23) {
                $RAfydi = ($Portal == 23) ? $codigo : 0;
            }
            if ($Portal == 25) {
                $RNcasa = ($Portal == 25) ? $codigo : 0;
            }
            if ($Portal == 28) {
                $RZhabitat = ($Portal == 28) ? $codigo : 0;
            }

            Inmuebles::updateInmueblesPortalesNew($codinm, $IdInmobiliaria, $Portal, $codigo, $sincroniza, $infoProm, $suc, $errorSinc, $urlpublica_dtp);

        }
        Inmuebles::updateInmueblesPortaleslOld($codinm, $IdInmobiliaria, $RMtoCuadrado, $RLaGuia, $RMeli, $RZonaProp, $RMiCasa, $RAfydi, $RNcasa, $RVivaReal, $RIdnd, $PublicaGuia, $PublicaMeli, $PublicaZonaProp, $PublicaVReal, $PublicaIdn);
        return 1;

        $stmt = null;
    }
    public function updateDescripcionOld($IdInmobiliaria, $codinm, $descripcion, $descripcionM2)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("UPDATE inmuebles
            SET
            descripcionlarga   = :descripcion,
            descripcionMetro   = :descripcionMetro
            WHERE codinm       = :codinm
            AND IdInmobiliaria = :IdInmobiliaria");

        if ($stmt->execute(array(
            ":descripcion"      => $descripcion,
            ":descripcionMetro" => $descripcionM2,
            ":codinm"           => $codinm,
            ":IdInmobiliaria"   => $IdInmobiliaria,
        ))) {

            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function updateDescripcionNew($IdInmobiliaria, $codinm, $descripcion, $descripcionM2)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("UPDATE inmnvo
            SET
            descripcionlarga   = :descripcion,
            descripcionMetro   = :descripcionMetro
            WHERE codinm       = :codinm
            AND IdInmobiliaria = :IdInmobiliaria");

        if ($stmt->execute(array(
            ":descripcion"      => $descripcion,
            ":descripcionMetro" => $descripcionM2,
            ":codinm"           => $codinm,
            ":IdInmobiliaria"   => $IdInmobiliaria,
        ))) {

            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function OrdenFotosNew2($data)
    {
        $connPDO = new Conexion();

        Inmuebles::updateOrdenFotosNew2($data, $data['finelement'], 99);
        Inmuebles::updateOrdenFotosNew2($data, $data['inicioelement'], $data['finelement']);
        Inmuebles::updateOrdenFotosNew2($data, 99, $data['inicioelement']);
        Inmuebles::checkOrdenFotosOld($data, $archorig);
    }
    public function updateOrdenFotosNew2($data, $actual, $nvo)
    {
        $connPDO = new Conexion();
        $idInm   = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $connPDO->prepare("UPDATE fotos_nvo
            SET
            posi_ft          =:nvoposi_ft
            where codinm_fto =:codinm
            and aa_fto       =:IdInmobiliaria
            and posi_ft      =:posi_ft");

        if ($stmt->execute(array(
            ":posi_ft"        => $actual,
            ":nvoposi_ft"     => $nvo,
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":codinm"         => $data['codinm'],
        ))) {
            return 1;
        } else {
            $response[] = array("Error" => $stmt->errorInfo());
        }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function checkOrdenFotosOld($data)
    {
        $connPDO = new Conexion();
        $idInm   = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $count   = 0;

        $stmt = $connPDO->prepare("SELECT posi_ft,foto
            FROM fotos_nvo
            where codinm_fto =:codinm
            and aa_fto       =:IdInmobiliaria

            order by posi_ft");

        if ($stmt->execute(array(
            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":codinm"         => $data['codinm'],
        ))) {
            //borramos las fotos actuales de la tabla, haciendo un replace en la  tabla
            Inmuebles::restoreFotosOld($data);
            while ($row = $stmt->fetch()) {
                $count++;
                $foto  = $row['foto'];
                $campo = "Foto" . $count;
                Inmuebles::updateOrdenFotosOld($data, $campo, $foto);
            }
            return 1;
        } else {
            $response[] = array("Error" => $stmt->errorInfo());
        }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }
    public function updateOrdenFotosOld($data, $campo, $foto)
    {
        $connPDO = new Conexion();

        $idInm   = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $archnvo = $foto;

        $stmt = $connPDO->prepare("UPDATE fotos
                SET
                $campo      =:archnvo
                where idInm =:idInm");

        if ($stmt->execute(array(
            ":idInm"   => $idInm,
            ":archnvo" => $archnvo,
        ))) {

            return 1;
        } else {
            $response[] = array("Error" => $stmt->errorInfo());
        }

        if ($response) {
            return $response;
        }
        $stmt = null;
    }

    public function updateTpInmOld($IdInmobiliaria, $codinm, $tpInm)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("UPDATE inmuebles
            SET
            IdTpInm              =:tipoinmueble
            WHERE codinm         =:codinm
            AND IdInmobiliaria   =:IdInmobiliaria");

        if ($stmt->execute(array(
            ":tipoinmueble"   => $tpInm,
            ":codinm"         => $codinm,
            ":IdInmobiliaria" => $IdInmobiliaria,
        ))) {
            Inmuebles::updateTpInmNew($IdInmobiliaria, $codinm, $tpInm);
            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function updateTpInmNew($IdInmobiliaria, $codinm, $tpInm)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("UPDATE inmnvo
            SET
            IdTpInm              =:tipoinmueble
            WHERE codinm         =:codinm
            AND IdInmobiliaria   =:IdInmobiliaria");

        if ($stmt->execute(array(
            ":tipoinmueble"   => $tpInm,
            ":codinm"         => $codinm,
            ":IdInmobiliaria" => $IdInmobiliaria,
        ))) {

            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function inmueblesConsignadosNvo($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT IdInmobiliaria,IdGestion,IdTpInm,IdBarrio,ValorCanon,ValorVenta,Administracion,idInm,codinm
                            from inmnvo i
                            where fingreso>=:fingreso
                            AND idEstadoinmueble=2
                            and IdGestion>0
                            and IdTpInm>0
                            and IdDestinacion>0
                            and (ValorCanon+ValorVenta)>0
                            limit 0," . $data['lim']);
        //$stmt->bindParam(':lim', );
        $stmt->bindParam(':fingreso', $data['fingreso']);

        if ($stmt->execute()) {

            $data = array();
            $connPDO->exec('chartset="utf-8"');
            while ($row = $stmt->fetch()) {
                $IdCiudad = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], 'IdCiudad');
                $Ciudad   = getCampo('ciudad', "where IdCiudad=" . $IdCiudad, 'Nombre');

                $data[] = array(
                    "Inmobiliaria"   => utf8_encode(ucwords(strtolower(getCampo('clientessimi', "where IdInmobiliaria=" . $row['IdInmobiliaria'], "Nombre")))),
                    "ValorVenta"     => number_format($row['ValorVenta']),
                    "ValorCanon"     => number_format($row['ValorCanon']),
                    "IdGestion"      => $row['IdGestion'],
                    "Gestion"        => ucwords(strtolower(getCampo('gestioncomer', "where IdGestion=" . $row['IdGestion'], "NombresGestion"))),
                    "TipoInm"        => ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble=" . $row['IdTpInm'], "Descripcion"))),
                    "Barrio"         => ucwords(strtolower(getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], "NombreB"))),
                    "ciudad"         => ucwords(strtolower($Ciudad)),
                    "Administracion" => $row['Administracion'],
                    "fingreso"       => $row['fingreso'],
                    "link" => "http://www.simiinmobiliarias.com/mwc/callcenter/inmuebleCruce.php?idInmu=".$row['IdInmobiliaria']."-".$row['codinm']
                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function getSedes($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT id_sed,nom_sed,est_sed,inmo_sed
                                FROM sedes_inmo
                                 WHERE inmo_sed=:IdInmobiliaria
                                AND est_sed=1");

        if ($stmt->execute(
            array(":IdInmobiliaria" => $data['IdInmobiliaria'])
        )) {
            $info = array();
            while ($row = $stmt->fetch()) {
                $info[] = array
                    (
                    "id_sed"  => utf8_encode($row['id_sed']),
                    "nom_sed" => utf8_encode($row['nom_sed']),
                );
            }
            return $info;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function getDataDetalleInmuebleMeli($data)
    {
        $connPDO = new Conexion();

        $idInm = $data['IdInmobiliaria'] . "-" . $data['codinm'];

        $stmt = $connPDO->prepare("SELECT  d.idcaracteristica,d.cantidad_dt,m.`Descripcion`,m.`NameCarMeli`,m.`CodCarMeli`
             FROM detalleinmueble d,maestrodecaracteristicasnvo m
             WHERE d.inmob   = :IdInmobiliaria
             AND d.codinmdet =:codinm
             AND d.idCaracteristica=m.`idCaracteristica`
             and m.`NameCarMeli` IS NOT NULL
             and m.`CodCarMeli` IS NOT NULL");

        if ($stmt->execute(array(

            ":IdInmobiliaria" => $data['IdInmobiliaria'],
            ":codinm"         => $data['codinm'],
        ))) {
            $info = array();
            while ($row = $stmt->fetch()) {

                $info[] = array(
                    "idcaracteristica" => $row['idcaracteristica'],
                    "Descripcion"      => utf8_encode($data['Descripcion']),
                    "cantidad_dt"      => $row['cantidad_dt'],
                    "NameCarMeli"      => utf8_encode($row['NameCarMeli']),
                    "CodCarMeli"       => utf8_encode($row['CodCarMeli']),
                );
            }

            return $info;
        } else {
            print_r($stmt->errorInfo());
        }

    }

    public function getCaracteristicasTipoInmueble($data)
    {

        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT  idCaracteristica,Descripcion,NameCarMeli,CodCarMeli
             FROM maestrodecaracteristicasnvo
             WHERE IdTipoInmueble = :IdTipoInmueble order by Descripcion");

        if ($stmt->execute(array(
            ":IdTipoInmueble" => $data['IdTipoInmueble'],
        ))) {
            $info = array();

            while ($row = $stmt->fetch()) {

                $info[] = array(
                    "idcaracteristica" => $row['idCaracteristica'],
                    "Descripcion"      => utf8_encode($row['Descripcion']),
                    "NameCarMeli"      => utf8_encode($row['NameCarMeli']),
                    "CodCarMeli"       => utf8_encode($row['CodCarMeli']),
                );
            }

            return $info;
        } else {
            print_r($stmt->errorInfo());
        }

    }
    public function updateMeli($codinm, $IdInmobiliaria, $Portal, $codigo, $estado2, $link)
    {
        $connPDO = new Conexion();

        $idInm = $IdInmobiliaria . "-" . $codinm;
        $f_sis = date('Y-m-d');
        $h_sis = date('H-i-s');
        $ip    = $_SERVER['REMOTE_ADDR'];

        $stmt = $connPDO->prepare("UPDATE detportalesinmueble
            SET cod_dtp     =:cod_dtp,
            urlpublica_dtp  =:link
            WHERE idinm_dtp =:idinm_dtp
            AND inmob_dtp   =:inmob_dtp
            AND portal_dtp  =:portal_dtp");

        $stmt->bindParam(":idinm_dtp", $codinm);
        $stmt->bindParam(":inmob_dtp", $IdInmobiliaria);
        $stmt->bindParam(":portal_dtp", $Portal);
        $stmt->bindParam(":cod_dtp", $codigo);
        $stmt->bindParam(":link", $link);

        if ($stmt->execute()) {
            Inmuebles::updateMeliOld($codinm, $IdInmobiliaria, $Portal, $codigo, $estado2, $link);
            echo $codinm." - ".$IdInmobiliaria." - ".$Portal." - ".$codigo." - ".$estado2." - ".$link;
            return 1;
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function updateMeliOld($codinm, $IdInmobiliaria, $Portal, $codigo, $estado2, $link)
    {
        $connPDO = new Conexion();

        $idInm = $IdInmobiliaria . "-" . $codinm;
        $f_sis = date('Y-m-d');
        $h_sis = date('H-i-s');
        $ip    = $_SERVER['REMOTE_ADDR'];

        $stmt = $connPDO->prepare("UPDATE inmuebles
            SET RMeli          =:cod_dtp,
            PublicaMeli        =:estado2,
            dirMeli            =:link
            WHERE codinm       =:idinm_dtp
            AND IdInmobiliaria =:inmob_dtp");

        $stmt->bindParam(":idinm_dtp", $codinm);
        $stmt->bindParam(":inmob_dtp", $IdInmobiliaria);
        $stmt->bindParam(":cod_dtp", $codigo);
        $stmt->bindParam(":estado2", $estado2);
        $stmt->bindParam(":link", $link);

        if ($stmt->execute()) {
            return 1;
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function getGestionContactos($data)
    {
        $connPDO = new Conexion();

        $sql = "SELECT usuariom2,clavem2,prefijom2,servidor,letra FROM usuariosm2 WHERE inmogm2 = :inmo  and servidor=1 LIMIT 0,1";

        $stmt = $connPDO->prepare($sql);

        $stmt->bindParam(':inmo', $_SESSION["IdInmmo"]);

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $paramsuser = array(
                    'usuario'  => $row['usuariom2'],
                    'clave'    => $row['clavem2'],
                    'letra'    => $row['letra'],
                    'prefijo'  => $row['prefijom2'],
                    'servidor' => $row['servidor'],
                );
            }
            $url      = "http://www.metrocuadrado.com/axis/GeocodeWS.jws?method=obtenerContactos&op1=" . $paramsuser['usuario'] . "&op2=" . $paramsuser['clave'] . "&op3=" . $data['inmom2'];
            $response = file_get_contents($url);
            $doc      = new DOMDocument();
            $doc->loadXML($response);
            $explode = $doc->getElementsByTagName('obtenerContactosResponse')->item(0)->nodeValue;
            if (strpos($explode, ';') === false) {
                $data = array(
                    'status' => 'Error',
                    'msg'    => 'No existen Contactos para este Inmueble',
                );
            } else {
                $registros = explode(";", $explode);
                unset($registros[0]);
                $registers = array();
                foreach ($registros as $key => $value) {
                    if (empty($value)) {
                        unset($registros[$key]);
                    } else {
                        $params      = explode(",", $value);
                        $registers[] = array(
                            'FechaRegistro' => $params[1],
                            'Canal'         => $params[0],
                            'Nombre'        => $params[2],
                            'Telefono'      => $params[3],
                            'Email'         => $params[4],
                            'Ciudad'        => $params[5],
                            'Comentario'    => $params[6],
                        );
                    }
                }
                $data = array(
                    'status'    => 'Ok',
                    'registros' => $registers,
                    'datos'     => $explode,
                    'url'       => $url,
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'msg'    => print_r($stmt->errorInfo()),
            );
        }
        return $data;
    }

    public function listadoInmueblesFiltroIframe($codInmo, $lim = 0, $tip_ope, $depto = 0, $area, $area1, $Precio, $Precio1, $zona, $tip_inm, $ciudad = 0,$offset=0,$localidad=0,$barrio=0)
    {
        //echo $codInmu."ff";
        //echo "$tip_ope, $depto,$area,$Precio ------<br>";
        
        
//echo $codInmo;
        $codInmo = explode(";",$codInmo);

        $in="";
        foreach ($codInmo as $key => $value) {
            $in .= strip_tags($value).",";
        }

        $in=trim($in,",");

        $condInm="where IdInmobiliaria IN ( ".$in." )";
            

        $connPDO = new Conexion();
        $cond1   = "";
        $localidad = "";
        if ($tip_ope > 0 && $tip_ope != 2) {
            $cond1 .= " and IdGestion=:tip_ope ";
        }
        if ($tip_inm > 0) {
            $cond1 .= " and IdTpInm=:tip_inm ";
        }
        if ($depto > 0) {
            $cond1 .= " and d.IdDepartamento=:depto ";
        }
        if ($ciudad > 0) {
            $cond1 .= " and c.IdCiudad=:ciud ";
        }
        if ($zona > 0) {
            $cond1 .= " and c.idzona=:zona ";
        }
        if ($localidad > 0) {
            $cond1 .= " and l.idlocalidad=:localidad ";
            $localidad = " ,localidad l";
        }
        if ($barrio > 0) {
            $cond1 .= " and IdBarrios=:barrio ";
        }
        if ($area > 0 && $area1 > 0) {
            $cond1 .= " and AreaConstruida between :area and :area1 ";
        }
        if ($area > 0 && $area1 <= 0) {
            $cond1 .= " and AreaConstruida >= :area";
        }
        if ($area <= 0 && $area1 > 0) {
            $cond1 .= " and AreaConstruida <= :area ";
        }
        if ($tip_ope == 5) {
            $campov = "Venta";
        } else {
            $campov = "Canon";
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope != 2) {

            $cond1 .= " and $campov between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope != 2) {
            $cond1 .= " and $campov >= :Precio ";
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope != 2) {
            $cond1 .= " and $campov <= :Precio1 ";
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon between :Precio and :Precio1 or Venta between :Precio and :Precio1) ";
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon >= :Precio or Venta >= :Precio ) ";
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon <= :Precio1 or Venta <= :Precio1) ";
        }

        $sql =" select
        IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
        Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
        Barrio,Gestion,AreaConstruida,AreaLote,latitud,fingreso,
        longitud,EdadInmueble,NombreInmo,logo,IdBarrios
        from datos_call c,departamento d,ciudad cu ".$localidad."
        ".$condInm."
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad
        AND c.idestado=2
        $cond1
        order by fingreso DESC limit $offset,$lim ";
        /*$sql="SELECT 
  c.IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
  Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
  Barrio,Gestion,AreaConstruida,AreaLote,latitud,
  longitud,EdadInmueble,NombreInmo,logo,IdBarrios,fingreso
  FROM datos_call c,departamento d,ciudad cu, item_grupos ig,localidad l
        WHERE c.IdInmobiliaria=ig.`IdInmobiliaria`
        #AND ig.`IdGrupoInmo`=11
        AND c.IdInmobiliaria IN($in)
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad 
        ".$cond1."
        ORDER BY fingreso DESC
        LIMIT $offset,$lim";*/
        //echo $cond1."ffffffff";
        $stmt = $connPDO->prepare( $sql );

        //echo $sql;
        /*$stmt = $connPDO->prepare(" SELECT 
  IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
  Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
  Barrio,Gestion,AreaConstruida,AreaLote,latitud,
  longitud,EdadInmueble,NombreInmo,logo,IdBarrios,fingreso
  from datos_call c,departamento d,ciudad cu
        ".$condInm."
        and c.idestado=2
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad 
        $cond1 
        order by fingreso $orden
        limit $lim,$total ");*/

        
        

        //$stmt->bindParam(':inmob', $in,PDO:l:l);
        
       
        //$stmt->bindParam(':lim',$lim);

        if ($tip_ope > 0 && $tip_ope != 2) {
            $stmt->bindParam(':tip_ope', $tip_ope);
        }
        if ($tip_inm > 0) {
            $stmt->bindParam(':tip_inm', $tip_inm);
        }
        //echo "<br> ***** ".$depto."*******<br>";
        if ($depto > 0) {
            $stmt->bindParam(':depto', $depto);
        }
        if ($ciudad > 0) {
            //echo $ciudad."ffffff--- <br>";
            $stmt->bindParam(':ciud', $ciudad);
        }
        if ($zona > 0) {
            $stmt->bindParam(':zona', $zona);
        }
        if ($localidad > 0) {
            $stmt->bindParam(':localidad', $localidad);
        }
        if ($barrio > 0) {
            $stmt->bindParam(':barrio', $barrio);
        }
        if ($area > 0 && $area1 > 0) {
            $stmt->bindParam(':area', $area);
            $stmt->bindParam(':area1', $area1);
        }
        if ($area > 0 && $area1 <= 0) {
            $stmt->bindParam(':area', $area);
        }
        if ($area <= 0 && $area1 > 0) {
            $stmt->bindParam(':area1', $area1);
        }

        $vlrCero = 0;
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio', $Precio);
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio', $Precio);
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope != 2) {
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 > 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio', $Precio);
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($Precio > 0 && $Precio1 <= 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio', $Precio);
        }
        if ($Precio <= 0 && $Precio1 > 0 && $tip_ope == 2) {
            $stmt->bindParam(':Precio1', $Precio1);
        }
        if ($stmt->execute()) {
            $existen = $stmt->rowCount();
            //echo "existen $existen <br>";
            while ($row = $stmt->fetch()) {

                /*$stmt1=$this->db->prepare("elect count(*) as total
                FROM datos_call
                where IdInmobiliaria=?");
                $stmt1->bind_param("i",$codInmo);
                $consulta1->execute();
                $consulta1->bind_result($total);*/

                if ($row['IdGestion'] == 1) {
                    $oper   = 'rent';
                    $precio = $row['Canon'];
                } else {
                    $oper   = 'sale';
                    $precio = $row['Venta'];
                }

                $foto1=str_replace('../', '', getCampo('fotos', "where idInm='" . $row["Codigo_Inmueble"] . "'", 'Foto1'));
                if(!empty($foto1)){
                    $foto1="https://www.simiinmobiliarias.com/mcomercialweb/".$foto1;
                }else{
                    $foto1="https://www.simiinmobiliarias.com/images/no_image_inm.png";
                }

                $IdBarrios = $row['IdBarrios'];
                $arreglo[] = array(
                    "IdInmobiliaria"   => $row['IdInmobiliaria'],
                    "Administracion"   => number_format($row['Administracion']), //1
                    "IdGestion"        => $row['IdGestion'], //2
                    "Estrato"          => $$row['Estrato'], //3
                    "IdTpInm"          => $row['IdTpInm'], //4
                    "Codigo_Inmueble"  => $row['Codigo_Inmueble'],
                    "codinm"  => $row['Codigo_Inmueble'],
                    "Tipo_Inmueble"    => ucwords(strtolower($row['Tipo_Inmueble'])),
                    "Venta"            => number_format($row['Venta']),
                    "Canon"            => number_format($row['Canon']),
                    "precio"           => number_format($precio),
                    "descripcionlarga" => $row['descripcionlarga'],
                    "Barrio"           => ucwords(strtolower($row['Barrio'])),
                    "Gestion"          => ucwords(strtolower($row['Gestion'])),
                    "AreaConstruida"   => number_format($row['AreaConstruida']),
                    "AreaLote"         => number_format($row['AreaLote']),
                    "latitud"          => $row['latitud'],
                    "longitud"         => $row['longitud'],
                    "EdadInmueble"     => $row['EdadInmueble'],
                    "NombreInmo"       => ucwords(strtolower($row['NombreInmo'])),
                    "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                    "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                    "oper"             => $oper,
                    "foto1"            => $foto1,
                    "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $row['logo']),
                );

            }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            if ($existen > 0) {
                return $arreglo;
            } else {
                return 0;
            }

        } else {
            //echo  "error ejecucion--".$stmt->errorInfo();
            print_r($stmt->errorInfo());
        }
        $stmt = null;

    }


    public function getInmuebleCodigo($codInmo,$codInmu)
    {

         $codInmo = explode(";",$codInmo);

        $in="";
        foreach ($codInmo as $key => $value) {
            $in .= "'".strip_tags($value.'-'.$codInmu)."',";
        }

        $in=trim($in,",");
        //echo $in;

        $condInm=" WHERE IdInm IN ( ".$in." )";


        $connPDO = new Conexion();
        $stmt = $connPDO->prepare(" select IdGestion,ValorCanon,ValorVenta,IdBarrio,IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,descripcionlarga,AreaConstruida,AreaLote,latitud,codinm
            FROM inmuebles ".$condInm);



       // $stmt->bindParam(':codInmo', $codInmo,  PDO::PARAM_STR);
        //$stmt->bindParam(':lim',$lim);

       

        if ($stmt->execute()) {

            while ($row = $stmt->fetch()) {
                
                if ($row['IdGestion'] == 1) {
                    $oper   = 'rent';
                    $precio = $row['ValorCanon'];
                } else {
                    $oper   = 'sale';
                    $precio = $row['ValorVenta'];
                }
                $IdBarrios = $row['IdBarrio'];
                $foto1=str_replace('../', '', getCampo('fotos', "where idInm='" . $row["codinm"] . "'", 'Foto1'));
                if(!empty($foto1)){
                    $foto1="https://www.simiinmobiliarias.com/mcomercialweb/".$foto1;
                }else{
                    $foto1="https://www.simiinmobiliarias.com/images/no_image_inm.png";
                }

                $arreglo = array(
                    "IdInmobiliaria"   => $row['IdInmobiliaria'],
                    "codinm" => $row["codinm"],
                    "Administracion"   => number_format($row['Administracion']), //1
                    "IdGestion"        => $row['IdGestion'], //2
                    "Estrato"          => $$row['Estrato'], //3
                    "IdTpInm"          => $row['IdTpInm'], //4
                    "Venta"            => number_format($row['ValorVenta']),
                    "Canon"            => number_format($row['ValorCanon']),
                    "precio"           => number_format($precio),
                    "descripcionlarga" => $row['descripcionlarga'],
                    "Barrio"           => ucwords(strtolower($row['Barrio'])),
                    "Gestion"          => ucwords(strtolower($row['Gestion'])),
                    "AreaConstruida"   => number_format($row['AreaConstruida']),
                    "AreaLote"         => number_format($row['AreaLote']),
                    "NombreInmo"       => ucwords(strtolower($row['NombreInmo'])),
                    "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                    "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                    "oper"             => $oper,
                    "foto1"            => "https://www.simiinmobiliarias.com/mcomercialweb/" . str_replace('../', '', getCampo('fotos', "where idInm='" . $row["IdInmobiliaria"]."-".$row["codinm"] . "'", 'Foto1')),
                );

            }

            return $arreglo;

        }else{
           return print_r($stmt->errorInfo()); 
        }

    }

    public function totalInmueblesFiltroIframe($codInmo, $lim = 0, $tip_ope, $depto = 0, $area, $Precio, $tip_inm, $ciudad = 0,$localidad=0,$barrio=0)
    {
        //echo $codInmu."ff";
        //echo "$tip_ope, $depto,$area,$Precio ------<br>";

        $connPDO = new Conexion();
        $cond1   = "";
        if ($tip_ope > 0 && $tip_ope != 2) {
            $cond1 .= " and IdGestion=:tip_ope ";
        }
        if ($tip_inm > 0) {
            $cond1 .= " and IdTpInm=:tip_inm ";
        }
        if ($depto > 0) {
            $cond1 .= " and d.IdDepartamento=:depto ";
        }
        if ($ciudad > 0) {
            $cond1 .= " and c.IdCiudad=:ciud ";
        }
        if ($zona > 0) {
            $cond1 .= " and l.idzona=:zona ";
        }
        if ($localidad > 0) {
            $cond1 .= " and l.idlocalidad=:localidad ";
        }
        if ($barrio > 0) {
            $cond1 .= " and IdBarrios=:barrio ";
        }
        if ($area > 0) {
            $cond1 .= " and AreaConstruida between :area and :area1 ";
        }
        if ($Precio > 0 && $tip_ope == 5) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 1) {
            $cond1 .= " and Canon between :Precio and :Precio1 ";
        }
        if ($Precio > 0 && $tip_ope == 2) {
            $cond1 .= " and (and Canon between :Precio and :Precio1 or Venta between :Precio and :Precio1) ";
        }
        //echo $cond1."ffffffff";
        $stmt = $connPDO->prepare(" select count(*) as tot
            FROM datos_call c,departamento d,ciudad cu
        WHERE IdInmobiliaria=:codInmo
        AND cu.IdDepartamento=d.IdDepartamento
        AND c.IdCiudad=cu.IdCiudad
        $cond1 ");

        $stmt->bindParam(':codInmo', $codInmo);
        //$stmt->bindParam(':lim',$lim);

        if ($tip_ope > 0 && $tip_ope != 2) {
            $stmt->bindParam(':tip_ope', $tip_ope);
        }
        if ($tip_inm > 0) {
            $stmt->bindParam(':tip_inm', $tip_inm);
        }
        //echo "<br> ***** ".$depto."*******<br>";
        if ($depto > 0) {
            $stmt->bindParam(':depto', $depto);
        }
        if ($ciudad > 0) {
            //echo $ciudad."ffffff--- <br>";
            $stmt->bindParam(':ciud', $ciudad);
        }
        if ($zona > 0) {
            $stmt->bindParam(':zona', $zona);
        }
        if ($localidad > 0) {
            $stmt->bindParam(':localidad', $localidad);
        }
        if ($barrio > 0) {
            $stmt->bindParam(':barrio', $barrio);
        }
        if ($area > 0) {
            list($are, $are1) = explode("|", $this->trae_area($area));
            //echo "$are -$are1";
            $stmt->bindParam(':area', $are);
            $stmt->bindParam(':area1', $are1);
        }
        if ($Precio > 0 && $tip_ope == 1) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            //echo "<br> $Prec,$Prec1 <br>";
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        if ($Precio > 0 && $tip_ope == 5) {
            list($Prec, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }
        //$stmt->bindParam(':Precio',$Precio);
        if ($Precio > 0 && $tip_ope == 2) {
            list($Precio, $Prec1) = explode("|", $this->trae_precio($Precio));
            $stmt->bindParam(':Precio', $Prec);
            $stmt->bindParam(':Precio1', $Prec1);
        }

        if ($stmt->execute()) {
            while ($row = $stmt->fetch()) {
                $tot = $row['tot'];
                //echo $tot."ffff";
            }
            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $tot;
        } else {
            //echo  "error ejecucion--".$stmt->errorInfo();
            //print_r($stmt->errorInfo());
        }
        $stmt = null;

    }

    public function enviarCorreoIframeNuevo($data){
        $mailSender= New mailSender($this->db,$this->pdo);
        $primaryc= ($data["primaryc"])?"#".urldecode($data["primaryc"]):'#ED3237';
        $inmueble = $this->infoInmueble($data['IdInmmo']."-".$data["inm"]);
        $inmobiliaria = $this->infoInmobiliaria($data['IdInmmo']);
        $telas=(!empty($inmueble[0]["TelAsesor"]))?'<p><b>Telefono:</b> '.$inmueble[0]["TelAsesor"].'</p>':'';
        $celas = (!empty($inmueble[0]["CelAsesor"]))?'<p><b>Celular: </b> '.$inmueble[0]["CelAsesor"].'</p>':'';
       // print_r($inmueble);
        $body='<html>
   <head><meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
   <title>Respuesta Ticket</title> 
   <style>
   </style>
   </head>
   <body style="margin: 0px; padding: 0px;">
   <table style="margin:0px auto;" border="0" cellpadding="0" cellspacing="0" width="100%" > 
   <tbody><tr>
   <td style="padding: 10px 0 30px 0;">
   <table align="center" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #cccccc; border-collapse: collapse;width: 100%">
   <tbody><tr>
   <td align="center" bgcolor="'.$primaryc.'" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
   <img src="'.$inmueble[0]["logo"].'" alt="Aldea Real State" width="172" height="136" style="display: block;">
   <p style="color: #FFFFFF; text-align: center">'.$inmueble[0]["NombreInmo"].'</p>
   </td>
   </tr>
   <tr>
   <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
   <table border="0" cellpadding="0" cellspacing="0" width="100%">
   <tbody><tr>
   <td style="color: '.$primaryc.'; font-family: Arial, sans-serif; font-size: 20px;">
   <b>Solicitud de contacto</b>
   </td>
   </tr>
   <tr>
    <td align="center">
    <div style="background-color:#009CFF; width:100px; font-size:10px;padding:10px 5px 10px 5px; margin:10px 0 0 0; display:inline-block; vertical-align:top;">
                <a style="color:white;text-align: center" target="_blank" href="https://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/fichatec3.php?reg='.$data['dt'].'"><div class="btn btn-info">Ver Ficha</div></a>
             </div>
    </td>
   </tr>
   <tr>
   <td style="color: '.$primaryc.'; font-family: Arial, sans-serif; font-size: 20px;">
   <b>Datos Asesor</b>
   </td>
   </tr>
   <tr>
   <td style="padding: 20px 0 30px 0; color: #153643; font-family:Arial, sans-serif; font-size: 16px; line-height:20px;">
   <p><b>Nombres:</b> '.ucwords(strtolower($inmueble[0]["NombreProm"])).'</p>
   <p><b>Correo:</b> '.$inmueble[0]["CorreoAsesor"].'</p>
   '.$telas.'
   '.$celas.'
   </td>
   </tr>

   <tr>
   <td style="color: '.$primaryc.'; font-family: Arial, sans-serif; font-size: 20px;">
   <b>Datos Inmobiliaria</b>
   </td>
   </tr>
   <tr>
   <td style="padding: 20px 0 30px 0; color: #153643; font-family:Arial, sans-serif; font-size: 16px; line-height:20px;">
   <p><b>Nombre:</b> '.ucwords(strtolower($inmueble[0]["NombreInmo"])).'</p>
   <p><b>Correo:</b> '.$inmobiliaria[0]["Correo"].'</p>
   <p><b>Telefonos:</b> '.$inmobiliaria[0]["Telefonos"].'</p>
   <p><b>Direccion:</b> '.$inmobiliaria[0]["Direccion"].'</p>
   </td>
   </tr>

   </tbody></table>
   </td>
   </tr>
   <tr>
   <td bgcolor="#D4D4D4" style="padding: 30px 30px 30px 30px;">
   <table border="0" cellpadding="0" cellspacing="0" width="100%">
   <tbody><tr>
  
   <td align="right" width="25%">
   <table border="0" cellpadding="0" cellspacing="0">
   <tbody><tr>
   <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">

   </td>
   <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
   <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">

   </td>
   </tr>
   </tbody></table>
   </td>
   </tr>
   </tbody></table>
   </td>
   </tr>
   </tbody></table>
   </td>
   </tr>
   </tbody></table>




   </body></html>';

       $mailSender->envioMailIframeNuevo($data['IdInmmo'],$body, $data["Email"],'',"Mensaje Contacto Inmueble ".$data['dt']);

    $body='<html>
   <head><meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
   <title>Respuesta Ticket</title> 
   <style>
   </style>
   </head>
   <body style="margin: 0px; padding: 0px;">
   <table style="margin:0px auto;" border="0" cellpadding="0" cellspacing="0" width="100%" > 
   <tbody><tr>
   <td style="padding: 10px 0 30px 0;">
   <table align="center" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #cccccc; border-collapse: collapse;width: 100%">
   <tbody><tr>
   <td align="center" bgcolor="'.$primaryc.'" style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
   <img src="'.$inmueble[0]["logo"].'" alt="Aldea Real State" width="172" height="136" style="display: block;">
   <p style="color: #FFFFFF; text-align: center">'.$inmueble[0]["NombreInmo"].'</p>
   </td>
   </tr>
   <tr>
   <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
   <table border="0" cellpadding="0" cellspacing="0" width="100%">
   <tbody><tr>
   <td style="color: '.$primaryc.'; font-family: Arial, sans-serif; font-size: 20px;">
   <b>Solicitud de contacto</b>
   </td>
   </tr>
   <tr>
    <td align="center">
    <div style="background-color:#009CFF; width:100px; font-size:10px;padding:10px 5px 10px 5px; margin:10px 0 0 0; display:inline-block; vertical-align:top;">
                <a style="color:white;" target="_blank" href="https://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/fichatec3.php?reg='.$data['dt'].'"><div class="btn btn-info">Ver Ficha</div></a>
             </div>
    </td>
   </tr>
   <tr>
   <td style="color: '.$primaryc.'; font-family: Arial, sans-serif; font-size: 20px;">
   <b>Datos Cliente</b>
   </td>
   </tr>
   <tr>
   <td style="padding: 20px 0 30px 0; color: #153643; font-family:Arial, sans-serif; font-size: 16px; line-height:20px;">
   <p><b>Nombres:</b> '.ucwords(strtolower($data["nombres"]))." ".ucwords(strtolower($data["apellidos"])).'</p>
   <p><b>Correo Cliente:</b> '.$data["Email"].'</p>
   <p><b>Telefono Cliente:</b> '.$data["telefono"].'</p>
   <p><b>Celular Cliente:</b> '.$data["celular"].'</p>
   <p><b>Ciudad Cliente:</b> '.$data["ciudad"].'</p>
   <p><b>Asunto:</b> Solicitud de contacto</p>
   <p><b>Comentario:</b> '.ucfirst(strtolower($data["comentario"])).'</p>
   </td>
   </tr>

   </tbody></table>
   </td>
   </tr>
   <tr>
   <td bgcolor="#D4D4D4" style="padding: 30px 30px 30px 30px;">
   <table border="0" cellpadding="0" cellspacing="0" width="100%">
   <tbody><tr>
   
   <td align="right" width="25%">
   <table border="0" cellpadding="0" cellspacing="0">
   <tbody><tr>
   <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">

   </td>
   <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
   <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">

   </td>
   </tr>
   </tbody></table>
   </td>
   </tr>
   </tbody></table>
   </td>
   </tr>
   </tbody></table>
   </td>
   </tr>
   </tbody></table>




   </body></html>';
   //envio al asesor $inmueble[0]["CorreoAsesor"]
   $mailSender->envioMailIframeNuevo($data['IdInmmo'],$body, $inmueble[0]["CorreoAsesor"],'',"Mensaje Contacto Inmueble ".$data['dt']);
   //envio a la inmobiliaria $inmobiliaria[0]["Correo"]
   $mailSender->envioMailIframeNuevo($data['IdInmmo'],$body, $inmobiliaria[0]["CorreoAsesor"],'',"Mensaje Contacto Inmueble ".$data['dt']);
   //si pasan emails por url enviamos dichos correos
   
        if(isset($data["emails"]) and !empty($data["emails"])){
            $emails = explode(";",urldecode($data["emails"]));
            foreach ($emails as $key => $value) {
                $mailSender->envioMailIframeNuevo($data['IdInmmo'],$body, $value,'',"Mensaje Contacto Inmueble ".$data['dt']);

            }
        }

        echo "1";
    }

    public function listadoInmueblesDestacadosIframe($codInmo, $lim)
    {
        //echo $codInmu."ff";
        
        $codInmo = explode(";",$codInmo);

        $in="";
        foreach ($codInmo as $key => $value) {
            $in .= strip_tags($value).",";
        }

        $in=trim($in,",");
        

        $condInm=" and IdInmobiliaria IN ( ".$in." )";
        

        if (($consulta = $this->db->prepare(" select
        IdInmobiliaria,Administracion,IdGestion,Estrato,IdTpInm,
        Codigo_Inmueble,Tipo_Inmueble,Venta,Canon,descripcionlarga,
        Barrio,Gestion,AreaConstruida,AreaLote,latitud,
        longitud,EdadInmueble,NombreInmo,logo,IdBarrios
        from datos_call,settings_portales
        where Codigo_Inmueble=inmu_p
        and IdInmobiliaria=inmob_p
        and  tipo_p=4
        and vlr_p=1
        ".$condInm."
        limit 0,?
        "))) {
            $consulta->bind_param("i", $lim);
            
            $consulta->execute();

            if ($consulta->bind_result($IdInmobiliaria, $Administracion, $IdGestion, $Estrato, $IdTpInm,
                $Codigo_Inmueble, $Tipo_Inmueble, $Venta, $Canon, $descripcionlarga,
                $Barrio, $Gestion, $AreaConstruida, $AreaLote, $latitud,
                $longitud, $EdadInmueble, $NombreInmo, $logo, $IdBarrios)) {
                while ($consulta->fetch()) {
                    if ($IdGestion == 1) {
                        $oper   = 'rent';
                        $precio = $Canon;
                    } else {
                        $oper   = 'sale';
                        $precio = $Venta;
                    }

                    
                    $foto1=str_replace('../', '', getCampo('fotos', "where idInm='" . $Codigo_Inmueble . "'", 'Foto1'));
                    if(!empty($foto1)){
                        $foto1="https://www.simiinmobiliarias.com/mcomercialweb/".$foto1;
                    }else{
                        $foto1="https://www.simiinmobiliarias.com/images/no_image_inm.png";
                    }

                    $arreglo[] = array(
                        "IdInmobiliaria"   => $IdInmobiliaria,
                        "codinm" => $Codigo_Inmueble,
                        "Administracion"   => number_format($Administracion), //1
                        "IdGestion"        => $IdGestion, //2
                        "Estrato"          => $Estrato, //3
                        "IdTpInm"          => $IdTpInm, //4
                        "Codigo_Inmueble"  => $Codigo_Inmueble,
                        "Tipo_Inmueble"    => ucwords(strtolower($Tipo_Inmueble)),
                        "Venta"            => number_format($Venta),
                        "Canon"            => number_format($Canon),
                        "precio"           => number_format($precio),
                        "descripcionlarga" => $descripcionlarga,
                        "Barrio"           => ucwords(strtolower($Barrio)),
                        "Gestion"          => ucwords(strtolower($Gestion)),
                        "AreaConstruida"   => number_format($AreaConstruida),
                        "AreaLote"         => number_format($AreaLote),
                        "latitud"          => $latitud,
                        "longitud"         => $longitud,
                        "EdadInmueble"     => $EdadInmueble,
                        "NombreInmo"       => ucwords(strtolower($NombreInmo)),
                        "ciudad"           => ucwords(strtolower(getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'Nombre'))),
                        "depto"            => ucwords(strtolower(getCampo('departamento', "where IdDepartamento=" . getCampo('ciudad', "where IdCiudad=" . getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdCiudad'), 'IdDepartamento'), 'Nombre'))),
                        "oper"             => $oper,
                        "foto1"            => $foto1,
                        "totaleregistros"  => $total,
                        "logo"             => "https://www.simiinmobiliarias.com/" . str_replace('../', '', $logo),
                    );

                }
            }

            // echo getCampo("perfiles","where pef_id=".$perfil,"perf_desc",1);
            // print_r($arreglo1)."------------------";
            return $arreglo;
        } else {
            return $conn->error;
        }
        $stmt = null;

    }

    public function vericaTokenIframe($data){

        $connPDO=new Conexion();

        $stmt=$connPDO->prepare("Select token_api,IdInmobiliaria
                from inmobiliaria i
                where i.token_api=:token_api
                and i.IdInmobiliaria= :idinmo ");
        if($stmt->execute(array(
            ":idinmo" => $data["inmo"],"token_api"=>$data["token"]
            )))
        {
            $existe=$stmt->rowCount();
            return $existe;

        }



    }

    public function listInmueblesDia($data){

        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT IdInmobiliaria,IdGestion,IdTpInm,IdBarrio,ValorCanon,ValorVenta,Administracion,idInm,codinm
                            from inmnvo i
                            where 
                            fingreso = :fingreso
                            AND idEstadoinmueble=2
                            and IdGestion>0
                            and IdTpInm>0
                            and IdDestinacion>0
                            and (ValorCanon+ValorVenta)>0
                            and IdInmobiliaria != 1
                            #limit 0," . $data['lim']);
        //$stmt->bindParam(':lim', );
        $stmt->bindParam(':fingreso', $data['fingreso']);

        if ($stmt->execute()) {


            $dato = array();
            $connPDO->exec('chartset="utf-8"');
            while ($row = $stmt->fetch()) {
                $IdCiudad = getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], 'IdCiudad');
                $Ciudad   = getCampo('ciudad', "where IdCiudad=" . $IdCiudad, 'Nombre');
                $dataImg=array("codInmue"=>$row['codinm'],"inmCod"=>$row['IdInmobiliaria']);
                $images=$this->getFotosNew2($dataImg);
                /*echo "<pre>";
                print_r($images[0]["Foto"]);
                echo "</pre>";*/
                $foto = trim($images[0]["Foto"]);
                if(count($images)>0 and !empty($foto) ){

                    $dato[] = array(
                        "Inmobiliaria"   => utf8_encode(ucwords(strtolower(getCampo('clientessimi', "where IdInmobiliaria=" . $row['IdInmobiliaria'], "Nombre")))),
                        "ValorVenta"     => number_format($row['ValorVenta']),
                        "ValorCanon"     => number_format($row['ValorCanon']),
                        "IdGestion"      => $row['IdGestion'],
                        "Gestion"        => ucwords(strtolower(getCampo('gestioncomer', "where IdGestion=" . $row['IdGestion'], "NombresGestion"))),
                        "TipoInm"        => ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble=" . $row['IdTpInm'], "Descripcion"))),
                        "Barrio"         => ucwords(strtolower(getCampo('barrios', "where IdBarrios=" . $row['IdBarrio'], "NombreB"))),
                        "ciudad"         => ucwords(strtolower($Ciudad)),
                        "Administracion" => $row['Administracion'],
                        "fingreso"       => $row['fingreso'],
                        "link" => "http://www.simiinmobiliarias.com/mwc/callcenter/inmuebleCruce.php?idInmu=".$row['IdInmobiliaria']."-".$row['codinm'],
                        "foto" => "/mcomercialweb/".$images[0]["Foto"]

                        
                    );

                }
            }
            return $dato;


        }else{
            return print_r($stmt->errorInfo());
        }

    }
    public function saveLogRetiroInmueble($data)
    {
        $connPDO = new Conexion();

        $idInm        = $data['IdInmobiliaria'] ."-".$data['codinm'];
        $fechaserv    = date('Y-m-d');
        $usu_creacion = $_SESSION['Id_Usuarios'];
        $h_creacion   = date('Y-m-d');
        $ip           = $_SERVER['REMOTE_ADDR'];
        
       
        $stmt = $connPDO->prepare("INSERT INTO logretiro
         (id_inmueble,lginmob,observaciones,fecharetiro,horaretiro,usuretiro,estadocam)
            VALUES
            (:id_inmueble,:lginmob,:observaciones,:fecharetiro,:horaretiro,:usuretiro,:estadocam)");

        if ($stmt->execute(array(
            ":id_inmueble"   => $idInm,
            ":lginmob"       => $data['IdInmobiliaria'],
            ":observaciones" => utf8_encode($data['observaciones']),
            ":fecharetiro"   => $fechaserv,
            ":horaretiro"    => $h_creacion,
            ":usuretiro"     => $usu_creacion,
            ":estadocam"     => $data['estadocam']
        ))) {
           
           
            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function updateRetiroInmueble($data)
    {
        $connPDO = new Conexion();

        $idInm        = $data['IdInmobiliaria'] . "-" . $data['codinm'];
               
        $stmt = $connPDO->prepare("UPDATE inmuebles 
            SET idEstadoinmueble =:estadocam
            WHERE idInm          =:idinm");

        if ($stmt->execute(array(
            ":idinm"   => $idInm,
            ":estadocam"       => $data['estadocam']
        ))) {
           
           Inmuebles::updateRetiroInmuebleNvo($data);
           Inmuebles::updateRetiroInmuebleCall($data);
           Inmuebles::saveLogRetiroInmueble($data);
            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function updateRetiroInmuebleNvo($data)
    {
        $connPDO = new Conexion();

        $idInm        = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $fechaserv    = date('Y-m-d');
        
        $stmt = $connPDO->prepare("UPDATE inmnvo 
            SET idEstadoinmueble=:estadocam
            WHERE idInm=:idinm");

        if ($stmt->execute(array(
            ":idinm"     => $idInm,
            ":estadocam" => $data['estadocam']
        ))) {
           
           
            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function updateRetiroInmuebleCall($data)
    {
        $connPDO = new Conexion();

        $idInm        = $data['IdInmobiliaria'] . "-" . $data['codinm'];
        $fechaserv    = date('Y-m-d');
        
        $stmt = $connPDO->prepare("UPDATE datos_call 
            SET idestado          =:estadocam
            WHERE Codigo_Inmueble =:idinm");

        if ($stmt->execute(array(
            ":idinm"     => $idInm,
            ":estadocam" => $data['estadocam']
        ))) {
           
           
            return 1;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function savecloneInmueble($data)
    {
        $connPDO = new Conexion();
        // $data['IdInmobiliaria']=1;
        // $data['codinm']=14058;
        $idInm        = $data['IdInmobiliaria'] ."-".$data['codinm'];
        $fechaserv    = date('Y-m-d');
        $usu_creacion = $_SESSION['Id_Usuarios'];
        $h_creacion   = date('Y-m-d');
        $ip           = $_SERVER['REMOTE_ADDR'];
        $nvoInm       = $this->consecInmuNew($data['IdInmobiliaria']);
        $idInmueblenvo=$data['IdInmobiliaria'] ."-".$nvoInm;
        $stmt = $connPDO->prepare("INSERT INTO inmuebles 
                                    (idInm, codinm,IdInmobiliaria,  RefInmueble, IdGestion, IdTpInm,IdBarrio,
                                    Direccion, NoGaraje,NoDeposito,ValorVenta,ValorCanon,Administracion, ValorMinimo, Estrato, 
                                    NomAdministrador, DirAdmon, TelAdmon, ValorHipoteca, AreaConstruida, AreaLote, AreaTerraza, Frente,                                     Fondo, NoPlantas, Ubicacion, EdadInmueble,Estado, Destinacion, IdUbicacion, chip, NoMatricula,      
                                    CedulaCatastral, Norma, FormadeVisita, NoAlcobas, NoLineasTelefonicas, Caracteristicas, 
                                    IdPropietario, idCaptador, IdPromotor, idPerito,idProcedencia,idEstadoinmueble,HoraVisitas,avisara,
                                    FConsignacion,AvaluoCatastral,fecharotacion,fechaestado,Propietario,Dir_Pro,TelParticular,
                                    TelOficina,TelCelular,Email,Cedula,IdCaptacion,Ref2,ConceptoEmp,Carrera,Calle,Telefonoinm,
                                    ComiVenta,ComiArren,Banos,DacionEnPago,ConMuebles,M2Arre,Edificio,IdDestinacion,NumLlaves,
                                    NumCasillero,M2Vent,FechaVenta,FechaArriendo,InmNuevo,Exclusivo,IdproArre,ProCorreo,TelPorteria,
                                    ERotulo,IdTipoCont,ObsVisita,FechaEnvio,FechaRetiro,RetiradoPor,RetiConcepto,UpdateWeb,NuevoWeb,
                                    FNaci,Aficiones,Club,Estadocivil,Actividad,Refer3,CarPlano,CalPlano,DNE,RefInmueble2,RefInmueble3,
                                    RefInmueble4,RefInmueble5,Direc,Nvp,Lvp,Bvp,Lbvp,Ovp,Nvs,Lvs,Bvs,Lbvs,Nn,Ovs,NroContrato,
                                    Apoderado,DirApoderado,
                                    TelApoderado,emailApoderado,cedulapoderado,celularapoderado,observacionvisita,linkvideo,
                                    descripcionlarga,tipo_contrato,fingreso,fmodificacion,num_visita,conceptohipoteca,amoblado,
                                    latitud,longitud,gradosla,minla,segla,gradoslon,minlon,seglon,escritura_inmu,restricciones,usu_creacion,f_creacion,h_creacion,usu_crea) 
                                    SELECT 
                                    '$idInmueblenvo','$nvoInm', IdInmobiliaria,   '$idInmueblenvo', IdGestion,   IdTpInm,IdBarrio,
                                    Direccion, NoGaraje,NoDeposito,ValorVenta,ValorCanon,Administracion, ValorMinimo, Estrato, 
                                    NomAdministrador, DirAdmon, TelAdmon, ValorHipoteca, AreaConstruida, AreaLote, AreaTerraza, Frente,                                     Fondo, NoPlantas, Ubicacion, EdadInmueble,Estado, Destinacion, IdUbicacion, chip, NoMatricula,      
                                    CedulaCatastral, Norma, FormadeVisita, NoAlcobas, NoLineasTelefonicas, Caracteristicas, 
                                    IdPropietario, idCaptador, IdPromotor, idPerito,idProcedencia,idEstadoinmueble,HoraVisitas,avisara,
                                    FConsignacion,AvaluoCatastral,fecharotacion,fechaestado,Propietario,Dir_Pro,TelParticular,
                                    TelOficina,TelCelular,Email,Cedula,IdCaptacion,Ref2,ConceptoEmp,Carrera,Calle,Telefonoinm,
                                    ComiVenta,ComiArren,Banos,DacionEnPago,ConMuebles,M2Arre,Edificio,IdDestinacion,NumLlaves,
                                    NumCasillero,M2Vent,FechaVenta,FechaArriendo,InmNuevo,Exclusivo,IdproArre,ProCorreo,TelPorteria,
                                    ERotulo,IdTipoCont,ObsVisita,FechaEnvio,FechaRetiro,RetiradoPor,RetiConcepto,UpdateWeb,NuevoWeb,
                                    FNaci,Aficiones,Club,Estadocivil,Actividad,Refer3,CarPlano,CalPlano,DNE,RefInmueble2,RefInmueble3,
                                    RefInmueble4,RefInmueble5,Direc,Nvp,Lvp,Bvp,Lbvp,Ovp,Nvs,Lvs,Bvs,Lbvs,Nn,Ovs,'$idInmueblenvo',
                                    Apoderado,DirApoderado,
                                    TelApoderado,emailApoderado,cedulapoderado,celularapoderado,observacionvisita,linkvideo,
                                    descripcionlarga,tipo_contrato,fingreso,fmodificacion,num_visita,conceptohipoteca,amoblado,
                                    latitud,longitud,gradosla,minla,segla,gradoslon,minlon,seglon,escritura_inmu,restricciones,usu_creacion,f_creacion,h_creacion,usu_crea
                                    FROM inmuebles 
                                    WHERE idInm=:id_inmueble");

        if ($stmt->execute(array(
            ":id_inmueble"   => $idInm
        ))) {
           
            Inmuebles::savecloneInmuebleNvo($data,$idInm,$nvoInm,$idInmueblenvo);
            Inmuebles::savecloneInmuebleDetalle($data,$idInm,$nvoInm,$idInmueblenvo);
            Inmuebles::savecloneInmuebleConsec($data);
            if(isset($data["fotos"])){

                Inmuebles::savecloneInmuebleFotos($data,$idInm,$nvoInm,$idInmueblenvo);
                Inmuebles::savecloneInmuebleFotosNvo($data,$idInm,$nvoInm,$idInmueblenvo);
            
            }
            return trim($idInmueblenvo);
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function savecloneInmuebleConsec($data)
    {
        $connPDO = new Conexion();
       
           
       
        $conse=Inmuebles::consecInmuNormal($data['IdInmobiliaria'])+1;
        
        $stmt = $connPDO->prepare("UPDATE inmobiliaria 
            set consecinmueble=:consecutivo 
            where IdInmobiliaria=:inmo");

        if ($stmt->execute(array(
            ":consecutivo"   => $conse,
            ":inmo"   => $data['IdInmobiliaria']
        ))) {
           
           
            return $conse;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    } 
    public function savecloneInmuebleNvo($data,$idInm,$nvoInm,$idInmueblenvo)
    {
        $connPDO = new Conexion();
       
        
        $fechaserv    = date('Y-m-d');
        $usu_creacion = $_SESSION['Id_Usuarios'];
        $h_creacion   = date('Y-m-d');
        $ip           = $_SERVER['REMOTE_ADDR'];
        
        $stmt = $connPDO->prepare("INSERT INTO inmnvo 
                                    (idInm,codinm,IdInmobiliaria,IdGestion,IdTpInm,IdBarrio,banos,alcobas,garaje,Direccion,DirMetro,dirs,Telefonoinm,ValorVenta,ValorCanon,AdmonIncluida,Administracion,ValorIva,admondto,Estrato,AreaConstruida,AreaLote,EdadInmueble,Destinacion,chip,NoMatricula,CedulaCatastral,idProcedencia,idEstadoinmueble,FConsignacion,AvaluoCatastral,FechaAvaluoComercial,Carrera,Calle,ComiVenta,ComiArren,IdDestinacion,descripcionlarga,descripcionMetro,fingreso,fmodificacion,latitud,longitud,escritura_inmu,restricciones,obserinterna,ValComiVenta,ValComiArr,f_creacion,h_creacion,usu_creacion,permite_geo,FechaAvaluoCatastral,ValorAvaluoComercial,migrado,politica_comp,usu_crea,codinterno,flagm2,leido,notificado,sede,uso) 
                                    SELECT 
                                    '$idInmueblenvo','$nvoInm', IdInmobiliaria,   IdGestion,   IdTpInm,IdBarrio,banos,alcobas,garaje,Direccion,DirMetro,dirs,Telefonoinm,ValorVenta,ValorCanon,AdmonIncluida,Administracion,ValorIva,admondto,Estrato,AreaConstruida,AreaLote,EdadInmueble,Destinacion,chip,NoMatricula,CedulaCatastral,idProcedencia,idEstadoinmueble,FConsignacion,AvaluoCatastral,FechaAvaluoComercial,Carrera,Calle,ComiVenta,ComiArren,IdDestinacion,descripcionlarga,descripcionMetro,fingreso,fmodificacion,latitud,longitud,escritura_inmu,restricciones,obserinterna,ValComiVenta,ValComiArr,f_creacion,h_creacion,usu_creacion,permite_geo,FechaAvaluoCatastral,ValorAvaluoComercial,migrado,politica_comp,usu_crea,codinterno,flagm2,leido,notificado,sede,uso
                                    FROM inmnvo 
                                    WHERE idInm=:id_inmueble");

        if ($stmt->execute(array(
            ":id_inmueble"   => $idInm
        ))) {
           
           
            return $idInmueblenvo;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function savecloneInmuebleDetalle($data,$idInm,$nvoInm,$idInmueblenvo)
    {
        $connPDO = new Conexion();
        
        
        $fechaserv    = date('Y-m-d');
        $usu_creacion = $_SESSION['Id_Usuarios'];
        $h_creacion   = date('Y-m-d');
        
        
        $stmt = $connPDO->prepare("INSERT INTO detalleinmueble(idinmueble,codinmdet,idusuario,idcaracteristica,inmob) 
                                    SELECT 
                                    '$idInmueblenvo','$nvoInm', idusuario,idcaracteristica,inmob FROM detalleinmueble 
                                    WHERE idinmueble=:id_inmueble");

        if ($stmt->execute(array(
            ":id_inmueble"   => $idInm
        ))) {
           
           
            return $idInmueblenvo;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function savecloneInmuebleFotos($data,$idInm,$nvoInm,$idInmueblenvo)
    {
        $connPDO = new Conexion();
       
        
        $fechaserv    = date('Y-m-d');
        $usu_creacion = $_SESSION['Id_Usuarios'];
        $h_creacion   = date('Y-m-d');
        
        
        $stmt = $connPDO->prepare("INSERT INTO fotos(idInm,Foto1,Foto2,Foto3,Foto4,Foto5,Foto6,Foto7,Foto8,Foto9,Foto10,
                                     Foto11,Foto12,Foto13,Foto14,Foto15,Foto16,Foto17,Foto18,Foto19,Foto20,Foto21,Foto22,Foto23,
                                        Foto24, Foto25, Foto26, Foto27, Foto28, Foto29, Foto30) 
                                    SELECT 
                                    '$idInmueblenvo',Foto1,Foto2,Foto3,Foto4,Foto5,Foto6,Foto7,Foto8,Foto9,Foto10,
                                     Foto11,Foto12,Foto13,Foto14,Foto15,Foto16,Foto17,Foto18,Foto19,Foto20,Foto21,Foto22,Foto23,
                                        Foto24, Foto25, Foto26, Foto27, Foto28, Foto29, Foto30 FROM fotos
                                        WHERE idInm=:id_inmueble");

        if ($stmt->execute(array(
            ":id_inmueble"   => $idInm
        ))) {
           
           
            return $idInmueblenvo;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
    public function savecloneInmuebleFotosNvo($data,$idInm,$nvoInm,$idInmueblenvo)
    {
        $connPDO = new Conexion();
       
        
        $fechaserv    = date('Y-m-d');
        $usu_creacion = $_SESSION['Id_Usuarios'];
        $h_creacion   = date('Y-m-d');
        $conse      = Inmuebles::conseFotosNvo($idInmueblenvo);
        
        $stmt = $connPDO->prepare("INSERT INTO fotos_nvo(conse,id_inmft,posi_ft,foto,est_fot,aa_fto,codinm_fto)
                                    SELECT 
                                    '$conse','$idInmueblenvo',posi_ft,foto,est_fot,aa_fto,'$nvoInm' 
                                    FROM fotos_nvo
                                        WHERE id_inmft=:id_inmueble");

        if ($stmt->execute(array(
            ":id_inmueble"   => $idInm
        ))) {
           
           
            return $idInmueblenvo;
        } else {
            return print_r($stmt->errorInfo()) . " error";
        }
        $stmt = null;
    }
     public function conseFotosNvo()
    {
        $connPDO = new Conexion();
        $response =0;
        $stmt    = $connPDO->prepare("SELECT MAX(conse) as conse 
            from fotos_nvo");
        
        if ($stmt->execute(
            )) 
        {
            while ($row = $stmt->fetch()) 
            {
                 $response = $row['conse'];
            }
            //$this->updateconsecInmu($response+1,$inmob);
            return $response + 1;
        } else {
            print_r($stmt->errorInfo());
        }
        $stmt = null;
    }
    public function infoPropetario($data)
    {

        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT Nombrepro,correopro,telpro,celpro 
            FROM lista_inmuebles2 
            WHERE Codigo_Inmueble = :idinm");
        
        $stmt->bindParam(':idinm', $data['idinm']);

        if ($stmt->execute()) 
        {

            $dato = array();
            $connPDO->exec('chartset="utf-8"');
            $cantidad=$stmt->rowCount();
            if($cantidad>0)
            {
                while ($row = $stmt->fetch()) 
                {
     
                    $dato[] = array(
                        "correopro"   => utf8_encode($row['correopro']),
                        "Nombrepro"   => utf8_encode(ucwords(strtolower( $row['Nombrepro']))),
                        "telpro"     => $row['telpro'],
                        "celpro" => $row['celpro']
                    );
                }
                return $response=array("datos"=>$dato,"status"=>0,"error"=>0);    
            }
            else
            {
                return array("msn"=>'Este inmueble no tiene asociado ningun propietario',"status"=>1,"error"=>0);
            }

            
        }
        else
        {
            return array("msn"=>$stmt->errorInfo(),"status"=>0,"error"=>1);//print_r($stmt->errorInfo());
        }
    }
    public function insertObservacionesPropietario($data)
    {
        $connPDO = new Conexion();
       
        
        $fechaserv    = date('Y-m-d');
        $usu_creacion = $_SESSION['Id_Usuarios'];
        $h_creacion   = date('Y-m-d');
        $idInm        = $data['codInmProp'];
        list($aa,$codinm)= explode('-',$idInm);
        
        
        $stmt = $connPDO->prepare("INSERT INTO log_propietario (idInm,logdes,fechacomen,IdUsuarioComen,IdInmo)
                                    VALUES(:idInm,:logdes,:fechacomen,:IdUsuarioComen,:IdInmo)");

        if ($stmt->execute(array(
            ":idInm"          => $idInm,
            ":logdes"         => $data['obserProp'],
            ":IdUsuarioComen" => $usu_creacion,
            ":IdInmo"         => $aa,
            ":fechacomen"     => $fechaserv
        ))) {
           
           
             if($stmt->rowCount()>0)
             {
                return $response=array("datos"=>1,"status"=>0,"error"=>0,"msn"=>'Bitacora actualizada');
             }
             else
            {
              return $response=array("datos"=>1,"status"=>1,"error"=>0);  
            }
        } else {
            return array("msn"=>'No se Guardo Bitacora',"error"=>1);
        }
        $stmt = null;
    }


}
