<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
// @include("../funciones/connPDO.php");

class Negocios
{
    public function __construct($conn = "")
    {
        $this->db = $conn;
    }

    private $id_usuario;

    public function consultaClienteSelect($cedula, $inmob, $idselect)
    {
        $w_conexion = new MySQL();
        getSelectP('clientes_inmobiliaria', 'cedula', 'CONCAT(UPPER(LEFT(nombre, 1)), LOWER(SUBSTRING(nombre, 2)))', "", "where inmobiliaria=$inmob", "$idselect", "", '', 'class="form-control chosen-select" id="$idselect"', '', '', 'Seleccione Cliente');
    }
    public function consultaCliente($cedula, $inmob)
    {
        if (($consulta = $this->db->prepare(" select telfijo,telcelular,email
		from clientes_inmobiliaria
        where cedula=?
        and inmobiliaria=?"))) {
            $consulta->bind_param("ii", $cedula, $inmob);
            $consulta->execute();
//                echo "$qry";
            if ($consulta->bind_result($telfijo, $telcelular, $email)) {
                while ($consulta->fetch()) {
                    $cadena = $telfijo . "  " . $telcelular . "?" . $email;
                }
            }
            echo $cadena;
        } else {
            echo "error --" . $conn->error;
        }

    }

    public function creaDemanda($inmob, $FechaD, $IdPromotor, $numced, $calle, $alacalle, $carrera, $alacarrera, $Ciudad, $barrio, $idgestion, $IdDestinacion, $IdTipoInm, $AreaIni, $AreaFin, $ValInicial, $ValFinal, $mgrupo, $descripcion, $fsis)
    {
        $w_conexion = new MySQL();
        include '../mcomercialweb/emailtaeD.class.php';
        $f_sis     = date('Y-m-d');
        $clave     = '0000';
        $idDemanda = consecutivo('IdDemanda', 'demandainmuebles', 0);
        $sql       = "INSERT INTO demandainmuebles
		(FechaD,IdPromotor,FechaLimite,CedulaIInt,Calle,
		 AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
		 IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
		 Descripcion,clave,IdCiudad,IdInmobiliaria,IdBarrio,
		 grupo_dem,IdDemanda)
		 VALUES ('$fsis','$IdPromotor','$FechaD','$numced','$calle',
			'$alacalle','$carrera','$alacarrera','$idgestion','$IdDestinacion',
			'$IdTipoInm','$ValInicial','$ValFinal','$AreaIni','$AreaFin',
			'$descripcion','$clave','$Ciudad','$inmob','$barrio',
			'$mgrupo','$idDemanda')";
        $res = $w_conexion->RecordSet($sql);
        if ($res) {

        }

    }
    public function creaDemandaNvo($inmob, $FechaD, $IdPromotor, $numced, $calle, $alacalle, $carrera, $alacarrera, $Ciudad, $barrio, $idgestion, $IdDestinacion, $IdTipoInm, $AreaIni, $AreaFin, $ValInicial, $ValFinal, $mgrupo, $descripcion, $fsis, $barrios, $localidad, $estrato)
    {
        $w_conexion = new MySQL();
        include '../mcomercialweb/emailtaeD.class.php';
        $f_sis     = date('Y-m-d');
        $clave     = '0000';
        $idDemanda = consecutivo('IdDemanda', 'demandainmuebles', 0);
        $sql       = "INSERT INTO demandainmuebles
		(FechaD,IdPromotor,FechaLimite,CedulaIInt,Calle,
		 AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
		 IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
		 Descripcion,clave,IdCiudad,IdInmobiliaria,IdBarrio,
		 grupo_dem,IdDemanda,localidad,estrato_dem)
		 VALUES ('$fsis','$IdPromotor','$FechaD','$numced','$calle',
			'$alacalle','$carrera','$alacarrera','$idgestion','$IdDestinacion',
			'$IdTipoInm','$ValInicial','$ValFinal','$AreaIni','$AreaFin',
			'$descripcion','$clave','$Ciudad','$inmob','$barrio',
			'$mgrupo','$idDemanda','$localidad','$estrato')";
        $res = $w_conexion->RecordSet($sql);
        if ($res) {
            /*
            recibimos la cadena de barrios y los descomponemos para hacer un ciclo
             */
            if ($barrios) {
                $barrios = subst($barrios, 0, -1);
                $cad     = explode(",", $barrios);

                for ($i = 0; $i < count($cad); $i++) {
                    list($bar1, $loc1) = explode("-", $cad[$i]);
                    $conse_de          = consecutivo('conse_de', 'demandas_barrios', 0);
                    $sql_bdm           = "INSERT INTO demandas_barrios
					(conse_de,id_demanda_de,id_ciu_de,id_loc_de,id_barrio_de,estado_de)
					VALUES('$conse_de','$idDemanda','$Ciudad','$loc1','" . $bar1 . "','1')";
                    $resbdm = $w_conexion->RecordSet($sql_bdm) or die("error " . mysql_error());
                }
            }
        }
    }

    public function editaDemanda($inmob, $FechaD, $IdPromotor, $numced, $calle, $alacalle, $carrera, $alacarrera, $Ciudad, $barrio, $idgestion, $IdDestinacion, $IdTipoInm, $AreaIni, $AreaFin, $ValInicial, $ValFinal, $mgrupo, $descripcion, $fsis, $IdDemanda, $Estado)
    {
        $w_conexion = new MySQL();
        $f_sis      = date('Y-m-d');
        $h_sis      = date('H:i:s');
        $usuario    = $_SESSION['Id_Usuarios'];
        $ip         = $_SERVER['REMOTE_ADDR'];

        //consulta de valores originales para guardar bitácora 2014-12-16
        $sql_orig = "SELECT IdDemanda,IdPromotor,FechaLimite,Calle,
					AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
					IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
					Descripcion,Estado,IdBarrio,grupo_dem
					FROM demandainmuebles
					where IdDemanda='$IdDemanda'";
        $res_or = $w_conexion->ResultSet($sql_orig);
        while ($f_or = $w_conexion->FilaSiguienteArray($res_or)) {
            $IdPromotor_or    = $f_or['IdPromotor'];
            $FechaLimite_or   = $f_or['FechaLimite'];
            $Calle_or         = $f_or['Calle'];
            $AlaCalle_or      = $f_or['AlaCalle'];
            $Carrera_or       = $f_or['Carrera'];
            $AlaCarrera_or    = $f_or['AlaCarrera'];
            $IdGestion_or     = $f_or['IdGestion'];
            $IdDestinacion_or = $f_or['IdDestinacion'];
            $IdTipoInm_or     = $f_or['IdTipoInm'];
            $ValInicial_or    = $f_or['ValInicial'];
            $ValFinal_or      = $f_or['ValFinal'];
            $AreaInicial_or   = $f_or['AreaInicial'];
            $AreaFinal_or     = $f_or['AreaFinal'];
            $Descripcion_or   = $f_or['Descripcion'];
            $Estado_or        = $f_or['Estado'];
            $IdBarrio_or      = $f_or['IdBarrio'];
            $grupo_dem_or     = $f_or['grupo_dem'];
        }

        $sql = "UPDATE demandainmuebles
				SET
				IdPromotor			='$IdPromotor',
				FechaLimite			='$FechaD',
				CedulaIInt			='$numced',
				Calle				='$calle',
		 		AlaCalle			='$alacalle',
				Carrera				='$carrera',
				AlaCarrera			='$alacarrera',
				IdGestion			='$idgestion',
				IdDestinacion		='$IdDestinacion',
		 		IdTipoInm			='$IdTipoInm',
				ValInicial			='$ValInicial',
				ValFinal			='$ValFinal',
				AreaInicial			='$AreaIni',
				AreaFinal			='$AreaFin',
		 		Descripcion			='$descripcion',
				IdCiudad			='$Ciudad',
				IdBarrio			='$barrio',
		 		grupo_dem			= '$mgrupo',
				Estado				= '$Estado'
				WHERE IdDemanda		= '$IdDemanda'";
        $res = $w_conexion->RecordSet($sql);
        if ($res) {
            ///guardar historial
            if ($IdPromotor_or != $IdPromotor) {
                $cambio++;
                $nom_campo   = "IdPromotor";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Promotor"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $IdPromotor, $IdPromotor_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($FechaLimite_or != $FechaD) {
                $cambio++;
                $nom_campo   = "FechaLimite";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Fecha Limite"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $Fechalimite, $FechaLimite_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($Calle_or != $calle) {
                $cambio++;
                $nom_campo   = "Calle";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Calle"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $calle, $Calle_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($AlaCalle_or != $alacalle) {
                $cambio++;
                $nom_campo   = "AlaCalle";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "A la Calle"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $alacalle, $AlaCalle_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($Carrera_or != $carrera) {
                $cambio++;
                $nom_campo   = "Carrera";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Carrera"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $carrera, $Carrera_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($AlaCarrera_or != $alacarrera) {
                $cambio++;
                $nom_campo   = "AlaCarrera";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "A la Carrera"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $alacarrera, $AlaCarrera_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($IdGestion_or != $idgestion) {
                $cambio++;
                $nom_campo   = "IdGestion";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Gestion"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $idgestion, $IdGestion_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($IdDestinacion_or != $IdDestinacion) {
                $cambio++;
                $nom_campo   = "IdDestinacion";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Destinacion"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $IdDestinacion, $IdDestinacion_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($IdTipoInm_or != $IdTipoInm) {
                $cambio++;
                $nom_campo   = "IdTipoInm";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Tipo de Inmueble"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $IdTipoInm, $IdTipoInm_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($ValInicial_or != $ValInicial) {
                $cambio++;
                $nom_campo   = "ValInicial";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Valor Inicial"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $ValInicial, $ValInicial_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($ValFinal_or != $ValFinal) {
                $cambio++;
                $nom_campo   = "ValFinal";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Valor Final"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $ValFinal, $ValFinal_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($AreaInicial_or != $AreaIni) {
                $cambio++;
                $nom_campo   = "AreaInicial";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Area Inicial"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $AreaIni, $AreaInicial_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($AreaFinal_or != $AreaFin) {
                $cambio++;
                $nom_campo   = "AreaFin";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Area Final"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $AreaFin, $AreaFinal_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($Descripcion_or != $descripcion) {
                $cambio++;
                $nom_campo   = "Descripcion";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Descripcion"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $descripcion, $Descripcion_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($Estado_or != $Estado) {
                $cambio++;
                $nom_campo   = "estado_dem";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Estado"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $Estado, $Estado_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }
            if ($IdBarrio_or != $barrio) {
                if (is_numeric($barrio)) {
                    $cambio++;
                    $nom_campo   = "barrio";
                    $cons_log    = consecutivo_log(1);
                    $campo_usu_g = "Barrio"; //nombre del campo en el formulario
                    $param_g     = 0; //codigo de la tabla par?metros
                    guarda_log_gral($cons_log, 1, $nom_campo, $barrio, $IdBarrio_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
                }
            }
            if ($grupo_dem_or != $mgrupo) {
                $cambio++;
                $nom_campo   = "grupo";
                $cons_log    = consecutivo_log(1);
                $campo_usu_g = "Grupo"; //nombre del campo en el formulario
                $param_g     = 0; //codigo de la tabla par?metros
                guarda_log_gral($cons_log, 1, $nom_campo, $mgrupo, $grupo_dem_or, $f_sis, $h_sis, $usuario, $ip, $IdDemanda, $campo_usu_g, $param_g);
            }

            echo $idDemanda;
        }
    }

    public function datosDemanda($idDemanda, $inmob)
    {
        $w_conexion = new MySQL();
        $arreglo1   = array();
        $sqlpagos   = "SELECT FechaD,IdPromotor,FechaLimite,CedulaIInt,Calle,
		 AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
		 IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
		 Descripcion,clave,IdCiudad,IdInmobiliaria,IdBarrio,
		 grupo_dem,IdDemanda,Estado
		 from demandainmuebles
		 where IdDemanda='$idDemanda'";
        //echo $sqlpagos."<br>";
        $res       = $w_conexion->ResultSet($sqlpagos);
        $cantidadt = $w_conexion->FilasAfectadas($res);
        if ($cantidadt == 0) {echo "No Hay Demandas para la busqueda seleccionada";}

        while ($fila7 = $w_conexion->FilaSiguienteArray($res)) {
            $i++;

            $arreglo[0]  = $fila7['FechaD'];
            $arreglo[1]  = $fila7['IdPromotor'];
            $arreglo[2]  = $fila7['FechaLimite'];
            $arreglo[3]  = $fila7['CedulaIInt'];
            $arreglo[4]  = ucwords(strtolower(getCampo('cliente_captacion', 'WHERE id_cliente_capta=' . $fila7['CedulaIInt'] . ' and id_inmobiliaria=' . $inmob, 'nombre')));
            $arreglo[5]  = $fila7['Calle'];
            $arreglo[6]  = $fila7['IdBarrio'];
            $arreglo[7]  = ucwords(strtolower(getCampo('ciudad', 'WHERE IdCiudad=' . getCampo('barrios', 'WHERE IdBarrios=' . $fila7['IdBarrio'], 'IdCiudad'), 'Nombre')));
            $arreglo[9]  = $fila7['AlaCalle'];
            $arreglo[10] = $fila7['Carrera'];
            $arreglo[11] = $fila7['AlaCarrera'];
            $arreglo[12] = $fila7['IdGestion'];
            $arreglo[13] = $fila7['IdDestinacion'];
            $arreglo[14] = $fila7['IdTipoInm'];
            $arreglo[15] = $fila7['ValInicial'];
            $arreglo[16] = $fila7['ValFinal'];
            $arreglo[17] = $fila7['AreaInicial'];
            $arreglo[18] = $fila7['AreaFinal'];
            $arreglo[19] = $fila7['Descripcion'];
            $arreglo[20] = $fila7['IdCiudad'];
            $arreglo[21] = $fila7['grupo_dem'];
            $arreglo[22] = $fila7['Estado'];

            $arreglo1[] = $arreglo;
        }
        return $arreglo1;
    }

    public function datosMisDemanda($tip_inm, $dest, $tip_ope, $asesor, $est_cli, $cod_inm, $ffecha_ini, $ffecha_fin, $inmob, $perfil, $usu)
    {
        $w_conexion = new MySQL();

        $cond  = "";
        $cond2 = "";
        if ($tip_inm > 0) {
            $cond .= " and IdTpInm='" . $tip_inm . "'";
        }
        if ($dest > 0) {
            $cond .= " and IdDestinacion='" . $dest . "'";
        }
        if ($tip_ope > 0) {
            if ($tip_ope != 2) {
                $cond .= " and IdGestion='" . $tip_ope . "'";
            } else {
                $cond .= " and IdGestion in ($tip_ope,1,5)";
            }
        }
        if ($asesor > 0) {
            $cond .= " and usu_cm='" . $asesor . "'";
        }
        if ($est_cli > 0) {
            $cond .= " and est_cm='" . $est_cli . "'";
        }
        if ($cod_inm > 0) {
            $cond .= " and inmuebles.idInm='" . $cod_inm . "'";
        }
        if ($ffecha_ini > 0 and $ffecha_fin > 0) {
            $cond .= " and fecha_cm  between'" . $ffecha_ini . "' and '" . $ffecha_fin . "'";
        }
        if ($inmob != 1) {
            $cond2 .= "AND IdInmobiliaria = '" . $inmob . "'";
        }
        if ($perfil == 3 or $perfil == 10) {
            //$cond2 .=" ";
        } else {
            $cond2 .= " AND IdPromotor = '" . $usu . "'";
        }
        $hoy      = date("Y-m-d");
        $sqlpagos = "";

        $data     = array();
        $sqlpagos = "Select idInm,  IdGestion,  IdTpInm, ComiArren,ComiVenta,
					Direccion,  IdBarrio ,IdInmobiliaria,condiciones_compartir_inm.*,IdPromotor,
					ValComiVenta,ValComiArr,IdDestinacion
					FROM inmuebles,condiciones_compartir_inm
					WHERE  idInm=id_inmu_cm
					and idEstadoinmueble=2
					$cond2
					$cond";
        //echo $sqlpagos."-------------------<br>";
        //        echo $perfil."ffffffffffffff";

        $res       = $w_conexion->ResultSet($sqlpagos);
        $cantidadt = $w_conexion->FilasAfectadas($res);
        if ($cantidadt == 0) {
            //echo "No Hay Demandas para la busqueda seleccionada";
        }

        while ($fila77 = $w_conexion->FilaSiguienteArray($res)) {
            $i++;

            $lupa = '<button type="button" id="botonpre2" class="btn btn-warning botonpre2 btn-xs"  data-toggle="modal"  data-target=".mymodales"><span class="glyphicon glyphicon-edit"></span></button>';

            $btnCLiente = '<button type="button" id="botonpre3" class="btn btn-info  botonpre3 btn-xs"  data-toggle="modal"  data-target=".mymodalescli"><span class="glyphicon glyphicon"><i class="fa fa-files-o"></i></span></button>';

            $botonedit = '<input type="hidden" class="idseg" value="' . $idseguimiento . '"><input type="hidden" class="idimb" value="' . $idInmueble . '">' . $lupa . " " . $btnCLiente55555;

            $data[] = array(
                "idInm"          => $fila77['idInm'],
                "Nombres"        => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $fila77['IdPromotor'] . "'", 'Nombres'))),
                "apellidos"      => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $fila77['IdPromotor'] . "'", 'apellidos'))),
                "NCompleto"      => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $fila77['IdPromotor'] . "'", 'concat(Nombres," ",apellidos)'))),
                "condiciones_cm" => $fila77['condiciones_cm'],
                "ComiVenta"      => $fila77['ComiVenta'] * 100,
                "ComiArren"      => $fila77['ComiArren'] * 100,
                "ValComiArr"     => number_format($fila77['ValComiArr']),
                "ValComiVenta"   => number_format($fila77['ValComiVenta']),
                "NombresGestion" => ucwords(strtolower(getCampo('gestioncomer', "where IdGestion='" . $fila77['IdGestion'] . "'", 'NombresGestion'))),
                "destinacion"    => ucwords(strtolower(getCampo('destinacion', "where Iddestinacion='" . $fila77['IdDestinacion'] . "'", 'Nombre'))),
                "tipoinmuebles"  => ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble='" . $fila77['IdTpInm'] . "'", 'Descripcion'))),
                "estcm"          => getCampo('parametros', "WHERE id_param=22 and conse_param='" . $fila77['est_cm'] . "'", 'desc_param'),
                "barr"           => ucwords(strtolower(getCampo('barrios', "WHERE IdBarrios='" . $fila77['IdBarrio'] . "'", 'NombreB'))),
                "botones"        => $botonedit,
            );

        }
        echo json_encode($data);
    }
    public function getUsuariosDemandas($idInmo)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT u.Nombres,
                  						u.Id_Usuarios,
                  						u.apellidos
          						FROM usuarios u,
               						 demandainmuebles d
          						WHERE u.Id_Usuarios=d.IdPromotor
          						AND u.IdInmmo=:idInmmo
          						GROUP BY d.IdPromotor
          						ORDER BY u.Nombres");
        $stmt->bindParam(":idInmmo", $idInmo["idInmmo"]);
        $stmt->execute();
        $data = array();
        while ($row = $stmt->fetch()) {
            $data[] = array(
                "id"        => $row["Id_Usuarios"],
                "nombres"   => ucwords(strtolower($row["Nombres"])),
                "apellidos" => ucwords(strtolower($row['apellidos'])));
        }
        return $data;

    }
    public function listDemandas($data)
    {

        $connPDO  = new Conexion();
        $Demandas = new Negocios();

        $cond      = "";
        $hoy       = date("Y-m-d");
        $condFecha = " and FechaLimite>='$hoy'";
        if ($data['tip_inm']) {
            $cond .= " and d.IdTipoInm=:inmueble";

        }
        if ($data['dest']) {
            $cond .= " and d.IdDestinacion=:dest";
        }
        if ($data['estrato']) {
            $cond .= " and d.estrato_dem=:estrato";
        }
        if ($data['tip_ope']) {
            if ($data['tip_ope'] != 2) {
                $cond .= " and d.IdGestion=:tip_ope";
            } else {
                $cond .= " and d.IdGestion != 3";
            }
        }
        if ($data['asesor']) {
            $cond .= " and d.IdPromotor=:asesor";
        }
        if ($data['est_cli']) {
            $cond .= " and d.Estado=:est_cli";
        }
        if ($data['codDemanda']) {
            //echo "ingresa a codDemanda ".$_GET['codDemanda'];
            $cond .= " and d.IdDemanda=:codDemanda";
        }
        if ($data['interesado']) {
            $cond .= " and d.CedulaIInt =:interesado";
        }
        if ($data["ffecha"] == 1) {
            if ($data["ffecha_ini"] > 0 and $data["ffecha_fin"] > 0) {
                $cond .= " and FechaD  between :ffecha_ini and :ffecha_fin";
                $condFecha = "";
            }
            if ($data['idInmmo'] != 1) {
                $cond2 .= "AND IdInmobiliaria = :idInmmo";
            }
        } else {
            if ($data["ffecha_ini"] > 0 and $data["ffecha_fin"] > 0) {
                $cond .= " and d.FechaLimite  between :ffecha_ini and :ffecha_fin";
                $condFecha = "";
            }
            if ($data['idInmmo'] != 1) {
                $cond2 .= "AND IdInmobiliaria = :idInmmo";
            }
        }
        if ($data['perfil'] == 3 or $data['perfil'] == 10) {

        }

        $stmt = $connPDO->prepare("SELECT d.IdDemanda,
								d.FechaD,
								d.IdPromotor,
								d.FechaLimite,
			      				d.CedulaIInt,
			      				d.Calle,
			      				d.AlaCalle,
			      				d.Carrera,
			      				d.AlaCarrera,
			      				d.IdGestion,
			      				d.IdDestinacion,
			      				d.IdTipoInm,
			      				d.ValInicial,
			      				d.ValFinal,
			      				d.AreaInicial,
			      				d.AreaFinal,
			      				d.Descripcion,
			                    c.Nombre as Interesado,
			                    c.email,
			                    c.telfijo,
			                    c.telcelular,
			                    g.NombresGestion,
			                    t.Descripcion As TipoInmueble,
			      				c.cedula as cedulaInte,
			      				d.Estado,
			      				ba.NombreB,
			      				d.grupo_dem,
			      				des.Nombre as Nombredes,
			      				IF ('$hoy'  BETWEEN d.FechaD AND d.FechaLimite,1,0)
			      				as RESULTADO
			      		FROM 	demandainmuebles d
			      		INNER JOIN clientes_inmobiliaria c On d.CedulaIInt = c.cedula
			      		LEFT JOIN barrios ba ON ba.IdBarrios 			   = d.IdBarrio
			      		INNER JOIN gestioncomer g On d.IdGestion           =g.IdGestion
			      		INNER JOIN tipoinmuebles t On d.IdTipoInm =t.idTipoInmueble
			      		INNER JOIN destinacion des on des.Iddestinacion = d.IdDestinacion
			      		WHERE c.inmobiliaria = :idInmmo
						$condFecha
			      		$cond2
			      		$cond");
        if ($data['tip_inm']) {
            $stmt->bindParam(":inmueble", $data['tip_inm']);
        }
        if ($data['estrato']) {
            $stmt->bindParam(':estrato', $data["estrato"]);
        }
        if ($data['dest']) {
            $stmt->bindParam(':dest', $data["dest"]);
        }
        if ($data['tip_ope']) {
            $stmt->bindParam(":tip_ope", $data["tip_ope"]);
        }
        if ($data['asesor']) {
            $stmt->bindParam(":asesor", $data["asesor"]);
        }
        if ($data['est_cli']) {
            $stmt->bindParam(':est_cli', $data["est_cli"]);
        }
        if ($data['codDemanda']) {
            //echo "ingresa a codDemanda ".$_GET['codDemanda'];
            $stmt->bindParam(":codDemanda", $data["codDemanda"]);
        }
        if ($data['interesado']) {
            $stmt->bindParam(":interesado", $data["interesado"]);
        }
        if ($data["ffecha_ini"] > 0 and $data["ffecha_fin"] > 0) {
            $stmt->bindParam(':ffecha_ini', $data["ffecha_ini"]);
            $stmt->bindParam(':ffecha_fin', $data["ffecha_fin"]);
        }
        if ($data['idInmmo'] != 1) {
            $cond2 .= "AND IdInmobiliaria = :idInmmo";

        }
        if ($data['perfil'] == 3 or $data['perfil'] == 10) {

        }
        $stmt->bindParam(':idInmmo', $data["idInmmo"]);
        $stmt->execute();
        $listnegocios = array();
        while ($row = $stmt->fetch()) {

            $logs = $this->logsObservaDemandas($row["IdDemanda"]);

            $negociarBtn = ($row["RESULTADO"] == 0) ? "" : "<button class='btn btn-success btn-sm'data-toggle='modal' data-target='#modal-negos' title='Aplicar Oferta'><span class='glyphicon glyphicon-ok'></span></button>";
            $editarBtn   = "<button class='btn btn-warning btn-sm editde' data-toggle='modal' data-target='#modal-deman' title='Editar Demanda' ><span class='fa fa-edit'></span></button>";

            $cambiosBtn = ($logs == 1) ? "
      			<button class='btn btn-info btn-sm' data-toggle='modal' data-target='#modal-logs' title='Ver Cambios'>
      			<span class='glyphicon glyphicon-eye-open'></span></button>" : "";

            $seguimientoBtn = "<button class='btn btn-danger btn-sm' data-toggle='modal'  data-target='#modal-id'  title='Seguimiento' alt='seguimiento'><span class='fa fa-sign-in'></span></button>";
            $dataModal      = $editarBtn . $cambiosBtn . $seguimientoBtn;
            $calle          = ucwords(strtolower("De la calle " . $row["Calle"] . " a la " . $row["AlaCalle"]));
            $carrera        = ucwords(strtolower("De la carrera " . $row["Carrera"] . " a la " . $row["AlaCarrera"]));
            $dataCliente    = $row['telfijo'] . "-" . $row['telcelular'];
            $inmuebles      = '11111';
            $inmuebles      = $Demandas->buscarInmuebles($row["IdDemanda"], $_SESSION['IdInmmo'], $row["IdGestion"], $row["IdDestinacion"], $row["IdTipoInm"]);

            $listnegocios[] = array(
                "idemanda"     => $row["IdDemanda"],
                "destinacion"  => ucwords(strtolower($row["Nombredes"])),
                "fechaLimite"  => $row["FechaLimite"],
                "fechaD"       => $row["FechaD"],
                "interesado"   => ucwords(strtolower($row["Interesado"])),
                "barrio"       => ucwords(strtolower($row["NombreB"])),
                "gestion"      => ucwords(strtolower($row["NombresGestion"])),
                "tipoInmueble" => ucwords(strtolower($row["TipoInmueble"])),
                "valorInicial" => "$" . number_format($row["ValInicial"]),
                "valorFinal"   => "$" . number_format($row["ValFinal"]),
                "Calle"        => $calle,
                "Carrera"      => $carrera,
                "btnModal"     => $dataModal,
                "telcli"       => $dataCliente,
                "mailcli"      => $row["email"],
                "ofertas"      => $inmuebles,
                "obser"        => $row["Descripcion"],

            );
        }
        return $listnegocios;
    } //fin function listDemandas()

    public function listInmDemandas($data)
    {
        //Danny

        $connPDO  = new Conexion();
        $Demandas = new Negocios();

        $cond = "";
        if ($data['tip_inm']) {
            $cond .= " and i.IdTpInm=:inmueble";

        }
        if ($data['dest']) {
            $cond .= " and i.IdDestinacion=:dest";
        }
        if ($data['tip_ope']) {
            if ($data['tip_ope'] != 2) {
                $cond .= " and i.IdGestion=:tip_ope";
            } else {
                $cond .= " and i.IdGestion != 3";
            }
        }
        if ($data['asesor']) {
            $cond .= " and i.IdPromotor=:asesor";
        }
        if ($data['idInm']) {
            //echo "ingresa a codDemanda ".$_GET['codDemanda'];
            $cond .= " and i.idInm=:idInm";
        }
        /*if($data['idInmmo']!=1)
        {
        $cond2 .="AND IdInmobiliaria = :idInmmo";
        }*/
        if ($data['perfil'] == 3 or $data['perfil'] == 10) {

        }
        $hoy = date("Y-m-d");

        $stmt = $connPDO->prepare("SELECT idInm,IdGestion,IdDestinacion,ValorVenta,ValorCanon,IdPromotor,IdTpInm,politica_comp
					FROM inmuebles i
					WHERE i.IdInmobiliaria = :idInmmo
					#and i.politica_comp!=''
					$cond2
					$cond");
        if ($data['tip_inm']) {
            $stmt->bindParam(":inmueble", $data['tip_inm']);
        }
        if ($data['dest']) {
            $stmt->bindParam(':dest', $data["dest"]);
        }
        if ($data['tip_ope']) {
            $stmt->bindParam(":tip_ope", $data["tip_ope"]);
        }
        if ($data['asesor']) {
            $stmt->bindParam(":asesor", $data["asesor"]);
        }

        if ($data['idInm']) {
            //echo "ingresa a codDemanda ".$_GET['codDemanda'];
            $stmt->bindParam(":idInm", $data["idInm"]);
        }

        if ($data['idInmmo'] != 1) {
            $cond2 .= "AND IdInmobiliaria = :idInmmo";

        }
        if ($data['perfil'] == 3 or $data['perfil'] == 10) {

        }

        $stmt->bindParam(':idInmmo', $data["idInmmo"]);
        $stmt->execute();
        $listnegocios = array();
        while ($row = $stmt->fetch()) {
            ///boton para descartar demanda
            $descartarBtn = ($row["RESULTADO"] == 1) ? "" : "<button class='btn btn-danger btn-sm dct' data-toggle='modal' data-target='#modal1-id' title='Descartar Demanda'><span class='glyphicon fa fa-times-circle'></span></button>";

            $negociarBtn    = ($row["RESULTADO"] == 0) ? "" : "<button class='btn btn-success btn-sm' 	><span class='glyphicon fa fa-check-square-o'></span></button>";
            $editarBtn      = "<button class='btn btn-warning btn-sm dct1' title='Buscar Demanda' data-toggle='modal' data-target='#modal2-id'><span class='fa fa-search'></span></button>";
            $cambiosBtn     = "<button class='btn btn-info btn-sm' toggle='modal' data-target='#modal-id'><span class='glyphicon glyphicon-eye-open'></span></button>";
            $seguimientoBtn = "<button class='btn btn-danger btn-sm'><span class='fa fa-sign-in'></span></button>";
            // $ofertabtn=($this->seguiDemandas($row["IdDemanda"])==1)?"<button class='btn btn-success btn-sm'><span class='glyphicon glyphicon-ok'></span></button>":"";
            $dataModal = $cambiosBtn;
            $dem       = 293;
            $dem       = $Demandas->buscarDemandas1($row["idInm"]);
            //echo $dem;

            $listnegocios[] = array(
                "idInm"        => $row["idInm"],
                "destinacion"  => ucwords(strtolower(getCampo('destinacion', "where IdDestinacion=" . $row["IdDestinacion"], 'Nombre'))),
                "gestion"      => ucwords(strtolower(getCampo('gestioncomer', "where IdGestion=" . $row["IdGestion"], 'NombresGestion'))),
                "tipoInmueble" => ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble=" . $row["IdTpInm"], 'Descripcion'))),
                "ValorVenta"   => "$" . number_format($row["ValorVenta"]),
                "ValorCanon"   => "$" . number_format($row["ValorCanon"]),
                "politica"     => $row["politica_comp"],
                "promotor"     => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $row["IdPromotor"] . "'", 'concat(Nombres," ",apellidos)'))),
                "dem"          => "$dem",
                "btnModal"     => $dataModal,

            );
        } //print_r($listnegocios)."ffff";
        return $listnegocios;
    } //fin function listDemandas()

    public function logsObservaDemandas($idDemanda)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT cons_log
								FROM    lg_ch_tb
								WHERE tipo_log = 1
  								AND client_g = :idDemanda
								ORDER BY cons_log DESC ");

        $stmt->bindParam("idDemanda", $idDemanda);
        $stmt->execute();
        $seguimiento = $stmt->rowCount();
        if ($seguimiento > 0) {
            return 1;
        }
    }
    public function saveTracingDemandas($sDemandas, $idSeguimiento)
    {
        $connPDO = new Conexion();
        $fecha   = date('Y-m-d');
        $user    = $_SESSION['codUser'];
        try {
            $stmt = $connPDO->prepare("INSERT INTO SeguimientoDemandaNvo
												(idSeguimiento,
												 idDemanda,tseguimiento,
												 observacion,resseg,
												 frec,horarec,
												 agendar,agendarc,
												 mailcop,fechaCre,usercrea)
						     		 VALUES 	(:idSeguimiento,
												 :idDemanda,:tseguimiento,
												 :observacion,:resseg,
												 :frec,:horarec,
												 :agendar,:agendarc,
												 :mailcop,:fechaCre,:usercrea)");
            if ($stmt->execute(array(
                ':idSeguimiento' => $idSeguimiento,
                ':idDemanda'     => $sDemandas['idDemanda'],
                ':tseguimiento'  => $sDemandas['tseguimiento'],
                ':observacion'   => $sDemandas['descripcion'],
                ':resseg'        => $sDemandas['resseg'],
                ':frec'          => $sDemandas['frec'],
                ':horarec'       => $sDemandas['hora_ini'],
                ':agendar'       => $sDemandas['agendar'],
                ':agendarc'      => $sDemandas['agendarc'],
                ':mailcop'       => $sDemandas['mailcop'],
                ':fechaCre'      => $fecha,
                ':usercrea'      => $user,
            ))) {
                if ($sDemandas['cerrard'] == '1') {
                    $this->closeDemandas($sDemandas['idDemanda']);
                }
                return 1;
            }
        } catch (PDOException $e) {
            print $e->getMessage();
        }

    }
    public function closeDemandas($cDemandas)
    {
        $connPDO = new Conexion();
        try
        {
            $stmt = $connPDO->prepare("UPDATE demandainmuebles
     				 			 SET Estado 	 = 1
     				             WHERE IdDemanda = :IdDemanda");
            $stmt->bindParam(":IdDemanda", $cDemandas);

            if ($stmt->execute()) {
                return 1;
            }

        } catch (PDOException $e) {
            print $e->getMessage();
        }

    }
    public function seguimientoDemandas($seDemandas)
    {
        $connPDO = new Conexion();

        try
        {
            $stmt = $connPDO->prepare("SELECT  idSeguimiento,
											idDemanda,
     										 tseguimiento,
     										 observacion,
     										 resseg,fechaCre,
     										 horarec,agendar,
     										 agendarc,mailcop,frec
									 FROM SeguimientoDemandaNvo
									 WHERE  idDemanda=:idDemanda
									 order by idSeguimiento desc");
            $stmt->bindParam(":idDemanda", $seDemandas);
            $stmt->execute();
            $datas = array();
            while ($row = $stmt->fetch()) {
                $nomTseguimiento = getCampo('parametros', "where id_param=20 and conse_param=" . $row['tseguimiento'], 'desc_param');
                $reSeguimiento   = getCampo('parametros', "where id_param=10 and conse_param=" . $row['resseg'], 'desc_param');
                $copyCliente     = getCampo('parametros', "where id_param=3 and conse_param=" . $row['agendarc'], 'desc_param');
                $fechaRec        = ($row['frec'] != '0000-00-00') ? $row['frec'] . " <br>" . $row['horarec'] : "NO";

                $datas[] = array(
                    'tiposeguimiento' => $nomTseguimiento,
                    'observacion'     => $row["observacion"],
                    'resultado'       => $reSeguimiento,
                    'recordatorio'    => $fechaRec,
                    'copiarCliente'   => $copyCliente,
                    'fechaSegui'      => $row["fechaCre"],
                );
            }
            return $datas;
            // echo $seDemandas;
        } catch (PDOException $e) {
            print $e->getMessage();
        }
    }
    public function logs_CruceDeNegocios($logsDemandas)
    {
        $connPDO = new Conexion();

        try {
            $stmt = $connPDO->prepare("SELECT cons_log,
 										 	tipo_log,
 										 	campo_g,
 										 	esta_ini_g,
 										 	esta_fin_g,
 										 	fecha_g,
 										 	hora_g,
 										 	usuario_g,
 										 	ip_g,
 										 	client_g,
 										 	campo_usu_g,
 										 	param_g
									FROM    lg_ch_tb
									WHERE tipo_log = 1
  									AND client_g = :idDemanda
									ORDER BY cons_log DESC ");

            $stmt->bindParam(":idDemanda", $logsDemandas["idDemanda"]);

            $stmt->execute();
            $data = array();
            while ($row = $stmt->fetch()) {
                $campo = $row['campo_g'];

                $estado_ini  = $row['esta_ini_g'];
                $estado_fin  = $row['esta_fin_g'];
                $fecha       = $row['fecha_g'];
                $hora        = $row['hora_g'];
                $usuario     = $row['usuario_g'];
                $ip_g        = $row['ip_g'];
                $campo_usu_g = $row['campo_usu_g'];
                $param_g     = $row['param_g'];
                $nomUsuario  = ucwords(strtolower(getCampo('usuarios',
                    "where Id_Usuarios='$usuario'",
                    'concat(Nombres," ",apellidos)')));
                $nomTabla = ucwords(strtolower(getCampo('parametros',
                    "WHERE id_param=17 and conse_param=1",
                    'desc_param')));
                if ($campo == "grupo") {
                    $dataIni = getCampo('parametros',
                        "WHERE id_param=3 and conse_param='" . $estado_ini . "'",
                        'desc_param');
                    $dataFin = getCampo('parametros',
                        "WHERE id_param=3 and conse_param='" . $estado_fin . "'",
                        'desc_param');
                } elseif ($campo == "IdPromotor") {
                    $dataIni = getCampo('usuarios',
                        "where Id_Usuarios='$estado_ini'",
                        'concat(Nombres," ",apellidos)');

                    $dataFin = getCampo('usuarios',
                        "where Id_Usuarios='$estado_fin'",
                        'concat(Nombres," ",apellidos)');
                } elseif ($campo == "estado_dem") {
                    $dataIni = getCampo('parametros',
                        "WHERE id_param=16 and conse_param='" . $estado_ini . "'",
                        'desc_param');
                    $dataFin = getCampo('parametros',
                        "WHERE id_param=16 and conse_param='" . $estado_fin . "'",
                        'desc_param');
                } elseif ($campo == "IdTipoInm") {
                    $dataIni = getCampo('tipoinmuebles',
                        "WHERE idTipoInmueble='" . $estado_ini . "'"
                        , 'CONCAT(UPPER(LEFT(Descripcion, 1)), LOWER(SUBSTRING(Descripcion, 2)))');
                    $dataFin = getCampo('tipoinmuebles',
                        "WHERE idTipoInmueble='" . $estado_fin . "'"
                        , 'CONCAT(UPPER(LEFT(Descripcion, 1)), LOWER(SUBSTRING(Descripcion, 2)))');
                } elseif ($campo == "IdDestinacion") {
                    $dataIni = getCampo('destinacion',
                        "WHERE Iddestinacion='" . $estado_ini . "'"
                        , 'CONCAT(UPPER(LEFT(Nombre, 1)), LOWER(SUBSTRING(Nombre, 2)))');
                    $dataFin = getCampo('destinacion',
                        "WHERE Iddestinacion='" . $estado_fin . "'"
                        , 'CONCAT(UPPER(LEFT(Nombre, 1)), LOWER(SUBSTRING(Nombre, 2)))');
                } elseif ($campo == 'IdGestion') {
                    $dataIni = getCampo('gestioncomer',
                        "WHERE IdGestion='" . $estado_ini . "'",
                        'CONCAT(UPPER(LEFT(NombresGestion, 1)), LOWER(SUBSTRING(NombresGestion, 2)))');

                    $dataFin = getCampo('gestioncomer', "WHERE IdGestion='" . $estado_fin . "'"
                        , 'CONCAT(UPPER(LEFT(NombresGestion, 1)), LOWER(SUBSTRING(NombresGestion, 2)))');
                } elseif ($campo == 'ValInicial' or $campo == 'ValFinal'
                    or $campo == 'AreaInicial' or $campo == 'AreaFin') {
                    $dataIni = number_format($estado_ini);
                    $dataFin = number_format($estado_fin);
                } elseif ($campo == 'barrio') {
                    $dataIni = getCampo('barrios', "WHERE idBarrios='" . $estado_ini . "'",
                        'CONCAT(UPPER(LEFT(NombreB, 1)), LOWER(SUBSTRING(NombreB, 2)))');
                    $dataFin = getCampo('barrios',
                        "WHERE idBarrios='" . $estado_fin . "'",
                        'CONCAT(UPPER(LEFT(NombreB, 1)), LOWER(SUBSTRING(NombreB, 2)))');
                } else {
                    $dataIni = $estado_ini;
                    $dataFin = $estado_fin;
                }
                //          $fecham="<ul>
                // <li><b>Fecha:</b>$fecha<b>Hora:</b>$hora</li>

                //                  </ul>";
                //          $lista="<ul>
                // <li><b>Ip:</b>$ip_g</li>
                // <li><b>Usuario:</b>$nomUsuario</li>
                //                  </ul>";
                $data[] = array(
                    "nomCapo"    => $campo_usu_g,
                    "estInicial" => $dataIni,
                    "estFinal"   => $dataFin,
                    "fecha"      => "<b></b>" . $fecha . "<b>--</b>" . $hora,
                    "ipuser"     => "<b>Ip:</b>" . $ip_g,
                    "usuario"    => $nomUsuario,
                );
            }
            return $data;

        } catch (PDOException $e) {
            print $e->getMessage();
        }
    }
    // public function aplicaDemandas($idDemanda)
    // {
    //     $connPDO= new Conexion();
    //     try {

    //         $stmt=$connPDO->prepare("SELECT *
    //                                 FROM demandainmuebles i
    //                                 WHERE i.IdDemanda = :idDemanda");
    //         $stmt->bindParam(":idDemanda",$idDemanda['idDemanda']);
    //         if($stmt->execute()){
    //             while($row = $stmt->fetch())
    //             {
    //                 $FechaD=$row['FechaD'];
    //              $IdPromotor=$row['IdPromotor'];
    //              $FechaLimite=$row['FechaLimite'];
    //              $CedulaIInt=$row['CedulaIInt'];
    //              $Calle=$row['Calle'];
    //              $AlaCalle=$row['AlaCalle'];
    //              $Carrera=$row['Carrera'];
    //              $AlaCarrera=$row['AlaCarrera'];
    //              $IdGestion=$row['IdGestion'];
    //              $IdDestinacion=$row['IdDestinacion'];
    //              $IdTipoInm=$row['IdTipoInm'];
    //              $ValInicial=$row['ValInicial'];
    //              $ValFinal=$row['ValFinal'];
    //              $AreaInicial=$row['AreaInicial'];
    //              $AreaFinal=$row['AreaFinal'];
    //              $Descripcion=$row['Descripcion'];
    //              $IdCiudad=$row['IdCiudad'];
    //              $IdBarrio=$row['IdBarrio'];
    //              $IdInmobiliaria=$row['IdInmobiliaria'];
    //              $linkvideo=$row['linkvideo'];
    //              if($IdGestion==2)
    //              {
    //                  $condG="1,2,5";
    //              }
    //              else
    //              {
    //                  $condG="$IdGestion,2";
    //              }
    //             }
    //         }
    //         else
    //         {
    //             print $e->getMessage();
    //         }
    //     } catch (PDOException $e) {
    //     print $e->getMessage();
    //     }
    // }
    public function datosDemandas($iddem)
    {
        //Danny
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT IdDemanda,IdPromotor,FechaLimite,Calle,
					AlaCalle,Carrera,AlaCarrera,IdGestion,IdDestinacion,
					IdTipoInm,ValInicial,ValFinal,AreaInicial,AreaFinal,
					Descripcion,Estado,IdBarrio,grupo_dem,CedulaIInt
        FROM demandainmuebles i
        WHERE i.IdDemanda = :ideman");
        $stmt->bindParam(':ideman', $iddem);
        $stmt->execute();
        //$stmt->debugDumpParams();
        $listnegocios = array();
        while ($row = $stmt->fetch()) {

            $listnegocios[] = array(
                "IdDemanda"     => $row["IdDemanda"],
                "IdPromotor"    => $row["IdPromotor"],
                "Promotor"      => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $row["IdPromotor"] . "'", 'concat(Nombres," ",apellidos)'))),
                "FechaLimite"   => $row["FechaLimite"],
                "Calle"         => $row["Calle"],
                "AlaCalle"      => $row["AlaCalle"],
                "Carrera"       => $row["Carrera"],
                "AlaCarrera"    => $row["AlaCarrera"],
                "Gestion"       => ucwords(strtolower(getCampo('gestioncomer', "where IdGestion='" . $row["IdGestion"] . "'", 'NombresGestion'))),
                "IdGestion"     => $row["IdGestion"],
                "Destinacion"   => ucwords(strtolower(getCampo('destinacion', "where Iddestinacion='" . $row["IdDestinacion"] . "'", 'Nombre'))),
                "IdDestinacion" => $row["IdDestinacion"],
                "TipoInm"       => ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble=" . $row["IdTipoInm"], 'Descripcion'))),
                "IdTipoInm"     => $row["IdTipoInm"],
                "ValInicial"    => $row["ValInicial"],
                "ValFinal"      => $row["ValFinal"],
                "AreaInicial"   => $row["AreaInicial"],
                "AreaFinal"     => $row["AreaFinal"],
                "Descripcion"   => $row["Descripcion"],
                "Estado"        => ucwords(strtolower(getCampo('parametros', "WHERE id_param=16 and conse_param='" . $row["Estado"] . "'", 'desc_param'))),
                "Barrio"        => ucwords(strtolower(getCampo('barrios', "WHERE IdBarrios=" . $row["IdBarrio"], 'NombreB'))),
                "IdBarrio"      => $row["IdBarrio"],
                "grupo_dem"     => $row["grupo_dem"],
                "CedulaIInt"    => ucwords(strtolower(getCampo('clientes_inmobiliaria', "WHERE cedula='" . $row["CedulaIInt"] . "'", 'nombre'))),

            );
        } //print_r($listnegocios)."ffff";
        return $listnegocios;
    }

    public function aplicaDemandas($datos, $migrupo = '', $inmoAct = '')
    {
        //Danny

        $connPDO = new Conexion();
        //$iddem =$_POST[];
        $Demandas = new Negocios();
        //echo $datos."---------------";
        $info = $Demandas->datosDemandas($datos);
        /*echo '<pre>';
        print_r($info);
        echo '</pre>';*/
        foreach ($info as $key => $data)
//echo "esto es carrera ".$data['Carrera']."fffff <br>";

        {
            $cond = "";
        }

        if ($data['AreaInicial'] && $data['AreaFinal']) {
            $cond .= " and i.AreaConstruida BETWEEN :AreaInicial AND :AreaFinal";
        }
        if ($data['Calle'] && $data['AlaCalle']) {
            $cond .= " and i.Calle BETWEEN :Calle AND :AlaCalle";
        }
        if ($data['Carrera'] && $data['AlaCarrera']) {
            $cond .= " and i.Carrera BETWEEN :Carrera AND :AlaCarrera";
        }

        //echo $data['IdTipoInm']."------------<br>";
        if ($data['IdTipoInm']) {
            $cond .= " and i.IdTpInm=:IdTpInm";
        }
        if ($data['IdDestinacion']) {
            $cond .= " and i.IdDestinacion=:IdDestinacion";
        }
        /*if($data['IdPromotor'])
        {
        $cond .=" and i.IdPromotor=:IdPromotor";
        }*/

        if ($data['IdGestion'] == 5) {
            $campo = 'ValorVenta';
        } else {
            $campo = 'ValorCanon';
        }
        if ($data['ValInicial'] && $data['ValFinal']) {
            $cond .= " AND i.$campo BETWEEN :ValInicial AND :ValFinal";
        }

        if ($data['IdBarrio']) {
            $cond .= " and i.IdBarrio=:IdBarrio";
        }

        if ($data['idInm']) {
            //echo "ingresa a codDemanda ".$_GET['codDemanda'];
            $cond .= " and i.idInm=:idInm";
        }

        if ($data['perfil'] == 3 or $data['perfil'] == 10) {

        }

        ////condicion que verifica los grupos existentes y omite la condicion para inmobiliarias TAE
        if ($inmoAct != 15555000 and $inmoAct != 1000 and $inmoAct != 10000 and $inmoAct != 1000000) {
            if (!empty($migrupo)) {
                $cond .= " AND i.IdInmobiliaria  IN ($migrupo)";
            }

        }

        $hoy = date("Y-m-d");

        $stmt = $connPDO->prepare("SELECT idInm,IdGestion,IdDestinacion,ValorVenta,ValorCanon,
		 			IdPromotor,IdTpInm,IdPromotor,linkvideo,IdInmobiliaria,
					Calle,Carrera,ValorCanon,ValorVenta,Estrato,
					AreaLote,AreaConstruida,descripcionlarga,EdadInmueble,Administracion,IdBarrio
					FROM inmuebles i
					WHERE i.idEstadoinmueble=2
					#and i.politica_comp!=''
					$cond");

        if ($data['AreaInicial'] && $data['AreaFinal']) {
            $stmt->bindParam(":AreaInicial", $data['AreaInicial']);
            $stmt->bindParam(":AreaFinal", $data['AreaFinal']);
        }

        if ($data['Calle'] && $data['AlaCalle']) {
            $stmt->bindParam(":Calle", $data['Calle']);
            $stmt->bindParam(":AlaCalle", $data['AlaCalle']);
        }
        if ($data['Carrera'] && $data['AlaCarrera']) {
            $stmt->bindParam(":Carrera", $data['Carrera']);
            $stmt->bindParam(":AlaCarrera", $data['AlaCarrera']);
        }
        if ($data['IdDestinacion']) {
            $stmt->bindParam(':IdDestinacion', $data["IdDestinacion"]);
        }
        if ($data['IdTipoInm']) {
            $stmt->bindParam(":IdTpInm", $data["IdTipoInm"]);
        }
        /*if($data['IdPromotor'])
        {
        $stmt->bindParam(":IdPromotor",$data["IdPromotor"]);
        }*/
        if ($data['ValInicial'] && $data['ValFinal']) {
            $stmt->bindParam(":ValInicial", $data['ValInicial']);
            $stmt->bindParam(":ValFinal", $data['ValFinal']);
        }
        if ($data['IdBarrio']) {
            $stmt->bindParam(":IdBarrio", $data['IdBarrio']);
        }
        if ($data['idInm']) {
            //echo "ingresa a codDemanda ".$_GET['codDemanda'];
            $stmt->bindParam(":idInm", $data["idInm"]);
        }
        if ($data['IdGestion']) {
            if ($data['IdGestion'] != 2) {
                $cond .= " and i.IdGestion=:IdGestion";
            } else {
                $cond .= " and i.IdGestion != 3";
            }
        }

        if ($data['perfil'] == 3 or $data['perfil'] == 10) {

        }

        $stmt->execute();

        $listnegocios = array();
        $existe       = $stmt->rowCount();
        //echo "<br>".$existe."<br>";
        while ($row = $stmt->fetch()) {
            ///boton para descartar demanda
            $descartarBtn = ($row["RESULTADO"] == 1) ? "" : "<button class='btn btn-danger btn-sm dct' data-toggle='modal' data-target='#modal1-id' title='Descartar Demanda'><span class='glyphicon fa fa-times-circle'></span></button>";

            $negociarBtn    = ($row["RESULTADO"] == 0) ? "" : "<button class='btn btn-success btn-sm' 	><span class='glyphicon fa fa-check-square-o'></span></button>";
            $editarBtn      = "<button class='btn btn-warning btn-sm dct1' title='Buscar Demanda' data-toggle='modal' data-target='#modal2-id'><span class='fa fa-search'></span></button>";
            $cambiosBtn     = "<button class='btn btn-info btn-sm' toggle='modal' data-target='#modal-id'><span class='glyphicon glyphicon-eye-open'></span></button>";
            $seguimientoBtn = "<button class='btn btn-danger btn-sm'><span class='fa fa-sign-in'></span></button>";
            // $ofertabtn=($this->seguiDemandas($row["IdDemanda"])==1)?"<button class='btn btn-success btn-sm'><span class='glyphicon glyphicon-ok'></span></button>":"";
            $dataModal   = $descartarBtn . $negociarBtn . $editarBtn . $cambiosBtn . $seguimientoBtn;
            $tipoinm     = ($row["IdTipoInm"] != "") ? "<li>Gestión:" . ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble='" . $row["IdTipoInm"], 'Descripcion'))) . "</li>" : "";
            $barr        = ($row["IdBarrio"] != "") ? "<li>Barrio:" . ucwords(strtolower(getCampo('barrios', "WHERE IdBarrios=" . $row["IdBarrio"], 'NombreB'))) . "</li>" : "";
            $edad        = ($row["EdadInmueble"] != "") ? "<li>Edad:" . $row["EdadInmueble"] . "</li>" : "";
            $Estrato     = ($row["Estrato"] != 0) ? "<li>Estrato:" . $row["Estrato"] . "</li>" : "";
            $Canon       = ($row["ValorCanon"] != 0) ? "<li>Canon: $" . number_format($row["ValorCanon"]) . "</li>" : "";
            $Ventas      = ($row["ValorVenta"] != 0) ? "<li>Venta $" . number_format($row["ValorVenta"]) . "</li>" : "";
            $Admon       = ($row["Administracion"] != 0) ? "<li>Admon $" . number_format($row["Administracion"]) . "</li>" : "";
            $idInm       = $row["idInm"];
            $existe      = $Demandas->validar_acepta($idInm);
            $gestion     = ($row["IdGestion"] != "") ? "<li>Gestion:" . ucwords(strtolower(getCampo('gestioncomer', "where IdGestion='" . $row["IdGestion"] . "'", 'NombresGestion'))) . "</li>" : "";
            $logoAA      = "<img style='max-width:150px; height:auto; ' src='http://www.simiinmobiliarias.com/logos/" . str_replace('../logos/', '', getCampo('clientessimi', "where IdInmobiliaria=" . $row["IdInmobiliaria"], 'logo')) . "' alt=Inmobiliaria'  border='0' class='manNegocios' />";
            $botonAcepta = "";

            if ($existe > 0) {
                $botonAcepta = "<img style='max-width:80px; height:auto;' src='http://www.simiinmobiliarias.com/mcomercialweb/images/cruce de manos.jpg' class='negociosbtn' alt=Negocios'  border='0' />";
            }
            $idInmu               = $row["idInm"];
            $listaCaracteristicas = "<ul>
									<li>Cod-Inmueble <span class='label label-success'>$idInmu</span></li>
										$gestion
										$tipoinm
										$barr
										$edad
										$Estrato
										$Canon
										$Ventas
										$Admon
								   </ul>";
            $inmu  = explode("-", $idInmu);
            $ficha = "<input type='hidden' value='$idInmu' class='idInmu'><button class='btn btn-info btn-sm btn-block fichapdf'  ><span class='glyphicon glyphicon-eye-open '></span> Ver ficha</button>";

            $video = ($row["linkvideo"] != "") ? "<a href='" . $row["linkvideo"] . "' target='_blank'><button type='button' class='btn btn-danger btn-block'><span class='fa fa-youtube'></span></button></a>" : "";

            $listnegocios[] = array(

                "IdPromotor"     => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $row["IdPromotor"] . "'", 'concat(Nombres," ",apellidos'))),
                "Calle"          => $row["Calle"],
                "Carrera"        => $row["Carrera"],
                "linkvideo"      => $video,
                "inmobiliaria"   => ucwords(strtolower(getCampo('clientessimi', "where IdInmobiliaria='" . $row["IdInmobiliaria"] . "'", 'Nombre'))),
                "logo"           => $logoAA,
                "IdDestinacion"  => ucwords(strtolower(getCampo('destinacion', "where Iddestinacion='" . $row["IdDestinacion"] . "'", 'Nombre'))),
                "listaC"         => $listaCaracteristicas,
                "AreaConstruida" => number_format($row["AreaConstruida"]),
                "AreaLote"       => number_format($row["AreaLote"]),
                "Descripcion"    => $row["descripcionlarga"],
                "botonFicha"     => $ficha,
                "botonAcepta"    => $botonAcepta,
            );
        } //print_r($listnegocios)."ffff";
        return $listnegocios;
    }
    public function validar_acepta($idInm)
    {
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("select id_inmu_cm
			from condiciones_compartir_inm
			where id_inmu_cm=:idInm");

        $stmt->bindParam(':idInm', $idInm);
        $stmt->execute();
        $existe = $stmt->rowCount();

        return $existe;
    }
    public function buscarDemandas1($codinmu, $migrupo = '')
    {
        //include('../mcomercialweb/grupoinmobiliaria.class.php');
        //include('../mcomercialweb/emailtae2.class.php');
        //echo "$migrupo esto es mi grupo";
        $f_actual = date('Y-m-d');
        $Demandas = new Negocios();
        ///boton para descartar demanda
        $descartarBtn = "<button class='btn btn-xs btn-danger btn-sm dct' data-toggle='modal' data-target='#modal1-id' title='Descartar Demanda'><span class='glyphicon fa fa-times-circle'></span></button> ";

        ///boton para info de la demanda
        //$FichaDemBtn="<button class='btn btn-xs btn-info btn-sm infodemanda' data-toggle='modal' data-target='#infoDemanda'  title='Ficha Demanda'><span class='glyphicon fa fa-search'></span></button><input type='hidden' id='ddem' value='".$codinmu."'> ";
        $FichaDemBtn = "<button class='btn btn-info btn-xs editde' data-toggle='modal' data-target='#modal-deman' title='Ficha Demanda' ><span class='fa fa-search'></span></button>";

        if ($control == 1) {
            global $f_actual, $w_conexion;
        } else {
            $w_conexion = new MySQL();
            global $f_actual;
        }

        list($aa, $codinm2) = explode("-", $codinmu);
        $condicionInmob1    = "";
        if ($_SESSION['IdInmmo'] != 1 and $_SESSION['IdInmmo'] != 1000 and $_SESSION['IdInmmo'] != 10000 and $_SESSION['IdInmmo'] != 1000000) {
            if (!empty($migrupo)) {
                $condicionInmob1 .= " AND d.IdInmobiliaria  IN ($migrupo)";
            } else if (!empty($otrosgrupos)) {
                $condicionInmob1 .= " AND d.IdInmobiliaria  NOT IN ($otrosgrupos)";
            } else {
                $condicionInmob1 .= " AND d.IdInmobiliaria  IN ($aa)";
            }
        }

        $condSimi = "and d.IdInmobiliaria not in (1,1000,1000000)";
        if ($_SESSION['IdInmmo'] == 1) {
            $condSimi = '';
        }

        $validaGlobal = 0;
        $flag         = 0;
        $varTexto     = "";
        $conCiud      = 0;

        /////descartar demandas para un inmueble debe tener resultado 1 y ser tipo 2 demandas a inmuebles
        $cade             = "";
        $condBloqueo      = "";
        $descartademandas = "SELECT cod_dem
			FROM descarta_demandas
			WHERE  inmu_dem='$codinmu'
			and res_dem=1
			and tp_dem=2";

        //echo $aplicademandas."<hr>";
        $res4    = $w_conexion->ResultSet($descartademandas);
        $existe4 = $w_conexion->FilasAfectadas($res4);
        if ($existe4 > 0) {
            while ($ff = $w_conexion->FilaSiguienteArray($res4)) {
                $cod_dem = $ff['cod_dem'];
                $cade    = $cod_dem . "," . $cade;
            }
            $cade = substr($cade, 0, -1);
            $condBloqueo .= "and IdDemanda not in ($cade) ";
        }

        $aplicademandas = "SELECT *
			FROM demandainmuebles d
			WHERE  d.Estado=0
			and FechaLimite>='$f_actual'
			$condBloqueo
			$condicionInmob1";

        //echo $aplicademandas."<hr>";
        $res3    = $w_conexion->ResultSet($aplicademandas);
        $existe3 = $w_conexion->FilasAfectadas($res3);
        //echo $existe3." existe";

        if ($existe3 > 0) {
            $varTexto .= "<ul>";
            while ($fila77 = $w_conexion->FilaSiguienteArray($res3)) {

                $IdDemanda      = $fila77['IdDemanda'];
                $IdGestion      = $fila77['IdGestion'];
                $IdTpInm        = $fila77['IdTipoInm'];
                $IdBarrio       = $fila77['IdBarrio'];
                $ValorVenta     = $fila77['ValorVenta'];
                $ValorCanon     = $fila77['ValorCanon'];
                $Administracion = $fila77['Administracion'];
                $Estrato        = $fila77['Estrato'];
                $politica_comp  = $fila77['politica_comp'];
                $IdDestinacion  = $fila77['IdDestinacion'];
                $AreaConstruida = $fila77['AreaConstruida'];
                $AreaLote       = $fila77['AreaLote'];
                $Carrera        = $fila77['Carrera'];
                $Calle          = $fila77['Calle'];
                $AlaCalle       = $fila77['AlaCalle'];
                $AlaCarrera     = $fila77['AlaCarrera'];
                $ValInicial     = $fila77['ValInicial'];
                $ValFinal       = $fila77['ValFinal'];
                $AreaInicial    = $fila77['AreaInicial'];
                $AreaFinal      = $fila77['AreaFinal'];
                $IdCiudad       = $fila77['IdCiudad'];
                $IdBarrio       = $fila77['IdBarrio'];
                $IdInmobiliaria = $fila77['IdInmobiliaria'];

                // echo "$IdDemanda $IdCiudad <br>";
                if ($IdGestion == 2) {
                    $condG = "1,2,5";
                } else {
                    $condG = "$IdGestion,2";
                }
                if ($IdTpInm == 1 or $IdTpInm == 11) {
                    $tpInm = "1,11";
                } else {
                    $tpInm = $IdTpInm;
                }
                //echo "$IdDemanda --- $tpInm=$IdTpInm <br>";
                //echo "$IdCiudad $IdDemanda  **  $tpInm,$condG,$Calle,$AlaCalle,$Carrera,$AlaCarrera,$IdDestinacion,$ValorVenta,$ValorCanon | $IdBarrio |,$AreaInicial,$AreaFinal,1,<br><br>";

                $conTodos = $Demandas->aplicaDemanda($codinmu, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $AlaCalle, $Carrera, $AlaCarrera, $IdDestinacion, $ValInicial, $ValFinal, $IdBarrio, $AreaInicial, $AreaFinal, 1, '', 0);
                //echo $conTodos."<br>------------------------------<br>";
                $conBarrio = $Demandas->aplicaDemanda($codinmu, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $AlaCalle, $Carrera, $AlaCarrera, $IdDestinacion, $ValInicial, $ValFinal, $IdBarrio, $AreaInicial, $AreaFinal, 2, $conTodos);
                //echo $conBarrio." barr<br>------------------------------<br>";
                $conArea = $Demandas->aplicaDemanda($codinmu, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $AlaCalle, $Carrera, $AlaCarrera, $IdDestinacion, $ValInicial, $ValFinal, $IdBarrio, $AreaInicial, $AreaFinal, 3, $conTodos);
                //echo $conArea." area<br>------------------------------<br>";
                $conDestinacion = $Demandas->aplicaDemanda($codinmu, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $AlaCalle, $Carrera, $AlaCarrera, $IdDestinacion, $ValInicial, $ValFinal, $IdBarrio, $AreaInicial, $AreaFinal, 4, $conTodos, 10);
                $conDir         = $Demandas->aplicaDemanda($codinmu, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $AlaCalle, $Carrera, $AlaCarrera, $IdDestinacion, $ValInicial, $ValFinal, $IdBarrio, $AreaInicial, $AreaFinal, 5, $conTodos);
                //echo $conDestinacion." dest<br>------------------------------<br>";
                if ($conTodos == 1) {
                    //echo "<br>La demanda $IdDemanda cumple al 100% <br>";
                    $varTexto .= "<li> <button title='Aplicar a la Demanda' type='button' id='aplicarDem' class='btn btn-xs btn-primary aplicarDem'  data-toggle='modal'  data-target='#modal-negos'><span class='fa fa-check-square-o'></span></button>" . $descartarBtn . $FichaDemBtn . " La demanda $IdDemanda cumple al 100% <input type='hidden' class='c_demd' value='" . $IdDemanda . "' ><input type='hidden'  class='c_inmu' value='" . $codinmu . "' > <i class='fa fa-star' title='Calificacion de la Oportunidad' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                    $flag = 1;
                }
                $ciudOr = getCampo('barrios b,inmuebles i', "where b.IdBarrios=i.IdBarrio and i.idInm='" . $codinmu . "'", 'b.IdCiudad', 0);
                //echo "ciudOr $ciudOr <br>";
                //echo "$IdDemanda $conTodos==todos and $conBarrio==barrio and $conArea==area and $conDestinacion==destinacion $conDir==dir <br>";
                if ($conTodos == 0 and $conBarrio == 0 and $conArea == 0 and $conDestinacion == 0 and $IdCiudad == 0 and $conDir == 0) {
                    //echo "$IdDemanda no coincide";

                }

                if ($conTodos == 0 and ($conBarrio == 1 or $conArea == 1 or $conDestinacion == 1 or $conDir == 1)) // or $IdCiudad==$ciudOr))
                {
                    if ($IdCiudad == $ciudOr) {$conCiud = 1;} else { $conCiud = 0;}
                    $var1         = "";
                    $var2         = "";
                    $var3         = "";
                    $var4         = "";
                    $validaGlobal = $conBarrio + $conArea + $conDestinacion + $conCiud + $conDir; //echo "<br>La demanda $IdDemanda ";
                    $varTexto .= '<li> <button title="Aplicar a la Demanda" type="button" id="aplicarDem" class="btn btn-xs btn-primary aplicarDem"  data-toggle="modal"  data-target="#modal-negos"><span class="fa fa-check-square-o"></span></button>' . $descartarBtn . $FichaDemBtn . 'La demanda ' . $IdDemanda . '<input type="hidden" class="c_demd" value="' . $IdDemanda . '" > <input type="hidden"  class="c_inmu" value="' . $codinmu . '" >';
                    if ($conBarrio == 1) {$var1 = " Barrio, ";} else { $var1 = "";}
                    if ($conArea == 1) {$var2 = " Area, ";} else { $var2 = "";}
                    if ($conDestinacion == 1) {$var3 = " Destinacion, ";} else { $var3 = "";}
                    if ($conCiud > 0) {$var4 = " Ciudad, ";} else { $var4 = "";}
                    if ($conDir == 1) {$var5 = " Direccion, ";} else { $var3 = "";}
                    //echo "<table class='TablaDatos' width='80%' align='center'><tr><td class='tituloFormulario'>Demandas Coincidentes</td></tr> <tr><td>";
                    if ($validaGlobal == 1) { //echo "Coincide en Valor, Tipo de Inmueble, Gestion,  $var1 $var2 $var3 $var4 $var5 Cumple al 44.4%<br>";
                        //$varTexto = $varTexto."Coincide en Tipo de Inmueble, Gestion,  $var1 $var2 $var3 $var4 $var5 Cumple al 44.4% <i class='fa fa-star' style='color:#ec6459'></i></li>"; $flag++;
                    }

                    if ($validaGlobal == 2) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 55.5%<br>";
                        $varTexto = $varTexto . "Coincide en Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 55.5% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                        $flag++;}

                    if ($validaGlobal == 3) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 66.6%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 66.6% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                        $flag++;}

                    if ($validaGlobal == 4) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 77.7%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 77.7% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                        $flag++;}

                    if ($validaGlobal == 5) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                        $flag++;}
                    //echo "</td></tr></table>";
                    //$flag=1;

                }
                // echo "Entra a alguna $IdDemanda  - $flag";
            } //while
            //echo $flag."flag";
            $varTexto .= "</ul>";
            if ($flag > 0) {
                //echo "---".$varTexto."---";
                //$envioemail = new EmailTae2();
                //$envioemail->EnvioEmailDemandaL($varTexto,$codinmu);
                //$enviado= $envioemail->ResultadoCorreoDemadaLocalizada();
                //echo $varTexto."--------------";
                return $varTexto;
            }
        } else {
            //echo "No Hay demandas coincidentes";
        }
    }
    //////////para enviar correo busco la demanda con la fecha mas antigua

    public function aplicaDemanda($codinmu, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $AlaCalle, $Carrera, $AlaCarrera, $IdDestinacion, $ValInicial, $ValFinal, $IdBarrio, $AreaInicial, $AreaFinal, $evaluar, $completos, $mostrar = '')
    {
        $w_conexion = new MySQL();
        global $f_actual;
        $condicionInmob = "";
        $cond2          = "";

        if ($_SESSION['IdInmmo'] == 1 or $_SESSION['IdInmmo'] == 1000 or $_SESSION['IdInmmo'] == 10000 or $_SESSION['IdInmmo'] == 1000000) {
            $condicionInmob = "";
        } else {
            $condicionInmob = "and i.idInmobiliaria not in (1,1000,10000,100000)";
        }

        if ($evaluar == 1) {
            if ($Calle > 0) {$cond2 .= " AND (i.Calle between '$Calle' AND '$AlaCalle')";}
            if ($Carrera > 0) {$cond2 .= " AND (i.Carrera between '$Carrera' AND '$AlaCarrera')";}
            if ($IdDestinacion > 0) {$cond2 .= " AND  i.IdDestinacion = '$IdDestinacion'";}
            if ($ValorCanon > 0) {
                if ($IdGestion == 1 or $IdGestion == 2) {$cond2 .= " AND (i.ValorCanon between '$ValInicial' AND '$ValFinal')";}
            }
            if ($ValorVenta > 0) {
                if ($IdGestion == 5) {$cond2 .= " AND (i.ValorVenta '$ValInicial' AND '$ValFinal')";}
            }
            if (!empty($AreaConstruida)) {$cond2 .= " AND (i.AreaConstruida between '$AreaInicial' AND '$AreaFinal')";}
            //if(!empty($IdCiudad)){$aplicademandas1=$aplicademandas1." AND b.IdCiudad='".$IdCiudai."'";}
            if (!empty($IdBarrio)) {$cond2 .= " AND i.IdBarrio='$IdBarrio'";}
        }
        if ($evaluar == 2) {
            if ($IdBarrio > 0) {$cond2 .= " AND i.IdBarrio='$IdBarrio'";}
            //echo "$ValorCanon -- $IdGestion <br>";
            if ($ValorCanon > 0) {
                if ($IdGestion == 1 or $IdGestion == 2) {$cond2 .= " AND (i.ValorCanon between '$ValInicial' AND '$ValFinal')";}
            }
            if ($ValorVenta > 0) {
                if ($IdGestion == 5) {$cond2 .= " AND (i.ValorVenta '$ValInicial' AND '$ValFinal')";}
            }
        }
        if ($evaluar == 3) {
            if ($AreaConstruida > 0) {$cond2 .= " AND (i.AreaConstruida between '$AreaInicial' AND '$AreaFinal')";}
            if ($ValorCanon > 0) {
                if ($IdGestion == 1 or $IdGestion == 2) {$cond2 .= " AND (i.ValorCanon between '$ValInicial' AND '$ValFinal')";}
            }
            if ($ValorVenta > 0) {
                if ($IdGestion == 5) {$cond2 .= " AND (i.ValorVenta '$ValInicial' AND '$ValFinal')";}
            }
        }
        if ($evaluar == 4) {
            if ($IdDestinacion > 0) {$cond2 .= " AND  i.IdDestinacion = '$IdDestinacion'";}
            if ($ValorCanon > 0) {
                if ($IdGestion == 1 or $IdGestion == 2) {$cond2 .= " AND (i.ValorCanon between '$ValInicial' AND '$ValFinal')";}
            }
            if ($ValorVenta > 0) {
                if ($IdGestion == 5) {$cond2 .= " AND (i.ValorVenta '$ValInicial' AND '$ValFinal')";}
            }
        }
        if ($evaluar == 5) {
            if ($Calle > 0) {$cond2 .= " AND (i.Calle between '$Calle' AND '$AlaCalle')";}
            if ($Carrera > 0) {$cond2 .= " AND (i.Carrera between '$Carrera' AND '$AlaCarrera')";}
            if ($IdDestinacion > 0) {$cond2 .= " AND  i.IdDestinacion = '$IdDestinacion'";}
            if ($ValorCanon > 0) {
                if ($IdGestion == 1 or $IdGestion == 2) {$cond2 .= " AND (i.ValorCanon between '$ValInicial' AND '$ValFinal')";}
            }
            if ($ValorVenta > 0) {
                if ($IdGestion == 5) {$cond2 .= " AND (i.ValorVenta '$ValInicial' AND '$ValFinal')";}
            }
        }
        if ($IdCiudad > 0) {
            $cond2 .= " AND b.IdCiudad ='$IdCiudad'";
        }

        $aplicademandas1 = "SELECT *
				FROM inmuebles i,barrios b
				WHERE i.IdTpInm in ($tpInm)
				AND i.IdGestion in ($condG)
				and i.idInm='$codinmu'
				AND i.IdBarrio=b.IdBarrios
				$cond2
				$condicionInmob";
        //echo $aplicademandas1."<br><br>";
        if ($mostrar == 1) {
            echo $aplicademandas1 . "<br><br>";
        }
        $res = $w_conexion->ResultSet($aplicademandas1);
        {
            $existe = $w_conexion->FilasAfectadas($res);
            //echo $existe." existe";
            if ($existe > 0) {
                return $existe;
            }
        }
    }
    public function descartaDemandas($usr_dem, $inmu_dem, $tp_dem, $res_dem, $inmob_dem, $ip_dem, $hor_dem, $fec_dem, $c_dem)
    {
        ///Danny

        $connPDO = new Conexion();

        try
        {

            $conse = consecutivo('conse_dem', 'descarta_demandas');
            $exito = 0; //"$conse$usr_dem,$inmu_dem,$tp_dem,$res_dem,$inmob_dem,$ip_dem,$hor_dem,$fec_dem";
            $stmt  = $connPDO->prepare("insert into descarta_demandas
			(conse_dem,usr_dem,inmu_dem,tp_dem,res_dem,
			inmob_dem,ip_dem,hor_dem,fec_dem,cod_dem)
			values
			(:conse_dem,:usr_dem,:inmu_dem,:tp_dem,:res_dem,
			:inmob_dem,:ip_dem,:hor_dem,:fec_dem,:cod_dem)");

            if ($stmt->execute(array(
                ':conse_dem' => $conse,
                ':usr_dem'   => $usr_dem,
                ':inmu_dem'  => $inmu_dem,
                ':tp_dem'    => $tp_dem,
                ':res_dem'   => $res_dem,
                ':inmob_dem' => $inmob_dem,
                ':ip_dem'    => $ip_dem,
                ':hor_dem'   => $hor_dem,
                ':fec_dem'   => $fec_dem,
                ':cod_dem'   => $c_dem,
            ))) {
                $exito = 1;
            }

            return $exito;
            // echo $seDemandas;
        } catch (PDOException $e) {
            print $e->getMessage();
        }
    }

    public function buscarInmuebles($coddem, $aa, $tipoGestion, $destinacion, $tipoInmueble, $migrupo = '')
    {
        //include('../mcomercialweb/grupoinmobiliaria.class.php');
        //include('../mcomercialweb/emailtae2.class.php');
        //echo "$migrupo esto es mi grupo";
        $f_actual = date('Y-m-d');
        $Demandas = new Negocios();
        if ($control == 1) {
            global $f_actual, $w_conexion;
        } else {
            $w_conexion = new MySQL();
            global $f_actual;
        }

        $condicionInmob1 = "";
        if ($_SESSION['IdInmmo'] != 1 and $_SESSION['IdInmmo'] != 1000 and $_SESSION['IdInmmo'] != 10000 and $_SESSION['IdInmmo'] != 1000000 and $_SESSION['IdInmmo'] != 6310000) {

            //echo $migrupo;
            if (!empty($migrupo)) {
                $condicionInmob1 .= " AND d.IdInmobiliaria  IN ($migrupo)";
            } else if (!empty($otrosgrupos)) {
                $condicionInmob1 .= " AND d.IdInmobiliaria  NOT IN ($otrosgrupos)";
            } else {
                // $condicionInmob1  .=" AND d.IdInmobiliaria  IN ($aa)";
            }
        }
        /*  echo $migrupo;
        echo "----------- ".$otrosgrupos;*/
        $condSimi = "and d.IdInmobiliaria not in (1,1000,1000000)";
        if ($_SESSION['IdInmmo'] == 1) {
            $condSimi = '';
        }

        $condBloqueo      = "";
        $descartademandas = "SELECT inmu_dem
			FROM descarta_demandas
			WHERE  cod_dem='$coddem'
			and inmob_dem=$aa
			and res_dem=1
			and tp_dem=1";

        // echo $aplicademandas."<br>";
        $res4    = $w_conexion->ResultSet($descartademandas);
        $existe4 = $w_conexion->FilasAfectadas($res4);
//echo $existe4."<br>";
        if ($existe4 > 0) {
            $cont = $existe4 + 1;
            while ($ff = $w_conexion->FilaSiguienteArray($res4)) {
                $inmu_dem = $ff['inmu_dem'];
                $cade     = "'" . $inmu_dem . "'," . $cade . "'";
            }
            if ($existe4 == 1) {
                $cade = substr($cade, 0, -2);
            } else {
                $cade = substr($cade, 0, -$cont);
            }
            $condBloqueo .= "and d.idInm not in ($cade) ";
        }

        $validaGlobal = 0;
        $flag         = 0;
        $varTexto     = "";

        $aplicademandas = "SELECT idInm,IdGestion,IdTpInm,IdBarrio,ValorVenta,
            ValorCanon,Estrato,IdDestinacion,AreaConstruida,AreaLote,
            Carrera,Calle,IdInmobiliaria
			FROM inmuebles d
			WHERE  idEstadoinmueble=2
            #and politica_comp!=''
            and IdTpInm='$tipoInmueble'
			and IdGestion='$tipoGestion'
			and IdDestinacion='$destinacion'
			$condSimi
			$condicionInmob1
			$condBloqueo
            #limit 0,300";

        // echo $aplicademandas."<hr>";
        $res3    = $w_conexion->ResultSet($aplicademandas);
        $existe3 = $w_conexion->FilasAfectadas($res3);
        //echo $existe3." existe";
        $IdGestion = 0;
        $conCiud   = 0;
        if ($existe3 > 0) {
            $varTexto .= "<ul>";
            while ($fila77 = $w_conexion->FilaSiguienteArray($res3)) {
                $idInm          = $fila77['idInm'];
                $IdGestion      = $fila77['IdGestion'];
                $IdTpInm        = $fila77['IdTpInm'];
                $IdBarrio       = $fila77['IdBarrio'];
                $ValorVenta     = $fila77['ValorVenta'];
                $ValorCanon     = $fila77['ValorCanon'];
                $Administracion = $fila77['Administracion'];
                $Estrato        = $fila77['Estrato'];
                $politica_comp  = $fila77['politica_comp'];
                $IdDestinacion  = $fila77['IdDestinacion'];
                $AreaConstruida = $fila77['AreaConstruida'];
                $AreaInicial    = $AreaConstruida;
                $AreaLote       = $fila77['AreaLote'];
                $Carrera        = $fila77['Carrera'];
                $Calle          = $fila77['Calle'];
                $IdInmobiliaria = $fila77['IdInmobiliaria'];
                $IdCiudad       = getCampo('barrios', "where IdBarrios=$IdBarrio", 'IdCiudad');

                // echo "$IdDemanda $IdCiudad <br>";
                if ($IdGestion == 2) {
                    $condG = "1,2,5";
                } elseif ($IdGestion <= 0) {
                    $condG = "0,2";
                } else {
                    $condG = "$IdGestion,2";
                }
                // echo "$idInm IdGestion $IdGestion --- $condG <br>";
                if ($IdTpInm == 1 or $IdTpInm == 11) {
                    $tpInm = "1,11";
                } else {
                    $tpInm = $IdTpInm;
                }
                if ($IdGestion == 1) {
                    $ValInicial = $ValorCanon;
                } else {
                    $ValInicial = $ValorVenta;
                }
                //echo "$IdDemanda --- $tpInm=$IdTpInm <br>";
                //echo "$IdCiudad $IdDemanda  **  $tpInm,$condG,$Calle,$AlaCalle,$Carrera,$AlaCarrera,$IdDestinacion,$ValorVenta,$ValorCanon | $IdBarrio |,$AreaInicial,$AreaFinal,1,<br><br>";

                $conTodos = $Demandas->aplicaInmueble($coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 1, '', 0);
                //echo $conTodos."<br>------------------------------<br>";
                $conBarrio  = $Demandas->aplicaInmueble($coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 2, $conTodos);
                $conBarrio1 = $Demandas->aplicaInmueble($coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 6, $conTodos);
                //echo $conBarrio." barr<br>------------------------------<br>";
                $conArea = $Demandas->aplicaInmueble($coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 3, $conTodos);
                //echo $conArea." area<br>------------------------------<br>";
                $conDestinacion = $Demandas->aplicaInmueble($coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 4, $conTodos, 10);
                $conDir         = $Demandas->aplicaInmueble($coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 5, $conTodos);
                //echo $conDestinacion." dest<br>------------------------------<br>";
                $aplicaI = $Demandas->validaAplicaInmueble($coddem, $idInm);

                $FichaBtn = "<button class='btn btn-xs btn-info btn-sm fichapdf'  data-toggle='modal'   title='Ficha Inmueble'><span class='glyphicon fa fa-search'></span></button><input type='hidden' id='dinmu' value='" . $idInm . "'>";

                if ($aplicaI == 0) {
                    $btn          = "<button title='Aplicar al Inmueble' type='button' id='aplicarInm' class='btn btn-xs btn-primary aplicarInm'  data-toggle='modal'  data-target='#modal-negos'><span class='fa fa-check-square-o'></span></button>";
                    $descartarBtn = "<button class='btn btn-xs btn-danger btn-sm dct' data-toggle='modal' data-target='#descarta-deman' title='Descartar Inmueble'><span class='glyphicon fa fa-times-circle'></span></button> ";
                } else {
                    $btn          = "<button title='En espera de Respuesta' type='button' id='aplicarInm' class='btn btn-xs btn-primary ' disabled ><span class='fa fa-check-square-o'></span></button>";
                    $descartarBtn = "<button class='btn btn-xs btn-danger btn-sm dctd' title='Descartar Inmueble' disabled><span class='glyphicon fa fa-times-circle' ></span></button> ";
                }
                if ($conTodos == 1) {

                    //echo "<br>La demanda $IdDemanda cumple al 100% <br>";

                    $varTexto .= "<li>" . $bnt . $descartarBtn . $FichaBtn . " El Inmueble $idInm cumple al 100% <input type='hidden' class='c_demd' value='" . $idInm . "' ><input type='hidden'  class='c_inmu' value='" . $coddem . "' > <i class='fa fa-star' title='Calificacion de la Oportunidad' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                    $flag = 1;
                }
                $ciudOr = getCampo('demandainmuebles', "where IdBarrio and IdDemanda='" . $coddem . "'", 'IdCiudad', 0);
                //echo "ciudOr $ciudOr <br>";
                //echo "$IdDemanda $conTodos==todos and $conBarrio==barrio and $conArea==area and $conDestinacion==destinacion $conDir==dir <br>";
                if ($conTodos == 0 and $conBarrio == 0 and $conBarrio1 == 0 and $conArea == 0 and $conDestinacion == 0 and $IdCiudad == 0 and $conDir == 0) {
                    //echo "$IdDemanda no coincide";

                }

                if ($conTodos == 0 and ($conBarrio1 == 1 or $conBarrio == 1 or $conArea == 1 or $conDestinacion == 1 or $conDir == 1)) // or $IdCiudad==$ciudOr))
                {
                    if ($IdCiudad == $ciudOr) {$conCiud = 1;} else { $conCiud = 0;}
                    $var1         = "";
                    $var2         = "";
                    $var3         = "";
                    $var4         = "";
                    $var5         = "";
                    $var6         = "";
                    $validaGlobal = $conBarrio + $conBarrio1 + $conArea + $conDestinacion + $conCiud + $conDir; //echo "<br>La demanda $IdDemanda ";
                    if ($validaGlobal > 1) {
                        $varTexto .= '<li>' . $btn . $descartarBtn . $FichaBtn . 'El Inmueble ' . $idInm . '<input type="hidden" class="c_demd" value="' . $idInm . '" > <input type="hidden"  class="c_inmu" value="' . $coddem . '" >';
                    }
                    if ($conBarrio1 == 1) {$var1 = " Barrio, ";} else { $var1 = "";}
                    if ($conBarrio == 1) {$var6 = "";} else { $var6 = "";}
                    if ($conArea == 1) {$var2 = " Area, ";} else { $var2 = "";}
                    if ($conDestinacion == 1) {$var3 = " Destinacion, ";} else { $var3 = "";}
                    if ($conCiud > 0) {$var4 = " Ciudad, ";} else { $var4 = "";}
                    if ($conDir == 1) {$var5 = " Direccion, ";} else { $var3 = "";}
                    //echo "<table class='TablaDatos' width='80%' align='center'><tr><td class='tituloFormulario'>Demandas Coincidentes</td></tr> <tr><td>";
                    if ($validaGlobal == 1) { //echo "Coincide en Valor, Tipo de Inmueble, Gestion,  $var1 $var2 $var3 $var4 $var5 Cumple al 44.4%<br>";
                        // $varTexto = $varTexto."Coincide en Tipo de Inmueble, Gestion,  $var1 $var2 $var3 $var4 $var5 Cumple al 44.4% <i class='fa fa-star' style='color:#ec6459'></i></li>"; $flag++;
                    }

                    if ($validaGlobal == 2) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 55.5%<br>";
                        $varTexto = $varTexto . "Coincide en Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 55.5% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                        $flag++;}

                    if ($validaGlobal == 3) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 66.6%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 66.6% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                        $flag++;}

                    if ($validaGlobal == 4) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 77.7%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 77.7% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                        $flag++;}

                    if ($validaGlobal == 5) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                        $flag++;}
                    if ($validaGlobal == 5) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                        $flag++;}
                    //echo "</td></tr></table>";
                    //$flag=1;

                }
                // echo "Entra a alguna $IdDemanda  - $flag";
            } //while
            //echo $flag."flag";
            $varTexto .= "</ul>";
            if ($flag > 0) {
                //echo "---".$varTexto."---";
                //$envioemail = new EmailTae2();
                //$envioemail->EnvioEmailDemandaL($varTexto,$codinmu);
                //$enviado= $envioemail->ResultadoCorreoDemadaLocalizada();
                //echo $varTexto."--------------";
                return $varTexto;
            }
        } else {
            //echo "No Hay demandas coincidentes";
        }
    }

    public function aplicaInmueble($codinmu, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, $evaluar, $completos, $mostrar = '')
    {
        $w_conexion = new MySQL();
        global $f_actual;
        $condicionInmob = "";
        $cond2          = "";

        if ($_SESSION['IdInmmo'] == 1 or $_SESSION['IdInmmo'] == 1000 or $_SESSION['IdInmmo'] == 10000 or $_SESSION['IdInmmo'] == 1000000) {
            $condicionInmob = "";
        } else {
            $condicionInmob = "and i.idInmobiliaria not in (1,1000,10000,100000)";
        }

        $ValInicial2  = $ValInicial * 0.8;
        $ValInicial3  = $ValInicial * 1.2;
        $AreaInicial1 = $AreaInicial * 0.7;
        $AreaInicial2 = $AreaInicial * 1.3;

        if ($evaluar == 1) {
            if ($Calle > 0) {$cond2 .= " AND (i.Calle <= '$Calle' AND AlaCalle >='$Calle' )";}
            if ($Carrera > 0) {$cond2 .= " AND (i.Carrera <= '$Carrera' AND AlaCarrera>='$Carrera')";}
            if ($IdDestinacion > 0) {$cond2 .= " AND  i.IdDestinacion = '$IdDestinacion'";}
            if ($IdGestion != 2) {
                $cond2 .= " AND ((i.ValInicial<= '$ValInicial2' and ValFinal>='$ValInicial2')
					  OR (i.ValInicial<= '$ValInicial3' AND ValFinal>='$ValInicial3'))";

            }
            if (!empty($AreaInicial)) {$cond2 .= " AND (i.AreaInicial >= '$AreaInicial1' AND AreaFinal <='$AreaInicial2')";}
            //if(!empty($IdCiudad)){$aplicademandas1=$aplicademandas1." AND b.IdCiudad='".$IdCiudai."'";}
            if (!empty($IdBarrio)) {$cond2 .= " AND i.IdBarrio='$IdBarrio'";}
        }
        if ($evaluar == 2) {
            if ($IdGestion != 2) {
                $cond2 .= " AND ((i.ValInicial<= '$ValInicial2' and ValFinal>='$ValInicial2')
					  OR (i.ValInicial<= '$ValInicial3' AND ValFinal>='$ValInicial3'))";
            }

        }
        if ($evaluar == 3) {
            if ($AreaInicial > 0) {
                $cond2 .= " AND (i.AreaInicial >= '$AreaInicial1' AND AreaFinal <='$AreaInicial2')";
            }

        }
        if ($evaluar == 4) {

            if ($AreaInicial > 0) {
                $cond2 .= " AND (i.AreaInicial >= '$AreaInicial1' AND AreaFinal <='$AreaInicial2')";
            }
            if ($IdDestinacion > 0) {$cond2 .= " AND  i.IdDestinacion = '$IdDestinacion'";}
            if ($IdGestion != 2) {
                $cond2 .= " AND ((i.ValInicial<= '$ValInicial2' and ValFinal>='$ValInicial2')
					  OR (i.ValInicial<= '$ValInicial3' AND ValFinal>='$ValInicial3'))";
            }

        }
        if ($evaluar == 5) {
            if ($Calle > 0) {$cond2 .= " AND (i.Calle <= '$Calle' AND AlaCalle >='$Calle')";}
            if ($Carrera > 0) {$cond2 .= " AND (i.Carrera <= '$Carrera' AND AlaCarrera>='$Carrera')";}
            if ($IdDestinacion > 0) {$cond2 .= " AND  i.IdDestinacion = '$IdDestinacion'";}
            if ($IdGestion != 2) {
                $cond2 .= " AND ((i.ValInicial<= '$ValInicial2' and ValFinal>='$ValInicial2')
					  OR (i.ValInicial<= '$ValInicial3' AND ValFinal>='$ValInicial3'))";
            }
            if ($AreaInicial > 0) {
                $cond2 .= " AND (i.AreaInicial >= '$AreaInicial1' AND AreaFinal <='$AreaInicial2')";
            }
        }
        if ($evaluar == 6) {
            if (!empty($IdBarrio)) {$cond2 .= " AND i.IdBarrio='$IdBarrio'";}

        }
        if ($IdCiudad > 0) {
            $cond2 .= " AND i.IdCiudad ='$IdCiudad'";
        }

        // echo "AreaInicial $AreaInicial -<br>";
        $aplicainmuebles1 = "SELECT *
				FROM demandainmuebles i
				WHERE i.IdTipoInm in ($tpInm)
				AND i.IdGestion in ($condG)
				and i.IdDemanda='$codinmu'
				#and IdDemanda=1166
				$cond2
				$condicionInmob
                ";
        // echo $aplicainmuebles1." $evaluar ---------<br><br>";
        if ($mostrar == 1) {
            echo $aplicainmuebles1 . "<br><br>";
        }
        $res = $w_conexion->ResultSet($aplicainmuebles1) or die("error evaluar inm <br>" . mysql_error());
        {
            $existe = $w_conexion->FilasAfectadas($res);
            //echo $existe." existe";
            if ($existe > 0) {
                /* while($ff=$w_conexion->FilaSiguienteArray($res))
                {
                //$idDem=  $ff['idInm'];
                //$idDemT= $idDem.",".$idDemT;
                }*/
                //$idDemT=substr($idDemT,0,-1);
                // echo $existe."ffff";
                return $existe;
            }
        }
    }
//////////
    public function seguimientoSolicitudes($codSolicitud)
    {
        $codSolicitud = $codSolicitud['idcapta'];
        $connPDO      = new Conexion();
        $stmt         = $connPDO->prepare("SELECT  idSeguimiento,
										 idDemanda,
										 tseguimiento,
										 observacion,
		  								 frec,
		  								 horarec,
		  								 agendar,
		  								 agendarc,
		  								 mailcop
								FROM SeguimientoSolicitudNvo
								WHERE  idDemanda='$codSolicitud'
								order by idSeguimiento desc");

        $stmt->execute();
        $data = array();
        while ($row = $stmt->fetch()) {
            $tipoSeguimiento = ucwords(strtolower(getCampo('parametros', "where id_param=20 and conse_param=" . $row['tseguimiento'], 'desc_param')));
            $observacion     = ucwords(strtolower($row["observacion"]));
            $fecha           = ($row['frec'] != '0000-00-00') ? $row['frec'] . " <br>" . $row['horarec'] : 'No';
            $copyCli         = ucwords(strtolower(getCampo('parametros', "where id_param=3 and conse_param=" . $row['agendarc'], 'desc_param')));

            $data[] = array(
                "tipoSeguimiento" => $tipoSeguimiento,
                "observacion"     => $observacion,
                "recordatorio"    => $fecha,
                "copyCLiente"     => $copyCli);
        }
        return $data;

    }
    public function listMisDemandasDetalle($codSolicitud)
    {
        $codSolicitud = $codSolicitud['idcapta'];

        $connPDO = new Conexion();
        ///evaluar si existe aprobacion
        $stmt1 = $connPDO->prepare("SELECT id_inmob_cm
								FROM condiciones_compartir_inm
								WHERE id_inmu_cm='$codSolicitud'
								and est_cm=2");
        $stmt1->execute();
        $existe = $stmt1->rowCount();

        $stmt = $connPDO->prepare("SELECT id_inmob_cm,
									    id_inmu_cm,
									    est_cm,
									    pct_comi_venta,
									    pct_como_renta,
									    vlr_comi_venta,
									    vlr_comi_renta,
									    condiciones_cm,
									    acepta_cm,
									    usu_cm,
									    ip_cm,
									    fecha_cm,
									    hora_cm,
									    conse_cm,
									    usu_acepta,
									    fecha_acepta,
									    cod_deman_cm
								FROM condiciones_compartir_inm
								WHERE id_inmu_cm='$codSolicitud'");
        $stmt->execute();
        $data = array();
        while ($row = $stmt->fetch()) {
            $idInmu     = $row['id_inmu_cm'];
            $conse_cm   = $row['conse_cm'];
            $demanda    = $row['cod_deman_cm'];
            $promotorN  = getCampo('usuarios', "where Id_Usuarios='" . $row['usu_cm'] . "'", 'Nombres');
            $promotorAp = getCampo('usuarios', "where Id_Usuarios='" . $row['usu_cm'] . "'", 'apellidos');
            $promotor   = ucwords(strtolower("$promotorN $promotorAp"));
            $politica   = $row['condiciones_cm'];
            $porVen     = $row['pct_comi_venta'] * 100;
            $porArr     = $row['pct_como_renta'] * 100;

            $valVen       = number_format($row['vlr_comi_venta']);
            $valArr       = number_format($row['vlr_comi_renta']);
            $est_cm       = getCampo('parametros', "WHERE id_param=22 and conse_param='" . $row['est_cm'] . "'", 'desc_param');
            $id_inmob_cm  = ucwords(strtolower(getCampo('inmobiliaria', "WHERE IdInmobiliaria='" . $row['id_inmob_cm'] . "'", 'NombreInm')));
            $fecha_cm     = $row['fecha_cm'];
            $hora_cm      = $row['hora_cm'];
            $usu_acepta   = $row['usu_acepta'];
            $fecha_acepta = $row['fecha_acepta'];

            $fecha = $fecha_cm;
            if ($fecha_acepta == '0000-00-00') {
                $segundos        = strtotime('now') - strtotime($fecha);
                $diferencia_dias = intval($segundos / 60 / 60 / 24);
                $demora          = "Solicitud Espera respuesta desde hace " . $diferencia_dias . " días";
            } else {
                $segundos = strtotime($fecha_acepta) - strtotime($fecha);
                //echo "$fecha_acepta - $fecha <br>";
                $diferencia_dias = intval($segundos / 60 / 60 / 24);
                $demora          = "La Solicitud se contestó en " . $diferencia_dias . " días";
            }
            $btnEdit     = '<button type="button" class="btn btn-warning editDeman"><span class="glyphicon glyphicon-edit"></span></button>';
            $consecutivo = "<input type='hidden' class='consec' name='conse' value='" . $conse_cm . "'>";
            $idinmub     = "<input type='hidden'  name='idinmu' value='" . $idInmu . "'>";
            $demandac    = "<input type='hidden' name='codDeman' class='codDeman' value='" . $demanda . "'>";

            if ($existe > 0) {

                if ($row['est_cm'] == 2) {
                    $btnEditSolicitud = "<span style='color:green;' class='fa fa-check'></span>";
                }
                if ($row['est_cm'] == 3) {
                    $btnEditSolicitud = "<span style='color:red' class='fa fa-ban'></span>";
                }
            } else {
                if ($row['est_cm'] == 2) {
                    $btnEditSolicitud = "<span style='color:red' class='fa fa-ban'></span>";
                }
                if ($row['est_cm'] == 3) {
                    $btnEditSolicitud = "<span style='color:red' class='fa fa-ban'></span>";
                }
                if ($row['est_cm'] <= 1) {
                    $btnEditSolicitud = $btnEdit;
                }
                //$btnEditSolicitud=($row['est_cm']==2 || $row['est_cm']==3)?"":$btnEdit;
            }
            $arriendo = "$" . $valArr . " <br> $porArr %";
            $venta    = "$" . $valVen . " <br> $porVen %";
            $feccm    = $fecha_cm . "<br>" . $hora_cm;
            $data[]   = array(
                'idInmueble'       => $idInmu,
                'btnedit'          => $btnEditSolicitud . $consecutivo . $idinmub . $demandac,
                'solicitante'      => $promotor,
                'politcaCompartir' => $politica,
                'arriendo'         => $arriendo,
                'venta'            => $venta,
                'estadoSolicitud'  => $est_cm,
                'fechaRes'         => $fecha_acepta,
                'fechaOfer'        => $fecha,
                'tiempoSolicitud'  => $demora,
                'deman'            => $demanda,
                'inmobiliaria'     => $id_inmob_cm,
            );

        }
        return $data;
    }
    public function CrearSolicitudCompartir($data)
    {

        try
        {
            $connPDO = new Conexion();

            $conse        = consecutivo('conse_cm', 'condiciones_compartir_inm');
            $usuario      = $_SESSION['Id_Usuarios'];
            $inmobiliaria = $_SESSION['IdInmmo'];
            $idinmu       = $data['codInmu'];
            $codDemanda   = $data["codDeman"];
            $f_sis        = date('Y-m-d');
            $h_sis        = date('H:i:s');
            $ip           = $_SERVER['REMOTE_ADDR'];
            $estado       = 0;
            $comiVenta    = getCampo('inmuebles', "where idInm='" . $idinmu . "'", 'ComiVenta');
            $comiArren    = getCampo('inmuebles', "where idInm='" . $idinmu . "'", 'ComiArren');
            $valComiV     = getCampo('inmuebles', "where idInm='" . $idinmu . "'", 'ValComiVenta');
            $valComiP     = getCampo('inmuebles', "where idInm='" . $idinmu . "'", 'ValComiArr');
            $politicaComp = getCampo('inmuebles', "where idInm='" . $idinmu . "'", 'politica_comp');
            $tipo_negocio = $data['tipo_negocio'];

            $stmt = $connPDO->prepare("INSERT INTO condiciones_compartir_inm
							           (conse_cm,
							           	id_inmob_cm,
							         	id_inmu_cm,
							         	pct_comi_venta,
							         	pct_como_renta,
							         	vlr_comi_venta,
							         	vlr_comi_renta,
							         	condiciones_cm,
							         	cod_deman_cm,
							         	ip_cm,
							         	fecha_cm,
							         	hora_cm,
							         	usu_cm,
							         	tipo_neg_cm)
							         values
							         (	:conse_cm,
							         	:inmo,
							         	:inmu,
							         	:comiVenta,
							         	:comiRenta,
							         	:valorVenta,
							         	:valorRenta,
							         	:politicas,
							         	:codDemanda,
							         	:ip_cm,
							         	:fecha_cm,
							         	:hora_cm,
							         	:usu_cm,
							         	:tipo_neg_cm
							         	)
				");
            if ($stmt->execute(array(
                ":conse_cm"    => $conse,
                ":inmo"        => $inmobiliaria,
                ":inmu"        => "$idinmu",
                ":comiVenta"   => $comiVenta,
                ":comiRenta"   => $comiArren,
                ":valorVenta"  => $valComiV,
                ":valorRenta"  => $valComiP,
                ":politicas"   => "$politicaComp",
                ":codDemanda"  => $codDemanda,
                ":ip_cm"       => "$ip",
                ":fecha_cm"    => "$f_sis",
                ":hora_cm"     => "$h_sis",
                ":usu_cm"      => $usuario,
                ":tipo_neg_cm" => $tipo_negocio,

            ))) {
                return 1;
            } else {
                // echo "\nPDO::errorInfo():\n";
                //             print_r($stmt->errorInfo());
                return 2;

            }

        } catch (PDOException $e) {
            return "some fail-messages";
        }

    }
    public function EditarSolicitudCompartir($data)
    {

        try
        {
            $connPDO = new Conexion();

            $f_sis        = date('Y-m-d');
            $h_sis        = date('H:i:s');
            $usuario      = $_SESSION['Id_Usuarios'];
            $ip           = $_SERVER['REMOTE_ADDR'];
            $descripcion  = $data['descripcion'];
            $idinmu       = $data['idinmu'];
            $conse        = $data['conse'];
            $estado_dem   = $data['estado_dem'];
            $tipo_negocio = $data["negocio"];

            $stmt = $connPDO->prepare("UPDATE condiciones_compartir_inm
			   SET
				usu_acepta			= '$usuario',
  				ip_acepta			= '$ip',
				fecha_acepta		= '$f_sis',
				hora_acepta			= '$h_sis',
				observacion_acepta= '$descripcion',
				est_cm				= '$estado_dem'
				WHERE id_inmu_cm	= '$idinmu'
				AND conse_cm		= '$conse'");

            // if($stmt->execute(array(
            //         ":usuario"=>$usuario,
            //        ":ip"=>$ip,
            //       ":f_sis"=>"$f_sis",
            //       ":h_sis"=>"$h_sis",
            //       ":descripcion"=>"$descripcion",
            //       ":estado_dem"=>$estado_dem,
            //       ":idinmu"=>"$idinmu",
            //       ":conse"=>$conse,
            //     )))
            if ($stmt->execute()) {
                return 1;
            } else {
                return 0;
            }

        } catch (PDOException $e) {
            return "some fail-messages";
        }

    }
    public function creaSeguimientoOferta($inmob, $data)
    {
        $w_conexion = new MySQL();
        // include('../mcomercialweb/PHPMailer_5.2.2/class.phpmailer.php');
        // include('../mcomercialweb/PHPMailer_5.2.2/class.smtp.php');
        // include('../mcomercialweb/emailtae2.class.php');

        $IdDemanda    = $data['idDemanda'];
        $tseguimiento = $data['tseguimiento'];
        $descripcion  = $data['descripcion'];
        $frec         = $data['frec'];
        $horarec      = $data['hora_ini'];
        $agendar      = $data['agendar'];
        $agendarc     = $data['agendarc'];
        $mailcop      = $data['mailcop'];
        $resseg       = $data['resseg'];

        $conse = consecutivo('idSeguimiento', 'SeguimientoSolicitudNvo');
        $sql   = "INSERT INTO SeguimientoSolicitudNvo
			(idSeguimiento,idDemanda,tseguimiento,observacion,
			frec,horarec,agendar,agendarc,mailcop,resseg)
			VALUES
			('$conse','$IdDemanda','$tseguimiento','$descripcion',
			'$frec','$horarec','$agendar','$agendarc','$mailcop','$resseg')";
        $res = $w_conexion->RecordSet($sql);
        if ($res) {
            // echo "res $sql";
            //echo $idDemanda;
            $response = array("idPromotor" => $IdPromotor, "f_sis" => $f_sis, "FechaD" => $FechaD, "numced" => $numced, "idDemanda" => $IdDemanda, "status" => 1);
            //$envioemail = new EmailTae();
            //$enviado= $envioemail->ResultadoCorreoDemada();
            return $response;

        }
    }

    public function condiDemanda($idInmueble)
    {
        $idInmueble = $idInmueble["idInmueble"];
        $connPDO    = new Conexion();
        $stmt       = $connPDO->prepare("SELECT
  									ValorVenta,
  									ValorCanon,
  									Administracion,
  									tipo_contrato,
  									Destinacion,
  									ComiVenta,
	    		                   	ComiArren,
  									NumLlaves,
  									NumCasillero,
  									IdProcedencia,
  									idCaptador,
  									IdPromotor,
  									IdDestinacion,
  									idEstadoinmueble,
  									Exclusivo,
  									IdUbicacion,
  									ValorHipoteca,
  									conceptohipoteca,
  									id_moneda,
  									IdTpInm,
  									ValComiVenta,
  									ValComiArr,
  									politica_comp,
  									IdInmobiliaria
  			FROM inmuebles
			WHERE idInm = '$idInmueble'");

        $stmt->execute();
        $data = array();
        while ($row = $stmt->fetch()) {
            $inm_vventa       = $row['ValorVenta'];
            $IdPromotor       = $row['IdPromotor'];
            $inm_vcanon       = $row['ValorCanon'];
            $inm_admon        = $row['Administracion'];
            $inm_destina      = $row['IdDestinacion'];
            $inm_comventa     = $row['ComiVenta'];
            $inm_comarren     = $row['ComiArren'];
            $inm_numllaves    = $row['NumLlaves'];
            $inm_numcas       = $row['NumCasillero'];
            $inm_proced       = $row['IdProcedencia'];
            $inm_captador     = $row['idCaptador'];
            $inm_promotor     = $row['IdPromotor'];
            $inm_estado       = $row['idEstadoinmueble'];
            $inm_exclusivo    = $row['Exclusivo'];
            $inm_tipocontrato = $row['tipo_contrato'];
            $inm_ubicacionlla = $row['IdUbicacion'];
            $inm_vhipo        = $row['ValorHipoteca'];
            $inm_conhipo      = $row['conceptohipoteca'];
            $inm_moneda       = $row['id_moneda'];
            $tipoinmuebleed   = $row['IdTpInm'];
            $inm_ValComiVenta = $row['ValComiVenta'];
            $inm_ValComiArr   = $row['ValComiArr'];
            $politica_comp    = $row['politica_comp'];
            $IdInmobiliaria   = $row['IdInmobiliaria'];

            if ($politica_comp != "") {
                $porceVenta     = $inm_comventa;
                $vcominven      = $inm_ValComiVenta;
                $porcenarriendo = $inm_comarren;
                $vcominarr      = $inm_ValComiArr;
                $idinmueble     = $idInmueble;
                $nprom          = ucwords(strtolower(getCampo('usuarios',
                    "where Id_Usuarios='" . $IdPromotor . "'", 'Nombres')));
                $aprom = ucwords(strtolower(getCampo('usuarios',
                    "where Id_Usuarios='" . $IdPromotor . "'", 'apellidos')));
                $promotornoms = "$nprom $aprom";
                $logoEmpresa  = getCampo('clientessimi', "where IdInmobiliaria='" . $IdInmobiliaria . "'", 'logo');
                $fotoSoli     = getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Foto');
                $largoSoli    = strlen($fotoSoli);
                if ($largoSoli <= 6) {
                    $foto1 = "../sj/img/def_avatar.gif";
                } else {
                    $foto1 = $fotoSoli;
                }
                $fotoAsesor = getCampo('usuarios', "where Id_Usuarios='" . "1024524000" . "'", 'Foto');
                //echo $fotoAsesor;
                $largoAsesor = strlen($fotoAsesor);
                if ($largoAsesor <= 6) {
                    $foto2 = "../sj/img/def_avatar.gif";
                } else {
                    $foto2 = $fotoAsesor;
                }
                $pVenta = ($inm_comventa * 100);
                $prenta = ($inm_comarren * 100);
                if ($inm_ValComiVenta > 0) {
                    $vVenta = number_format($inm_ValComiVenta);
                } else {
                    $vVenta = number_format($inm_vventa * $inm_comventa);
                }
                if ($inm_ValComiArr > 0) {
                    $vRenta = number_format($inm_ValComiArr);
                } else {
                    $vRenta = number_format($inm_vcanon * $inm_comarren);
                }
                $data[] = array("Nombre" => $promotornoms,
                    "fotopromo"              => $foto1,
                    "idPromotor"             => $inm_promotor,
                    "fotoasesor"             => $foto2,
                    "venta"                  => $vVenta,
                    "renta"                  => $vRenta,
                    "porcenventa"            => $porceVenta,
                    "vcominven"              => $vcominven,
                    "porcenarriendo"         => $porcenarriendo,
                    "vcominarr"              => $vcominarr,
                    "idinmueble"             => $idinmueble,
                    "idUsuario"              => $_SESSION['Id_Usuarios'],
                    "logoEmpresa"            => $logoEmpresa,
                    "politica_comp"          => ucfirst(strtolower($politica_comp)),
                );
            } //FIN del if condicion demandas
            else {
                $data[] = array(
                    "valor" => 0);
            }
        } //FIN del while
        return $data;
    } //FIN del function
    public function condiOferta($idDemanda)
    {
        $idDemanda = $idDemanda["idDemanda"];
        //echo $idDemanda."ffff";
        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT
  									IdPromotor,

  									IdTipoInm,

  									IdInmobiliaria
  			FROM demandainmuebles
			WHERE IdDemanda = '$idDemanda'");

        $stmt->execute();
        $data = array();
        while ($row = $stmt->fetch()) {
            $IdPromotor     = $row['IdPromotor'];
            $tipoinmuebleed = $row['IdTipoInm'];
            $IdInmobiliaria = $row['IdInmobiliaria'];

            if ($politica_comp != "0890890") {
                $idDemanda    = $idDemanda;
                $nprom        = ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Nombres')));
                $aprom        = ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'apellidos')));
                $promotornoms = "$nprom $aprom";
                $logoEmpresa  = getCampo('clientessimi', "where IdInmobiliaria='" . $IdInmobiliaria . "'", 'logo');
                $fotoSoli     = getCampo('usuarios', "where Id_Usuarios='" . $IdPromotor . "'", 'Foto');
                $largoSoli    = strlen($fotoSoli);
                if ($largoSoli <= 6) {
                    $foto1 = "../sj/img/def_avatar.gif";
                } else {
                    $foto1 = $fotoSoli;
                }

                $data[] = array("Nombre" => $promotornoms,
                    "fotopromo"              => $foto1,
                    "idPromotor"             => $IdPromotor,
                    "fotoasesor"             => $foto2,
                    "idDemanda"              => $idDemanda,
                    "idUsuario"              => $_SESSION['Id_Usuarios'],
                    "logoEmpresa"            => $logoEmpresa,
                );
            } //FIN del if condicion demandas
            else {
                $data[] = array(
                    "valor" => 0);
            }
        } //FIN del while
        return $data;
    } //FIN del function
    public function datosControlDemandas($data)
    {
        //Danny

        $connPDO  = new Conexion();
        $Demandas = new Negocios();

        $cond = "";

        if ($data['codDemanda']) {
            //echo "ingresa a codDemanda ".$_GET['codDemanda'];
            $cond .= " and i.IdDemanda=:codDemanda";
        }
        if ($data["ffecha_ini"] > 0 and $data["ffecha_fin"] > 0) {
            $cond .= " AND i.FechaD  between :ffecha_ini and :ffecha_fin";
        }
        /*if($data['idInmu'])
        { //echo "ingresa a codDemanda ".$_GET['codDemanda'];
        $cond .=" and i.idInm=:idInmu";
        }*/
        /*if($data['idInmmo']!=1)
        {
        $cond2 .="AND IdInmobiliaria = :idInmmo";
        }*/
        if ($data['perfil'] == 3 or $data['perfil'] == 10) {

        }
        $hoy = date("Y-m-d");

        $stmt = $connPDO->prepare("SELECT IdDemanda,IdGestion,IdDestinacion,IdInmobiliaria,IdPromotor,
		 			Estado,FechaLimite,FechaD
					FROM demandainmuebles i
					WHERE i.IdInmobiliaria >= 1
					$cond limit 0,70");

        if ($data['codDemanda']) {
            //echo "ingresa a codDemanda ".$_GET['codDemanda'];
            $stmt->bindParam(":codDemanda", $data["codDemanda"]);
        }
        if ($data["ffecha_ini"] > 0 and $data["ffecha_fin"] > 0) {
            $stmt->bindParam(':ffecha_ini', $data["ffecha_ini"]);
            $stmt->bindParam(':ffecha_fin', $data["ffecha_fin"]);
        }
        /*
        if($data['idInm'])
        { //echo "ingresa a codDemanda ".$_GET['codDemanda'];
        $stmt->bindParam(":idInm",$data["idInm"]);
        }*/
        /*
        if($data['idInmmo']!=1)
        {
        $cond2 .="AND IdInmobiliaria = :idInmmo";

        }
        if($data['perfil']==3 or $data['perfil']==10)
        {

        }

        $stmt->bindParam(':idInmmo',$data["idInmmo"]);
         */
        $stmt->execute();

        $listnegocios = array();
        while ($row = $stmt->fetch()) {
            ///boton para descartar demanda
            $descartarBtn = ($row["RESULTADO"] == 1) ? "" : "<button class='btn btn-danger btn-sm dct' data-toggle='modal' data-target='#modal1-id' title='Descartar Demanda'><span class='glyphicon fa fa-times-circle'></span></button>";

            $negociarBtn    = ($row["RESULTADO"] == 0) ? "" : "<button class='btn btn-success btn-sm' 	><span class='glyphicon fa fa-check-square-o'></span></button>";
            $editarBtn      = "<button class='btn btn-warning btn-sm dct1' title='Buscar Demanda' data-toggle='modal' data-target='#modal2-id'><span class='fa fa-search'></span></button>";
            $cambiosBtn     = "<button class='btn btn-info btn-sm' toggle='modal' data-target='#modal-id'><span class='glyphicon glyphicon-eye-open'></span></button>";
            $seguimientoBtn = "<button class='btn btn-danger btn-sm'><span class='fa fa-sign-in'></span></button>";
            // $ofertabtn=($this->seguiDemandas($row["IdDemanda"])==1)?"<button class='btn btn-success btn-sm'><span class='glyphicon glyphicon-ok'></span></button>":"";

            $demInmu   = $Demandas->datosControlDemandasInmAplican($row['IdDemanda']);
            $dataModal = $cambiosBtn;
            //$dem=293;
            //$dem=$Demandas->buscarDemandas1($row["idInm"]);
            //echo $dem;

            $listnegocios[] = array(
                "idInm"        => $row["IdInmobiliaria"],
                "ninmob"       => ucwords(strtolower(getCampo('clientessimi', "where IdInmobiliaria=" . $row["IdInmobiliaria"], 'Nombre'))),
                "frespuesta"   => "frespuesta",
                "tipoInmueble" => ucwords(strtolower(getCampo('tipoinmuebles', "where idTipoInmueble=" . $row["IdTpInm"], 'Descripcion'))),
                "Estado"       => getCampo("parametros", "where id_param=16 and conse_param=" . $row["Estado"], "desc_param"),
                "diassin"      => "diassin",
                "IdDemanda"    => $row["IdDemanda"],
                "promotor"     => ucwords(strtolower(getCampo('usuarios', "where Id_Usuarios='" . $row["IdPromotor"] . "'", 'concat(Nombres," ",apellidos)'))),
                "dem"          => "$dem",
                "FechaLimite"  => $row['FechaLimite'],
                "FechaD"       => $row['FechaD'],
                "inmu"         => "$demInmu",
                "btnModal"     => $dataModal,

            );
        } //print_r($listnegocios)."ffff";

        ///mostrar errores
        //return print_r($stmt->errorInfo())."\nPDO::errorInfo():\n";
        return $listnegocios;
    }
    public function datosControlDemandasInmAplican($data)
    {
        //Danny

        $connPDO  = new Conexion();
        $Demandas = new Negocios();
        /*if($data['inmob'])
        { //echo "ingresa a codDemanda ".$_GET['codDemanda'];
        $cond .=" and i.idInm=:idInm";
        }
         */
        $demanda = $data;
        //echo $demanda."ffffffffffff";
        $hoy = date("Y-m-d");

        $stmt = $connPDO->prepare("SELECT id_inmu_cm,fecha_cm,fecha_acepta
					FROM condiciones_compartir_inm
					WHERE cod_deman_cm=:cod_deman_cm
					group by id_inmu_cm	");
        /*
        if($data['idInmmo']!=1)
        {
        $cond2 .="AND IdInmobiliaria = :idInmmo";

        }
        if($data['perfil']==3 or $data['perfil']==10)
        {

        }*/

        $stmt->bindParam(':cod_deman_cm', $demanda);
        $stmt->execute();

        $listado_Inmubles = '<ul>';

        while ($row = $stmt->fetch()) {

            $idInm      = $row["id_inmu_cm"];
            $btnInfoinm = '<button type="button" id="botonpre3" class="btn btn-xs btn-info  botonpre3"  data-toggle="modal"  data-target="#modal2-id"><span class="glyphicon glyphicon"><i class="fa fa-files-o"></i></span></button><input type="hidden" class="inmudem" value="' . $idInm . '" ><input type="hidden" class="coddem" value="' . $demanda . '" >';
            $fecha      = $row["fecha_cm"];
            if ($row["fecha_acepta"] == '0000-00-00') {
                $segundos        = strtotime('now') - strtotime($fecha);
                $diferencia_dias = intval($segundos / 60 / 60 / 24);
                $demora          = "<b class='text-warning'>Solicitud Espera</b> respuesta desde hace " . $diferencia_dias . " días";
            } else {
                $segundos = strtotime($row["fecha_acepta"]) - strtotime($fecha);
                //echo "$fecha_acepta - $fecha <br>";
                $diferencia_dias = intval($segundos / 60 / 60 / 24);
                if ($diferencia_dias == 0) {
                    $demora = "La Solicitud se contestó el mismo día";
                } else {
                    $demora = "La Solicitud se contestó en " . $diferencia_dias . " días";
                }
            }

            $listado_Inmubles .= '<li>' . $row["id_inmu_cm"] . ' ' . $btnInfoinm . ' ' . $demora . ' </li>';
        }
        $listado_Inmubles .= '</ul>';
        //echo $listado_Inmubles."----";
        ///mostrar errores
        //return print_r($stmt->errorInfo())."\nPDO::errorInfo():\n";
        return $listado_Inmubles;
    }
    public function infoInmuebleCompartido($demanda, $inmudem)
    {
        //Danny

        $connPDO  = new Conexion();
        $Demandas = new Negocios();
        /*if($data['inmob'])
        { //echo "ingresa a codDemanda ".$_GET['codDemanda'];
        $cond .=" and i.idInm=:idInm";
        }
         */

        //echo $demanda."ffffffffffff";
        $hoy           = date("Y-m-d");
        $datosInmueble = array();
        $stmt          = $connPDO->prepare("SELECT id_inmu_cm,fecha_cm,fecha_acepta,est_cm,
		 			pct_comi_venta,pct_como_renta,vlr_comi_venta,vlr_comi_renta,condiciones_cm,acepta_cm,
					condiciones_cm,observacion_acepta
					FROM condiciones_compartir_inm
					WHERE cod_deman_cm=:cod_deman_cm
					and id_inmu_cm = :id_inmu_cm
					group by id_inmu_cm	");

        $stmt->bindParam(':cod_deman_cm', $demanda);
        $stmt->bindParam(':id_inmu_cm', $inmudem);
        $stmt->execute();

        while ($row = $stmt->fetch()) {

            $estado     = getCampo('parametros', "WHERE id_param=22 and conse_param='" . $row['est_cm'] . "'", 'desc_param');
            $btnEditSol = '<button type="button" class="btn btn-warning btn-xs btn-seg"><span class="glyphicon glyphicon-edit"></span></button>';
            $btnEditSol = ($row['est_cm'] == 1 || $row['est_cm'] == 4) ? $btnEditSol : "";

            $venta           = "$" . $row['vlr_comi_venta'] . "<br>" . $row['pct_comi_venta'] . "%";
            $renta           = "$" . $row['vlr_comi_renta'] . "<br>" . $row['pct_comi_renta'] . "%";
            $idInm           = $row["id_inmu_cm"];
            $datosInmueble[] = array(
                "cod_deman"          => $demanda,
                "fecha_acepta"       => $row['fecha_acepta'],
                "pct_comi_venta"     => $venta,
                "pct_comi_renta"     => $renta,
                "condiciones_cm"     => $row['condiciones_cm'],
                "observacion_acepta" => $row['observacion_acepta'],
                "fecha_cm"           => $row['fecha_cm'],
                "est_cm"             => $estado,
                "btnSeg"             => $btnEditSol,
            );

        }

        return json_encode($datosInmueble);
    }
    public function validaAplicaInmueble($coddem, $idInm)
    {
        //Danny

        $connPDO = new Conexion();
        $stmt    = $connPDO->prepare("SELECT id_inmu_cm
					FROM condiciones_compartir_inm
					WHERE cod_deman_cm=:cod_deman_cm
					and id_inmu_cm = :id_inmu_cm
					group by id_inmu_cm	");

        $stmt->bindParam(':cod_deman_cm', $coddem);
        $stmt->bindParam(':id_inmu_cm', $idInm);
        $stmt->execute();
        $conteo = $stmt->rowCount();

        return $conteo;
    }

    public function traeClientes()
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare('SELECT cedula,nombre FROM clientes_inmobiliaria a
                                  WHERE a.`inmobiliaria`=:inmobiliaria AND nombre <>	 ""');

        if ($stmt->execute(array(

            ":inmobiliaria" => $_SESSION["IdInmmo"],
        ))) {
            $response = array();
            while ($row = $stmt->fetch()) {
                if ($row["nombre"] != "") {
                    $response[] = array(
                        "idCliente" => $row['cedula'],
                        "Nombre"    => utf8_encode($row['nombre']),
                    );
                }

            }
            return $response;
        } else {
            return $stmt->errorInfo();
            $stmt = null;
        }
    }
    public function listDemandasBeta($data, $migrupo)
    {

        $connPDO  = new Conexion();
        $Demandas = new Negocios();

        $cond      = "";
        $hoy       = date("Y-m-d");
        $condFecha = " and FechaLimite>='$hoy'";
        if ($data['tip_inm']) {
            $cond .= " and d.IdTipoInm=:inmueble";

        }
        if ($data['dest']) {
            $cond .= " and d.IdDestinacion=:dest";
        }
        if ($data['estrato']) {
            $cond .= " and d.estrato_dem=:estrato";
        }
        if ($data['tip_ope']) {
            if ($data['tip_ope'] != 2) {
                $cond .= " and d.IdGestion=:tip_ope";
            } else {
                $cond .= " and d.IdGestion != 3";
            }
        }
        if ($data['asesor']) {
            $cond .= " and d.IdPromotor=:asesor";
        }
        $data['est_cli'] = trim($data['est_cli']);
        if ($data['est_cli'] >= 0) {
            $cond .= " and d.Estado=:est_clie";
        }

        if ($data['interesado']) {
            $cond .= " and d.CedulaIInt =:interesado";
        }
        if ($data["ffecha"] == 1) {
            if ($data["ffecha_ini"] > 0 and $data["ffecha_fin"] > 0) {
                $cond .= " and FechaD  between :ffecha_ini and :ffecha_fin";
                $condFecha = "";
            }
            // if($data['idInmmo']!=1)
            // {
            //     $cond2 .="AND IdInmobiliaria = :idInmmo";
            // }
        } else {
            if ($data["ffecha_ini"] > 0 and $data["ffecha_fin"] > 0) {
                $cond .= " and d.FechaLimite  between :ffecha_ini and :ffecha_fin";
                $condFecha = "";
            }

        }
        if ($data['codDemanda']) {
            //echo "ingresa a codDemanda ".$_GET['codDemanda'];
            $cond .= " and d.IdDemanda=:codDemanda";
        }
        if ($data['perfil'] == 3 or $data['perfil'] == 10) {

        }
        if ($data['idInmmo'] != 1) {
            $cond2 .= "AND c.inmobiliaria = :idInmmo";
        }

        /*echo "<pre>";
        print_r("SELECT d.IdDemanda,
        d.FechaD,
        d.IdPromotor,
        d.FechaLimite,
        d.CedulaIInt,
        d.Calle,
        d.AlaCalle,
        d.Carrera,
        d.AlaCarrera,
        d.IdGestion,
        d.IdDestinacion,
        d.IdTipoInm,
        d.ValInicial,
        d.ValFinal,
        d.AreaInicial,
        d.AreaFinal,
        d.IdCiudad,
        d.Descripcion,
        c.Nombre as Interesado,
        c.email,
        c.telfijo,
        c.telcelular,
        g.NombresGestion,
        t.Descripcion As TipoInmueble,
        c.cedula as cedulaInte,
        d.Estado,
        d.localidad,
        d.estrato_dem,
        ba.NombreB,
        d.grupo_dem,
        des.Nombre as Nombredes,
        IF ('$hoy'  BETWEEN d.FechaD AND d.FechaLimite,1,0)
        as RESULTADO
        FROM     demandainmuebles d
        INNER JOIN clientes_inmobiliaria c On d.CedulaIInt = c.cedula
        LEFT JOIN barrios ba ON ba.IdBarrios                = d.IdBarrio
        INNER JOIN gestioncomer g On d.IdGestion           =g.IdGestion
        INNER JOIN tipoinmuebles t On d.IdTipoInm =t.idTipoInmueble
        INNER JOIN destinacion des on des.Iddestinacion = d.IdDestinacion
        WHERE c.inmobiliaria >0
        $condFecha
        $cond2
        $cond LIMIT 0,3");

        echo "</pre>";

        echo "<pre>";
        print_r($data);
        echo "</pre>";

        exit;*/
        $stmt = $connPDO->prepare("SELECT d.IdDemanda,
								d.FechaD,
								d.IdPromotor,
								d.FechaLimite,
			      				d.CedulaIInt,
			      				d.Calle,
			      				d.AlaCalle,
			      				d.Carrera,
			      				d.AlaCarrera,
			      				d.IdGestion,
			      				d.IdDestinacion,
			      				d.IdTipoInm,
			      				d.ValInicial,
			      				d.ValFinal,
			      				d.AreaInicial,
			      				d.AreaFinal,
			      				d.IdCiudad,
			      				d.Descripcion,
			                    c.Nombre as Interesado,
			                    c.email,
			                    c.telfijo,
			                    c.telcelular,
			                    g.NombresGestion,
			                    t.Descripcion As TipoInmueble,
			      				c.cedula as cedulaInte,
			      				d.Estado,
			      				d.localidad,
			      				d.estrato_dem,
			      				ba.NombreB,
			      				d.grupo_dem,
			      				des.Nombre as Nombredes,
			      				IF ('$hoy'  BETWEEN d.FechaD AND d.FechaLimite,1,0)
			      				as RESULTADO
			      		FROM 	demandainmuebles d
			      		INNER JOIN clientes_inmobiliaria c On d.CedulaIInt = c.cedula
			      		LEFT JOIN barrios ba ON ba.IdBarrios 			   = d.IdBarrio
			      		INNER JOIN gestioncomer g On d.IdGestion           =g.IdGestion
			      		INNER JOIN tipoinmuebles t On d.IdTipoInm =t.idTipoInmueble
			      		INNER JOIN destinacion des on des.Iddestinacion = d.IdDestinacion
			      		WHERE c.inmobiliaria >0
						$condFecha
			      		$cond2
			      		$cond");

        if ($data['tip_inm']) {
            $stmt->bindParam(":inmueble", $data['tip_inm']);
        }
        if ($data['dest']) {
            $stmt->bindParam(':dest', $data["dest"]);
        }
        if ($data['estrato']) {
            $stmt->bindParam(':estrato', $data["estrato"]);
        }
        if ($data['tip_ope']) {
            $stmt->bindParam(":tip_ope", $data["tip_ope"]);
        }
        if ($data['asesor']) {
            $stmt->bindParam(":asesor", $data["asesor"]);
        }
        if ($data['est_cli'] >= 0) {

            $stmt->bindParam(':est_clie', $data['est_cli']);
        }
        if ($data['codDemanda']) {
            //echo "ingresa a codDemanda ".$_GET['codDemanda'];
            $stmt->bindParam(":codDemanda", $data["codDemanda"]);
        }
        if ($data['interesado']) {
            $stmt->bindParam(":interesado", $data["interesado"]);
        }
        if ($data["ffecha_ini"] > 0 and $data["ffecha_fin"] > 0) {
            $stmt->bindParam(':ffecha_ini', $data["ffecha_ini"]);
            $stmt->bindParam(':ffecha_fin', $data["ffecha_fin"]);
        }
        if ($data['idInmmo'] != 1) {
            $stmt->bindParam(':idInmmo', $data["idInmmo"]);

        }
        if ($data['perfil'] == 3 or $data['perfil'] == 10) {

        }

        $stmt->execute();
        $listnegocios = array();
        $listaBarrios = '';
        //$inmueblesAfectados=array();
        while ($row = $stmt->fetch()) {

            $logs = $this->logsObservaDemandas($row["IdDemanda"]);

            $negociarBtn = ($row["RESULTADO"] == 0) ? "" : "<button class='btn btn-success btn-sm'data-toggle='modal' data-target='#modal-negos' title='Aplicar Oferta'><span class='glyphicon glyphicon-ok'></span></button>";
            $editarBtn   = "<button class='btn btn-warning btn-sm editde' data-toggle='modal' data-target='#modal-deman' title='Editar Demanda' ><span class='fa fa-edit'></span></button>";
            $btnMapa     = "<button class='btn btn-primary btn-sm vmapa' style='display:none' data-toggle='modal' data-target='#modal-map' title='Ver en el Mapa' ><span class='fa fa-location-arrow'></span></button>";

            $cambiosBtn = ($logs == 1) ? "
      			<button class='btn btn-info btn-sm' data-toggle='modal' data-target='#modal-logs' title='Ver Cambios'>
      			<span class='glyphicon glyphicon-eye-open'></span></button>" : "";

            $seguimientoBtn = "<button class='btn btn-danger btn-sm' data-toggle='modal'  data-target='#modal-id'  title='Seguimiento' alt='seguimiento'><span class='fa fa-sign-in'></span></button>";
            $dataModal      = $editarBtn . $cambiosBtn . $seguimientoBtn . $btnMapa;
            $calle          = ucwords(strtolower("De la calle " . $row["Calle"] . " a la " . $row["AlaCalle"]));
            $carrera        = ucwords(strtolower("De la carrera " . $row["Carrera"] . " a la " . $row["AlaCarrera"]));
            $dataCliente    = $row['telfijo'] . "-" . $row['telcelular'];
            // $inmuebles='11111';
            //$data["prioridad"]=2;
            $a = $this->buscaBarriosLocalidad($row["IdDemanda"]);
            // $a=substr($a,0-1);
            $a = explode(",", $a);

            $listaBarrios = '';
            for ($i = 0; $i < count($a); $i++) {
                $listaBarrios = getCampo('barrios', "where IdBarrios=" . $a[$i], 'NombreB') . " - " . $listaBarrios;
                //echo "$listaBarrios  ".$a[$i];
            }

            $nCiu               = getCampo('ciudad', "where IdCiudad=" . $row['IdCiudad'], 'Nombre');
            $btnsearchinmuebles = '<button class="btn btn-primary searchInmuebles" data-demanda="' . $row["IdDemanda"] . '">Ver Inmuebles que aplican</button><div style="width: 871px" id="' . $row["IdDemanda"] . '">&nbsp;</div>';
            // $inmuebles=$Demandas->buscarInmueblesBeta($row["IdDemanda"],$_SESSION['IdInmmo'],$row["IdGestion"],$row["IdDestinacion"],$row["IdTipoInm"],$row['IdCiudad'],$row['localidad'],$data["prioridad"],$row["ValInicial"],$row["ValFinal"],$row["estrato_dem"],$migrupo);
            //$inmueblesAfectados[]=$row["IdDemanda"];
            $listnegocios[] = array(
                "idemanda"     => $row["IdDemanda"], //."---".$data['est_cli']." <<<<".$cond,
                "destinacion"  => ucwords(strtolower($row["Nombredes"])),
                "fechaLimite"  => $row["FechaLimite"],
                "fechaD"       => $row["FechaD"],
                "interesado"   => ucwords(strtolower($row["Interesado"])),
                "barrio"       => ucwords(strtolower($listaBarrios)),
                "gestion"      => ucwords(strtolower($row["NombresGestion"])),
                "tipoInmueble" => ucwords(strtolower($row["TipoInmueble"])),
                "valorInicial" => "$" . number_format($row["ValInicial"]),
                "valorFinal"   => "$" . number_format($row["ValFinal"]),
                "Calle"        => $calle,
                "Carrera"      => $carrera,
                "AreaInicial"  => $row["AreaInicial"],
                "AreaFinal"    => $row["AreaFinal"],
                "btnModal"     => $dataModal,
                "telcli"       => $dataCliente,
                "mailcli"      => $row["email"],
                "estrato"      => $row["estrato_dem"],
                "ofertas"      => $btnsearchinmuebles,
                "obser"        => $row["Descripcion"],

            );
        }
        //$listnegocios[]=array("inmus"=>$inmueblesAfectados);
        return $listnegocios;

    } //fin function listDemandas()

    public function searchInmuebleByDemanda($data)
    {
       // echo "<pre>";
       // print_r($data);
       // echo "</pre>";
       // exit();
        $sql = "SELECT d.IdDemanda,
								d.FechaD,
								d.IdPromotor,
								d.FechaLimite,
			      				d.CedulaIInt,
			      				d.Calle,
			      				d.AlaCalle,
			      				d.Carrera,
			      				d.AlaCarrera,
			      				d.IdGestion,
			      				d.IdDestinacion,
			      				d.IdTipoInm,
			      				d.ValInicial,
			      				d.ValFinal,
			      				d.AreaInicial,
			      				d.AreaFinal,
			      				d.IdCiudad,
			      				d.Descripcion,
			      				d.Estado,
			      				d.localidad,
			      				d.estrato_dem,
			      				d.grupo_dem
			      		FROM 	demandainmuebles d
			      		WHERE d.IdDemanda = :iddemanda";
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare($sql);

        $stmt->bindParam(':iddemanda', $data['iddemanda']);

        if ($stmt->execute()) {
            $result    = $stmt->fetch(PDO::FETCH_ASSOC);
            $inmuebles = $this->buscarInmueblesBeta($result["IdDemanda"], $_SESSION['IdInmmo'], $result["IdGestion"], $result["IdDestinacion"], $result["IdTipoInm"], $result['IdCiudad'], $result['localidad'], $data["prioridad"], $result["ValInicial"], $result["ValFinal"], $result["estrato_dem"],$data['alcobas'],$data['garaje'],$data['banio'],$data['politicas']);
            if (empty($inmuebles)) {
                return null;
            }
            return $inmuebles;
        } else {
            return print_r($stmt->errorInfo());
        }
    }

    public function buscarInmueblesBeta($coddem, $aa, $tipoGestion, $destinacion, $tipoInmueble, $IdCiudad, $localidad, $prioridad, $valorI, $valorF, $estratoD,$alcobas,$garaje,$banio,$politicas, $migrupo = '')
    {
        //include('../mcomercialweb/grupoinmobiliaria.class.php');
        //include('../mcomercialweb/emailtae2.class.php');

        // echo "$migrupo esto es mi grupo";
        $f_actual = date('Y-m-d');
        $Demandas = new Negocios();
        if ($control == 1) {
            global $f_actual, $w_conexion;
        } else {
            $w_conexion = new MySQL();
            global $f_actual;
        }

		$condicionInmob1 = "";
		$condicion       = "";
		$condicionPol    = "";
        if ($_SESSION['IdInmmo'] != 10 and $_SESSION['IdInmmo'] != 1000 and $_SESSION['IdInmmo'] != 10000 and $_SESSION['IdInmmo'] != 1000000 and $_SESSION['IdInmmo'] != 6310000) {

            //echo $migrupo;
            if (!empty($migrupo)) {
                // $condicionInmob1  .=" AND d.IdInmobiliaria  IN ($migrupo)";
            } else if (!empty($otrosgrupos)) {
                $condicionInmob1 .= " AND d.IdInmobiliaria  NOT IN ($otrosgrupos)";
            } else {
                // $condicionInmob1  .=" AND d.IdInmobiliaria  IN ($aa)";
            }
        }
        /*  echo $migrupo;
        echo "----------- ".$otrosgrupos;*/
        $condSimi = "and d.IdInmobiliaria not in (1,1000,1000000)";
        if ($_SESSION['IdInmmo'] == 1) {
            $condSimi = '';
        }

        $condBloqueo = "";

        $condBloqueo = $this->bloqueoDemandas($coddem, $aa);

        $validaGlobal  = 0;
        $flag          = 0;
        $varTexto      = "";
        $condCiu       = "";
        $condLocalidad = "";
        if ($IdCiudad > 0) {
            $condCiu = " and b.IdCiudad=$IdCiudad";
        }

        /*
        utilizamos la variable prioridad (tabla maestros 45 1 Valor 2 localidad 3 barrio) para afinar las consultas por demandas
        si la variable vale 0 se maneja la lógica abierta
         */
        if ($prioridad == 1) {
            if ($tipoGestion == 1) {
                $condValor = " and ValorCanon between '$valorI' and '$valorF'";
            } elseif ($tipoGestion == 5) {
                $condValor = " and ((ValorCanon between '$valorI' and '$valorF') or(ValorVenta between '$valorI' and '$valorF'))";
            } elseif ($tipoGestion == 5) {
                $condValor = " and ValorVenta between '$valorI' and '$valorF'";
            }

        } elseif ($prioridad == 2) {

            $condLocalidad = " and b.IdLocalidad=$localidad";

        } elseif ($prioridad == 3) {
            $tiene_barrios = getEvaluaCampo('demandas_barrios', "where id_demanda_de=$coddem and estado_de=1", 0);

            /*
            evaluamos si existen barrios para la demanda en la tabla demandas_barrios, si no existen, se hace la consulta por localidad. Si existen
            se hace la consulta con el listado de barrios.
             */
            if ($tiene_barrios == 0) {
                $condLocalidad = " and b.IdLocalidad=$localidad";
            } else {
                $bars          = $this->buscaBarriosLocalidad($coddem);
                $condLocalidad = " and b.IdBarrios in ($bars)";
            }
        } elseif ($prioridad == 4) {
            //$condLocalidad .=" and  d.estrato='$estratoD'";
        } else {

        }
        if($alcobas>0)
        {
        	$condicion .=" and d.alcobas='$alcobas'";
        }
        if($garaje>0)
        {
        	$condicion .=" and d.garaje='$garaje'";
        }
        if($banio>0)
        {
        	$condicion .=" and d.banos='$banio'";
        }
        if($politicas==1)
        {
        	$condicionPol .=" and politica_comp!=''";
        }
        $aplicademandas = "SELECT idInm,IdGestion,IdTpInm,d.IdBarrio,ValorVenta,
            ValorCanon,Estrato,IdDestinacion,AreaConstruida,AreaLote,
            Carrera,Calle,d.IdInmobiliaria,estrato,d.politica_comp
			FROM inmnvo d,barrios b,inmobiliaria i
			WHERE b.IdBarrios=d.IdBarrio
			and i.IdInmobiliaria=d.IdInmobiliaria
			and idEstadoinmueble=2
			and IdTpInm='$tipoInmueble'
			and IdGestion='$tipoGestion'
			and IdDestinacion='$destinacion'
            $condicionPol
            and d.IdInmobiliaria>1
            and i.no_cruce=0
            #and idInm='81-267'
            $condicion
            $condLocalidad
            $condCiu
			$condSimi
			$condicionInmob1
			$condBloqueo
			$condValor
			order by politica_comp desc
            limit 0,1000";

        // echo $aplicademandas." *  $prioridad <hr> <br>";
        // echo $aplicademandas." *  alcobas $alcobas  garaje $garaje banio $banio<hr> <br>";
        $res3    = $w_conexion->ResultSet($aplicademandas); //or die(' error $aplicademandas '.mysql_error());
        $existe3 = $w_conexion->FilasAfectadas($res3);
        //echo $existe3." existe";

        $IdGestion = 0;
        $conCiud   = 0;
        if ($existe3 > 0) {
            $varTexto .= "<ul>";
            $cadInmuebles = '';
            while ($fila77 = $w_conexion->FilaSiguienteArray($res3)) {
                $idInm          = $fila77['idInm'];
                $IdGestion      = $fila77['IdGestion'];
                $IdTpInm        = $fila77['IdTpInm'];
                $IdBarrio       = $fila77['IdBarrio'];
                $ValorVenta     = $fila77['ValorVenta'];
                $ValorCanon     = $fila77['ValorCanon'];
                $Administracion = $fila77['Administracion'];
                $Estrato        = $fila77['Estrato'];
                $politica_comp  = $fila77['politica_comp'];
                $flag_comparte  = strlen($politica_comp);
                $IdDestinacion  = $fila77['IdDestinacion'];
                $AreaConstruida = $fila77['AreaConstruida'];
                $AreaInicial    = $AreaConstruida;
                $AreaLote       = $fila77['AreaLote'];
                $Carrera        = $fila77['Carrera'];
                $estrato        = $fila77['estrato'];
                $Calle          = $fila77['Calle'];
                $IdInmobiliaria = $fila77['IdInmobiliaria'];
                $IdBarrio       = $fila77['IdBarrio'];
                $IdCiudad       = getCampo('barrios', "where IdBarrios=$IdBarrio", 'IdCiudad');
                $iconWarning    = ($flag_comparte > 0) ? '' : "<i class='fa fa-warning' style='color:#f3c022'></i> <label  >Inmueble No ha definido Politicas de Compartir</label>";
                if ($prioridad > 0) {
                    $tiene_barrios1 = getEvaluaCampo('demandas_barrios', "where id_demanda_de=$coddem and estado_de=1", 0);

                    if ($tiene_barrios1 > 0) {

                        //si tiene barrios creamos el listado de barrios llenamos la varialble que se envía a la función aplicainmueble

                        //$IdBarrio=$this->buscaBarriosLocalidad($coddem);
                        $IdBarrio = $fila77['IdBarrio'];
                    } else {

                    }

                }

                // echo "$IdDemanda $IdCiudad <br>";
                if ($IdGestion == 2) {
                    $condG = "1,2,5";
                } elseif ($IdGestion <= 0) {
                    $condG = "0,2";
                } else {
                    $condG = "$IdGestion,2";
                }
                // echo "$idInm IdGestion $IdGestion --- $condG <br>";
                if ($IdTpInm == 1 or $IdTpInm == 11) {
                    $tpInm = "1,11";
                } else {
                    $tpInm = $IdTpInm;
                }
                if ($IdGestion == 1) {
                    $ValInicial = $ValorCanon;
                } else {
                    $ValInicial = $ValorVenta;
                }
                // echo "$IdDemanda --- $IdBarrio <br>";
                //echo "$IdCiudad $IdDemanda  **  $tpInm,$condG,$Calle,$AlaCalle,$Carrera,$AlaCarrera,$IdDestinacion,$ValorVenta,$ValorCanon | $IdBarrio |,$AreaInicial,$AreaFinal,1,<br><br>";

                $conTodos = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 1, '', 0);
                //echo $conTodos."<br>------------------------------<br>";
                $conBarrio = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 2, $conTodos);
                if ($prioridad <= 0) {
                    if ($IdBarrio == 0) {
                        //$conBarrio1=$Demandas->aplicaInmuebleBeta($estrato,$coddem,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$Carrera,$IdDestinacion,$ValInicial,$IdBarrio,$AreaInicial,6,$conTodos,'',$localidad);
                    } else {
                        $conBarrio1 = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 6, $conTodos);
                    }
                } else {
                    if ($tiene_barrios1 == 0) {
                        $conBarrio1 = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 6, $conTodos, '', $localidad);
                    } else {
                        $conBarrio1 = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 6, $conTodos);
                    }
                }
                //echo $conBarrio." barr<br>------------------------------<br>";
                $conArea = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 3, $conTodos);
                //echo $conArea." area<br>------------------------------<br>";
                $conDestinacion = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 4, $conTodos, 10);
                $conDir         = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 5, $conTodos);
                $conEst         = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 7, $conTodos);
                // echo $conArea." conArea<br>------------------------------<br>";
                $aplicaI = $Demandas->validaAplicaInmueble($coddem, $idInm);

                $tieneAceptacion = $this->evaluaFichaDemandas($coddem, $_SESSION["Id_Usuarios"], $idInm);
                $btnEnviaFicha   = ($tieneAceptacion > 0) ? "<button class='btn btn-spartan btn-xs efich' data-toggle='modal' data-target='#modal-efich' title='Enviar Ficha' ><span class='fa fa-share'></span></button>" : "";

                $FichaBtn = "<button class='btn btn-xs btn-info btn-sm fichapdf'  data-toggle='modal'   title='Ficha Inmueble'><span class='glyphicon fa fa-search'></span></button><input type='hidden' id='dinmu' value='" . $idInm . "'>";

                if ($aplicaI == 0) {
                    $btn          = "<button title='Aplicar al Inmueble' type='button' id='aplicarInm' class='btn btn-xs btn-primary aplicarInm'  data-toggle='modal'  data-target='#modal-negos'><span class='fa fa-check-square-o'></span></button>";
                    $descartarBtn = "<button class='btn btn-xs btn-danger btn-sm dct' data-toggle='modal' data-target='#descarta-deman' title='Descartar Inmueble'><span class='glyphicon fa fa-times-circle'></span></button> ";
                } else {
                    $btn          = "<button title='En espera de Respuesta' type='button' id='aplicarInm' class='btn btn-xs btn-primary ' disabled ><span class='fa fa-check-square-o'></span></button>";
                    $descartarBtn = "<button class='btn btn-xs btn-danger btn-sm dctd' title='Descartar Inmueble' disabled><span class='glyphicon fa fa-times-circle' ></span></button> ";
                }
                if ($conTodos == 1) {

                    //echo "<br>La demanda $IdDemanda cumple al 100% <br>";

                    $varTexto .= "<li>" . $bnt . $descartarBtn . $FichaBtn . $btnEnviaFicha . " El Inmueble $idInm cumple al 100% <input type='hidden' class='_demd' value='" . $idInm . "' ><input type='hidden'  class='c_inmu' value='" . $coddem . "' > <i class='fa fa-star' title='Calificacion de la Oportunidad' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i></li> $iconWarning";
                    $flag         = 1;
                    $cadInmuebles = $idInm . "," . $cadInmuebles;
                }
                $ciudOr = getCampo('demandainmuebles', "where IdBarrio and IdDemanda='" . $coddem . "'", 'IdCiudad', 0);
                //echo "ciudOr $ciudOr <br>";
                // echo "$IdDemanda $conTodos==todos and $conBarrio==barrio and $conArea==area and $conDestinacion==destinacion $conDir==dir <br>";
                if ($conTodos == 0 and $conBarrio == 0 and $conBarrio1 == 0 and $conArea == 0 and $conDestinacion == 0 and $IdCiudad == 0 and $conDir == 0 and $conEst == 0) {
                    //echo "$IdDemanda no coincide";

                }

                if ($conTodos == 0 and ($conBarrio1 == 1 or $conBarrio == 1 or $conArea == 1 or $conDestinacion == 1 or $conDir == 1 or $conEst == 1)) // or $IdCiudad==$ciudOr))
                {
                    if ($IdCiudad == $ciudOr) {$conCiud = 1;} else { $conCiud = 0;}
                    $var1         = "";
                    $var2         = "";
                    $var3         = "";
                    $var4         = "";
                    $var5         = "";
                    $var6         = "";
                    $var7         = "";
                    $validaGlobal = $conBarrio + $conBarrio1 + $conArea + $conDestinacion + $conCiud + $conDir + $conEst; //echo "<br>La demanda $IdDemanda ";
                    // echo "$validaGlobal=conBarrio $conBarrio --";
                    // echo "$validaGlobal=conBarrio1 $conBarrio1 --";
                    // echo "$validaGlobal=conArea $conArea--";
                    // echo "$validaGlobal=conDestinacion $conDestinacion --";
                    // echo "$validaGlobal=conCiud $conCiud --";
                    // echo "$validaGlobal=conDir $conDir --";
                    // echo "$validaGlobal=conEst $conEst --";

                    if ($validaGlobal > 1) {
                        $varTexto .= '<li>' . $btn . $descartarBtn . $FichaBtn . $btnEnviaFicha . 'El Inmueble ' . $idInm . '<input type="hidden" class="c_demd" value="' . $idInm . '" > <input type="hidden"  class="c_inmu" value="' . $coddem . '" >';
                        $cadInmuebles = $idInm . "," . $cadInmuebles;
                    }
                    if ($conBarrio1 == 1) {
                        if ($tiene_barrios1 > 0) {
                            $var1 = " Barrio, ";
                        } else {
                            $var1 = " Localidad, ";
                        }
                    } else { $var1 = "";}
                    if ($conBarrio == 1) {$var6 = "";} else { $var6 = "";}
                    if ($conArea == 1) {$var2 = " Area, ";} else { $var2 = "";}
                    if ($conDestinacion == 1) {$var3 = " Destinacion, ";} else { $var3 = "";}
                    if ($conCiud > 0) {$var4 = " Ciudad, ";} else { $var4 = "";}
                    if ($conDir == 1) {$var5 = " Direccion, ";} else { $var5 = "";}
                    if ($conEst == 1) {$var7 = " Estrato, ";} else { $var6 = "";}

                    //echo "<table class='TablaDatos' width='80%' align='center'><tr><td class='tituloFormulario'>Demandas Coincidentes</td></tr> <tr><td>";
                    if ($validaGlobal == 1) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion,  $var1 $var2 $var3 $var4 $var5 Cumple al 44.4%<br>";
                        // $varTexto = $varTexto."Coincide en Tipo de Inmueble, Gestion,  $var1 $var2 $var3 $var4 $var5 Cumple al 44.4% <i class='fa fa-star' style='color:#ec6459'></i></li>";

                        //$flag++;
                    }

                    if ($validaGlobal == 2) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 55.5%<br>";
                        $varTexto = $varTexto . "Coincide en Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 $var7 Cumple al 55.5% <i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li> $iconWarning";
                        $flag++;}

                    if ($validaGlobal == 3) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 66.6%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5  $var7 Cumple al 66.6% <i class='fa fa-star' style='color:#1fb5ad'></i><i class='fa fa-star' style='color:#1fb5ad'></i><i class='fa fa-star' style='color:#1fb5ad'></i></li> $iconWarning";
                        $flag++;}

                    if ($validaGlobal == 4) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 77.7%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5  $var7 Cumple al 77.7% <i class='fa fa-star' style='color:#f3c022'></i><i class='fa fa-star' style='color:#f3c022'></i><i class='fa fa-star' style='color:#f3c022'></i><i class='fa fa-star' style='color:#f3c022'></i><i class='fa fa-star' style='color:#f3c022'></i></li> $iconWarning";
                        $flag++;}

                    if ($validaGlobal == 5) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5  $var7 Cumple al 80% <i class='fa fa-star' style='color:#f3c022'></i><i class='fa fa-star' style='color:#f3c022'></i><i class='fa fa-star' style='color:#f3c022'></i><i class='fa fa-star' style='color:#f3c022'></i></li> $iconWarning";
                        $flag++;}
                    if ($validaGlobal == 6) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5  $var7 Cumple al 89% <i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i></li> $iconWarning";
                        $flag++;}
                    if ($validaGlobal == 7) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5  $var7 Cumple al 95% <i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i></li> $iconWarning";
                        $flag++;}
                    //echo "</td></tr></table>";
                    //$flag=1;

                }

                // echo "Entra a alguna $IdDemanda  - $flag -- validaGlobal $validaGlobal";
            } //while
            // echo $flag."flag";

            if ($flag > 0) {
                $cadInmuebles = substr($cadInmuebles, 0, -1);
                $varTexto .= "<input type='hidden' id='inmus' class='inmus' name='inmus' value='" . $cadInmuebles . "'></ul>";
                //echo "---".$varTexto."---";
                //$envioemail = new EmailTae2();
                //$envioemail->EnvioEmailDemandaL($varTexto,$codinmu);
                //$enviado= $envioemail->ResultadoCorreoDemadaLocalizada();
                //echo $varTexto."--------------";
                return utf8_encode($varTexto);
            }
        } else {
            //echo "No Hay demandas coincidentes";
            return null;
        }
        //$w_conexion->CerrarConexion();
    }
    public function aplicaInmuebleBeta($estratoI, $codinmu, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, $evaluar, $completos, $mostrar = '', $localidad = '')
    {

        $w_conexion = new MySQL();
        global $f_actual;
        $condicionInmob = "";
        $cond2          = "";

        if ($_SESSION['IdInmmo'] == 1 or $_SESSION['IdInmmo'] == 1000 or $_SESSION['IdInmmo'] == 10000 or $_SESSION['IdInmmo'] == 1000000) {
            $condicionInmob = "";
        } else {
            $condicionInmob = "and i.idInmobiliaria not in (1,1000,10000,100000)";
        }

        $ValInicial2  = $ValInicial * 0.8;
        $ValInicial3  = $ValInicial * 1.2;
        $AreaInicial1 = $AreaInicial * 0.3;
        $AreaInicial2 = $AreaInicial * 1.6;

        if ($evaluar == 1) {
            if ($Calle > 0) {$cond2 .= " AND (i.Calle <= '$Calle' AND AlaCalle >='$Calle' )";}
            if ($Carrera > 0) {$cond2 .= " AND (i.Carrera <= '$Carrera' AND AlaCarrera>='$Carrera')";}
            if ($IdDestinacion > 0) {$cond2 .= " AND  i.IdDestinacion = '$IdDestinacion'";}
            if ($IdGestion != 2) {
                $cond2 .= " AND ((i.ValInicial<= '$ValInicial2' and ValFinal>='$ValInicial2')
					  OR (i.ValInicial<= '$ValInicial3' AND ValFinal>='$ValInicial3'))";

            }
            if (!empty($AreaInicial)) {$cond2 .= " AND (i.AreaInicial >= '$AreaInicial1' AND AreaFinal <='$AreaInicial2')";}
            //if(!empty($IdCiudad)){$aplicademandas1=$aplicademandas1." AND b.IdCiudad='".$IdCiudai."'";}
            if (!empty($IdBarrio)) {$cond2 .= " AND i.IdBarrio in ($IdBarrio)";}
        }
        if ($evaluar == 2) {
            if ($IdGestion != 2) {
                $cond2 .= " AND ((i.ValInicial<= '$ValInicial2' and ValFinal>='$ValInicial2')
					  OR (i.ValInicial<= '$ValInicial3' AND ValFinal>='$ValInicial3'))";
            }

        }
        if ($evaluar == 3) {
            if ($AreaInicial > 0) {
                $cond2 .= " AND (i.AreaInicial >= '$AreaInicial1' AND AreaFinal <='$AreaInicial2')";
            }

        }
        if ($evaluar == 4) {

            if ($AreaInicial > 0) {
                $cond2 .= " AND (i.AreaInicial >= '$AreaInicial1' AND AreaFinal <='$AreaInicial2')";
            }
            if ($IdDestinacion > 0) {$cond2 .= " AND  i.IdDestinacion = '$IdDestinacion'";}
            if ($IdGestion != 2) {
                $cond2 .= " AND ((i.ValInicial<= '$ValInicial2' and ValFinal>='$ValInicial2')
					  OR (i.ValInicial<= '$ValInicial3' AND ValFinal>='$ValInicial3'))";
            }

        }
        if ($evaluar == 5) {
            if ($Calle > 0) {$cond2 .= " AND (i.Calle <= '$Calle' AND AlaCalle >='$Calle')";}
            if ($Carrera > 0) {$cond2 .= " AND (i.Carrera <= '$Carrera' AND AlaCarrera>='$Carrera')";}
            if ($IdDestinacion > 0) {$cond2 .= " AND  i.IdDestinacion = '$IdDestinacion'";}
            if ($IdGestion != 2) {
                $cond2 .= " AND ((i.ValInicial<= '$ValInicial2' and ValFinal>='$ValInicial2')
					  OR (i.ValInicial<= '$ValInicial3' AND ValFinal>='$ValInicial3'))";
            }
            if ($AreaInicial > 0) {
                $cond2 .= " AND (i.AreaInicial >= '$AreaInicial1' AND AreaFinal <='$AreaInicial2')";
            }
        }
        if ($evaluar == 6) {
            if ($localidad) {
                $cond2 .= " AND i.localidad ='$localidad' ";
            } else {
                if (!empty($IdBarrio)) {$cond2 .= " AND i.IdBarrio in ($IdBarrio) ";}
            }

        }
        if ($evaluar == 8) {

            if (!empty($IdBarrio)) {$cond2 .= " AND i.IdBarrio in ($IdBarrio) ";}

        }
        if ($evaluar == 7) {
            if ($estratoI > 0) {
                $cond2 .= " AND i.estrato_dem='$estratoI'";
            }

        }
        if ($IdCiudad > 0) {
            $cond2 .= " AND i.IdCiudad ='$IdCiudad'";
        }

        // echo "AreaInicial $AreaInicial -<br>";
        $aplicainmuebles1 = "SELECT *
				FROM demandainmuebles i
				WHERE i.IdTipoInm in ($tpInm)
				AND i.IdGestion in ($condG)
				and i.IdDemanda='$codinmu'
				#and IdDemanda=1166
				$cond2
				$condicionInmob
                ";
        // echo $aplicainmuebles1." $evaluar seiss ---------<br><br>";
        if ($mostrar == 1) {
            echo $aplicainmuebles1 . "<br><br>";
        }
        $res = $w_conexion->ResultSet($aplicainmuebles1) or die("error evaluar inm  $aplicainmuebles1<br>" . mysql_error());
        {
            $existe = $w_conexion->FilasAfectadas($res);
            //echo $existe." existe";
            if ($existe > 0) {
                /* while($ff=$w_conexion->FilaSiguienteArray($res))
                {
                //$idDem=  $ff['idInm'];
                //$idDemT= $idDem.",".$idDemT;
                }*/
                //$idDemT=substr($idDemT,0,-1);
                // echo $existe."ffff";
                return $existe;
            }
        }
    }
    public function bloqueoDemandas($coddem, $aa)
    {
        $w_conexion       = new MySQL();
        $descartademandas = "SELECT inmu_dem
			FROM descarta_demandas
			WHERE  cod_dem='$coddem'
			and inmob_dem=$aa
			and res_dem=1
			and tp_dem=1";

        // echo $aplicademandas."<br>";
        $res4    = $w_conexion->ResultSet($descartademandas);
        $existe4 = $w_conexion->FilasAfectadas($res4);

        if ($existe4 > 0) {
            $cont = $existe4 + 1;
            while ($ff = $w_conexion->FilaSiguienteArray($res4)) {
                $inmu_dem = $ff['inmu_dem'];
                $cade     = "'" . $inmu_dem . "'," . $cade . "'";
            }
            if ($existe4 == 1) {
                $cade = substr($cade, 0, -2);
            } else {
                $cade = substr($cade, 0, -$cont);
            }
            $condBloqueo .= "and d.idInm not in ($cade) ";
            return $condBloqueo;
        }
    }
    public function buscaBarriosLocalidad($coddem)
    {
        $w_conexion = new MySQL();
        /*
        construimos la cadena de barrios
         */
        $barrDem = "SELECT id_barrio_de
		FROM demandas_barrios
		WHERE  id_demanda_de='$coddem'
		and estado_de=1";

        // echo $aplicademandas."<br>";
        $res5    = $w_conexion->ResultSet($barrDem);
        $existe5 = $w_conexion->FilasAfectadas($res5);

        if ($existe5 > 0) {
            while ($ff5 = $w_conexion->FilaSiguienteArray($res5)) {
                $id_barrio_de = $ff5['id_barrio_de'];
                $cade5        = $id_barrio_de . "," . $cade5;
            }
            $cade5 = substr(trim($cade5), 0, -1);

            $condLocalidad = " and b.IdBarrios in ($cade5)";
        }
        return $cade5;
    }
    public function traeBitacoraDemandas($data)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare('SELECT tipo_log,campo_g,campo_usu_g,esta_ini_g,esta_fin_g,
     					fecha_g,hora_g,usuario_g,ip_g,client_g,param_g	pk
 						FROM lg_ch_tb a
                        WHERE a.`client_g`=:client_g
                        AND tipo_log=1');

        if ($stmt->execute(array(

            ":client_g" => $data['client_g'],

        ))) {
            $response = array();
            while ($row = $stmt->fetch()) {
                if ($row['campo_usu_g'] == 'Gestion') {
                    $estadoIni = getCampo('gestioncomer', "where IdGestion=" . $row['esta_ini_g'], 'NombresGestion');
                    $estadoFin = getCampo('gestioncomer', "where IdGestion=" . $row['esta_fin_g'], 'esta_fin_g');
                }
                if ($row['campo_usu_g'] == 'Barrio') {
                    $estadoIni = getCampo('barrios', "where IdBarrios=" . $row['esta_ini_g'], 'NombreB');
                    $estadoFin = getCampo('barrios', "where IdBarrios=" . $row['esta_fin_g'], 'NombreB');
                }

                if ($row['campo_usu_g'] == 'IdTipoInm') {
                    $estadoIni = getCampo('tipoinmuebles', "where idTipoInmueble=" . $row['esta_ini_g'], 'Descripcion');
                    $estadoFin = getCampo('tipoinmuebles', "where idTipoInmueble=" . $row['esta_fin_g'], 'Descripcion');
                }

                $response[] = array(
                    "campo_g"     => utf8_encode($row['campo_g']),
                    "campo_usu_g" => utf8_encode($row['campo_usu_g']),
                    "esta_ini_g"  => utf8_encode($estadoIni),
                    "esta_fin_g"  => utf8_encode($estadoFin),
                    "fecha_g"     => $row['fecha_g'],
                    "hora_g"      => $row['hora_g'],
                    "usuario_g"   => utf8_encode(getCampo('usuarios', "where Id_Usuarios='" . $row['usuario_g'] . "'", 'concat(Nombres," ",apellidos)')),
                    "ip_g"        => $row['ip_g'],
                );
            }
            return $response;
        } else {
            return $stmt->errorInfo();
            $stmt = null;
        }
    }
    public function evaluaFichaDemandas($cod_deman_cm, $usu_acepta, $idInm)
    {
        $connPDO = new Conexion();
        /*
        Se verifica que la demanda sea aceptada para permitir enviar la ficha
         */

        $stmt = $connPDO->prepare('SELECT cod_deman_cm
 						FROM condiciones_compartir_inm
                        WHERE cod_deman_cm	=:cod_deman_cm
                        AND usu_acepta		=:usu_acepta
                        AND id_inmu_cm 		=:idInm
                        AND acepta_cm		=1');

        if ($stmt->execute(array(

            ":cod_deman_cm" => $cod_deman_cm,
            ":usu_acepta"   => $usu_acepta,
            ":idInm"        => $idInm,

        ))) {
            $response = $stmt->rowCount();
            return $response;
        } else {
            return $stmt->errorInfo();
            $stmt = null;
        }
    }
    public function saveLogPoliticasComp($data)
    {
        $connPDO = new Conexion();
         $conse = consecutivo('conse_poli', 'log_seg_activa_politicas');
         list($inmob,$inmueble)=explode("-",$data['codInmueble']);
         $userSolicita=$_SESSION['codUser'];
         $inmobiliariaSolicita=$_SESSION['IdInmmo'];
         $token = $this->generate_token($len = 80);
        $stmt=$connPDO->prepare("INSERT INTO log_seg_activa_politicas (conse_poli,inmob_poli,inmueble_poli,user_poli,token_poli,inmobten_poli) VALUES (:conse_poli,:inmo_poli,:inmueble_poli,:user_poli,:token_poli,:inmobten_poli)");

        if($stmt->execute(array(
                ":conse_poli"=>$conse,
                ":inmo_poli"=>$inmobiliariaSolicita,
                ":inmueble_poli"=>$inmueble,
                ":user_poli"=>$userSolicita,
                ":token_poli"=>$token,
                ":inmobten_poli"=>$inmob

            )))
        {
            if($stmt->rowCount()>0)
            {
                return $data[]=array("Error"=>0,"token"=>$token,"inmobiliaria"=> $inmobiliariaSolicita,"usuario"=>$userSolicita,"inmueble"=>$data['codInmueble']);
            }else
            {
                return $data[]=array("Error"=>1,"msn"=>"No fue posible Enviar la solicitud vuelva a intentarlo");
            }
        }else
        {
            return $data[]=array("Error"=>1,"msn"=>$stmt->errorInfo(),"inmo"=>$conse);
        }

    }
    public function verifyToken($data)
    {
        if(!empty($data['hash']))
        { 
            $connPDO = new Conexion();
            list($inmob,$inmueble)=explode("-",$data['codInmueble']);
            $stmt=$connPDO->prepare("SELECT est_poli,token_poli FROM 
                                     log_seg_activa_politicas WHERE
                                     inmueble_poli=:inmueble_poli
                                     and token_poli =:token_poli 
                                     
                                     ");
            if($stmt->execute(array(
                    ":inmueble_poli"=>$inmueble,
                    ":token_poli"=>$data['hash'],

                )))
            {   
                if($stmt->rowCount())
                {
                    $result = $stmt->fetch(PDO::FETCH_ASSOC);

                    if($result['est_poli']==0)
                    {
                        return $response[]=array("Error"=>0,"msn"=>"Peticion sin contestar");
                    }
                    if($result['est_poli']==1)
                    {
                        return $response[]=array("Error"=>1,"msn"=>'<b><i class="fa fa-check-square"></i> Esta petición fue Aceptada anteriormente, la inmobiliaria aplicara.</b>',"status"=>"alert-success");
                    }
                    if($result['est_poli']==2){
                        return $response[]=array("Error"=>1,"msn"=>'<b><i class="fa fa-info"></i> Esta petición fue rechazada anteriormente</b>',"status"=>"alert-info");
                    }   
                }else{
                    return $response[]=array("Error"=>1,"msn"=>'<b><i class="fa fa-info"></i> Peticion Errada</b>',"status"=>"alert-danger");
                }
            }else
            {
                return $response[]=array("Error"=>1,"msn"=>$stmt->errorInfo());
            }
        }else{
            if($inmob==$_SESSION['IdInmmo'])
            {
                 return $response[]=array("Error"=>2,"msn"=>"Misma inmobiliaria","status"=>1);
            }else
            {
                 return $response[]=array("Error"=>2,"msn"=>"Diferente inmobiliaria","status"=>2);
            }
        }

       
    }
    public function verifyInvitacion($data)
    {
        $connPDO = new Conexion();
        list($inmob,$inmueble)=explode("-",$data['codInmueble']);

        $nombreInmob=getCampo("inmobiliaria","where IdInmobiliaria='" . $inmob . "'","Nombre");
        $stmt=$connPDO->prepare("SELECT est_poli,token_poli FROM 
                                 log_seg_activa_politicas WHERE
                                 inmueble_poli=:inmueble_poli
                                 and user_poli =:user_poli 
                                 ");
        if($stmt->execute(array(
                ":inmueble_poli"=>$inmueble,
                ":user_poli"=>$_SESSION['codUser']
            )))
        {   
            if($stmt->rowCount())
            {
                $result  = $stmt->fetch(PDO::FETCH_ASSOC);
                if($result['est_poli']==0)
                {

                    return $response[]=array("Error"=>1,"msn"=>'<b><i class="fa fa-info-circle"></i>Su petición esta en proceso, se le informara por correo electronico o por las notificaciones emergentes</b>',"status"=>1);
                }
                if($result['est_poli']==1)
                {

                    return $response[]=array("Error"=>1,"msn"=>"Peticion Aceptada","status"=>2);
                }
                if($result['est_poli']==2)
                {

                    return $response[]=array("Error"=>1,"msn"=>'<b><i class="fa fa-info-circle"></i>La inmobiliaria '.$nombreInmob.' no desea compartir el inmueble en esta ocasion.</b>',"status"=>3);
                }
            }else{
                return $response[]=array("Error"=>0,"msn"=>"no tiene ivitacion");
            }
        }else
        {
            return $response[]=array("Error"=>1,"msn"=>$stmt->errorInfo());
        }
    }
    public function verifyStatus($data)
    {
        $connPDO = new Conexion();
        list($inmob,$inmueble)=explode("-",$data['codInmueble']);

        $nombreInmob=getCampo("inmobiliaria","where IdInmobiliaria='" . $inmob . "'","Nombre");
        $stmt=$connPDO->prepare("SELECT est_poli,token_poli FROM 
                                 log_seg_activa_politicas WHERE
                                 inmueble_poli=:inmueble_poli
                                 ");
        if($stmt->execute(array(
                ":inmueble_poli"=>$inmueble,
            )))
        {   
            if($stmt->rowCount()>0)
            {
                $result  = $stmt->fetch(PDO::FETCH_ASSOC);
                if($result['est_poli']==0)
                {

                    return $response[]=array("Error"=>0,"msn"=>'<b><i class="fa fa-info-circle"></i></b>',"status"=>0);
                }
                if($result['est_poli']==1)
                {

                    return $response[]=array("Error"=>0,"msn"=>"Peticion Aceptada","status"=>1);
                }
                if($result['est_poli']==2)
                {

                    return $response[]=array("Error"=>0,"msn"=>'<b><i class="fa fa-info-circle"></i>La inmobiliaria '.$nombreInmob.' no quiere compartir el inmueble en estos momentos.</b>',"status"=>2);
                }
            }else{
                return $response[]=array("Error"=>2,"msn"=>"no tiene peticiones de actualizacion de politicas");
            }
        }else
        {
            return $response[]=array("Error"=>1,"msn"=>$stmt->errorInfo());
        }
    }
    public function actualizaPoliticas($data)
    {
        $connPDO = new Conexion();
        list($inmo,$inmu)=explode("-",$data['idinm']);
        $stmt= $connPDO->prepare("UPDATE inmnvo
                SET ValComiArr = :ValComiArr, ComiArren = :ComiArren ,ValComiVenta=:ValComiVenta,ComiVenta=:ComiVenta,politica_comp=:politica_comp 
                WHERE idInm=:idInm 
                AND IdInmobiliaria=:IdInmobiliaria");
        if($stmt->execute(array(
                ":ValComiArr"=>$data['ValComiArr'],
                ":ComiArren"=>$data['ComiArren']/100,
                ":ValComiVenta"=>$data['ValComiVenta'],
                ":ComiVenta"=>$data['ComiVenta']/100,
                ":politica_comp"=>$data['politica_comp'],
                ":idInm"=>$data['idinm'],
                ":IdInmobiliaria"=>$inmo
            )))
        {   
            if($stmt->rowCount()>0)
            {
                $actPoliticas=$this->updatePoliticasInmueblesOld($data);
                if($actPoliticas==1)
                {
                   $actestadiPoliticas=$this->updateStatusPoliticas($inmu,1,"Se acepto las actualización de politicas");
                   

                        return $response[] = array("Error"=>0,"msn"=>"Politicas Actualizadas Correctamente","text"=>"Ya puede Iniciar cruce de Negocios","func"=> "actualizaPoliticas","datos"=>$this-> getUserPeticionActPoliticas($inmu,2)); 
                   
                }else{
                    return $actPoliticas;
                }

            }else
            {
               return $response[] = array("Error"=>2,"msn"=>"No realizo ningun cambio de politcas","text"=>"Vuelva a intentarlo por favor" ,"func"=> "actualizaPoliticas"); 
            }
        }else{
            return $response[] =array("Error"=>1,"msn"=>$stmt->errorInfo(),"data"=>$data,"func"=> "actualizaPoliticas"); 
        }
    }
    public function updatePoliticasInmueblesOld($data)
    {
         $connPDO = new Conexion();
         list($inmo,$inmu)=explode("-",$data['idinm']);
        
        $stmt= $connPDO->prepare("UPDATE inmuebles
                SET ValComiArr = :ValComiArr, ComiArren = :ComiArren ,ValComiVenta=:ValComiVenta,ComiVenta=:ComiVenta,politica_comp=:politica_comp 
                WHERE idInm=:idInm 
                AND IdInmobiliaria=:IdInmobiliaria");
        if($stmt->execute(array(
                ":ValComiArr"=>$data['ValComiArr'],
                ":ComiArren"=>$data['ComiArren']/100,
                ":ValComiVenta"=>$data['ValComiVenta'],
                ":ComiVenta"=>$data['ComiVenta']/100,
                ":politica_comp"=>$data['politica_comp'],
                ":idInm"=>$data['idinm'],
                ":IdInmobiliaria"=>$inmo
            )))
        {   
            if($stmt->rowCount()>0)
            {
                return 1;
            }else
            {
                $response[] = array("Error"=>1,"msn"=>"No se actualizo la politca","text"=>"Vuelva a intentarlo por favor" ,"func"=> "updatePoliticasInmueblesOld"); 
            }
        }else{
            return $response[] =array("Error"=>1,"msn"=>$stmt->errorInfo(),"func"=> "updatePoliticasInmueblesOld"); 
        }
    }
    public function getUserPeticionActPoliticas($imueble,$status)
    {
         $connPDO = new Conexion();
         $sql="";
         if($status==2)
         {
            $sql .=" and est_poli != 2"; 
         }
          if($status==1)
         {
            $sql .=" and est_poli = 0"; 
         }
        $stmt=$connPDO->prepare("SELECT user_poli,inmob_poli FROM 
                                 log_seg_activa_politicas WHERE
                                 inmueble_poli=:inmueble_poli
                                 and inmobten_poli=:inmobten_poli
                                 $sql
                                 ");
        if($stmt->execute(array(
                ":inmueble_poli"=>$imueble,
                ":inmobten_poli"=>$_SESSION['IdInmmo']
            )))
        {   
            
                while ($row=$stmt->fetch()) {
                    $data[]=array("usuarios"=>$row['user_poli'],"inmobiliaria"=>$row["inmob_poli"]);
                }
                return $response=array("Error"=>0,"usuarios"=>$data);
            
        }else
        {
            return $response[]=array("Error"=>1,"msn"=>$stmt->errorInfo());
        }
    }
    public function updateStatusPoliticas($inmueble,$estado,$mensaje)
    {
        $connPDO = new Conexion();

        $usuarios=$this->getUserPeticionActPoliticas($inmueble,1);
        
        foreach ($usuarios['usuarios'] as $key => $value) {
          
       
        $stmt=$connPDO->prepare("UPDATE log_seg_activa_politicas
                SET est_poli = :est_poli, asesorinm_poli = :asesorinm_poli ,comentarios_poli=:comentarios_poli,inmobten_poli=:inmobten_poli 
                WHERE inmueble_poli=:idInm 
                AND user_poli=:user_poli
                and est_poli = 0");
        if($stmt->execute(array(
            ":est_poli"=>$estado,
            ":asesorinm_poli"=>$_SESSION['codUser'],
            ":comentarios_poli"=>$mensaje,
            ":idInm"=>$inmueble,
            ":user_poli"=>$value['usuarios'],
            ":inmobten_poli"=>$_SESSION['IdInmmo']
            )))
        {
            if($stmt->rowCount())
            {
                // return 1;
            }else{
               // return $response[]=array("Error"=>1,"msn"=>"No genero cambios"); 
            }
        }else{
            // return $response[]=array("Error"=>1,"msn"=>$stmt->errorInfo());
        }
         }
    }
    public function updateStatusPoliticasIndividual($inmueble,$estado,$mensaje,$token)
    {
        $connPDO = new Conexion();

        $stmt=$connPDO->prepare("UPDATE log_seg_activa_politicas
                SET est_poli = :est_poli, asesorinm_poli = :asesorinm_poli ,comentarios_poli=:comentarios_poli,inmobten_poli=:inmobten_poli  
                WHERE inmueble_poli=:idInm 
                and est_poli = 0
                and token_poli =:token_poli ");
        if($stmt->execute(array(
            ":est_poli"=>$estado,
            ":asesorinm_poli"=>$_SESSION['codUser'],
            ":comentarios_poli"=>$mensaje,
            ":idInm"=>$inmueble,
            ":token_poli"=>$token,
            ":inmobten_poli"=>$_SESSION['IdInmmo']
            )))
        {
            if($stmt->rowCount())
            {
                $info=$this->getInvitaciones($inmueble,$token);
                return $response[]=array("Error"=>0,"msn"=>"Se actulizo Correctamente","status"=>"Rechazo la invitación de la ".$info['Inmobiliaria']." para actualizar las politcas del inmueble realizar cruce de negocios");
            }else{
                return $response[]=array("Error"=>1,"msn"=>"No genero cambios"); 
            }
        }else{
            return $response[]=array("Error"=>1,"msn"=>$stmt->errorInfo());
        }
        
    }
    public function getInvitaciones($inmueble,$token)
    {
        $connPDO = new Conexion();
        $sql="";
        if($token != ""){
            $sql.=" AND token_poli=:token_poli";
           } 
         $stmt=$connPDO->prepare("SELECT inmob_poli,inmueble_poli,user_poli,est_poli,inmobten_poli,asesorinm_poli,comentarios_poli,token_poli 
                                 FROM log_seg_activa_politicas 
                                 WHERE inmueble_poli=:inmueble_poli
                                 $sql
                                 ");
          if($token != ""){
            $stmt->bindParam(":token_poli",$token);
           }
           $stmt->bindParam(":inmueble_poli",$inmueble); 
        if($stmt->execute())
        {
            while ($row=$stmt->fetch()) {
               
                
                $token='<input type="hidden" name="" id="tokenPoliacc" class="tokenPoli" value="'.$row['token_poli'].'" >';
                 $inmob='<input type="hidden" name="" id="idInmoPoli" class="idInmoPoli" value="'.$row['inmob_poli'].'" >';
                $btnAceptar='<button type="button" class="btn btn-spartan btn-tableActPoliticas btn-xs btn-accionPolits" data-toggle="tooltip" data-placement="bottom" title="Aceptar Invitación"><i class="fa fa-check"></i></button> ';


                $btnRechazar='<button type="button" class="btn btn-watermelon btn-tableDescPoliti btn-xs btn-accionPolits" data-toggle="tooltip" data-placement="bottom" title="Descartar Invitación"><i class="fa fa-remove"></i></button>';

                $inmobiliaria=getCampo("inmobiliaria","where IdInmobiliaria=".$row['inmob_poli'],"NombreInm");
                $estado="";
                $msnInmob="";
                if($row['est_poli']==0)
                {
                    $estado.=$btnAceptar.$btnRechazar;
                    $msnInmob.="La inmobiliaria <b>".ucfirst(strtolower($inmobiliaria))."</b> desea hacer cruce de Negocios con el inmueble 1-14058, proceda a la actualizacion de politicas a compartir";
                }
                if($row['est_poli']==1)
                {
                    $estado.='<span class="label label-success">Aceptado</span> '.$btnRechazar;
                    $msnInmob.="La actualización de politicas a compartir se realizo para que la  inmobiliaria <b>".ucfirst(strtolower($inmobiliaria))."</b> pueda hacer cruce de negocios.";
                }
                if($row['est_poli']==2)
                {
                    $estado.='<span class="label label-danger">Rechazado</span> '.$btnAceptar;
                     $msnInmob.="La actualización de politicas a compartir se rechazo para que la  inmobiliaria <b>".ucfirst(strtolower($inmobiliaria))."</b>no pueda hacer cruce de negocios con este inmueble.";
                }
                $logo=(getCampo("clientessimi","where IdInmobiliaria=".$row['inmob_poli'],"Logo")!="")?"https://www.simiinmobiliarias.com/logos/".getCampo("clientessimi","where IdInmobiliaria=".$row['inmob_poli'],"Logo"):"";

                $data[]=array(

                    "Inmobiliaria"=>'<div class="imgBackgroundPoli" style="background-image:url('.$logo.')"></div>'.$msnInmob,
                    "inmueble"=>$row['inmobten_poli']."-".$inmueble,
                    "user_poli"=>$row['user_poli']."-----".$row['inmob_poli'],
                    "acciones"=>$estado.$token.$inmob,
                    "logos"=>"",

                );

            }
            return $data;
        }else{
            return $response[]=array("Error"=>1,"msn"=>$stmt->errorInfo());
        }

    }
    public function generate_token($len = 40)
    {
        //un array perfecto para crear claves
        $chars = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        );
        //desordenamos el array chars
        shuffle($chars);
        $num_chars = count($chars) - 1;
        $token = '';
 
        //creamos una key de 40 carácteres
        for ($i = 0; $i < $len; $i++)
        {
            $token .= $chars[mt_rand(0, $num_chars)];
        }
        return $token;
    }
    public function buscarInmueblesBetaCallCruce($codinm,$coddem, $aa)
    {
        //include('../mcomercialweb/grupoinmobiliaria.class.php');
        //include('../mcomercialweb/emailtae2.class.php');

        // echo "$migrupo esto es mi grupo";
        $f_actual = date('Y-m-d');
        $Demandas = new Negocios();
        if ($control == 1) {
            global $f_actual, $w_conexion;
        } else {
            $w_conexion = new MySQL();
            global $f_actual;
        }

        $condicionInmob1 = "";
        $condicion       = "";
        $condicionPol    = "";
        if ($_SESSION['IdInmmo'] != 10 and $_SESSION['IdInmmo'] != 1000 and $_SESSION['IdInmmo'] != 10000 and $_SESSION['IdInmmo'] != 1000000 and $_SESSION['IdInmmo'] != 6310000) {

            //echo $migrupo;
            if (!empty($migrupo)) {
                // $condicionInmob1  .=" AND d.IdInmobiliaria  IN ($migrupo)";
            } else if (!empty($otrosgrupos)) {
                $condicionInmob1 .= " AND d.IdInmobiliaria  NOT IN ($otrosgrupos)";
            } else {
                // $condicionInmob1  .=" AND d.IdInmobiliaria  IN ($aa)";
            }
        }
        /*  echo $migrupo;
        echo "----------- ".$otrosgrupos;*/
        $condSimi = "and d.IdInmobiliaria not in (1,1000,1000000)";
        if ($_SESSION['IdInmmo'] == 1) {
            $condSimi = '';
        }

        $condBloqueo = "";

        $condBloqueo = $this->bloqueoDemandas($coddem, $aa);

        $validaGlobal  = 0;
        $flag          = 0;
        $varTexto      = "";
        $varTexto1      = "";
        $varTexto2      = "";
        $varTexto3      = "";
        $condCiu       = "";
        $condLocalidad = "";
        if ($IdCiudad > 0) {
            $condCiu = " and b.IdCiudad=$IdCiudad";
        }

        /*
        utilizamos la variable prioridad (tabla maestros 45 1 Valor 2 localidad 3 barrio) para afinar las consultas por demandas
        si la variable vale 0 se maneja la lógica abierta
         */
        if ($prioridad == 1) {
            if ($tipoGestion == 1) {
                $condValor = " and ValorCanon between '$valorI' and '$valorF'";
            } elseif ($tipoGestion == 5) {
                $condValor = " and ((ValorCanon between '$valorI' and '$valorF') or(ValorVenta between '$valorI' and '$valorF'))";
            } elseif ($tipoGestion == 5) {
                $condValor = " and ValorVenta between '$valorI' and '$valorF'";
            }

        } elseif ($prioridad == 2) {

            $condLocalidad = " and b.IdLocalidad=$localidad";

        } elseif ($prioridad == 3) {
            $tiene_barrios = getEvaluaCampo('demandas_barrios', "where id_demanda_de=$coddem and estado_de=1", 0);

            /*
            evaluamos si existen barrios para la demanda en la tabla demandas_barrios, si no existen, se hace la consulta por localidad. Si existen
            se hace la consulta con el listado de barrios.
             */
            if ($tiene_barrios == 0) {
                $condLocalidad = " and b.IdLocalidad=$localidad";
            } else {
                $bars          = $this->buscaBarriosLocalidad($coddem);
                $condLocalidad = " and b.IdBarrios in ($bars)";
            }
        } elseif ($prioridad == 4) {
            //$condLocalidad .=" and  d.estrato='$estratoD'";
        } else {

        }
        if($alcobas>0)
        {
            $condicion .=" and d.alcobas='$alcobas'";
        }
        if($garaje>0)
        {
            $condicion .=" and d.garaje='$garaje'";
        }
        if($banio>0)
        {
            $condicion .=" and d.banos='$banio'";
        }
        if($politicas==1)
        {
            //$condicionPol .=" and politica_comp!=''";
        }
        $info=array();
        $aplicademandas = "SELECT idInm,IdGestion,IdTpInm,d.IdBarrio,ValorVenta,
            ValorCanon,Estrato,IdDestinacion,AreaConstruida,AreaLote,
            Carrera,Calle,d.IdInmobiliaria,estrato,d.politica_comp
            FROM inmnvo d,barrios b,inmobiliaria i
            WHERE b.IdBarrios=d.IdBarrio
            and i.IdInmobiliaria=d.IdInmobiliaria
            and idEstadoinmueble in (2,4)
            and i.no_cruce=0
            $condicionPol
            and d.IdInmobiliaria='$aa'
            and codinm='$codinm'
           
            limit 0,1";

        // echo $aplicademandas." *  $prioridad <hr> <br>";
        // echo $aplicademandas." *  alcobas $alcobas  garaje $garaje banio $banio<hr> <br>";
        $res3    = $w_conexion->ResultSet($aplicademandas); //or die(' error $aplicademandas '.mysql_error());
        $existe3 = $w_conexion->FilasAfectadas($res3);
        //echo $existe3." existe";

        $IdGestion = 0;
        $conCiud   = 0;
        if ($existe3 > 0) {
            $varTexto .= "";
            $cadInmuebles = '';
            while ($fila77 = $w_conexion->FilaSiguienteArray($res3)) {
                $idInm          = $fila77['idInm'];
                $IdGestion      = $fila77['IdGestion'];
                $IdTpInm        = $fila77['IdTpInm'];
                $IdBarrio       = $fila77['IdBarrio'];
                $ValorVenta     = $fila77['ValorVenta'];
                $ValorCanon     = $fila77['ValorCanon'];
                $Administracion = $fila77['Administracion'];
                $Estrato        = $fila77['Estrato'];
                $politica_comp  = $fila77['politica_comp'];
                $flag_comparte  = strlen($politica_comp);
                $IdDestinacion  = $fila77['IdDestinacion'];
                $AreaConstruida = $fila77['AreaConstruida'];
                $AreaInicial    = $AreaConstruida;
                $AreaLote       = $fila77['AreaLote'];
                $Carrera        = $fila77['Carrera'];
                $estrato        = $fila77['estrato'];
                $Calle          = $fila77['Calle'];
                $IdInmobiliaria = $fila77['IdInmobiliaria'];
                $IdBarrio       = $fila77['IdBarrio'];
                $IdCiudad       = getCampo('barrios', "where IdBarrios=$IdBarrio", 'IdCiudad');
                $iconWarning    = ($flag_comparte > 0) ? '' : "<i class='fa fa-warning' style='color:#f3c022'></i> <label  >Inmueble No ha definido Politicas de Compartir</label>";
                if ($prioridad > 0) {
                    $tiene_barrios1 = getEvaluaCampo('demandas_barrios', "where id_demanda_de=$coddem and estado_de=1", 0);

                    if ($tiene_barrios1 > 0) {

                        //si tiene barrios creamos el listado de barrios llenamos la varialble que se envía a la función aplicainmueble

                        //$IdBarrio=$this->buscaBarriosLocalidad($coddem);
                        $IdBarrio = $fila77['IdBarrio'];
                    } else {

                    }

                }

                // echo "$IdDemanda $IdCiudad <br>";
                if ($IdGestion == 2) {
                    $condG = "1,2,5";
                } elseif ($IdGestion <= 0) {
                    $condG = "0,2";
                } else {
                    $condG = "$IdGestion,2";
                }
                // echo "$idInm IdGestion $IdGestion --- $condG <br>";
                if ($IdTpInm == 1 or $IdTpInm == 11) {
                    $tpInm = "1,11";
                } else {
                    $tpInm = $IdTpInm;
                }
                if ($IdGestion == 1) {
                    $ValInicial = $ValorCanon;
                } else {
                    $ValInicial = $ValorVenta;
                }
                // echo "$IdDemanda --- $IdBarrio <br>";
                //echo "$IdCiudad $IdDemanda  **  $tpInm,$condG,$Calle,$AlaCalle,$Carrera,$AlaCarrera,$IdDestinacion,$ValorVenta,$ValorCanon | $IdBarrio |,$AreaInicial,$AreaFinal,1,<br><br>";

                $conTodos = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 1, '', 0);
                //echo $conTodos."<br>------------------------------<br>";
                $conBarrio = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 2, $conTodos);
                if ($prioridad <= 0) {
                    if ($IdBarrio == 0) {
                        //$conBarrio1=$Demandas->aplicaInmuebleBeta($estrato,$coddem,$IdCiudad,$IdGestion,$tpInm,$condG,$Calle,$Carrera,$IdDestinacion,$ValInicial,$IdBarrio,$AreaInicial,6,$conTodos,'',$localidad);
                    } else {
                        $conBarrio1 = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 6, $conTodos);
                    }
                } else {
                    if ($tiene_barrios1 == 0) {
                        $conBarrio1 = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 6, $conTodos, '', $localidad);
                    } else {
                        $conBarrio1 = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 6, $conTodos);
                    }
                }
                //echo $conBarrio." barr<br>------------------------------<br>";
                $conArea = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 3, $conTodos);
                //echo $conArea." area<br>------------------------------<br>";
                $conDestinacion = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 4, $conTodos, 10);
                $conDir         = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 5, $conTodos);
                $conEst         = $Demandas->aplicaInmuebleBeta($estrato, $coddem, $IdCiudad, $IdGestion, $tpInm, $condG, $Calle, $Carrera, $IdDestinacion, $ValInicial, $IdBarrio, $AreaInicial, 7, $conTodos);
                // echo $conArea." conArea<br>------------------------------<br>";
                $aplicaI = $Demandas->validaAplicaInmueble($coddem, $idInm);

                $tieneAceptacion = $this->evaluaFichaDemandas($coddem, $_SESSION["Id_Usuarios"], $idInm);
                $btnEnviaFicha   = ($tieneAceptacion > 0) ? "<button class='btn btn-spartan btn-xs efich' data-toggle='modal' data-target='#modal-efich' title='Enviar Ficha' ><span class='fa fa-share'></span></button>" : "";

                $FichaBtn = "<button class='btn btn-xs btn-info btn-sm fichapdf'  data-toggle='modal'   title='Ficha Inmueble'><span class='glyphicon fa fa-search'></span></button><input type='hidden' id='dinmu' value='" . $idInm . "'>";

                if ($aplicaI == 0) {
                    $btn          = "<button title='Aplicar al Inmueble' type='button' id='aplicarInm' class='btn btn-xs btn-primary aplicarInm'  data-toggle='modal'  data-target='#modal-negos'><span class='fa fa-check-square-o'></span></button>";
                    $descartarBtn = "<button class='btn btn-xs btn-danger btn-sm dct' data-toggle='modal' data-target='#descarta-deman' title='Descartar Inmueble'><span class='glyphicon fa fa-times-circle'></span></button> ";
                } else {
                    $btn          = "<button title='En espera de Respuesta' type='button' id='aplicarInm' class='btn btn-xs btn-primary ' disabled ><span class='fa fa-check-square-o'></span></button>";
                    $descartarBtn = "<button class='btn btn-xs btn-danger btn-sm dctd' title='Descartar Inmueble' disabled><span class='glyphicon fa fa-times-circle' ></span></button> ";
                }
                if ($conTodos == 1) {

                    //echo "<br>La demanda $IdDemanda cumple al 100% <br>";

                    $varTexto .= "<li>" . " El Inmueble $idInm cumple al 100%";
                    $varTexto1 .= "<li>" . "<i class='fa fa-star' title='Calificacion de la Oportunidad' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i></li> ";
                    $varTexto2 .= "<li>" . $iconWarning;
                    $varTexto3 .= "100";
                    $flag         = 1;
                    $cadInmuebles = $idInm . "," . $cadInmuebles;
                }
                $ciudOr = getCampo('demandainmuebles', "where IdBarrio and IdDemanda='" . $coddem . "'", 'IdCiudad', 0);
                //echo "ciudOr $ciudOr <br>";
                // echo "$IdDemanda $conTodos==todos and $conBarrio==barrio and $conArea==area and $conDestinacion==destinacion $conDir==dir <br>";
                if ($conTodos == 0 and $conBarrio == 0 and $conBarrio1 == 0 and $conArea == 0 and $conDestinacion == 0 and $IdCiudad == 0 and $conDir == 0 and $conEst == 0) {
                    //echo "$IdDemanda no coincide";

                }

                if ($conTodos == 0 and ($conBarrio1 == 1 or $conBarrio == 1 or $conArea == 1 or $conDestinacion == 1 or $conDir == 1 or $conEst == 1)) // or $IdCiudad==$ciudOr))
                {
                    if ($IdCiudad == $ciudOr) {$conCiud = 1;} else { $conCiud = 0;}
                    $var1         = "";
                    $var2         = "";
                    $var3         = "";
                    $var4         = "";
                    $var5         = "";
                    $var6         = "";
                    $var7         = "";
                    $validaGlobal = $conBarrio + $conBarrio1 + $conArea + $conDestinacion + $conCiud + $conDir + $conEst; //echo "<br>La demanda $IdDemanda ";
                    // echo "$validaGlobal=conBarrio $conBarrio --";
                    // echo "$validaGlobal=conBarrio1 $conBarrio1 --";
                    // echo "$validaGlobal=conArea $conArea--";
                    // echo "$validaGlobal=conDestinacion $conDestinacion --";
                    // echo "$validaGlobal=conCiud $conCiud --";
                    // echo "$validaGlobal=conDir $conDir --";
                    // echo "$validaGlobal=conEst $conEst --";

                    if ($validaGlobal > 1) {
                        $varTexto .=  'El Inmueble ' . $idInm . '<input type="hidden" class="c_demd" value="' . $idInm . '" > ';
                        $cadInmuebles = $idInm . "," . $cadInmuebles;
                    }
                    if ($conBarrio1 == 1) {
                        if ($tiene_barrios1 > 0) {
                            $var1 = " Barrio, <br>";
                        } else {
                            $var1 = " Localidad, <br>";
                        }
                    } else { $var1 = "";}
                    if ($conBarrio == 1) {$var6 = "";} else { $var6 = "";}
                    if ($conArea == 1) {$var2 = " Area, <br>";} else { $var2 = "";}
                    if ($conDestinacion == 1) {$var3 = " Destinacion, <br>";} else { $var3 = "";}
                    if ($conCiud > 0) {$var4 = " Ciudad, <br>";} else { $var4 = "";}
                    if ($conDir == 1) {$var5 = " Direccion, <br>";} else { $var5 = "";}
                    if ($conEst == 1) {$var7 = " Estrato, <br>";} else { $var6 = "";}

                    //echo "<table class='TablaDatos' width='80%' align='center'><tr><td class='tituloFormulario'>Demandas Coincidentes</td></tr> <tr><td>";
                    if ($validaGlobal == 1) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion,  $var1 $var2 $var3 $var4 $var5 Cumple al 44.4%<br>";
                        // $varTexto = $varTexto."Coincide en Tipo de Inmueble, Gestion,  $var1 $var2 $var3 $var4 $var5 Cumple al 44.4% <i class='fa fa-star' style='color:#ec6459'></i></li>";

                        $varTexto = $varTexto . "Coincide en Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 $var7 Cumple al 44.5% ";
                        $varTexto1 = $varTexto1 . "<i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                        $varTexto2 = $varTexto2 .  $iconWarning;
                        $varTexto3 .= "55";
                        $flag++;
                    }

                    if ($validaGlobal == 2) {
                        $varTexto = $varTexto . "Coincide en Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 $var7 Cumple al 55.5% ";
                        $varTexto1 = $varTexto1 . "<i class='fa fa-star' style='color:#ec6459'></i><i class='fa fa-star' style='color:#ec6459'></i></li>";
                        $varTexto2 = $varTexto2 .  $iconWarning;
                        $varTexto3 .= "55";
                        $flag++;}

                    if ($validaGlobal == 3) {
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5  $var7 Cumple al 66.6% ";
                        $varTexto1 = $varTexto1 . "<i class='fa fa-star' style='color:#1fb5ad'></i><i class='fa fa-star' style='color:#1fb5ad'></i><i class='fa fa-star' style='color:#1fb5ad'></i></li> ";
                        $varTexto2 = $varTexto2 .  $iconWarning;
                        $varTexto3 .= "66.6";
                        $flag++;}

                    if ($validaGlobal == 4) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 77.7%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5  $var7 Cumple al 77.7% ";
                        $varTexto1 = $varTexto1 ."<i class='fa fa-star' style='color:#f3c022'></i><i class='#f3c022' fa-star' style='color:#f3c022'></i><i class='fa fa-star' style='color:#f3c022'></i><i class='fa fa-star' style='color:#f3c022'></i></li>";
                        $varTexto2 = $varTexto2 .  $iconWarning;
                        $varTexto3 .= "77.7";
                        $flag++;}

                    if ($validaGlobal == 5) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5  $var7 Cumple al 80% ";
                        $varTexto1 = $varTexto1 . "<i class='fa fa-star' style='color:#f3c022'></i><i class='fa fa-star' style='color:#f3c022'></i><i class='fa fa-star' style='color:#f3c022'></i><i class='fa fa-star' style='color:#f3c022'></i></li>";
                        $varTexto2 = $varTexto2 .  $iconWarning;
                        $varTexto3 .= "80";
                        $flag++;}
                    if ($validaGlobal == 6) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5  $var7 Cumple al 89% ";
                        $varTexto1 = $varTexto1 . "<i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i></li>";
                        $varTexto2 = $varTexto2 .  $iconWarning;
                        $varTexto3 .= "89";
                        $flag++;}
                    if ($validaGlobal == 7) {
                        //echo "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5 Cumple al 89%<br>";
                        $varTexto = $varTexto . "Coincide en Valor, Tipo de Inmueble, Gestion, $var1 $var2 $var3 $var4 $var5  $var7 Cumple al 95% ";
                        $varTexto1 = $varTexto1 . "<i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i><i class='fa fa-star' style='color:green'></i></li>";
                        $varTexto2 = $varTexto2 .  $iconWarning;
                        $varTexto3 .= "95";
                        $flag++;}
                    //echo "</td></tr></table>";
                    //$flag=1;

                }
                $info[]=array(
                    "texto_cumple"    => utf8_encode($varTexto),
                    "estrellas"       => $varTexto1,
                    "tiene_politicas" => $varTexto2,
                    "porcentaje" => $varTexto3
                    );
                // echo "Entra a alguna $IdDemanda  - $flag -- validaGlobal $validaGlobal";
            } //while
            // echo $flag."flag";

            if ($flag > 0) {
               
                return $info;
            }
        } else {
            //echo "No Hay demandas coincidentes";
            return null;
        }
        //$w_conexion->CerrarConexion();
    }

}
