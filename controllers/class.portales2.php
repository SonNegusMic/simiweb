<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
@include "../funciones/connPDO.php";

class Portales
{
    public function __construct($conn = '')
    {
        $this->db = $conn;
    }

    private $id_usuario;

    public function dataInmueblesPublicar($id)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("Select i.idInm,i.PublicaVReal,i.PublicaGuia,i.PublicaM2,
                i.PublicaMiCasa,i.PublicaZonaProp,i.RMtoCuadrado,i.RZonaProp,i.estadouraki,
                i.coduraki,i.RVivaReal,i.RLaGuia, i.PublicaDoomos,i.PublicaMeli,i.RMeli,
                i.Publicagpt,i.IdGestion,i.PublicaProperati ,i.PublicaLamudi,i.PublicaOlx,i.PublicaIdn,
                Publicalocanto,PublicaPautaR,PublicaTM,PublicaAbad,RNcasa
                from inmuebles i
                where i.IdInmobiliaria =:idinmo
                $cond
                AND i.idEstadoinmueble=2");

        if ($stmt->execute(array(
            ":idinmo" => $id,
        ))) {

            while ($rows = $stmt->fetch()) {

                $data[] = array(
                    "idInm"            => $rows['idInm'],
                    "PublicaVReal"     => $rows['PublicaVReal'],
                    "PublicaGuia"      => $rows['PublicaGuia'],
                    "PublicaM2"        => $rows['PublicaM2'],
                    "PublicaMiCasa"    => $rows['PublicaMiCasa'],
                    "PublicaZonaProp"  => $rows['PublicaZonaProp'],
                    "RMtoCuadrado"     => $rows['RMtoCuadrado'],
                    "RZonaProp"        => $rows['RZonaProp'],
                    "estadouraki"      => $rows['estadouraki'],
                    "Publicagpt"       => $rows['Publicagpt'],
                    "IdGestion"        => $rows['IdGestion'],
                    "PublicaProperati" => $rows['PublicaProperati'],
                    "PublicaLamudi"    => $rows['PublicaLamudi'],
                    "PublicaOlx"       => $rows['PublicaOlx'],
                    "PublicaIdn"       => $rows['PublicaIdn'],
                    "Publicalocanto"   => $rows['Publicalocanto'],
                    "PublicaPautaR"    => $rows['PublicaPautaR'],
                    "PublicaTM"        => $rows['PublicaTM'],
                    "PublicaAbad"      => $rows['PublicaAbad'],
                    "RNcasa"           => $rows['RNcasa'],

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function evaluaPortal($idInmo, $idPortal)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("Select IdPortal
                        From PublicaPortales
                        Where IdPortal=:idPortal
                        And IdInmobiliaria=:idinmo");

        if ($stmt->execute(array(
            ":idinmo"   => $idInmo,
            ":idPortal" => $idPortal,
        ))) {

            $existe = $stmt->rowCount();
            return $existe;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function validaInfoInmueblesPublicar($id)
    {
        $connPDO = new Conexion();
        $error   = 0;
        $cadena  = "";

        $stmt = $connPDO->prepare("select IdInm,IdBarrio,IdPropietario,IdPromotor,IdCaptador,
                DirMetro,ValorVenta,ValorCanon,Administracion, IdTpInm,
                Estrato,AreaConstruida,EdadInmueble,idEstadoInmueble,IdGestion
                from inmuebles
                where idInm=:idinmu");

        if ($stmt->execute(array(
            ":idinmu" => $id,
        ))) {

            while ($rows = $stmt->fetch()) {

                $IdBarrios        = $rows['IdBarrio'];
                $IdPropietario    = $rows['IdPropietario'];
                $IdPromotor       = $rows['IdPromotor'];
                $IdCaptador       = $rows['IdCaptador'];
                $DirMetro         = $rows['DirMetro'];
                $ValorVenta       = $rows['ValorVenta'];
                $ValorCanon       = $rows['ValorCanon'];
                $Administracion   = $rows['Administracion'];
                $IdTpInm          = $rows['IdTpInm'];
                $Estrato          = $rows['Estrato'];
                $AreaConstruida   = $rows['AreaConstruida'];
                $EdadInmueble     = $rows['EdadInmueble'];
                $idEstadoInmueble = $rows['idEstadoInmueble'];
                $IdGestion        = $rows['IdGestion'];

            }
            $barrzp = getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdBarrZp');
            $barrml = strlen(getCampo('barrios', "where IdBarrios=$IdBarrios", 'IdBarrMeli')); //echo $barrml;
            //inicio de validación
            if ($barrml <= 3 and $IdBarrios > 0 and $this->evaluaPortal($id, 2) == 1) {
                $error++;
                $cadena = $cadena . " " . " Barrio No codificado en TuInmueble<br>";
            }
            if ($barrzp <= 0 and $IdBarrios > 0 and $this->evaluaPortal($id, 10) == 1) {
                $error++;
                $cadena = $cadena . " " . " Barrio No codificado en ZonaProp<br>";
            }
            if ($IdBarrios <= 0) {
                $error++;
                $cadena = $cadena . " " . " Falta Barrio<br>";
            }
            if ($IdGestion <= 0) {
                $error++;
                $cadena = $cadena . " " . " Falta Gestion<br>";
            }
            if ($IdPropietario <= 0) {
                $error++;
                $cadena = $cadena . " " . " Falta Propietario<br>";
            }
            if ($IdPromotor <= 0) {
                $error++;
                $cadena = $cadena . " " . " Falta Promotor<br>";
            }
            if ($IdCaptador <= 0) {
                $error++;
                $cadena = $cadena . " " . " Falta Captador<br>";
            }
            if ($DirMetro == '' and $PubPortMC == 1) {
                $error++;
                $cadena = $cadena . " " . " Falta Falta Dirección Para MetroCuadrado<br>";
            }
            if ($ValorVenta <= 0 and $IdGestion != 1) {
                $error++;
                $cadena = $cadena . " " . " Falta Valor Venta<br>";
            }
            if ($ValorCanon <= 0 and $IdGestion == 1) {
                $error++;
                $cadena = $cadena . " " . " Falta Valor Canon<br>";
            }
            if ($Administracion <= 0) {
                //$error++;
                //  $cadena = $cadena." "."No Registra Administracion<br>";
            }
            if ($IdTpInm <= 0) {
                $error++;
                $cadena = $cadena . " " . " Falta Tipo Inmueble <br>";
            }
            if ($AreaConstruida <= 0) {
                $error++;
                $cadena = $cadena . " " . " Falta Area<br>";
            }
            if ($Estrato <= 0) {
                $error++;
                $cadena = $cadena . " " . " Falta Estrato<br>";
            }
            if ($EdadInmueble <= 0) {
                $error++;
                $cadena = $cadena . " " . " Falta Edad del Inmueble<br>";
            }
            if ($idEstadoInmueble != 2) {
                $error++;
                $cadena = $cadena . " " . " Estado no es disponible<br>";
            }
            $data[] = array("mensaje" => $cadena);
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }

    public function inmueblesInmobiliariaPortales($id)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT i.IdPortal,i.IdInmobiliaria,i.mailventa,i.mailrenta,
                p.NomPortal,p.imagenp,p.pago,telrenta,telventa,ordport
                from Portales p, PublicaPortales i
                where i.IdPortal=p.IdPortal
                and i.IdInmobiliaria= :idinmo
                and p.estport=1
                order by p.ordport");
        if ($stmt->execute(array(
            ":idinmo" => $id,
        ))) {
            $data = array();
            while ($row = $stmt->fetch()) {

                $data[] = array(
                    "ordport"   => $row['ordport'],
                    "IdPortal"  => $row['IdPortal'],
                    "NomPortal" => $row['NomPortal'],
                    "imagenp"   => $row['imagenp'],
                    "pago"      => $row['pago'],

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function getDataM2($idInmo, $usr)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("Select usuariom2,clavem2,prefijom2,letra,servidor,sucursalm2,sucurnomm2
                        From usuariosm2
                        Where inmogm2=:idinmo
                        and usuariom2=:usr
                        group by usuariom2
                        #limit 0,1");

        if ($stmt->execute(array(
            ":idinmo" => $idInmo,
            ":usr"    => $usr,
        ))) {

            $data = array();
            while ($row = $stmt->fetch()) {

                $data[] = array(
                    "usuariom2"  => $row['usuariom2'],
                    "clavem2"    => $row['clavem2'],
                    "prefijom2"  => $row['prefijom2'],
                    "letra"      => $row['letra'],
                    "servidor"   => $row['servidor'],
                    "sucursalm2" => $row['sucursalm2'],
                    "sucurnomm2" => $row['sucurnomm2'],

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function insertUserM2($data)
    {
        $connPDO = new Conexion();

        $idInmo     = $data['idInmo'];
        $usuariom2  = $data['userM2'];
        $sucurnomm2 = $data['sucurnomm2'];
        $sucursalm2 = $data['sucursalM2'];
        $idusariom2 = $data['idusariom2'];
        $servidorM2 = $data['servidorM2'];
        $letraM2    = $data['letraM2'];
        $prefijoM2  = $data['prefijoM2'];
        $servidorM2 = $data['servidorM2'];
        $info       = $this->getDataM2($idInmo, $usuariom2);
        foreach ($info as $key => $value) {}

        $stmt = $connPDO->prepare("REPLACE into usuariosm2
        (idusariom2,usuariom2,clavem2,prefijom2,letra,servidor,sucurnomm2,sucursalm2,inmogm2)
        values
        (:idusariom2,:usuariom2,:clavem2,:prefijom2,:letra,:servidor,:sucurnomm2,:sucursalm2,:inmogm2)");

        if ($stmt->execute(array(
            ":idusariom2" => $idusariom2,
            ":usuariom2"  => $usuariom2,
            ":clavem2"    => $value['clavem2'],
            ":prefijom2"  => $prefijoM2,
            ":letra"      => $letraM2,
            ":servidor"   => $value['servidor'],
            ":sucurnomm2" => $sucurnomm2,
            ":sucursalm2" => $sucursalm2,
            ":inmogm2"    => $idInmo,
        ))) {

            return 1;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function insertFirstUserM2($data)
    {
        $connPDO = new Conexion();

        $idInmo     = $data['idInmo'];
        $idusariom2 = $data['idusariom2'];
        $usuariom2  = $data['usuariom2'];
        $clavem2    = $data['clavem2'];
        $prefijom2  = $data['prefijom2'];
        $letra      = $data['letra'];
        $servidor   = $data['servidor'];
        $sucurnomm2 = $data['sucurnomm2'];
        $sucursalm2 = $data['sucursalm2'];

        $existe = $this->verifyFirstUserM2($idInmo, $sucursalm2);

        if ($existe == 0) {

            $stmt = $connPDO->prepare("REPLACE into usuariosm2
            (idusariom2,usuariom2,clavem2,prefijom2,letra,servidor,sucurnomm2,sucursalm2,inmogm2)
            values
            (:idusariom2,:usuariom2,:clavem2,:prefijom2,:letra,:servidor,:sucurnomm2,:sucursalm2,:inmogm2)");

            if ($stmt->execute(array(
                ":inmogm2"    => $idInmo,
                ":idusariom2" => $idusariom2,
                ":usuariom2"  => $usuariom2,
                ":clavem2"    => $clavem2,
                ":prefijom2"  => $prefijom2,
                ":letra"      => $letra,
                ":servidor"   => $servidor,
                ":sucurnomm2" => $sucurnomm2,
                ":sucursalm2" => $sucursalm2,
            ))) {

                return 1;

            } else {
                return print_r($stmt->errorInfo());

            }
        } else {
            $stmt = $connPDO->prepare("update usuariosm2
                set
                usuariom2           =:usuariom2,
                clavem2             =:clavem2,
                prefijom2           =:prefijom2,
                letra               =:letra,
                servidor            =:servidor,
                sucurnomm2          =:sucurnomm2,
                sucursalm2          =:sucursalm2
                where inmogm2       =:inmogm2
                and sucursalm2      =:sucursalm22");

            if ($stmt->execute(array(
                ":inmogm2"     => $idInmo,
                ":usuariom2"   => $usuariom2,
                ":clavem2"     => $clavem2,
                ":prefijom2"   => $prefijom2,
                ":letra"       => $letra,
                ":servidor"    => $servidor,
                ":sucurnomm2"  => $sucurnomm2,
                ":sucursalm2"  => $sucursalm2,
                ":sucursalm22" => $sucursalm2,
            ))) {

                return 1;

            } else {
                return print_r($stmt->errorInfo());

            }

        }
        $stmt = null;
    }
    public function verifyFirstUserM2($idInmo, $sucursalm2)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("select idusariom2 from usuariosm2
                                where inmogm2=:idinmo
                                and sucursalm2=:sucursalm2");

        if ($stmt->execute(array(
            ":idinmo"     => $idInmo,
            ":sucursalm2" => $sucursalm2,
        ))) {
            $existe = $stmt->rowCount();

            return $existe;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function insertFirstUserFinca($data)
    {
        $connPDO = new Conexion();

        $idInmo    = $data['idInmo'];
        $usuario   = $data['usuario'];
        $idusguia  = $data['idusguia'];
        $sucursalg = $data['sucursalg'];
        $sucurnom  = $data['sucurnom'];
        $mail_suc  = $data['mail_suc'];
        $tel_suc   = $data['tel_suc'];

        $existe = $this->verifyFirstUserFinca($idInmo, $sucursalg);

        if ($existe == 0) {

            $stmt = $connPDO->prepare("insert into usuariosguia
            (idusguia,usuario,sucursalg,sucurnom,inmog)
            values
            (:idusguia,:usuario,:sucursalg,:sucurnom,:inmog)");

            if ($stmt->execute(array(
                ":inmog"     => $idInmo,
                ":idusguia"  => $idusguia,
                ":usuario"   => $usuario,
                ":sucursalg" => $sucursalg,
                ":sucurnom"  => $sucurnom,
            ))) {

                return 1;

            } else {
                return print_r($stmt->errorInfo());

            }
        } else {
            $stmt = $connPDO->prepare("update usuariosguia
                set
                usuario             = :usuario,
                sucurnom            = :sucurnom,
                sucursalg           = :sucursalg
                where inmog         = :inmog
                and  sucursalg      = :sucursalg1");

            if ($stmt->execute(array(
                ":inmog"      => $idInmo,
                ":usuario"    => $usuario,
                ":sucursalg"  => $sucursalg,
                ":sucursalg1" => $sucursalg,
                ":sucurnom"   => $sucurnom,

            ))) {

                return 1;

            } else {
                return print_r($stmt->errorInfo());

            }
        }

        $stmt = null;
    }
    public function verifyFirstUserFinca($idInmo, $sucursal)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("select usuario
                                from usuariosguia
                                where inmog=:idinmo
                                and sucursalg=:sucursal");

        if ($stmt->execute(array(
            ":idinmo"   => $idInmo,
            ":sucursal" => $sucursal,
        ))) {
            $existe = $stmt->rowCount();

            return $existe;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function getDataFinca($data)
    {
        $connPDO = new Conexion();
        $idInmo  = $data['inmo'];
        $usr     = $data['usuariofr'];
        $stmt    = $connPDO->prepare("Select idusguia,usuario,sucursalg,sucurnom,tel_suc,inmog,mail_suc
                        From usuariosguia
                        Where inmog=:idinmo
                        and idusguia=:usr
                        group by usuario,sucursalg
                        #limit 0,1");

        if ($stmt->execute(array(
            ":idinmo" => $idInmo,
            ":usr"    => $usr,
        ))) {

            $data = array();
            while ($row = $stmt->fetch()) {

                $data[] = array(
                    "mail_suc"  => $row['mail_suc'],
                    "usuario"   => $row['usuario'],
                    "sucursalg" => $row['sucursalg'],
                    "sucurnom"  => $row['sucurnom'],
                    "tel_suc"   => $row['tel_suc'],

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function insertUserFinca($data)
    {
        $connPDO = new Conexion();

        $idInmo    = $data['idinmo'];
        $idusguia  = $data['idusariofr'];
        $usuario   = $data['userFR'];
        $sucursalg = $data['idSucursalFR'];
        $sucurnom  = $data['nomSucurFr'];

        // $info=$this->getDataFinca($idInmo);
        // foreach($info as $key => $value)
        // {}

        $stmt = $connPDO->prepare("replace into usuariosguia
        (idusguia,usuario,sucursalg,sucurnom,inmog)
        values
        (:idusguia,:usuario,:sucursalg,:sucurnom,:inmog)");

        if ($stmt->execute(array(
            ":inmog"     => $idInmo,
            ":idusguia"  => $idusguia,
            ":usuario"   => $usuario,
            ":sucursalg" => $sucursalg,
            ":sucurnom"  => $sucurnom,
        ))) {

            return 1;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function getDataM2User($data)
    {
        $connPDO = new Conexion();
        $idInmo  = $data['inmo'];
        $usr     = $data['usuariom2'];
        $stmt    = $connPDO->prepare("Select usuariom2,clavem2,prefijom2,letra,servidor,sucursalm2,sucurnomm2
                        From usuariosm2
                        Where inmogm2=:idinmo
                        and usuariom2=:usuariom2
                        group by usuariom2
                        #limit 0,1");

        if ($stmt->execute(array(
            ":idinmo"    => $idInmo,
            ":usuariom2" => $usr,
        ))) {

            $data = array();
            while ($row = $stmt->fetch()) {
                $serv   = ($row['servidor'] == 1) ? 'Servidor de Produccion' : 'Servidor de Pruebas';
                $data[] = array(
                    "usuariom2"  => $row['usuariom2'],
                    "clavem2"    => $row['clavem2'],
                    "prefijom2"  => $row['prefijom2'],
                    "letra"      => $row['letra'],
                    "servidor"   => $serv,
                    "sucursalm2" => $row['sucursalm2'],
                    "sucurnomm2" => $row['sucurnomm2'],

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function getDataFincaUser($data)
    {
        $connPDO = new Conexion();
        $idInmo  = $data['inmo'];
        $usr     = $data['usuariofn'];
        $stmt    = $connPDO->prepare("Select usuario,sucursalg,sucurnom
                        From usuariosguia
                        Where inmog=:idinmo
                        and usuario=:usuario
                        group by usuario
                        #limit 0,1");

        if ($stmt->execute(array(
            ":idinmo"  => $idInmo,
            ":usuario" => $usr,
        ))) {

            $data = array();
            while ($row = $stmt->fetch()) {
                $data[] = array(
                    "usuario"   => $row['usuario'],
                    "sucursalg" => $row['sucursalg'],
                    "sucurnom"  => $row['sucurnom'],

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function evaluaUsuarioM2($idusuario)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("Select idusariom2
                        From usuariosm2
                        Where idusariom2=:idusuario");

        if ($stmt->execute(array(
            ":idusuario" => $idusuario,
        ))) {

            $existe = $stmt->rowCount();
            return $existe;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function evaluaUsuarioFinca($idusuario)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("Select idusguia
                        From usuariosguia
                        Where idusguia=:idusguia");

        if ($stmt->execute(array(
            ":idusguia" => $idusuario,
        ))) {

            $existe = $stmt->rowCount();
            return $existe;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function getDataM2IdUsuario($data)
    {
        $connPDO = new Conexion();
        $idInmo  = $data['inmo'];
        $usr     = $data['usuariom2'];
        $stmt    = $connPDO->prepare("Select idusariom2,usuariom2,clavem2,prefijom2,letra,servidor,sucursalm2,sucurnomm2
                        From usuariosm2
                        Where inmogm2=:idinmo
                        and idusariom2=:usuariom2
                        group by usuariom2
                        #limit 0,1");

        if ($stmt->execute(array(
            ":idinmo"    => $idInmo,
            ":usuariom2" => $usr,
        ))) {

            $data = array();
            while ($row = $stmt->fetch()) {
                $serv   = ($row['servidor'] == 1) ? 'Servidor de Produccion' : 'Servidor de Pruebas';
                $data[] = array(
                    "usuariom2"  => $row['usuariom2'],
                    "clavem2"    => $row['clavem2'],
                    "prefijom2"  => $row['prefijom2'],
                    "letra"      => $row['letra'],
                    "servidor"   => $serv,
                    "sucursalm2" => $row['sucursalm2'],
                    "sucurnomm2" => $row['sucurnomm2'],

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }

    public function datosInmueblePortales($data)
    {
        $connPDO = new Conexion();
        $idInmu  = $data['inmu'];
        $stmt    = $connPDO->prepare("SELECT idInm,RLaGuia,RMtoCuadrado,RVivaReal,RMeli,RNcasa,
            RZonaProp,RIdnd,RMiCasa,PublicaOlx,Publicalocanto,Publicagpt,IdGestion,PublicaMiCasa,
            PublicaProperati,PublicaLamudi,PublicaAbad,dirMeli,RAfydi,IdInmobiliaria,PublicaDoomos,flagm2
            FROM  inmuebles
            WHERE idInm=:idInmu
            LIMIT 0,1");

        if ($stmt->execute(array(
            ":idInmu" => $idInmu,
        ))) {
            $rutalogo            = "https://www.simiinmobiliarias.com/mcomercialweb/images/";
            $data                = array();
            $btnM2               = '';
            $btnFinca            = '';
            $btnMeli             = '';
            $btnNc               = '';
            $btni24              = '';
            $btnIdonde           = '';
            $btnGoplaceit        = '';
            $btnLocanto          = '';
            $btnProperati        = '';
            $btnLamudi           = '';
            $btnOlx              = '';
            $btnAbad             = '';
            $afydi               = '';
            $locanto             = '';
            $btnM2Elimina        = '';
            $btnFincaElimina     = '';
            $btnMeliElimina      = '';
            $btnNcElimina        = '';
            $btni24Elimina       = '';
            $btnGoplaceitElimina = '';
            $btnIdondeElimina    = '';
            $btnLocantoElimina   = '';
            $btnProperatiElimina = '';
            $btnLamudiElimina    = '';
            $btnOlxElimina       = '';
            $btnAbadElimina      = '';
            $Micasa              = '';
            $btnMicasa           = '';
            $btnMicasaElimina    = '';
            while ($row = $stmt->fetch()) {

                $linkFinca = ($row['RLaGuia']) ? 'http://www.fincaraiz.com.co/detail.aspx?a=' . $row['RLaGuia'] : '';
                $linkM2    = ($row['RMtoCuadrado']) ? 'http://www.metrocuadrado.com/web/busqueda/inmueble/' . $row['RMtoCuadrado'] : '';
                //$linkMeli=($row['RMeli']>0)?'http://casa.mercadolibre.com.co/'.$row['RMeli'].'-inmueble-_JM':'';
                $linkMeli1     = ($row['RMeli'] > 0) ? 'http://www.eltiempo.com' : '';
                $linkNc        = ($row['RNcasa'] > 0) ? 'http://www.nuestracasa.com.co/propiedades.php?id_inmueble=' . $row['RNcasa'] : '';
                $linki24       = ($row['RZonaProp'] > 0) ? 'http://www.inmuebles24.co/propiedades/inmueble-' . $row['RZonaProp'] . '.html' : 'http://www.inmuebles24.co';
                $linkIwanti    = 'http://co.iwanti.com/anuncio/index?BusquedaForm%5BidCategoria%5D=4&BusquedaForm%5BtextoABuscar%5D=' . $idInmu;
                $linkDoomos    = 'http://www.doomos.com.co/pmx/buscar-por-codigo.html?code=' . $idInmu;
                $linkIcasas    = 'http://www.icasas.com.co/';
                $idInmu1       = str_replace("-", "", $idInmu);
                $linkOlx       = 'https://bogotacity.olx.com.co/nf/search/' . $idInmu1;
                $linkLocanto   = 'http://www.locanto.com.co';
                $linkIdonde    = 'http://www.idonde.com';
                $linkGoplaceit = 'http://www.goplaceit.com.co';

                $linkPublicaFinca = '../../mcomercialweb/a_inmueblesportalesGUIANvo2.php?publica=' . $idInmu . '&usr=' . $_SESSION['Id_Usuarios'] . '&est=2"';
                $linkRetiraFinca  = '../../mcomercialweb/a_inmueblesportalesGUIANvo2.php?publica=' . $idInmu . '&usr=' . $_SESSION['Id_Usuarios'] . '&est=3"';
                if (getCampo('inmobiliaria', "where IdInmobiliaria=" . $_SESSION['IdInmmo'], 'activarn') == 1) {
                    $app_rutam2 = "../../mcomercialweb/portales/a_inmueblesportalesMCNvo3.php";
                } else {
                    $app_rutam2 = "../../mcomercialweb/portales/a_inmueblesportalesMCNvo.php";
                }
                $linkPublicaMetro     = $app_rutam2 . '?publica=' . $idInmu . '&usr=' . $_SESSION['Id_Usuarios'];
                $linkActualizaMetro   = $app_rutam2 . '?publica=' . $idInmu . '&op=2&lg=4&cod_repu=' . $row['RMtoCuadrado'] . '&usr=' . $_SESSION['Id_Usuarios'];
                $linkEliminaMetro     = '../../mcomercialweb/portales/a_eliminacionMC.php?publica=' . $idInmu . '&cod_repu=' . $row['RMtoCuadrado'] . '&usr=' . $_SESSION['Id_Usuarios'];
                $linkPublicaMeli      = '../../PHPSDK/file_export2.php?opt=1&publicar=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkActualizaMeli    = '../../PHPSDK/file_export2.php?opt=2&publicar=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkRetiraMeli       = '../../PHPSDK/file_export2.php?opt=3&publicar=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkPublicaNcasa     = '../../Portales/nuestraCasa/arrayNCPro.php?data=5&inmu=' . $idInmu;
                $linkRetiraNcasa      = '../../Portales/nuestraCasa/arrayNCPro.php?data=2&inmu=' . $idInmu;
                $linkPublicaI24       = 'http://web3.siminmueble.com/mcomercialweb/a_inmueblesportalesZP_1.php?publica=' . $idInmu . '&usr=' . $_SESSION['Id_Usuarios'];
                $linkRetiraI24        = 'http://web3.siminmueble.com/mcomercialweb/e_inmueblesportalesZP2.php?idinmu=' . $idInmu . '&cod_inmo=' . $row['IdInmobiliaria'];
                $linkPublicaIdonde    = '../../mcomercialweb/a_inmueblesportalesIdonde.php?publica=' . $idInmu . '&est=1&usr=' . $_SESSION['Id_Usuarios'];
                $linkRetiraIdonde     = '../../mcomercialweb/a_inmueblesportalesIdonde.php?publica=' . $idInmu . '&est=2"';
                $linkPublicaLocanto   = '../../mcomercialweb/portales/a_locanto.php?publica=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkRetiraLocanto    = '../../mcomercialweb/portales/a_locanto.php?publica=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkPublicaGoplaceit = '../../mcomercialweb/a_inmueblesportalesGP2.php?idinmu=' . $idInmu . '&inmob=' . $row['IdInmobiliaria'];
                $linkRetiraGoplaceit  = '../../mcomercialweb/a_eliminaportalesGP.php?idinmu=' . $idInmu . '&inmob=' . $row['IdInmobiliaria'];
                $linkPublicaAfydi     = '../../mwc/Afydi/apiAfydi.php?codInmu=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkRetiraAfydi      = '../../restClient/index2.php?publica=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkPublicaMicasa    = '../../mcomercialweb/a_inmueblesportalesmicasa.php?publica=' . $idInmu . '&usr=' . $_SESSION['Id_Usuarios'];
                $linkRetiraMicasa     = '../../mcomercialweb/e_inmueblesportalesMiCasa.php?publica=' . $idInmu;
                $linkOlxPublica       = '../../mcomercialweb/a_inmuebles_olx.php?publica=' . $idInmu . '&gest=' . $rows['IdGestion'] . '&usr' . $_SESSION['Id_Usuarios'];
                $linkOlxElimina       = '../../mcomercialweb/a_inmuebles_olx.php?retirar=1&publica=' . $idInmu . '&gest=' . $rows['IdGestion'];

                if ($this->verifyPortales($row['IdInmobiliaria'], 1) > 0) {
                    $logoDoomos = ($this->verifyPortales($row['IdInmobiliaria'], 1) > 0) ? '<a href="' . $linkDoomos . '" target="_blank"><img src="../../mcomercialweb/images/log_gratuitos.jpg"></a>' : '';
                    $btnDomos   = '<span class="label label-primary">Masivo</span> ';
                    $doomos     = '<div class="col-sm-3"><div class="col-sm-12">' . $logoDoomos . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['PublicaDoomos'] . '</span> ' . $btnDomos . ' ' . $btnDomose . '</div></div>';
                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 22) > 0) {
                    $logoIwanti = ($this->verifyPortales($row['IdInmobiliaria'], 22) > 0) ? '<a href="' . $linkIwanti . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(22) . '"></a>' : '';
                    $btnDomos   = '<span class="label label-primary">Masivo</span> ';
                    $iwanti     = '<div class="col-sm-3"><div class="col-sm-12">' . $logoIwanti . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['PublicaDoomos'] . '</span> ' . $btnDomos . ' ' . $btnDomose . '</div></div>';
                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 24) > 0) {
                    $logoIcasasm = ($this->verifyPortales($row['IdInmobiliaria'], 24) > 0) ? '<a href="' . $linkIcasas . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(24) . '"></a>' : '';
                    $logoIcasas  = ($this->verifyPortales($row['IdInmobiliaria'], 24) > 0) ? $logoIcasasm : '';
                    $btnDomos    = '<span class="label label-primary">Masivo</span> ';
                    $icasas      = '<div class="col-sm-3"><div class="col-sm-12">' . $logoIcasas . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['PublicaDoomos'] . '</span> ' . $btnDomos . ' ' . $btnDomose . '</div></div>';
                }

                if ($this->verifyPortales($row['IdInmobiliaria'], 8) > 0) {

                    $lF        = ($row['RLaGuia']) ? '<a href="' . $linkFinca . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(8) . '"></a>' : '<img src="' . $rutalogo . $this->getLogoPortal(8) . '">';
                    $logoFinca = ($this->verifyPortales($row['IdInmobiliaria'], 8) > 0) ? $lF : '';
                    $btnFinca  = ($row['RLaGuia']) ? '<a target="_blank" href="' . $linkPublicaFinca . '"><button type="button" id="finca" class="btn btn-success  finca  btn-xs mensajeT" data-toggle="modal" data-target="#actFinca"  data-toggle="tooltip" data-placement="bottom" title="Actualizar FincaRaiz" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '<a target="_blank" href="' . $linkPublicaFinca . '"><button type="button" id="fincaAct" class="btn btn-warning  finca  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Publicar Finca" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>';

                    $btnFincaElimina = ($row['RLaGuia']) ? '<a target="_blank" href="' . $linkRetiraFinca . '"><button type="button" id="fincaEli" class="btn btn-danger  finca  btn-xs mensajeT" data-toggle="modal" data-target="#actFinca"  data-toggle="tooltip" data-placement="bottom" title="Retirar FincaRaiz" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '';

                    $finca = '<div class="col-sm-3"><div class="col-sm-12">' . $logoFinca . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['RLaGuia'] . '</span> ' . $btnFinca . ' ' . $btnFincaElimina . '</div></div>';
                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 10) > 0) {
                    $linkM2m = ($row['RMtoCuadrado'] > 0) ? '<a href="' . $linkM2 . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(10) . '"></a>' : '<img src="' . $rutalogo . $this->getLogoPortal(10) . '">';

                    $logoM2       = ($this->verifyPortales($row['IdInmobiliaria'], 10) > 0) ? $linkM2m : '';
                    $btnM2o       = ($row['RMtoCuadrado']) ? '<a target="_blank"  href="' . $linkActualizaMetro . '"><button type="button" id="m2" class="btn btn-spartan  m2  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Actualiza MetroCuadrado" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '<a  target="_blank" href="' . $linkPublicaMetro . '"><button type="button" id="m2Act" class="btn btn-warning  m2  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Publica MetroCuadrado" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>';
                    $btnM2        = ($row['flagm2'] == 2) ? '<span class="label label-danger">Direccion No Valida</label>' : $btnM2o;
                    $btnM2Elimina = ($row['RMtoCuadrado'] > 0) ? '<a target="_blank" href="' . $linkEliminaMetro . '"><button type="button" id="m2Eli" class="btn btn-danger  m2  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Despublica MetroCuadrado" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '';
                    $metro        = '<div class="col-sm-3"><div class="col-sm-12">' . $logoM2 . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['RMtoCuadrado'] . '</span> ' . $btnM2 . ' ' . $btnM2Elimina . '</div></div>';
                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 2) > 0) {
                    $linkMelim      = ($row['RMeli'] > 0) ? '<a href="' . $row['dirMeli'] . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(2) . '"></a>' : '<img src="' . $rutalogo . $this->getLogoPortal(2) . '">';
                    $logoMeli       = ($this->verifyPortales($row['IdInmobiliaria'], 2) > 0) ? $linkMelim : '';
                    $btnMeli        = ($row['RMeli'] > 0) ? '<a target="_blank" href="' . $linkActualizaMeli . '"><button type="button" id="meli" class="btn btn-spartan  meli  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Actualiza TuInmueble" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '<a target="_blank" href="' . $linkPublicaMeli . '"><button type="button" id="meliAct" class="btn btn-warning  meli  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Publica TuInmueble" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>';
                    $btnMeliElimina = ($row['RMeli'] > 0) ? '<a target="_blank" href="' . $linkRetiraMeli . '"><button type="button" id="meliEli" class="btn btn-danger  meli  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Despublica TuInmueble" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '';
                    $meli           = '<div class="col-sm-3"><div class="col-sm-12">' . $logoMeli . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['RMeli'] . '</span> ' . $btnMeli . ' ' . $btnMeliElimina . '</div></div>';
                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 6) > 0) {
                    $lG                  = ($row['Publicagpt'] == 'Publicado') ? '<a href="' . $linkGoplaceit . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(6) . '"></a>' : '<img src="' . $rutalogo . $this->getLogoPortal(6) . '">';
                    $logoGoplaceit       = ($this->verifyPortales($row['IdInmobiliaria'], 6) > 0) ? $lG : '';
                    $btnGoplaceit        = ($row['Publicagpt'] == 'Publicado') ? '<a target="_blank" href="' . $linkPublicaGoplaceit . '"><button type="button" id="gpt" class="btn btn-spartan gpt btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Actualiza Goplaceit" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '<a target="_blank" href="' . $linkPublicaGoplaceit . '"><button type="button" id="gptAct" class="btn btn-warning gpt btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Publica Goplaceit" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>';
                    $btnGoplaceitElimina = ($row['Publicagpt'] == 'Publicado') ? '<a target="_blank" href="' . $linkRetiraGoplaceit . '"><button type="button" id="gptEli" class="btn btn-danger gpt  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Despublica Goplaceit" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '';
                    $color               = ($row['Publicagpt'] != 'Publicado') ? 'danger' : 'success';
                    $Goplaceit           = '<div class="col-sm-3"><div class="col-sm-12">' . $logoGoplaceit . '</div><div class="col-sm-12">' . '<span class="label label-' . $color . '">' . $row['Publicagpt'] . '</span> ' . $btnGoplaceit . ' ' . $btnGoplaceitElimina . '</div></div>';

                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 25) > 0) {
                    $lNc          = ($row['RNcasa'] > 0) ? '<a href="' . $linkNc . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(25) . '"></a>' : '<img src="' . $rutalogo . $this->getLogoPortal(25) . '">';
                    $logoNc       = ($this->verifyPortales($row['IdInmobiliaria'], 25) > 0) ? $lNc : '';
                    $btnNc        = ($row['RNcasa'] > 0) ? '<a target="_blank" href="' . $linkPublicaNcasa . '"><button type="button" id="nCasa" class="btn btn-spartan  nCasa  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Actualiza NuestraCasa" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '<a target="_blank" href="' . $linkPublicaNcasa . '"><button type="button" id="nCasaAct" class="btn btn-warning  nCasa  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Publica NuestraCasa" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>';
                    $btnNcElimina = ($row['RNcasa'] > 0) ? '<a target="_blank" href="' . $linkRetiraNcasa . '"><button type="button" id="nCasaEli" class="btn btn-danger  nCasa  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Despublica NuestraCasa" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '';
                    $nCasa        = '<div class="col-sm-3"><div class="col-sm-12">' . $logoNc . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['RNcasa'] . '</span> ' . $btnNc . ' ' . $btnNcElimina . '</div></div>';
                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 5) > 0) {
                    $l24           = ($row['RZonaProp'] > 0) ? '<a href="' . $linki24 . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(5) . '"></a>' : '<img src="' . $rutalogo . $this->getLogoPortal(5) . '">';
                    $logoi24       = ($this->verifyPortales($row['IdInmobiliaria'], 5) > 0) ? $l24 : '';
                    $btni24        = ($row['RZonaProp'] > 0) ? '<a target="_blank" href="' . $linkPublicaI24 . '"><button type="button" id="i24" class="btn btn-spartan  i24  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Actualiza I24" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '<a target="_blank" href="' . $linkPublicaI24 . '"><button type="button" id="i24Act" class="btn btn-warning  i24  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Publica I24" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>';
                    $btni24Elimina = ($row['RZonaProp'] > 0) ? '<a target="_blank" href="' . $linkRetiraI24 . '"><button type="button" id="i24Eli" class="btn btn-danger  i24  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Despublica I24" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '';
                    $i24           = '<div class="col-sm-3"><div class="col-sm-12">' . $logoi24 . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['RZonaProp'] . '</span> ' . $btni24 . ' ' . $btni24Elimina . '</div></div>';
                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 14) > 0) {
                    $logoIdonde       = '<a href="' . $linkIdonde . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(14) . '"></a>';
                    $btnIdonde        = ($row['RIdnd'] > 0) ? '<a target="_blank" href="' . $linkPublicaIdonde . '"><button type="button" id="iDonde" class="btn btn-spartan  iDonde  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Actualiza Idonde" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '<a target="_blank" href="' . $linkPublicaIdonde . '"><button type="button" id="iDondeAct" class="btn btn-warning iDonde  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Publica Idonde" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>';
                    $btnIdondeElimina = ($row['RMeli'] > 0) ? '<a target="_blank" href="' . $linkRetiraIdonde . '"><button type="button" id="iDondeEli" class="btn btn-danger  iDonde  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Despublica Idonde" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '';
                    $iDonde           = '<div class="col-sm-3"><div class="col-sm-12">' . $logoIdonde . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['RIdnd'] . '</span> ' . $btnIdonde . ' ' . $btnIdondeElimina . '</div></div>';
                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 18) > 0) {
                    $logoLocanto       = '<a href="' . $linkLocanto . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(18) . '"></a>';
                    $btnLocanto        = ($row['Publicalocanto'] == 'Publicado') ? '<a target="_blank" href="' . $linkPublicaLocanto . '"><button type="button" id="locanto" class="btn btn-spartan  locanto  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Actualiza Locanto" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '<a target="_blank" href="' . $linkPublicaLocanto . '"><button type="button" id="locantoAct" class="btn btn-warning  locanto  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Publica Locanto" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>';
                    $btnLocantoElimina = ($row['Publicalocanto'] == 'Publicado') ? '<a target="_blank" href="' . $linkRetiraLocanto . '"><button type="button" id="locantoEli" class="btn btn-danger  locanto  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Despublica Locanto" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '';
                    $locanto           = '<div class="col-sm-3"><div class="col-sm-12">' . $logoLocanto . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['Publicalocanto'] . '</span> ' . $btnLocanto . ' ' . $btnLocantoElimina . '</div></div>';
                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 12) > 0) //no activo
                {
                    $logoProperati = '<a href="' . $linkproperati . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(12) . '"></a>';
                    $btnProperati  = ($row['PublicaProperati'] == 'Publicado') ? '<button type="button" id="properati" class="btn btn-spartan  properati  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="MetroCuadrado" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button>' : '<button type="button" id="properatiAct" class="btn btn-warning  properati  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Properati" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button>';
                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 13) > 0) //no activo
                {
                    $logoLamudi = '<a href="' . $link . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(13) . '"></a>';
                    $btnLamudi  = ($row['PublicaLamudi'] == 'Publicado') ? '<button type="button" id="lamudi" class="btn btn-spartan  lamudi  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="MetroCuadrado" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button>' : '<button type="button" id="lamudiAct" class="btn btn-warning   lamudi  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Lamudi" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button>';
                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 20) > 0) //exclusivo para abad faciolince
                {
                    $logoAbad       = '<img src="' . $rutalogo . $this->getLogoPortal(20) . '">';
                    $btnAbad        = ($row['PublicaAbad'] == 'Publicado') ? '<button type="button" id="abad" class="btn btn-spartan  abad  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Actualiza Idonde" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button>' : '<button type="button" id="abadAct" class="btn btn-warning  abad  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Publica Idonde" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button>';
                    $btnAbadElimina = ($row['PublicaAbad'] == 'Publicado') ? '<button type="button" id="abadEli" class="btn btn-danger  abad  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Despublica Abad" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button>' : '';
                    $abad           = '<div class="col-sm-3"><div class="col-sm-12">' . $logoAbad . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['RAfydi'] . '</span> ' . $btnAfydi . ' ' . $btnAfydiElimina . '</div></div>';

                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 15) > 0) {
                    $lOlx          = ($row['PublicaOlx'] == 'Publicado') ? '<a href="' . $linkOlx . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(15) . '"></a>' : '<img src="' . $rutalogo . $this->getLogoPortal(15) . '">';
                    $logoOlx       = ($this->verifyPortales($row['IdInmobiliaria'], 15) > 0) ? $lOlx : '';
                    $btnOlx        = ($row['PublicaOlx'] == 'Publicado') ? '<a target="_blank" href="' . $linkOlxPublica . '"><button type="button" id="olx" class="btn btn-spartan  olx  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Actualiza Olx" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '<a target="_blank" href="' . $linkOlxPublica . '"><button type="button" id="olxAct" class="btn btn-warning olx btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Publica Olx" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>';
                    $btnOlxElimina = ($row['PublicaOlx'] == 'Publicado') ? '<a target="_blank" href="' . $linkOlxElimina . '"><button type="button" id="olxEli" class="btn btn-danger  olx  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Despublica Olx" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '';
                    $olx           = '<div class="col-sm-3"><div class="col-sm-12">' . $logoOlx . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['PublicaOlx'] . '</span> ' . $btnOlx . ' ' . $btnOlxElimina . '</div></div>';
                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 23) > 0) {
                    $logoAfydi       = '<img src="' . $rutalogo . $this->getLogoPortal(23) . '">';
                    $btnAfydi        = ($row['RAfydi'] > 0) ? '<a target="_blank" href="' . $linkPublicaAfydi . '"><button type="button" id="afydi" class="btn btn-spartan  afydi  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Actualiza Afydi" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '<a target="_blank" href="' . $linkPublicaAfydi . '"><button type="button" id="afydiAct" class="btn btn-warning afydi btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Publica Afydi" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>';
                    $btnAfydiElimina = ($row['RAfydi'] > 0) ? '<a target="_blank" href="' . $linkRetiraAfydi . '"><button type="button" id="afydiEli" class="btn btn-danger  afydi  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Despublica Afydi" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '';
                    $afydi           = '<div class="col-sm-3"><div class="col-sm-12">' . $logoAfydi . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['RAfydi'] . '</span> ' . $btnAfydi . ' ' . $btnAfydiElimina . '</div></div>';

                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 4) > 0) {
                    //$logoMicasa        ='<a href="'.$linkMicasa.'" target="_blank"><img src="'.$rutalogo.$this->getLogoPortal(4).'"></a>';
                    $logoMicasa       = '<img src="' . $rutalogo . $this->getLogoPortal(4) . '">';
                    $btnMicasa        = ($row['PublicaMiCasa'] == 'Publicado') ? '<a target="_blank" href="' . $linkPublicaMicasa . '"><button type="button" id="micasa" class="btn btn-spartan  micasa  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Actualiza Micasa" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '<a target="_blank" href="' . $linkPublicaMicasa . '"><button type="button" id="micasaAct" class="btn btn-warning  micasa  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Publica Micasa" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>';
                    $btnMicasaElimina = ($row['PublicaMiCasa'] == 'Publicado') ? '<a target="_blank" href="' . $linkRetiraMicasa . '"><button type="button" id="micasaEli" class="btn btn-danger  micasa  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Despublica Micasa" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button></a>' : '';
                    $Micasa           = '<div class="col-sm-3"><div class="col-sm-12">' . $logoMicasa . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['PublicaMiCasa'] . '</span> ' . $btnMicasa . ' ' . $btnMicasaElimina . '</div></div>';
                }
                $data[] = array(
                    "RLaGuia"        => $finca,
                    "RMtoCuadrado"   => $metro,
                    "meli"           => $meli,
                    "iDonde"         => $iDonde,
                    "RNCasa"         => $nCasa,
                    "PublicaOlx"     => $olx,
                    "Publicalocanto" => $locanto,
                    "PublicaDoomos"  => $doomos,
                    "i24"            => $i24,
                    "Goplaceit"      => $Goplaceit,
                    "iwanti"         => $iwanti,
                    "icasas"         => $icasas,
                    "Micasa"         => $Micasa,
                    "RAfydi"         => $afydi,
                );
                //             $data[]=array(
                //             "codRLaGuia"             => $row['RLaGuia'],
                //             "codRMtoCuadrado"          => $row['RMtoCuadrado'],
                // "codRVivaReal"             => $row['RVivaReal'],
                // "codRMeli"                 => $row['RMeli'],
                // "codRNcasa"             => $row['RNcasa'],
                // "codRZonaProp"             => $row['RZonaProp'],
                // "codRIdnd"                 => $row['RIdnd'],
                // "codRMiCasa"             => $row['RMiCasa'],
                // "codPublicaOlx"         => $row['PublicaOlx'],
                // "codPublicalocanto"     => $row['Publicalocanto'],
                // "codPublicaProperati"     => $row['PublicaProperati'],
                // "codPublicaLamudi"         => $row['PublicaLamudi'],
                // "codPublicaAbad"         => $row['PublicaAbad'],
                // "codRAfydi"             => $row['RAfydi'],
                // "logoFinca"                => $logoFinca,
                // "logoM2"                => $logoM2,
                // "logoMeli"                => $logoMeli,
                // "logoNc"                => $logoNc,
                // "logoi24"                => $logoi24,
                // "logoIdonde"            => $logoIdonde,
                // "logoLocanto"            => $logoLocanto,
                // "logoProperati"            => $logoProperati,
                // "logoLamudi"            => $logoLamudi,
                // "logoAbad"                => $logoAbad,
                // "logoOlx"                => $logoOlx,
                // "btnFinca"                => $btnFinca,
                // "btnMeli"                => $btnMeli,
                // "btnNc"                    => $btnNc,
                // "btni24"                => $btni24,
                // "btnIdonde"                => $btnIdonde,
                // "btnLocanto"            => $btnLocanto,
                // "btnProperati"            => $btnProperati,
                // "btnLamudi"                => $btnLamudi,
                // "btnOlx"                => $btnOlx,
                // "btnAbad"                => $btnAbad,
                // "linkFinca"                => $linkFinca,
                // "linkM2"                => $linkM2,
                // "linkMeli"                => $linkMeli,
                // "linkMeli"                => $linkMeli
                //             );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function datosInmueblePortalesMasivo($datas)
    {

        $connPDO                 = new Conexion();
        $idInmu                  = $datas['inmu'];
        $dretiro                 = $datas['idretiro'];
        list($codinmob, $parte2) = explode("-", $idInmu);
        $stmt                    = $connPDO->prepare("SELECT idInm,RLaGuia,RMtoCuadrado,RVivaReal,RMeli,RNcasa,
            RZonaProp,RIdnd,RMiCasa,PublicaOlx,Publicalocanto,Publicagpt,IdGestion,PublicaMiCasa,
            PublicaProperati,PublicaLamudi,PublicaAbad,dirMeli,RAfydi,IdInmobiliaria,PublicaDoomos,flagm2
            FROM  inmuebles
            WHERE idInm=:idInmu
            LIMIT 0,1");

        if ($stmt->execute(array(
            ":idInmu" => $idInmu,
        ))) {
            $rutalogo            = "https://www.simiinmobiliarias.com/mcomercialweb/images/";
            $datsa               = array();
            $btnM2               = '';
            $btnFinca            = '';
            $btnMeli             = '';
            $btnNc               = '';
            $btni24              = '';
            $btnIdonde           = '';
            $btnGoplaceit        = '';
            $btnLocanto          = '';
            $btnProperati        = '';
            $btnLamudi           = '';
            $btnOlx              = '';
            $btnAbad             = '';
            $afydi               = '';
            $locanto             = '';
            $btnM2Elimina        = '';
            $btnFincaElimina     = '';
            $btnMeliElimina      = '';
            $btnNcElimina        = '';
            $btni24Elimina       = '';
            $btnGoplaceitElimina = '';
            $btnIdondeElimina    = '';
            $btnLocantoElimina   = '';
            $btnProperatiElimina = '';
            $btnLamudiElimina    = '';
            $btnOlxElimina       = '';
            $btnAbadElimina      = '';
            $Micasa              = '';
            $btnMicasa           = '';
            $btnMicasaElimina    = '';

            $usuarioHabilitado = getCampo('usuariosguia', "where inmog=" . $codinmob . " limit 0,1", 'idusguia');
            $usuarioFinca      = ($dretiro != 1) ? $_SESSION['Id_Usuarios'] : $usuarioHabilitado;
            while ($row = $stmt->fetch()) {

                $linkFinca = ($row['RLaGuia'] > 0) ? 'http://www.fincaraiz.com.co/detail.aspx?a=' . $row['RLaGuia'] : '';
                $linkM2    = ($row['RMtoCuadrado'] > 0) ? 'http://www.metrocuadrado.com/web/busqueda/inmueble/' . $row['RMtoCuadrado'] : '';
                //$linkMeli=($row['RMeli']>0)?'http://casa.mercadolibre.com.co/'.$row['RMeli'].'-inmueble-_JM':'';
                $linkMeli1   = ($row['RMeli'] > 0) ? 'http://www.eltiempo.com' : '';
                $linkNc      = ($row['RNcasa'] > 0) ? 'http://www.nuestracasa.com.co/propiedades.php?id_inmueble=' . $row['RNcasa'] : '';
                $linki24     = ($row['RZonaProp'] > 0) ? 'http://www.inmuebles24.co/propiedades/inmueble-' . $row['RZonaProp'] . '.html' : '';
                $linkIwanti  = 'http://co.iwanti.com/anuncio/index?BusquedaForm%5BidCategoria%5D=4&BusquedaForm%5BtextoABuscar%5D=' . $idInmu;
                $idInmu1     = str_replace("-", "", $idInmu);
                $linkOlx     = 'https://bogotacity.olx.com.co/nf/search/' . $idInmu1;
                $linkLocanto = 'http://www.locanto.com.co';
                $linkIdonde  = 'http://www.idonde.com';

                $linkPublicaFinca = '../../mcomercialweb/a_inmueblesportalesGUIANvo2.php?publica=' . $idInmu . '&usr=' . $usuarioFinca . '&est=2';
                $linkRetiraFinca  = '../../mcomercialweb/a_inmueblesportalesGUIANvo2.php?publica=' . $idInmu . '&usr=' . $_SESSION['Id_Usuarios'] . '&est=3';
                //'../../mcomercialweb/portales/framefinca.php';
                if (getCampo('inmobiliaria', "where IdInmobiliaria=" . $_SESSION['IdInmmo'], 'activarn') == 1) {
                    $app_rutam2 = "../../mcomercialweb/portales/a_inmueblesportalesMCNvo3.php";
                } else {
                    $app_rutam2 = "../../mcomercialweb/portales/a_inmueblesportalesMCNvo.php";
                }
                $linkPublicaMetro     = $app_rutam2 . '?publica=' . $idInmu . '&usr=' . $_SESSION['Id_Usuarios'];
                $linkActualizaMetro   = $app_rutam2 . '?publica=' . $idInmu . '&op=2&lg=4&cod_repu=' . $row['RMtoCuadrado'] . '&usr=' . $_SESSION['Id_Usuarios'];
                $linkEliminaMetro     = '../../mcomercialweb/portales/a_eliminacionMC.php?publica=' . $idInmu . '&cod_repu=' . $row['RMtoCuadrado'] . '&usr=' . $_SESSION['Id_Usuarios'];
                $linkPublicaMeli      = '../../PHPSDK/file_export2.php?opt=1&publicar=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkActualizaMeli    = '../../PHPSDK/file_export2.php?opt=2&publicar=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkRetiraMeli       = '../../PHPSDK/file_export2.php?opt=3&publicar=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkPublicaNcasa     = '../../Portales/nuestraCasa/arrayNCPro.php?data=5&inmu=' . $idInmu;
                $linkRetiraNcasa      = '../../Portales/nuestraCasa/arrayNCPro.php?data=2&inmu=' . $idInmu;
                $linkPublicaI24       = 'http://web3.siminmueble.com/mcomercialweb/a_inmueblesportalesZP_1.php?publica=' . $idInmu . '&usr=' . $_SESSION['Id_Usuarios'];
                $linkRetiraI24        = 'http://web3.siminmueble.com/mcomercialweb/e_inmueblesportalesZP2.php?idinmu=' . $idInmu . '&cod_inmo=' . $row['IdInmobiliaria'];
                $linkPublicaIdonde    = '../../mcomercialweb/a_inmueblesportalesIdonde.php?publica=' . $idInmu . '&est=1&usr=' . $_SESSION['Id_Usuarios'];
                $linkRetiraIdonde     = '../../mcomercialweb/a_inmueblesportalesIdonde.php?publica=' . $idInmu . '&est=2"';
                $linkPublicaLocanto   = '../../mcomercialweb/portales/a_locanto.php?publica=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkRetiraLocanto    = '../../mcomercialweb/portales/a_locanto.php?publica=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkPublicaGoplaceit = '../../mcomercialweb/a_inmueblesportalesGP2.php?idinmu=' . $idInmu . '&inmob=' . $row['IdInmobiliaria'];
                $linkRetiraGoplaceit  = '../../mcomercialweb/a_eliminaportalesGP.php?idinmu=' . $idInmu . '&inmob=' . $row['IdInmobiliaria'];
                $linkPublicaAfydi     = '../../mwc/Afydi/apiAfydi.php?codInmu=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkRetiraAfydi      = '../../restClient/index2.php?publica=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkPublicaMicasa    = '../../mcomercialweb/a_inmueblesportalesmicasa.php?publica=' . $idInmu . '&usr=' . $_SESSION['Id_Usuarios'];
                $linkRetiraMicasa     = '../../mcomercialweb/e_inmueblesportalesMiCasa.php?publica=' . $idInmu;
                $linkOlxPublica       = '../../mcomercialweb/a_inmuebles_olx.php?publica=' . $idInmu . '&gest=' . $rows['IdGestion'] . '&usr' . $_SESSION['Id_Usuarios'];
                $linkOlxElimina       = '../../mcomercialweb/a_inmuebles_olx.php?retirar=1&publica=' . $idInmu . '&gest=' . $rows['IdGestion'];
                $linkPublicaZhabitat  = 'https://www.simiinmobiliarias.com/mcomercialweb/a_inmueblesportalesZHabitat.php?data=1&publica=' . $idInmu;
                $linkEliminaZhabitat  = 'https://www.simiinmobiliarias.com/mcomercialweb/a_inmueblesportalesZHabitat.php?data=2&publica=' . $idInmu;
                $btnError             = '<div class="clearfix">&nbsp</div><button data-toggle="modal" data-target="#modalErroesPortales" type="button" class="btn btn-info btn-xs veriErrors"><i class="fa fa-eye"></i> Ver errores</button>';
                /*********************************
                 *           Doomos              *
                 *********************************/
                $sincroDoomos = getCampo('settings_portales', "where portal_p=1 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                if ($this->verifyPortales($row['IdInmobiliaria'], 1) > 0 && $sincroDoomos == 1) {
                    $logoDoomos = ($this->verifyPortales($row['IdInmobiliaria'], 1) > 0) ? '<a href="' . $link . '" target="_blank"><img src="../../mcomercialweb/images/log_gratuitos.jpg"></a>' : '';
                    $btnDomos   = '<span class="label label-primary">Masivo</span> ';
                    $doomos     = '<div class="col-sm-3"><div class="col-sm-12">' . $logoDoomos . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['PublicaDoomos'] . '</span> ' . $btnDomos . ' ' . $btnDomose . '</div></div>';
                }
                /*********************************
                 *           iwanti              *
                 *********************************/
                $sincroIwanti = getCampo('settings_portales', "where portal_p=1 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                if ($this->verifyPortales($row['IdInmobiliaria'], 22) > 0 && $sincroIwanti == 1) {
                    $logoIwanti = ($this->verifyPortales($row['IdInmobiliaria'], 22) > 0) ? '<a href="' . $link . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(22) . '"></a>' : '';
                    $btnDomos   = '<span class="label label-primary">Masivo</span> ';
                    $iwanti     = '<div class="col-sm-3"><div class="col-sm-12">' . $logoIwanti . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['PublicaDoomos'] . '</span> ' . $btnDomos . ' ' . $btnDomose . '</div></div>';
                }
                /*********************************
                 *           Icasas          *
                 *********************************/
                $sincroIcasas = getCampo('settings_portales', "where portal_p=1 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                if ($this->verifyPortales($row['IdInmobiliaria'], 24) > 0 && $sincroIcasas == 1) {
                    $logoIcasas = ($this->verifyPortales($row['IdInmobiliaria'], 24) > 0) ? '<a href="' . $link . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(24) . '"></a>' : '';
                    $btnDomos   = '<span class="label label-primary">Masivo</span> ';
                    $icasas     = '<div class="col-sm-3"><div class="col-sm-12">' . $logoIcasas . '</div><div class="col-sm-12">' . '<span class="label label-success">' . $row['PublicaDoomos'] . '</span> ' . $btnDomos . ' ' . $btnDomose . '</div></div>';
                }

                /*********************************
                 *           finca Raiz          *
                 *********************************/
                $sincrofincaRaiz = getCampo('settings_portales', "where portal_p=8 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $errorFinca      = getCampo('settings_portales', "where portal_p=8 and tipo_p=6 and inmu_p='" . $idInmu . "'", 'vlr_p');

                $logoFinca = ($this->verifyPortales($row['IdInmobiliaria'], 8) > 0) ? '<img src="' . $rutalogo . $this->getLogoPortal(8) . '">' : '';
                if ($this->verifyPortales($row['IdInmobiliaria'], 8) > 0 && $sincrofincaRaiz == 1 && $errorFinca == 0) {
                    $iframeFinca = "";
                    if ($datas['method'] == 1) {

                        $iframeFinca = ($row['RLaGuia'] > 0) ? '<iframe width="100%" height="200" src="' . $linkPublicaFinca . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<iframe width="100%" height="200" src="' . $linkPublicaFinca . '" frameborder="0" allowfullscreen scrolling="no"></iframe>';
                    } elseif ($datas['method'] == 2) {

                        $iframeFinca = ($row['RLaGuia'] > 0) ? '<iframe width="100%" height="200" src="' . $linkRetiraFinca . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<h4><label class="label label-warning">Este Inmueble no esta Publicado en Fincaraiz</label></h4>';
                    }

                    $finca = '<div class="col-sm-6" >
                                    <div class="col-sm-12">' . $logoFinca . '</div>
                                    <div class="col-sm-12">' . $iframeFinca . '</div>
                                    <div class="clrearfix">&nbsp</div>
                                </div>';
                } else {
                    if ($errorFinca == 1 && $sincrofincaRaiz == 1) {
                        $finca = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoFinca . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-danger">Este inmueble tiene errores para sincronizar a este portal</span>' . $btnError . '
                                        <input type="hidden" name="codPortal" value="8">
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    } else {

                        $finca = '<div class="col-sm-6 inactivo">
                                        <div class="col-sm-12">' . $logoFinca . '</div>
                                        <div class="col-sm-12">
                                            <span class="label label-warning">Portal no activo para este inmueble</span>
                                        </div>
                                    </div><div class="clrearfix">&nbsp</div>';
                    }
                }
                /*********************************
                 *           Metro Cuadrado      *
                 *********************************/
                $sincroMetrocuadrado = getCampo('settings_portales', "where portal_p=10 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $errorMetro          = getCampo('settings_portales', "where portal_p=10 and tipo_p=6 and inmu_p='" . $idInmu . "'", 'vlr_p');

                $logoM2 = ($this->verifyPortales($row['IdInmobiliaria'], 10) > 0) ? '<img src="' . $rutalogo . $this->getLogoPortal(10) . '">' : '';
                if ($this->verifyPortales($row['IdInmobiliaria'], 10) > 0 && $row['flagm2'] != 2 && $sincroMetrocuadrado == 1 && $errorMetro == 0) {
                    //echo "aaaa ".$row['flagm2'];
                    $iframeM2 = "";

                    if ($datas['method'] == 1) {

                        $iframeM2 = ($row['RMtoCuadrado'] > 0) ? '<iframe width="100%" height="200" src="' . $linkActualizaMetro . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<iframe width="100%" height="200" src="' . $linkPublicaMetro . '&valor=' . $sincroMetrocuadrado . '" frameborder="0" allowfullscreen scrolling="no"></iframe>';
                    } elseif ($datas['method'] == 2) {

                        $iframeM2 = ($row['RMtoCuadrado'] > 0) ? '<iframe width="100%" height="200" src="' . $linkEliminaMetro . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<h4><label class="label label-warning">Este Inmueble no esta Publicado en m2</label></h4>';
                    }

                    $metro = '<div class="col-sm-6 inactivo" >
                                    <div class="col-sm-12">' . $logoM2 . '</div>
                                    <div class="col-sm-12">' . $iframeM2 . '</div>
                                    <div class="clrearfix">&nbsp</div>
                                </div>';
                } else {
                    if ($errorMetro == 1 && $sincroMetrocuadrado == 1) {

                        $metro = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoM2 . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-danger">Este inmueble tiene errores para sincronizar a este portal</span>' . $btnError . '
                                        <input type="hidden" name="codPortal" value="10">
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    } else {
                        $metro = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoM2 . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-warning">Portal no activo para este inmueble</span>
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    }
                }
                /*********************************
                 *           Meli                *
                 *********************************/
                $sincroMercadoLibre = getCampo('settings_portales', "where portal_p=2 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $errorMeli          = getCampo('settings_portales', "where portal_p=2 and tipo_p=6 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $logoMeli           = ($this->verifyPortales($row['IdInmobiliaria'], 2) > 0) ? '<img src="' . $rutalogo . $this->getLogoPortal(2) . '">' : '';
                if ($this->verifyPortales($row['IdInmobiliaria'], 2) > 0 && $sincroMercadoLibre == 1 && $errorMeli == 0) {

                    $iframeMeli = "";

                    if ($datas['method'] == 1) {

                        $iframeMeli = ($row['RMeli'] > 0) ? '<iframe width="100%" height="200" src="' . $linkActualizaMeli . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<iframe width="100%" height="200" src="' . $linkPublicaMeli . '" frameborder="0" allowfullscreen scrolling="no"></iframe>';
                    } elseif ($datas['method'] == 2) {

                        $iframeMeli = ($row['RMeli'] > 0) ? '<iframe width="100%" height="200" src="' . $linkRetiraMeli . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<h4><label class="label label-warning">Este Inmueble no esta Publicado en Mercado Libre</label></h4>';
                    }

                    $meli = '<div class="col-sm-6">
                                    <div class="col-sm-12">' . $logoMeli . '</div>
                                    <div class="col-sm-12">' . $iframeMeli . '</div>
                                    <div class="clrearfix">&nbsp</div>
                                </div>';
                } else {
                    if ($errorMeli == 1 && $sincroMercadoLibre == 1) {
                        $meli = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoMeli . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-danger">Este inmueble tiene errores para sincronizar a este portal</span>' . $btnError . '
                                        <input type="hidden" name="codPortal" value="2">
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    } else {
                        $meli = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoMeli . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-warning">Portal no activo para este inmueble</span>
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    }

                }
                /*********************************
                 *         goplaceit             *
                 *********************************/
                $sincroGoplaceit = getCampo('settings_portales', "where portal_p=6 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $errorGoplaceit  = getCampo('settings_portales', "where portal_p=6 and tipo_p=6 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $logoGoplaceit   = ($this->verifyPortales($row['IdInmobiliaria'], 6) > 0) ? '<img src="' . $rutalogo . $this->getLogoPortal(6) . '">' : '';
                if ($this->verifyPortales($row['IdInmobiliaria'], 6) > 0 && $sincroGoplaceit == 1 && $errorGoplaceit == 0) {

                    $iframeGoplaceit = "";

                    if ($datas['method'] == 1) {

                        $iframeGoplaceit = ($row['Publicagpt'] == 'Publicado') ? '<iframe width="100%" height="200" src="' . $linkPublicaGoplaceit . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<iframe width="100%" height="200" src="' . $linkPublicaGoplaceit . '" frameborder="0" allowfullscreen scrolling="no"></iframe>';
                    } elseif ($datas['method'] == 2) {

                        $iframeGoplaceit = ($row['Publicagpt'] == 'Publicado') ? '<iframe width="100%" height="200" src="' . $linkRetiraGoplaceit . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<h4><label class="label label-warning">Este Inmueble no esta Publicado en Goplaceit</label></h4>';
                    }

                    $Goplaceit = '<div class="col-sm-6">
                                    <div class="col-sm-12">' . $logoGoplaceit . '</div>
                                    <div class="col-sm-12">' . $iframeGoplaceit . '</div>
                                    <div class="clrearfix">&nbsp</div>
                                </div>';
                } else {
                    if ($errorGoplaceit == 1 && $sincroGoplaceit == 1) {
                        $Goplaceit = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoGoplaceit . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-danger">Este inmueble tiene errores para sincronizar a este portal</span>' . $btnError . '
                                        <input type="hidden" name="codPortal" value="6">
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    } else {
                        $Goplaceit = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoGoplaceit . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-warning">Portal no activo para este inmueble</span>
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    }

                }
                /*********************************
                 *         Nuestra Casa          *
                 *********************************/
                $sincroNcasa = getCampo('settings_portales', "where portal_p=25 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $errorNcasa  = getCampo('settings_portales', "where portal_p=25 and tipo_p=6 and inmu_p='" . $idInmu . "'", 'vlr_p');

                $logoNc = '<img src="' . $rutalogo . $this->getLogoPortal(25) . '">';
                if ($this->verifyPortales($row['IdInmobiliaria'], 25) > 0 && $sincroNcasa == 1 && $errorNcasa == 0) {

                    $iframeNc = "";
                    if ($datas['method'] == 1) {

                        $iframeNc = ($row['RNcasa'] > 0) ? '<iframe width="100%" height="200" src="' . $linkPublicaNcasa . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<iframe width="100%" height="200" src="' . $linkPublicaNcasa . '" frameborder="0" allowfullscreen scrolling="no"></iframe>';
                    } elseif ($datas['method'] == 2) {

                        $iframeNc = ($row['RNcasa'] > 0) ? '<iframe width="100%" height="200" src="' . $linkRetiraNcasa . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<h4><label class="label label-warning">Este Inmueble no esta Publicado en Goplaceit</label></h4>';
                    }

                    $nCasa = '<div class="col-sm-6">
                                    <div class="col-sm-12">' . $logoNc . '</div>
                                    <div class="col-sm-12">' . $iframeNc . '</div>
                                    <div class="clrearfix">&nbsp</div>
                                </div>';

                } else {
                    if ($errorNcasa == 1 && $sincroNcasa == 1) {

                        $nCasa = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoNc . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-danger">Este inmueble tiene errores para sincronizar a este portal</span>' . $btnError . '
                                        <input type="hidden" name="codPortal" value="25">
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    } else {
                        $nCasa = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoNc . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-warning">Portal no activo para este inmueble</span>
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    }
                }
                /*********************************
                 *        Inmuebles24            *
                 *********************************/
                $sincroI24 = getCampo('settings_portales', "where portal_p=5 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $errorI24  = getCampo('settings_portales', "where portal_p=5 and tipo_p=6 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $logoI24   = '<img src="' . $rutalogo . $this->getLogoPortal(5) . '">';
                if ($this->verifyPortales($row['IdInmobiliaria'], 5) > 0 && $sincroI24 == 1 && $errorI24 == 0) {

                    $iframeI24 = "";

                    if ($datas['method'] == 1) {

                        $iframeI24 = ($row['RZonaProp'] > 0) ? '<iframe width="100%" height="200" src="' . $linkPublicaI24 . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<iframe width="100%" height="200" src="' . $linkPublicaI24 . '" frameborder="0" allowfullscreen scrolling="no"></iframe>';
                    } elseif ($datas['method'] == 2) {

                        $iframeI24 = ($row['RZonaProp'] > 0) ? '<iframe width="100%" height="200" src="' . $linkRetiraI24 . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<h4><label class="label label-warning">Este Inmueble no esta Publicado en Inmuebles 24</label></h4>';
                    }

                    $i24 = '<div class="col-sm-6">
                                    <div class="col-sm-12">' . $logoI24 . '</div>
                                    <div class="col-sm-12">' . $iframeI24 . '</div>
                                    <div class="clrearfix">&nbsp</div>
                                </div>';
                } else {
                    if ($errorI24 == 1 && $sincroI24 == 1) {
                        $i24 = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoI24 . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-danger">Este inmueble tiene errores para sincronizar a este portal</span>' . $btnError . '
                                        <input type="hidden" name="codPortal" value="5">
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    } else {
                        $i24 = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoI24 . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-warning">Portal no activo para este inmueble</span>
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    }

                }
                /*********************************
                 *          Idonde               *
                 *********************************/
                $sincroIdonde = getCampo('settings_portales', "where portal_p=14 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $errorIdonde  = getCampo('settings_portales', "where portal_p=14 and tipo_p=6 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $logoIdonde   = '<img src="' . $rutalogo . $this->getLogoPortal(14) . '">';
                if ($this->verifyPortales($row['IdInmobiliaria'], 14) > 0 && $sincroIdonde == 1 && $errorIdonde == 0) {
                    $iframeIdonde = "";

                    if ($datas['method'] == 1) {

                        $iframeIdonde = ($row['RIdnd'] > 0) ? '<iframe width="100%" height="200" src="' . $linkPublicaIdonde . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<iframe width="100%" height="200" src="' . $linkPublicaIdonde . '" frameborder="0" allowfullscreen scrolling="no"></iframe>';
                    } elseif ($datas['method'] == 2) {

                        $iframeIdonde = ($row['RIdnd'] > 0) ? '<iframe width="100%" height="200" src="' . $linkRetiraIdonde . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<h4><label class="label label-warning">Este Inmueble no esta Publicado en Idonde</label></h4>';
                    }

                    $iDonde = '<div class="col-sm-6">
                                    <div class="col-sm-12">' . $logoIdonde . '</div>
                                    <div class="col-sm-12">' . $iframeIdonde . '</div>
                                    <div class="clrearfix">&nbsp</div>
                                </div>';

                } else {
                    if ($errorIdonde == 1 && $sincroIdonde == 1) {
                        $iDonde = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoIdonde . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-danger">Este inmueble tiene errores para sincronizar a este portal</span>' . $btnError . '
                                        <input type="hidden" name="codPortal" value="14">
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    } else {
                        $iDonde = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoIdonde . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-warning">Portal no activo para este inmueble</span>
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    }

                }
                /*********************************
                 *            Locanto            *
                 *********************************/
                $sincroLocanto = getCampo('settings_portales', "where portal_p=18 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $errorLocanto  = getCampo('settings_portales', "where portal_p=18 and tipo_p=6 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $logoLocanto   = '<img src="' . $rutalogo . $this->getLogoPortal(18) . '">';
                if ($this->verifyPortales($row['IdInmobiliaria'], 18) > 0 && $sincroLocanto == 1 && $errorLocanto == 0) {

                    $iframeLocanto = "";

                    if ($datas['method'] == 1) {

                        $iframeLocanto = ($row['Publicalocanto'] == 'Publicado') ? '<iframe width="100%" height="200" src="' . $linkPublicaLocanto . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<iframe width="100%" height="200" src="' . $linkPublicaLocanto . '" frameborder="0" allowfullscreen scrolling="no"></iframe>';
                    } elseif ($datas['method'] == 2) {

                        $iframeLocanto = ($row['Publicalocanto'] == 'Publicado') ? '<iframe width="100%" height="200" src="' . $linkRetiraLocanto . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<h4><label class="label label-warning">Este Inmueble no esta Publicado en Locanto</label></h4>';
                    }

                    $locanto = '<div class="col-sm-6">
                                    <div class="col-sm-12">' . $logoLocanto . '</div>
                                    <div class="col-sm-12">' . $iframeLocanto . '</div>
                                    <div class="clrearfix">&nbsp</div>
                                </div>';
                } else {
                    if ($errorLocanto == 1 && $sincroLocanto == 1) {
                        $locanto = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoLocanto . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-danger">Este inmueble tiene errores para sincronizar a este portal</span>' . $btnError . '
                                        <input type="hidden" name="codPortal" value="18">
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    } else {
                        $locanto = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoLocanto . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-warning">Portal no activo para este inmueble</span>
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    }

                }
                if ($this->verifyPortales($row['IdInmobiliaria'], 13) > 0) //no activo
                {
                    $logoLamudi = '<a href="' . $link . '" target="_blank"><img src="' . $rutalogo . $this->getLogoPortal(13) . '"></a>';
                    $btnLamudi  = ($row['PublicaLamudi'] == 'Publicado') ? '<button type="button" id="lamudi" class="btn btn-spartan  lamudi  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="MetroCuadrado" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button>' : '<button type="button" id="lamudiAct" class="btn btn-warning   lamudi  btn-xs mensajeT"   data-toggle="tooltip" data-placement="bottom" title="Lamudi" ><span class="glyphicon glyphicon"><i class="fa fa-share-alt"></i></span></button>';
                }
                /*********************************
                 *              Olx              *
                 *********************************/
                $sincroOlx = getCampo('settings_portales', "where portal_p=18 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $errorOlx  = getCampo('settings_portales', "where portal_p=18 and tipo_p=6 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $logoOlx   = '<img src="' . $rutalogo . $this->getLogoPortal(15) . '">';
                if ($this->verifyPortales($row['IdInmobiliaria'], 15) > 0 && $sincroOlx == 1 && $errorOlx == 0) {

                    $iframeOlx = "";

                    if ($datas['method'] == 1) {

                        $iframeOlx = ($row['PublicaOlx'] == 'Publicado') ? '<iframe width="100%" height="200" src="' . $linkOlxPublica . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<iframe width="100%" height="200" src="' . $linkOlxPublica . '" frameborder="0" allowfullscreen scrolling="no"></iframe>';
                    } elseif ($datas['method'] == 2) {

                        $iframeOlx = ($row['PublicaOlx'] == 'Publicado') ? '<iframe width="100%" height="200" src="' . $linkOlxElimina . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<h4><label class="label label-warning">Este Inmueble no esta Publicado en OLX</label></h4>';
                    }

                    $olx = '<div class="col-sm-6">
                                    <div class="col-sm-12">' . $logoOlx . '</div>
                                    <div class="col-sm-12">' . $iframeOlx . '</div>
                                    <div class="clrearfix">&nbsp</div>
                                </div>';

                } else {
                    if ($errorOlx == 1 && $sincroOlx == 1) {
                        $olx = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoOlx . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-danger">Este inmueble tiene errores para sincronizar a este portal</span>' . $btnError . '
                                        <input type="hidden" name="codPortal" value="15">
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    } else {
                        $olx = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoOlx . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-warning">Portal no activo para este inmueble</span>
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    }

                }
                /*********************************
                 *           Afydi               *
                 *********************************/
                $sincroAfidy = getCampo('settings_portales', "where portal_p=23 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $errorAfidy  = getCampo('settings_portales', "where portal_p=23 and tipo_p=6 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $logoAfydi   = '<img src="' . $rutalogo . $this->getLogoPortal(23) . '">';
                if ($this->verifyPortales($row['IdInmobiliaria'], 23) > 0 && $sincroAfidy == 1 && $errorAfidy == 0) {

                    $iframeAfydi = "";

                    if ($datas['method'] == 1) {

                        $iframeAfydi = ($row['RAfydi'] > 0) ? '<iframe width="100%" height="200" src="' . $linkPublicaAfydi . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<iframe width="100%" height="200" src="' . $linkPublicaAfydi . '" frameborder="0" allowfullscreen scrolling="no"></iframe>';
                    } elseif ($datas['method'] == 2) {

                        $iframeAfydi = ($row['RAfydi'] > 0) ? '<iframe width="100%" height="200" src="' . $linkRetiraAfydi . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<h4><label class="label label-warning">Este Inmueble no esta Publicado en Afydi</label></h4>';
                    }

                    $afydi = '<div class="col-sm-6">
                                    <div class="col-sm-12">' . $logoAfydi . '</div>
                                    <div class="col-sm-12">' . $iframeAfydi . '</div>
                                    <div class="clrearfix">&nbsp</div>
                                </div>';
                } else {
                    if ($errorAfidy == 1 && $sincroAfidy == 1) {
                        $afydi = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoAfydi . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-danger">Este inmueble tiene errores para sincronizar a este portal</span>' . $btnError . '
                                        <input type="hidden" name="codPortal" value="23">
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    } else {
                        $afydi = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoAfydi . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-warning">Portal no activo para este inmueble</span>
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    }

                }
                /*********************************
                 *            Mi casa            *
                 *********************************/
                $sincroMiCasa = getCampo('settings_portales', "where portal_p=4 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $errorMiCasa  = getCampo('settings_portales', "where portal_p=4 and tipo_p=6 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $logoMicasa   = '<img src="' . $rutalogo . $this->getLogoPortal(4) . '">';
                if ($this->verifyPortales($row['IdInmobiliaria'], 4) > 0 && $sincroMiCasa == 1 && $errorMiCasa == 0) {

                    $iframeMicasa = "";

                    if ($datas['method'] == 1) {

                        $iframeMicasa = ($row['PublicaMiCasa'] == 'Publicado') ? '<iframe width="100%" height="200" src="' . $linkPublicaMicasa . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<iframe width="100%" height="200" src="' . $linkPublicaMicasa . '" frameborder="0" allowfullscreen scrolling="no"></iframe>';
                    } elseif ($datas['method'] == 2) {

                        $iframeMicasa = ($row['PublicaMiCasa'] == 'Publicado') ? '<iframe width="100%" height="200" src="' . $linkRetiraMicasa . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<h4><label class="label label-warning">Este Inmueble no esta Publicado en Mi casa</label></h4>';
                    }

                    $Micasa = '<div class="col-sm-6">
                                    <div class="col-sm-12">' . $logoMicasa . '</div>
                                    <div class="col-sm-12">' . $iframeMicasa . '</div>
                                    <div class="clrearfix">&nbsp</div>
                                </div>';
                } else {
                    if ($errorMiCasa == 1 && $sincroMiCasa == 1) {
                        $Micasa = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoMicasa . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-danger">Este inmueble tiene errores para sincronizar a este portal</span>' . $btnError . '
                                        <input type="hidden" name="codPortal" value="4">
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    } else {
                        $Micasa = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoMicasa . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-warning">Portal no activo para este inmueble</span>
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    }

                }
                /*********************************
                 *            Zona Habitat       *
                 *********************************/
                $sincroZh = getCampo('settings_portales', "where portal_p=28 and tipo_p=1 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $errorZh  = getCampo('settings_portales', "where portal_p=28 and tipo_p=6 and inmu_p='" . $idInmu . "'", 'vlr_p');
                $logoZh   = '<img src="' . $rutalogo . $this->getLogoPortal(28) . '">';

                if ($this->verifyPortales($row['IdInmobiliaria'], 28) > 0 && $sincroZh == 1 && $errorZh == 0) {

                    $iframeZh = "";
                    if ($datas['method'] == 1) {

                        $iframeZh = ($row['RZhabitat'] > 0) ? '<iframe width="100%" height="200" src="' . $linkPublicaZhabitat . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<iframe width="100%" height="200" src="' . $linkPublicaZhabitat . '" frameborder="0" allowfullscreen scrolling="no"></iframe>';
                    } elseif ($datas['method'] == 2) {

                        $iframeZh = ($row['RZhabitat'] > 0) ? '<iframe width="100%" height="200" src="' . $linkEliminaZhabitat . '" frameborder="0" allowfullscreen scrolling="no"></iframe>' : '<h4><label class="label label-warning">Este Inmueble no esta Publicado en Goplaceit</label></h4>';
                    }

                    $zhabitat = '<div class="col-sm-6">
                                    <div class="col-sm-12">' . $logoZh . '</div>
                                    <div class="col-sm-12">' . $iframeZh . '</div>
                                    <div class="clrearfix">&nbsp</div>
                                </div>';

                } else {
                    if ($errorNcasa == 1 && $sincroNcasa == 1) {

                        $zhabitat = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoZh . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-danger">Este inmueble tiene errores para sincronizar a este portal</span>' . $btnError . '
                                        <input type="hidden" name="codPortal" value="25">
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    } else {
                        $zhabitat = '<div class="col-sm-6 inactivo">
                                    <div class="col-sm-12">' . $logoZh . '</div>
                                    <div class="col-sm-12">
                                        <span class="label label-warning">Portal no activo para este inmueble</span>
                                    </div>
                                </div><div class="clrearfix">&nbsp</div>';
                    }
                }

                $data[] = array(
                    "RLaGuia"        => $finca,
                    "RMtoCuadrado"   => $metro,
                    "meli"           => $meli,
                    "iDonde"         => $iDonde,
                    "RNCasa"         => $nCasa,
                    "PublicaOlx"     => $olx,
                    "Publicalocanto" => $locanto,
                    "i24"            => $i24,
                    "Goplaceit"      => $Goplaceit,
                    "Micasa"         => $Micasa,
                    "RAfydi"         => $afydi,
                    "Zhabitat"       => $zhabitat,
                );

            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function getLogoPortal($idPortal)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("SELECT imagenp
            FROM  Portales
            WHERE IdPortal=:idPortal
            LIMIT 0,1");

        if ($stmt->execute(array(
            ":idPortal" => $idPortal,
        ))) {

            while ($row = $stmt->fetch()) {

                $data = $row['imagenp'];

            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function verifyPortales($id, $idPortal)
    {
        $connPDO = new Conexion();

        $stmt = $connPDO->prepare("Select p.IdPortal,p.IdInmobiliaria
                from PublicaPortales p
                where p.IdInmobiliaria= :idinmo
                and IdPortal=:portal");
        if ($stmt->execute(array(
            ":idinmo" => $id,
            ":portal" => $idPortal,
        ))) {
            $existe = $stmt->rowCount();
            return $existe;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function getDataM2Inmob($data)
    {
        $connPDO = new Conexion();
        $idInmo  = $data['inmo'];
        $stmt    = $connPDO->prepare("Select idusariom2,usuariom2,clavem2,prefijom2,letra,servidor,sucursalm2,sucurnomm2
                        From usuariosm2
                        Where inmogm2=:idinmo

                        group by usuariom2
                        #limit 0,1");

        if ($stmt->execute(array(
            ":idinmo" => $idInmo,
        ))) {

            $data = array();
            while ($row = $stmt->fetch()) {

                $data[] = array(
                    "usuariom2"  => $row['usuariom2'],
                    "clavem2"    => $row['clavem2'],
                    "prefijom2"  => $row['prefijom2'],
                    "letra"      => $row['letra'],
                    "servidor"   => $row['servidor'],
                    "sucursalm2" => $row['sucursalm2'],
                    "sucurnomm2" => $row['sucurnomm2'],

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function getDataFincaInmob($data)
    {
        $connPDO = new Conexion();
        $idInmo  = $data['inmo'];

        $stmt = $connPDO->prepare("Select idusguia,usuario,sucursalg,sucurnom,tel_suc,inmog,mail_suc
                        From usuariosguia
                        Where inmog=:idinmo

                        group by usuario,sucursalg
                        #limit 0,1");

        if ($stmt->execute(array(
            ":idinmo" => $idInmo,
        ))) {

            $data = array();
            while ($row = $stmt->fetch()) {

                $data[] = array(
                    "mail_suc"  => $row['mail_suc'],
                    "usuario"   => $row['usuario'],
                    "sucursalg" => $row['sucursalg'],
                    "sucurnom"  => $row['sucurnom'],
                    "tel_suc"   => $row['tel_suc'],

                );
            }
            return $data;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function deleteUserM2($data)
    {
        $connPDO = new Conexion();

        $idInmo     = $data['idInmo'];
        $sucursalm2 = $data['codSucursal'];

        $stmt = $connPDO->prepare("delete from usuariosm2
                                where inmogm2=:idinmo
                                and sucursalm2=:sucursalm2");

        if ($stmt->execute(array(
            ":idinmo"     => $idInmo,
            ":sucursalm2" => $sucursalm2,
        ))) {

            return 1;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function deleteUserFinca($data)
    {
        $connPDO = new Conexion();

        $idInmo     = $data['idInmo'];
        $sucursalm2 = $data['codSucursal'];

        $stmt = $connPDO->prepare("delete from usuariosguia
                                where inmog=:idinmo
                                and sucursalg=:sucursalm2");

        if ($stmt->execute(array(
            ":idinmo"     => $idInmo,
            ":sucursalm2" => $sucursalm2,
        ))) {

            return 1;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function deleteIdUserM2($data)
    {
        $connPDO = new Conexion();

        $idUsuario = $data['idUsuario'];

        $stmt = $connPDO->prepare("delete from usuariosm2
                                where idusariom2=:idUsuario");

        if ($stmt->execute(array(
            ":idUsuario" => $idUsuario,
        ))) {
            return 1;
        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }
    public function deleteIdUserFinca($data)
    {
        $connPDO = new Conexion();

        $idUsuario = $data['idUsuario'];

        $stmt = $connPDO->prepare("delete from usuariosguia
                                where idusguia=:idUsuario");

        if ($stmt->execute(array(
            ":idUsuario" => $idUsuario,
        ))) {

            return 1;

        } else {
            return print_r($stmt->errorInfo());

        }
        $stmt = null;
    }

}
