<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
include "../funciones/cge00001.php";
include "../funciones/myfncs.php";
include "../funciones/conni.php";
require_once '../mcomercialweb/nusoap/lib/nusoap.php';
include "../controllers/class.elpais.php";
include "../funciones/connPDO.php";
$w_conexion = new MySQL();
$connPDO  = new Conexion();
$elpais     = new elpais($conn,$connPDO);
$data    = $_GET['data'];
$usr_act = $_SESSION['Id_Usuarios'];
$codinm               = $_GET['inm'];
$debug                = ($_GET['mostrar']) ? 1 : 0;
list($inmobi, $inmue) = explode("-", $codinm);
$idElpais=getCampo('inmobiliaria', ' where IdInmobiliaria = ' .$inmobi, 'cod_elpais');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	    <!--Core CSS -->
    <link href="../mwc/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="../mwc/css/bootstrap-reset.css" rel="stylesheet">
    <link href="../mwc/font-awesome/css/font-awesome.css" rel="stylesheet" />
</head>
<body>
	

<?php
if ($inmobi != 0) {

	$d = $elpais->infoInmueble($codinm, $debug);
    $veficaCodigo=getCampo('inmuebles', ' where idInm = "' .$codinm.'"'  , 'RPais');

	if($_GET['mostrar']==2)
	{
		echo "<pre>";
		print_r($d);
		echo "</pre>";
		exit();
	}
    if ($data == 1) {

        $uri  = 'http://stage.inmuebles.elpais.rbmwebsolutions.com/apiREST.php/rest/'.$idElpais.'/customer_data.json';
        $datosCLiente=curl('GET', $uri, $data);
        $d['customerId']=$idElpais;
        $d['productId']=$datosCLiente['result']['purchaseOrder']['ProductId'];
        if($datosCLiente['result']['companyName']=="Simi Bronce")
        {
        	?>
        		<div class="alert alert-info">Peticiones Restantes: <?php echo 50-$datosCLiente['result']['purchaseOrder']['consumedAdvices'] ?> para publicar, si desea puede despublicar inmuebles para liberar peticiones.</div>
        	<?php 
        	if($datosCLiente['result']['purchaseOrder']['totalAdvices']==$datosCLiente['result']['purchaseOrder']['consumedAdvices'])
        	{
        		?>
        			<div class="alert alert-warning"><h4>No puede publicar mas al Portal, Contacte al Pais.com o elimine publicaciones para habilitar nuevas publicaciones </h4></div>
        		<?php 
        		exit();
        	}	
        }
        if($veficaCodigo > 0 )
        {
        	
        	$d['adviceId']=$veficaCodigo;
        	$uri  = 'http://stage.inmuebles.elpais.rbmwebsolutions.com/apiREST.php/rest/1/bulk/advice_update.json';
	        $data = json_encode($d);
        	$response=curl('PUT', $uri, $data);
        	if($response['result']['code']==401)
	        {
	        	echo $response['result']['log']; 
	        }
	        if($response['result']['code']==200)
	        {
	        	$codigo = $response['result']['RBM ID']; 
	        	?>
	        	<div class="alert alert-success"><h4>Se actualizo la publicacion del inmueble <?php echo $codigo; ?></h4>
					<a target="_blank" href="http://stage.inmuebles.elpais.rbmwebsolutions.com/aviso/apartamentos-alquiler-cali-sur-alameda-vp<?php echo $codigo ?>">Ver Publicación</a>
	        	</div>
	        	<?php
	        }

        	
        }else
        {

	        $uri  = 'http://stage.inmuebles.elpais.rbmwebsolutions.com/apiREST.php/rest/1/bulk/advice_importer.json';
	        $data = json_encode($d);

	        $response=curl('POST', $uri, $data);

	        if($response['result']['code']==401)
	        {
	        	echo $response['result']['log']; 
	        }
	        if($response['result']['code']==200)
	        {
	        	$codigo = $response['result']['RBM ID']; 

	        	 $response = $elpais->insertElpaisdtp($codinm,$codigo);
			        if($response['Error']>0)
			        {
			        	echo "<pre>";
			        	print_r($response['Error']);
			        	echo "</pre>";
			        }
			        else
			        {
			        	?>
			        		<div class="alert alert-success"><h4>Se  publico el inmueble <?php echo $codigo; ?></h4><a target="_blank" href="http://stage.inmuebles.elpais.rbmwebsolutions.com/aviso/apartamentos-alquiler-cali-sur-alameda-vp<?php echo $codigo ?>">Ver Publicación</a></div>
			        	<?php 
			        }
	        }

        }
        
    }
    if ($data == 2) {
    	if($veficaCodigo == 0)
    	{
    		?>
    			<div class="alert alert-warning"><h4>El inmueble <?php echo $codinm ?> no se puede eliminar por que no existe en el Portal el Pais </h4></div>
    		<?php 
    	}else
    	{

	    	//705738
	        $s=array("advice_id"=>$veficaCodigo);
	        $response = $elpais->deleteELpaisdtp($codinm,$veficaCodigo);
	        $uri  = 'http://stage.inmuebles.elpais.rbmwebsolutions.com/apiREST.php/rest/1/bulk/cancel_advice.json';
		    $data = json_encode($s);
		    $responses=curl('PUT', $uri, $data);
		    if($responses['Error']>0)
			        {
			        	echo "<pre>";
			        	print_r($response['Error']);
			        	echo "</pre>";
			        }
			if($responses['result']['code']==200)
	        {			
	        	
			        	$codigo = $response['result']['RBM ID']; 
			        	?>
			        		<div class="alert alert-success"><h4>Se  despublico el inmueble <?php echo $codigo; ?> Exitosamente</h4></div>
			        	<?php 
			        
	        }
    	}
    }

}



?>
</body>
</html>
<?php
$w_conexion->CerrarConexion();

function curl($method, $uri, $data)
{

    $token       = 'Authorization:  Basic c2ltaTpTMW0xdDQzbHRkNA=';
    $curl        = curl_init($uri);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $token));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    $data=curl_exec($curl);
    curl_close($curl);
    $response=json_decode($data,true);
  	return $response;

}
