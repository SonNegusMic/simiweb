<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
header('Content-Type: text/html; charset=utf-8');
include("../funciones/fncfields2.php");
include("../funciones/fncfields3.php");
include("../funciones/cge00001.php");
include("../funciones/myfncs.php");
include("../funciones/conni.php");
include("../funciones/connPDO.php");
include("../controllers/class.callCenter.php");
require('../mwc/inmuebles/PHPExcel/PHPExcel.php');
$w_conexion	 	= new MySQL();
$pdo= new Conexion();
$call=new CallCenter($pdo);
$objPHPExcel = new PHPExcel;

if( isset($_GET["report"]) ){

	

	$datas= array();

	$response = $call->filterClientes($datas,$_GET);

	
	



	$objPHPExcel->getProperties()->setCreator("Codedrinks") //Autor
			 ->setLastModifiedBy("Codedrinks") 
			 //Ultimo usuario que lo modificó
			 ->setTitle("Filtro busqueda de clientes")
			 ->setSubject("Filtro busqueda de clientes")
			 ->setDescription("Filtro busqueda de clientes")
			 ->setKeywords("Filtro busqueda de clientes")
			 ->setCategory("Reporte filtro excel");
	$tituloReporteDemanda = "FILTRO BUSQUEDA CLIENTES";

	$tituloReporteCitas = "FILTRO BUSQUEDA CLIENTES";

	$titulosColumnas = array(		
	'Nombre', 
	'Telefono',
	'Celular',
	'Correo',
	'Descripcion',
	'Estado',
	'Empresa',
	'Observacion',
	'Asesor'
	
	);

	$objPHPExcel->setActiveSheetIndex(0)
    ->mergeCells('A1:I1')
    ->mergeCells('A2:I2');

    // Se agregan los titulos del reporte
    $objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1',$tituloReporteDemanda)
	    ->setCellValue('A3',  $titulosColumnas[0])
        ->setCellValue('B3',  $titulosColumnas[1])
	    ->setCellValue('C3',  $titulosColumnas[2])
		->setCellValue('D3',  $titulosColumnas[3])
		->setCellValue('E3',  $titulosColumnas[4])
		->setCellValue('F3',  $titulosColumnas[5])
		->setCellValue('G3',  $titulosColumnas[6])
		->setCellValue('H3',  $titulosColumnas[7])
		->setCellValue('I3',  $titulosColumnas[7]);

	$i=4;
	if(count($response["aaData"])>0){
		foreach ($response["aaData"] as $key => $value) {

			

			$objPHPExcel->setActiveSheetIndex(0)
	    		    ->setCellValue('A'.$i,  utf8_encode($value["nombre"]))
		            ->setCellValue('B'.$i,  utf8_encode($value["telefono"]))
		            ->setCellValue('C'.$i,  utf8_encode($value["celular"]))
	        		->setCellValue('D'.$i,  utf8_encode($value["correo"]))
	    		    ->setCellValue('E'.$i,  utf8_encode($value["descripcion"]))
	        		->setCellValue('F'.$i,  utf8_encode($value["estado"]))
		            ->setCellValue('G'.$i,  utf8_encode($value["empresa"]))
		            ->setCellValue('H'.$i,  utf8_encode($value["observacion"]))
		            ->setCellValue('I'.$i,  utf8_encode($value["asesor"]));

		            if($i%2 == 0)
				    {
					$objPHPExcel->getActiveSheet()
								->getStyle("A$i:I$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFE0F2FF');

				    }
	        		$i++;

		}
	}

	

	$estiloTituloReporte = array(
	'font' => array(
		'name'      => 'Verdana',
	    'bold'      => true,
	    'italic'    => false,
	    'strike'    => false,
	   	'size' =>10,
	    	'color'     => array(
	        	'rgb' => 'FFFFFF'
	       	)
	),
	'fill' => array(
		'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
		'color'	=> array('argb' => 'FF220835')
	),
	'borders' => array(
	   	'allborders' => array(
	    	'style' => PHPExcel_Style_Border::BORDER_NONE                    
	   	)
	), 
	'alignment' =>  array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'rotation'   => 0,
			'wrap'          => TRUE
	)
	);

	$estiloTituloColumnas = array(
	'font' => array(
	    'name'      => 'Arial',
	    'bold'      => true, 
	    'size' 		=>7,                         
	    'color'     => array(
	        'rgb' => '000000'
	    )
	),
	'fill' 	=> array(
		'type'		=> PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
		'rotation'   => 90,
		'startcolor' => array(
			'rgb' => 'c47cf2'
		),
		'endcolor'   => array(
			'argb' => 'FF431a5d'
		)
	),
	'borders' => array(
		'top'     => array(
	        'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	        'color' => array(
	            'rgb' => '143860'
	        )
	    ),
	    'bottom'     => array(
	        'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	        'color' => array(
	            'rgb' => '143860'
	        )
	    )
	),
	'alignment' =>  array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'          => TRUE
	));

	$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($estiloTituloReporte);
	$objPHPExcel->getActiveSheet()->getStyle('A3:I3')->applyFromArray($estiloTituloColumnas);	
	
	for($i = 'A'; $i <= 'I'; $i++){
		$objPHPExcel->setActiveSheetIndex(0)			
			->getColumnDimension($i)->setAutoSize(TRUE);
	}

	
	// Se asigna el nombre a la hoja
	$objPHPExcel->getActiveSheet()->setTitle('Clientes');

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

 	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment;filename='filtro".$_SESSION["IdInmmo"].date("Y-m-d h:i:s").".xls'");
	header("Cache-Control: max-age=0");

	$objWriter->save('php://output');


}