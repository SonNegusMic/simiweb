<?php

// exit();
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
include "../funciones/fncfields2.php";
include "../funciones/fncfields3.php";
include "../funciones/cge00001.php";
include "../funciones/myfncs.php";
include "../funciones/conni.php";

include "../controllers/class.webhook.php";
@include "../funciones/connPDO.php";

$connPDO = new Conexion();

$w_conexion = new MySQL();

$wh        = new Webhook($connPDO);
$inmuebles = $wh->getInmuebles();

if (isset($_GET['hub_verify_token'])) {
    if ($_GET['hub_verify_token'] === 'desarrollo_simi') {
        echo $_GET['hub_challenge'];
        return;
    } else {
        echo 'Invalid Verify Token';
        return;
    }
}

$input = json_decode(file_get_contents('php://input'), true);

// $wh->welcomeMessage();
if (isset($input['entry'][0]['messaging'][0]['sender']['id'])) {

    $sender = $input['entry'][0]['messaging'][0]['sender']['id']; //sender facebook id

    $issetText    = isset($input['entry'][0]['messaging'][0]['message']);
    $issetPayload = isset($input['entry'][0]['messaging'][0]['postback']);

    if (isset($input['entry'][0]['messaging'][0]['message'])) {
        $message = $input['entry'][0]['messaging'][0]['message']['text'];
    } elseif (isset($input['entry'][0]['messaging'][0]['postback'])) {
        $message = $input['entry'][0]['messaging'][0]['postback']['payload'];
    }

    $existeRegistro = $wh->existeRegistro($sender);

    if ($existeRegistro <= '0') {
        $isertaPreguntaVacia = $wh->insertaPreguntaVacia($sender, 1);

        if ($isertaPreguntaVacia == 1) {
            $wh->primaryMessage($sender);
        } else {
            $wh->sendMessageText($sender, 'Ocurrio un error al insertar la primera pregunta Vacia, por favor comuniquese con el Web Master');
            $wh->primaryMessage($sender);
        }

    } else {
        $verificarFecha = $wh->verificarFecha($sender);
        if ($verificarFecha == 1) {
            $Preguntas = $wh->getPreguntas();
            foreach ($Preguntas as $key => $value) {
                $verificaRegistro = $wh->verificarPregunta($sender, $value['id']);
                $getMetodoQns     = getCampo('questions_facebook', ' where id = ' . $value['id'], 'metodo_qsn');
                if ($verificaRegistro > 0) {
                    $respuestaPregunta = getCampo('contactos_facebook', ' where tipo_fb = ' . $value['id'] . ' AND estado = 1', 'respuesta');
                    if (empty($respuestaPregunta) || $respuestaPregunta == null) {
                        $getRespuestaQns = getCampo('questions_facebook', ' where id = ' . $value['id'], 'respuesta_qns');
                        $comapos         = strpos($getRespuestaQns, ',');
                        $tablapos        = strpos($getRespuestaQns, 'tabla');
                        $parametrospos   = strpos($getRespuestaQns, 'parametros');
                        $numeropos       = strpos($getRespuestaQns, 'numero');
                        $likepos       = strpos($getRespuestaQns, 'like');
                        if ($comapos !== false) {
                            $arrayRespuestas = explode(',', $getRespuestaQns);
                            if (in_array($message, $arrayRespuestas)) {
                                $actualizarPregunta = $wh->updateRespuestaPregunta($sender, $value['id'], $message);
                                if ($actualizarPregunta == 1) {
                                    $wh->sendMessageText($sender, 'Se actualizo la pregunta con id = ' . $value['id']);
                                } else {
                                    $wh->sendMessageText($sender, 'Ocurrio un error al actualizar la pregunta = ' . $value['id']);
                                }
                            } else {
                                $wh->sendMessageText($sender, 'Parece que la respuesta no es valida, Intenta nuevamente.');
                                $wh->$getMetodoQns($sender);
                                break;
                            }

                        } elseif ($tablapos !== false) {
                            $getidtableQns   = getCampo('questions_facebook', ' where id = ' . $value['id'], 'id_table');
                            $getdesctableQns = getCampo('questions_facebook', ' where id = ' . $value['id'], 'desc_table');
                            // $wh->sendMessageText($sender,'tablapos');
                            if ($parametrospos === false) {

                                // $wh->sendMessageText($sender,'notieneparametros');
                                $tabla           = explode('_', $getRespuestaQns);
                                $respuestasTabla = $wh->getRespuestasTabla($tabla[1], $getidtableQns);

                                if (in_array($message, $respuestasTabla)) {
                                    $wh->sendMessageText($sender, 'entro en el inarray');
                                    $actualizarPregunta = $wh->updateRespuestaPregunta($sender, $value['id'], $message);
                                    if ($actualizarPregunta == 1) {
                                        $wh->sendMessageText($sender, 'Se actualizo la pregunta con id = ' . $value['id']);
                                    } else {
                                        $wh->sendMessageText($sender, 'Ocurrio un error al actualizar la pregunta = ' . $value['id']);
                                    }
                                } elseif (strpos($message, 'pag') !== false) {
                                    // $wh->sendMessageText($sender,'entroalpospag');
                                    $pag = explode('-', $message);
                                    // $wh->sendMessageText($sender,$getMetodoQns);
                                    $wh->$getMetodoQns($sender, $pag[1]);
                                    // $wh->sendMessageText($sender,json_encode($prueba));
                                    break;
                                } else {
                                    $wh->sendMessageText($sender, 'Parece que la respuesta no es valida, Intenta nuevamente.');
                                    $wh->$getMetodoQns($sender);
                                    break;
                                }
                                // $wh->sendMessageText($sender,$message);
                                // break;

                            }
                        } elseif($numeropos !== FALSE){

                        }
                    } else {
                        // $wh->sendMessageText($sender,'el mensaje no esta vacio ' . $respuestaPregunta . ' id = ' . $value['id']);
                    }
                    // break;

                } else {
                    $isertaPreguntaVacia = $wh->insertaPreguntaVacia($sender, $value['id']);
                    if ($isertaPreguntaVacia == 1) {
                        $wh->$getMetodoQns($sender);
                    } else {
                        $wh->sendMessageText($sender, 'Ocurrio un error al insertar la primera pregunta Vacia, por favor comuniquese con el Web Master');
                        $wh->$getMetodoQns($sender);
                    }
                    break;
                }
            }
        }
    }
}
