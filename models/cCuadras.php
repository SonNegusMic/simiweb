<?php

session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
include "../funciones/cge00001.php";
include "../funciones/myfncs.php";
include "../funciones/conni.php";
require_once '../mcomercialweb/nusoap/lib/nusoap.php';

///conexion pdo
include "../controllers/class.cCuadras.php";
//include("../mwc/class.recibos.php");

// include("../controllers/class_users.php");

$w_conexion = new MySQL();
$cCuadras   = new cCuadras($conn);
$soapClient = new nusoap_client("http://webservice.ciencuadras.com/?wsdl");
$cliente    = new nusoap_client('http://webservice.ciencuadras.com/?wsdl');
//$dReciboss        = new Inmuebles($connsr);

$data    = $_GET['data'];
$usr_act = $_SESSION['Id_Usuarios'];
//$dRecibos= new Recibos($connsr);
$codinm = $_GET['inm'];
$debug  = ($_GET['mostrar']) ? 1 : 0;

list($inmobi,$inmue)=explode("-",$codinm);
if ($inmobi != 1) {

    if ($data == 1) {
        // echo "hola1334".date('s'); exit();
        $d = $cCuadras->infoInmueble($codinm, $debug);
        if ($debug) {
            echo "<pre>";
            print_r($d);
            echo "</pre>";die;
        }

        if (!$d['error']) {
            //return $d;
            // echo "<pre>";
            // print_r($d);
            // echo "</pre>";
            // echo "hola";
            try {
                //$cadena=print_r($ap_param);
                // list($inmob,$a)=explode("-",$inm_inm);
                // $sql_log=str_replace("'","",$cadena);
                // //inserta_datos_call($inm_inm);
                // $conse=consecutivo('conse_bit','cambios_inmueble');
                //  cambios_inmueble($conse,$inm_inm,$cod_user,$fechaserv,$hor,$inmob,$ip,$sql_log,'4');
                //$info = $soapClient->__call("crearInmueble", $ap_param);
                $info = $soapClient->call("crearInmueble", $d);


                // json_decode(print_r($info));
                if ($info['mensaje'] > 0) {
                    // echo "Inmueble Publicado con el Codigo ".$info['mensaje'];
                    $html = "<!DOCTYPE>";
                    //html
                    $html .= "<html>";
                    //head
                    $html .= "<head>";
                    //librarys css
                    $html .= '<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">';
                    //END head
                    $html .= "</head>";
                    //body
                    $html .= "<body>";
                    $html .= '<div class="alert alert-success col-sm-4" role="alert"><i class="glyphicon glyphicon-ok"></i> Inmueble Publicado con el Codigo ' . $info['mensaje'] . '</div>';
                    //Scripts Librarys
                    $html .= '<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="crossorigin="anonymous"></script>';
                    $html .= '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>';
                    //END body
                    $html .= "</body>";
                    //END html
                    $html .= "</html>";
                    echo $html;
                    $sql = "update inmuebles
                      set RCcuadras='" . $info['mensaje'] . "'
                      where  idInm='" . $codinm . "'";
                    $res = $w_conexion->RecordSet($sql);
                }

                //           echo 'Request : <br/><xmp>',
                // // $soapClient->__getLastRequest(),
                //  '</xmp><br/><br/> Error Message : <br/>';

                // $fault->getMessage();
                //exit();
            } catch (SoapFault $fault) {
                $error = 1;
                print("Hubo un problema inesperado al Publicar: " . $fault->faultcode . "-" . $fault->faultstring . ". ");
            }
        } else {
            $html = "<!DOCTYPE>";
            //html
            $html .= "<html>";
            //head
            $html .= "<head>";
            //librarys css
            $html .= '<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">';
            //END head
            $html .= "</head>";
            //body
            $html .= "<body>";
            $html .= '<h3>Debe solucionar los siguientes aspectos para poder realizar la publicacion:</h3>';
            $count = 1;
            foreach ($d['msgerror'] as $value) {
                $html .= '<div class="alert alert-danger col-sm-4" role="alert"><i class="glyphicon glyphicon-remove"></i> ' . $value . '</div>';
                $count++;
            }
            //Scripts Librarys
            $html .= '<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="crossorigin="anonymous"></script>';
            $html .= '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>';
            //END body
            $html .= "</body>";
            //END html
            $html .= "</html>";
            echo $html;
        }
        //print_r($info);
        // echo $info['mensaje'];
        //print_r($_POST);
    }
    if ($data == 2) {
        $d = $cCuadras->infoInmueble($codinm, $debug);
        if (!$d['error']) {
            try {
                $d['datos_inmueble_entrada']['activo'] = 2;
                $info                                  = $soapClient->call("crearInmueble", $d);
                if ($info['mensaje'] > 0) {
                    // echo "Inmueble Publicado con el Codigo ".$info['mensaje'];
                    $html = "<!DOCTYPE>";
                    //html
                    $html .= "<html>";
                    //head
                    $html .= "<head>";
                    //librarys css
                    $html .= '<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">';
                    //END head
                    $html .= "</head>";
                    //body
                    $html .= "<body>";
                    $html .= '<div class="alert alert-success col-sm-4" role="alert"><i class="glyphicon glyphicon-ok"></i> Inmueble Despublicado con el Codigo ' . $info['mensaje'] . '</div>';
                    //Scripts Librarys
                    $html .= '<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="crossorigin="anonymous"></script>';
                    $html .= '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>';
                    //END body
                    $html .= "</body>";
                    //END html
                    $html .= "</html>";
                    echo $html;
                    $sql = "update inmuebles
                      set RCcuadras='0'
                      where  idInm='" . $codinm . "'
                      and RCcuadras='" . $info['mensaje'] . "'";
                    $res = $w_conexion->RecordSet($sql);
                }
            } catch (SoapFault $fault) {
                $error = 1;
                print("Hubo un problema inesperado al Publicar: " . $fault->faultcode . "-" . $fault->faultstring . ". ");
            }}
    }
} else {
    echo "<h1>La inmobiliaria simi no tiene permisos para sincronizar a este portal</h1>";
}
$w_conexion->CerrarConexion();
