<?php error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
session_start();
//require('../funciones/connisr.php');

////para visualización en interfase actual
$ctr= $_GET['ctr'];
$condI="";
if($ctr==1)
{
	$condI="style='visibility:hidden';";
}
$mruta="../";
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
<?php header('Content-Type: text/html; charset=utf-8');?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Danny Marin">
    <link rel="icon" type="image/png" href="../sj/img/favicon.ico">
    <link href="../sj/img/favicon.ico" type="image/x-icon" rel="shortcut icon" />

    <title>Simiweb</title>

    <!--Core CSS -->
    <link href="<?php echo $mruta;?>bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $mruta;?>css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo $mruta;?>font-awesome/css/font-awesome.css" rel="stylesheet" />

    
	<link href="<?php echo $mruta;?>../siminmueblenvo/css/chosen.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo $mruta;?>css/style.css" rel="stylesheet">
    <link href="<?php echo $mruta;?>css/style-responsive.css" rel="stylesheet" />

    <!-- Stylos select2 -->
    <link rel="stylesheet" href="<?php echo $mruta;?>js/select2/select2.css">
    <link rel="stylesheet" href="<?php echo $mruta;?>js/select2/select2-bootstrap.css">
    

    <!-- Stylos data piker -->
    <link rel="stylesheet" href="../../Datapiker-bootstrap/css/bootstrap-datetimepicker.min.css">

    <!-- Estilos  datatables-->
    <link href="../../docsCargados/js/datatables-responsive/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../docsCargados/js/datatables-responsive/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../docsCargados/js/datatables-responsive/css/responsive.bootstrap.min.css" rel="stylesheet">
    
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="data:application/octet-stream;base64,LyohIG1hdGNoTWVkaWEoKSBwb2x5ZmlsbCAtIFRlc3QgYSBDU1MgbWVkaWEgdHlwZS9xdWVyeSBpbiBKUy4gQXV0aG9ycyAmIGNvcHlyaWdodCAoYykgMjAxMjogU2NvdHQgSmVobCwgUGF1bCBJcmlzaCwgTmljaG9sYXMgWmFrYXMuIER1YWwgTUlUL0JTRCBsaWNlbnNlICovCi8qISBOT1RFOiBJZiB5b3UncmUgYWxyZWFkeSBpbmNsdWRpbmcgYSB3aW5kb3cubWF0Y2hNZWRpYSBwb2x5ZmlsbCB2aWEgTW9kZXJuaXpyIG9yIG90aGVyd2lzZSwgeW91IGRvbid0IG5lZWQgdGhpcyBwYXJ0ICovCndpbmRvdy5tYXRjaE1lZGlhPXdpbmRvdy5tYXRjaE1lZGlhfHxmdW5jdGlvbihhKXsidXNlIHN0cmljdCI7dmFyIGMsZD1hLmRvY3VtZW50RWxlbWVudCxlPWQuZmlyc3RFbGVtZW50Q2hpbGR8fGQuZmlyc3RDaGlsZCxmPWEuY3JlYXRlRWxlbWVudCgiYm9keSIpLGc9YS5jcmVhdGVFbGVtZW50KCJkaXYiKTtyZXR1cm4gZy5pZD0ibXEtdGVzdC0xIixnLnN0eWxlLmNzc1RleHQ9InBvc2l0aW9uOmFic29sdXRlO3RvcDotMTAwZW0iLGYuc3R5bGUuYmFja2dyb3VuZD0ibm9uZSIsZi5hcHBlbmRDaGlsZChnKSxmdW5jdGlvbihhKXtyZXR1cm4gZy5pbm5lckhUTUw9JyZzaHk7PHN0eWxlIG1lZGlhPSInK2ErJyI+ICNtcS10ZXN0LTEgeyB3aWR0aDogNDJweDsgfTwvc3R5bGU+JyxkLmluc2VydEJlZm9yZShmLGUpLGM9NDI9PT1nLm9mZnNldFdpZHRoLGQucmVtb3ZlQ2hpbGQoZikse21hdGNoZXM6YyxtZWRpYTphfX19KGRvY3VtZW50KTsKCi8qISBSZXNwb25kLmpzIHYxLjMuMDogbWluL21heC13aWR0aCBtZWRpYSBxdWVyeSBwb2x5ZmlsbC4gKGMpIFNjb3R0IEplaGwuIE1JVC9HUEx2MiBMaWMuIGoubXAvcmVzcG9uZGpzICAqLwooZnVuY3Rpb24oYSl7InVzZSBzdHJpY3QiO2Z1bmN0aW9uIHgoKXt1KCEwKX12YXIgYj17fTtpZihhLnJlc3BvbmQ9YixiLnVwZGF0ZT1mdW5jdGlvbigpe30sYi5tZWRpYVF1ZXJpZXNTdXBwb3J0ZWQ9YS5tYXRjaE1lZGlhJiZhLm1hdGNoTWVkaWEoIm9ubHkgYWxsIikubWF0Y2hlcywhYi5tZWRpYVF1ZXJpZXNTdXBwb3J0ZWQpe3ZhciBxLHIsdCxjPWEuZG9jdW1lbnQsZD1jLmRvY3VtZW50RWxlbWVudCxlPVtdLGY9W10sZz1bXSxoPXt9LGk9MzAsaj1jLmdldEVsZW1lbnRzQnlUYWdOYW1lKCJoZWFkIilbMF18fGQsaz1jLmdldEVsZW1lbnRzQnlUYWdOYW1lKCJiYXNlIilbMF0sbD1qLmdldEVsZW1lbnRzQnlUYWdOYW1lKCJsaW5rIiksbT1bXSxuPWZ1bmN0aW9uKCl7Zm9yKHZhciBiPTA7bC5sZW5ndGg+YjtiKyspe3ZhciBjPWxbYl0sZD1jLmhyZWYsZT1jLm1lZGlhLGY9Yy5yZWwmJiJzdHlsZXNoZWV0Ij09PWMucmVsLnRvTG93ZXJDYXNlKCk7ZCYmZiYmIWhbZF0mJihjLnN0eWxlU2hlZXQmJmMuc3R5bGVTaGVldC5yYXdDc3NUZXh0PyhwKGMuc3R5bGVTaGVldC5yYXdDc3NUZXh0LGQsZSksaFtkXT0hMCk6KCEvXihbYS16QS1aOl0qXC9cLykvLnRlc3QoZCkmJiFrfHxkLnJlcGxhY2UoUmVnRXhwLiQxLCIiKS5zcGxpdCgiLyIpWzBdPT09YS5sb2NhdGlvbi5ob3N0KSYmbS5wdXNoKHtocmVmOmQsbWVkaWE6ZX0pKX1vKCl9LG89ZnVuY3Rpb24oKXtpZihtLmxlbmd0aCl7dmFyIGI9bS5zaGlmdCgpO3YoYi5ocmVmLGZ1bmN0aW9uKGMpe3AoYyxiLmhyZWYsYi5tZWRpYSksaFtiLmhyZWZdPSEwLGEuc2V0VGltZW91dChmdW5jdGlvbigpe28oKX0sMCl9KX19LHA9ZnVuY3Rpb24oYSxiLGMpe3ZhciBkPWEubWF0Y2goL0BtZWRpYVteXHtdK1x7KFteXHtcfV0qXHtbXlx9XHtdKlx9KSsvZ2kpLGc9ZCYmZC5sZW5ndGh8fDA7Yj1iLnN1YnN0cmluZygwLGIubGFzdEluZGV4T2YoIi8iKSk7dmFyIGg9ZnVuY3Rpb24oYSl7cmV0dXJuIGEucmVwbGFjZSgvKHVybFwoKVsnIl0/KFteXC9cKSciXVteOlwpJyJdKylbJyJdPyhcKSkvZywiJDEiK2IrIiQyJDMiKX0saT0hZyYmYztiLmxlbmd0aCYmKGIrPSIvIiksaSYmKGc9MSk7Zm9yKHZhciBqPTA7Zz5qO2orKyl7dmFyIGssbCxtLG47aT8oaz1jLGYucHVzaChoKGEpKSk6KGs9ZFtqXS5tYXRjaCgvQG1lZGlhICooW15ce10rKVx7KFtcU1xzXSs/KSQvKSYmUmVnRXhwLiQxLGYucHVzaChSZWdFeHAuJDImJmgoUmVnRXhwLiQyKSkpLG09ay5zcGxpdCgiLCIpLG49bS5sZW5ndGg7Zm9yKHZhciBvPTA7bj5vO28rKylsPW1bb10sZS5wdXNoKHttZWRpYTpsLnNwbGl0KCIoIilbMF0ubWF0Y2goLyhvbmx5XHMrKT8oW2EtekEtWl0rKVxzPy8pJiZSZWdFeHAuJDJ8fCJhbGwiLHJ1bGVzOmYubGVuZ3RoLTEsaGFzcXVlcnk6bC5pbmRleE9mKCIoIik+LTEsbWludzpsLm1hdGNoKC9cKFxzKm1pblwtd2lkdGhccyo6XHMqKFxzKlswLTlcLl0rKShweHxlbSlccypcKS8pJiZwYXJzZUZsb2F0KFJlZ0V4cC4kMSkrKFJlZ0V4cC4kMnx8IiIpLG1heHc6bC5tYXRjaCgvXChccyptYXhcLXdpZHRoXHMqOlxzKihccypbMC05XC5dKykocHh8ZW0pXHMqXCkvKSYmcGFyc2VGbG9hdChSZWdFeHAuJDEpKyhSZWdFeHAuJDJ8fCIiKX0pfXUoKX0scz1mdW5jdGlvbigpe3ZhciBhLGI9Yy5jcmVhdGVFbGVtZW50KCJkaXYiKSxlPWMuYm9keSxmPSExO3JldHVybiBiLnN0eWxlLmNzc1RleHQ9InBvc2l0aW9uOmFic29sdXRlO2ZvbnQtc2l6ZToxZW07d2lkdGg6MWVtIixlfHwoZT1mPWMuY3JlYXRlRWxlbWVudCgiYm9keSIpLGUuc3R5bGUuYmFja2dyb3VuZD0ibm9uZSIpLGUuYXBwZW5kQ2hpbGQoYiksZC5pbnNlcnRCZWZvcmUoZSxkLmZpcnN0Q2hpbGQpLGE9Yi5vZmZzZXRXaWR0aCxmP2QucmVtb3ZlQ2hpbGQoZSk6ZS5yZW1vdmVDaGlsZChiKSxhPXQ9cGFyc2VGbG9hdChhKX0sdT1mdW5jdGlvbihiKXt2YXIgaD0iY2xpZW50V2lkdGgiLGs9ZFtoXSxtPSJDU1MxQ29tcGF0Ij09PWMuY29tcGF0TW9kZSYma3x8Yy5ib2R5W2hdfHxrLG49e30sbz1sW2wubGVuZ3RoLTFdLHA9KG5ldyBEYXRlKS5nZXRUaW1lKCk7aWYoYiYmcSYmaT5wLXEpcmV0dXJuIGEuY2xlYXJUaW1lb3V0KHIpLHI9YS5zZXRUaW1lb3V0KHUsaSksdm9pZCAwO3E9cDtmb3IodmFyIHYgaW4gZSlpZihlLmhhc093blByb3BlcnR5KHYpKXt2YXIgdz1lW3ZdLHg9dy5taW53LHk9dy5tYXh3LHo9bnVsbD09PXgsQT1udWxsPT09eSxCPSJlbSI7eCYmKHg9cGFyc2VGbG9hdCh4KSooeC5pbmRleE9mKEIpPi0xP3R8fHMoKToxKSkseSYmKHk9cGFyc2VGbG9hdCh5KSooeS5pbmRleE9mKEIpPi0xP3R8fHMoKToxKSksdy5oYXNxdWVyeSYmKHomJkF8fCEoenx8bT49eCl8fCEoQXx8eT49bSkpfHwoblt3Lm1lZGlhXXx8KG5bdy5tZWRpYV09W10pLG5bdy5tZWRpYV0ucHVzaChmW3cucnVsZXNdKSl9Zm9yKHZhciBDIGluIGcpZy5oYXNPd25Qcm9wZXJ0eShDKSYmZ1tDXSYmZ1tDXS5wYXJlbnROb2RlPT09aiYmai5yZW1vdmVDaGlsZChnW0NdKTtmb3IodmFyIEQgaW4gbilpZihuLmhhc093blByb3BlcnR5KEQpKXt2YXIgRT1jLmNyZWF0ZUVsZW1lbnQoInN0eWxlIiksRj1uW0RdLmpvaW4oIlxuIik7RS50eXBlPSJ0ZXh0L2NzcyIsRS5tZWRpYT1ELGouaW5zZXJ0QmVmb3JlKEUsby5uZXh0U2libGluZyksRS5zdHlsZVNoZWV0P0Uuc3R5bGVTaGVldC5jc3NUZXh0PUY6RS5hcHBlbmRDaGlsZChjLmNyZWF0ZVRleHROb2RlKEYpKSxnLnB1c2goRSl9fSx2PWZ1bmN0aW9uKGEsYil7dmFyIGM9dygpO2MmJihjLm9wZW4oIkdFVCIsYSwhMCksYy5vbnJlYWR5c3RhdGVjaGFuZ2U9ZnVuY3Rpb24oKXs0IT09Yy5yZWFkeVN0YXRlfHwyMDAhPT1jLnN0YXR1cyYmMzA0IT09Yy5zdGF0dXN8fGIoYy5yZXNwb25zZVRleHQpfSw0IT09Yy5yZWFkeVN0YXRlJiZjLnNlbmQobnVsbCkpfSx3PWZ1bmN0aW9uKCl7dmFyIGI9ITE7dHJ5e2I9bmV3IGEuWE1MSHR0cFJlcXVlc3R9Y2F0Y2goYyl7Yj1uZXcgYS5BY3RpdmVYT2JqZWN0KCJNaWNyb3NvZnQuWE1MSFRUUCIpfXJldHVybiBmdW5jdGlvbigpe3JldHVybiBifX0oKTtuKCksYi51cGRhdGU9bixhLmFkZEV2ZW50TGlzdGVuZXI/YS5hZGRFdmVudExpc3RlbmVyKCJyZXNpemUiLHgsITEpOmEuYXR0YWNoRXZlbnQmJmEuYXR0YWNoRXZlbnQoIm9ucmVzaXplIix4KX19KSh0aGlzKTsK"></script>
    <![endif]-->


    <style type="   ">  
        .radio
        {
            width: 10%;
        }
            form{
            color: #474747;
        }
       .select2-match{
            text-decoration: underline;
        }
    </style>
</head>

<body>
<header class="header fixed-top clearfix" <?php echo $condI;?>>
<?php include_once("../commons/header.php");
require($mruta."class.recibos.php");
include($mruta."../funciones/fncfields2.php");
include($mruta."../funciones/fncfields3.php");

$dRecibos= new Recibos($connsr);
$formatoFicha=getCampo('clientessimi',"where IdInmobiliaria=".$_SESSION['IdInmmo'],"fichatec");
$inmoActual=$_SESSION['IdInmmo'];	
$usuActual=$_SESSION['Id_Usuarios'];
?>
</header>
<!--header end-->
<!--sidebar start-->
<aside <?php echo $condI;?>>
    <?php include_once("../commons/menu.php");?>
</aside>
<section id="container" >
 <?php
if($ctr!=1)
{
    ?>
    <section id="main-content">
    <section class="wrapper">
    <?php
}?>
<!--header start-->


        <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Listado Precaptaciones
                    </header>
                    <div class="panel-body">
                    <form action="" method="POST"   class="Negocios" >
                        <input type="hidden" name="idInmmo" value="<?php echo $_SESSION['IdInmmo']  ?>">
                    <div>
                    <div class="row">
                     <div class="col-sm-3 col-md-3 ">
                        <label></label>
                        <div>
                                <span class=""></span>
                                <div class='input-group date' id="">
                                        <span class="input-group-addon">
                                    <span class="fa fa-bars"></span>
                                     </span>
                                     <div>
                                <?php 
                                    getSelectParam(40, 'desc_param','','ffecha','','','class="form-control chosen-select input-sm " id="effecha"',1,'','Filtro por fecha de');
                                 ?>
                                </div>
                                    
                                </div> 
                            </div>
                            <label for=""></label>
                            <div>
                             <div class='input-group date' id="fecha_inis">
                              <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                <input type='text' name="ffecha_ini"  class="form-control input-sm form-control-inline input-medium filthypillow" id="fecha_inis"/>
                                <span class="input-group-addon"> Desde</span>
                                
                             </div> 
                             </div> 
                             <label></label>               
                            <div>
                                 <div class='input-group date' id="fecha_fins">
                              <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                <input type='text' name="ffecha_fin" class="form-control input-sm form-control-inline input-medium filthypillow" id="fecha_fins"/>
                                <span class="input-group-addon fiecha_fins">Hasta</span>
                                
                             </div>  
                            </div>
                            
                          
                    </div>
                    <div class="col-sm-3 col-md-3">
                    <label> </label>
                          <div>
                                <span class=""></span>
                                <div class='input-group date' id="">
                                <span class="input-group-addon">
                                    <span class="fa fa-user"></span>
                                     </span>
                                     <select name="asesor" id="asesor" class="form-control input-sm chosen-select">
                                         <option value=''>Asesor</option>
                                     </select>
                                    
                                </div> 
                            </div>
                        <label></label>
                        <div>
                            <div class='input-group date'>
                            <span class="input-group-addon">
                                    <span class="fa fa-male"></span>
                                     </span>
                            <?php getSelectP('gestioncomer','IdGestion', 'CONCAT(UPPER(LEFT(NombresGestion, 1)), LOWER(SUBSTRING(NombresGestion, 2)))','','where IdGestion>0','tip_ope','','','class="form-control input-sm chosen-select" id="gestion"','','','Gesti&oacuten');?>
                            </div>
                        </div>
                        <label></label>
                         <div>
                            <div class='input-group date'>
                            <span class="input-group-addon">
                                    <span class="fa fa-dot-circle-o"></span>
                                     </span>
                                     <?php  getSelectParam(16, 'desc_param','','est_cli','','','class="form-control chosen-select input-sm " id="est_cli"',1,'','Estado');
                                     ?>
                                     
                            </div>
                        </div>
                        </div>
                       <div class="col-sm-3 col-md-3">
                       <label></label>
                         <div>
                            <div class='input-group date'>
                            <span class="input-group-addon">
                                    <span class="fa fa fa-suitcase"></span>
                                     </span>
                            <?php 
                                getSelect('destinacion','Iddestinacion', 'CONCAT(UPPER(LEFT(Nombre, 1)), LOWER(SUBSTRING(Nombre, 2)))',"","",'dest',"",'','class="form-control chosen-select input-sm " id="dest"','','','','','Destinación');
                            ?>
                            </div>
                        </div>
                        <label></label>
                        <div>
                         <div class='input-group date'>
                            <span class="input-group-addon">
                                    <span class="fa fa-users"></span>
                                     </span>
                            <?php  getSelectP('clientes_inmobiliaria','cedula', 'CONCAT(UPPER(LEFT(nombre, 1)), LOWER(SUBSTRING(nombre, 2)))','','where inmobiliaria='.$_SESSION['IdInmmo'],'interesado',"$CedulaIInt",'',' id="interesado" class="form-control input-sm chosen-select"','','','Interesado');?>
                        </div>
                        </div>
                        <label></label>
                        <div>
                         <div class='input-group date'>
                            <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-dashboard"></span>
                                     </span>
                            <input type="text" name="codDemanda" id="codDemanda" class="form-control input-sm" placeholder="Cod Demanda">
                        </div>
                        </div>
                      </div>  
                       <div class="col-sm-3 col-md-3">
                       <label></label>
                         <div>
                             <div class='input-group date'>
                                <span class="input-group-addon">
                                    <span class="fa fa-home"></span>
                                     </span>
                             <?php getSelectP('tipoinmuebles','idTipoInmueble', 'CONCAT(UPPER(LEFT(Descripcion, 1)), LOWER(SUBSTRING(Descripcion, 2)))',"","",'tip_inm',"",'','class="form-control input-sm chosen-select" id="tip_inm" ','','','Tipo de Inmueble'); ?>
                             </div>
                         </div>
                        <label for=""></label>
                        <div>
                        <button class="btn btn-primary btn-sm data_inm btn-block searchcruce"><span class="fa fa-search"></span> Mostrar Resultados </button>
                      </div>
                      <label for=""></label>
                        <div>
                        <button class="btn btn-info btn-sm data_inm btn-block delete"><span class="fa  fa-eraser"></span> Borrar Resultados </button>
                      </div>
                      </div> 
           
                     </form>
                    <label ></label>
                    <div class="row">
                    <div class="col-sm-12">
                       
                    </div>
                   
                    </div>
                    </div>
                    <label ></label>
                    <div id="contenidoAjax" align="center"></div>
                      
                    </div>
                   
                    <div id="cruce">
                    
                    <button class="btn btn-success reporte"><span class="glyphicon glyphicon-share-alt"></span> <a href="reportexls_precapta.php"></a>Exportar Excel</button>
                        <table id="crucenegocios" style="white-space:normal" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Aplicar</th>
                                    <th>F.Creaci&oacute;n</th>
                                    <th>F.Vencimiento</th>
                                    <th>Valor Inicial</th>
                                    <th>Valor Final</th>
                                    <th>Barrio</th>
                                    <th>Gestion</th>
                                    <th>Destinacion</th>
                                    <th>Tipo Inmueble</th>
                                    <th>Calle</th>
                                    <th>Carrera</th>
                                    <th>Cliente Interesado</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                   
                
            </div>
           </section>   
        </div> <!-- fin de la grilla col-lg-12 --> 
        
<?php
if($ctr!=1)
{
    ?>
     </section> <!-- wraper -->
     </section> <!-- FIn del mainconent -->
    <?php
}?>
       
</section> <!-- FIn del container -->
    <!--main content end-->
<!--right sidebar start-->
<?php include("../commons/menuLateral.php");?>
<!--right sidebar end-->
<?php
if($ctr!=1)
{
	?>
	
	<?php
}?>
<!-- Modal de negocios -->
<
<div class="modal fade" id="modal-id">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Seguimiento a Demanda 293</h4>
            </div>
            <div class="modal-body">
            
              <form action="" class="form-horizontal" id="seguimientore" role="form">
                            
                  <div class=" input-group">
                     <span class="input-group-addon "><i class="fa fa-search"></i></span>
                     <div>
                         <?php 
                              getSelectParam(20, 'desc_param','','tseguimiento','','','data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control chosen-select " id="tseguimiento"',1,'','Tipo Seguimineto');
                          ?>
                     </div>
                  </div>
                   <label></label>
                  <div class="form-pupi">
                     <div class='input-group ' >
                        <span class="input-group-addon">
                                <span class="glyphicon glyphicon-dashboard"></span>
                        </span>
                        <textarea name="" id="input" class="form-control" rows="3"  placeholder="Observación" data-parsley-error-message="Mensaje mayor de 10 caracteres" data-parsley-minlength="10"  data-parsley-required="true" ></textarea>
                     </div>
                  </div>
                  <label></label>
                   <div class='input-group ' id="">
                     <span class="input-group-addon">
                        <span class="fa fa-bars"></span>
                     </span>
                     <div>
                         <?php 
                               getSelectParam(10, 'desc_param','','resseg','','','data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control chosen-select input-sm id="resseg"',1,'','Resultado');
                          ?>
                     </div>
                  </div>
                  <label></label>
                    <div class='input-group date' id="">
                     <span class="input-group-addon">
                        <span class="fa fa-bars"></span>
                     </span>
                     <div>
                         <?php 
                               getSelectParam(18, 'desc_param','','agendar','','','data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control chosen-select input-sm id="agendar"',1,'','Crear Evento');
                          ?>
                     </div>
                  </div>
                   <label for=""></label>
                     <div>
                        <div class='input-group date' id="fecha_event">
                              <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                <input type='text' name="ffecha_ini"  class="form-control input-sm form-control-inline input-medium filthypillow" data-parsley-required="true" data-parsley-error-message="" id="fecha_event"/>
                                <span class="input-group-addon"> Fecha</span>
                           </div> 
                     </div> 
                  <label for=""></label>
                     <div>
                        <div class='input-group date' id="hora_event" data-parsley-error-message="error en la hora">
                              <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                <input type='text' name="hora_ini" data-parsley-required="true"  class="form-control input-sm form-control-inline input-medium filthypillow" id="hora_event"/>
                                <span class="input-group-addon">Hora</span>
                        </div> 
                     </div>
                    <label></label>
                    <div class='input-group date' id="">
                     <span class="input-group-addon">
                        <span class="fa fa-bars"></span>
                     </span>
                     <div>
                         <?php 
                               getSelectParam(3, 'desc_param','','agendar','','','data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control chosen-select input-sm" id="copyclient"',1,'','Copiar al Cliente');
                          ?>
                     </div>
                  </div> 
                  <label></label>
                     <div>
                         <div class='input-group date' data-parsley-error-message="Correo Invalido">
                            <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-dashboard"></span>
                            </span>
                            <input type="text" name="correoAlt" id="correoAlt"  data-parsley-required="true" class="form-control input-sm" placeholder="Copiar a Otro Email">
                        </div>
                     </div>
                     <label></label>
                    <div class='input-group date' id="">
                     <span class="input-group-addon">
                        <span class="fa fa-bars"></span>
                     </span>
                     <div>
                         <?php 
                               
                               getSelectParam(3, 'desc_param','','cerrard','','','data-parsley-errors-messages-disabled data-parsley-required="true" class="form-control chosen-select input-sm" id="cerrard"',1,'','Cerrar Demanda');
                          ?>
                     </div>
                  </div> 
                   <label></label>
                   
                     <div>
                           <button  class="btn btn-info guardsegui"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar seguimiento</button>
                     </div>
              </form>
             
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal de Editar demanda -->
<!-- Modal de camobios demanda -->
<!-- Modal de seguimiento -->


<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->
<script src="<?php echo $mruta;?>js/jquery.js"></script>
<script src="<?php echo $mruta;?>bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?php echo $mruta;?>js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo $mruta;?>js/jquery.scrollTo.min.js"></script>
<script src="<?php echo $mruta;?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="<?php echo $mruta;?>js/jquery.nicescroll.js"></script>

<!--common script init for all pages-->
<script src="<?php echo $mruta;?>js/scripts.js"></script>

<!-- Selec2 -->
<script type="text/javascript" language="javascript" src="<?php echo $mruta;?>/js/select2/select2.js"></script>


<!-- datapicker js -->
<script type="text/javascript" language="javascript" src="../../Datapiker-bootstrap/js/moment.js"></script>
<script type="text/javascript" language="javascript" src="../../Datapiker-bootstrap/js/bootstrap-datetimepicker.min.js"></script>

<!-- js datatbles responsive -->
<script type="text/javascript" language="javascript" src="../../docsCargados/js/datatables-responsive/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="../../docsCargados/js/datatables-responsive/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="../../docsCargados/js/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" language="javascript" src="../../docsCargados/js/datatables-responsive/js/responsive.bootstrap.min.js"></script>
<script src="../../validator/js/parsley.js"></script>
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#crucenegocios').DataTable({});
       
        function cruceNegocios()
        {
            $(".searchcruce").on("click",function(e){
                    var todaInfo=$(".Negocios").serialize();
                    alert(todaInfo);
                    $.ajax({
                        url: '../../models/negocios.php?data=5',
                        type: 'post',
                        // dataType: 'json',
                        data: todaInfo,
                        beforeSend:function()
                        {    
                        $('div#contenidoAjax').html('<p><img src="../images/gifs/loading.gif" width="40px" height="40px" /></p>');
                        },
                        success:function(datas)
                        {
                            $('div#contenidoAjax').html("");
                           
                            var parsedJson = $.parseJSON(datas);
                            dataTable1(parsedJson);
                        }

                    });
                    e.preventDefault();
            });
        }
        function dataTable1(data)
        {
            
            $('#crucenegocios').DataTable({
            // "order": [[ 0, "desc" ]],
            "processing": true,
            "sEcho":2,
            "iTotalRecords":"5683",
            "iDisplayLength":50,
            data:data,
            columns:[
            {'data':'idemanda',"sClass": "id_demanda"},
            {'data':'btnModal',"sClass": "resultadobtn"},
            {'data':'fechaD',"sClass": "fechaCrea"},
            {'data':'fechaLimite',"sClass": "fechaVen"},
            {'data':'valorInicial',"sClass": "valorInicial"},
            {'data':'valorFinal',"sClass":"valorFinal"},
            {'data':'barrio',"sClass": "barrio"},
            {'data':'gestion',"sClass": "gestion"},
            {'data':'destinacion',"sClass": "destinacion"},
            {'data':'tipoInmueble',"sClass": "tipoinmueble"},
            {'data':'Calle',"sClass":"calle"},
            {'data':'Carrera',"sClass":"carrera"},
            {'data':'interesado',"sClass": "interesado"},
            ],
            destroy: true,
           // "bPaginate": false,
            "bAutoWidth": false,
            "sPrevious": "Anterior",
            });
           
        }
        function dataPiker(dateNow )
        {
            
             $('#fecha_fins').datetimepicker({
                        format           : 'YYYY/MM/DD',
                        // defaultDate      :dateNow,
                        // maxDate          :moment(),
                         widgetPositioning: 
                        {
                        vertical         : 'bottom',
                        // horizontal: 'left'
                        },
                        useCurrent: false //Important! See issue #1075
                });

                $('#fecha_inis').datetimepicker({
                        format: 'YYYY/MM/DD',
                        });
                        $("#fecha_inis").on("dp.change", function (e) {
                        $('#fecha_fins').data("DateTimePicker").minDate(e.date);
                        });
                        $("#fecha_fins").on("dp.change", function (e) {
                        $('#fecha_inis').data("DateTimePicker").maxDate(e.date);
                });
                 $('#fecha_event').datetimepicker({
                        format           : 'YYYY/MM/DD',
                        // defaultDate      :dateNow,
                        minDate          :moment(),
                });
               
               $('#hora_event').datetimepicker({
                        format           : 'HH:mm',
                        // defaultDate      :dateNow,
                        minDate          :moment(),
                        enabledHours: [7,8,9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
                });
                  
        
        }
        function selectFilter()
        {
            $(document).ready(function() {
             $(".chosen-select").select2();
        });
        }
        function obtenerPromotores(idInmmo)
        {
            $.ajax({
                url: '../../models/negocios.php?data=4',
                type: 'post',
                dataType: 'json',
                data: {idInmmo: idInmmo},
                success:function(data)
                {
                    console.log(data);
                    var option="";
                    option += "<option value=''>Asesor</option>";
                    var i=0;
                    for(i in data)
                    {
                        
                        option +="<option value='"+data[i].id+"'>"+data[i].nombres+" "+data[i].apellidos+"</option>";
                    }
                    $("#asesor").html(option);
                }
            });
        }
         
         function dataInfo(data)
         {
            $.parseJSON('json');
            console.log(data);
            var i;
            var html="";
            
            alert(html);
         }

         function validarFromulario(miForm,submit,nomFunction)
         {
            
            
            
            
            $.listen('parsley:field:error', function(ParsleyField) {
             ParsleyField.$element.closest('.input-group').removeClass('has-success');
             ParsleyField.$element.closest('.input-group').addClass('has-error');
            });
            $.listen('parsley:field:success', function(ParsleyField) {
            ParsleyField.$element.closest('.input-group').removeClass('has-error');
            // ParsleyField.$element.closest('.input-group').addClass('has-success');
            ParsleyField.$element.closest('.input-group').addClass('has-success');
            });
            var result;
           
            miForm.on('click',submit,function( event ) {
                $(this).parsley().validate();
                if ($(this).parsley().isValid()) {
                    var data = miForm.serialize();
                     $.ajax({
                         url: 'prueba.php',
                         type: 'post',
                         data: data,
                         
                         success:function(datas)
                         {
                            var data = $.parseJSON(datas);
                            console.log(data);
                            
                            eval( nomFunction + '('+data+')');
                         }
                     });
                }
                event.preventDefault();
            });
           
             
            
        }
        
       var dateNow = new Date();
       var idInmmo="<?php echo  $_SESSION['IdInmmo']; ?>";
       obtenerPromotores(idInmmo);
       selectFilter();
       dataPiker(dateNow);
       cruceNegocios(idInmmo);
       
        
           var formsegui=validarFromulario($("#seguimientore"),$('.guardsegui'),"dataInfo");

           if(formsegui)
           {
            
            
           }
       // var formw = $('#seguimientore').parsley({
       //  trigger:      'change',
       //  successClass: "has-success",
       //  errorClass: "has-error",
       //  classHandler: function (el) {
       //      return el.$element.closest('.input-group'); //working 
       //  },
          
       //  });
      
    });
</script>
</body>
</html>

