<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
header('Content-Type: text/html; charset=utf-8');
include("../funciones/fncfields2.php");
include("../funciones/fncfields3.php");
include("../funciones/cge00001.php");
include("../funciones/myfncs.php");
include("../funciones/conni.php");
include("../funciones/connPDO.php");
include("../controllers/class.citas.php");
require('../mwc/inmuebles/PHPExcel/PHPExcel.php');
$w_conexion	 	= new MySQL();
$pdo= new Conexion();
$citas=new Citas($w_conexion,$pdo);
$objPHPExcel = new PHPExcel;

if( isset($_GET["report"]) ){
	$response=$citas->getCitas($_GET);

	$objPHPExcel->getProperties()->setCreator("Codedrinks") //Autor
			 ->setLastModifiedBy("Codedrinks") 
			 //Ultimo usuario que lo modificó
			 ->setTitle("Reporte Excel Inmuebles de la Inmobiliaria")
			 ->setSubject("Reporte Excel Inmuebles de la Inmobiliaria")
			 ->setDescription("Reporte de alumnos")
			 ->setKeywords("reporte Inmuebles de la Inmobiliaria")
			 ->setCategory("Reporte excel");
	$tituloReporte = "REPORTE DE CITAS";				 
	$titulosColumnas = array(		
            						 'Cita', 
            						 'Inmueble',
            						 'Interesado',
            						 'Estado Interesado',
            						 'Telefono',
            						 'Promotor',
            						 'Tarea',
            						 'Estado',
            						 'Fecha',
            						 'Hora',
            						 'Barrio',
            						 'Dirección',
            						 'Exito',
            						 'Observación General',
            						 'Medio', 
                                     'Ultimo Seguimiento', 
            						 );
	$objPHPExcel->setActiveSheetIndex(0)
		    ->mergeCells('A1:P1')
		    ->mergeCells('A2:P2');

    // Se agregan los titulos del reporte
    $objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1',$tituloReporte)
	    ->setCellValue('A3',  $titulosColumnas[0])
        ->setCellValue('B3',  $titulosColumnas[1])
	    ->setCellValue('C3',  $titulosColumnas[2])
		->setCellValue('D3',  $titulosColumnas[3])
		->setCellValue('E3',  $titulosColumnas[4])
		->setCellValue('F3',  $titulosColumnas[5])
		->setCellValue('G3',  $titulosColumnas[6])
		->setCellValue('H3',  $titulosColumnas[7])
		->setCellValue('I3',  $titulosColumnas[8])
		->setCellValue('J3',  $titulosColumnas[9])
		->setCellValue('K3',  $titulosColumnas[10])
		->setCellValue('L3',  $titulosColumnas[11])
		->setCellValue('M3',  $titulosColumnas[12])
		->setCellValue('N3',  $titulosColumnas[13])
		->setCellValue('O3',  $titulosColumnas[14])
		->setCellValue('P3',  $titulosColumnas[15]);

		$i=4;

		foreach ($response as $key => $value) {

			$direccion		= ucwords(strtolower(getCampo('inmuebles',"WHERE idInm='".$value['id_inmueble']."'",'Direccion')));
			$IdBarrio		= getCampo('inmuebles',"WHERE idInm='".$value['id_inmueble']."'",'IdBarrio');
			$Barrio		= ucwords(strtolower(getCampo('barrios','WHERE IdBarrios='.$IdBarrio,'NombreB')));
			$exito			= getCampo('parametros',"where id_param=10 and conse_param=".$value['exito_cita'],'desc_param');
			$estacli		= ucwords(strtolower(getCampo('clientes_inmobiliaria',"where cedula=".$value['id_cliente'],'est_cli')));
			$Estcliente		=  ucwords(strtolower(getCampo('parametros',"where id_param=15 and conse_param=".$estacli,'desc_param')));

			if($fila_s['id_cliente']==0){
				$Ncliente="";
				$idMedio="";
				$Medio="";
			}else{
				$estacli		= ucwords(strtolower(getCampo('clientes_inmobiliaria',"where cedula=".$value['id_cliente'],'est_cli')));
				$Ncliente		= ucwords(strtolower(getCampo('clientes_inmobiliaria',"where cedula=".$value['id_cliente'],'nombre')));
				$Estcliente		=  ucwords(strtolower(getCampo('parametros',"where id_param=15 and conse_param=".$estacli,'desc_param')));
				$TelCliente		= ucwords(strtolower(getCampo('clientes_inmobiliaria',"where cedula=".$value['id_cliente'],'concat(telfijo," ",telcelular)')));
				$idMedio		= ucwords(strtolower(getCampo('clientes_inmobiliaria',"where cedula=".$value['id_cliente'],'idmedio')));
				$Medio		= ucwords(strtolower(getCampo('medio',"where idmedio=".$idMedio,'descripcion')));
			}

			
			
			 $ultimoSeguimiento		= ucwords(strtolower(getCampo('retro_cita',"where idcita=".$value['idCita']." limit 0,30",'resultado')));
			 
			//$NPromotor		= ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$value['qasigna']."' limit 0,2",'Nombres')));
	 		/*$APromotor		= ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios=".$value['qasigna'],'apellidos')));*/
	 		$NPromotor = ucwords(strtolower(getCampo('usuarios',"where Id_Usuarios='".$value['qasigna']."' limit 0,30",'concat(Nombres," ",apellidos)')));
	 		
	 		$tarea	= ucwords(strtolower(getCampo('asunto_cita','WHERE id_asunto='.$value['id_asunto'],'descripcion')));

	 		$estado		= getCampo('parametros',"where id_param= 11 and conse_param=".$value['estado'],'desc_param');	

			
			$objPHPExcel->setActiveSheetIndex(0)
    		    ->setCellValue('A'.$i,  utf8_encode($value["idCita"]))
	            ->setCellValue('B'.$i,  utf8_encode($value["id_inmueble"]))
	            ->setCellValue('C'.$i,  utf8_encode($Ncliente))
        		->setCellValue('D'.$i,  utf8_encode($Estcliente))
    		    ->setCellValue('E'.$i,  utf8_encode($value["telefonoCli"]))
        		->setCellValue('F'.$i,  utf8_encode($NPromotor))
	            ->setCellValue('G'.$i,  utf8_encode($tarea))
	            ->setCellValue('H'.$i,  utf8_encode($estado))
	            ->setCellValue('I'.$i,  $value["fechCita"])
    		    ->setCellValue('J'.$i,  utf8_encode($value["hora"]))
        		->setCellValue('K'.$i,  utf8_encode($Barrio))
        		->setCellValue('L'.$i,  utf8_encode($direccion))
	            ->setCellValue('M'.$i,  utf8_encode($exito))
    		    ->setCellValue('N'.$i,  utf8_encode($value["description"]))
        		->setCellValue('O'.$i,  utf8_encode($Medio))
        		->setCellValue('P'.$i,  utf8_encode($ultimoSeguimiento));

        	if($i%2 == 0)
		    {
			$objPHPExcel->getActiveSheet()
						->getStyle("A$i:P$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFE0F2FF');

		    }
		    
				

			$i++;
		}

	$estiloTituloReporte = array(
        	'font' => array(
	        	'name'      => 'Verdana',
    	        'bold'      => true,
        	    'italic'    => false,
                'strike'    => false,
               	'size' =>10,
	            	'color'     => array(
    	            	'rgb' => 'FFFFFF'
        	       	)
            ),
	        'fill' => array(
				'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
				'color'	=> array('argb' => 'FF220835')
			),
            'borders' => array(
               	'allborders' => array(
                	'style' => PHPExcel_Style_Border::BORDER_NONE                    
               	)
            ), 
            'alignment' =>  array(
        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        			'rotation'   => 0,
        			'wrap'          => TRUE
    		)
        );

	$estiloTituloColumnas = array(
        'font' => array(
            'name'      => 'Arial',
            'bold'      => true, 
            'size' 		=>7,                         
            'color'     => array(
                'rgb' => '000000'
            )
        ),
        'fill' 	=> array(
			'type'		=> PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
			'rotation'   => 90,
    		'startcolor' => array(
        		'rgb' => 'c47cf2'
    		),
    		'endcolor'   => array(
        		'argb' => 'FF431a5d'
    		)
		),
        'borders' => array(
        	'top'     => array(
                'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                'color' => array(
                    'rgb' => '143860'
                )
            ),
            'bottom'     => array(
                'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                'color' => array(
                    'rgb' => '143860'
                )
            )
        ),
		'alignment' =>  array(
    			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    			'wrap'          => TRUE
		));

	$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($estiloTituloReporte);
	$objPHPExcel->getActiveSheet()->getStyle('A3:P3')->applyFromArray($estiloTituloColumnas);	

	for($i = 'A'; $i <= 'P'; $i++){
		$objPHPExcel->setActiveSheetIndex(0)			
			->getColumnDimension($i)->setAutoSize(TRUE);
	}

	// Se asigna el nombre a la hoja
	$objPHPExcel->getActiveSheet()->setTitle('Citas');

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

 	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment;filename='Citas".$_SESSION["IdInmmo"].date("Y-m-d hh:mm:ss").".xls'");
	header("Cache-Control: max-age=0");

	$objWriter->save('php://output');


	/*echo "<pre>";
	print_r($response);
	echo "</pre>";*/
	//print_r($_GET);
}