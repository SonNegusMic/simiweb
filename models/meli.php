<?php
//exit();
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

include("../funciones/fncfields2.php");
include("../funciones/fncfields3.php");
include("../funciones/cge00001.php");
include("../funciones/myfncs.php");
include("../funciones/conni.php");
include("../funciones/connisr.php");
 ///conexion pdo
include("../controllers/class.inmuebles.php");
include("../controllers/class.mercadolibre.php");


//$w_conexion = new MySQL();
//$profile = new Profile($conn);
// $mailpqrs= new mailPqr();

$data=$_GET["data"];
$w_conexion = new MySQL();
$mercaLibre = new Mercadolibre($conn);
$inmClass = new Inmuebles();

//1 = inicializacion
if( $data == 1 ){
	
	$mercaLibre->getInit();

	if( isset($_SESSION["referer"]) AND isset($_GET['code'])){
		$url = $_SESSION["referer"]."&code=".$_GET['code'];
		
		header('Location: '.$url );
	}


	//print_r($_SESSION);
}

//2 = obtenes valores especifico
if( $data == 2 ){
	$mercaLibre->getData();
}

//3 = eliminacion de registro
if( $data == 3 ){
	$mercaLibre->deleteData();
}

//4 = listado de items
if( $data == 4 ){
	$mercaLibre->getItems();
}

//5 = actualizacion de items
if( $data == 5 ){
	$body = array('text' => 'Adding new description <strong>html</strong>');
	$mercaLibre->putData($body);
}

//6 = add user test
if( $data == 6 ){
	$mercaLibre->addUserTest();
}

//7 = add propiedad
//
if( $data == 7 ){


	$inmueble = array( "title"=> "Propiedad Prueba Creada",
	  'category_id'=> "MCO1474",
	  'price'=> 100000,
	  'currency_id'=> "COP",
	  'available_quantity'=> 1,
	  'buying_mode'=> "classified",
	  'listing_type_id'=> "silver",
	  'condition'=> "not_specified",
	  'pictures'=> array(
	  	array(
		  	'id'=> "MLA2096545948_102011",
		    'source'=>"http://media.point2.com/p2a/htmltext/f2a4/590f/3627/f49be256595a86c91457/original.jpg"
	    )
	  ),
	  'seller_contact'=>array(
		'contact'=> "Contact name",
		'other_info'=> "Additional contact info",
		'area_code'=> "011",
		'phone'=> "4444-5555",
		'area_code2'=> "",
		'phone2'=> "",
		'email'=> "contact-email@somedomain.com",
		'webmail'=> ""
	  ),
	  'location'=> array(
		'address_line'=> "My property address 1234",
		'zip_code'=> "01234567",
	    'neighborhood'=> array(
	  		'id'=> "TUxBQlBBUzgyNjBa"
		),
		'latitude'=> '-34.48755',
		'longitude'=> '-58.56987',
	  ),
	  'attributes'=> array(
		array(
	  	'id'=> "MLA1472-ANTIG",
	  	'value_id'=> "MLA1472-ANTIG-A_ESTRENAR"
		),
		array(
	  	'id'=> "MLA1472-DISPOSIC",
	  	'value_id'=> "MLA1472-DISPOSIC-FRENTE"
		),
		array(
	  	'id'=> "MLA1472-AMBQTY",
	  	'value_id'=> "MLA1472-AMBQTY-2"
		),
		array(
	  	'id'=> "MLA1472-BATHQTY",
	  	'value_id'=> "MLA1472-BATHQTY-1"
		),
		array(
	  	'id'=> "MLA1472-DORMQTY",
	  	'value_id'=> "MLA1472-DORMQTY-2"
		),
		array(
	  	'id'=> "MLA1472-EDIFIC",
	  	'value_id'=> "MLA1472-EDIFIC-DEPARTAMENTO"
		),
		array(
	  	'id'=> "MLA1472-MTRS",
	  	'value_name'=> "80"
		),
		array(
	  	'id'=> "MLA1472-MTRSTOTAL",
	  	'value_name'=> "100"
		)
	  ),
	  'description' => "This is the real estate property descritpion." );

  echo "<pre>".( json_encode($inmueble));
 	echo "entro";

	$publicar= $mercaLibre->addProperty("https://api.mercadolibre.com/items",$inmueble);

	print_r($publicar);
}

/*********************************
 *              listado categorias principales               *
 *********************************/

if($data == 8){
	$listado=$mercaLibre->getList("https://api.mercadolibre.com/sites/MCO/categories");//MLA
	echo "<pre>";
	print_r($listado);
	echo "</pre>";
}
/*********************************
 *              listado de categoria inmuebles               *
 *********************************/
if($data == 9){
	$listado=$mercaLibre->getList("https://api.mercadolibre.com/categories/MCO1459");
	echo "<pre>";
	print_r($listado);
	echo "</pre>";
}

/*********************************
 *              listado categorias hijas inmuebles,                *
 *********************************/
if( $data == 10 ){
	$listado = $mercaLibre->getList("https://api.mercadolibre.com/categories/MCO1459");

	//print_r($listado["body"]->children_categories);

	echo "<ul>";
	foreach ($listado["body"]->children_categories as $key => $value) {
		# code...
		echo "<li>";
			echo "<strong>".$value->id."</strong> ".$value->name;
		echo "</li>";
	}
	echo "</ul>";
}
/*********************************
 *              listado de categoria inmuebles  pais            *
 *********************************/
if($data == 11){
	//$listado=$mercaLibre->getList("https://api.mercadolibre.com/sites/MLB/categories/all");
	//$listado = $mercaLibre->getList("https://api.mercadolibre.com/sites/MLA/search?category=MLA5726");
	$listado = $mercaLibre->getList("https://api.mercadolibre.com/sites/MCO/search?category=MCO1071");
	echo "<pre>";
	print_r($listado);
	echo "</pre>";

	/* $ch = curl_init();  
	 curl_setopt($ch, CURLOPT_URL, "https://api.mercadolibre.com/sites/MLB/categories/all");  
	 curl_setopt($ch, CURLOPT_HEADER, true);  //queremos las cabeceras de la respuesta
	 curl_setopt($ch, CURLOPT_NOBODY, true);  //no queremos el cuerpo de la respuesta
	 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //el resultado lo almacenaremos en una cadena
	 $data = curl_exec($ch);  
	 curl_close($ch);
	 echo $data;  */
	 
}


if( $data == 12 ){

	$propiedad = array(
	"title"=> "Propiedad de prueba 12",
	"category_id"=> "MCO1474",//venta, arriendo, arriendo temporal
	"price"=> 1500000,
	"currency_id"=> "COP",
	"available_quantity"=> 1,
	"buying_mode"=> "classified",
	"listing_type_id"=> "gold",//gold para venta
	"condition"=> "not_specified",//new, old, not_specified
	"pictures"=> array(
		array("source"=>"https://simiinmobiliarias.com/mcomercialweb/Fotos/98/98-2182/WhatsAppImage20161005at8.36.18AM.jpg"),
		array("source"=>"https://simiinmobiliarias.com/mcomercialweb/Fotos/98/98-2182/WhatsAppImage20161005at8.36.41AM.jpg"),
		array("source"=>"https://simiinmobiliarias.com/mcomercialweb/Fotos/98/98-2182/WhatsAppImage20161005at8.36.49AM.jpg"),
	),
	"seller_contact"=> array(
		"contact"=> "Jhon doe",
		"other_info"=> "Additional contact info",
		"area_code"=> "011",
		"phone"=> "4444-5555",
		"area_code2"=> "",
		"phone2"=> "",
		"email"=> "jhon-doe@somedomain.com",
		"webmail"=> ""
	),
	"location"=> array(
		"address_line"=> "My property address 1234567",
		"zip_code"=> "01234567",
		"neighborhood"=> array(
			"id"=> "TUNPQkFNQjk3Nzkz"
		),
		"latitude"=> 4.7042101,
		"longitude"=> -74.05383970000003
	),
	"attributes"=> array(
		array(
		"id"=> "BATHQTY",
		"value_name"=> "2"//habitaciones
		),
		array(
		"id"=> "AMBQTY",//baños
		"value_name"=> "1"
		),
		array(
		"id"=> "MCO1459-MTRS",//metros
		"value_name"=> "3"
		),
		array(
		"id"=> "MCO1459-ANTIG",//antiguedad
		"value_name"=> "4"
		)
	),
	"description"=> "This is the real estate property <strong>descritpion</strong>, Nulla porttitor accumsan tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Donec rutrum congue leo eget malesuada. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur aliquet quam id dui posuere blandit.

Cras ultricies ligula sed magna dictum porta. <strong>Quisque</strong> velit nisi, pretium ut lacinia in, elementum id enim. Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Sed porttitor lectus nibh. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.

Sed porttitor lectus nibh. Cras ultricies ligula sed magna dictum porta. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Proin eget tortor risus. Donec rutrum congue leo eget malesuada. Nulla quis lorem ut libero malesuada feugiat."
	);



/* $service_url = "https://api.mercadolibre.com/items?access_token=APP_USR-4044113243382026-113012-3f8ce5664f2bb1398648244e3f4eba5c__M_G__-236296032";
      
       $curl_post_data = json_encode($propiedad);
       $headers =  'Authorization: APP_USR-4044113243382026-113012-3f8ce5664f2bb1398648244e3f4eba5c__M_G__-236296032';
    $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);
        $response = json_decode($curl_response,true);
        curl_close($curl);
        echo "<pre>";
        print_r($response);
        echo "</pre>";
        exit;*/
	/*
	
{
"title": "Property title",
"category_id": "MCO1473",
"price": 1000000,
"currency_id": "COP",
"available_quantity": 1,
"buying_mode": "classified",
"listing_type_id": "silver",
"condition": "not_specified",
"pictures": [
{
"source":"http://mla-d2-p.mlstatic.com/item-de-test-no-ofertar-543605-MLA25041518406_092016-O.jpg?square=false"
}
],
"seller_contact": {
"contact": "Contact name",
"other_info": "Additional contact info",
"area_code": "011",
"phone": "4444-5555",
"area_code2": "",
"phone2": "",
"email": "contact-email@somedomain.com",
"webmail": ""
},
"location": {
"address_line": "My property address 1234",
"zip_code": "01234567",
"neighborhood": {
"id": "TUNPQjExRDk0MjYwMw"
},
"latitude": 4.7042101,
"longitude": -74.05383970000003
},
"attributes": [
{
"id": "BATHQTY",
"value_name": "2"//Baños
},
{
"id": "AMBQTY",
"value_name": "1"//Habitaciones
},
{
"id": "MCO1459-MTRS",
"value_name": "1"//
},
{
"id": "MCO1459-ANTIG",
"value_name": "4"
}
],
"description": "This is the real estate property descritpion."
}	


{
  "message": "Validation error",
  "error": "validation_error",
  "status": 400,
  "cause": [
    {
      "code": "item.attributes.missing_required",
      "message": "The attributes [BATHQTY, AMBQTY, MCO1459-MTRS, MCO1459-ANTIG] are required for category MCO1473. Check the attribute is present in the attributes list or in all variation attributes combination."
    }
  ]
}



https://api.mercadolibre.com/items?access_token=APP_USR-7703169885575284-112817-f41f70140af93d6eca83291fa9287523__N_A__-235779118







{
"title": "propiedad de prueba 4",
"category_id": "MCO164683",
"price": 1500000,
"currency_id": "COP",
"available_quantity": 1,
"buying_mode": "classified",
"listing_type_id": "silver",
"condition": "not_specified",
"pictures": [

{"source":"https://www.simiinmobiliarias.com/mcomercialweb/Fotos/98/98-2182/WhatsAppImage20161005at8.36.18AM.jpg"},
{"source":"https://www.simiinmobiliarias.com/mcomercialweb/Fotos/98/98-2182/WhatsAppImage20161005at8.36.41AM.jpg"},
{"source":"https://www.simiinmobiliarias.com/mcomercialweb/Fotos/98/98-2182/WhatsAppImage20161005at8.36.49AM.jpg"},

],
"seller_contact": {
"contact": "Contact name",
"other_info": "Additional contact info",
"area_code": "011",
"phone": "4444-5555",
"area_code2": "",
"phone2": "",
"email": "contact-email@somedomain.com",
"webmail": ""
},
"location": {
"address_line": "My property address 1234",
"zip_code": "01234567",
"neighborhood": {
"id": "TUNPQjExRDk0MjYwMw"
},
"latitude": 4.7042101,
"longitude": -74.05383970000003
},
"attributes": [{
"id": "BATHQTY",
"value_name": "2"
},
{
"id": "AMBQTY",
"value_name": "1"
},
{
"id": "MCO1459-MTRS",
"value_name": "1"
},
{
"id": "MCO1459-ANTIG",
"value_name": "4"
}
],
"description": "This is the real estate property descritpion."
}



**********************ATRIBUTOS RETORNADOS**************************
"attributes": [
    {
      "id": "MCO1459-INMUEBLE",
      "name": "Inmueble",
      "value_id": "MCO1459-INMUEBLE-APARTAMENTO",
      "value_name": "Apartamento",
      "attribute_group_id": "FIND",
      "attribute_group_name": "Ficha técnica"
    },
    {
      "id": "MCO1459-MTRS",
      "name": "Metros de const.",
      "value_id": "",
      "value_name": "3",
      "attribute_group_id": "FIND",
      "attribute_group_name": "Ficha técnica"
    },
    {
      "id": "MCO1459-OPERACION",
      "name": "Operación",
      "value_id": "MCO1459-OPERACION-VENTA",
      "value_name": "Venta",
      "attribute_group_id": "FIND",
      "attribute_group_name": "Ficha técnica"
    },
    {
      "id": "AMBQTY",
      "name": "Habitaciones",
      "value_id": "",
      "value_name": "1",
      "attribute_group_id": "PRINCIPALES",
      "attribute_group_name": "Características principales"
    },
    {
      "id": "BATHQTY",
      "name": "Baños",
      "value_id": "",
      "value_name": "2",
      "attribute_group_id": "PRINCIPALES",
      "attribute_group_name": "Características principales"
    },
    {
      "id": "MCO1459-ANTIG",
      "name": "Años de antigüedad",
      "value_id": "",
      "value_name": "4",
      "attribute_group_id": "PRINCIPALES",
      "attribute_group_name": "Características principales"
    }
  ],


*******errores*************
*
* Listing type silver is not available for category MCO1474

	 */


	$agregar=$mercaLibre->addProperty("https://api.mercadolibre.com/items",$propiedad);
	//print_r($agregar);
	echo "<pre>";
	print_r($agregar);
	echo "</pre>";

}

/*********************************
 *              listado departamentos pais               *
 *********************************/
if( $data == 13 ){
	echo "<h1>Listado de departamentos</h1>";
	$listado = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/countries/CO");
	//$listado = $mercaLibre->getList("https://api.mercadolibre.com/countries/CO");
	echo "<pre>";
	print_r($listado);
	echo "</pre>";

}

/*********************************
 *              listado ciudades segun departamento               *
 *********************************/

if( $data == 14 ){

	//listado viene bogotá
	$listado = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/states/TUNPUENVTmE3NmQ4");
	//cundinamarca - TUNPUENVTmE3NmQ4
	//valle - TUNPUFZBTGExNmNjNg
	//$listado = $mercaLibre->getList("https://api.mercadolibre.com/states/CO-CUN");
	
	//listado pero no viene bogotá
	//$listado = $mercaLibre->getList("https://api.mercadolibre.com/states/CO-CUN");
	//cundinamarca
	echo "<h1>Listado de ciudades</h1>";
	echo "<pre>";
	print_r($listado);
	echo "</pre>";

	
}

/*********************************
 *              listado barrios segun ciudad               *
 *********************************/

if( $data == 15 ){

	$listado = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/cities/TUNPQ0JPRzYwNjYwNA");
	//$listado = $mercaLibre->getList("https://api.mercadolibre.com/cities/TUNPQ0JPRzYwNjYwNA");
	//bogotá - TUNPUEJPR1gxMDljZA
	echo "<h1>Listado de barrios</h1>";
	echo "<pre>";
	print_r($listado);
	echo "</pre>";

	
}

/*********************************
 *              caracteristicas del tipo de inmueble               *
 *********************************/
if( $data == 16 ){

	//
	//$listado = $mercaLibre->getList("https://api.mercadolibre.com/categories/MLA79242/attributes");
	
	//listado de caracteristicas de la categoria - departamentos - MLA1472
	$listado = $mercaLibre->getList("https://api.mercadolibre.com/categories/MLA1472");
	//$listado = $mercaLibre->getList("https://api.mercadolibre.com/cities/TUNPQ0JPRzYwNjYwNA");
	//bogotá - TUNPUEJPR1gxMDljZA
	echo "<h1>Listado de caracteristicas por categoria de inmueble(tipo inmueble)</h1>";
	echo "<pre>";
	print_r($listado);
	echo "</pre>";

	
}

/*********************************
 *              caracteristicas del tipo de destinacion de inmueble               *
 *********************************/
if( $data == 17 ){

	//
	//$listado = $mercaLibre->getList("https://api.mercadolibre.com/categories/MLA79242/attributes");
	
	//listado de caracteristicas de la categoria - departamentos - MLA1472
	$listado = $mercaLibre->getList("https://api.mercadolibre.com/categories/MLA1472");
	//$listado = $mercaLibre->getList("https://api.mercadolibre.com/cities/TUNPQ0JPRzYwNjYwNA");
	//bogotá - TUNPUEJPR1gxMDljZA
	echo "<h1>Listado de destinacion por categoria de inmueble(tipo inmueble)</h1>";
	echo "<ul>";
	foreach ($listado["body"]->children_categories as $key => $value) {
		# code...
		echo "<li>";
			echo "<strong>".$value->id."</strong> ".$value->name;
		echo "</li>";
	}
	echo "</ul>";

	
}

/*********************************
 *              eliminar publicacion               *
 *********************************/

if( $data == 18 ){

	$arg=array("deleted"=>"true");
	$delete = $mercaLibre->deleteProperty("https://api.mercadolibre.com/items/MCO432229441",$arg);
	
	echo "<h1>Eliminacion de inmueble</h1>";
	
	echo "<pre>";
	print_r($delete);
	echo "</pre>";

}

/*********************************
 *              homologacion               *
 *********************************/

if( $data == 19 ){

	//listado principal de categorias
	$categoriaprincipal=$mercaLibre->getList("https://api.mercadolibre.com/sites/MCO/categories");
	//tipo inmueble
	$tipoinmueblesimi="MCO1496";//inmuebles , MCO1472-Apartamento , MCO1478-oficina, MCO1466 casas, MCO1496  fincas
	//variable donde almacenamos el id de el tipo de inmueble
	$tipoinmueblemeli=0;
	//texto otros inmuebles
	$otrosInmuebles = "Otros Inmuebles";
	//id otros inmuebes
	$otroinmueblemeli =0;
	//tipogestion en simi
	$tipogestionsimi = "Arriendo";//Arriendo, Venta
	//tipogestion en meli
	$tipogestionmeli = 0;
	//texto grupoid CARACTERISTICAS
	$textcaracteristica ="Inmueble";
	//array con caracteristicas
	$caracteristicassimi = array("BATHQTY"=>array("id"=>"BATHQTY","name"=>"Baños","val"=>1),"AMBQTY"=>array("id"=>"AMBQTY","name"=>"Habitaciones","val"=>1),"MCO1459-ANTIG"=>array("id"=>"MCO1459-ANTIG","name"=>"Años de antigüedad","val"=>3),"MCO1459-MTRS"=>array("id"=>"MCO1459-MTRS","name"=>"Metros de const.","val"=>20),"MCO1459-BATHQTY"=>array("id"=>"MCO1459-MTRS","name"=>"Baños","val"=>1));
	$caracteristicassimionly = array("BATHQTY","AMBQTY","MCO1459-ANTIG","MCO1459-MTRS");
	//bandera para verificar que trae caracteristica buscada
	$foundcaracteristicrequired = 0;
	//acum total de caracteristicas requeridas y que estan correctamente
	$acumcaracteristicrequired = 0;
	//acum total de caracteristicas requeridas
	$iacount=0;


	//recorremos la categoria principal
	foreach ($categoriaprincipal["body"] as $key => $value) {
		
		//validamos si inmueble esta dentro del array lo cual nos permitira elegir el id correspondiente
		if ( strpos( strtolower( $value->name ), 'inmueble') !== false) {
		    $inmuebleidmeli=$value->id;
		}

	}
	//guardamos la categoria que requerimos la cual es inmuebles
	$categoriapadre = $inmuebleidmeli;//"MCO1459-INMUEBLE";//inmuebles

	//listado de las categorias hijas de inmuebles (estás son el tipo de inmueble)
	$listadoTipoInmueble=$mercaLibre->getList("https://api.mercadolibre.com/categories/".$categoriapadre);
	//recorremos los tipos de inmuebles de la categoria principal
	if( count($listadoTipoInmueble["body"]->children_categories) !=0  ){

	
		foreach ($listadoTipoInmueble["body"]->children_categories as $key => $value) {

			//validamos si el tipo de inmuebles esta dentro del array lo cual nos permitira elegir el id correspondiente
				
			if( strpos( strtolower ( $value->id ), strtolower( $tipoinmueblesimi) ) !== false ){

				$tipoinmueblemeli=$value->id;
				
			}

			if( strpos( strtolower( $value->name ), strtolower($otrosInmuebles) )!== false ){
				
				$otroinmueblemeli=$value->id;
				
			}

		}

	}else{
		echo"No contiene categoria padre";
	}
	

	//validamos si no se encontro concordancia en el  nombre del tipo de inmueble, se le asigna el id de "otros inmuebles"
	if( $tipoinmueblemeli === 0 ){
		$tipoinmueblemeli = $otroinmueblemeli;
	}
		//echo $tipoinmueblemeli;


	//listado de tipo de gestion
	$tipogestion = $mercaLibre->getList("https://api.mercadolibre.com/categories/".$tipoinmueblemeli);

	foreach ($tipogestion["body"]->children_categories as $key => $value) {
		
		//elegimos el tipo de gestion
		if( strtolower( $value->name ) == strtolower( $tipogestionsimi ) ){

			$tipogestionmeli = $value->id;

		}
		
		
	}

	/*echo "<pre>";
	print_r($tipogestion);
	echo "</pre>";
	echo "tipoinmueblemeli: ".$tipoinmueblemeli;
	exit;*/
	

	//listado de caracteristicas de tipo de inmueble
	$caracteristicas = $mercaLibre->getList("https://api.mercadolibre.com/categories/".$tipogestionmeli."/attributes");


	$found=0;
	//recorremos el array y validamos el name para omitir la primera posicion, validamos si es o no requerido con el parametro tags que nos trae {"required": true}
	foreach ($caracteristicas["body"] as $key => $value) {
		if( strtolower($value->name) != strtolower($textcaracteristica) ){
			//echo $value->name."<br>";
			if($value->tags->required){
				$i=0;
				//echo $value->id."|";
				foreach ($caracteristicassimi as $key => $valuecaracteristicassimi) {
					$i++;
					//echo $value->id."<br> no valid <br>conteo <br>";
					$setnamesimi = strtolower( $valuecaracteristicassimi["id"] );
					$setvalimi = strtolower( $valuecaracteristicassimi["val"] );
					$setvalimi =  trim($setvalimi) ;
					
					//si encuentra el id dentro del array
					//if( strpos( $setnamesimi , strtolower($value->id) ) !== false ){
					//echo "caracteristica meli: ".$value->id."<br>";
					//echo "caracteristica simi: ".$setnamesimi."<br>";
					if(  $value->id == $setnamesimi  ){
					//echo $setvalimi." | ".$setnamesimi." | ".$value->id." | ".strpos( $setnamesimi , strtolower($value->id))."<br>";
					//echo $value->id." <br>valid <br>";
						$foundcaracteristicrequired=1;
						$acumcaracteristicrequired++;
						if(  empty( $setvalimi ) or $setvalimi == 0 ){
							echo "La característica: ".$setnamesimi." Es obligatoria <br>";
						}
						//echo "entro";
					}else{
						//echo strtolower($value->id)."|".$setnamesimi."<br>";
						
						/*if( !in_array($value->id, $found) ){
							array_push($found,$value->id);
						}*/
						//echo "La característica: ".$setnamesimi." Es obligatoria <br>";
					}

				}
				$foundcaracteristicrequired=0;
				$iacount++;

				if( !array_key_exists($value->id, $caracteristicassimi) ){
					$found=1;
					echo "La característica: ".$value->id." Es obligatoria <br>";
				}

			}
			
			
		}
	}

echo "<pre>";
	print_r($caracteristicassimi);
	echo "</pre>";	

/*echo "tipoinmueblemeli: ".$tipoinmueblemeli;
echo "<pre>";
print_r($caracteristicas);
echo "</pre>";*/



	if(  $found == 0){
		echo "validación de requeridas resultado: SATISFACTORIO, será sincronizado";

		$propiedad = array(
		"title"=> "Propiedad de prueba 11",
		"category_id"=> $tipogestionmeli,//venta, arriendo, arriendo temporal
		"price"=> 1500000,
		"currency_id"=> "COP",
		"available_quantity"=> 1,
		"buying_mode"=> "classified",
		"listing_type_id"=> "gold",//gold para venta
		"condition"=> "not_specified",//new, old, not_specified
		"pictures"=> array(
			array("source"=>"https://www.simiinmobiliarias.com/mcomercialweb/Fotos/98/98-2182/WhatsAppImage20161005at8.36.18AM.jpg"),
			array("source"=>"https://www.simiinmobiliarias.com/mcomercialweb/Fotos/98/98-2182/WhatsAppImage20161005at8.36.41AM.jpg"),
			array("source"=>"https://www.simiinmobiliarias.com/mcomercialweb/Fotos/98/98-2182/WhatsAppImage20161005at8.36.49AM.jpg"),
		),
		"seller_contact"=> array(
			"contact"=> "Jhon doe",
			"other_info"=> "Additional contact info",
			"area_code"=> "011",
			"phone"=> "4444-5555",
			"area_code2"=> "",
			"phone2"=> "",
			"email"=> "jhon-doe@somedomain.com",
			"webmail"=> ""
		),
		"location"=> array(
			"address_line"=> "My property address 1234567",
			"zip_code"=> "01234567",
			"neighborhood"=> array(
				"id"=> "TUNPQjExRDk0MjYwMw"
			),
			"latitude"=> 4.7042101,
			"longitude"=> -74.05383970000003
		),
		"attributes"=> array(
			array(
			"id"=> "BATHQTY",
			"value_name"=> "2"//habitaciones
			),
			array(
			"id"=> "AMBQTY",//baños
			"value_name"=> "1"
			),
			array(
			"id"=> "MCO1459-MTRS",//metros
			"value_name"=> "3"
			),
			array(
			"id"=> "MCO1459-ANTIG",//antiguedad
			"value_name"=> "4"
			)
		),
		"description"=> "This is the real estate property <strong>descritpion</strong>, Nulla porttitor accumsan tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Donec rutrum congue leo eget malesuada. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Nulla porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur aliquet quam id dui posuere blandit.

	Cras ultricies ligula sed magna dictum porta. <strong>Quisque</strong> velit nisi, pretium ut lacinia in, elementum id enim. Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Proin eget tortor risus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Sed porttitor lectus nibh. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.

	Sed porttitor lectus nibh. Cras ultricies ligula sed magna dictum porta. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Proin eget tortor risus. Donec rutrum congue leo eget malesuada. Nulla quis lorem ut libero malesuada feugiat."
		);
	}
	//echo $tipogestionmeli;
	//echo $tipoinmueblemeli;
	//buscamos la categoriapadre seleccionada
	


}

/*********************************
 *                             *
 *********************************/
if( $data == 20 ){

	//buscamos la ciudad en meli almacenada en simi
	$ciudad = 11001;
	//id ciudad en meli
	$ciudadMeli = "TUNPQ0JPRzYwNjYwNAs";
	//si es bogotá es tomado como un departamento
	$departamento ="TUNPUEJPR1gxMDljZA";
	//si es bogotá almacenar localidad meli
	$localidadMeli = "TUNPQ0ZPTjcyNzAz";
	//localidad
	$localidad = "12";
	//id barrio meli
	$barrio = "TUNPQkFNQjk3Nzkz";
	//verificamos que el id de ciudad exista
	$verificaCiudad = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/cities/".$ciudadMeli);
	
	//si no existe homologamos las ciudades del departamento del inmueble que se hace la publicacion
	if( isset($verificaCiudad["body"]->status) and $verificaCiudad["body"]->status == 404 ){

		echo "No existe la ciudad en meli, se debe rehomologar las ciudades";

		$ciudadesDepartamento = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/states/".$departamento);

		if( isset($ciudadesDepartamento["body"]->status) and $ciudadesDepartamento["body"]->status == 404 ){

			$departamentoPais = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/countries/CO");

			foreach ($departamentoPais["body"]->states as $key => $value) {
				# id,name - realizamos el update en la tabla departamentos
				echo "<br>No existe el departamento en meli se hace homologacion";
			}

			$departamento ="TUNPUEJPR1gxMDljZA";//le asignamos el id meli del departamente recientemente actualizado

		}
		//volvemos a invocar las ciudades del departamento
		$ciudadesDepartamento = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/states/".$departamento);
		echo"<br> Ya se homologo el departamento";
		$ciudadMeli = "TUNPQ0JPRzYwNjYwNA";
//echo "Ciudad: ".$ciudadMeli;
		//si ciudad es bogota, se trata los barrios por localidad
		if( $ciudadMeli == "TUNPQ0JPRzYwNjYwNA" ){
			$barriosLocalidad = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/cities/".$localidadMeli);
/*echo "<pre>";
print_r($barriosLocalidad);
echo "</pre>";
		
exit;*/
$i=0;
			//recorremos barrios para actualizar los barrios de la localidad
			foreach ($barriosLocalidad["body"]->neighborhoods as $key => $value) {
				$i++;
				echo " - ".$i." - Barrio: ".$value->name;
				$conexion = new Conexion();
				$name = ''.utf8_decode($value->name).'%';
				$setName = utf8_decode($value->name);
		        $stmt     = $conexion->prepare('UPDATE barrios2 SET idml2 =:idMeli, NameBarrMeli = :NameBarrMeli
										  WHERE NombreB LIKE  :nombreMeli AND IdCiudad = :IdCiudad AND IdLocalidad = :IdLocalidad ');
		        if ($stmt->execute(array(
		            ":idMeli" => $value->id,
		            ":nombreMeli"  => $name,
		            ":IdCiudad" => $ciudad,
		            ":IdLocalidad" => $localidad,
		            ":NameBarrMeli" => $setName))) {
		            echo "<br>se homologo la ciudad";
		        } else {
		            print_r($stmt->errorInfo());
		        }
			}

		}
		exit;

		foreach ($ciudadesDepartamento["body"] as $key => $value) {
			echo "Ciudad:".$value->name;
			$conexion = new Conexion();
	        $stmt     = $conexion->prepare('UPDATE barrios2 SET idml2 =:idMeli
									  WHERE NombreB LIKE  "%:nombreMeli%" AND IdCiudad = :IdCiudad');
	        if ($stmt->execute(array(
	            ":idMeli" => $value->id,
	            ":nombreMeli"  => $value->name,
	            ":IdCiudad" => $ciudad))) {
	            echo "<br>se homologo la ciudad";
	        } else {
	            print_r($stmt->errorInfo());
	        }
		}

		//al id ser diferente pasamos a volver a seleccionar el id de meli de la ciudad que ha sido actualizado anteriormente
		$ciudadMeli = "TUNPQ0JPRzYwNjYwNA";
		//echo"<br> Ya se homologo la ciudad";
	}

}


if( $data == 21 ){

	

	//listado principal de categorias
	$categoriaprincipal=$mercaLibre->getList("https://api.mercadolibre.com/sites/MCO/categories");
	//tipo inmueble
	$tipoinmueblesimi="MCO1466";// MCO1466 - casas, MCO1478 - oficina, MCO1472 - apto, MCO50564 - bodegas
	//variable donde almacenamos el id de el tipo de inmueble
	$tipoinmueblemeli=0;
	//texto otros inmuebles
	$otrosInmuebles = "Otros Inmuebles";
	//id otros inmuebes
	$otroinmueblemeli =0;
	//tipogestion en simi
	$tipogestionsimi = "Arriendo";
	//tipogestion en meli
	$tipogestionmeli = 0;
	//texto grupoid CARACTERISTICAS
	$textcaracteristica ="Inmueble";//Inmueble, Bodega, Casas, Oficina
	//IdTipoInmueble a actualizar 1-inmueble, 6 bodega, 2 casas, 4 oficina
	$IdTipoInmueble = 1;
	//array con caracteristicas
	//array que almacenara caracteristicas de meli no encontradas en simi
	$notfound=array();

	//bandera para verificar que trae caracteristica buscada
	$foundcaracteristicrequired = 0;
	//acum total de caracteristicas requeridas y que estan correctamente
	$acumcaracteristicrequired = 0;
	//acum total de caracteristicas requeridas
	$iacount=0;

	//recorremos la categoria principal
	foreach ($categoriaprincipal["body"] as $key => $value) {
		
		//validamos si inmueble esta dentro del array lo cual nos permitira elegir el id correspondiente inmueble categoria padre
		if ( strpos( strtolower( $value->name ), 'inmueble') !== false) {
		    $inmuebleidmeli=$value->id;
		}

	}
	//guardamos la categoria que requerimos la cual es inmuebles
	$categoriapadre = $inmuebleidmeli;//"MCO1459-INMUEBLE";//inmuebles

	//listado de las categorias hijas de inmuebles (estás son el tipo de inmueble)
	
	$listadoTipoInmueble=$mercaLibre->getList("https://api.mercadolibre.com/categories/".$categoriapadre);
	//recorremos los tipos de inmuebles de la categoria principal
	//print_r($listadoTipoInmueble);
	if( isset($listadoTipoInmueble["body"]->children_categories) ){

	
		foreach ($listadoTipoInmueble["body"]->children_categories as $key => $value) {

			//validamos si el tipo de inmuebles esta dentro del array lo cual nos permitira elegir el id correspondiente
				
			if( strpos( strtolower ( $value->id ), strtolower( $tipoinmueblesimi) ) !== false ){

				$tipoinmueblemeli=$value->id;
				
			}

			if( strpos( strtolower( $value->name ), strtolower($otrosInmuebles) )!== false ){
				
				$otroinmueblemeli=$value->id;
				
			}

		}

	}

	//validamos si no se encontro concordancia en el  nombre del tipo de inmueble, se le asigna el id de "otros inmuebles"
	if( $tipoinmueblemeli === 0 ){
		$tipoinmueblemeli = $otroinmueblemeli;
	}


	//listado de tipo de gestion
	$tipogestion = $mercaLibre->getList("https://api.mercadolibre.com/categories/".$tipoinmueblemeli);
	
	if( isset($tipogestion["body"]->children_categories) ){
		foreach ($tipogestion["body"]->children_categories as $key => $value) {
			
			//elegimos el tipo de gestion
			if( strtolower( $value->name ) == strtolower( $tipogestionsimi ) ){

				$tipogestionmeli = $value->id;

			}
			
			
		}
	}

	

	//listado de caracteristicas de tipo de inmueble
	$caracteristicas = $mercaLibre->getList("https://api.mercadolibre.com/categories/".$tipogestionmeli."/attributes");

	echo"<h2>Atributos caracteristicas meli</h2>";
	//echo "<pre>";
	//print_r($caracteristicas);
	//echo "</pre>";

	//listado de tipo de gestion
	$tipogestion = $mercaLibre->getList("https://api.mercadolibre.com/categories/".$tipoinmueblemeli);

	if( isset($tipogestion["body"]->children_categories) ){
		foreach ($tipogestion["body"]->children_categories as $key => $value) {
			
			//elegimos el tipo de gestion
			if( strtolower( $value->name ) == strtolower( $tipogestionsimi ) ){

				$tipogestionmeli = $value->id;

			}
			
			
		}

	}

	$caracteristicas = $mercaLibre->getList("https://api.mercadolibre.com/categories/".$tipogestionmeli."/attributes");
	
	echo count($caracteristicas["body"])."<br>";
    $connPDO = new Conexion();
    $i=0;
    foreach ($caracteristicas["body"] as $key => $value) {
    	$i++;
    	$name = rtrim($value->name,"es");
    	$name = rtrim($name,"s");
    	$name = '%'.utf8_decode($name).'%';
    	$stmt=$connPDO->prepare("SELECT Descripcion,idCaracteristica,CodCarMeli,NameCarMeli,IdTipoInmueble
             FROM maestrodecaracteristicasnvo
             WHERE DescripcionMeli LIKE :Descripcion AND IdTipoInmueble = :IdTipoInmueble");
    	echo "caracteristicas: ".$name."<br>";
    	//echo "caracteristicas: ".."<br>";
    	//
    		
    	//validacion de nombre meli en grupocaracteristica
    	
    	/*$grupostmt=$connPDO->prepare("SELECT DETALLE,IdGrupo
             FROM grupocaracteristicas
             WHERE DETALLE LIKE :Descripcion ");

    	if($grupostmt->execute(
            array( 
            	   ":Descripcion"=> $name
            	)
            ))
        {	

        	$rowgrupo=$grupostmt->fetch();
        	echo "<h2>GRUPOS DE CARACTERISTICAS GRUPO</h2>";
        	/*echo "<pre>";
        	print_r($rowgrupo);
        	echo "</pre>";*/
        	/*while($rowgrupo=$grupostmt->fetch()){

        		$grupocarstmt=$connPDO->prepare("SELECT Descripcion,idCaracteristica,CodCarMeli,NameCarMeli,IdTipoInmueble
	             FROM maestrodecaracteristicasnvo
	             WHERE IdGrupo LIKE :IdGrupo AND DescripcionMeli LIKE :Descripcion ");

        		if($grupocarstmt->execute(
		            array( 
		            	   ":Descripcion"=> $name,
		            	   ":IdGrupo" => $rowgrupo["IdGrupo"]
		            	)
		            ))
		        {	


		        }

        	}


        }*/
    	

    	if($stmt->execute(
            array( ":IdTipoInmueble"       => $IdTipoInmueble,
            	   ":Descripcion"		   => $name
            	)
            ))
        {	

        	$j=0;
        	$row2=$stmt->fetch();
        	if(empty($row2["idCaracteristica"])){
        		array_push($notfound,str_replace("%","",$name));
        	}
			echo $i." Encontrado ".$row2["idCaracteristica"]." <br> ";

        	while($row=$stmt->fetch()){
        		$j++;
        		//$connPDO = new Conexion();
        		//echo $j." - ".$row["Descripcion"]." | ".$value->id." |||";
        		 $stmt= $connPDO->prepare('UPDATE maestrodecaracteristicasnvo SET CodCarMeli =:CodCarMeli, NameCarMeli = :NameCarMeli
					WHERE idCaracteristica =  :idCaracteristica');
		        if ($stmt->execute(array(
		            ":CodCarMeli" => $value->id,
		            ":NameCarMeli"  => utf8_decode($value->name),
		            "idCaracteristica" => $row["idCaracteristica"]))) {

		          
		        } else {
		            print_r($stmt->errorInfo());
		        }
        	}

        }


    }


    $grupostmt=$connPDO->prepare("SELECT Descripcion,idCaracteristica,CodCarMeli,NameCarMeli,IdTipoInmueble,DescripcionMeli
             FROM maestrodecaracteristicasnvo
             WHERE IdTipoInmueble = :IdTipoInmueble");

    if($grupostmt->execute(
            array( 
            	   ":IdTipoInmueble"=> $IdTipoInmueble
            	)
            )){	


    	while($row=$grupostmt->fetch()){
    	
	    	foreach ($caracteristicas["body"] as $key => $value2) {
	    		if( strpos( strtolower ( $value2->name ), strtolower( $row["DescripcionMeli"]) ) !== false ){
	    		echo $value2->name." - ".$row["DescripcionMeli"];

	    			$stmt= $connPDO->prepare('UPDATE maestrodecaracteristicasnvo SET CodCarMeli =:CodCarMeli, NameCarMeli = :NameCarMeli
						WHERE idCaracteristica = :idCaracteristica');
	    			$descripcion = "%".$row["DescripcionMeli"]."%";
			        if ($stmt->execute(array(
			            ":CodCarMeli" => $value2->id,
			            ":NameCarMeli"  => utf8_decode($value2->name),
			            ":idCaracteristica" => $row["idCaracteristica"]))) {

			           echo "<br>se homologo la caracteristica: ".$row["DescripcionMeli"]."con id simi: ".$row["idCaracteristica"]."<br>";
			        } else {
			            print_r($stmt->errorInfo());
			        }

	    		}

	    	}

	    	foreach ($notfound as $key => $value) {
		    	if( $row["NameCarMeli"] == $value ){
		    		unset($notfound[$key]);

		    	}
		    }

	    }


    }


    

    echo "<pre>";
    print_r($notfound);
    echo "</pre>";


	if( $acumcaracteristicrequired != 0 and $acumcaracteristicrequired == $iacount){
		echo "validación de requeridas resultado: SATISFACTORIO, será sincronizado";
		echo "<pre>";
		print_r($caracteristicas);
		echo "</pre>";

	}


	
	


}
/*********************************
 *              homologacion ciudades meli               *
 *********************************/
if( $data == 22 ){
	//traemos los departamentos a recorrer y a actualizar en simi
	$departamentoPais = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/countries/CO");
	$pais = 169;
	$bogota="bogot";
	$idbogota = 11001;

	foreach ($departamentoPais["body"]->states as $key => $value) {
		# id,name
		# 
		# 
		$name = ''.utf8_decode($value->name).'%';
		$connPDO = new Conexion();
		

		$stmt = $connPDO->prepare("SELECT CodDepMeli,Nombre,IdPais,IdDepartamento
						   FROM departamento2
						   WHERE Nombre LIKE  :nombreMeli AND IdPais = :IdPais ");

		if( strpos( strtolower( $value->name ), strtolower($bogota) )!== false ){
			$bogotamelicod = $value->id;
			$bogotameliname = $value->name;
			$stmtBog = $connPDO->prepare('UPDATE ciudad2 SET CodCiuMeli =:CodCiuMeli
								  WHERE  IdCiudad = :IdCiudad');
			$listadoLocalidadesBogota = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/states/".$value->id);

			#Actualizamos las localidades de bogotá
			#
			foreach ($listadoLocalidadesBogota["body"]->cities as $key => $valueLocalidad) {
				
				$nameLocalidad = strtolower(trim($valueLocalidad->name));
				$nameLocalidad = ''. str_replace("sur", "", utf8_decode($nameLocalidad)).'';
				$nameLocalidad = trim($nameLocalidad);
				$stmtCiu = $connPDO->prepare('UPDATE localidad2 SET CodLocMeli =:CodLocMeli
								  WHERE descripcion LIKE :nombreMeli ');
		        if ($stmtCiu->execute(array(
			            ":CodLocMeli" => $valueLocalidad->id,
			            ":nombreMeli" => $nameLocalidad))) {


		        	echo "---- Homologada la Localidad de bogotá: ".$nameLocalidad;
		        	$listadoBarriosLocalidadesBogota = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/cities/".$valueLocalidad->id
		        		);
		        	
		        	# Actualizamos los barrios de la localidad
		        	foreach ($listadoBarriosLocalidadesBogota["body"]->neighborhoods as $key => $valuelistadoBarriosLocalidadesBogota) {

		        		$nameBarrioLocal = strtolower(trim($valuelistadoBarriosLocalidadesBogota->name));
		        		$nameBarrioLocal = str_replace("-","",$nameBarrioLocal);
						$nameBarrioLocal = ''. utf8_decode($nameBarrioLocal).'';
						$nameBarrioLocal = trim($nameBarrioLocal);

						#busqueda de localidad por el id de meli
						
						$stmtLocalsim = $connPDO->prepare("SELECT idlocalidad,descripcion,CodLocMeli
						   FROM localidad2
						   WHERE CodLocMeli  = ':CodLocMeli' ");

						if ($stmtLocalsim->execute(array(
			            ":CodLocMeli" => $valueLocalidad->id))) {

			            	//$data = $stmtLocalsim->fetch();
			            	while ($rowLoc = $stmtLocalsim->fetch()) {
			            		$codigoLocal = $rowLoc["idlocalidad"];
			            	}

  							//$idlocalidadsimi = $data["idlocalidad"];
  							//echo "idlocalidadsimi: ".$idlocalidadsimi."<br>";
			            	//echo ("Codigo: ".$codigoLocal);
			            }

			            //exit;

		        		$stmtBar = $connPDO->prepare('UPDATE barrios2 SET idml2 =:idml2, NameBarrMeli = :NameBarrMeli  WHERE NombreB LIKE :NameBarrMeli AND IdLocalidad = :IdLocalidad ' );

		        		if ($stmtBar->execute(array(
			            ":idml2" => $valueLocalidad->id,
			            ":NameBarrMeli" => $nameBarrioLocal,
			            ":IdLocalidad" => $codigoLocal))) {

			            	echo "---- Homologada El barrio de bogota: ".$nameBarrioLocal;

			            }

		        	}

			    }

			    

			}
			

			if ($stmtBog->execute(array(
				":CodCiuMeli" => $bogotamelicod,
		        ":IdCiudad" => $idbogota ))) {
			}

			//exit;
		}



        if ($stmt->execute(array(
            ":nombreMeli"  => $name,
            ":IdPais" => $pais))) {

			$encontrado = $stmt->rowCount();
			

			while ($row = $stmt->fetch()) {


	            $codigo = $row["IdDepartamento"];
	            //echo $codigo."<br>";
	            $stmtDep = $connPDO->prepare('UPDATE departamento2 SET CodDepMeli =:CodDepMeli
								  WHERE IdDepartamento = :IdDepartamento');

	            if ($stmtDep->execute(array(
		            ":IdDepartamento" => $codigo,
		            ":CodDepMeli" => $value->id))) {
		        	
		            echo "<br>se homologo el departamento: ".$name."|".$codigo;

		        	##luego de actualizar el codigo meli de departamento, recorremos las ciudades y las buscamos en simi para actualizar su codigo
		        	$listadoCiudades = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/states/".$value->id);


		        	foreach ($listadoCiudades["body"]->cities as $key => $valueCiudades) {
		        		
		        	$nameCiudad = ''.utf8_decode($valueCiudades->name).'%';

		        		$stmtCiu = $connPDO->prepare('UPDATE ciudad2 SET CodCiuMeli =:CodCiuMeli
								  WHERE IdDepartamento = :IdDepartamento AND  Nombre LIKE  :nombreMeli AND IdCiudad != '.$idbogota);

		        		if ($stmtCiu->execute(array(
			            ":IdDepartamento" => $codigo,
			            ":CodCiuMeli" => $valueCiudades->id,
			            ":nombreMeli" => $nameCiudad))) {

		        		
							echo "-- se homologo la ciudad: ".$nameCiudad."<br>";


							##luego de actualizar el codigo meli de ciudad, recorremos los barrios y los buscamos en simi para actualizar su codigo	excepto bogotá por que es tratado como un departamento
							#
							
							$listadoBarrios = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/cities/".$valueCiudades->id);   

							
		        	
				        	# Actualizamos los barrios de la localidad
				        	foreach ($listadoBarrios["body"]->neighborhoods as $key => $valuelistadoBarriosLocalidadesBogota) {

				        		$nameBarrioLocal = strtolower(trim($valuelistadoBarriosLocalidadesBogota->name));
								$nameBarrioLocal = ''. utf8_decode($nameBarrioLocal).'';
								$nameBarrioLocal = trim($nameBarrioLocal);

								#busqueda de localidad por el id de meli
								
								$stmtLocalsim = $connPDO->prepare("SELECT IdCiudad,Nombre,CodCiuMeli
								   FROM ciudad2
								   WHERE CodCiuMeli  = :CodCiuMeli ");

								if ($stmtLocalsim->execute(array(
					            ":CodCiuMeli" => $valueCiudades->id))) {

					            	//$data = $stmtLocalsim->fetch();
					            	while ($rowLoc = $stmtLocalsim->fetch()) {
					            		$codigoLocal = $rowLoc["IdCiudad"];
					            	}

		  							//$idlocalidadsimi = $data["idlocalidad"];
		  							//echo "idlocalidadsimi: ".$idlocalidadsimi."<br>";
					            	//echo ("Codigo: ".$codigoLocal);
					            }
					            //echo $valueCiudades->id." | ";
					            //echo "codigoLocal:".$codigoLocal."<br>";
					            //exit;

				        		$stmtBar = $connPDO->prepare('UPDATE barrios2 SET idml2 =:idml2, NameBarrMeli = :NameBarrMeli  WHERE NombreB LIKE :NameBarrMeli AND IdCiudad = :IdCiudad ' );

				        		if ($stmtBar->execute(array(
					            ":idml2" => $valuelistadoBarriosLocalidadesBogota->id,
					            ":NameBarrMeli" => $nameBarrioLocal,
					            ":IdCiudad" => $codigoLocal))) {

					            	//echo "---- Homologada El barrio de otras ciudades: ".$nameBarrioLocal;

					            }

				        	}     		
		        			

		        		}else {
				            print_r($stmtCiu->errorInfo());
				        }

		        	}

		        } else {
		            print_r($stmtDep->errorInfo());
		        }


	        }

			


        }
        
        
		
	}

	
}

if($data == 23){

	$connPDO = new Conexion();

	if(isset($_GET["ciudad"])){

		$listadoBarrios = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/cities/".$_GET["ciudad"]);   

							

    	# Actualizamos los barrios de la localidad
    	foreach ($listadoBarrios["body"]->neighborhoods as $key => $valuelistadoBarriosLocalidadesBogota) {

    		$nameBarrioLocal = strtolower(trim($valuelistadoBarriosLocalidadesBogota->name));
			$nameBarrioLocal = ''. utf8_decode($nameBarrioLocal).'';
			$nameBarrioLocal = trim($nameBarrioLocal);

			#busqueda de localidad por el id de meli
			
			$stmtLocalsim = $connPDO->prepare("SELECT IdCiudad,Nombre,CodCiuMeli
			   FROM ciudad2
			   WHERE CodCiuMeli  = :CodCiuMeli ");

			if ($stmtLocalsim->execute(array(
            ":CodCiuMeli" => $_GET["ciudad"]))) {
//echo" entro";
            	//$data = $stmtLocalsim->rowCount();
//print_r($data);
            	while ($rowLoc = $stmtLocalsim->fetch()) {
            		//echo "test";
            		$codigoLocal = $rowLoc["IdCiudad"];
            	}

					//$idlocalidadsimi = $data["idlocalidad"];
					//echo "idlocalidadsimi: ".$idlocalidadsimi."<br>";
            	//echo ("Codigo: ".$codigoLocal);
            }else{
            	print_r($stmtLocalsim->errorInfo());
            }
            echo $valuelistadoBarriosLocalidadesBogota->id." | ";
            echo "codigoLocal:".$codigoLocal."<br>";
            //exit;

    		$stmtBar = $connPDO->prepare('UPDATE barrios2 SET idml2 =:idml2, NameBarrMeli = :NameBarrMeli  WHERE NombreB LIKE :NameBarrMeli AND IdCiudad = :IdCiudad ' );

    		if ($stmtBar->execute(array(
            ":idml2" => $valueLocalidad->id,
            ":NameBarrMeli" => $nameBarrioLocal,
            ":IdCiudad" => $codigoLocal))) {

            	echo "---- Homologada El barrio de otras ciudades: ".$nameBarrioLocal;

            }

    	}  

	}

}

/*********************************
 *              metodo para publicacion de inmueble en meli               *
 *********************************/


if( $data == 24){

	//echo "<pre>";
	//print_r($_SESSION);
	//echo "</pre>";
	if( isset($_GET["inm"]) and isset($_GET["inmob"]) ){

		$valInm= $mercaLibre->getInfoInmueble($_GET["inm"]);

		//echo $title."<br>";
		$params = array('access_token' => $_SESSION['access_token']);
		$data = $mercaLibre->getList("/users/me",$params);

		$dataPlan = $mercaLibre->getList("/users/".$data["body"]->id."/classifieds_promotion_packs",$params);
        
        if( $dataPlan["body"]->status == 404 ){

        	if(!isset($_SESSION["access_token"])){
        		if( isset($_GET["code"]) ){
					$code = "&code=".$_GET['code'];
				}else{
					$code="";
				}
				$_SESSION["referer"] = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".$code;
				//echo '<a href="' . $mercaLibre->getAuthUrl('https://www.simiinmobiliarias.com/models/meli.php', $updatePropiedad["AUTH_URL"]) . '">Login using MercadoLibre oAuth 2.0</a>';
// print_r($_SESSION);
        		// exit;*/
				header('Location: '.$mercaLibre->getAuthUrl('https://simiinmobiliarias.com/models/meli.php?data=1', $agregar["AUTH_URL"]) );
        	}else{

        		$link = $mercaLibre->getLink();
        		echo '<center><img src="../mwc/images/gifs/loading.gif" width="40px" height="40px" id="loading"/></center>';
				
				echo "<div style='color: red; text-align: center'><h4>No cuenta con un plan habilitado o no est&aacute; logueado en su cuenta de mercadolibre . <a href='".$link["AUTH_URL"]."'' >Logueate en Mercadolibre</a> y actualiza est&aacute; p&aacute;gina</h4></div>";		
        	}
        	
        	return false;
        }	

		$propiedad = array(
			"title"=> $valInm["title"],
			//"subtitle"=> $valInm["title"],
			"category_id"=> $valInm["category_id"],//venta, arriendo, arriendo temporal
			"price"=> $valInm["price"],
			"currency_id"=> "COP",
			"available_quantity"=> 1,
			"buying_mode"=> "classified",
			"listing_type_id"=> $dataPlan["body"][0]->listing_details[0]->listing_type_id,//"gold",//gold para venta
			"condition"=> $valInm["condition"],//new, old, not_specified
			"pictures"=> $valInm["imagenes"],
			"seller_contact"=> array(
				"contact"=> $valInm["infoInmobiliaria"][0]["Nombre"],
				"other_info"=> $valInm["infoInmobiliaria"][0]["Direccion"],
				//"area_code"=> "011",
				"phone"=> $valInm["infoInmobiliaria"][0]["Telefonos"],
				//"area_code2"=> "",
				//"phone2"=> "",
				"email"=> $valInm["infoInmobiliaria"][0]["Correo"],
				//"webmail"=> ""
			),
			"location"=> array(
				"address_line"=> $valInm["barrio"],//$valInm["direccion"],
				"zip_code"=> "00000000",
				"neighborhood"=> array(
					"id"=> $valInm["idbarriomeli"]
				),
				"latitude"=> $valInm["latitud"],
				"longitude"=> $valInm["longitud"]
			),
			
			"attributes" =>$valInm["caracteristicas"]["caracteristicassimi"],
			"description"=> $valInm["idInm"]." ".$valInm["descripcionLarga"]
			);

		
		
		/*echo "<pre>";
		print_r(json_encode($propiedad));
		echo "</pre>";
		exit;*/
			
			

			$meliInmu = $inmClass->datosInmueblePublicaciones($_GET["inmob"],$_GET["inm"],2);

			if(empty($meliInmu[0]["cod_dtp"])){
				$agregar=$mercaLibre->addProperty("https://api.mercadolibre.com/items",$propiedad);
			}else{

				$url = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?data=29&inm=".$_GET["inm"]."&inmob=".$_GET["inmob"]."&code=".$_GET["code"];
			
				header('Location: '.$url );
				return false;

			}

			/*echo "<pre>";
			print_r($agregar);
			echo "</pre>";*/
			
			
			if($agregar["httpCode"] == 201){
				
				$inmClass->updateInmueblesPortalesNew($_GET["inm"],$_GET["inmob"],2,$agregar["body"]->id,0,0,0,0,$agregar["body"]->permalink);
				$updateMelOld = $inmClass->updateMeliOld($_GET["inm"],$_GET["inmob"],2,$agregar["body"]->id,"Publicado",$agregar["body"]->permalink);

			}

			if($agregar["httpCode"] == 400){
				
				echo $agregar["body"]->message;
				
				//echo $agregar["body"]["message"];
			}

			if( isset($_SESSION["referer"]) ){

				if(!empty($meliInmu[0]["cod_dtp"])){

					$url = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?data=29&inm=".$_GET["inm"]."&inmob=".$_GET["inmob"]."&code=".$_GET["code"];
			
					header('Location: '.$url );
					return false;

				}

				unset($_SESSION["referer"]);
			}

		
		
		//print_r($valInm);
		if( isset($agregar["login"])  ){
			//echo "debe loguearse";
			//return array("login"=>1,"AUTH_URL"=>Meli::$AUTH_URL['MLB']);
			//echo $mercaLibre->getAuthUrl('https://www.simiinmobiliarias.com/models/meli.php', $updatePropiedad["AUTH_URL"]);
			//header('Location: '.$mercaLibre->getAuthUrl('https://www.simiinmobiliarias.com/models/meli.php', $updatePropiedad["AUTH_URL"]) );
			//$_SESSION["referer"]="1";//equivale el return hacia el update
			if( isset($_GET["code"]) ){
				$code = "&code=".$_GET['code'];
			}else{
				$code="";
			}
			$_SESSION["referer"] = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".$code;
			//echo '<a href="' . $mercaLibre->getAuthUrl('https://www.simiinmobiliarias.com/models/meli.php', $updatePropiedad["AUTH_URL"]) . '">Login using MercadoLibre oAuth 2.0</a>';
			header('Location: '.$mercaLibre->getAuthUrl('https://simiinmobiliarias.com/models/meli.php?data=1', $agregar["AUTH_URL"]) );
		}else{

			
			if($agregar["httpCode"] == 201){
				echo '<center><img src="../mwc/images/gifs/loading.gif" width="40px" height="40px" id="loading"/></center>';
				
				echo "<div style='color: green; text-align: center'><h3>Inmueble <strong>".$_GET["inmob"]."-".$_GET["inm"]."</strong> Publicado con &Eacute;xito en Mercadolibre</h3>";
				if( $valInm["idGestion"] == 2 ){
					echo "<h4>Fue publicado en arriendo, porfavor duplique el inmueble y crealo en ´tipo de gestion´ venta y vuelva a publicarlo</h4>";
				}	
				echo "</div>";
				echo '<script type="text/javascript">
				document.getElementById("loading").style.display = "none"; 
				setTimeout(function(){ window.close(); }, 3000);

				</script>';
			}else{
				
				if($agregar["body"]->status == 403){

					header('Location: https://simiinmobiliarias.com/models/meli.php?data=24&inm='.$_GET["inm"].'&inmob='.$_GET["inmob"] );
					echo '<center><img src="../mwc/images/gifs/loading.gif" width="40px" height="40px" id="loading"/></center>';
				

				}else{
					/*echo "<pre>";
					print_r($agregar["body"]);
					echo "</pre>";

					echo "<pre>";
					print_r($propiedad);
					echo "</pre>";*/
					
					
					
					
					//print_r($agregar["body"]);	
				}
			}

			
		}
			/*echo "<pre>";
			print_r($propiedad);
			print_r($valInm);
			print_r($agregar);

			echo "</pre>";*/
			/*echo "Id de mercadolibre es: ".$agregar["body"]->id;*/


	}

}

/*********************************
 *              homologacion barrios por ciudad meli               *
 *********************************/
if( $data == 25 ){

	$connPDO = new Conexion();

	if( $_GET["localidad"] ){


		$listadoBarriosLocalidadesBogota = $mercaLibre->getList("https://api.mercadolibre.com/classified_locations/cities/".$_GET["localidad"]
		        		);
		        	
		# Actualizamos los barrios de la localidad
		foreach ($listadoBarriosLocalidadesBogota["body"]->neighborhoods as $key => $valuelistadoBarriosLocalidadesBogota) {

			$nameBarrioLocal = strtolower(trim($valuelistadoBarriosLocalidadesBogota->name));
			$nameBarrioLocal = ''. utf8_decode($nameBarrioLocal).'';
			$nameBarrioLocal = trim($nameBarrioLocal);

			$nameBarrioLocalQuery = ''.$nameBarrioLocal.'%';

			echo "---- Homologada El barrio de bogota: ".$nameBarrioLocal."<br>";
			$nameBarrioLocal = str_replace("-","",$nameBarrioLocal);


			#busqueda de localidad por el id de meli
			
			$stmtLocalsim = $connPDO->prepare("SELECT IdBarrios,IdCiudad,IdLocalidad,NombreB,idml2
			   FROM barrios2
			   WHERE NombreB  LIKE :nombre ");

			if ($stmtLocalsim->execute(array(
	        ":nombre" => $nameBarrioLocalQuery))) {//$valueLocalidad->id

	        	//$data = $stmtLocalsim->fetch();
	        	while ($rowLoc = $stmtLocalsim->fetch()) {
	        		$codigoclocal = $rowLoc["IdLocalidad"];
	        	}

					//$idlocalidadsimi = $data["idlocalidad"];
					//echo "idlocalidadsimi: ".$idlocalidadsimi."<br>";
	        	//echo ("Codigo: ".$codigoLocal);
	        }

	        echo "codigociudad: ".$codigoclocal."<br>";

	        //exit;

			$stmtBar = $connPDO->prepare('UPDATE barrios2 SET idml2 =:idml2, NameBarrMeli = :NameBarrMeli  WHERE NombreB LIKE :NameBarrMeli AND IdLocalidad = :IdLocalidad ' );

			if ($stmtBar->execute(array(
	        ":idml2" => $valuelistadoBarriosLocalidadesBogota->id,
	        ":NameBarrMeli" => $nameBarrioLocal,
	        ":IdLocalidad" => $codigoclocal))) {

	        	

	        }

		}


	}

	

}

/*********************************
 *              funcion test de caracteristicas               *
 *********************************/

if( $data == 26 ){

	$data=array("codinm"=>14892,"habitaciones"=>1,"metros"=>20,"edad"=>3,"banos"=>2,"tipoInmueble"=>"apartamento","tipogestion"=>"arriendo");

	$mercaLibre->validarCaracteristica2($data);

}

/*********************************
 *              retornamos las caracteristicas por tipo de inmueble               *
 *********************************/
if($data==27)
{

	//listado de caracteristicas de tipo de inmueble
	//$caracteristicas = $mercaLibre->getList("https://api.mercadolibre.com/categories/".$tipogestionmeli."/attributes");

	$caracteristicas = $mercaLibre->getAllCaracteristicasTipoInmueble($_POST["tipoInmueble"]);
	$response["0"]=$inmClass->getCaracteristicasTipoInmueble($_POST);
	$response["1"]=$caracteristicas;
	echo json_encode($response);
}

/*********************************
 *              actualizamos meli en simi               *
 *********************************/
if($data==28)
{
	$connPDO = new Conexion();
	$stmt= $connPDO->prepare('UPDATE maestrodecaracteristicasnvo SET CodCarMeli =:CodCarMeli, NameCarMeli = :NameCarMeli
		WHERE idCaracteristica = :idCaracteristica');
    if ($stmt->execute(array(
        ":CodCarMeli" => $_POST["meli"],
        ":NameCarMeli"  => $_POST["name"],
        ":idCaracteristica" => $_POST["simi"]))) {

       echo "Homologado";
    } else {
        print_r($stmt->errorInfo());
    }
}

/*********************************
 *              actualizamos la descripcion del inmueble en meli               *
 *********************************/
if($data==29){
	if(isset($_GET["inm"]) and isset($_GET["inmob"]) ){

		//echo $_GET["inm"];
		//actualizamos el id de mercadolibre del inmueble
		$data=array();
		$data["codinm"]=$_GET["inm"];
		$data["IdInmobiliaria"]=$_GET["inmob"];
		// 2 = mercadolibre
		
		$dataInm = $inmClass->getDataFinancialInmueble($data);
		/*echo "<pre>";
		print_r($dataInm);
		echo "</pre>";
exit;*/
		$dataInmPublicacion = $inmClass->datosInmueblePublicaciones($_GET["inmob"],$_GET["inm"],2);
		//MLB12343412
		$body = array('text' => $dataInm[0]["descripcionlarga"]);

		if( isset($_GET["melicod"]) ){
			$dataInmPublicacion[0]["cod_dtp"] = $_GET["melicod"];
		}

		$updateDescripcion = $mercaLibre->putDataDescription($body,$dataInmPublicacion[0]["cod_dtp"]);

		/*echo "<pre>";
print_r($dataInmPublicacion);
echo "</pre>";*/

		$valInm= $mercaLibre->getInfoInmueble($_GET["inm"]);

		$propiedad = array(
			"title"=> $valInm["title"],
			//"subtitle"=> $valInm["title"],
			"price"=> $valInm["price"],
			"currency_id"=> "COP",
			"available_quantity"=> 1,
			"buying_mode"=> "classified",
			"condition"=> $valInm["condition"],//new, old, not_specified
			"pictures"=> $valInm["imagenes"],
			"seller_contact"=> array(
				"contact"=> $valInm["infoInmobiliaria"][0]["Nombre"],
				"other_info"=> $valInm["infoInmobiliaria"][0]["Direccion"],
				//"area_code"=> "011",
				"phone"=> $valInm["infoInmobiliaria"][0]["Telefonos"],
				//"area_code2"=> "",
				//"phone2"=> "",
				"email"=> $valInm["infoInmobiliaria"][0]["Correo"],
				//"webmail"=> ""
			),
			"location"=> array(
				"address_line"=> $valInm["barrio"],//$valInm["direccion"],
				"zip_code"=> "00000000",
				"neighborhood"=> array(
					"id"=> $valInm["idbarriomeli"]
				),
				"latitude"=> $valInm["latitud"],
				"longitude"=> $valInm["longitud"]
			),
			
			"attributes" =>$valInm["caracteristicas"]["caracteristicassimi"]
			);
		$updatePropiedad = $mercaLibre->putData($propiedad,$dataInmPublicacion[0]["cod_dtp"]);

		/*echo $dataInmPublicacion[0]["cod_dtp"];
		echo "<pre>";
		print_r($updateDescripcion);
		echo "</pre>";*/

		/*echo "<pre>";
		print_r($updatePropiedad);
		echo "</pre>";

		echo "Inmueble actualizado con exito";*/
		
		if( isset($_SESSION["referer"]) ){
			unset($_SESSION["referer"]);
		}


		

		if( isset($updatePropiedad["login"])  ){
			//echo "debe loguearse";
			//return array("login"=>1,"AUTH_URL"=>Meli::$AUTH_URL['MLB']);
			//echo $mercaLibre->getAuthUrl('https://www.simiinmobiliarias.com/models/meli.php', $updatePropiedad["AUTH_URL"]);
			//header('Location: '.$mercaLibre->getAuthUrl('https://www.simiinmobiliarias.com/models/meli.php', $updatePropiedad["AUTH_URL"]) );
			//$_SESSION["referer"]="1";//equivale el return hacia el update
			if( isset($_GET["code"]) ){
				$code = "&code=".$_GET['code'];
			}else{
				$code="";
			}
			$_SESSION["referer"] = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".$code;
			//echo '<a href="' . $mercaLibre->getAuthUrl('https://www.simiinmobiliarias.com/models/meli.php', $updatePropiedad["AUTH_URL"]) . '">Login using MercadoLibre oAuth 2.0</a>';
			header('Location: '.$mercaLibre->getAuthUrl('https://simiinmobiliarias.com/models/meli.php?data=1', $updatePropiedad["AUTH_URL"]) );
		}else{

			
			if($updatePropiedad["httpCode"] == 200){
				echo '<center><img src="../mwc/images/gifs/loading.gif" width="40px" height="40px" id="loading"/></center>';
				
				echo "<div style='color: green; text-align: center'><h3>Inmueble <strong>".$_GET["inmob"]."-".$_GET["inm"]."</strong> Actualizado con Éxito  en Mercadolibre </h3></div>";	
				echo '<script type="text/javascript">
				document.getElementById("loading").style.display = "none"; 
				setTimeout(function(){ window.close(); }, 3000);
				</script>';
			}else{
				
				if ( strpos( strtolower( $updatePropiedad["body"]->message ), 'status:closed, has_bids:false') !== false) {
			
				if( !isset($_SESSION["republi"]) ){
					$_SESSION["republi"] = 1;
					header('Location: https://simiinmobiliarias.com/models/meli.php?data=31'.'&inmob='.$_GET["inmob"].'&inm='.$_GET["inm"].'&meli='.$valInm["Rmeli"]);
					echo '<center><img src="../mwc/images/gifs/loading.gif" width="40px" height="40px" id="loading"/></center>';
				}else{
					unset($_SESSION["republi"]);
					echo"<h2>Error al intentar republicar el inmueble</h2>";
				}
				


			}
			}
			
		}


	}
}

if($data==30){
	$connPDO = new Conexion();
	//listado principal de categorias
	$categoriaprincipal=$mercaLibre->getList("https://api.mercadolibre.com/sites/MCO/categories");
	//tipo gestion
	$tipogestionsimi = array("arriendo","venta");

	foreach ($categoriaprincipal["body"] as $key => $value) {
		
		//validamos si inmueble esta dentro del array lo cual nos permitira elegir el id correspondiente inmueble categoria padre
		if ( strpos( strtolower( $value->name ), 'inmueble') !== false) {
		    $inmuebleidmeli=$value->id;
		}

	}

	$categoriapadre = $inmuebleidmeli;//"MCO1459-INMUEBLE";//inmuebles

	//listado de las categorias hijas de inmuebles (estás son el tipo de inmueble)
	$listadoTipoInmueble=$mercaLibre->getList("https://api.mercadolibre.com/categories/".$categoriapadre);

	

	$tipoInmueble = $inmClass->getTipoInmuebles();

	foreach ($tipogestionsimi as $key => $valuetipogestionsimi) {
		

		//recorremos el listado de tipos de inmueble de simi
		foreach ($tipoInmueble as $key => $valueTipoInmueble) {
			//recorremos los tipo de inmueble de meli para compararlo
			foreach ($listadoTipoInmueble["body"]->children_categories as $key => $value) {

				$nameMeli =  strtolower($value->name);
				$nameSimi = strtolower($valueTipoInmueble["Descripcion"]);
				//validamos si es igual
				
				if ( strpos( strtolower( $nameMeli ), $nameSimi) !== false) {
					//echo $value->id." ".$value->name."<br>";

					//listado de tipo de gestion
					$tipogestion = $mercaLibre->getList("https://api.mercadolibre.com/categories/".$value->id);

					
						
					foreach ($tipogestion["body"]->children_categories as $key => $valueTipoGestion) {
						//elegimos el tipo de gestion
						if( strtolower( $valueTipoGestion->name ) == strtolower( $valuetipogestionsimi ) ){
						

							$tipogestionmeli = $value->id;

							$caracteristicas = $mercaLibre->getList("https://api.mercadolibre.com/categories/".$tipogestionmeli."/attributes");

							foreach ($caracteristicas["body"] as $key => $valueCaracteristicas) {


								 $stmt= $connPDO->prepare('UPDATE maestrodecaracteristicasnvo SET CodCarMeli =:CodCarMeli
									WHERE NameCarMeli = :NameCarMeli AND idCaracteristica =  :idCaracteristica');
						        if ($stmt->execute(array(
						            ":NameCarMeli"  => utf8_decode($valueCaracteristicas->name),
						            ":CodCarMeli"	=> $valueCaracteristicas->id,
						            "idCaracteristica" => $valueTipoInmueble["idTipoInmueble"]))) {

						        	echo "Codigo de ".$valueCaracteristicas->name." actualizado <br>";

						          
						        } else {
						            print_r($stmt->errorInfo());
						        }

							}

						}


						
						
					}

					
					

				}
				
			}
		
		}

	}

	/*echo "<pre>";
	print_r($tipoInmueble);
	echo "</pre>";

	echo "<pre>";
	print_r($listadoTipoInmueble);
	echo "</pre>";*/
	

}

/*********************************
	 *       METODO PARA VERFICIAR Y PARA  REPUBLICACION DE INMUEBLE EN MELI REPUBLICACION, VIA GET RECIBE
	 *       inm = id inmueble
     *    	 inmob = id inmobiliaria
*********************************/

if($data==31){
	//vericar estado de inmueble meli
	
	if( isset($_GET["inmob"]) and isset($_GET["inm"])){

		$dataInmPublicacion = $inmClass->datosInmueblePublicaciones($_GET["inmob"],$_GET["inm"],2);
		
		
		$meli = str_replace("-","",$_GET["meli"]);
		$inmuebleMeli=$mercaLibre->getList("https://api.mercadolibre.com/items/".$dataInmPublicacion[0]["cod_dtp"]);

		if( $inmuebleMeli["body"]->status == "closed" ){
			
			$valInm= $mercaLibre->getInfoInmueble($_GET["inm"]);

			$propiedad = array(
				"price"=> (int)$valInm["price"],				
				"quantity" => 1,
				"listing_type_id" => "gold"
				);

			//echo $valInm["price"];

			//echo json_encode($propiedad);

			//print_r($propiedad);

			$inmuebleMeli= $mercaLibre->republicInmueble($dataInmPublicacion[0]["cod_dtp"],$propiedad);
			//print_r($inmuebleMeli);
			//print_r($inmuebleMeli);
			$inmClass->updateMeli($_GET["inm"],$_GET["inmob"],2,$dataInmPublicacion[0]["cod_dtp"],"Publicado",$inmuebleMeli["body"]->permalink);
			//$inmClass->updateMeliOld($_GET["inm"],$_GET["inmob"],2,$dataInmPublicacion[0]["cod_dtp"],"Publicado");
			if($inmuebleMeli["httpCode"] == 201){
				//echo "Inmueble Republicado con Éxito";
				echo "<div style='color: green; text-align: center'><h3>Inmueble <strong>".$_GET["inmob"]."-".$_GET["inm"]."</strong> Republicado con Éxito</h3></div>";	
				echo '<script type="text/javascript">
				document.getElementById("loading").style.display = "none"; 
				setTimeout(function(){ window.close(); }, 3000);
				</script>';
			}


			if( isset($inmuebleMeli["login"])  ){
		
				if( isset($_GET["code"]) ){
					$code = "&code=".$_GET['code'];
				}else{
					$code="";
				}
				$_SESSION["referer"] = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".$code;
				
				header('Location: '.$mercaLibre->getAuthUrl('https://simiinmobiliarias.com/models/meli.php?data=1', $updatePropiedad["AUTH_URL"]) );
			}


			//$inmuebleMeli["body"]->id
			//print_r($inmuebleMeli);
		}



	}

}

/*********************************
	 *       METODO PARA  CAMBIAR DE ESTADO INMUEBLE, VIA GET RECIBE
	 *       inm = id inmueble
     *    	 inmob = id inmobiliaria
     *    	 status = closed, paused, active
*********************************/
if($data==32){

	if(isset($_GET["inmob"]) AND isset($_GET["inm"]) AND isset($_GET["status"])){

		//exit;
		$propiedad = array(
			"status"=> $_GET["status"]);
		$dataInmPublicacion = $inmClass->datosInmueblePublicaciones($_GET["inmob"],$_GET["inm"],2);

		$updateState = $mercaLibre->putData($propiedad,$dataInmPublicacion[0]["cod_dtp"]);
		//print_r($updateState);
		if($updateState["httpCode"] == 201){	
			echo "Se cambio el estado del inmueble satisfactoriamente";
		}

	}

	


}

/*********************************
 *              eliminar publicacion  de meli final             *
 *********************************/

if( $data == 33 ){

	
	$dataInmPublicacion = $inmClass->datosInmueblePublicaciones($_GET["inmob"],$_GET["inm"],2);
	$arg=array("deleted"=>"true");
	//print_r($dataInmPublicacion[0]["cod_dtp"]);
	$propiedad = array(
			"status"=> "closed");
	
	$mercaLibre->putData($propiedad,$dataInmPublicacion[0]["cod_dtp"]);

	$delete = $mercaLibre->deleteProperty("https://api.mercadolibre.com/items/".$dataInmPublicacion[0]["cod_dtp"],$arg);
	
	//print_r($delete);
	
	//$inmClass->updateMeliOld($_GET["inm"],$_GET["inmob"],2,"","Por Publicar");

	
	
	if( isset($_SESSION["referer"]) ){
				
			unset($_SESSION["referer"]);
	}

		
		

	if( isset($delete["login"])  ){
		//echo "debe loguearse";
		//return array("login"=>1,"AUTH_URL"=>Meli::$AUTH_URL['MLB']);
		//echo $mercaLibre->getAuthUrl('https://simiinmobiliarias.com/models/meli.php', $updatePropiedad["AUTH_URL"]);
		//header('Location: '.$mercaLibre->getAuthUrl('https://simiinmobiliarias.com/models/meli.php', $updatePropiedad["AUTH_URL"]) );
		//$_SESSION["referer"]="1";//equivale el return hacia el update
		if( isset($_GET["code"]) ){
			$code = "&code=".$_GET['code'];
		}else{
			$code="";
		}
		$_SESSION["referer"] = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".$code;
		//echo '<a href="' . $mercaLibre->getAuthUrl('https://simiinmobiliarias.com/models/meli.php', $updatePropiedad["AUTH_URL"]) . '">Login using MercadoLibre oAuth 2.0</a>';
		header('Location: '.$mercaLibre->getAuthUrl('https://simiinmobiliarias.com/models/meli.php?data=1', $updatePropiedad["AUTH_URL"]) );
	}else{

		echo '<center><img src="../mcomercialweb/images/logo_mercadolibre.png"><img src="../mwc/images/gifs/loading.gif" width="40px" height="40px" id="loading"/></center>';
		
		if($delete["httpCode"] == 200){
			$inmClass->updateMeli($_GET["inm"],$_GET["inmob"],2,"","Por Publicar","");
			echo "<div style='color: green; text-align: center'><h3>Inmueble <strong>".$_GET["inmob"]."-".$_GET["inm"]."</strong> Despublicado con Éxito</h3></div>";	
			echo '<script type="text/javascript">
			document.getElementById("loading").style.display = "none"; 
			setTimeout(function(){ window.close(); }, 3000);
			</script>';
		}else{

			print_r($updatePropiedad["body"]->message);	
		}
		
	}
	

}


if($data == 34){

	//obtenemos los datos del usuario
	//https://api.mercadolibre.com/users/me?access_token=$ACCESS_TOKEN
	//$arg=array("access_token");
	$params = array('access_token' => $_SESSION['access_token']);
	$data = $mercaLibre->getList("/users/me",$params);

	//optenemos el plan de la inmobiliaria curl -X GET https://api.mercadolibre.com/users/{user_id}/classifieds_promotion_packs?access_token=$ACCESS_TOKEN;

	$params = array('access_token' => $_SESSION['access_token']);
	$dataPlan = $mercaLibre->getList("/users/".$data["body"]->id."/classifieds_promotion_packs",$params);
	echo "<pre>";
	print_r($dataPlan);
	echo "</pre>";
	echo "entro";
}

if($data == 35){

}


?>