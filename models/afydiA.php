<?php
    session_start();
    error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors', '1');
    include("../funciones/fncfields2.php");
    include("../funciones/fncfields3.php");
    include("../funciones/cge00001.php");
    include("../funciones/myfncs.php");
    include("../funciones/conni.php");
    include("../controllers/class.inmuebles.php");
    include("../controllers/class.afydi.php");
    
    $w_conexion	 	= new MySQL();
    $apiAfydi		= new Afydi($conn);
    $data 			= $_GET['data'];
    $usr_act		= $_SESSION['Id_Usuarios'];
    $codInmu        = "1-14810";
    $datasw=$apiAfydi->listadoInmueblesAfydi(1,$codInmu,$usr_act);
    $codAfydi = getCampo("inmuebles","where idInm='".$codInmu."'","RAfydi",0); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>

   <!--Core CSS -->
    <link href="../mwc/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="../mwc/css/bootstrap-reset.css" rel="stylesheet">
    <link href="../mwc/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <style type="text/css">
      body{
        background-color: #D5D5D5;
      }
      .errors
      {
        margin-left: 15px;
      }
      .cont-afydi
      {
        margin-top:50px;
      }
      .font-h3
      {
        color: white;
        font-weight: bold;
      }
    </style>
</head>
<body>
<div class="container">
  <div class="col-sm-12 cont-afydi">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title text-center font-h3">Sincroonizaci&oacute;n Afydi</h3>
      </div>
      <div class="panel-body">
        <div class="col-sm-12">

        <?php
            if($codAfydi == ""){
                $result=$apiAfydi->insertInmueble($datasw);
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
                if($result['errors']> 0)
                {
                    echo "<legend class='alert alert-danger'>Revise los sisguientes Errores</legend>";
                }
                else
                {
                    // $response=$apiAfydi->getInmueble('227345');
                    foreach ($result as $key => $value) {
                       $afydi = $value[0]['idpro'];
                       $resultU=$apiAfydi->updateCodeAfydi($codInmu,$afydi);

                    }
                    if($resultU == 1)
                    {
                        echo "<legend class='alert alert-success'>Se Sincronizo Correctamente Codigo ".$afydi."</legend>";
                    }
                    else
                    {
                        echo "<legend class='alert alert-danger'>No se actualizo Codigo Domus en Simi ".$afydi."</legend>";
                    }
                }
        ?>
                
        <?php
                foreach ($result as $key) {
                    if($key["codpro"])
                    {
        ?>
            <div class="col-sm-4">            
            <h4>
                <span class="label label-danger">
                    <?php echo $key["codpro"] ?>
                </span>
            </h4>
            </div>
        <?php               
                    }
                    else
                    {

                        foreach ($key as $keys => $value) {             
        ?>
            <div class="col-sm-4">            
            <h4>
                <span class="label label-danger">
                    <?php echo $value[0] ?>
                </span>
            </h4>
            </div>
        <?php            }
                    }
                }
                
            }
            else
            {
                // $response=$apiAfydi->getInmueble('227345');
                $result=$apiAfydi->updateInmueble($datasw); 
                $afydi=$result['id'];
                 if($result['errors']> 0)
                 {
                     echo "<div class='alert alert-danger'>Revise los sisguientes Errores</div>";
                               foreach ($result as $key) {
                 foreach ($key as $keys => $value) {             
        ?>
                <div class="col-sm-4">            
                <h4>
                    <span class="label label-danger">
                        <?php echo $value[0] ?>
                    </span>
                </h4>
                </div>
        <?php            }
                    }   
                 }
                 else
                 {
                     echo "<legend class='alert alert-success'>Se Actualizo Correctamente ".$afydi."</legend>";
                 }


       
                
            }
        ?>
        </div>
      </div>
    </div>
  </div>
</div> 
