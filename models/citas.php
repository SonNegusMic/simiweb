<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
header('Content-Type: text/html; charset=utf-8');
include("../funciones/fncfields2.php");
include("../funciones/fncfields3.php");
include("../funciones/cge00001.php");
include("../funciones/myfncs.php");
include("../funciones/conni.php");
include("../funciones/connPDO.php");
include("../controllers/class.citas.php");
include("../controllers/class.mailCitasDanny.php");
$w_conexion	 	= new MySQL();
$pdo= new Conexion();
$citas=new Citas($w_conexion,$pdo);
$mailCitas=new mailCitas($w_conexion,$pdo);
$data=$_GET['data'];
if($data==1)
{
	// echo "<pre>";
	// print_r($_POST);
	// echo "</pre>";
	$response=$citas->getCitas($_POST);

	echo json_encode($response);
}
if($data==2)
{
	
	$response=$citas->saveCliente($_POST);

	echo json_encode($response);
}
if($data==3)
{
	
	$response=$citas->getRetroCita($_POST);

	echo json_encode($response);
}
if($data==4)
{
	
	if($_POST['idcita']>0)
	{
		$response=$citas->updateCita($_POST);

		if($response>0)
		{

			$response=$mailCitas->envioMailCitaNew($_POST);

		}else
		{
			 $response=0;
		}
	}
	else
	{
		$response=$citas->saveCita($_POST);	
		//$response="1";
	}

	echo json_encode($response);
}
if($data==5)
{
	$response=$citas->saveRetroCita($_POST);

	if($response==1)
	{
		if(!empty($_POST["closeCita"]) or $_POST["cerrarCita"] == 1)
		{
			$response=$citas->closeCita($_POST);
			$response=$response;
		}
		else
		{
			$response=1;
		}

	}else
	{
		$response= 0;
	}
	if($response)
	{
		echo json_encode($response);
	}
}
if($data==6)
{
	$response=$citas->getCitasId($_POST);

	echo json_encode($response);
}

if($data==7)
{
	$response=$citas->getDaysEnabled($_POST);

	echo json_encode($response);
}
if($data==8)
{
	$response=$citas->exitoCita($_POST);

	echo json_encode($response);
}

if($data==9){

	
	$response=$citas->logCita($_POST);

	echo json_encode($response);

}


?>