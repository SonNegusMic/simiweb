<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
header('Content-Type: text/html; charset=utf-8');
include("../funciones/fncfields2.php");
include("../funciones/fncfields3.php");
include("../funciones/cge00001.php");
include("../funciones/myfncs.php");
include("../funciones/conni.php");
include("../funciones/connPDO.php");
include("../controllers/class.callCenter.php");
require('../mwc/inmuebles/PHPExcel/PHPExcel.php');
$w_conexion	 	= new MySQL();
$pdo= new Conexion();
$call=new CallCenter($pdo);
$objPHPExcel = new PHPExcel;

if( isset($_GET["report"]) ){

	$responseCitas = $call->getHistorialCitas($_GET);

	$responseDemanda = $call->getHistorialDemanda($_GET);


	$objPHPExcel->getProperties()->setCreator("Codedrinks") //Autor
			 ->setLastModifiedBy("Codedrinks") 
			 //Ultimo usuario que lo modificó
			 ->setTitle("Reporte Excel historico de cliente")
			 ->setSubject("Reporte Excel historico de cliente")
			 ->setDescription("Reporte de historico cliente")
			 ->setKeywords("reporte de historico cliente")
			 ->setCategory("Reporte excel");
	$tituloReporteDemanda = "HISTORICO DEMANDA CLIENTE";

	$tituloReporteCitas = "HISTORICO CITAS CLIENTE";

	$titulosColumnas = array(		
	'Ciudad', 
	'Gestion',
	'Tipo Inmueble',
	'Destinacion',
	'Valor Final',
	'Valor Inicial',
	'Fecha',
	'Descripcion',
	
	);

	$titulosColumnasCitas = array(
	'Fecha',
	'Hora',
	'Inmueble',
	'Atendió',
	);

	$objPHPExcel->setActiveSheetIndex(0)
    ->mergeCells('A1:H1')
    ->mergeCells('A2:H2');

    // Se agregan los titulos del reporte
    $objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1',$tituloReporteDemanda)
	    ->setCellValue('A3',  $titulosColumnas[0])
        ->setCellValue('B3',  $titulosColumnas[1])
	    ->setCellValue('C3',  $titulosColumnas[2])
		->setCellValue('D3',  $titulosColumnas[3])
		->setCellValue('E3',  $titulosColumnas[4])
		->setCellValue('F3',  $titulosColumnas[5])
		->setCellValue('G3',  $titulosColumnas[6])
		->setCellValue('H3',  $titulosColumnas[7]);

	$i=4;
	if(count($responseDemanda["data"])>0){
		foreach ($responseDemanda["data"] as $key => $value) {

			

			$objPHPExcel->setActiveSheetIndex(0)
	    		    ->setCellValue('A'.$i,  utf8_encode($value["ciudad"]))
		            ->setCellValue('B'.$i,  utf8_encode($value["NombresGestion"]))
		            ->setCellValue('C'.$i,  utf8_encode($value["tipo"]))
	        		->setCellValue('D'.$i,  utf8_encode($value["Nombre"]))
	    		    ->setCellValue('E'.$i,  utf8_encode($value["ValFinal"]))
	        		->setCellValue('F'.$i,  utf8_encode($value["ValInicial"]))
		            ->setCellValue('G'.$i,  utf8_encode($value["FechaD"]))
		            ->setCellValue('H'.$i,  utf8_encode($value["Descripcion"]));

		            if($i%2 == 0)
				    {
					$objPHPExcel->getActiveSheet()
								->getStyle("A$i:H$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFE0F2FF');

				    }
	        		$i++;

		}
	}
	/*para listado de citas*/
	$nuevoTituloNumber =$i+2;
	$nuevoTituloNumber2 =$nuevoTituloNumber+1;
	$nuevoTituloNumber3 =$nuevoTituloNumber2+1;
	$objPHPExcel->setActiveSheetIndex(0)
    ->mergeCells('A'.$nuevoTituloNumber.':D'.$nuevoTituloNumber)
    ->mergeCells('A'.$nuevoTituloNumber2.':D'.$nuevoTituloNumber2);

	$objPHPExcel->setActiveSheetIndex(0)
	            ->setCellValue('A'.$nuevoTituloNumber,  $tituloReporteCitas);
	
		$i = $nuevoTituloNumber3 +1;
	
	if(count($responseCitas["data"])>0){

		foreach ($responseCitas["data"] as $key => $value) {

			

			$objPHPExcel->setActiveSheetIndex(0)
	    		    ->setCellValue('A'.$i,  utf8_encode($value["Fecha"]))
		            ->setCellValue('B'.$i,  utf8_encode($value["Hora"]))
		            ->setCellValue('C'.$i,  utf8_encode($value["id_inmueble"]))
	        		->setCellValue('D'.$i,  utf8_encode($value["Nombres"]));

		            if($i%2 == 0)
				    {
					$objPHPExcel->getActiveSheet()
								->getStyle("A$i:D$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFE0F2FF');

				    }
	        		$i++;

		}
	}

	$estiloTituloReporte = array(
	'font' => array(
		'name'      => 'Verdana',
	    'bold'      => true,
	    'italic'    => false,
	    'strike'    => false,
	   	'size' =>10,
	    	'color'     => array(
	        	'rgb' => 'FFFFFF'
	       	)
	),
	'fill' => array(
		'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
		'color'	=> array('argb' => 'FF220835')
	),
	'borders' => array(
	   	'allborders' => array(
	    	'style' => PHPExcel_Style_Border::BORDER_NONE                    
	   	)
	), 
	'alignment' =>  array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'rotation'   => 0,
			'wrap'          => TRUE
	)
	);

	$estiloTituloColumnas = array(
	'font' => array(
	    'name'      => 'Arial',
	    'bold'      => true, 
	    'size' 		=>7,                         
	    'color'     => array(
	        'rgb' => '000000'
	    )
	),
	'fill' 	=> array(
		'type'		=> PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
		'rotation'   => 90,
		'startcolor' => array(
			'rgb' => 'c47cf2'
		),
		'endcolor'   => array(
			'argb' => 'FF431a5d'
		)
	),
	'borders' => array(
		'top'     => array(
	        'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	        'color' => array(
	            'rgb' => '143860'
	        )
	    ),
	    'bottom'     => array(
	        'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	        'color' => array(
	            'rgb' => '143860'
	        )
	    )
	),
	'alignment' =>  array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			'wrap'          => TRUE
	));

	$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($estiloTituloReporte);
	$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->applyFromArray($estiloTituloColumnas);	
	/*para listado de citas*/
	$objPHPExcel->getActiveSheet()->getStyle('A'.$nuevoTituloNumber.':D'.$nuevoTituloNumber)->applyFromArray($estiloTituloReporte);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$nuevoTituloNumber2.':D'.$nuevoTituloNumber2)->applyFromArray($estiloTituloColumnas);	

	for($i = 'A'; $i <= 'H'; $i++){
		$objPHPExcel->setActiveSheetIndex(0)			
			->getColumnDimension($i)->setAutoSize(TRUE);
	}

	// Se agregan los titulos del reporte
    $objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$nuevoTituloNumber,$tituloReporteCitas)
	    ->setCellValue('A'.$nuevoTituloNumber3,  $titulosColumnasCitas[0])
        ->setCellValue('B'.$nuevoTituloNumber3,  $titulosColumnasCitas[1])
	    ->setCellValue('C'.$nuevoTituloNumber3,  $titulosColumnasCitas[2])
		->setCellValue('D'.$nuevoTituloNumber3,  $titulosColumnasCitas[3]);

	// Se asigna el nombre a la hoja
	$objPHPExcel->getActiveSheet()->setTitle('Citas');

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
/*echo "<pre>";
print_r($_GET);
echo "</pre>";
echo "<pre>";
print_r($responseDemanda);
echo "</pre>";
echo "entro";
exit;*/

 	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment;filename='historico".$_SESSION["IdInmmo"].date("Y-m-d hh:mm:ss").".xls'");
	header("Cache-Control: max-age=0");

	$objWriter->save('php://output');


}