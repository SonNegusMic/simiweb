<?php
//exit();
session_start();
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');
include("../funciones/fncfields2.php");
include("../funciones/fncfields3.php");
include("../funciones/cge00001.php");
include("../funciones/myfncs.php");
include("../funciones/conni.php");

include("../controllers/class.callCenter.php");
@include("../funciones/connPDO.php");
include('../mcomercialweb/PHPMailer_5.2.2/class.phpmailer.php');
include('../mcomercialweb/PHPMailer_5.2.2/class.smtp.php');
include('../controllers/class.mailCard2.php');

$connPDO = new Conexion();

$mailCard= New MailCard();

$phpmailer = new PHPMailer();

$w_conexion = new MySQL();
$call = new CallCenter($connPDO,$phpmailer);

$data=$_GET["data"];
$correo = $_GET["mail"];

if($data == 1)
{
    $explode1 = explode("&", $_POST["dataSerialize"]);
    //$explode2 = explode("=", $explode1);
    $nuevopost = array();
    $barrios = array();
    foreach ($explode1 as $value) {
        $explode2= explode("=",$value);
        if ($explode2[0] == 'barrios%5B%5D') {
            $valbarriofinal = explode('-', $explode2[1]);
            array_push($barrios, $valbarriofinal[0]);
            $nuevopost['barrios'] = $barrios;
        }else{
            $nuevopost[$explode2[0]] = $explode2[1];            
        }
    }
    // echo "<pre>";
    //  print_r($nuevopost);
    //  echo "</pre>";die; 

    $response=$call->filterClientes($_POST, $nuevopost);
	echo json_encode($response);
	

}
if($data==2)
{
	/*obtiene el listado inmuebles disponibles y reservados por una inmobiliaria
	@IdInmobiliaria codigo de la inmobiliaria
	*/	
	// if($_GET['a'])
	// {
	// 	$_POST=$_GET['a'];
	// }
    
    
    $explode1 = explode("&", $_POST["dataSerialize"]);
    //$explode2 = explode("=", $explode1);
    $nuevopost = array();
    $barrios = array();
    $tpinmuebles = array();
    foreach ($explode1 as $value) {
        $explode2= explode("=",$value);
        if ($explode2[0] == 'barrios%5B%5D') {
            $valbarriofinal = explode('-', $explode2[1]);
            array_push($barrios, $valbarriofinal[0]);
            $nuevopost['barrios'] = $barrios;
        }elseif($explode2[0] == 'tpinmuebles%5B%5D'){
            $valtpinmueblefinal = explode('-', $explode2[1]);
            array_push($tpinmuebles, $valtpinmueblefinal[0]);
            $nuevopost['tpinmuebles'] = $tpinmuebles;
        }else{
            $nuevopost[$explode2[0]] = $explode2[1];            
        }
    }
    // echo "<pre>";
    //  print_r($nuevopost);
    //  echo "</pre>";die; 

	$response=$call->listaInmueblesCall($_POST, $nuevopost);
	echo json_encode($response);
}
if ($data == 3) {
	$response = $call->detail_inmueble($_POST['idinm']);

	$response['logo'] = getCampo('clientessimi',' where IdInmobiliaria = ' . $response['IdInmobiliaria'],'logo');
	$response['logo'] = "http://www.simiinmobiliarias.com/" . str_replace('../', '', $response['logo']);
	if ($response['IdGestion'] == 1) {
        $response['oper'] = 'rent';
        $response['precio'] = number_format($response['ValorCanon']);
    } else {
        $response['oper'] = 'sale';
        $response['precio'] = number_format($response['ValorVenta']);
    }

    $response['Tipo_Inmueble'] = ucfirst(strtolower(getCampo('tipoinmuebles',' where idTipoInmueble = ' . $response['IdTpInm'], 'Descripcion')));
    $response['ciudad'] = ucfirst(strtolower(getCampo('ciudad',' where IdCiudad = ' . getCampo('barrios', ' where IdBarrios = ' . $response['IdBarrio'], 'IdCiudad'), 'Nombre')));
    $response['depto'] = ucfirst(strtolower(getCampo('departamento',' where IdDepartamento = ' . getCampo('ciudad', ' where IdCiudad = ' . getCampo('barrios', ' where IdBarrios = ' . $response['IdBarrio'], 'IdCiudad'), 'IdDepartamento'), 'Nombre')));
    $response['Gestion'] = ucfirst(strtolower(getCampo('gestioncomer',' where IdGestion = ' . $response['IdGestion'],'NombresGestion')));
    $response['fotos'] = $call->getFotos($_POST['idinm']);
    $response['zona'] = ucfirst(strtolower(getCampo('zonas',' where IdZona = ' . $response['IdZona'],'NombreZ')));
    $response['localidad'] = ucfirst(strtolower(getCampo('localidad',' where idlocalidad = ' . $response['IdLocalidad'],'descripcion')));
    $response['barrio'] = ucfirst(strtolower(getCampo('barrios',' where IdBarrios = ' . $response['IdBarrio'],'NombreB')));

    for($i = 0; $i < count($response['fotos']); $i++){
        $response['fotos'][$i]['foto'] = "http://www.simiinmobiliarias.com/mcomercialweb/" . $response['fotos'][$i]['foto'];
        $valimage = val_image($response['fotos'][$i]['foto']);
        if ($valimage == 404) {
            $response['fotos'][$i]['foto'] = $response['logo'];
        } else {
            $response['fotos'][$i]['foto'] = $response['fotos'][$i]['foto'];
        }
    }

    // echo "<pre>";
    // print_r($response);
    // echo "</pre>";
    echo json_encode($response);
}
if ($data == 4) {
    $response = $call->crearReserva($_POST);
    echo json_encode($response);
}
if($data==5)
{
    /*devuelve información de los terceros
    @codinm codigo inmueble
    @IdInmobiliaria codigo de la inmobiliaria
    @rol_tr rol de tercero 1 propietario 2 apoderado 3 promotor 4 captador
    @iduser id tercero 
    */  
    $response=$call->datosInmuebleTerceros($_POST);
    echo json_encode($response);
}

if ($data == 6) {
    $response = $call->getClienteLlamada($_GET);

    echo json_encode($response);
}

if ($data == 7) {


    // echo "<pre>";
    // print_r($_REQUEST);
    // print_r($_SESSION);
    // print_r($_POST);
    // echo "</pre>";die;
    $response = $call->cearLlamada($_POST, $_REQUEST, $phpmailer);

    echo json_encode($response);
}

if ($data == 8) {
    $response = $call->grabarLlamada($_GET, $_POST);

    echo json_encode($response);
}

if ($data == 9) {
    $response = $call->getBarriosZona($_POST);

    echo json_encode($response);
}

if ($data == 10) {
    $response = $call->getDatosFicha();

    echo json_encode($response);
}

if ($data == 11) {
    // sleep(3);
    $response = $call->enviarFicha($_POST, $phpmailer, $mailCard);

    echo json_encode($response);
}
if ($data == 12) {
    /* trae las caracteristicas por cada inmueble.
    @IdInmobiliaria
    @codinm
    @tpcar tipo de caracteristica (1 internos, 2 externos, 3 alrededores)
     */
    $response = $call->getDataDetalleInmuebleFull($_POST);

    echo json_encode($response);
}
if ($data == 13) {
    /* valida el estado del inmueble
    @IdInmobiliaria
    @codinm
    
     */
    $response = $call->validaEstadoInm($_POST);

    echo json_encode($response);
}

if ($data == 14) {
    $response = $call->saveObservacionPropietario($_POST);

    echo json_encode($response);
}

if ($data == 15) {
    $response = $call->updateClienteCall($_POST);

    echo json_encode($response);
}

if ($data == 16) {
    $response = $call->getSitiosInteres($_POST);

    echo json_encode($response);
}

if ($data == 17) {
    $respoonse = array(
        'status' => 'Ok',
        'totalRegistros' => $_SESSION['RtblCall']
    );

    echo json_encode($respoonse);
}
if ($data == 18) {
    $respoonse = array(
        'status' => 'Ok',
        'totalRegistros' => $_SESSION['RSimilarestblCall']
    );

    echo json_encode($respoonse);
}

if($data == 19){
    $response = $call->crearCliente("",$_POST);
    
    echo json_encode($response);
}

if($data == 20){
    $response = $call->getDataCliente($_POST);
    
    echo json_encode($response);
}

if ($data == 21) {
    $response = $call->getUsuariosTercero($_POST);

    echo json_encode($response);
}

if ($data == 22) {
    $response = $call->getHistorialCitas($_POST);

    echo json_encode($response);
}

if ($data == 23) {
    $response = $call->getHistorialDemanda($_POST);

    echo json_encode($response);
}

if ($data == 24) {
    $response = $call->getDemandaUser($_POST);
    echo json_encode($response);
}

if ($data == 25) {
    $response = $call->getDataDemanda($_POST);
    echo json_encode($response);
}

if ($data == 26) {
    $response = $call->crearDemandaCallCruce($_POST);

    echo json_encode($response);
}

if ($data == 27) {
    $response = $call->editarDemandaCallCruce($_POST);

    echo json_encode($response);
}

if ($data == 28) {
    $response = $call->condiDemanda($_POST);

    echo json_encode($response);
}

if ($data == 29) {
    $save = $call->CrearSolicitudCompartir($_POST);
    if(isset($_POST) && !empty($_POST) && $save==1)
    {
        $response = $call->EnvioEmailOferta($_POST['codDeman'],$_POST['codInmu'],'');
    }

    echo $response;
}

function val_image($url) {
    $fp = curl_init($url);
    $ret = curl_setopt($fp, CURLOPT_RETURNTRANSFER, 1);
    $ret = curl_setopt($fp, CURLOPT_TIMEOUT, 30);
    $ret = curl_exec($fp);
    $info = curl_getinfo($fp, CURLINFO_HTTP_CODE);
    curl_close($fp);
    return $info;
}
